/**
 * Copyright 2016 Kareem Alkaseer.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mentorlycon.spark.examples

import java.sql.{Timestamp}
import java.text.SimpleDateFormat

import org.apache.spark.sql.expressions.{MutableAggregationBuffer, UserDefinedAggregateFunction}
import org.apache.spark.sql.{DataFrame, Row, SQLContext}
import org.apache.spark.sql.types._
import org.apache.spark.{SparkContext, SparkConf}
import org.apache.spark.sql.functions._

import scala.util.{Try, Failure, Success}

class ConditionalAggregate[T, R, TT<: DataType, RT <: DataType](
  initial: R, update: (R, T) => R, merge: (R, R) => R,
  tfactory: () => TT, rfactory: () => RT)
extends UserDefinedAggregateFunction {

  def inputSchema = new StructType().add("value", tfactory())
  def bufferSchema = new StructType().add("count", rfactory())
  def dataType = rfactory()
  def deterministic = true
  def initialize(buf: MutableAggregationBuffer) = buf.update(0, initial)
  def update(buf: MutableAggregationBuffer, input: Row) = {
    buf.update(0, update(buf.getAs[R](0), input.getAs[T](0)))
  }
  def merge(buf1: MutableAggregationBuffer, buf2: Row) = {
    buf1.update(0, merge(buf1.getAs[R](0), buf2.getAs[R](0)))
  }
  def evaluate(buf: Row) = buf.getAs[R](0)
}

class UserTypeAggregate(f: (Int) => Boolean) extends UserDefinedAggregateFunction {

  def inputSchema = new StructType().add("isNew", IntegerType)
  def bufferSchema = new StructType().add("count", LongType)
  def dataType = LongType
  def deterministic = true
  def initialize(buf: MutableAggregationBuffer) = buf.update(0, 0L)
  def update(buf: MutableAggregationBuffer, input: Row) = {
    buf.update(0, if (!input.anyNull && f(input.getInt(0))) buf.getLong(0) + 1L else buf.getLong(0))
  }
  def merge(buf1: MutableAggregationBuffer, buf2: Row) = {
    buf1.update(0, buf1.getLong(0) + buf2.getLong(0))
  }
  def evaluate(buf: Row) = buf.getLong(0)
}

object Student {

  val conf = new SparkConf().setMaster("local").setAppName("Student")
  val sc = new SparkContext(conf)
  val sqlContext = new SQLContext(sc)

  var usersFirstSeen: DataFrame = null
  var dailyFeeds: DataFrame = null
  var feeds: DataFrame = null

  val pathPrefix = "file:///Users/kalkaseer/Dev/Datasets/"

  var datasetName = "set_a"
  var datasetDir = datasetName + "_files"

  def workingSet(name: String): Unit = {
    datasetName = name
    datasetDir = datasetName + "_files"
  }

  var debug = true
  var saveToDisk = true
  var saveAggregates = true
  var generateUserAggregates = false

  var loadFeeds: () => DataFrame = { touch }


  sqlContext.udf.register(
    "agg_negative", new ConditionalAggregate[Integer, Long, IntegerType, LongType](
      0L, updateNegative, mergeAcc, tfactory, rfactory))

  sqlContext.udf.register(
    "agg_positive", new ConditionalAggregate[Integer, Long, IntegerType, LongType](
      0L, updatePositive, mergeAcc, tfactory, rfactory))

  sqlContext.udf.register("agg_new_users", new UserTypeAggregate(updateNewUsers))
  sqlContext.udf.register("agg_returning_users", new UserTypeAggregate(updateReturningUsers))

  sqlContext.udf.register("cleanup_keywords", (s: String) => cleanupKeywords(s))
  sqlContext.udf.register("create_timestamp", (s: String) => parseDate(s))
  sqlContext.udf.register("cleanup_medium", (s: String) => if (s != null && !s.equals("(none)")) s else null)
  sqlContext.udf.register("assign_bounced", (s: String) => if (s.equals("Y")) 1 else 0)
  sqlContext.udf.register("assign_appflag", (s: String) => if (s.equals("1")) 1 else 0)


  def updateNegative(acc: Long, value: Integer): Long = {
    if (value == 0) acc + 1L else acc
  }

  def updatePositive(acc: Long, value: Integer): Long = {
    if (value == 1) acc + 1L else acc
  }

  def mergeAcc(acc1: Long, acc2: Long): Long = {
    return acc1 + acc2
  }

  def tfactory() = IntegerType
  def rfactory() = LongType


  def updateNewUsers(isNew: Int) = isNew == 1
  def updateReturningUsers(isNew: Int) = isNew != 1


  def cleanupKeywords(keywords: String): String = keywords match {
    case null => null
    case "" => null
    case _ =>
      keywords.split("\\|")
        .map(_.replaceAll("\\?|\\+", "").trim)
        .reduce(_ + "|" + _)
  }

  def parseDate(s: String): Timestamp = s match {
    case "" => null
    case null => null
    case _ => {
      val format = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss")
      Try(new Timestamp(format.parse(s).getTime)) match {
        case Success(t) => t
        case Failure(_) => null
      }
    }
  }

  def main(args: Array[String]) {

    workingSet("set_a")
    run()
    loadFeeds = makeSetB
    run()
  }

  def touch(): DataFrame = {

    val schema = StructType(Array(
      StructField("id",        LongType,    false),
      StructField("date",      StringType,  false),
      StructField("country",   StringType,  false),
      StructField("campaign",  StringType,  false),
      StructField("source",    StringType,  true),
      StructField("keywords",  StringType,  true),
      StructField("cookie",    StringType,  false), // represent unique users
      StructField("device",    StringType,  true),
      StructField("medium",    StringType,  true),
      StructField("session",   IntegerType, false),
      StructField("bounced",   StringType,  false), // doesn't allow for bounce rate analysis
      StructField("pageViews", IntegerType, false), // assumed to be
      StructField("duration",  IntegerType, true),
      StructField("appFlag",   StringType,  false)
    ))

    val df = sqlContext.read
      .format("com.databricks.spark.csv")
      .option("header", "false")
      .schema(schema)
      .load(pathPrefix + datasetName)

    df.registerTempTable("feedsTemp")

    sqlContext.sql(
      "select id, cast(create_timestamp(date) as timestamp) as date, country, campaign, source, " +
        "cleanup_keywords(keywords) as keywords, " +
        "cookie, device, cleanup_medium(medium) as medium, session, assign_bounced(bounced) as bounced, pageViews, " +
        "duration, assign_appflag(appFlag) as appFlag from feedsTemp"
    )
  }

  def facts() {
    if (feeds == null)
      feeds = loadDataFrame(pathPrefix + datasetDir + "/feeds")
    val factsCube = feeds.groupBy("date", "country", "source", "medium", "campaign", "keywords", "device", "appFlag")
      .agg(
        countDistinct("cookie").as("unique_cookies"),
        countDistinct("session").as("unique_session"),
        sum("pageViews").as("total_pageViews"),
        sum("duration").as("total_duration"),
        avg("duration").as("average_duration"),
        expr("agg_negative(bounced) as bounced_no"),
        expr("agg_positive(bounced) as bounced_yes"),
        expr("agg_negative(appFlag) as app_0"),
        expr("agg_positive(appFlag) as app_1"))

    factsCube.persist()

    if (saveToDisk)
      writeToDisk(factsCube, pathPrefix + datasetDir + "/facts")

    if (debug)
      show(factsCube, "factsCube", 200)

    factsCube.unpersist()
  }

  def loadSetA(): DataFrame = {
    loadDataFrame(pathPrefix + "set_a_files/feeds")
  }

  def makeSetB(): DataFrame = {
    workingSet("set_b")
    loadSetA().unionAll(touch())
  }

  def run() {

    feeds = loadFeeds()
    feeds.registerTempTable("feeds")
    feeds.persist()

    if (saveToDisk)
      writeToDisk(feeds, pathPrefix + datasetDir + "/feeds")

    var dailyFeedTemp = sqlContext.sql(
      "select to_date(date) as date, country, campaign, source, keywords, cookie, device, medium, session, bounced, " +
        "pageViews, duration, appFlag from feeds")
    dailyFeedTemp.persist()
    dailyFeedTemp.registerTempTable("dailyFeedTemp")

    if (generateUserAggregates)
      userAggregates()

    usersFirstSeen =
      sqlContext.sql("select cookie, to_date(date) as date from feeds").groupBy("cookie")
        .agg(min("date").as("first_seen"))
    usersFirstSeen.persist()
    usersFirstSeen.registerTempTable("usersFirstSeen")

    feeds.unpersist()
    feeds = null

    if (saveToDisk)
      writeToDisk(usersFirstSeen, pathPrefix + datasetDir + "/firstSeen")

    if (debug)
      show(usersFirstSeen, "usersFirstSeen", 100)

    dailyFeeds = sqlContext.sql(
      "select to_date(f.date) as date, f.country, f.campaign, f.source, f.keywords, f.cookie, f.device, f.medium, " +
      "f.session, f.bounced, f.pageViews, f.duration, f.appFlag, " +
      "cast(to_date(f.date) = u.first_seen as int) as is_new_user from dailyFeedTemp f " +
      "left outer join usersFirstSeen u on f.cookie = u.cookie")
    dailyFeeds.persist()
    dailyFeeds.registerTempTable("dailyFeed")

    if (saveToDisk)
      writeToDisk(dailyFeeds, pathPrefix + datasetDir + "/dailyFeeds")

    if (debug)
      show(dailyFeeds, "dailyFeeds", 100)

    dailyFeedTemp.unpersist()
    dailyFeedTemp = null

    usersFirstSeen.unpersist()
    usersFirstSeen = null

    var aggregates = dailyFeeds.groupBy("date").agg(
      count("date").as("#records"),
      countDistinct("session").as("unique_sessions"),
      sum("pageViews").as("page_views"),
      avg("duration").as("average_duration"),
      expr("agg_negative(bounced) as bounced_no"),
      expr("agg_positive(bounced) as bounced_yes"),
      expr("agg_negative(appFlag) as app_0"),
      expr("agg_positive(appFlag) as app_1"),
      expr("agg_new_users(is_new_user) as new_users"),
      expr("agg_returning_users(is_new_user) as returning_users")
    ).sort("date")

    if (debug)
      show(aggregates, "aggregates", 100)

    if (saveToDisk || saveAggregates)
      writeToDisk(aggregates, pathPrefix + datasetDir + "/aggregates")

    aggregates = null

    val countriesNewUsers = sqlContext.sql(
      "select date, country, count(date) as new_users from dailyFeed " +
      "where is_new_user = 1 " +
      "group by country, date " +
      "order by country, date")

    if (debug)
      show(countriesNewUsers, "countriesDailyNewUsers", 100)

    if (saveToDisk)
      writeToDisk(countriesNewUsers, pathPrefix + datasetDir + "/countriesNewUsers")

    dailyFeeds.unpersist()
    dailyFeeds = null
  }

  def processAggregates(dataset: String) {

    val aggregates = loadAggregates(dataset)
    aggregates.persist()
    val size = aggregates.count()
    aggregates.show(size.toInt)
  }

  def loadAggregates(dataset: String): DataFrame = {
    sqlContext.read
      .format("parquet")
      .load(pathPrefix + dataset + "_files/aggregates.parquet")
      .sort("date")
  }

  def loadDataFrame(path: String): DataFrame = {
    sqlContext.read.format("parquet").load(path + ".parquet")
  }

  def writeToDisk(df: DataFrame, path: String, csv: Boolean = true) {
    df.write.format("parquet").save(path + ".parquet")
    if (csv) {
      df.write
        .format("com.databricks.spark.csv")
        .option("header", "true")
        .save(path + ".csv")
    }
  }

  def show(df: DataFrame, name: String, count: Int = -1) {
    val size = df.count().toInt
    println("DataFrame: " + name + " count: " + size)
    df.show(if (count == -1) size else count)
  }

}
