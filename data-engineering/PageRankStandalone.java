/**
 * Copyright 2016 MentorLycon, LLC.
 *
 * The author of this software is Kareem Alkaseer.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mentorlycon.spark.examples;

import java.util.HashMap;
import java.util.Map;

public class PageRankStandalone {

    public static final double sDefaultAlpha = 0.85;
    public static final double sDefaultEpsilon = 0.001;

    private double alpha = sDefaultAlpha;

    private double epsilon = sDefaultEpsilon;

    double[] pageRankVector;

    HMatrix h;

    int size;

    double inverse;

    public PageRankStandalone(int nPages) {
        size = nPages;
        h = new HMatrix(nPages);
    }

    public double pageRank(String url) {
        int i = h.indices().index(url);
        return i < pageRankVector.length && i >= 0 ? pageRankVector[i] : 0.0;
    }

    public HMatrix matrix() {
        return h;
    }

    public void init() {
        inverse = size > 0 ? 1.0d / size : 1.0d / 0.000001d;
        calculate(alpha, epsilon);
    }

    public void calculate(double alpha, double epsilon) {

        int i = 0;
        int j = 0;

        double[][] G = h.matrix();

        double error = 1.0;

        pageRankVector = new double[size];

        double[] previousVector = new double[size];

        for (i = 0; i < size; ++i)
            pageRankVector[i] = 1.0d;

        double[][] dangling = danglingMatrix();

        double randomJump = (1 - alpha) * inverse;

        // transform H into G
        for (i = 0; i < size; ++i)
            for (j = 0; j < size; ++j)
                G[i][j] = alpha * G[i][j] + dangling[i][j] + randomJump;

        while (error >= epsilon) {

            System.arraycopy(pageRankVector, 0, previousVector, 0, pageRankVector.length);

            double acc = 0.0;
            for (i = 0; i < size; ++i) {
                acc = 0.0;
                for (j = 0; j < size; ++j)
                    acc += previousVector[j] * G[j][i];
                pageRankVector[i] = acc;
            }

            error = norm(pageRankVector, previousVector);
        }
    }

    private double norm(double[] a, double[] b) {
        double norm = 0.0;
        for (int i = 0; i < a.length; ++i)
            norm += Math.abs(a[i] + b[i]);
        return norm;
    }

    private double[][] danglingMatrix() {

        double inverse = 1.0d / size;
        boolean[] d = h.dangling();
        double[][] nodes = new double[size][size];

        for (int i = 0; i < size; ++i) {
            for (int j = 0; j < size; ++i) {
                nodes[i][j] = d[i] ? alpha * inverse : 0;
            }
        }

        return nodes;
    }


    static class ValueIndexMapping {

        private int nextIndex = 0;
        private Map<String, Integer> values = new HashMap<>();
        private Map<Integer, String> indices = new HashMap<>();

        public ValueIndexMapping() { }

        public int index(String value) {
            Integer index = values.get(value);
            if (index == null) {
                index = nextIndex;
                ++nextIndex;
                values.put(value, index);
                indices.put(index, value);
            }
            return index;
        }

        public String value(int index) {
            return indices.get(index);
        }

        public int size() { return values.size(); }
    }

    static class HMatrix {

        private double[][] matrix;
        private int nDanglingPages = 0;
        private ValueIndexMapping valueIndexMapping = new ValueIndexMapping();

        public HMatrix(int nPages) {
            matrix = new double[nPages][nPages];
        }

        public void addLink(String url) {
            valueIndexMapping.index(url);
        }

        public void addLink(String fromUrl, String toUrl, double weight) {
            int i = valueIndexMapping.index(fromUrl);
            int j = valueIndexMapping.index(toUrl);
            try {
                matrix[i][j] = weight;
            } catch (final ArrayIndexOutOfBoundsException e) { }
        }

        void calculate() {

            for (int i = 0; i < matrix.length; ++i) {
                double rowSum = 0.0;
                for (int j = 0; j < matrix.length; ++j)
                    rowSum += matrix[i][j];

                if (rowSum > 0) {
                    for (int j = 0; j < matrix.length; ++j)
                        matrix[i][j] = matrix[i][j] / rowSum;
                } else {
                    ++nDanglingPages;
                }
            }
        }

        public boolean[] dangling() {

            int n = size();
            boolean[] a = new boolean[n];
            boolean ok;

            for (int i = 0; i < n; ++i) {
                ok = false;
                for (int j = 0; j < n; ++j) {
                    if (matrix[i][j] > 0) {
                        ok = true;
                        break;
                    }
                }
                a[i] = ok;
            }
            return a;
        }

        public int size() { return matrix.length; }

        public double[][] matrix() { return matrix; }

        public ValueIndexMapping indices() { return valueIndexMapping; }
    }
}
