/**
 * Copyright 2016 Kareem Alkaseer.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mentorlycon.spark.examples

import org.apache.spark.graphx.{Graph, Edge, VertexId}
import org.apache.spark.rdd.RDD
import org.apache.spark.{HashPartitioner, Partitioner, SparkConf, SparkContext}


class DomainPartitioner(count: Int) extends Partitioner {
  override def numPartitions: Int = count
  override def getPartition(key: Any): Int = {
    val domain = new java.net.URL(key.toString).getHost()
    val hcode = domain.hashCode % numPartitions
    if (hcode < 0) { hcode + numPartitions } else { hcode }
  }
  override def equals(other: Any): Boolean = other match {
    case dp: DomainPartitioner =>
      dp.numPartitions == numPartitions
    case _ =>
      false
  }
}

object PageRank {

  def main(args: Array[String]) {
//    naive()
//    efficient()
    Student.run()
  }

  def naive() {

    val conf = new SparkConf().setMaster("local").setAppName("PageRank")
    val sc = new SparkContext(conf)

    val links = sc.parallelize(List(
      ("http://url1", List("http://url2", "http://url3")),
      ("http://url2", List("http://url3", "http://url4")),
      ("http://url5", List("http://url6", "http://url7"))
    )).partitionBy(new DomainPartitioner(10))
      .persist()

    var ranks = links.mapValues(l => 1.0)

    for (i <- 0 until 10) {

      val contribs = links.join(ranks).flatMap {
        case (src, (links, rank)) =>
          links.map(dest => (dest, rank / links.size))
      }

      val mapped = contribs.reduceByKey((x, y) => x + y).mapValues(v => 0.15 + 0.85 * v)
      ranks = ranks.union(mapped).reduceByKey((x, y) => x + y)
      val total = ranks.values.reduce(_+_)
      ranks = ranks.mapValues(x => x / total)
    }

    println("ranks: " + ranks.collect().mkString(","))
  }

  def efficient() {

    val conf = new SparkConf().setMaster("local").setAppName("PageRank")
    val sc = new SparkContext(conf)

    val links: RDD[(VertexId, String)] = sc.parallelize(
      Array(
        (1L, "http://url1"), (2L, "http://url2"), (3L, "http://url3"),
        (4L, "http://url4"), (5L, "http://url5"), (6L, "http://url6"),
        (7L, "http://url7")
      ))

    val edges: RDD[Edge[Int]] = sc.parallelize(
      Array(
        Edge(1L, 2L, 0), Edge(1L, 3L, 0),
        Edge(2L, 3L, 0), Edge(2L, 4L, 0),
        Edge(5L, 6L, 0), Edge(5L, 7L, 0)
      ))

    val defaultUrl = "http://none"

    val graph = Graph(links, edges, defaultUrl)

    val ranks = graph.pageRank(0.0001).vertices

    println(ranks.collect().mkString("\n"))
  }
}

