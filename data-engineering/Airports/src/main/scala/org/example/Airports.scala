/**
 * Copyright 2016 Kareem Alkaseer.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.example

import java.io.File

import org.apache.spark.sql.{SparkSession, DataFrame}
import org.apache.spark.sql.types._
import org.apache.spark.sql.functions._


object Airports extends App {


  val a = new Airports(Some("data"), "data")

  a.touchCountries()
  a.touchAirports()
  a.touchRunways()
  a.makeMaster()

  a.country("ru").show
  a.country("zim").show

  val airportRanks = a.countryAirportsRanks
  airportRanks.createOrReplaceTempView("airportRanks")
  a.max(airportRanks, "airportRanks", 10).show()
  a.min(airportRanks, "airportRanks", 10).show()

  a.countryRunwayTypes

  val runwayRanks = a.countryRunwaysRanks
  runwayRanks.show(10000)
  runwayRanks.createOrReplaceTempView("runwayRanks")
  a.max(runwayRanks, "runwayRanks", 10).show()
  a.min(runwayRanks, "runwayRanks", 10).show()
}


class Airports(inputDir: Option[String], dataDir: String) {

  val sqlc = SparkSession.builder
    .master("local[2]")
    .appName("Airports")
    .getOrCreate()

  val toBool = udf((o: AnyRef) => Utils.getBoolean(o))

  var countriesDF :DataFrame = null
  var airportsDF :DataFrame = null
  var runwaysDF :DataFrame = null
  var countryAirportsDF :DataFrame = null
  var airportRunwaysDF :DataFrame = null


  def touchCountries(force: Boolean = false): DataFrame = {

    val f = new File(dataDir + File.separator + "countries.parquet")

    if (! force && f.exists()) {

      countriesDF = sqlc.read.format("parquet").load(f.getAbsolutePath)

    } else {

      val schema = StructType(Array(
        StructField("id", LongType),
        StructField("code", StringType),
        StructField("name", StringType),
        StructField("continent", StringType),
        StructField("wikiLink", StringType),
        StructField("keywords", StringType)
      ))

      countriesDF = sqlc.read
        .format("com.databricks.spark.csv")
        .option("header", "true")
        .schema(schema)
        .load(inputDir.get + File.separator + "countries.csv")

      if (f.exists()) {
        for (sf <- f.listFiles())
          sf.delete()
        f.delete()
      }

      countriesDF.write.format("parquet").save(f.getAbsolutePath)

    }

    countriesDF.createOrReplaceTempView("countries")
    countriesDF
  }

  def touchAirports(force: Boolean = false): DataFrame = {

    val f = new File(dataDir + File.separator + "airports.parquet")

    if (! force && f.exists()) {

      airportsDF = sqlc.read.format("parquet").load(f.getAbsolutePath)

    } else {

      val schema = StructType(Array(
        StructField("id", LongType),
        StructField("identity", StringType),
        StructField("type", StringType),
        StructField("name", StringType),
        StructField("latitude", DoubleType),
        StructField("longitude", DoubleType),
        StructField("elevation", IntegerType, true),
        StructField("continent", StringType),
        StructField("country", StringType),
        StructField("region", StringType),
        StructField("municipality", StringType),
        StructField("_serviceScheduled", StringType),
        StructField("gpsCode", StringType),
        StructField("iata", StringType),
        StructField("localCode", StringType),
        StructField("homeLink", StringType),
        StructField("wikiLink", StringType),
        StructField("keywords", StringType)
      ))

      airportsDF = sqlc.read
        .format("com.databricks.spark.csv")
        .option("header", "true")
        .schema(schema)
        .load(inputDir.get + File.separator + "airports.csv")
        .selectExpr("*", "cast(_serviceScheduled as boolean) serviceScheduled")
        .drop("_serviceScheduled")


      if (f.exists()) {
        for (sf <- f.listFiles())
          sf.delete()
        f.delete()
      }

      airportsDF.write.format("parquet").save(f.getAbsolutePath)
    }

    airportsDF.createOrReplaceTempView("airports")
    airportsDF
  }

  def touchRunways(force: Boolean = false): DataFrame = {

    val f = new File(dataDir + File.separator + "runways.parquet")

    if (! force && f.exists()) {

      runwaysDF = sqlc.read.format("parquet").load(f.getAbsolutePath)

    } else {

      val schema = StructType(Array(
        StructField("id", LongType),
        StructField("airport", StringType),
        StructField("airportIdentity", StringType),
        StructField("length", IntegerType, true),
        StructField("width", IntegerType, true),
        StructField("surface", StringType),
        StructField("_lighted", StringType, true),
        StructField("_closed", StringType, true),
        StructField("leIdentity", StringType),
        StructField("leLatitude", DoubleType, true),
        StructField("leLongitude", DoubleType, true),
        StructField("leElevation", IntegerType, true),
        StructField("leHeadingDegree", FloatType, true),
        StructField("leDisplacedThreshold", IntegerType, true),
        StructField("heIdentity", StringType),
        StructField("heLatitude", DoubleType, true),
        StructField("heLongitude", DoubleType, true),
        StructField("heElevation", IntegerType, true),
        StructField("heHeadingDegree", FloatType, true),
        StructField("heDisplacedThreshold", IntegerType, true)
      ))

      runwaysDF = sqlc.read
        .format("com.databricks.spark.csv")
        .option("header", "true")
        .schema(schema)
        .load(inputDir.get + File.separator + "runways.csv")
        .selectExpr("*", "cast(_lighted as boolean) lighted")
        .selectExpr("*", "cast(_closed as boolean) closed")
        .drop("_lighted")
        .drop("_closed")

      if (f.exists()) {
        for (sf <- f.listFiles())
          sf.delete()
        f.delete()
      }

      runwaysDF.write.format("parquet").save(f.getAbsolutePath)
    }


    runwaysDF.createOrReplaceTempView("runways")
    runwaysDF
  }

  def makeMaster(force :Boolean = false)  = {

    val countryAirportsFile = new File(dataDir + File.separator + "countryAirports.parquet")
    val airportRunwaysFile = new File(dataDir + File.separator + "airportRunways.parquet")

    if (! force && countryAirportsFile.exists()) {

      countryAirportsDF = sqlc.read.format("parquet").load(countryAirportsFile.getAbsolutePath)

    } else {

      countryAirportsDF = sqlc.sql(
        "select c.id as c_id, c.code as c_code, c.name as c_name, c.continent as c_continent, " +
          "c.wikiLink as c_wikiLink, c.keywords as c_keywords, " +
          "a.id as a_id, a.identity as a_identity, a.type as a_type, " +
          "a.name as a_name, a.latitude as a_latitude, a.longitude as a_longitude, " +
          "a.elevation as a_elevation, a.region as a_region, a.municipality as a_municipality, " +
          "a.serviceScheduled as a_serviceScheduled, a.gpsCode as a_gpsCode, " +
          "a.iata as a_iata, a.localCode as a_localCode, a.homeLink as a_homeLink, " +
          "a.wikiLink as a_wikiLink, a.keywords as a_keywords " +
          "from countries c left outer join airports a on c.code = a.country "
      )
    }

    countryAirportsDF.createOrReplaceTempView("countryAirports")

    if (! force && airportRunwaysFile.exists()) {
      airportRunwaysDF = sqlc.read.format("parquet").load(airportRunwaysFile.getAbsolutePath)
    } else {

      airportRunwaysDF = sqlc.sql(
        "select cap.*, r.id as r_id, r.length as r_length, r.width as r_width, " +
          "r.surface as r_surface, r.lighted as r_lighted, r.closed as r_closed, " +
          "r.leIdentity as r_leIdentity, r.leLatitude as r_leLatitude, " +
          "r.leLongitude as r_leLongitude, " +
          "r.leElevation as r_leElevation, r.leDisplacedThreshold as r_leDisplacedThreshold, " +
          "r.heIdentity as r_heIdentity, r.heLatitude as r_heLatitude, " +
          "r.heLongitude as r_heLongitude, " +
          "r.heElevation as r_heElevation, r.heDisplacedThreshold as r_heDisplacedThreshold " +
          "from countryAirports cap left outer join runways as r on cap.a_identity = r.airportIdentity "
      )

      airportRunwaysDF.write.format("parquet").save(airportRunwaysFile.getAbsolutePath)
    }

    airportRunwaysDF.createOrReplaceTempView("airportsRunways")
  }

  def countryAirportsRanks: DataFrame = {

    sqlc.sql(
      "select c_code, count, dense_rank() over (order by count desc) as rank from (" +
      "  select c_code, count(a_id) as count from countryAirports group by c_code order by count desc" +
      ") order by count desc"
    )
  }

  def max(df: DataFrame, table: String, n: Int): DataFrame = {
    sqlc.sql(
      s"select * from $table where rank in (" +
      s"  select rank from $table order by rank asc limit $n" +
      s")"
    )
  }

  def min(df: DataFrame, table: String, n: Int): DataFrame = {
    sqlc.sql(
      s"select * from $table where rank in (" +
      s"  select rank from $table order by rank desc limit $n" +
      s")"
    )
  }

  def countryRunwaysRanks: DataFrame = {

    sqlc.sql(
      "select c_code, count, dense_rank() over (order by count desc) as rank from (" +
        "  select c_code, count(r_leIdentity) as count from airportsRunways where r_leIdentity <> null" +
        "  group by c_code order by count desc" +
        ") order by count desc"
    )
  }

  def countryRunwayTypes: DataFrame = {
    sqlc.sql(
      "select c_code, a_name, r_leIdentity from airportsRunways where r_leIdentity is not null " +
      " group by c_code, a_name, r_leIdentity " +
      " order by c_code, a_name"
    )
  }

  def country(s: String): DataFrame = {
    sqlc.sql(
      s"select c_name, a_name, r_id from airportsRunways where " +
      s" (length('$s') = 2 and upper(c_code) = upper('$s')) or " +
      s"  upper(c_name) like concat('%', upper('$s'), '%')"
    )
  }


}
