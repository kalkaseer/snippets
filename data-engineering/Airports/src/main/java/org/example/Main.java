/**
 * Copyright 2016 Kareem Alkaseer.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.example;

import org.apache.commons.cli.*;
import org.example.beans.*;
import org.example.sql.SqlDbInterface;
import org.example.sql.SqlRowSet;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.nio.charset.Charset;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Main {

    private static final Logger kLog = Logger.getLogger(Main.class.getName());

    private static final byte[] newline = new byte[]{ '\n' };


    private File logDir;

    private File logFile;

    private String dbPath;

    private String cvsPath;


    public void start(String [] args) {

        dbPath = "data/airports.db";
        cvsPath = "data";

        CommandLine line = parseArgs(args);

        if (line.hasOption("cvsfiles")) {
            cvsPath = line.getOptionValue("cvsfile");
        }

        if (line.hasOption("dbfile")) {
            dbPath = line.getOptionValue("dbfile");
        }

        if (true /*line.hasOption("logDir")*/) {

//            String logPath = line.getOptionValue("logDir");

            String logPath = "log";

            File f = new File(logPath);

            if (! f.exists()) {
                if (! f.mkdirs()) {
                    kLog.warning("failed to create log files");
                    logDir = null;
                }
            }

            if (! f.isDirectory()) {

                logDir = f.getParentFile();
                if (logDir == null) {
                    kLog.warning("failed to create log directory");
                    logDir = null;
                } else {
                    logFile = f;
                }

            } else {

                logDir = f;
                logFile = new File(f, makeFilename("commands_", "log"));
            }
        }

        ensureDatabase(dbPath, cvsPath);

        Scanner scan = new Scanner(System.in);
        String s;

        try {
            do {
                System.out.print("=> ");
                s = scan.nextLine();

                if (s.equalsIgnoreCase("report")) {
                    report();
                } else {
                    lookup(s);
                }

            } while (!s.equalsIgnoreCase("quit"));
        } catch (final NoSuchElementException e) { }
    }

    private String makeFilename(String prefix, String ext) {

        Calendar cal = Calendar.getInstance();
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'_'HH-mm-ss");
        df.setTimeZone(cal.getTimeZone());
        String s = prefix != null ?
            prefix + df.format(cal.getTime()) : df.format(cal.getTime());

        if (ext != null && ext.length() != 0)
            return s + "." + ext;
        return s;
    }

    public static void main(String [] args) {
        new Main().start(args);
    }

    private CommandLine parseArgs(String [] args) {

        Options options = new Options();

        options.addOption( new Option("report", "generate report"));
        options.addOption( Option.builder()
            .longOpt("country").hasArg().desc("country name or ISO code").build());
        options.addOption( Option.builder()
            .longOpt("dbfile").hasArg().desc("path to db file").build());
        options.addOption( Option.builder()
            .longOpt("cvsfiles").hasArg().desc("path to directory containing cvs files").build());
        options.addOption( Option.builder()
            .longOpt("logDir").hasArg().desc("path to directory to log command output").build());

        CommandLineParser parser = new DefaultParser();
        try {
            return parser.parse(options, args);
        } catch (ParseException e) {
            kLog.severe("could not parse command line arguments");
            System.exit(0);
        }
        return null;
    }

    public void ensureDatabase(String dbPath, String dataPath) {

        try {
            new SqlDbInterface(dbPath, dataPath);
        } catch (Exception e) {
            Utils.rethrow(e);
        }
    }

    private byte[] encode(String s) {
        return s.getBytes(Charset.forName("UTF-8"));
    }

    private BufferedOutputStream logCommand(BufferedOutputStream out, String s) throws Exception {

        if (logDir != null) {
            if (out == null)
                out = new BufferedOutputStream(new FileOutputStream(logFile, true));
            out.write(encode(s));
            out.write(newline);
        }

        System.out.println(s);

        return out;
    }

    private BufferedOutputStream logReport(BufferedOutputStream out, String s) throws Exception {

        if (logDir != null) {
            if (out == null)
                out = new BufferedOutputStream(
                        new FileOutputStream(new File(logDir, makeFilename("report_", "log")), true));
            out.write(encode(s));
            out.write(newline);
        }

        System.out.println(s);

        return out;
    }

    public void test() {

        DbInterface db;
        DbContext cxt = null;
        RowSet rs = null;

        try {

            db = new SqlDbInterface(dbPath);
            cxt = db.context();

            rs = db.get(cxt, db.query().table("countries"));

            RecordConverter converter = new RecordConverter();

            while (rs.next()) {
                Country c = converter.bean(rs, Country.class, Country.kDefaultConverter);
                System.out.println(c);
            }

            rs.close();
            rs = db.get(cxt, db.query().table("airports"));

            while (rs.next()) {
                Airport a = converter.bean(rs, Airport.class, Airport.kDefaultConverter);
                System.out.println(a);
            }
            rs.close();

            rs = db.get(cxt, db.query().table("runways"));

            while (rs.next()) {
                Runway a = converter.bean(rs, Runway.class, Runway.kDefaultConverter);
                System.out.println(a);
            }
            rs.close();

            rs = db.get(cxt, db.query().table("country_airports").limit(59));

            while (rs.next()) {
                List<CountryAirports> a = CountryAirports.kDefaultConverter.list(rs, CountryAirports.class);
                System.out.println(a);
            }
            rs.close();

            rs = db.get(cxt, db.query().table("airport_runways").orderBy(Airport.C.identity).limit(20));

            while (rs.next()) {
                List<AirportRunways> a = AirportRunways.kDefaultConverter.list(rs, AirportRunways.class);
                a.forEach(m -> System.out.println("airport: " + m.getAirport().getIdentity()
                        + " " + m.getAirport().getName() + " " + m.getAirport().getCountry() + " " + m.getRunways()));
            }
            rs.close();

//             QUERY

            rs = db.get(
                cxt,
                db.query()
                    .table("airport_runways")
                    .cells(
                        Arrays.asList(
                            Airport.C.name,
                            Airport.C.identity,
                            Country.C.code,
                            Country.C.continent,
                            Runway.C.id))
                    .orderBy(Airport.C.name)
                    .where(Collections.singletonList(Country.C.code.toUpperCase()))
                    .args(Collections.singletonList(("ru".toUpperCase())))
            );

            while (rs.next()) {
                AirportRunways a = AirportRunways.kDefaultConverter.bean(rs, AirportRunways.class);
            }
            rs.close();

            rs = db.get(
                cxt,
                db.query()
                    .table("airport_runways")
                    .cells(
                        Arrays.asList(
                            Airport.C.name,
                            Airport.C.identity,
                            Country.C.code,
                            Country.C.continent,
                            Runway.C.id))
                    .orderBy(Airport.C.name)
                    .where(Collections.singletonList(Country.C.code.toUpperCase()))
                    .args(Collections.singletonList("%zim%".toUpperCase()))
                    .whereRelations(Collections.singletonList(" LIKE "))
            );

            while (rs.next()) {
                AirportRunways a = AirportRunways.kDefaultConverter.bean(rs, AirportRunways.class);
            }
            rs.close();

            // REPORTING

            Query q = db.query()
                .table("country_airports")
                .cells(
                    Arrays.asList(
                        Country.C.id,
                        Country.C.code,
                        Country.C.name))
                .agg("count", Collections.singletonList(Airport.C.id), Airport.C.count)
                .groupBy(Country.C.code)
                .orderBy("count", Collections.singletonList(Airport.C.id), false)
                .limit(10);

            rs = db.get(cxt, q);
            while (rs.next()) {
                CountryAirportsRank a = CountryAirportsRank
                    .kDefaultConverter.bean(rs, CountryAirportsRank.class);
                System.out.println(a);
            }

            Connection conn = (Connection) cxt.context();
            Statement s = conn.createStatement();
            ResultSet r = s.executeQuery("select c_code, c_name, count(a_id) " +
                "from country_airports group by c_code order by count(a_id) desc limit 10");
            SqlRowSet qrs = new SqlRowSet(r);
            while (qrs.next()) qrs.print();
            qrs.close();


            q = db.query()
                .table("country_airports")
                .cells(
                    Arrays.asList(
                        Country.C.id,
                        Country.C.code,
                        Country.C.name))
                .agg("count", Collections.singletonList(Airport.C.id), Airport.C.count)
                .groupBy(Country.C.code)
                .orderBy("count", Collections.singletonList(Airport.C.id), true)
                .limit(10);

            rs = db.get(cxt, q);
            while (rs.next()) {
                CountryAirportsRank a = CountryAirportsRank
                    .kDefaultConverter.bean(rs, CountryAirportsRank.class);
                System.out.println(a);
            }

            conn = (Connection) cxt.context();
            s = conn.createStatement();
            r = s.executeQuery("select c_code, c_name, count(a_id) " +
                "from country_airports group by c_code order by count(a_id) asc limit 10");
            qrs = new SqlRowSet(r);
            while (qrs.next()) qrs.print();
            qrs.close();

            DbEngine<DbContext> engine = db.engine();

            rs = engine.countryRunwaysTypes(cxt);
            while (rs.next()) {
                System.out.println(Arrays.toString(converter.array(rs)));
            }
            rs.close();

            rs = engine.countryAirportsRank(cxt, 10, true);
            while (rs.next()) {
                CountryAirportsRank a = CountryAirportsRank
                    .kDefaultConverter.bean(rs, CountryAirportsRank.class);
                System.out.println(a);
            }
            rs.close();

            rs = engine.countryAirportsRank(cxt, 10, false);
            while (rs.next()) {
                CountryAirportsRank a = CountryAirportsRank
                    .kDefaultConverter.bean(rs, CountryAirportsRank.class);
                System.out.println(a);
            }
            rs.close();

            rs = engine.countryRunwaysRank(cxt, 10, true);
            while (rs.next()) {
                CountryRunwaysRank a = CountryRunwaysRank
                    .kDefaultConverter.bean(rs, CountryRunwaysRank.class);
                System.out.println(a);
            }
            rs.close();

            rs = engine.countryRunwaysRank(cxt, 10, false);
            while (rs.next()) {
                CountryRunwaysRank a = CountryRunwaysRank
                    .kDefaultConverter.bean(rs, CountryRunwaysRank.class);
                System.out.println(a);
            }
            rs.close();


        } catch (Exception e) {

            kLog.log(Level.SEVERE, "exception thrown in test", e);

        } finally {
            try {
                if (rs != null)
                    rs.close();
                if (cxt != null)
                    cxt.close();
            } catch (final Exception e) {
                kLog.log(Level.WARNING, "exception thrown while finishing db connection", e);
            }
        }
    }


    public void report() {

        DbInterface db;
        DbContext cxt = null;
        RowSet rs = null;

        BufferedOutputStream out = null;

        try {

            db = new SqlDbInterface(dbPath);
            cxt = db.context();

            RecordConverter converter = new RecordConverter();
            DbEngine<DbContext> engine = db.engine();

            out = logReport(null, "=====================================");
                  logReport(out,  "Countries with lowest airports count");
                  logReport(out,  "=====================================");

            rs = engine.countryAirportsRank(cxt, 10, true);
            while (rs.next()) {
                CountryAirportsRank a = CountryAirportsRank
                    .kDefaultConverter.bean(rs, CountryAirportsRank.class);

                logReport(out,
                    a.getCountryName() + ": " + a.getAirportCount() + " (" + a.getRank() + ")");
            }
            rs.close();

            logReport(out, "=====================================");
            logReport(out, "Countries with highest airports count");
            logReport(out, "=====================================");

            rs = engine.countryAirportsRank(cxt, 10, false);
            while (rs.next()) {
                CountryAirportsRank a = CountryAirportsRank
                    .kDefaultConverter.bean(rs, CountryAirportsRank.class);

                logReport(out,
                    a.getCountryName() + ": " + a.getAirportCount() + " (" + a.getRank() + ")");
            }
            rs.close();


            logReport(out, "=====================================");
            logReport(out, "Country runways types");
            logReport(out, "=====================================");

            rs = engine.countryRunwaysTypes(cxt);
            while (rs.next()) {
                logReport(out, Arrays.toString(converter.array(rs)));
            }
            rs.close();

            logReport(out, "=====================================");
            logReport(out, "Most common runways per country");
            logReport(out, "=====================================");

            rs = engine.countryRunwaysRank(cxt, 10, true);
            while (rs.next()) {
                CountryRunwaysRank a = CountryRunwaysRank
                    .kDefaultConverter.bean(rs, CountryRunwaysRank.class);
                logReport(out,
                    a.getCountryName() + ": " + a.getRunwayCount() + " (" + a.getRank() + ")");
            }
            rs.close();


        } catch (Exception e) {
            kLog.log(Level.SEVERE, "exception thrown in report()", e);
        } finally {

            try {
                if (rs != null)
                    cxt.close();

                if (cxt != null)
                    cxt.close();

                if (out != null)
                    out.close();

            } catch (Exception e) {
                kLog.log(Level.SEVERE, "exception thrown in report()", e);
            }
        }

    }


    public void lookup(String country) {

        DbInterface db;
        DbContext cxt = null;
        RowSet rs = null;

        BufferedOutputStream out = null;

        try {

            out = logCommand(null, "=====================================");
                  logCommand(out, "Country");
                  logCommand(out, "=====================================");

            db = new SqlDbInterface(dbPath);
            cxt = db.context();

            if (country.length() == 2) {

                rs = db.get(
                    cxt,
                    db.query()
                        .table("airport_runways")
                        .cells(
                            Arrays.asList(
                                Airport.C.name,
                                Airport.C.identity,
                                Country.C.code,
                                Country.C.continent,
                                Runway.C.id))
                        .orderBy(Airport.C.name)
                        .where(Collections.singletonList(Country.C.code.toUpperCase()))
                        .args(Collections.singletonList((country.toUpperCase())))
                );


            } else {

                rs = db.get(
                    cxt,
                    db.query()
                        .table("airport_runways")
                        .cells(
                            Arrays.asList(
                                Airport.C.name,
                                Airport.C.identity,
                                Country.C.code,
                                Country.C.continent,
                                Runway.C.id))
                        .orderBy(Airport.C.name)
                        .where(Collections.singletonList("upper(" + Country.C.name + ")"))
                        .args(Collections.singletonList(("%" + country + "%").toUpperCase()))
                        .whereRelations(Collections.singletonList(" LIKE "))
                );

            }

            while (rs.next()) {
                AirportRunways a = AirportRunways.kDefaultConverter.bean(rs, AirportRunways.class);
                Airport ap = a.getAirport();
                logCommand(out, ap.getName());
                logCommand(out, "\tIdentity: " + ap.getIdentity());
                logCommand(out, "\tCountry: " + ap.getCountry());
                logCommand(out, "\tContinent: " + ap.getContinent());
                logCommand(out, "\tRunways: " + a.getRunways().size());
            }
            rs.close();

        } catch (Exception e) {

            kLog.log(Level.SEVERE, "exception thrown in lookup()", e);

        } finally {

            try {
                if (rs != null)
                    cxt.close();

                if (cxt != null)
                    cxt.close();

                if (out != null)
                    out.close();
            } catch (Exception e) {
                kLog.log(Level.SEVERE, "exception thrown in lookup()", e);
            }

        }
    }
}
