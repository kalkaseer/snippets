/**
 * Copyright 2016 Kareem Alkaseer.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.example;

import java.sql.SQLXML;
import java.sql.Timestamp;
import java.util.Iterator;
import java.util.Map;

public interface RowSet extends AutoCloseable {

    int columnCount();

    String columnName(int i);

    String columnLabel(int i);

    String getString(int i) throws RuntimeException;

    String getString(String column) throws RuntimeException;

    Boolean getBoolean(int i) throws RuntimeException;

    Boolean getBoolean(String column) throws RuntimeException;

    Byte getByte(int i);

    Byte getByte(String column);

    Short getShort(int i);

    Short getShort(String column);

    Integer getInt(int i);

    Integer getInt(String column);

    Long getLong(int i);

    Long getLong(String column);

    Float getFloat(int i);

    Float getFloat(String column);

    Double getDouble(int i);

    Double getDouble(String column);

    Timestamp getTimestamp(int i);

    Timestamp getTimestamp(String column);

    SQLXML getXml(int i);

    SQLXML getXml(String column);

    Object getObject(int i);

    Object getObject(String column);

    boolean next();

    boolean previous();

    Object[] array();

    int rowSize();
}
