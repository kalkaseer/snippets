/**
 * Copyright 2016 Kareem Alkaseer.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.example;

import com.google.common.base.Splitter;

import java.sql.Time;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;
import java.util.regex.Pattern;

public class Utils {

    public static final Splitter kCsvSplitter =
        Splitter.on(Pattern.compile(",(?=([^\"]*\"[^\"]*\")*[^\"]*$)"));

    public static Boolean getBoolean(Object o) throws ParseException {

        if (o == null) return null;
        if (o.getClass().equals(Boolean.TYPE) || o instanceof Boolean)
            return (Boolean) o;
        else if (o instanceof String) {
            final String s = (String) o;
            if (s.equalsIgnoreCase("no") || s.equalsIgnoreCase("false") || s.equals("0")) return false;
            if (s.equalsIgnoreCase("yes") || s.equalsIgnoreCase("true") || s.equals("1")) return true;
            throw new ParseException("cannot parse boolean: " + s, 0);
        } else if (o instanceof Number) {
            final Number n = (Number) o;
            int i = n.intValue();
            if (i == 0) return false;
            if (i == 1) return true;
            throw new ParseException("cannot parse boolean: " + i, 0);
        }

        throw new ParseException("cannot parse boolean: unsupported type", 0);
    }

    public static void rethrow(Exception e) {
        if (e instanceof RuntimeException)
            throw (RuntimeException) e;
        throw new RuntimeException(e);
    }

    public static Object parseFromString(String o, DataType type) {

        switch (type) {
            case VARCHAR:
                return o;
            case INT:
                if (o.length() == 0) return null;
                return Integer.valueOf(o);
            case LONG:
                if (o.length() == 0) return null;
                return Long.valueOf(o);
            case SHORT:
                if (o.length() == 0) return null;
                return Short.valueOf(o);
            case BOOL:
                if (o.length() == 0) return null;
                try {
                    return getBoolean(o);
                } catch (ParseException e) {
                    rethrow(e);
                }
            case DOUBLE:
                if (o.length() == 0) return null;
                return Double.valueOf(o);
            case REAL:
            case FLOAT:
                if (o.length() == 0) return null;
                return Float.valueOf(o);
        }

        throw new IllegalArgumentException("unsupported type: " + type);
    }

    public static Object parseFromInteger(Integer o, DataType type) {
        switch (type) {
            case VARCHAR:
                return String.valueOf(o);
            case INT:
                return o;
            case LONG:
                return o.longValue();
            case SHORT:
                return o.shortValue();
            case BOOL:
                return o == 1;
            case DOUBLE:
                return o.doubleValue();
            case REAL:
            case FLOAT:
                return o.floatValue();
        }

        throw new IllegalArgumentException("unsupported type: " + type);
    }

    public static Object parseFromLong(Long o, DataType type) {
        switch (type) {
            case VARCHAR:
                return String.valueOf(o);
            case INT:
                return o.intValue();
            case LONG:
                return o;
            case SHORT:
                return o.shortValue();
            case BOOL:
                return o == 1;
            case DOUBLE:
                return o.doubleValue();
            case REAL:
            case FLOAT:
                return o.floatValue();
        }

        throw new IllegalArgumentException("unsupported type: " + type);
    }

    public static Object parseFromShort(Short o, DataType type) {
        switch (type) {
            case VARCHAR:
                return String.valueOf(o);
            case INT:
                return o.intValue();
            case LONG:
                return o.longValue();
            case SHORT:
                return o;
            case BOOL:
                return o == 1;
            case DOUBLE:
                return o.doubleValue();
            case REAL:
            case FLOAT:
                return o.floatValue();
        }

        throw new IllegalArgumentException("unsupported type: " + type);
    }

    public static Object parseFromDouble(Number o, DataType type) {
        switch (type) {
            case DOUBLE:
                return o.doubleValue();
            case VARCHAR:
                return String.valueOf(o);
            case INT:
                return o.intValue();
            case LONG:
                return o.longValue();
            case SHORT:
                return o.shortValue();
            case BOOL:
                return o.intValue() == 1;
            case REAL:
            case FLOAT:
                return o.floatValue();
        }

        throw new IllegalArgumentException("unsupported type: " + type);
    }

    public static Object parseFromFloat(Float o, DataType type) {
        switch (type) {
            case REAL:
            case FLOAT:
                return o;
            case VARCHAR:
                return String.valueOf(o);
            case INT:
                return o.intValue();
            case LONG:
                return o.longValue();
            case SHORT:
                return o.shortValue();
            case BOOL:
                return o == 1;
            case DOUBLE:
                return o.doubleValue();
        }

        throw new IllegalArgumentException("unsupported type: " + type);
    }

    public static Object parseFromBoolean(Boolean o, DataType type) {
        switch (type) {
            case VARCHAR:
                return o ? "true" : "false";
            case INT:
                return o ? 1 : 0;
            case LONG:
                return o ? 1L : 0L;
            case SHORT:
                return o ? (short) 1 : (short) 0;
            case DOUBLE:
                return o ? 1d : 0d;
            case REAL:
            case FLOAT:
                return o ? 1f : 0f;
            case BOOL:
                return o;
        }

        throw new IllegalArgumentException("unsupported type: " + type);
    }

    public static Object parseFromTimestamp(Timestamp o, DataType type) {
        switch (type) {
            case DATE:
                return new java.sql.Date(o.getTime());
            case TIMESTAMP:
                return o;
            case TIME:
                return new Time(o.getTime());
            case LONG:
                return o.getTime();
        }

        throw new IllegalArgumentException("unsupported type: " + type);
    }

    public static Object parseFromDate(java.util.Date o, DataType type) {
        switch (type) {
            case DATE:
                return o;
            case TIMESTAMP: {
                if (o instanceof Timestamp) {
                    final Timestamp ts1 = (Timestamp) o;
                    final Timestamp ts2 = new Timestamp(ts1.getTime());
                    ts2.setNanos(ts1.getNanos());
                    return ts2;
                } else {
                    final Timestamp ts = new Timestamp(o.getTime());
                    ts.setNanos(0);
                    return ts;
                }
            }
            case TIME:
                return new java.sql.Time(o.getTime());
        }

        throw new IllegalArgumentException("unsupported type: " + type);
    }


    public static Object extractValue(Object o, DataType type) {

        if (o == null) {
            return null;
        }

        if (o instanceof String) {
            return parseFromString((String) o, type);
        }

        else if (o.getClass().equals(Integer.TYPE) || o instanceof Integer) {
            return parseFromInteger((Integer) o, type);
        }

        else if (o.getClass().equals(Long.TYPE) || o instanceof Long) {
            return parseFromLong((Long) o, type);
        }

        else if (o.getClass().equals(Short.TYPE) || o instanceof Short) {
            return parseFromShort((Short) o, type);
        }

        else if (o.getClass().equals(Double.TYPE) || o instanceof Double) {
            return parseFromDouble((Double) o, type);
        }

        else if (o.getClass().equals(Float.TYPE) || o instanceof Float) {
            return parseFromFloat((Float) o, type);
        }

        else if (o.getClass().equals(Boolean.TYPE) || o instanceof Boolean) {
            return parseFromBoolean((Boolean) o, type);
        }

        else if (o instanceof Timestamp) {
            return parseFromTimestamp((Timestamp) o, type);
        }

        else if (o instanceof Time) {
            if (type == DataType.TIME)
                return o;
        }

        else if (o instanceof java.util.Date) {
            return parseFromDate((java.util.Date) o, type);
        }

        throw new IllegalArgumentException("unsupported type: " + type);
    }


    public static List<String> split(String s) {
        ArrayList<String> values = new ArrayList<>();
        Iterable<String> it = kCsvSplitter.split(s);
        it.forEach(values::add);
        return values;
    }

    public static final Function<String, List<String>> keywordsSplitter =
        s -> {
            if (s != null && s.length() != 0)
                return Utils.split(s);
            return Collections.emptyList();
        };

    public static List<String> splitAndFix(String s) {

        List<String> values = split(s);

        for (int i = 0; i < values.size(); ++i) {
            final String v = values.get(i);
            if (v.startsWith("\"")) {
                if (v.length() > 1) {
                    if (v.endsWith("\"")) {
                        values.set(i, v.substring(1, v.length() - 1));
                    }
                } else {
                    values.set(i, "");
                }
            }
        }

        return values;
    }
}
