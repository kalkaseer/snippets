/**
 * Copyright 2016 Kareem Alkaseer.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.example;

import java.util.ArrayList;
import java.util.List;

public class BasicQuery implements Query {

    protected String table;
    protected List<String> cells;
    protected List<String> where;
    protected List<String> whereRelations;
    protected List<Object> args;
    protected List<Aggregate> aggregates;
    protected GroupingMode mode = GroupingMode.AND;
    protected String orderBy;
    protected String groupBy;
    protected int limit = -1;
    protected int offset = -1;

    @Override public String table() { return table; }
    @Override public Query table(String t) { table = t; return this; }

    @Override public List<String> where() { return where; }
    @Override public Query where(List<String> w) { where = w; return this; }
    @Override public boolean whereSet() { return where != null && where.size() != 0; }

    @Override public List<String> whereRelations() { return whereRelations; }
    @Override public Query whereRelations(List<String> r) {
        whereRelations = r; return this;
    }
    @Override public boolean whereRelationsSet() {
        return whereRelations != null && whereRelations.size() != 0;
    }

    @Override public List<Object> args() { return args; }
    @Override public Query args(List<Object> a) { args = a; return this; }

    @Override public List<String> cells() { return cells; }
    @Override public Query cells(List<String> c) { cells = c; return this; }
    @Override public boolean cellsSet() { return cells != null && cells.size() != 0; }

    @Override public GroupingMode mode() { return mode; }
    @Override public Query mode(GroupingMode m) { mode = m; return this; }

    @Override public String orderBy() { return orderBy; }
    @Override public Query orderBy(String o) { orderBy = o; return this; }
    @Override public Query orderBy(String fn, List<String> cells, boolean asc) {
        orderBy(new BasicAggregate(fn, cells, null).raw() + (asc ? " ASC" : " DESC"));
        return this;
    }
    @Override public boolean orderBySet() { return orderBy != null && orderBy.length() != 0; }

    @Override  public String groupBy() { return groupBy; }
    @Override public Query groupBy(String g) { groupBy = g; return this; }
    @Override public boolean groupBySet() { return groupBy != null && groupBy.length() != 0; }

    @Override public int limit() { return limit; }
    @Override public Query limit(int l) { limit = l; return this; }

    @Override public int offset() { return offset; }
    @Override public Query offset(int o) { offset = o; return this; }


    @Override public List<Aggregate> agg() { return aggregates; }
    @Override public Query agg(String fn, List<String> cells, String label) {
        if (aggregates == null)
            aggregates = new ArrayList<>();
        aggregates.add(new BasicAggregate(fn, cells, label)); return this;
    }
    @Override public boolean aggregatesSet() { return aggregates != null && aggregates.size() != 0; }


    public void tables(List<String> tables, boolean withLabels) {
        final StringBuilder b = new StringBuilder();
        if (withLabels) {
            b.append(tables.get(0)).append(" as ").append(tables.get(1));
            for (int i = 2; i < tables.size() / 2; i += 2)
                b.append(',').append(tables.get(i)).append(" as ").append(tables.get(i+1));
        } else {
            for (int i = 0; i < tables.size(); i += 2)
                b.append(',').append(tables.get(i));
        }
    }

}
