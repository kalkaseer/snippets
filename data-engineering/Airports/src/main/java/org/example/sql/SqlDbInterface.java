/**
 * Copyright 2016 Kareem Alkaseer.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.example.sql;

import java.io.*;
import java.sql.*;
import java.text.ParseException;
import java.util.*;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import org.example.*;
import org.example.beans.Airport;
import org.example.beans.Country;
import org.example.beans.Runway;

public class SqlDbInterface implements DbInterface {

    private static final Logger kLog = Logger.getLogger(SqlDbInterface.class.getName());

    private static final String kJdbcPrefix = "jdbc:sqlite:";

    private static final String kJdbcDriverName = "org.sqlite.JDBC";


    static {
        try {
            Class.forName(kJdbcDriverName);
        } catch (Exception e) {
            kLog.log(Level.SEVERE, "Cannot load JDBC driver", e);
            System.exit(0);
            Utils.rethrow(e);
        }
    }


    private String dbPath;

    private String dataPath;

    private LinkedHashMap<String, Definition> definitions;

    private LinkedHashMap<String, String> viewDefinitions;

    private List<String> indexDefinitions;

    private boolean ignoreFirstLineInCvs = true;


    public SqlDbInterface(String dbPath, String dataPath, boolean ignoreFirstLine)
        throws Exception
    {
        this.dbPath = dbPath;
        this.dataPath = dataPath;
        this.ignoreFirstLineInCvs = ignoreFirstLine;
        init();
    }

    public SqlDbInterface(String dbPath, String dataPath)
        throws Exception
    {
        this.dbPath = dbPath;
        this.dataPath = dataPath;
        init();
    }

    public SqlDbInterface(String dbPath)
        throws Exception
    {
        this.dbPath = dbPath;
        init();
    }

    @Override
    public DbContext context(String... params) {
        try {
            return new SqlContext(
                DriverManager.getConnection(kJdbcPrefix + dbPath));
        } catch (Exception e) {
            Utils.rethrow(e);
            return null;
        }
    }

    public <T> void execute(Consumer<? super T> c, T t) {
        try {
            c.accept(t);
        } catch (Exception e) {
            Utils.rethrow(e);
        }
    }

    private void init() throws SQLException, IOException, ParseException {

        Connection conn = null;
        Statement stmt = null;
        ResultSet rs;

        makeDefs();

        try {

            conn = DriverManager.getConnection(kJdbcPrefix + dbPath);

            conn.setAutoCommit(false);

            stmt = conn.createStatement();

            DatabaseMetaData dbmeta = conn.getMetaData();

            for (final Map.Entry<String, Definition> e : definitions.entrySet()) {

                final Definition def = e.getValue();

                rs = dbmeta.getTables(null, null, e.getKey().toUpperCase(), null);
                if (! rs.next()) {
                    def.create(stmt);
                    if (dataPath != null)
                        insertCvs(def.tableName, conn, true);
                }
                rs.close();
            }

            for (final Map.Entry<String, String> e : viewDefinitions.entrySet()) {

                rs = dbmeta.getTables(null, null, e.getKey().toUpperCase(), null);
                if (! rs.next()) {
                    stmt.executeUpdate(e.getValue());
                }
                rs.close();
            }

            for (final String s : indexDefinitions)
                stmt.executeUpdate(s);

            conn.commit();


        } catch (final SQLException e) {

            if (conn == null) {
                kLog.log(Level.SEVERE, "Could not connect to the SQLite", e);
                System.exit(0);
            }

            try {
                kLog.log(Level.SEVERE, "sever database error", e);
                conn.rollback();
                System.exit(0);
            } catch (SQLException e2) {
                kLog.log(Level.SEVERE,
                    "sever database error: DATABASE MIGHT NOT BE IN A CONSISTENT STATE", e);
                System.exit(0);
            }

        } finally {

            boolean setMode = false;

            try {

                if (stmt != null) {
                    stmt.close();
                }

                if (conn != null) {
                    conn.setAutoCommit(true);
                    conn.close();
                }

            } catch (final SQLException e) {
                if (! setMode) {
                    kLog.log(Level.WARNING, "could not close statement", e);
                } else {
                    kLog.log(Level.SEVERE, "could not set commit mode", e);
                    System.exit(0);
                }
            }
        }

    }

    private void makeDefs() {

        definitions = new LinkedHashMap<>();
        viewDefinitions = new LinkedHashMap<>();

        ImmutableList<ColumnConstraint> primaryKeyConstraint =
                ImmutableList.of(ColumnConstraint.PRIMARY_KEY);

        ImmutableList<ColumnConstraint> notNullConstraint =
                ImmutableList.of(ColumnConstraint.NOT_NULL);

        ImmutableList<Column> columns = new ImmutableList.Builder<Column>()
            .add(new Column(Country.C.id, DataType.INT, primaryKeyConstraint))
            .add(new Column(Country.C.code, DataType.VARCHAR, notNullConstraint))
            .add(new Column(Country.C.name, DataType.VARCHAR, notNullConstraint))
            .add(new Column(Country.C.continent, DataType.VARCHAR, notNullConstraint))
            .add(new Column(Country.C.wikiLink, DataType.VARCHAR, "", notNullConstraint))
            .add(new Column(Country.C.keywords, DataType.VARCHAR, "", notNullConstraint))
            .build();

        Definition countriesDef = new Definition(
            "countries", columns, new String[] {
            "CHECK(" +
                Country.C.code + " <> '' and " +
                Country.C.name + " <> '' and " +
                Country.C.continent + " <> '')"});

        columns = ImmutableList.of(
            new Column(Airport.C.id, DataType.INT, primaryKeyConstraint),
            new Column(Airport.C.identity, DataType.VARCHAR, notNullConstraint),
            new Column(Airport.C.type, DataType.VARCHAR, notNullConstraint),
            new Column(Airport.C.name, DataType.VARCHAR, notNullConstraint),
            new Column(Airport.C.latitude, DataType.DOUBLE, notNullConstraint),
            new Column(Airport.C.longitude, DataType.DOUBLE, notNullConstraint),
            new Column(Airport.C.elevation, DataType.INT, null),
            new Column(Airport.C.continent, DataType.VARCHAR, "", notNullConstraint),
            new Column(Airport.C.country, DataType.VARCHAR, notNullConstraint),
            new Column(Airport.C.region, DataType.VARCHAR, notNullConstraint),
            new Column(Airport.C.municipality, DataType.VARCHAR, notNullConstraint),
            new Column(Airport.C.serviceScheduled, DataType.BOOL, notNullConstraint),
            new Column(Airport.C.gpsCode, DataType.VARCHAR, "", notNullConstraint),
            new Column(Airport.C.iata, DataType.VARCHAR, "", notNullConstraint),
            new Column(Airport.C.localCode, DataType.VARCHAR, notNullConstraint),
            new Column(Airport.C.homeLink, DataType.VARCHAR, "", notNullConstraint),
            new Column(Airport.C.wikiLink, DataType.VARCHAR, "", notNullConstraint),
            new Column(Airport.C.keywords, DataType.VARCHAR, "", notNullConstraint)
        );

        Definition airportsDef = new Definition(
            "airports", columns, new String[] {
            "CHECK(" + Airport.C.identity + " <> '' AND " +
                Airport.C.type + " <> '' AND " +
                Airport.C.name + " <> '')",
            "FOREIGN KEY(" + Airport.C.country + ") REFERENCES countries(" + Country.C.code + ")"
        });

        columns = ImmutableList.of(
            new Column(Runway.C.id, DataType.INT, primaryKeyConstraint),
            new Column(Runway.C.airportRef, DataType.INT, notNullConstraint),
            new Column(Runway.C.airportIdentity, DataType.VARCHAR, notNullConstraint),
            new Column(Runway.C.length, DataType.INT, null),
            new Column(Runway.C.width, DataType.INT, null),
            new Column(Runway.C.surface, DataType.VARCHAR, "", notNullConstraint),
            new Column(Runway.C.lighted, DataType.BOOL, null),
            new Column(Runway.C.closed, DataType.BOOL, null),
            new Column(Runway.C.leIdentity, DataType.VARCHAR, "", notNullConstraint),
            new Column(Runway.C.leLatitude, DataType.DOUBLE, null),
            new Column(Runway.C.leLongitude, DataType.DOUBLE, null),
            new Column(Runway.C.leElevation, DataType.INT, null),
            new Column(Runway.C.leHeadingDegree, DataType.REAL, null),
            new Column(Runway.C.leDisplacedThreshold, DataType.INT, null),
            new Column(Runway.C.heIdentity, DataType.VARCHAR, "", null),
            new Column(Runway.C.heLatitude, DataType.DOUBLE, null),
            new Column(Runway.C.heLongitude, DataType.DOUBLE, null),
            new Column(Runway.C.heElevation, DataType.INT, null),
            new Column(Runway.C.heHeadingDegree, DataType.REAL, null),
            new Column(Runway.C.heDisplacedThreshold, DataType.INT, null)
        );

        Definition runwaysDef = new Definition(
            "runways", columns, new String[] {
            "FOREIGN KEY(" + Runway.C.airportRef + ") REFERENCES airports(" + Airport.C.id + ")",
            "FOREIGN KEY(" + Runway.C.airportIdentity + ") REFERENCES airports(" + Airport.C.identity + ")"
        });


        definitions.put(countriesDef.tableName, countriesDef);
        definitions.put(airportsDef.tableName, airportsDef);
        definitions.put(runwaysDef.tableName, runwaysDef);

        final StringBuilder b = new StringBuilder();
        b.append("CREATE VIEW country_airports AS ")
            .append("SELECT ")
            .append(Country.C.id).append(',')
            .append(Country.C.code).append(',')
            .append(Country.C.name).append(',')
            .append(Country.C.continent).append(',')
            .append(Country.C.wikiLink).append(',')
            .append(Country.C.keywords).append(',')
            .append(Airport.C.id).append(',')
            .append(Airport.C.identity).append(',')
            .append(Airport.C.type).append(',')
            .append(Airport.C.name).append(',')
            .append(Airport.C.latitude).append(',')
            .append(Airport.C.longitude).append(',')
            .append(Airport.C.elevation).append(',')
            .append(Airport.C.region).append(',')
            .append(Airport.C.municipality).append(',')
            .append(Airport.C.serviceScheduled).append(',')
            .append(Airport.C.gpsCode).append(',')
            .append(Airport.C.iata).append(',')
            .append(Airport.C.localCode).append(',')
            .append(Airport.C.homeLink).append(',')
            .append(Airport.C.wikiLink).append(',')
            .append(Airport.C.keywords).append(' ')
            .append("FROM countries AS c LEFT JOIN airports AS a ON c.")
            .append(Country.C.code).append("=a.").append(Airport.C.country);

        viewDefinitions.put("country_airports", b.toString());

        b.setLength(0);
        b.append("CREATE VIEW airport_runways AS ")
            .append("SELECT ca.*,")
            .append(Runway.C.id).append(',')
            .append(Runway.C.length).append(',')
            .append(Runway.C.width).append(',')
            .append(Runway.C.surface).append(',')
            .append(Runway.C.lighted).append(',')
            .append(Runway.C.closed).append(',')
            .append(Runway.C.leIdentity).append(',')
            .append(Runway.C.leLatitude).append(',')
            .append(Runway.C.leLongitude).append(',')
            .append(Runway.C.leElevation).append(',')
            .append(Runway.C.leHeadingDegree).append(',')
            .append(Runway.C.leDisplacedThreshold).append(',')
            .append(Runway.C.heIdentity).append(',')
            .append(Runway.C.heLatitude).append(',')
            .append(Runway.C.heLongitude).append(',')
            .append(Runway.C.heElevation).append(',')
            .append(Runway.C.heHeadingDegree).append(',')
            .append(Runway.C.heDisplacedThreshold).append(' ')
            .append("FROM country_airports as ca LEFT JOIN runways as r ON ")
            .append("ca.").append(Airport.C.id).append("=r.").append(Runway.C.airportRef);

        viewDefinitions.put("airport_runways", b.toString());

        viewDefinitions.put(
            "country_airports_ranks",
            "create view country_airports_ranks as " +
            "  with c1 as ( " +
            "  select " + Country.C.id + "," + Country.C.code + "," + Country.C.name + "," +
            "  count(" + Airport.C.id + ") as " + Airport.C.count +
            "  from country_airports " +
            "    group by " + Country.C.code +
            "    order by " + Airport.C.count + " desc" +
            "  )" +
            "  select c1." + Country.C.id + "," +
            "         c1." + Country.C.code + "," +
            "         c1." + Country.C.name + "," +
            "         c1." + Airport.C.count + "," +
            "         count(c2." + Airport.C.count + ") as " + Airport.C.rank +
            "  from c1, c1 as c2 " +
            "  where c1." + Airport.C.count + " < c2." + Airport.C.count + " or " +
            "        (c1." + Airport.C.count + " = c2." + Airport.C.count + " and " +
            "         c1." + Country.C.code + " = c2." + Country.C.code + ") " +
            "  group by c1." + Country.C.code + ", c1." + Airport.C.count +
            "  order by c1." + Airport.C.count + " desc, c1." + Country.C.code + " desc");

        viewDefinitions.put(
            "country_runways",
            "create view country_runways as" +
            "  select c." + Country.C.code + ", c." + Country.C.name + ", r." + Runway.C.leIdentity +
            "  from countries as c left join airports as a" +
            "  on c." + Country.C.code + " = a." + Airport.C.country +
            "  left join runways as r" +
            "  on a." + Airport.C.identity + " = r." + Runway.C.airportIdentity);


        viewDefinitions.put(
            "country_runways_ranks",
            "create view country_runways_ranks as" +
            "  with c1 as (       " +
            "  select " + Country.C.code + ", " + Country.C.name + ", " +  Runway.C.leIdentity + "," +
            "  count(" + Runway.C.leIdentity + ") as " + Runway.C.count +
            "  from country_runways" +
            "    where " + Runway.C.leIdentity + " <> ''" +
            "    group by " + Country.C.code + ", " + Runway.C.leIdentity +
            "    order by " + Runway.C.count + " desc, " + Country.C.code + " desc" +
            "  )" +
            "  select c1." + Country.C.code + ", c1." + Country.C.name + ","  +
            "         c1." + Runway.C.leIdentity + "," +
            "         c1." + Runway.C.count + "," +
            "         count(c2." + Runway.C.count + ") as " + Runway.C.rank +
            "  from c1, c1 as c2" +
            "  where c1." + Runway.C.count + " < c2." + Runway.C.count + " or" +
            "    (c1." + Runway.C.count + " = c2." + Runway.C.count +
            "     and c1." + Country.C.code + " = c2." + Country.C.code + ")" +
            "  group by c1." + Country.C.code + ", c1." + Runway.C.count +
            "  order by c1." + Runway.C.count + " desc, c1." + Country.C.code + " desc");

        indexDefinitions = Arrays.asList(
            "create unique index if not exists countries_code_uidx on countries(" + Country.C.code + ")",
            "create unique index if not exists airports_ident_uidx on airports("  + Airport.C.identity +  ")",
            "create index if not exists runways_le_ident_idx on runways("  + Runway.C.leIdentity +  ")"
        );

    }


    private void insertCvs(String table, Connection conn, boolean relaxed)
        throws SQLException, ParseException, IOException
    {

        BufferedReader br = null;

        String line;

        Definition def = definitions.get(table);

        PreparedStatement stmt = def.prepareInsert(conn);

        try {
            br = new BufferedReader(new InputStreamReader(
                new FileInputStream(dataPath + "/" + table + ".csv"), "UTF8"));
            if (ignoreFirstLineInCvs) {
                br.readLine();
            }

            while ((line = br.readLine()) != null) {

                try {
                    final List<String> values = Utils.splitAndFix(line);
                    def.insert(stmt, values);
                } catch (final ParseException e) {
                    if (relaxed) continue;
                    throw e;
                }
            }
        } finally {
            if (br != null)
                try {
                    br.close();
                } catch (final IOException e) {
                    kLog.log(Level.WARNING, "Could not close file stream", e);
                }
        }
    }


    private enum ColumnConstraint {

        NOT_NULL("NOT NULL"),
        PRIMARY_KEY("PRIMARY KEY");

        ColumnConstraint(String s) {
            string = s;
        }

        public final String string;
    }

    private static class Column {

        public final String name;
        public final DataType type;
        public final boolean nullable;
        public final Object defaultValue;
        public final ImmutableList<ColumnConstraint> constraints;

        Column(String name, DataType type, ImmutableList<ColumnConstraint> constraints) {
            this(name, type, null, constraints);
        }

        Column(
            String name, DataType type, Object defaultValue,
            ImmutableList<ColumnConstraint> constraints
        ) {
            this.name = name;
            this.type = type;
            this.constraints = constraints;

            if (constraints != null) {
                boolean isNullable = true;
                for (ColumnConstraint c : constraints) {
                    if (c == ColumnConstraint.NOT_NULL) {
                        isNullable = false;
                        break;
                    }
                }
                nullable = isNullable;
            } else {
                nullable = true;
            }

            this.defaultValue = defaultValue;
        }

        @Override
        public String toString() {

            final StringBuilder b = new StringBuilder(name).append(' ').append(type.string);

            if (constraints != null && constraints.size() != 0) {
                b.append(' ').append(constraints.get(0).string);
                for (int i = 1; i < constraints.size(); ++i)
                    b.append(' ').append(constraints.get(i));
            }

            return b.toString();
        }

    }

    private static class Definition {

        private String createString;
        private String insertString;
        private LinkedHashMap<String, Column> columns;
        public final String tableName;


        public Definition(String tableName, ImmutableList<Column> columns, String [] constraints) {

            this.tableName = tableName;
            this.columns = new LinkedHashMap<>();
            init(columns, constraints);
        }

        private void init(ImmutableList<Column> columns, String [] constraints) {

            final StringBuilder b1 =
                new StringBuilder("CREATE TABLE ").append(tableName).append('(');

            final StringBuilder b2 =
                new StringBuilder("INSERT INTO ").append(tableName).append('(');

            this.columns.put(columns.get(0).name, columns.get(0));

            b1.append(columns.get(0).toString());

            b2.append(columns.get(0).name);

            for (int i = 1; i < columns.size(); ++i) {
                final Column c = columns.get(i);
                this.columns.put(c.name, c);
                b1.append(',').append(c.toString());
                b2.append(',').append(c.name);

            }

            if (constraints != null && constraints.length != 0) {
                b1.append(',').append(constraints[0]);
                for (int i = 1; i < constraints.length; ++i)
                    b1.append(',').append(constraints[i]);
            }
            b1.append(')');

            createString = b1.toString();

            b2.append(") VALUES(?");
            for (int i = 1; i < columns.size(); ++i)
                b2.append(",?");
            b2.append(')');

            insertString = b2.toString();

        }

        public void create(Statement stmt) throws SQLException {
            stmt.execute(createString);
        }

        public PreparedStatement prepareInsert(Connection conn) throws SQLException {
            return conn.prepareStatement(insertString);
        }

        public void insert(PreparedStatement stmt, List<?> values)
            throws SQLException, ParseException
        {

            stmt.clearParameters();
            int i = 0;

            for (final Column c : columns.values()) {

                if (i >= values.size()) {
                    handleMissingValuesFromRow(c, stmt, i);
                    ++i;
                    continue;
                }

                if (values.get(i) == null) {
                    stmt.setNull(i+1, c.type.sqlType);
                } else {
                    kColumnSetterMap.get(c.type).set(values.get(i), stmt, i);
                }

                i++;
            }

            stmt.executeUpdate();
        }

        private void handleMissingValuesFromRow(Column c, PreparedStatement stmt, int i) throws SQLException{

            if (c.nullable)
                stmt.setNull(i+1, c.type.sqlType);
            else if (c.defaultValue != null) {
                kColumnSetterMap.get(c.type).set(c.defaultValue, stmt, i);
            }
        }

    }


    @FunctionalInterface
    private static interface ColumnSetter {
        void set(Object value, PreparedStatement stmt, int i);
    }

    private static final ColumnSetter kFloatColumnSetter = (value, stmt, i) -> {

        Object o = Utils.extractValue(value, DataType.FLOAT);
        try {
            if (o != null) {
                stmt.setFloat(i + 1, (Float) o);
            } else {
                stmt.setNull(i + 1, DataType.FLOAT.sqlType);
            }
        } catch (final Exception e) {
            Utils.rethrow(e);
        }
    };

    private final static Map<DataType, ColumnSetter> kColumnSetterMap =
        new ImmutableMap.Builder<DataType, ColumnSetter>()

        .put(DataType.INT, (value, stmt, i) -> {

            Object o = Utils.extractValue(value, DataType.INT);
            try {
                if (o != null) {
                    stmt.setInt(i + 1, (Integer) o);
                } else {
                    stmt.setNull(i + 1, DataType.INT.sqlType);
                }
            } catch (final Exception e) {
                Utils.rethrow(e);
            }

        }).put(DataType.LONG, (value, stmt, i) -> {

        Object o = Utils.extractValue(value, DataType.LONG);
        try {
            if (o != null) {
                stmt.setLong(i + 1, (Long) o);
            } else {
                stmt.setNull(i + 1, DataType.LONG.sqlType);
            }
        } catch (final Exception e) {
            Utils.rethrow(e);
        }

    }).put(DataType.SHORT, (value, stmt, i) -> {

        Object o = Utils.extractValue(value, DataType.SHORT);
        try {
            if (o != null) {
                stmt.setShort(i + 1, (Short) o);
            } else {
                stmt.setNull(i + 1, DataType.SHORT.sqlType);
            }
        } catch (final Exception e) {
            Utils.rethrow(e);
        }

    }).put(DataType.REAL, kFloatColumnSetter).put(DataType.FLOAT, kFloatColumnSetter)

    .put(DataType.DOUBLE, (value, stmt, i) -> {

        Object o = Utils.extractValue(value, DataType.DOUBLE);
        try {
            if (o != null) {
                stmt.setDouble(i + 1, (Double) o);
            } else {
                stmt.setNull(i + 1, DataType.DOUBLE.sqlType);
            }
        } catch (final Exception e) {
            Utils.rethrow(e);
        }

    }).put(DataType.VARCHAR, (value, stmt, i) -> {

        Object o = Utils.extractValue(value, DataType.VARCHAR);
        try {
            if (o != null) {
                stmt.setString(i + 1, (String) o);
            } else {
                stmt.setNull(i + 1, DataType.VARCHAR.sqlType);
            }
        } catch (final Exception e) {
            Utils.rethrow(e);
        }

    }).put(DataType.BOOL, (value, stmt, i) -> {

        Object o = Utils.extractValue(value, DataType.BOOL);
        try {
            if (o != null) {
                stmt.setBoolean(i + 1, (Boolean) o);
            } else {
                stmt.setNull(i + 1, DataType.BOOL.sqlType);
            }
        } catch (final Exception e) {
            Utils.rethrow(e);
        }

    }).put(DataType.CHAR, (value, stmt, i) -> {

        Object o = Utils.extractValue(value, DataType.CHAR);
        try {
            if (o != null) {
                stmt.setInt(i + 1, (int) o);
            } else {
                stmt.setNull(i + 1, DataType.CHAR.sqlType);
            }
        } catch (final Exception e) {
            Utils.rethrow(e);
        }

    }).put(DataType.DATE, (value, stmt, i) -> {

        Object o = Utils.extractValue(value, DataType.DATE);
        try {
            if (o != null) {
                stmt.setDate(i + 1, (java.sql.Date) o);
            } else {
                stmt.setNull(i + 1, DataType.DATE.sqlType);
            }
        } catch (final Exception e) {
            Utils.rethrow(e);
        }

    }).put(DataType.TIME, (value, stmt, i) -> {

        Object o = Utils.extractValue(value, DataType.TIME);
        try {
            if (o != null) {
                stmt.setTime(i + 1, (java.sql.Time) o);
            } else {
                stmt.setNull(i + 1, DataType.TIME.sqlType);
            }
        } catch (final Exception e) {
            Utils.rethrow(e);
        }

    }).put(DataType.TIMESTAMP, (value, stmt, i) -> {

        Object o = Utils.extractValue(value, DataType.TIMESTAMP);
        try {
            if (o != null) {
                stmt.setTimestamp(i + 1, (java.sql.Timestamp) o);
            } else {
                stmt.setNull(i + 1, DataType.TIMESTAMP.sqlType);
            }
        } catch (final Exception e) {
            Utils.rethrow(e);
        }
    })

    .build();


    private String construct(Query q) {

        final StringBuilder b = new StringBuilder("SELECT ");

        List<String> slist = q.cells();

        if (q.cellsSet()) {
            b.append(slist.get(0));
            for (int i = 1; i < slist.size(); ++i)
                b.append(',').append(slist.get(i));
        } else {
            b.append('*');
        }

        if (q.aggregatesSet()) {
            b.append(',');
            final List<Aggregate> agg = q.agg();
            b.append(agg.get(0).raw());
            for (int i = 1; i < agg.size(); ++i)
                b.append(',').append(agg.get(i).raw());
        }

        b.append(" FROM ").append(q.table());

        slist = q.where();

        if (q.whereSet()) {

            b.append(" WHERE ");

            if (!q.whereRelationsSet()) {
                String grouping =
                    q.mode() == null || q.mode() == GroupingMode.AND ? " AND " : " OR ";
                b.append(slist.get(0)).append("=?");
                for (int i = 1; i < slist.size(); ++i)
                    b.append(grouping).append(slist.get(i)).append("=?");
            } else {
                List<String> rels = q.whereRelations();
                String grouping =
                    q.mode() == null || q.mode() == GroupingMode.AND ? " AND " : " OR ";
                b.append(slist.get(0)).append(' ').append(rels.get(0)).append(" ? ");
                for (int i = 1; i < slist.size(); ++i)
                    b.append(grouping).append(rels.get(i)).append(" ? ");
            }
        }

        if (q.groupBySet())
            b.append(" GROUP BY ").append(q.groupBy());

        if (q.orderBySet())
            b.append(" ORDER BY ").append(q.orderBy());

        if (q.limit() > 0)
            b.append(" LIMIT ?");

        if (q.offset() > -1)
            b.append(" OFFSET ?");

//        System.out.println(b.toString()); // TODO

        return b.toString();
    }

    @Override
    public <C extends DbContext> RowSet get(C cxt, Query q) throws Exception {

        assert(cxt instanceof SqlContext);
        assert((q.args() == null && q.where() == null) || (q.where().size() == q.args().size()));
        assert(!q.whereRelationsSet() || q.whereRelations().size() == q.args().size());

        String stmtString = construct(q);
        PreparedStatement stmt = ((SqlContext) cxt).context().prepareStatement(stmtString);
        final List<Object> args = q.args();
        if (args != null) {
            for (int i = 0; i < args.size(); ++i)
                kColumnSetterMap.get(DataType.of(args.get(i))).set(args.get(i), stmt, i);
        }

        int i = args != null ? args.size() : 0;

        if (q.limit() > 0) {
            stmt.setInt(i + 1, q.limit());
            ++i;
        }

        if (q.offset() > -1) {
            stmt.setInt(i + 1, q.offset());
        }

        return new SqlRowSet(stmt.executeQuery());
    }

    @Override
    public Query query() {
        return new BasicQuery();
    }

    @SuppressWarnings("unchecked")
    @Override
    public SqlEngine engine() { return new SqlEngine(); }
}
