/**
 * Copyright 2016 Kareem Alkaseer.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.example.sql;

import org.example.DbContext;
import org.example.Utils;

import java.sql.Connection;
import java.sql.SQLException;

public class SqlContext implements DbContext {

    private final Connection conn;

    public SqlContext(Connection conn) {
        this.conn = conn;
    }

    @Override
    public Connection context() { return conn; }

    @Override
    public void close() {
        try {
            conn.close();
        } catch (SQLException e) {
            Utils.rethrow(e);
        }
    }
}
