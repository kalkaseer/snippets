/**
 * Copyright 2016 Kareem Alkaseer.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.example.sql;

import org.example.DataType;
import org.example.RowSet;
import org.example.Utils;

import java.sql.*;
import java.util.NoSuchElementException;


public class SqlRowSet implements RowSet {

    private final ResultSet rs;

    private Object[] last;

    private boolean previousIsCurrent = false;

    private boolean started = false;

    public SqlRowSet(ResultSet rs) throws Exception {
        this.rs = rs;
    }

    @Override
    public Object[] array() {

        Object[] res = new Object[columnCount()];
        for (int i = 0; i < res.length; ++i) {
            res[i] = getObject(i + 1);
        }

        return res;
    }

    public boolean next() {

        if (previousIsCurrent) {
            previousIsCurrent = false;
            return true;
        }

        try {
            if (started) {
                boolean res = rs.next();
                if (res) {
                    last = array();
                }
                return res;
            } else {
                boolean res = rs.next();
                if (res) {
                    started = true;
                }
                return res;
            }
        } catch (final Exception e) {
            Utils.rethrow(e);
        }

        throw new NoSuchElementException();
    }

    public boolean previous() {
        if (previousIsCurrent)
            return false;
        previousIsCurrent = true;
        return true;
    }

    @Override
    public int columnCount() {
        try {
            return rs.getMetaData().getColumnCount();
        } catch (final SQLException e) {
            Utils.rethrow(e);
            return 0;
        }
    }

    @Override
    public String columnName(int i) {
        try {
            ResultSetMetaData m = rs.getMetaData();
            String cname = m.getColumnLabel(i);
            if (cname == null || cname.length() == 0)
                cname = m.getColumnName(i);
            return cname;
        } catch (final SQLException e) {
            Utils.rethrow(e);
            return null;
        }
    }

    @Override
    public String columnLabel(int i) {
        try {
            return rs.getMetaData().getColumnLabel(i);
        } catch (final SQLException e) {
            Utils.rethrow(e);
            return null;
        }
    }

    public ResultSetMetaData metadata() throws SQLException {
        return rs.getMetaData();
    }

    @Override
    public String getString(int i) {
        try {
            return (String) Utils.extractValue(rs.getObject(i), DataType.VARCHAR);
        } catch (final SQLException e) {
            Utils.rethrow(e);
            return null;
        }
    }

    @Override
    public String getString(String column) {
        try {
            return (String) Utils.extractValue(rs.getObject(column), DataType.VARCHAR);
        } catch (final SQLException e) {
            Utils.rethrow(e);
            return null;
        }
    }

    @Override
    public Boolean getBoolean(int i) {
        try {
            return (Boolean) Utils.extractValue(rs.getObject(i), DataType.BOOL);
        } catch (final Exception e) {
            Utils.rethrow(e);
            return null;
        }
    }

    @Override
    public Boolean getBoolean(String column) {
        try {
            return (Boolean) Utils.extractValue(rs.getObject(column), DataType.BOOL);
        } catch (final Exception e) {
            Utils.rethrow(e);
            return null;
        }
    }

    @Override
    public Byte getByte(int i) {
        try {
            final Integer o = (Integer) Utils.extractValue(rs.getObject(i), DataType.INT);
            if (o != null)
                return o.byteValue();
            return null;
        } catch (final SQLException e) {
            Utils.rethrow(e);
            return null;
        }
    }

    @Override
    public Byte getByte(String column) {
        try {
            final Integer o = (Integer) Utils.extractValue(rs.getObject(column), DataType.INT);
            if (o != null)
                return o.byteValue();
            return null;
        } catch (final SQLException e) {
            Utils.rethrow(e);
            return null;
        }
    }

    @Override
    public Short getShort(int i) {
        try {
            return (Short) Utils.extractValue(rs.getObject(i), DataType.SHORT);
        } catch (final SQLException e) {
            Utils.rethrow(e);
            return null;
        }
    }

    @Override
    public Short getShort(String column) {
        try {
            return (Short) Utils.extractValue(rs.getObject(column), DataType.SHORT);
        } catch (final SQLException e) {
            Utils.rethrow(e);
            return null;
        }
    }

    @Override
    public Integer getInt(int i) {
        try {
            return (Integer) Utils.extractValue(rs.getObject(i), DataType.INT);
        } catch (final SQLException e) {
            Utils.rethrow(e);
            return null;
        }
    }

    @Override
    public Integer getInt(String column) {
        try {
            return (Integer) Utils.extractValue(rs.getObject(column), DataType.INT);
        } catch (final SQLException e) {
            Utils.rethrow(e);
            return null;
        }
    }

    @Override
    public Long getLong(int i) {
        try {
            return (Long) Utils.extractValue(rs.getObject(i), DataType.LONG);
        } catch (final SQLException e) {
            Utils.rethrow(e);
            return null;
        }
    }

    @Override
    public Long getLong(String column) {
        try {
            return (Long) Utils.extractValue(rs.getObject(column), DataType.LONG);
        } catch (final SQLException e) {
            Utils.rethrow(e);
            return null;
        }
    }

    @Override
    public Float getFloat(int i) {
        try {
            return (Float) Utils.extractValue(rs.getObject(i), DataType.FLOAT);
        } catch (final SQLException e) {
            Utils.rethrow(e);
            return null;
        }
    }

    @Override
    public Float getFloat(String column) {
        try {
            return (Float) Utils.extractValue(rs.getObject(column), DataType.FLOAT);
        } catch (final SQLException e) {
            Utils.rethrow(e);
            return null;
        }
    }

    @Override
    public Double getDouble(int i) {
        try {
            return (Double) Utils.extractValue(rs.getObject(i), DataType.DOUBLE);
        } catch (final SQLException e) {
            Utils.rethrow(e);
            return null;
        }
    }

    @Override
    public Double getDouble(String column) {
        try {
            return (Double) Utils.extractValue(rs.getObject(column), DataType.DOUBLE);
        } catch (final SQLException e) {
            Utils.rethrow(e);
            return null;
        }
    }

    @Override
    public Timestamp getTimestamp(int i) {
        try {
            return (Timestamp) Utils.extractValue(rs.getObject(i), DataType.TIMESTAMP);
        } catch (final SQLException e) {
            Utils.rethrow(e);
            return null;
        }
    }

    @Override
    public Timestamp getTimestamp(String column) {
        try {
            return (Timestamp) Utils.extractValue(rs.getObject(column), DataType.TIMESTAMP);
        } catch (final SQLException e) {
            Utils.rethrow(e);
            return null;
        }
    }

    @Override
    public SQLXML getXml(int i) {
        try {
            return rs.getSQLXML(i);
        } catch (final SQLException e) {
            Utils.rethrow(e);
            return null;
        }
    }

    @Override
    public SQLXML getXml(String column) {
        try {
            return rs.getSQLXML(column);
        } catch (final SQLException e) {
            Utils.rethrow(e);
            return null;
        }
    }

    @Override
    public Object getObject(int i) {
        try {
            return rs.getObject(i);
        } catch (final SQLException e) {
            Utils.rethrow(e);
            return null;
        }
    }

    @Override
    public Object getObject(String column) {
        try {
            return rs.getObject(column);
        } catch (final SQLException e) {
            Utils.rethrow(e);
            return null;
        }
    }

    @Override
    public void close() {
        try {
            rs.close();
        } catch (final SQLException e) {
            Utils.rethrow(e);
        }
    }

    @Override
    public int rowSize() {
        try {
            return rs.getMetaData().getColumnCount();
        } catch (final SQLException e) {
            Utils.rethrow(e);
            return 0;
        }
    }


    public void print() throws Exception {
        for(int i = 1; i <= columnCount(); ++i) {
            System.out.print(columnName(i) + "=" + rs.getObject(i) + " ");
        }
        System.out.println();
    }
}
