/**
 * Copyright 2016 Kareem Alkaseer.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.example.sql;

import org.example.DbEngine;
import org.example.RowSet;
import org.example.Utils;
import org.example.beans.Airport;
import org.example.beans.Country;
import org.example.beans.Runway;

import java.sql.Statement;

public class SqlEngine implements DbEngine<SqlContext> {

    private static final String kCountryRunwaysTypesString =
        "select " + Country.C.code + ", " +
        Country.C.name + ", " +
        Airport.C.name + ", " +
        Runway.C.surface +
        " from airport_runways where " + Runway.C.surface + " <> \"\"" +
        " group by " + Country.C.code + ", " + Airport.C.name + ", " + Runway.C.surface +
        " order by " + Country.C.code + ", " + Airport.C.name;

    private static final String kCountryAirportRankString =
        "select * from country_airports_ranks " +
        "where " + Airport.C.rank + " in " +
        "  (select distinct " + Airport.C.rank +
        "   from country_airports_ranks order by " + Airport.C.rank + " %s limit %d) " +
        "order by " + Airport.C.rank + " %s, " + Country.C.code + " %s";

    private static final String kCountryRunwaysRanksString =
        "select * from country_runways_ranks " +
        "where " + Runway.C.rank + " in" +
        "  (select distinct " + Runway.C.rank + "" +
        "   from country_runways_ranks order by " + Runway.C.rank + " %s limit %d) " +
        "order by " + Runway.C.rank + " %s, " + Country.C.code + " %s";


    public SqlEngine() { }


    @Override
    public RowSet countryAirportsRank(SqlContext cxt, int n, boolean asc) {

        try {
            Statement stmt = cxt.context().createStatement();
            return new SqlRowSet(stmt.executeQuery(
                String.format(
                    kCountryAirportRankString,
                    (asc ? "desc" : "asc"),
                    (n > 0 ? n : 10),
                    (asc ? "desc" : "asc"),
                    (asc ? "desc" : "asc"))));
        } catch (final Exception e) {
            Utils.rethrow(e);
            return null;
        }
    }

    @Override
    public RowSet countryRunwaysRank(SqlContext cxt, int n, boolean asc) {

        try {
            Statement stmt = cxt.context().createStatement();
            return new SqlRowSet(stmt.executeQuery(
                String.format(
                    kCountryRunwaysRanksString,
                    (asc ? "desc" : "asc"),
                    (n > 0 ? n : 10),
                    (asc ? "desc" : "asc"),
                    (asc ? "desc" : "asc"))));
        } catch (final Exception e) {
            Utils.rethrow(e);
            return null;
        }
    }


    @Override
    public RowSet countryRunwaysTypes(SqlContext cxt) {

        try {
            Statement stmt = cxt.context().createStatement();
            return new SqlRowSet(stmt.executeQuery(kCountryRunwaysTypesString));
        } catch (final Exception e) {
            Utils.rethrow(e);
            return null;
        }
    }

}
