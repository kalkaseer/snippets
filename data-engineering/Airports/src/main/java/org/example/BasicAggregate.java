/**
 * Copyright 2016 Kareem Alkaseer.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.example;

import java.util.Collections;
import java.util.List;

public class BasicAggregate implements Aggregate {


    private final String func;

    private final List<String> columns;

    private final String label;


    public BasicAggregate(String func, List<String> columns, String label) {
        assert(func != null && func.length() != 0);
        this.func = func;
        this.columns = columns != null && columns.size() != 0 ?
            columns : Collections.singletonList("*");
        this.label = label;
    }


    @Override
    public String function() {
        return func;
    }

    @Override
    public List<String> columns() {
        return Collections.unmodifiableList(columns);
    }

    @Override
    public String label() {
        return label;
    }

    @Override
    public String raw() {
        final StringBuilder b = new StringBuilder(func).append('(');
        columns.forEach(b::append);
        b.append(')');
        if (label != null && label.length() != 0)
            b.append(" AS ").append(label);
        return b.toString();
    }
}
