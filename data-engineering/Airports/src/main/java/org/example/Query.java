/**
 * Copyright 2016 Kareem Alkaseer.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.example;

import java.util.List;

public interface Query {

    String table();
    Query table(String t);

    List<String> where();
    Query where(List<String> w);
    boolean whereSet();

    List<String> whereRelations();
    Query whereRelations(List<String> r);
    boolean whereRelationsSet();

    List<Object> args();
    Query args(List<Object> a);

    List<String> cells();
    Query cells(List<String> c);
    boolean cellsSet();

    GroupingMode mode();
    Query mode(GroupingMode m);

    String orderBy();
    Query orderBy(String o);
    Query orderBy(String fn, List<String> cells, boolean asc);
    boolean orderBySet();

    String groupBy();
    Query groupBy(String g);
    boolean groupBySet();

    int limit();
    Query limit(int l);

    int offset();
    Query offset(int o);

    List<Aggregate> agg();
    Query agg(String fn, List<String> cells, String label);
    boolean aggregatesSet();
}