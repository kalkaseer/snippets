/**
 * Copyright 2016 Kareem Alkaseer.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.example.beans;

import com.google.common.collect.ImmutableMap;
import org.example.BeanConverter;

public class Runway {


    public static final class C {

        private C() { }

        public static final String id                   = "r_id";
        public static final String airportRef           = "r_ap_ref";
        public static final String airportIdentity      = "r_ap_ident";
        public static final String length               = "r_length";
        public static final String width                = "r_width";
        public static final String surface              = "r_surface";
        public static final String lighted              = "r_lighted";
        public static final String closed               = "r_closed";
        public static final String leIdentity           = "r_le_ident";
        public static final String leLatitude           = "r_le_latitude";
        public static final String leLongitude          = "r_le_longitude";
        public static final String leElevation          = "r_le_elevation";
        public static final String leHeadingDegree      = "r_le_heading_deg";
        public static final String leDisplacedThreshold = "r_le_dis_threshold";
        public static final String heIdentity           = "r_he_ident";
        public static final String heLatitude           = "r_he_latitude";
        public static final String heLongitude          = "r_he_longitude";
        public static final String heElevation          = "r_he_elevation";
        public static final String heHeadingDegree      = "r_he_heading_deg";
        public static final String heDisplacedThreshold = "r_he_dis_threshold";
        public static final String rank                 = "r_rank";
        public static final String count                = "r_count";
    }


    private long id = -1;

    private long airportRef;

    private String airportIdentity;

    private Integer length;

    private Integer width;

    private String surface;

    private Boolean lighted;

    private Boolean closed;

    private String leIdentity;

    private Double leLatitude;

    private Double leLongitude;

    private Integer leElevation;

    private Float leHeadingDegree;

    private Integer leDisplacedThreshold;

    private String heIdentity;

    private Double heLatitude;

    private Double heLongitude;

    private Integer heElevation;

    private Float heHeadingDegree;

    private Integer heDisplacedThreshold;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getAirportRef() {
        return airportRef;
    }

    public void setAirportRef(long airportRef) {
        this.airportRef = airportRef;
    }

    public String getAirportIdentity() {
        return airportIdentity;
    }

    public void setAirportIdentity(String airportIdentity) {
        this.airportIdentity = airportIdentity;
    }

    public Integer getLength() {
        return length;
    }

    public void setLength(Integer length) {
        this.length = length;
    }

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public String getSurface() {
        return surface;
    }

    public void setSurface(String surface) {
        this.surface = surface;
    }

    public Boolean getLighted() {
        return lighted;
    }

    public void setLighted(Boolean lighted) {
        this.lighted = lighted;
    }

    public Boolean getClosed() {
        return closed;
    }

    public void setClosed(Boolean closed) {
        this.closed = closed;
    }

    public String getLeIdentity() {
        return leIdentity;
    }

    public void setLeIdentity(String leIdentity) {
        this.leIdentity = leIdentity;
    }

    public Double getLeLatitude() {
        return leLatitude;
    }

    public void setLeLatitude(Double leLatitude) {
        this.leLatitude = leLatitude;
    }

    public Double getLeLongitude() {
        return leLongitude;
    }

    public void setLeLongitude(Double leLongitude) {
        this.leLongitude = leLongitude;
    }

    public Integer getLeElevation() {
        return leElevation;
    }

    public void setLeElevation(Integer leElevation) {
        this.leElevation = leElevation;
    }

    public Float getLeHeadingDegree() {
        return leHeadingDegree;
    }

    public void setLeHeadingDegree(Float leHeadingDegree) {
        this.leHeadingDegree = leHeadingDegree;
    }

    public Integer getLeDisplacedThreshold() {
        return leDisplacedThreshold;
    }

    public void setLeDisplacedThreshold(Integer leDisplacedThreshold) {
        this.leDisplacedThreshold = leDisplacedThreshold;
    }

    public String getHeIdentity() {
        return heIdentity;
    }

    public void setHeIdentity(String heIdentity) {
        this.heIdentity = heIdentity;
    }

    public Double getHeLatitude() {
        return heLatitude;
    }

    public void setHeLatitude(Double heLatitude) {
        this.heLatitude = heLatitude;
    }

    public Double getHeLongitude() {
        return heLongitude;
    }

    public void setHeLongitude(Double heLongitude) {
        this.heLongitude = heLongitude;
    }

    public Integer getHeElevation() {
        return heElevation;
    }

    public void setHeElevation(Integer heElevation) {
        this.heElevation = heElevation;
    }

    public Float getHeHeadingDegree() {
        return heHeadingDegree;
    }

    public void setHeHeadingDegree(Float heHeadingDegree) {
        this.heHeadingDegree = heHeadingDegree;
    }

    public Integer getHeDisplacedThreshold() {
        return heDisplacedThreshold;
    }

    public void setHeDisplacedThreshold(Integer heDisplacedThreshold) {
        this.heDisplacedThreshold = heDisplacedThreshold;
    }

    @Override
    public String toString() {

        return "id=" +id + " aRef=" + airportRef + " aIdent=" + airportIdentity + " length=" + length +
            " width=" + width + " surface=" + surface + " lighted=" + lighted + " closed" + closed +
            " leIdent=" + leIdentity + " leLat=" + leLatitude + " leLng=" + leLongitude + " leElevation=" + leElevation +
            " leHeadingDef=" + leHeadingDegree + " leDisplacement=" + leDisplacedThreshold +
            " heIdent=" + heIdentity + " heLat=" + heLatitude + " heLng=" + heLongitude + " heElevation=" + heElevation +
            " heHeadingDef=" + heHeadingDegree + " heDisplacement=" + heDisplacedThreshold;
    }


    public static final ImmutableMap<String, String> kMapping =
        new ImmutableMap.Builder<String, String>()
            .put(C.id,                   "id")
            .put(C.length,               "length")
            .put(C.width,                "width")
            .put(C.surface,              "surface")
            .put(C.lighted,              "lighted")
            .put(C.closed,               "closed")
            .put(C.airportRef,           "airportRef")
            .put(C.airportIdentity,      "airportIdentity")
            .put(C.leIdentity,           "leIdentity")
            .put(C.leLatitude,           "leLatitude")
            .put(C.leLongitude,          "leLongitude")
            .put(C.leHeadingDegree,      "leHeadingDegree")
            .put(C.leElevation,          "leElevation")
            .put(C.leDisplacedThreshold, "leDisplacedThreshold")
            .put(C.heIdentity,           "heIdentity")
            .put(C.heLatitude,           "heLatitude")
            .put(C.heLongitude,          "heLongitude")
            .put(C.heHeadingDegree,      "heHeadingDegree")
            .put(C.heDisplacedThreshold, "heDisplacedThreshold")
            .put(C.heElevation,          "heElevation")
            .build();

    public static class Converter extends BeanConverter {

        public Converter() {
            super(kMapping);
        }
    }

    public static final Converter kDefaultConverter = new Converter();
}
