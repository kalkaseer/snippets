/**
 * Copyright 2016 Kareem Alkaseer.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.example.beans;

import com.google.common.collect.ImmutableMap;
import org.example.BeanConverter;
import org.example.Utils;

import java.util.List;
import java.util.function.Function;

public class Country {

    public static final class C {

        private C() { };

        public static final String id        = "c_id";
        public static final String name      = "c_name";
        public static final String code      = "c_code";
        public static final String continent = "c_continent";
        public static final String wikiLink  = "c_wiki_link";
        public static final String keywords  = "c_keywords";
    }




    private long id = -1;

    private String code;

    private String name;

    private String wikiLink;

    private List<String> keywords;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getWikiLink() {
        return wikiLink;
    }

    public void setWikiLink(String wikiLink) {
        this.wikiLink = wikiLink;
    }

    public List<String> getKeywords() { return keywords; }

    public void setKeywords(List<String> keywords) { this.keywords = keywords; }


    @Override
    public String toString() {
        return code;
    }



    public static final ImmutableMap<String, String> kMapping =
        new ImmutableMap.Builder<String, String>()
            .put(C.id,        "id")
            .put(C.code,      "code")
            .put(C.name,      "name")
            .put(C.continent, "continent")
            .put(C.wikiLink,  "wikiLink")
            .put(C.keywords,  "keywords")
            .build();

    public static final ImmutableMap<String, Function> kPropsConverter =
        new ImmutableMap.Builder<String, Function>()
            .put("keywords", Utils.keywordsSplitter).build();

    public static class Converter extends BeanConverter {

        public Converter() {
            super(kMapping, kPropsConverter);
        }
    }

    public static final Converter kDefaultConverter = new Converter();
}
