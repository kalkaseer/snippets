/**
 * Copyright 2016 Kareem Alkaseer.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.example.beans;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import org.example.BeanConverter;
import org.example.GroupedBeanConverter;

import java.util.ArrayList;
import java.util.List;


public class CountryAirports {

    private Country country;

    private List<Airport> airports;


    public Country getCountry() { return country; }

    public void setCountry(Country c) { country = c; }

    public List<Airport> getAirports() { return airports; }

    public void setAirports(List<Airport> a) { airports = a; }

    public void add(Airport a) { airports.add(a); }


    public String toString() {
        if (country != null) {
            if (airports != null) {
                return country + ":" + airports;
            } else {
                return country + ":";
            }
        } else {
            if (airports != null) {
                return ":" + airports;
            } else {
                return ":";
            }
        }
    }



//    public static final ImmutableMap<String, String> kCountryMapping =
//        new ImmutableMap.Builder<String, String>()
//            .put("c_id", "id")
//            .put("c_code", "code")
//            .put("c_name", "name")
//            .put("c_wiki_link", "wikiLink")
//            .put("c_keywords", "keywords")
//            .build();
//
//    public static final ImmutableMap<String, String> kAirportMapping =
//        new ImmutableMap.Builder<String, String>()
//            .put("a_id", "id")
//            .put("a_name", "name")
//            .put("a_home_link", "homeLink")
//            .put("a_wiki_link", "wikiLink")
//            .put("a_keywords", "keywords")
//            .put("a_lat", "latitude")
//            .put("a_lng", "longitude")
//            .build();


    public static class Converter extends GroupedBeanConverter {

        public static final BeanConverter kCountryConverter =
            new BeanConverter(Country.kMapping, Country.kPropsConverter);

        public static final BeanConverter kAirportConverter =
            new BeanConverter(
                Airport.kMapping, Airport.kPropsConverter, Airport.class,
                ImmutableMap.of("airports", ArrayList.class));


        public Converter() {

            super(
                ImmutableMap.of("airports", (container, member) -> {
                    ((List<Airport>) container).add((Airport) member);
                }),
                ImmutableMap.of(
                    "country", kCountryConverter,
                    "airports", kAirportConverter
                ),
                ImmutableList.of(Country.C.code)
            );
        }
    }

    public static final Converter kDefaultConverter = new Converter();

}
