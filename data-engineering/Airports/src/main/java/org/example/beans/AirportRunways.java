/**
 * Copyright 2016 Kareem Alkaseer.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.example.beans;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import org.example.BeanConverter;
import org.example.GroupedBeanConverter;

import java.util.ArrayList;
import java.util.List;


public class AirportRunways {

    private Airport airport;

    private List<Runway> runways;


    public Airport getAirport() { return airport; }

    public void setAirport(Airport a) { airport = a; }

    public List<Runway> getRunways() { return runways; }

    public void setRunways(List<Runway> r) { runways = r; }

    public void add(Runway r) { runways.add(r); }


    @Override
    public String toString() {
        if (airport != null) {
            if (runways != null)
                return airport + ":" + runways;
            else
                return airport + ":";
        } else {
            if (runways != null)
                return ":" + runways;
            else
                return "::";
        }
    }


    public static class Converter extends GroupedBeanConverter {

        private static final ImmutableMap<String, String> kRunwayMapping;

        private static final ImmutableMap<String, String> kAirportMapping;

        static {

            final ImmutableMap.Builder<String, String> b = new ImmutableMap.Builder<>();
            Runway.kMapping
                .entrySet()
                .stream()
                .filter(e -> !e.getKey().equals(Runway.C.airportRef) && !e.getKey().equals(Runway.C.airportIdentity))
                .forEach(e -> b.put(e.getKey(), e.getValue()));
            b.put(Airport.C.id, "airportRef")
             .put(Airport.C.identity, "airportIdentity");
            kRunwayMapping = b.build();

            final ImmutableMap.Builder<String, String> b2 = new ImmutableMap.Builder<>();
            Airport.kMapping
                .entrySet()
                .stream()
                .filter(e -> !e.getKey().equals(Airport.C.continent) && !e.getKey().equals(Airport.C.country))
                .forEach(e -> b2.put(e.getKey(), e.getValue()));
            b2.put(Country.C.continent, "continent")
              .put(Country.C.code, "country");
            kAirportMapping = b2.build();

        }


        public static final BeanConverter kAirportConverter =
                new BeanConverter(kAirportMapping, Airport.kPropsConverter);

        public static final BeanConverter kRunwayConverter =
            new BeanConverter(kRunwayMapping, null, Runway.class,
                ImmutableMap.of("runways", ArrayList.class));


        public Converter() {

            super(
                ImmutableMap.of("runways", (container, member) -> {
                    ((List<Runway>) container).add((Runway) member);
                }),
                ImmutableMap.of(
                    "airport", kAirportConverter,
                    "runways", kRunwayConverter
                ),
                ImmutableList.of(Airport.C.identity)
            );
        }
    }

    public static final Converter kDefaultConverter = new Converter();

}
