/**
 * Copyright 2016 Kareem Alkaseer.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.example.beans;

import com.google.common.collect.ImmutableMap;
import org.example.BeanConverter;
import org.example.Utils;

import java.util.List;
import java.util.function.Function;

public class Airport {


    public static final class C {

        private C() { }

        public static final String id               = "a_id";
        public static final String name             = "a_name";
        public static final String identity         = "a_ident";
        public static final String latitude         = "a_lat";
        public static final String longitude        = "a_lng";
        public static final String gpsCode          = "a_gps_code";
        public static final String localCode        = "a_local_code";
        public static final String type             = "a_type";
        public static final String elevation        = "a_elevation";
        public static final String continent        = "a_continent";
        public static final String country          = "a_country";
        public static final String region           = "a_region";
        public static final String municipality     = "a_municipality";
        public static final String serviceScheduled = "a_sch_service";
        public static final String iata             = "a_iata";
        public static final String homeLink         = "a_home_link";
        public static final String wikiLink         = "a_wiki_link";
        public static final String keywords         = "a_keywords";
        public static final String count            = "a_count";
        public static final String rank             = "a_rank";
    }




    private long id = -1;

    private String identity;

    private String type;

    private String name;

    private Double latitude;

    private Double longitude;

    private Integer elevation;

    private String continent;

    private String country;

    private String region;

    private String municipality;

    private Boolean serviceScheduled;

    private String gpsCode;

    private String iata;

    private String localCode;

    private String homeLink;

    private String wikiLink;

    private List<String> keywords;


    public long getId() { return id; }

    public void setId(long id) {
        this.id = id;
    }

    public String getIdentity() {
        return identity;
    }

    public void setIdentity(String identity) {
        this.identity = identity;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Integer getElevation() {
        return elevation;
    }

    public void setElevation(Integer elevation) {
        this.elevation = elevation;
    }

    public String getContinent() {
        return continent;
    }

    public void setContinent(String continent) {
        this.continent = continent;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getMunicipality() {
        return municipality;
    }

    public void setMunicipality(String municipality) {
        this.municipality = municipality;
    }

    public Boolean getServiceScheduled() {
        return serviceScheduled;
    }

    public void setServiceScheduled(Boolean serviceScheduled) {
        this.serviceScheduled = serviceScheduled;
    }

    public String getGpsCode() {
        return gpsCode;
    }

    public void setGpsCode(String gpsCode) {
        this.gpsCode = gpsCode;
    }

    public String getIata() {
        return iata;
    }

    public void setIata(String iata) {
        this.iata = iata;
    }

    public String getLocalCode() {
        return localCode;
    }

    public void setLocalCode(String localCode) {
        this.localCode = localCode;
    }

    public String getHomeLink() {
        return homeLink;
    }

    public void setHomeLink(String homeLink) {
        this.homeLink = homeLink;
    }

    public String getWikiLink() {
        return wikiLink;
    }

    public void setWikiLink(String wikiLink) {
        this.wikiLink = wikiLink;
    }

    public List<String> getKeywords() {
        return keywords;
    }

    public void setKeywords(List<String> keywords) {
        this.keywords = keywords;
    }


    @Override
    public String toString() {
        return name;
    }


    public static final ImmutableMap<String, String> kMapping =
        new ImmutableMap.Builder<String, String>()
            .put(C.id,                "id")
            .put(C.continent,         "continent")
            .put(C.country,           "country")
            .put(C.elevation,         "serviceScheduled")
            .put(C.gpsCode,           "gpsCode")
            .put(C.iata,              "iata")
            .put(C.identity,          "identity")
            .put(C.keywords,          "keywords")
            .put(C.latitude,          "latitude")
            .put(C.localCode,         "localCode")
            .put(C.homeLink,          "homeLink")
            .put(C.longitude,         "longitude")
            .put(C.municipality,      "municipality")
            .put(C.name,              "name")
            .put(C.region,            "region")
            .put(C.serviceScheduled,  "serviceScheduled")
            .put(C.type,              "type")
            .put(C.wikiLink,          "wikiLink")
            .build();

    public static final ImmutableMap<String, Function> kPropsConverter =
        new ImmutableMap.Builder<String, Function>()
            .put("keywords", Utils.keywordsSplitter).build();

    public static class Converter extends BeanConverter {

        public Converter() {
            super(kMapping, kPropsConverter);
        }
    }

    public static final Converter kDefaultConverter = new Converter();
}
