/**
 * Copyright 2016 Kareem Alkaseer.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.example.beans;

import com.google.common.collect.ImmutableMap;
import org.example.BeanConverter;

public class CountryAirportsRank {

    private long countryId = -1;
    private String countryCode;
    private String countryName;
    private int airportCount;
    private int rank = 1;

    public long getCountryId() {
        return countryId;
    }

    public void setCountryId(long countryId) {
        this.countryId = countryId;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public int getAirportCount() {
        return airportCount;
    }

    public void setAirportCount(int airportCount) {
        this.airportCount = airportCount;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }


    @Override
    public String toString() {
        return "cid=" + countryId + " ccode=" + countryCode + " cname=" + countryName +
            " acount=" + airportCount + " rank=" + rank;
    }


    public static class Converter extends BeanConverter {

        public static final ImmutableMap<String, String> kMapping =
            new ImmutableMap.Builder<String, String>()
                .put(Country.C.id,   "countryId")
                .put(Country.C.code, "countryCode")
                .put(Country.C.name, "countryName")
                .put(Airport.C.count, "airportCount")
                .put(Airport.C.rank, "rank")
                .build();


        public Converter() {
            super(kMapping);
        }
    }

    public static final Converter kDefaultConverter = new Converter();

}
