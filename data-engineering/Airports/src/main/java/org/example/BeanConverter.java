/**
 * Copyright 2016 Kareem Alkaseer.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.example;

import com.google.common.collect.ImmutableMap;

import java.beans.BeanInfo;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.sql.SQLXML;
import java.sql.Timestamp;
import java.util.*;
import java.util.function.Function;


public class BeanConverter {

    public static final int NONE = -1;

    private final ImmutableMap<Class<?>, Object> kPrimitives =
        new ImmutableMap.Builder<Class<?>, Object>()
            .put(Short.TYPE, (short) 0)
            .put(Integer.TYPE, 0)
            .put(Long.TYPE, 0L)
            .put(Float.TYPE, 0f)
            .put(Double.TYPE, 0d)
            .put(Boolean.TYPE, Boolean.FALSE)
            .put(Character.TYPE, (char) 0)
            .build();


    private final ImmutableMap<String, String> columnToPropertyMapping;

    private final ImmutableMap<String, Function> propConverters;

    private final Class<?> defaultClass;

    private final ImmutableMap<String, Class<?>> containerClasses;


    public BeanConverter() {
        columnToPropertyMapping = null;
        propConverters = null;
        defaultClass = null;
        containerClasses = null;
    }

    public BeanConverter(ImmutableMap<String, String> columnToProperty) {
        this.columnToPropertyMapping = columnToProperty;
        this.propConverters = null;
        defaultClass = null;
        containerClasses = null;
    }

    public BeanConverter(
        ImmutableMap<String, String> columnToProperty,
        ImmutableMap<String, Function> propConverters
    ) {
        this.columnToPropertyMapping = columnToProperty;
        this.propConverters = propConverters;
        defaultClass = null;
        containerClasses = null;
    }

    public <T> BeanConverter(
        ImmutableMap<String, String> columnToProperty,
        ImmutableMap<String, Function> propConverters,
        Class<T> defaultClass,
        ImmutableMap<String, Class<?>> containerClasses
    ) {
        this.columnToPropertyMapping = columnToProperty;
        this.propConverters = propConverters;
        this.defaultClass = defaultClass;
        this.containerClasses = containerClasses;
    }


    public <T> T bean(RowSet rs, Class<T> klass) throws Exception {

        final PropertyDescriptor[] descriptors = propertyDescriptors(klass);
        int[] columnPropertyMapping = mapColumnsToProperties(rs, descriptors);
        return makeBean(rs, null, klass, descriptors, columnPropertyMapping);
    }

    public <T> List<T> list(RowSet rs, Class<T> klass) throws Exception {

        List<T> res = new ArrayList<>();

        final PropertyDescriptor[] descriptors = propertyDescriptors(klass);
        int[] columnPropertyMapping = mapColumnsToProperties(rs, descriptors);

        do {
            res.add(makeBean(rs, null, klass, descriptors, columnPropertyMapping));
        } while (rs.next());

        return res;
    }

    protected <T> T makeBean(
        RowSet rs,
        String property,
        Class<T> klass,
        PropertyDescriptor[] desc,
        int[] columnPropertyMap
    ) throws Exception {

//        ((SqlRowSet) rs).print(); // TODO

        T bean;

        if (!klass.isInterface()) {

            bean = klass.newInstance();

        } else if (containerClasses == null) {

            throw new RuntimeException(
                "expected non-interface class or default container classes");

        } else {

            Class<?> c = c = containerClasses.get(property);

            if (c != null) {
                assert (klass.isAssignableFrom(c));
                bean = (T) c.newInstance();
            } else {
                throw new RuntimeException("expected non-interface class or default container class: "
                    + klass.getName());
            }

        }



        for (int i = 1; i < columnPropertyMap.length; ++i) {

            if (columnPropertyMap[i] == NONE)
                continue;

            setBeanProperty(rs, bean, desc[columnPropertyMap[i]], i);
        }

        return bean;
    }

    protected <T> void setBeanProperty(
        RowSet rs, T bean, PropertyDescriptor prop, int idx
    ) throws Exception {

        Class<?> ptype = prop.getPropertyType();

        Object value = null;

        if (ptype != null) {
            Object tmp = convertColumn(rs, idx, ptype);
            Function f = null;
            if (propConverters != null)
                f = propConverters.get(prop.getName());
            value = f != null ? f.apply(tmp) : tmp;
            if (value == null && ptype.isPrimitive()) {
                value = kPrimitives.get(ptype);
            }
        }

        callSetter(bean, prop, value);
    }

    protected Object convertColumn(RowSet rs, int index, Class<?> propType)
        throws Exception
    {
        if (!propType.isPrimitive() && rs.getObject(index) == null)
            return null;

        if (propType.equals(String.class)) {
            return rs.getString(index);
        } else if (propType.equals(Integer.TYPE) || propType.equals(Integer.class)) {
            return rs.getInt(index);
        } else if (propType.equals(Long.TYPE) || propType.equals(Long.class)) {
            return rs.getLong(index);
        } else if (propType.equals(Short.TYPE) || propType.equals(Short.class)) {
            return rs.getShort(index);
        } else if (propType.equals(Float.TYPE) || propType.equals(Float.class)) {
            return rs.getFloat(index);
        } else if (propType.equals(Double.TYPE) || propType.equals(Double.class)) {
            return rs.getDouble(index);
        } else if (propType.equals(Boolean.TYPE) || propType.equals(Boolean.class)) {
            return rs.getBoolean(index);
        } else if (propType.equals(Byte.TYPE) || propType.equals(Byte.class)) {
            return rs.getByte(index);
        } else if (propType.equals(Timestamp.class)) {
            return rs.getTimestamp(index);
        } else if (propType.equals(SQLXML.class)) {
            return rs.getXml(index);
        } else {
            return rs.getObject(index);
        }
    }

    protected void callSetter(Object bean, PropertyDescriptor desc, Object value)
        throws Exception
    {
        Method m = desc.getWriteMethod();
        if (m == null)
            return;

        Class<?> [] params = m.getParameterTypes();

        if (value instanceof Date) {
            final String t = params[0].getName();
            if ("java.util.Date".equals(t)) {
                value = new java.sql.Date(((Date) value).getTime());
            } else if ("java.sql.Time".equals(t)) {
                value = new java.sql.Time(((Date) value).getTime());
            } else if ("java.sql.Timestamp".equals(t)) {
                final Timestamp ts = (Timestamp) value;
                value = new Timestamp(ts.getTime());
                ((Timestamp) value).setNanos(ts.getNanos());
            }
        } else if (value instanceof String && params[0].isEnum()) {
            value = Enum.valueOf(params[0].asSubclass(Enum.class), (String) value);
        }

        if (isCompatibleClass(value, params[0])) {
            m.invoke(bean, value);
        } else {
            throw new Exception("inconvertible types for property " + desc.getName() +
                    " [" + value.getClass().getName() + ", " + params[0].getName() + "]");
        }
    }

    private static boolean isCompatibleClass(Object value, Class<?> klass) {

        if (value == null || klass.isInstance(value))
            return true;

        else if (klass.equals(Integer.TYPE) && value instanceof Integer)
            return true;

        else if (klass.equals(Long.TYPE) && value instanceof Long)
            return true;

        else if (klass.equals(Short.TYPE) && value instanceof Short)
            return true;

        else if (klass.equals(Double.TYPE) && value instanceof Double)
            return true;

        else if (klass.equals(Float.TYPE) && value instanceof Float)
            return true;

        else if (klass.equals(Character.TYPE) && value instanceof Character)
            return true;

        else if (klass.equals(Byte.TYPE) && value instanceof Byte)
            return true;

        else if (klass.equals(Boolean.TYPE) && value instanceof Boolean)
            return true;

        return false;
    }


    protected int[] mapColumnsToProperties(RowSet rs, PropertyDescriptor [] desc)
        throws Exception
    {
        int count = rs.columnCount();
        int[] mapping = new int[count + 1];

        Arrays.fill(mapping, NONE);

        for (int c = 1; c <= count; ++c) {
            final String cname = rs.columnName(c);
            String pname = null;
            if (columnToPropertyMapping != null)
                pname = columnToPropertyMapping.get(cname);
            if (pname == null)
                pname = cname;

            for (int i = 0;i < desc.length; ++i) {
                if (pname.equalsIgnoreCase(desc[i].getName())) {
                    mapping[c] = i;
                    break;
                }
            }
        }

        return mapping;
    }


    protected PropertyDescriptor[] propertyDescriptors(Class<?> c) throws Exception {

        final BeanInfo info = Introspector.getBeanInfo(c);
        return info.getPropertyDescriptors();
    }


    public Class<?> defaultClass() { return defaultClass; }

    public Map<String, Class<?>> containerClasses() { return containerClasses; }
}
