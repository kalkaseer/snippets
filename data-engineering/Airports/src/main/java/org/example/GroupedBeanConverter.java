/**
 * Copyright 2016 Kareem Alkaseer.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.example;

import com.google.common.collect.ImmutableMap;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.util.*;
import java.util.function.BiConsumer;
import java.util.function.Function;

public class GroupedBeanConverter extends BeanConverter {

    private final Map<String, BiConsumer> repeatedMembers;

    private final List<String> groupKeys;

    private final Map<String, BeanConverter> converters;

    public GroupedBeanConverter(
        Map<String, BiConsumer> repeatedMembers,
        Map<String, BeanConverter> converters,
        List<String> groupKeys
    ) {
        super();
        this.repeatedMembers = repeatedMembers;
        this.converters = converters;
        this.groupKeys = groupKeys;
    }

    public GroupedBeanConverter(
        ImmutableMap<String, String> sharedColumnToProperty,
        Map<String, BiConsumer> repeatedMembers,
        Map<String, BeanConverter> converters,
        List<String> orderedBy
    ) {
        super(sharedColumnToProperty);
        this.repeatedMembers = repeatedMembers;
        this.converters = converters;
        this.groupKeys = orderedBy;
    }

    public GroupedBeanConverter(
        ImmutableMap<String, String> sharedColumnToProperty,
        ImmutableMap<String, Function> sharedPropConverters,
        Map<String, BiConsumer> repeatedMembers,
        Map<String, BeanConverter> converters,
        List<String> orderedBy
    ) {
        super(sharedColumnToProperty, sharedPropConverters);
        this.repeatedMembers = repeatedMembers;
        this.converters = converters;
        this.groupKeys = orderedBy;
    }

    private Object callGetter(Object bean, PropertyDescriptor desc) throws Exception {
        Method m = desc.getReadMethod();
        if (m == null)
            return null;
        return m.invoke(bean);
    }

    private boolean sameKeys(Map<String, Pair> values) {
        return values.values().stream().allMatch(pair -> {
            if (pair.o1 != null)
                return pair.o1.equals(pair.o2);
            else if (pair.o2 == null)
                return true;
            return false;
        });
    }

    private class Pair {

        public Pair(Object _1, Object _2) { o1 = _1; o2 = _2; }
        Object o1;
        Object o2;

        public String toString() { return o1 + ":" + o2; }
    }

    @Override
    protected <T> T makeBean(
        RowSet rs,
        String property,
        Class<T> klass,
        PropertyDescriptor[] desc,
        int[] columnPropertyMap
    ) throws Exception {


        if (groupKeys == null)
            return super.makeBean(rs, null, klass, desc, columnPropertyMap);

        final HashMap<String, Pair> keysTuples = new HashMap<>(groupKeys.size());
        groupKeys.forEach(k -> keysTuples.put(k, new Pair(rs.getObject(k), null)));

        final HashMap<String, Object> containers = new HashMap<>(repeatedMembers.size());

//        ((SqlRowSet) rs).print(); // TODO


        T bean = klass.newInstance();

        for (PropertyDescriptor d : desc) {

            final String pname = d.getName();
            BeanConverter convert = converters.get(pname);

            if (convert != null) {

                final PropertyDescriptor[] descs = propertyDescriptors(d.getPropertyType());
                int[] mapping = convert.mapColumnsToProperties(rs, descs);

                Class<?> ptype = d.getPropertyType();
                Object obj = convert.makeBean(rs, pname, ptype, descs, mapping);
                callSetter(bean, d, obj);

                if (repeatedMembers.get(pname) != null)
                    containers.put(pname, obj);
            }
        }

        addMemeber(rs, containers);


        while (rs.next()) {

//            ((SqlRowSet) rs).print(); // TODO

            keysTuples.forEach((s, p) -> {
                keysTuples.get(s).o2 = rs.getObject(s);
            });

            if (!sameKeys(keysTuples)) {
                rs.previous();
                return bean;
            }

            addMemeber(rs, containers);


            keysTuples.forEach((s, p) -> {
                keysTuples.get(s).o1 = keysTuples.get(s).o2;
            });
        }

        return bean;
    }


    private void addMemeber(RowSet rs, HashMap<String, Object> containers) {

        repeatedMembers.forEach((property, consumer) -> {
            BeanConverter convert = converters.get(property);
            if (convert != null) {
                try {
                    final Class<?> ptype = convert.defaultClass();
                    if (ptype == null)
                        return;
                    final PropertyDescriptor[] descs = propertyDescriptors(ptype);
                    int[] mapping = convert.mapColumnsToProperties(rs, descs);
                    Object member = convert.makeBean(rs, null, ptype, descs, mapping);
                    consumer.accept(containers.get(property), member);
                } catch (final Exception e) {
                    Utils.rethrow(e);
                }
            }
        });
    }

}
