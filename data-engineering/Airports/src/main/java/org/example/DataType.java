/**
 * Copyright 2016 Kareem Alkaseer.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.example;

import java.sql.Time;
import java.sql.Timestamp;
import java.sql.Types;

public enum DataType {

    INT("INTEGER", Types.INTEGER),
    SHORT("SMALLINT", Types.SMALLINT),
    LONG("BIGINT", Types.BIGINT),
    REAL("REAL", Types.REAL),
    FLOAT("FLOAT", Types.FLOAT),
    DOUBLE("DOUBLE PRECISION", Types.DOUBLE),
    VARCHAR("VARCHAR", Types.VARCHAR),
    CHAR("INTEGER", Types.INTEGER),
    BOOL("BOOL", Types.BOOLEAN),
    TIMESTAMP("TIMESTAMP", Types.TIMESTAMP),
    DATE("DATE", Types.DATE),
    TIME("TIME", Types.TIME);


    DataType(String s, int t) {
        string = s;
        sqlType = t;
    }

    public final String string;
    public final int sqlType;


    public static <T> DataType of(T o) {

        Class<?> c = o.getClass();

        if (c.equals(String.class)) {
            return VARCHAR;
        } else if (c.equals(Integer.TYPE) || c.equals(Integer.class)) {
            return INT;
        } else if (c.equals(Long.TYPE) || c.equals(Long.class)) {
            return LONG;
        } else if (c.equals(Short.TYPE) || c.equals(Short.class)) {
            return SHORT;
        } else if (c.equals(Float.TYPE) || c.equals(Float.class)) {
            return FLOAT;
        } else if (c.equals(Double.TYPE) || c.equals(Double.class)) {
            return DOUBLE;
        } else if (c.equals(Boolean.TYPE) || c.equals(Boolean.class)) {
            return BOOL;
        } else if (c.equals(Character.TYPE) || c.equals(Character.class)) {
            return CHAR;
        } else if (c.equals(Byte.TYPE) || c.equals(Byte.class)) {
            return INT;
        } else if (c.equals(Timestamp.class) || c.equals(java.util.Date.class)) {
            return TIMESTAMP;
        } else if (c.equals(Time.class)) {
            return TIME;
        } else if (c.equals(java.sql.Date.class)) {
            return DATE;
        }

        throw new IllegalArgumentException("unsupported class: " + c);
    }
}
