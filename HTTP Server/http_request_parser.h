#ifndef BLINK_HTTP_PARSER_H_
#define BLINK_HTTP_PARSER_H_

#include <tuple>

namespace blink {
namespace net {
namespace http {
	
struct request;

class request_parser {

public:
	
	request_parser();
	
	void reset();
	
	enum result_type { good, bad, intermediate };
	
	/// Parse some data. The returned result element is good when a complete
	/// request has been parsed, bad if the data is invalid, intermediate when
	/// more data is required. The InputIterator returned indicates how much of 
	/// the input has been consumed. 
	template <typename InputIterator>
	std::tuple<result_type, InputIterator> parse(
		request & req, InputIterator begin, InputIterator end
	) {
		while(begin != end) {
			result_type result = consume(req, *begin++);
			if (result == good || result == bad)
				return std::make_tuple(result, begin);
		}
		return std::make_tuple(intermediate, begin);
	}
	
private:
	
	/// Handle next character of input.
	result_type consume(request & req, char input);
	
	/// Check if a byte is an HTTP character.
	static bool is_char(int c);
	
	/// Check if a byte is an HTTP control character.
	static bool is_ctl(int c);
	
	/// Check if a byte is an HTTP tspecial character.
	static bool is_tspecial(int c);
	
	/// Check if a byte is a digit.
	static bool is_digit(int c);
	
	/// Current state of the parser
	enum state {
		method_start,
		method,
		uri,
		http_version_h,
		http_version_t_1,
		http_version_t_2,
		http_version_p,
		http_version_slash,
		http_version_major_start,
		http_version_major,
		http_version_minor_start,
		http_version_minor,
		expecting_newline_1,
		header_line_start,
		header_lws,
		header_name,
		space_before_header_value,
		header_value,
		expecting_newline_2,
		expecting_newline_3,
	} state_;
	
};
	
} // namespace http
} // namespace net
} // namespace blink

#endif // BLINK_HTTP_PARSER_H_
