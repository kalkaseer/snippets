#ifndef COMPILER_H_
#define COMPILER_H_

#if defined(__GCC__)
# define GCC_VERSION ( __GNUC__ * 10000     \
                     + __GNUC_MINOR__ * 100 \
                     + __GNUC_PATCHLEVEL__)
#endif

/* Compatibility with non-clang compilers */
#ifndef __has_attribute
# define __has_attribute(x) 0
#endif


#if defined(__GCC__)
# if defined (__llvm__) || defined(__clang__)
#  if __has_attribute(always_inline)
#   define force_inline __attribute__(always_inline)
#  else
#   define force_inline
#  endif
# else /* not __llvm__ || __clang */
#  define force_inline __attribute__((always_inline))
# endif
#elif defined(WIN32)
# define force_inline __forceinline
#else
# define force_inline
#endif

#if defined(__GCC__)
# define likely(x)   __builtin_expect((x), 1)
# define unlikely(x) __builtin_expect((x), 0)
#else
# define likely(x)    (x)
#define  unlikely(x)  (x)
#endif

#if defined(__GCC__)
# define __prefetch(addr, rw, locality) \
   void __builtin_prefetch( const void *addr, int rw,  int locality );
#else
# define __prefetch(addr, rw, locality)
#endif

#if defined(__GCC__) && GCC_VERSION > 40300
# define __hot  __attribute__((hot))
# define __cold __attribute__((cold))
#else
# define __hot
# define __cold
#endif

#if defined(__GCC__) && GCC_VERSION > 29600
# define __pure __attribute__((pure))
#else
# define __pure
#endif

#if __STDC_VERSION__ >= 199901L || defined(__C99_RESTRICT) && (     \
      defined(__xlC__) || defined(__IBMC__) ||                      \
      defined(__IBMCPP__) || defined(__xlc__)                       \
    )
# define restrict restrict
#elif defined(__GCC__) || defined(__ICC) || defined(__INTEL_COMPILER) || \
      defined(__xlC__) || defined(__IBMC__) || defined(__IBMCPP__)    || \
      defined(__xlc__)
# define restrict __restrict__
#elif defined(WIN32)
# define restrict __restrict
#else
# define restrict
#endif

#ifdef __cplusplus
# define ALIGNP(p, t) \
    (reinterpret_cast<uint8*>(((reinterpret_cast<uintptr_t>(p) + \
    ((t) - 1)) & ~((t) - 1))))

#else  /* __cplusplus */

# define ALIGNP(p, t) ((uint8 *) ((((uintptr_t>(p)) + ((t) - 1)) & ~((t) - 1))))
#endif /* __cplusplus */

#define IS_ALIGNED(p, a) (!((uintptr_t)(p) & ((a) - 1)))

#endif /* COMPILER_H_ */
