#include <utility>
#include <vector>
#include <memory>
#include "connection_manager.h"
#include "http_connection.h"

#include "http_parser.h"

namespace blink {
namespace net {
namespace http {

connection::connection(
	asio::ip::tcp::socket && s,
	connection_manager & m,
	request_handler & h
) : socket{ std::move(s) },
    manager{ m },
    req_handler{ h }
{ }

void connection::start(asio::yield_context yield) {
	read(yield);
}

void connection::stop() {
	socket.close();
}

void connection::read(asio::yield_context yield) {

	auto self(shared_from_this());
	std::error_code ec;
  std::size_t length = socket.async_read_some(asio::buffer(buffer), yield[ec]);
  
  if (!ec) {
    request_parser::result_type result;
    std::tie(result, std::ignore) = req_parser.parse(
      req, buffer.data(), buffer.data() + length);
    if (result == request_parser::good) {
      req_handler.handle(req, res);
      write(yield);
    } else if (result == request_parser::bad) {
      response::make(res, response::bad_request);
      write(yield);
    } else {
      read(yield);
    }
  } else if (ec && ec != asio::error::operation_aborted) {
		manager.stop(shared_from_this());
    return;
	}
  
}

void connection::write(asio::yield_context yield) {
  
	auto self(shared_from_this());
	std::vector<asio::const_buffer> bufs;
  res.to_buffers(bufs);
	std::error_code ec;
	asio::async_write(socket, bufs, yield[ec]);
  
  if (!ec) {
    // Close connection gracefully.
    asio::error_code ignored_ec;
    socket.shutdown(asio::ip::tcp::socket::shutdown_both, ignored_ec);
  }
  
  if (ec != asio::error::operation_aborted)
    manager.stop(shared_from_this());
}

} // namespace http
} // namespace net
} // namespace blink
