#ifndef _BLINK_HTTP_PARSER_H
#define _BLINK_HTTP_PARSER_H

#include <functional>
#include <memory>
#include <sys/types.h>
#include <cassert>

#include "basic_types.h"
#include "http_payload.h"
#include "http_method.h"
#include "str_const.h"
#include "url.h"

#define HTTP_PARSER_VERSION_MAJOR 2
#define HTTP_PARSER_VERSION_MINOR 6
#define HTTP_PARSER_VERSION_PATCH 1

// Compile with -DHTTP_PARSER_STRICT=0 to make less checks but parse faster.
#ifndef HTTP_PARSER_STRICT
# define HTTP_PARSER_STRICT 1
#endif

// By default, if HTTP parser is in strict mode, URL parser will be as well.
#if HTTP_PARSER_STRICT
# define URL_PARSER_STRICT 1
#else
# define URL_PARSER_STRICT 0
#endif

#ifndef HTTP_PARSER_NOTIY_CALLBACKS
# define HTTP_PARSER_NOTIY_CALLBACKS 0
#endif

#ifndef HTTP_MAX_HEADER_SIZE
# define HTTP_MAX_HEADER_SIZE (80*1024)
#endif

namespace blink {
namespace net {
namespace http {
	
namespace parsing {

/* ***************************************************************************
 * enum error and relevant functions.
 * ***************************************************************************/
 
/****************************************************************************
 * Signifies parsing errors encountered as the parser executes or lack of them.
 ****************************************************************************/
enum class error : uint8 {
	
	none = 0, // No errors
	
	// Callbacks-related errors
	cb_message_begin,
	cb_url,
	cb_header_field,
	cb_header_value,
	cb_headers_complete,
	cb_body,
	cb_message_complete,
	cb_status_code,
	cb_chunk_header,
	cb_chunk_complete,
	
	// Parsing-related errors
	invalid_eof,
	header_overflow,
	closed_connection,
	version,
	status_code,
	method,
	url,
	host,
	port,
	path,
	query,
	fragment,
	linefeed_expected,
	header_token,
	content_length,
	unexpected_content_length,
	chunck_size,
	constant,
	state,
	strict_assertion,
	paused,
	unknown,
};

/****************************************************************************
 * Converts and error code to a readable string. 
 ****************************************************************************/
const str_const & name(error error);

/****************************************************************************
 * Gets the description for an error code.
 ****************************************************************************/
const str_const & description(error error);

/****************************************************************************
 * Returns the library version. Bits 16-23 contain the major version number,
 * bits 8-15 the minor version number and bits 0-7 the patch level.
 ****************************************************************************/
uint64 version();

inline uint32 version_major(uint64 v) {
	return static_cast<uint32>(((v) >> 16) & 255);
} 

inline uint32 version_minor(uint64 v) {
	return static_cast<uint32>(((v) >> 8) & 255);
} 

inline uint32 version_patch(uint64 v) {
	return static_cast<uint32>(v & 255);
} 

} // namespace parsing

/* ***************************************************************************
 * Forward declarations
 * **************************************************************************/

namespace parsing {
namespace details {
	enum class state : uint8;
}
}

class parser;
typedef std::shared_ptr<parser> parser_ptr;


/***************************************************************************
 * @class parser
 * @author Kareem Alkaseer
 * @date 25/02/2016
 * @file http_parser.h
 * @brief An event-based HTTP parser for versions 1.0, 1.1 and 2.
 * *************************************************************************/
class parser
	: public std::enable_shared_from_this<parser>
{

public:

	enum class flag : uint8 {
		chunked             = 1 << 0,
		keep_alive          = 1 << 1,
		close               = 1 << 2,
		connection_upgrade  = 1 << 3,
		trailing            = 1 << 4,
		upgrade             = 1 << 5,
		skip_body           = 1 << 6,
		content_length      = 1 << 7,
	};
	
	using string_view = url::string_view;
	
	typedef string_view::iterator       iterator;
	typedef string_view::const_iterator const_iterator;

	typedef
		std::function<bool (parser *, const_iterator, const_iterator)>
		data_callback;

	typedef std::function<bool (parser *)> callback;

	typedef std::function<bool (parser *, uint16)> status_code_callback;
	
	struct callbacks {

		data_callback        on_url;
		data_callback        on_header_field;
		data_callback        on_header_value;
		data_callback        on_body;
		callback             on_message_begin;
		callback             on_message_complete;
		callback             on_headers_complete;
		/// parser::content_length() will return the chunk length.
		callback             on_chunk_header;
		callback             on_chunk_complete;
		status_code_callback on_status_code;
		
		void reset();
	};
	
	typedef std::shared_ptr<callbacks> callbacks_ptr;
	
	template< class ...Args >
	static inline callbacks_ptr make_callback_ptr(Args && ...args) {
	  return std::make_shared<callbacks>(std::forward<Args>(args)...);
	}

	enum class result : uint8 { good, bad, intermediate };
	
	enum class type : uint8 { request = 0, response = 1, both };

private:

	using pstate = parsing::details::state;

	using perror = parsing::error;

	enum class hstate : uint8;
	
public:
	
	explicit parser(type parser_type);
	
	std::tuple<result, iterator> parse(
		const callbacks & callbacks, const_iterator start, const_iterator end);
	
	bool parse_url(string_view str, url::parser_ptr parser, bool is_connect);
	
	/// Should only be used when parser is not in error state.
	void pause(bool flag);
	
	void reset();
	
	void skip_body() {
	  if (carries(flag::skip_body)) return;
	  add(flag::skip_body);
	}

	bool should_keep_alive();
	
	bool is_final_chunk();
	
	inline uint64 content_length() force_inline { return content_length_; }
	
	inline type kind() force_inline {
		return static_cast<type>(bits_.type);
	}
	
	inline uint8 flags() force_inline {
		return static_cast<uint8>(bits_.flags);
	}
	
	inline bool is_lenient() force_inline {
		return static_cast<bool>(bits_.lenient);
	}
	
	inline uint16 status_code() force_inline {
		return static_cast<uint16>(bits_.status_code);
	}
	
	inline http::method method() force_inline {
		return static_cast<http::method>(bits_.method);
	}
	
	inline perror error() force_inline {
		return static_cast<perror>(bits_.error);
	}
	
	inline bool in(http::method m) {
		return method() == m;
	}
	
	inline bool in(perror e) {
		return error() == e;
	}
	
	inline bool in(type t) {
		return kind() == t;
	}
		
	inline bool is_upgrade() force_inline {
		return static_cast<bool>(bits_.upgrade);
	}
	
private:

	bool needs_eof();
	
	inline pstate state() force_inline {
		return static_cast<pstate>(bits_.state);
	}
	
	inline void state(pstate s) {
		bits_.state = static_cast<uint8>(s);
	}
	
	inline bool in(pstate s) {
		return state() == s;
	}
	
	inline hstate header_state() force_inline {
		return static_cast<hstate>(bits_.header_state);
	}
	
	inline void header_state(hstate s) {
		bits_.header_state = static_cast<uint8>(s);
	}
	
	inline bool in(hstate s) {
		return header_state() == s;
	}
	
	inline void error(perror e) {
		bits_.error = static_cast<uint8>(e);
	}
	
	inline void kind(type t) {
		bits_.type = static_cast<uint8>(t);
	}
	
	inline void method(http::method m) {
		bits_.method = static_cast<uint8>(m);
	}
	
	inline uint8 index() force_inline {
		return bits_.index;
	}
	
	inline void index(uint8 i) {
		bits_.index = static_cast<uint8>(i);
	}
	
	inline void increment_index() {
		++bits_.index;
	}
	
	inline void flags(uint8 set) {
		bits_.flags = set;
	} 
	
	inline void add(flag f) {
		bits_.flags |= static_cast<uint8>(f);
	}
	
	inline bool carries(flag f) {
		return bits_.flags & static_cast<uint8>(f);
	}
	
	inline bool carries(uint8 f) {
		return bits_.flags & f;
	}
	
	inline bool exactly(uint8 f) {
		return (bits_.flags & f) == f;
	}
	
	inline void upgrade(bool set) {
		bits_.upgrade = static_cast<uint8>(set);
	}
	
	inline void content_length(uint64 length) {
		content_length_ = length;
	}
	
	static inline void execute_callback(
		parser * p, parser::callback c, perror e
	) __hot
	{
		assert(p->error() == perror::none);
		if (likely(c)) {
			if (unlikely(!c(p)))
				p->error(e); 
		}
	}
		
	static inline void execute_data_callback(
		parser * p, data_callback c, perror e,
		iterator * mark, const_iterator end
	) __hot
	{
		assert(p->error() == perror::none);
		if (*mark != end) {
			if (likely(c)) {
				if (unlikely(!c(p, *mark, end)))
					p->error(e);
			}
			*mark = end;
		}
	}

	static inline void execute_status_code_callback(
		parser * p, status_code_callback c
	) __hot
	{
		assert(p->error() == perror::none);
		if (likely(c)) {
			if (unlikely(!c(p, p->status_code())))
				p->error(perror::cb_status_code);
		}
	}
	
	static inline perror return_value(
    parser * p, pstate s, perror e
  ) force_inline {
		p->state(s);
		return e;
	}
	
	static inline void count_header_size(parser * p, uint32 v) force_inline {
		p->nread_ += v;
		if (unlikely(p->nread_ > HTTP_MAX_HEADER_SIZE))
			p->error(perror::header_overflow);
	}


	/* ************************************************************************
	 * Members
	 * ************************************************************************/
	struct Bits {
		unsigned int type            : 2; /// enum type
		unsigned int flags           : 8; /// enum flag.
		unsigned int state           : 7; /// enum state.
		unsigned int header_state    : 7; /// enum seader_state.
		unsigned int index           : 7; /// index into current matcher.
		unsigned int lenient         : 1;
		unsigned int status_code     : 16; /// Status code of an HTTP response.
		unsigned int method          : 8;  /// HTTP request method.
		unsigned int error           : 7;  /// enum parsing::error
		/// true = Upgrade header present, false = otherwise
		unsigned int upgrade         : 1;  
	} bits_;
	
	/// Number of bytes read during current operation.
	uint32 nread_; 
	
	/// Body size in bytes or 0 if no Content-Length header.
	uint64 content_length_;
	
	uint16 version_major_;
	uint16 version_minor_;
};


} // namespace http
} // namespace net
} // namespace blink

inline uint8 operator|(
	const blink::net::http::parser::flag f1,
	const blink::net::http::parser::flag f2
) {
	return static_cast<uint8>(f1) | static_cast<uint8>(f2);
}

#endif // _BLINK_HTTP_PARSER_H
