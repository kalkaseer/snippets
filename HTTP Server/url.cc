#include <cassert>
#include "url.h"
#include "ascii_utils.h"

namespace blink {
namespace net {

using namespace encoding::ascii;

#if HTTP_PARSER_STRICT
# define T(v) 0
#else
# define T(v) v
#endif

namespace details {
static constexpr uint8 normal_url_char_table[32] = {
/*   0 nul    1 soh    2 stx    3 etx    4 eot    5 enq    6 ack    7 bel  */
        0    |   0    |   0    |   0    |   0    |   0    |   0    |   0,
/*   8 bs     9 ht    10 nl    11 vt    12 np    13 cr    14 so    15 si   */
        0    | T(2)   |   0    |   0    | T(16)  |   0    |   0    |   0,
/*  16 dle   17 dc1   18 dc2   19 dc3   20 dc4   21 nak   22 syn   23 etb */
        0    |   0    |   0    |   0    |   0    |   0    |   0    |   0,
/*  24 can   25 em    26 sub   27 esc   28 fs    29 gs    30 rs    31 us  */
        0    |   0    |   0    |   0    |   0    |   0    |   0    |   0,
/*  32 sp    33  !    34  "    35  #    36  $    37  %    38  &    39  '  */
        0    |   2    |   4    |   0    |   16   |   32   |   64   |  128,
/*  40  (    41  )    42  *    43  +    44  ,    45  -    46  .    47  /  */
        1    |   2    |   4    |   8    |   16   |   32   |   64   |  128,
/*  48  0    49  1    50  2    51  3    52  4    53  5    54  6    55  7  */
        1    |   2    |   4    |   8    |   16   |   32   |   64   |  128,
/*  56  8    57  9    58  :    59  ;    60  <    61  =    62  >    63  ?  */
        1    |   2    |   4    |   8    |   16   |   32   |   64   |   0,
/*  64  @    65  A    66  B    67  C    68  D    69  E    70  F    71  G  */
        1    |   2    |   4    |   8    |   16   |   32   |   64   |  128,
/*  72  H    73  I    74  J    75  K    76  L    77  M    78  N    79  O  */
        1    |   2    |   4    |   8    |   16   |   32   |   64   |  128,
/*  80  P    81  Q    82  R    83  S    84  T    85  U    86  V    87  W  */
        1    |   2    |   4    |   8    |   16   |   32   |   64   |  128,
/*  88  X    89  Y    90  Z    91  [    92  \    93  ]    94  ^    95  _  */
        1    |   2    |   4    |   8    |   16   |   32   |   64   |  128,
/*  96  `    97  a    98  b    99  c   100  d   101  e   102  f   103  g  */
        1    |   2    |   4    |   8    |   16   |   32   |   64   |  128,
/* 104  h   105  i   106  j   107  k   108  l   109  m   110  n   111  o  */
        1    |   2    |   4    |   8    |   16   |   32   |   64   |  128,
/* 112  p   113  q   114  r   115  s   116  t   117  u   118  v   119  w  */
        1    |   2    |   4    |   8    |   16   |   32   |   64   |  128,
/* 120  x   121  y   122  z   123  {   124  |   125  }   126  ~   127 del */
        1    |   2    |   4    |   8    |   16   |   32   |   64   |   0,
};

#undef T

/* **********************************************
 * Helper functions
 * **********************************************/

static inline bool
is_member(const uint8 * a, unsigned int c) force_inline {
	return !!(a[c >> 3] & (1 << (c & 7)));
}

static inline bool is_mark(char c) force_inline {
	return
		c == '-'  || c == '_' || c == '.' ||
		c == '!'  || c == '~' || c == '*' ||
		c == '\'' || c == '(' || c == ')';
}

static inline bool is_userinfo_char(char c) force_inline {
	return 
		is_alphanum(c) || is_mark(c) ||
		c == '%' || c == ';' || c == ':' || c == '&' || c == '=' ||
		c == '+' || c == '$' || c == ',';
}

#if URL_PARSER_STRICT

static inline bool is_url_char(unsigned char c) force_inline { 
	return is_member(normal_url_char_table, c);
}

static inline bool is_host_char(char c) force_inline {
	return is_alphanum(c) || c == '.' || c == '-';
}

#else

static inline bool is_url_char(unsigned char c) force_inline {
	return (is_member(normal_url_char_table, c) || (c  & 0x80));
}

static inline bool is_host_char(char c) force_inline {
	return is_alphanum(c) || c == '.' || c == '-' || c == '_';
}

#endif // URL_PARSER_STRICT

/* End helper functions ************************/

} // namespace details

using namespace details;


url::parser::parser()
	: field_set_(0),
	  port_(0)
{
	std::memset(data_, 0, sizeof(*data_));
}

void url::parser::reset() {
	std::memset(this, 0, sizeof(*this));
}

enum class host_state : uint8 {
	dead = 1,
	userinfo_start,
	userinfo,
	host_start,
	host_v6_start,
	host,
	host_v6,
	host_v6_end,
	host_v6_zone_start,
	host_v6_zone,
	port_start,
	port
};

url::state url::parser::parse_token(state s, const char c) {
	
	if (c == ' ' || c == '\r' || c == '\n') return state::dead;
	
#if HTTP_PARSER_STRICT
	if (c == '\t' || c == '\f') return state::dead;
#endif
	
	switch (s) {
			
		case state::schema:
			if (is_alpha(c)) return s;
			if (c == ':') return state::schema_slash;
			break;
			
		case state::schema_slash:
			if (c == '/') return state::schema_double_slash;
			break;
			
		case state::schema_double_slash:
			if (c == '/') return state::server_start;
			break;
			
		case state::server_with_at:
			if (c == '@') return state::dead;
			// Fallthrough
			
		case state::server_start:
		case state::server:
			if (c == '/') return state::path;
			if (c == '?') return state::query_start;
			if (c == '@') return state::server_with_at;
			if (is_userinfo_char(c) || c == '[' || c == ']') return state::server;
			break;
			
		case state::path:
			if (is_url_char(c)) return s;
			switch (c) {
				case '?': return state::query_start;
				case '#': return state::fragment_start;
			}
			break;
			
		case state::query_start:
		case state::query:
			if (is_url_char(c)) return state::query;
			switch (c) {
				case '?': return state::query; // allow extra '?' in query
				case '#': return state::fragment_start;
			}
			break;
			
		case state::fragment_start:
			if (is_url_char(c)) return state::fragment;
			switch (c) {
				case '?': return state::fragment;
				case '#': return s;
			}
			break;
			
		case state::fragment:
			if (is_url_char(c)) return s;
			switch (c) {
				case '?': case '#': return s;
			}
			break;
		
		default:
			break;
	}
	
	// Reaching this means we are in error
	return state::dead;
}

host_state parse_host_token(host_state s, const char ch) {
	switch(s) {
		
		case host_state::userinfo_start:
		case host_state::userinfo:
		
			if (ch == '@') return host_state::userinfo_start;
			if (is_userinfo_char(ch)) return host_state::userinfo;
			break;
		
		case host_state::host_start:
		
			if (ch == '[') return host_state::host_v6_start;
			if (is_host_char(ch)) return host_state::host;
			break;
		
		case host_state::host:
		
			if (is_host_char(ch)) return host_state::host;
			// FALL-THROGH
		
		case host_state::host_v6_end:
		
			if (ch == ':') return host_state::port_start;
			break;
		
		case host_state::host_v6:
		
			if (ch == ']') return host_state::host_v6_end;
			// FALL-THROGH
		
		case host_state::host_v6_start:
		
			if (is_hex(ch) || ch == ':' || ch == '.')
				return host_state::host_v6;
			if (s == host_state::host_v6 && ch == '%')
				return host_state::host_v6_zone_start;
			break;
		
		case host_state::host_v6_zone:
		
			if (ch == ']') return host_state::host_v6_end;
			// FALL-THROUGH
		
		case host_state::host_v6_zone_start:
		
			// RFC 6874 Zone ID consists of 1*(unreserved / pct-encoded) */
			if (
				is_alphanum(ch) || 
				ch == '%' || ch == '.' || ch == '-' || ch == '~'
			)
				return host_state::host_v6_zone;
			break;
		
		case host_state::port:
		case host_state::port_start:
		
			if (is_digit(ch)) return host_state::port;
			break;
		
		default:
		
			break;
	}
	return host_state::dead;
}

bool url::parser::parse_host(const char * buf, bool with_info) {
	
	assert(is_set(field::host));
	
	const char *p;
	std::size_t buflen = offset(field::host) + length(field::host);
	length(field::host, 0);
	host_state current_state = with_info ?
		host_state::userinfo_start : host_state::host_start;
	
	for (p = buf + offset(field::host); p < buf + buflen; ++p) {
		
		host_state new_state = parse_host_token(current_state, *p);
		if (new_state == host_state::dead)
			return false;
		
		switch (new_state) {
			
			case host_state::host:
				if (current_state != host_state::host)
					offset(field::host, p - buf);
				++length_ref(field::host);
				break;
			
			case host_state::host_v6:
				if (current_state != host_state::host_v6)
					offset(field::host, p - buf);
				++length_ref(field::host);
				break;
			
			case host_state::host_v6_zone_start:
			case host_state::host_v6_zone:
				++length_ref(field::host);
				break;
			
			case host_state::port:
				if (current_state != host_state::port) {
					offset(field::port, p - buf);
					length(field::port, 0);
					set(field::port);
				}
				++length_ref(field::port);
				break;
			
			case host_state::userinfo:
				if (current_state != host_state::userinfo) {
					offset(field::userinfo, p - buf);
					length(field::userinfo, 0);
					set(field::userinfo);
				}
				++length_ref(field::userinfo);
				break;
			
			default:
				break;
		}
		current_state = new_state;
	}
	
	// Ensure that we don't end somewhere unexpected.
	switch (current_state) {
		case host_state::host_start:
		case host_state::host_v6_start:
		case host_state::host_v6:
		case host_state::host_v6_zone_start:
		case host_state::host_v6_zone:
		case host_state::userinfo:
		case host_state::userinfo_start:
			return false;
		default:
			break;
	}
	
	return 0;
}

bool url::parser::parse(string_view str, state state) {
	
	field current_field;
	field old_field = field::max;
	bool with_info = false;
	port_ = field_set_ = 0;
	
	for (
		string_view::iterator iter = str.cbegin();
		iter != str.cend();
		++iter
	) {
		state = parse_token(state, *iter);
		switch (state) {
			
			case state::dead:
				return false;
			
			case state::schema_slash:
			case state::schema_double_slash:
			case state::server_start:
			case state::query_start:
			case state::fragment_start:
				continue;
			
			case state::schema:
				current_field = field::schema;
				break;
			
			case state::server_with_at:
				with_info = true;
				// Fall-Through
			
			case state::server:
				current_field = field::host;
				break;
			
			case state::path:
				current_field = field::path;
				break;
			
			case state::query:
				current_field = field::query;
				break;
			
			case state::fragment:
				current_field = field::fragment;
				break;
			
			default:
				assert(!"Unexpected state");
				return false;
		}
		
		if (current_field == old_field) {
			++length_ref(current_field);
			continue;
		}
		
		offset(current_field, iter - str.data());
		length(current_field, 1);
		set(current_field);
		
		old_field = current_field;
	}
	
	// Host must be present if there is a schema.
	// http:///example will fail
	if (!(is_set(field::schema) && is_set(field::host)))
		return false;
	
	if (is_set(field::host) && !parse_host(str.data(), with_info))
		return false;
	
	if (is_set(field::port)) {
//		int saved = errno;
		errno = 0;
		char * p;
		unsigned long v = std::strtoul(str.data() + offset(field::port), &p, 10);
		if (p == str.data()) // 
		// No need to check (v == ULONG_MAX && errno == ERANGE) because we
		// are capping the range to 2^16 anyway.
		if (v > 0xffff)
			return false;
		port_ = static_cast<uint16>(v);
	}
	return true;
}

} // namespace net
} // namespace blink
