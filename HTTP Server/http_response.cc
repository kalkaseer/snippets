#include "http_response.h"

namespace blink {
namespace net {
namespace http {
	
namespace status_lines {

const std::string ok = "HTTP/1.0 200 OK\r\n";
const std::string created = "HTTP/1.0 201 Created\r\n";
const std::string accepted = "HTTP/1.0 202 Accepted\r\n";
const std::string no_content = "HTTP/1.0 204 No Content\r\n";
const std::string multiple_choices = "HTTP/1.0 300 Multiple Choices\r\n";
const std::string moved_permenantly = "HTTP/1.0 301 Moved Permenantly\r\n";
const std::string moved_temporarily = "HTTP/1.0 302 Moved Temporarily\r\n";
const std::string not_modified = "HTTP/1.0 304 Not Modified\r\n";
const std::string bad_request = "HTTP/1.0 400 Bad Request\r\n";
const std::string unauthorized = "HTTP/1.0 401 Anauthorized\r\n";
const std::string forbidden = "HTTP/1.0 403 Forbidden\r\n";
const std::string not_found = "HTTP/1.0 404 Not Found\r\n";
const std::string internal_server_error = "HTTP/1.0 500 Internal Server Error\r\n";
const std::string not_implemented = "HTTP/1.0 501 Not Implemented\r\n";
const std::string bad_gateway = "HTTP/1.0 502 Bad Gateway\r\n";
const std::string service_unavailable = "HTTP/1.0 503 Service Unavailable\r\n";

asio::const_buffer to_buffer(response::status_code status) {
	
	switch (status) {
		case response::ok: return asio::buffer(ok);
		case response::created: return asio::buffer(created);
		case response::accepted: return asio::buffer(accepted);
		case response::no_content: return asio::buffer(no_content);
		case response::multiple_choices: return asio::buffer(multiple_choices);
		case response::moved_permenantly: return asio::buffer(moved_permenantly);
		case response::moved_temporarily: return asio::buffer(moved_temporarily);
		case response::not_modified: return asio::buffer(not_modified);
		case response::bad_request: return asio::buffer(bad_request);
		case response::unauthorized: return asio::buffer(unauthorized);
		case response::forbidden: return asio::buffer(forbidden);
		case response::not_found: return asio::buffer(not_found);
		case response::internal_server_error: return asio::buffer(internal_server_error);
		case response::not_implemented: return asio::buffer(not_implemented);
		case response::bad_gateway: return asio::buffer(bad_gateway);
		case response::service_unavailable: return asio::buffer(service_unavailable);
		default: return asio::buffer(internal_server_error);
	}
}
	
} // namespace status_lines

namespace strings {

const char name_value_separator[] = { ':', ' '};
const char crlf[] = { '\r', '\n' };

} // namespace strings

void response::to_buffers(std::vector<asio::const_buffer> & bufs) {
	bufs.push_back(status_lines::to_buffer(status()));
	for (auto & h : headers()) {
		bufs.push_back(asio::buffer(h.name()));
		bufs.push_back(asio::buffer(strings::name_value_separator));
		bufs.push_back(asio::buffer(h.value()));
		bufs.push_back(asio::buffer(strings::crlf));
	}
	bufs.push_back(asio::buffer(strings::crlf));
	bufs.push_back(asio::buffer(content()));
}

namespace default_responses {

const char ok[] = "";
const char created[] = 
	"<html>"
	"<head><title>Created</title></head>"
	"<body><h1>201 Created</h1></body>"
	"</html>";
const char accepted[] = 
	"<html>"
	"<head><title>Accepted</title></head>"
	"<body><h1>202 Accepted</h1></body>"
	"</html>";
const char no_content[] = 
	"<html>"
	"<head><title>No Content</title></head>"
	"<body><h1>204 No Content</h1></body>"
	"</html>";
const char multiple_choices[] = 
	"<html>"
	"<head><title>Multiple Choices</title></head>"
	"<body><h1>300 Multiple Choices</h1></body>"
	"</html>";
const char moved_permenantly[] = 
	"<html>"
	"<head><title>Moved Permenantly</title></head>"
	"<body><h1>301 Moved Permenantly</h1></body>"
	"</html>";
const char moved_temporarily[] = 
	"<html>"
	"<head><title>Moved Temporarily</title></head>"
	"<body><h1>302 Moved Temporarily</h1></body>"
	"</html>";
const char not_modified[] = 
	"<html>"
	"<head><title>Not Modified</title></head>"
	"<body><h1>304 Not Modified</h1></body>"
	"</html>";
const char bad_request[] =
	"<html>"
	"<head><title>Bad Request</title></head>"
	"<body><h1>400 Bad Request</h1></body>"
	"</html>";
const char unauthorized[] = 
	"<html>"
	"<head><title>Unauthorized</title></head>"
	"<body><h1>401 Unauthorized</h1></body>"
	"</html>";
const char forbidden[] = 
	"<html>"
	"<head><title>Forbidden</title></head>"
	"<body><h1>403 Forbidden</h1></body>"
	"</html>";
const char not_found[] = 
	"<html>"
	"<head><title>Not Found</title></head>"
	"<body><h1>404 Not Found</h1></body>"
	"</html>";
const char internal_server_error[] = 
	"<html>"
	"<head><title>Internal Server Error</title></head>"
	"<body><h1>500 Internal Server Error</h1></body>"
	"</html>";
const char not_implemented[] = 
	"<html>"
	"<head><title>Not Implemented</title></head>"
	"<body><h1>501 Not Implemented</h1></body>"
	"</html>";
const char bad_gateway[] = 
	"<html>"
	"<head><title>Bad Gateway</title></head>"
	"<body><h1>502 Bad Gateway</h1></body>"
	"</html>";
const char service_unavailable[] = 
	"<html>"
	"<head><title>Service Unavailable</title></head>"
	"<body><h1>503 Service Unavailable</h1></body>"
	"</html>";

std::string to_string(response::status_code status) {
	switch (status) {
		case response::ok: return ok;
		case response::created: return created;
		case response::accepted: return accepted;
		case response::no_content: return no_content;
		case response::multiple_choices: return multiple_choices;
		case response::moved_permenantly: return moved_permenantly;
		case response::moved_temporarily: return moved_temporarily;
		case response::not_modified: return not_modified;
		case response::bad_request: return bad_request;
		case response::unauthorized: return unauthorized;
		case response::forbidden: return forbidden;
		case response::not_found: return not_found;
		case response::internal_server_error: return internal_server_error;
		case response::not_implemented: return not_implemented;
		case response::bad_gateway: return bad_gateway;
		case response::service_unavailable: return service_unavailable;
		default: return internal_server_error;
  }
}

} // namespace default_responses

void response::make(response & res, response::status_code status) {
	res.status() = status;
	res.content(default_responses::to_string(status));
	res.headers().emplace_back(
    "Content-Length", std::to_string(res.content().size()));
  res.headers().emplace_back("Content-Type", "text/html");
}

void response::make(
  response & res, response::status_code status, const std::string & mimetype
) {
  res.status() = status;
  res.content(default_responses::to_string(status));
  res.headers().emplace_back(
    "Content-Length", std::to_string(res.content().size()));
  res.headers().emplace_back("Content-Type", mimetype);
}

void response::make(
  response & res, response::status_code status, std::string && mimetype
) {
  res.status() = status;
  res.content(default_responses::to_string(status));
  res.headers().emplace_back(
    "Content-Length", std::to_string(res.content().size()));
  res.headers().emplace_back(
    "Content-Type", std::forward<std::string>(mimetype));
}
	
} // namespace http
} // namespace net
} // namespace blink
