#ifndef _BLINK_URL_H
#define _BLINK_URL_H

#include <memory>
#include <string>
#include <stdexcept>
#include <experimental/string_view>
#include "basic_types.h"
#include "compiler.h"

namespace blink {
namespace net {

/***************************************************
 * Class: url_exception
 ***************************************************/
class url_exception
	: public std::runtime_error
{
public:

	url_exception(const std::string & what)
		: std::runtime_error(what)
	{ }
	
	url_exception(const char * what)
		: std::runtime_error(what)
	{ }
};


/***************************************************
 * Class: url
 ***************************************************/
struct url {
	
	using string_view = std::experimental::string_view;
	
	/***************************************************
	 * Enum class: url::field
	 ***************************************************/
	enum class field : uint8 {
		schema = 0,
		host,
		port,
		path,
		query,
		fragment,
		userinfo,
		max
	};
	
	/***************************************************
	 * Enum class: url::state
	 ***************************************************/
	enum class state : uint8 {
		dead = 1,
		schema,
		schema_slash,
		schema_double_slash,
		server_start,
		server,
		server_with_at,
		path,
		query_start,
		query,
		fragment_start,
		fragment,
	};

	/***************************************************
	 * Inner class: url::parser
	 ***************************************************/
	class parser {
	
	public:
	
	  parser();
	
		bool parse(const string_view s, state state = state::server_start);
		
		static state parse_token(state s, const char c);

		void reset();
		
		inline uint16 field_set() {
			return field_set_;
		}
		
		inline bool is_set(uint16 fields) {
			return field_set_ & fields;
		}
		
		inline bool is_set(field field) {
			return field_set_ & (1 << static_cast<uint16>(field));
		}
		
		inline uint16 port() {
			return port_;
		}
		
		inline uint16 offset(field field) {
			return data_[static_cast<uint8>(field)].offset;
		}
		
		inline uint16 length(field field) {
			return data_[static_cast<uint8>(field)].length;
		}
		
	protected:
		
		inline uint16 & offset_ref(field field) {
			return data_[static_cast<uint8>(field)].offset;
		}
		
		inline uint16 & length_ref(field field) {
			return data_[static_cast<uint8>(field)].length;
		}
		
		inline void offset(field field, uint16 off) {
			data_[static_cast<uint8>(field)].offset = off;
		}
		
		inline void length(field field, uint16 len) {
			data_[static_cast<uint8>(field)].length = len;
		}
		
		inline void set(field field) {
			field_set_ |= (1 << static_cast<uint16>(field));
		}
		
	private:
		
		bool parse_host(const char * buf, bool with_info);
		
		
		uint16 field_set_; /// Bitmask of (1 << field::value).
		uint16 port_;      /// Converted field::port string.
		
		struct {
			uint16 offset; /// Offset into buffer in which field starts.
			uint16 length; /// Length of run in buffer.
		} data_[static_cast<uint8>(field::max)];
	};

	typedef std::shared_ptr<parser> parser_ptr;
	
	/* *** End inner class url::parser ******************************/
	
	url(const std::string & s)
		: parser_(),
		  data_(s)
	{
		if (!parser_.parse(data_))
			throw url_exception("couldn't parse: " + data_.to_string());
	}
	
	url(std::string && s)
		: parser_(),
		  data_(std::move(s))
	{
		if (!parser_.parse(data_))
			throw url_exception("couldn't parse: " + data_.to_string());
	}
	
	inline bool parse(const string_view s, state state = state::server_start) {
		return parser_.parse(s, state);
	}
		
	inline static state parse_token(state s, const char c) {
		return parser::parse_token(s, c);
	}
	
	inline std::string data() { return data_.to_string(); }
	
	inline const string_view host() {
		if (parser_.is_set(field::host))
			return string_view(
				data_.data() + parser_.offset(field::host),
				parser_.length(field::host));
		return string_view("");
	}
	
private:
	
	parser parser_;
	
	string_view data_;
};

typedef std::shared_ptr<url> url_ptr;

} // namespace net
} // namespace blink

uint8 inline operator|(
	const blink::net::url::field f1, const blink::net::url::field f2
) {
	return static_cast<uint8>(f1) | static_cast<uint8>(f2);
}

#endif // _BLINK_URL_H
