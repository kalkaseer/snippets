#ifndef _HTTP_CONSTANTS_H
#define _HTTP_CONSTANTS_H

#include "str_const.h"

namespace blink {
namespace net {
namespace http {

namespace constants {
  
  static constexpr char cr = '\r';
  static constexpr char lf = '\n';
  
  static constexpr str_const chunked = "chunked";
  static constexpr str_const keep_alive = "keep-alive";
  static constexpr str_const close = "close";
	static constexpr str_const upgrade = "upgrade";
  
} // namespace constants

#ifndef HTTP_INCLUDE_HEADERS
# define HTTP_INCLUDE_HEADERS 1
#endif

#if HTTP_INCLUDE_HEADERS
namespace headers {
  
  static constexpr str_const accept = "Accept";
  static constexpr str_const accept_charset = "Accept-Charset";
  static constexpr str_const accept_features = "Accept-Features";
  static constexpr str_const accept_encoding = "Accept-Encoding";
  static constexpr str_const accept_language = "Accept-Language";
  static constexpr str_const accept_range = "Accept-Range";
  static constexpr str_const access_control_allow_credentials = "Access-Control-Allow-Creditionals";
  static constexpr str_const access_control_allow_origin = "Access-Control-Allow-Origin";
  static constexpr str_const access_control_allow_methods = "Access-Control-Allow-Methods";
  static constexpr str_const access_control_allow_headers = "Access-Control-Allow-Headers";
  static constexpr str_const access_control_max_age = "Access-Control-Max-Age";
  static constexpr str_const access_control_expose_headers = "Access-Control-Expose-Headers";
  static constexpr str_const access_control_request_method = "Access-Control-Request-Method";
  static constexpr str_const access_control_request_headers = "Access-Control-Request-Headers";
  
  static constexpr str_const age = "Age";
  static constexpr str_const allow = "Allows";
  static constexpr str_const alterantives = "Alternatives";
  static constexpr str_const authrization = "Authorization";
  static constexpr str_const cache_control = "Cache-Control";
  static constexpr str_const connection = "Connection";
  static constexpr str_const proxy_connection = "proxy-connection";
  static constexpr str_const content_encoding = "Content-Encoding";
  static constexpr str_const content_language = "Content-Language";
  static constexpr str_const content_length = "Content-Length";
  static constexpr str_const content_location = "Content-Location";
  static constexpr str_const content_md5 = "Content-MD5";
  static constexpr str_const content_range = "Content-Range";
  static constexpr str_const content_security_policy = "Content-Security-Policy";
  static constexpr str_const content_type = "Content-Type";
  static constexpr str_const cookie = "Cookie";
  static constexpr str_const dnt = "DNT";
  static constexpr str_const date = "Date";
  static constexpr str_const etag = "ETag";
  static constexpr str_const expect = "Expect";
  static constexpr str_const expires = "Expires";
  static constexpr str_const from = "From";
  static constexpr str_const if_match = "If-Match";
  static constexpr str_const if_modified_since = "If-Modified-Since";
  static constexpr str_const if_none_match = "If-None-Match";
  static constexpr str_const if_range = "If-Range";
  static constexpr str_const if_unmodified_since = "If-Unmodified-Since";
  static constexpr str_const last_event_id = "Last-Event-ID";
  static constexpr str_const last_modified = "Last-Modified";
  static constexpr str_const link = "Link";
  static constexpr str_const location = "Location";
  static constexpr str_const max_forwards = "Max-Forwards";
  static constexpr str_const negotiate = "Negotiate";
  static constexpr str_const origin = "Origin";
  static constexpr str_const pragma = "Pragma";
  static constexpr str_const proxy_authenticate = "Proxy-Authenticate";
  static constexpr str_const proxy_authorization = "Proxy-Authrization";
  static constexpr str_const range = "Range";
  static constexpr str_const referer = "Referer";
  static constexpr str_const retry_after = "Retry-After";
  static constexpr str_const sec_websocket_extentions = "Sec-Websocket-Extensions";
  static constexpr str_const sec_websocket_origin = "Sec-Websocket-Origin";
  static constexpr str_const sec_websocket_protocol = "Sec-Websocket-Protocol";
  static constexpr str_const sec_websocket_version = "Sec-Websocket-Version";
  static constexpr str_const server = "Server";
  static constexpr str_const set_cookie = "Set-Cookie";
  static constexpr str_const strict_transport_security = "Strict-Transport-Security";
  static constexpr str_const tnc = "TNC";
  static constexpr str_const te = "TE";
  static constexpr str_const trailer = "Trailer";
  static constexpr str_const transfer_encoding = "Transfer-Encoding";
  static constexpr str_const upgrade = "Upgrade";
  static constexpr str_const user_agent = "User-Agent";
  static constexpr str_const variant_vary = "Variant-Vary";
  static constexpr str_const vary = "Vary";
  static constexpr str_const via = "Via";
  static constexpr str_const warning = "Warning";
  static constexpr str_const www_authenticate = "WWW-Authenticate";
  static constexpr str_const x_content_duration = "X-Content-Duration";
  static constexpr str_const x_content_security_policy = "X-Content-Securty-Policy";
  static constexpr str_const x_dns_prefetch_control = "X-DNSPrefetch-Control";
  static constexpr str_const x_frame_option = "X-Frame-Options";
  static constexpr str_const x_requested_with = "X-Requested-With";
  
} // namespace headers
#endif // HTTP_INCLUDE_HEADERS

} // namespace net
} // namespace http
} // namespace blink

#endif // _HTTP_CONSTANTS_H