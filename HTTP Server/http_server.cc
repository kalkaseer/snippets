#include "http_server.h"

#include <asio/spawn.hpp>
#include "http_connection.h"

namespace blink {
namespace net {
namespace http {

server::server(
  const std::string & address,
  const std::string & port,
  const std::string & doc_root
) : iocontext(),
    signals(iocontext),
    acceptor(iocontext),
    conn_manager(iocontext),
    socket(iocontext),
    req_handler(doc_root)
{
  init(address, port);
}
  
server::server(
  std::string && address, std::string && port, std::string && doc_root
) : iocontext(),
    signals(iocontext),
    acceptor(iocontext),
    conn_manager(iocontext),
    socket(iocontext),
    req_handler(std::move(doc_root))
{
  init(address, port);
}

void server::init(const std::string & address, const std::string & port) {

  // Register to handle signals that indicate the server should exit.
  // It is safe to register for the same signal multiple times in a program,
  // provided all rgistrations for the specified signal is made through Asio.
  signals.add(SIGINT);
  signals.add(SIGTERM);
#if defined(SIGQUIT)
  signals.add(SIGQUIT);
#endif // defined(SIGQUIT)
  
  spawn(
    iocontext,
    [this](asio::yield_context yield) { await_stop(); });
  
  spawn(
    iocontext,
    [this, &address, &port](asio::yield_context yield) {
      // Open the acceptor with the option to reuse the address.
      asio::ip::tcp::resolver resolver{ iocontext };
      asio::ip::tcp::endpoint endpoint = *resolver.resolve({address, port});
      acceptor.open(endpoint.protocol());
      acceptor.set_option(asio::ip::tcp::acceptor::reuse_address(true));
      acceptor.bind(endpoint);
      acceptor.listen();
      
      accept();
    });
}
 
void server::run() {
	
	// The IO service run() call will always block until all asynchronous
	// operations have finished. While the server is running, there is always at
	// least one asynchronous operation outstanding: the asynchronous accept call
	// waiting for new incoming connections.
	iocontext.run();
}

void server::accept() {
  spawn(
    iocontext,
    [this](asio::yield_context yield) {
      asio::ip::tcp::socket socket{ iocontext }; // TODO: check
        std::error_code ec;
        this->acceptor.async_accept(socket, yield[ec]);
        if (!this->acceptor.is_open())
          return;

        if (!ec)
          conn_manager.start(std::make_shared<connection>(
            std::move(socket), conn_manager, req_handler));

        accept();
    });
}

void server::await_stop() {
  spawn(
  iocontext,
  [this](asio::yield_context yield){
    this->signals.async_wait(yield);
    // The server is stopped by cancelling all outstanding asynchronous
    // operations. Once all operations have finihsed, the io_context::run()
    // call will exit.
    this->acceptor.close();
    this->conn_manager.stop_all();
  });
}

} // namespace http
} // namespace net
} // namespace blink
