#ifndef _BLINK_SERVER_H
#define _BLINK_SERVER_H

#include <asio.hpp>
#include "connection_manager.h"
#include "http_request_handler.h"

namespace blink {
namespace net {
namespace http {

/// HTTP server
class server {

public:
	
	server(const server & ) = delete;
	server& operator=(const server &) = delete;
	
	/// Construct a server to listen on the specified TCP address and port,
	/// and serve up files from the given directory.
	explicit server(
		const std::string & address,
		const std::string & port,
		const std::string & doc_root);
	
	/// Construct a server to listen on the specified TCP address and port,
	/// and serve up files from the given directory.
	explicit server(
		std::string && address, std::string && port, std::string && doc_root);
	
	/// Run the server and start IO operations.
	void run();
	
private:
  
  /// Common initialisation.
  void init(const std::string & address, const std::string & port);
	
	/// Perform an asynchronous accept operation.
	void accept();
	
	/// Wait for a request to stop the server.
	void await_stop();
	
	/// The IO context used to perform asynchronous operations.
	asio::io_context iocontext;
	
	/// The signal set to register for process termination notifications.
	asio::signal_set signals;
	
	/// Acceptor for listing for incoming connections.
	asio::ip::tcp::acceptor acceptor;
	
	/// Connection manager which owns all live connections.
	connection_manager conn_manager;
	
	/// The next socket to be accepted.
	asio::ip::tcp::socket socket;
	
	/// Handler for all incoming requests.
	request_handler req_handler;

	/// Parser for all incoming requests.
	request_parser req_parser;
};

} // namespace http
} // namespace net
} // namespace blink

#endif // _BLINK_SERVER_H
