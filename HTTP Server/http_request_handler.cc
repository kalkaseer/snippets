#include <fstream>
#include <sstream>
#include <string>
#include <algorithm>

#include "http_request.h"
#include "http_request_handler.h"
#include "http_response.h"
#include "mimetypes.h"
#include "url.h"

namespace blink {
namespace net {
namespace http {

request_handler::request_handler(const std::string & doc_dir)
	: doc_root(doc_dir)
{ }

request_handler::request_handler(std::string && doc_dir)
	: doc_root(std::move(doc_dir))
{ }
  

void request_handler::handle(const request & req, response & res) {
	
	// Decode URL to path.
	std::string request_path;
	if (!url_decode(req.uri(), request_path)) {
		response::make(res, response::bad_request);
		return;
	}

	// Request path must be absolute and not contain "..".
	if (
		request_path.empty() ||
		request_path[0] != '/' ||
		request_path.find("..") != std::string::npos
	) {
		response::make(res, response::bad_request);
		return;
	}

	// If path ends in a slash (i.e. is a directory) then add "index.html"
	if (request_path[request_path.size() - 1] == '/')
		request_path += "index.html";

	// Determine the file extension
	std::size_t last_slash_pos = request_path.find_last_of("/");
	std::size_t last_dot_pos = request_path.find_last_of(".");
	std::string extension;
	if (last_dot_pos != std::string::npos && last_dot_pos > last_slash_pos)
		extension = request_path.substr(last_dot_pos + 1);

	// Open the file to send back
	std::string full_path = doc_root + request_path;
	std::ifstream is{ full_path.c_str(), std::ios::in | std::ios::binary };
	if (!is) {
		response::make(res, response::not_found);
		return;
	}

	// Fill out the reply to be sent to the client
	res.status() = response::ok;
	char buf[512];
	while (is.read(buf, sizeof(buf)).gcount() > 0)
		res.content().append(buf, is.gcount());
	res.headers().emplace_back(
    "Content-Length", std::to_string(res.content().size()));
	res.headers().emplace_back(
    "Content-Type", mimetypes::extension_to_type(extension));
}

bool request_handler::url_decode(const std::string & in, std::string & out) {
	
	out.clear();
	out.reserve(in.size());
	
	for (std::size_t i = 0; i < in.size(); ++i) {
		if (in[i] == '%') {
			if (i + 3 <= in.size()) {
				int value = 0;
				std::istringstream is(in.substr(i + 1, 2));
				if (is >> std::hex >> value) {
					out += static_cast<char>(value);
					i += 2;
				} else {
					return false;
				}
			} else {
				return false;
			}
		} else if (in[i] == '+') {
			out += ' ';
		} else {
			out += in[i];
		}
	}
	
	return true;
}

} // namespace http
} // namespace net
} // namespace blink
