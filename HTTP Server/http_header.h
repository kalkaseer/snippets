#ifndef BLINK_HEADER_H_
#define BLINK_HEADER_H_

#include <string>
#include <memory>
#include <string>

namespace blink {
namespace net {
namespace http {
	
class header {
  
public:
  
  typedef std::string value_type;
  
public:
  
  explicit header() { }
  
  explicit header(const header & rhs)
    : name_(rhs.name_),
      value_(rhs.value_)
  { }
  
  explicit header(header && rhs)
    : name_(std::move(rhs.name_)),
      value_(std::move(rhs.value_))
  { }
  
  explicit header(const std::string & name, const std::string & value)
    : name_(name),
      value_(value)
  { }
  
  explicit header(std::string && name, const std::string && value)
    : name_(std::move(name)),
      value_(std::move(value))
  { }
  
  inline header & operator=(const header & rhs) {
    name_  = rhs.name_;
    value_ = rhs.value_;
		return *this;
  }
  
  inline header & operator=(header && rhs) {
    name_  = std::move(rhs.name_);
    value_ = std::move(rhs.value_);
		return *this;
  }
  
  inline bool operator==(const header & rhs) {
    return name_ == rhs.name_ && value_ == rhs.value_;
  }
  
  inline bool operator!=(const header & rhs) {
    return !operator==(rhs);
  }
  
  inline bool is_same(const header & rhs) {
    return name_ == rhs.name_;
  }
  
  inline const value_type & name() const { return name_; }
  
  inline value_type & name() { return name_; }

  inline void name(value_type && n) { name_ = n; }
  
  inline void name(const value_type & n) { name_ = n; }

  inline const value_type & value() const { return value_; }
  
  inline value_type & value() { return value_; }

  inline void value(const value_type & v) { value_ = v; }

  inline void value(value_type && v) { value_ = v; }
	
	
private:
	
	value_type name_;
  
  value_type value_;
};
	
} // namespace http
} // namesoace net
} // namespace blink

#endif //BLINK_HEADER_H_
