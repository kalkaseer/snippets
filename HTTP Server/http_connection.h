#ifndef _BLINK_CONNECTION_H
#define _BLINK_CONNECTION_H

#include <array>
#include <memory>
#include <asio.hpp>
#include <asio/spawn.hpp>

#include "http_request.h"
#include "http_request_handler.h"
#include "http_request_parser.h"
#include "http_response.h"

namespace blink {
namespace net {
namespace http {

class connection_manager;

/// A single connection from a client
class connection
	: public std::enable_shared_from_this<connection>
{
public:
	connection(const connection &) = delete;
	connection & operator=(const connection &) = delete;
	
	/// Construct a connection with a given socket.
	explicit connection(
		asio::ip::tcp::socket && socket,
		connection_manager & manager,
		request_handler & handler);
		
	/// Start the first asynchronous operation for the connection.
  void start(asio::yield_context yield);
	
	/// Stop all asynchronous operations associated with the connection.
	void stop();
	
private:
	
	/// Perform an asynchronous read operation.
  void read(asio::yield_context yield);
	
	/// Perform an asynchronous write operation.
  void write(asio::yield_context yield);
	
	/// Socket associated with the connection.
	asio::ip::tcp::socket socket;
	
	/// Manager for this connection.
	connection_manager & manager;
	
	/// Request handler associated with this connection;
	request_handler & req_handler;
	
	/// Buffer for incoming data.
	std::array<char, 8192> buffer;
	
	/// Incoming request.
	request req;
	
	/// Request parser for the incoming request.
	request_parser req_parser;
	
	/// Response to be sent to the client.
	response res;
};

typedef std::shared_ptr<connection> connection_ptr;

} // namespace http
} // namespace net
} // namespace blink

#endif // _BLINK_CONNECTION_H
