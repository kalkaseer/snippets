#ifndef _BLINK_STR_CONST_H
#define _BLINK_STR_CONST_H

#include <cstddef>
#include <iostream>
#include <stdexcept>
#include <string>
#include <cstring>

namespace blink {
	
class str_const {
	
private:
	const char * const p;
	const std::size_t  s;

public:
	template <std::size_t N>
	constexpr str_const(const char (&a)[N])
		:p(a), s(N-1)
	{
    static_assert(N >= 0, "Invalid literal string, size is zero.");
  }
	
	constexpr char operator[](std::size_t n) const {
		return n < s ? p[n] : throw std::out_of_range("index out of range");
	}
	
	constexpr std::size_t size() const { return s; }
	
	constexpr const char * const data() const { return p; }
};

inline std::ostream & operator<<(std::ostream & s, const str_const & sc) {
	s.write(sc.data(), sc.size());
	return s;
}

inline bool operator==(const str_const & sc, const std::string & str) {
	if (sc.size() == str.size())
		return std::strncmp(sc.data(), str.data(), sc.size()) == 0;
	return false;
}

inline bool operator==(const std::string & str, const str_const & sc) {
	return sc == str;
}

} // namespace blink

#endif // _BLINK_STR_CONST_H