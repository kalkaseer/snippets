#ifndef _BLINK_HTTP_METHOD_H
#define _BLINK_HTTP_METHOD_H

#include "str_const.h"
#include "utility_macros.h"
#include "basic_types.h"

namespace blink {
namespace net {
namespace http {
	
enum class method : uint8 {
	del = 0,
	get,
	head,
	post,
	put,
	// pathological
	connect,
	options,
	trace,
	// WebDAV
	copy,
	lock,
	mkcol,
	move,
	propfind,
	proppatch,
	search,
	unlock,
	bind,
	rebind,
	unbind,
	acl,
	// Subversion
	report,
	mkactivity,
	checkout,
	merge,
	// upnp
	msearch,
	notify,
	subscribe,
	unsubscribe,
	// RFC-5789
	patch,
	purge,
	// CalDAV
	mkcalendar,
	// RFC-2068
	link,
	unlink,
};

static constexpr str_const unknown_method{ "unknown" };

namespace details {
static constexpr str_const method_names[33] = {
	"DELETE",
	"GET",
	"HEAD",
	"POST",
	"PUT",
	"CONNECT",
	"OPTIONS",
	"TRACE",
	"COPY",
	"LOCK",
	"MKCOL",
	"MOVE",
	"PROPFIND",
	"PROPMATCH",
	"SEARCH",
	"UNLOCK",
	"BIND",
	"REBIND",
	"UNBIND",
	"ACL",
	"REPORT",
	"MKACTIVITY",
	"CHECKOUT",
	"MERGE",
	"M-SEARCH",
	"NOTIFY",
	"SUBSCRIBE",
	"UNSUBSCRIBE",
	"PATCH",
	"PURGE",
	"MKCALENDAR",
	"LINK",
	"UNLINK"
};
} // namespace details

constexpr const str_const & to_string(method method) {
	return ELEM_AT(details::method_names, method, unknown_method);
}

} // namespace http
} // namespace net
} // namespace blink

#endif // _BLINK_HTTP_METHOD_H
