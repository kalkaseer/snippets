#ifndef _BLINK_CONNECTION_MANAGER_H
#define _BLINK_CONNECTION_MANAGER_H

#include <unordered_set>
#include <asio.hpp>

#include "http_connection.h"

namespace blink {
namespace net {
namespace http {

/// Manages open connections
class connection_manager {
	
public:
	
	connection_manager(const connection_manager &) = delete;
	connection_manager & operator=(const connection_manager &) = delete;
	
	/// Construct a connection manager.
  connection_manager(asio::io_context & context);
	
	/// Add the given connection to the manager and start it.
	void start(connection_ptr c);
	
	/// Stop the given connection.
	void stop(connection_ptr c);
	
	/// Stop all connections.
	void stop_all();
	
private:
	
	/// Managed connections.
	std::unordered_set<connection_ptr> connections;
	
	/// Backend IO context
	asio::io_context & iocontext;
};

} // namespace http
} // namespace net
} // namespace blink

#endif // _BLINK_CONNECTION_MANAGER_H
