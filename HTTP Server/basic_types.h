#ifndef BLINK_BASICTYPES_H_
#define BLINK_BASICTYPES_H_

#include <stddef.h>  /* for NULL, size_t */

#if !(defined(_MSC_VER) && (_MSC_VER < 1600))
#include <stdint.h>  /* for uintptr_t */
#endif

#define HAS_INT64  /* For now it is always defined */

#if !defined(_MSC_VER)

#include <stdint.h>

typedef int8_t              int8;
typedef int16_t             int16;
typedef int32_t             int32;
typedef uint8_t             uint8;
typedef uint16_t            uint16;
typedef uint32_t            uint32;

# if defined(OSX)
   typedef uint64_t         uint64;
   typedef int64_t          int64;
#  define HAS_INT64
#  ifndef INT64_C
#   define INT64_C(x) x ## LL
#  endif
#  ifndef UINT64_C
#   define UINT64_C(x) x ## ULL
#  endif
#  define INT64_F "l"

# elif defined(__LP64__)
   typedef unsigned long    uint64;  // NOLINT
   typedef long             int64;  // NOLINT
#  define HAS_INT64
#  ifndef INT64_C
#   define INT64_C(x) x ## L
#  endif
#  ifndef UINT64_C
#   define UINT64_C(x) x ## UL
#  endif
#  define INT64_F "l"

# else
   typedef uint64_t         uint64;
   typedef int64_t          int64;
#  define HAS_INT64
#  ifndef INT64_C
#   define INT64_C(x) x ## LL
#  endif
#  ifndef UINT64_C
#   define UINT64_C(x) x ## ULL
#  endif
#  define INT64_F "ll"

# endif


#else /* _MSC_VER */

/* Define C99 equivalent types, since MSVC doesn't provide stdint.h. */
typedef signed char         int8;
typedef signed short        int16;
typedef signed int          int32;
typedef __int64             int64;
typedef unsigned char       uint8;
typedef unsigned short      uint16;
typedef unsigned int        uint32;
typedef unsigned __int64    uint64;

#define HAS_INT64

#ifndef INT64_C
# define INT64_C(x) x ## I64
#endif
#ifndef UINT64_C
# define UINT64_C(x) x ## UI64
#endif
#define INT64_F "I64"

#endif /* !_MSC_VER */

#if defined(__GNUC__)
  typedef __int128 int128;
  typedef unsigned __int128 uint128;
#endif

#endif /* BLINK_BASICTYPES_H_ */
