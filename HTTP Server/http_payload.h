#ifndef _BLINK_HTTP_PAYLOAD_H
#define _BLINK_HTTP_PAYLOAD_H

#include <vector>
#include <string>
#include "compiler.h"
#include "http_header.h"

namespace blink {
namespace net {
namespace http {

class payload {

public:
	
	explicit payload() { }
	
	~payload() { }
	
	inline std::vector<header> & headers() force_inline { return headers_; }
	
	inline std::string & content() force_inline { return content_; }
	
	inline void headers(std::vector<header> & headers) force_inline {
		this->headers_ = headers;
	}
	
	inline void headers(std::vector<header> && headers) force_inline {
		headers_ = std::move(headers);
	}
	
	inline void content(std::string & content) force_inline {
		this->content_ = content;
	}
	
	inline void content(std::string && content) force_inline {
		content_ = std::move(content);
	}

private:

	std::vector<header> headers_;
	std::string content_;
};

} // namespace http
} // namespace net
} // namespace blink
#endif // _BLINK_HTTP_PAYLOAD_H