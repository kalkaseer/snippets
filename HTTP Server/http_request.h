#ifndef BLINK_REQUEST_H_
#define BLINK_REQUEST_H_

#include <string>
#include "http_payload.h"
#include "compiler.h"
#include "http_method.h"

namespace blink {
namespace net {
namespace http {
	
class request
  : public payload
{

public:
  
  inline std::string & method() force_inline { return method_; }
  
  inline const std::string & method() const force_inline { return method_; }

  inline std::string & uri() force_inline { return uri_; }
  
  inline const std::string & uri() const force_inline { return uri_; }

  inline int & version_major() force_inline { return http_version_major_; }
  
  inline const int & version_major() const force_inline { return http_version_major_; }

  inline int & version_minor() force_inline { return http_version_minor_; }
  
  inline const int & version_minor() const force_inline { return http_version_minor_; }

private:
	
	std::string method_;
	std::string uri_;
	int http_version_major_;
	int http_version_minor_;
};
	
} // namespace http
} // namespace net
} // namespace blink

#endif // BLINK_REQUEST_H_
