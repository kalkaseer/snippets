#include "connection_manager.h"

#include <asio/spawn.hpp>

namespace blink {
namespace net {
namespace http {
	
connection_manager::connection_manager(asio::io_context & cxt)
	: iocontext(cxt)
{ }

void connection_manager::start(connection_ptr c) {
	connections.insert(c);
	spawn(
		iocontext,
    [this, &c](asio::yield_context yield) {
			c->start(yield);
		});
}

void connection_manager::stop(connection_ptr c) {
	connections.erase(c);
	c->stop();
}

void connection_manager::stop_all() {
	for (auto & c : connections)
		c->stop();
	connections.clear();
}

} // namespace http
} // namespace net
} // namespace blink
