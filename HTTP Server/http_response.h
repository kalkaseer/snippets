#ifndef BLINK_RESPONSE_H
#define BLINK_RESPONSE_H

#include <string>
#include <vector>
#include <asio.hpp>
#include "http_payload.h"
#include "compiler.h"

namespace blink {
namespace net {
namespace http {

class response 
	: public payload
{
	
public:
	
	enum status_code {
		ok = 200,
		created = 201,
		accepted = 202,
		no_content = 204,
		multiple_choices = 300,
		moved_permenantly = 301,
		moved_temporarily = 302,
		not_modified = 304,
		bad_request = 400,
		unauthorized = 401,
		forbidden = 403,
    not_found = 404,
		internal_server_error = 500,
    not_implemented = 501,
		bad_gateway = 502,
		service_unavailable = 503
	};
	
	/// Convert response into a vector of buffers. The buffers do not own the
	/// underlying memory blocks so the response object must remain valid
	/// and unchanged until the write operation has completed.
	void to_buffers(std::vector<asio::const_buffer> & bufs);
	
	/// Get a stock response
	static void make(response & res, status_code status);
	
	static void make(
	  response & res, response::status_code status, const std::string & mimetype);

	static void make(
    response & res, response::status_code status, std::string && mimetype);

	inline status_code & status() force_inline { return status_; }
	
private:
	
	status_code status_;
};

} // namespace http
} // namespace net
} // namespace blink

#endif //BLINK_RESPONSE_H
