#ifndef _BLINK_REQUEST_HANDLER_H
#define _BLINK_REQUEST_HANDLER_H

#include <string>

namespace blink {
namespace net {
namespace http {

struct response;
struct request;

/// Common handler for all incoming request
class request_handler {

public:
	
	request_handler(const request_handler &) = delete;
	request_handler& operator=(const request_handler &) = delete;
	
	/// Construct a handler with a directory containing files to be served.
	explicit request_handler(const std::string & doc_dir);
	
	explicit request_handler(std::string && doc_dir);
	
	/// Handle a request
	void handle(const request & req, response & res);
	
private:
	
	/// The directory containing the files to be served.
	std::string doc_root;
	
	/// Perform URL decoding on a string. Returns false if encoding was invalid.
	static bool url_decode(const std::string & in, std::string & out);
	
};

} // namespace http
} // namespace net
} // namespace blink

#endif // _BLINK_REQUEST_HANDLER_H
