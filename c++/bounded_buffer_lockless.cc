#include <thread>
#include <mutex>
#include <chrono>
#include <vector>
#include <atomic>
#include "sync_printer.h"
#include "spinlock.h"
#include "timeit.h"

template< class T >
class bounded_buffer {
	
public:
	
	explicit bounded_buffer(std::size_t capacity)
		: m_capacity(capacity), m_count(0), m_front(0), m_rear(0)
	{
		m_buffer = new T[capacity];
	}
	
	~bounded_buffer() {
		delete [] m_buffer;
	}
	
	void put(T value) {
		m_spin([this, &value]() { this->put_impl(std::ref(value)); });
	}
	
	T get() {
		T value;
		m_spin([this, &value](){ value = this->get_impl(); });
		return value;
	}
	
private:
	
	inline void put_impl(T & value) {
		
		m_buffer[m_rear] = value;
		m_rear = (m_rear + 1) % m_capacity;
		++m_count;
		
		m_not_empty.notify_one();
	}
	
	inline T get_impl() {
		
		T value = m_buffer[m_front];
		m_front = (m_front + 1) % m_capacity;
		--m_count;
		
		m_not_full.notify_one();
		
		return value;
	}
	
	using unique_lock = std::unique_lock<std::mutex>;
	
	spinlock m_spin;
	
	T * m_buffer;
	std::size_t m_capacity;
	std::size_t m_count;
	std::size_t m_front;
	std::size_t m_rear;
	std::condition_variable m_not_full;
	std::condition_variable m_not_empty;
	std::mutex m_lock;
};

void consume(int id, bounded_buffer<int> & buffer) {
	for (int i = 0; i < 19; ++i) {
		//printer::println("Consumer ", id, " consumed ", buffer.get());
		std::this_thread::sleep_for(std::chrono::milliseconds(75));
	}
}

void produce(int id, bounded_buffer<int> & buffer) {
	for (int i = 0; i < 30; ++i) {
		buffer.put(i);
		//printer::println("Producer ", id, " produced ", i);
		std::this_thread::sleep_for(std::chrono::milliseconds(50));
	}
}

void test() {
	
	bounded_buffer<int> buffer(400);
	
	std::vector<std::thread> consumers;
	std::vector<std::thread> producers;
	
	for (int i = 0; i < 3; ++i) consumers.emplace_back(consume, i, std::ref(buffer));
	for (int i = 0; i < 2; ++i) producers.emplace_back(produce, i, std::ref(buffer));
	
	for (auto & th : consumers) th.join();
	for (auto & th : producers) th.join(); 
}

int main() {
	
	std::cout << "lockless: " << timeit(5, test) << std::endl;
	return 0;
}