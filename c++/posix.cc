/*
 * Copyright (c) 2015-2016 Kareem Alkaseer. All rights reserved.
 */

#include "posix.h"

namespace blink {
namespace system {

group_ids_ptr get_groups() {

	group_ids_ptr ptr;
	gid_t dummy[] = {0};
	int size = getgroups(0, dummy);
	if (size > 0) {
		group_ids_t * list = new (std::nothrow) group_ids_t(
# 		if defined(__linux__)
			size + 1
# 		else
			size
# 		endif
		);
		if (list) {
			size = getgroups(size, list->data());
			if (size > 0) {
# 			if defined(__linux__)
				gid_t egid = getegid();
				bool matched = false;
				for (int i = 0; i < size; ++i)
					if (egid == (*list)[i]) { matched = true; break; }
				if (matched) {
					list->pop_back();
					list->shrink_to_fit();
				} else {
					list->push_back(egid);
				}	
# 			endif
				ptr.reset(list);
			} else {
				delete[] list;
			}
		}
	}
	return ptr;
}

stats_ptr file_info(const std::string & path) {

	stats_ptr ptr;
	struct stat * buf = new (std::nothrow) struct stat;
	if (buf) {
		if (stat(path.c_str(), buf) == 0)
			ptr.reset(buf);
		else
			delete buf;
	}
	return ptr;
}

stats_ptr file_info(std::string && path) {
	return file_info(path);
}

bool can_read(const std::string & path) {
  group_ids_ptr groups = get_groups();
  if (groups) return can_read(path, groups);
  return false;
}

bool can_read(const std::string && path) {
  return can_read(path);
}

bool can_read(const std::string & path, const group_ids_ptr & groups) {
	
	stats_ptr st = file_info(path);
	if (st) return can_read(st, groups);
	return false;
}

bool can_read(const std::string && path, const group_ids_ptr & groups) {
	return can_read(path, groups);
}

bool can_read(const stats_ptr & st, const group_ids_ptr & groups) {
	
	uid_t userid = geteuid();
	if ((st->st_uid == userid) && ((st->st_mode & S_IRUSR) != 0))
		return true;
	if ((st->st_mode & S_IRGRP) != 0) {
		for (gid_t id : *groups)
			if (st->st_gid == id)
				return true;
		return false;
	}
	if ((st->st_mode & S_IROTH) != 0)
		return true;
	return false;
}

bool can_read(const stats_ptr && st, const group_ids_ptr & groups) {
	return can_read(st, groups);
}

bool can_open(const std::string & path) {
	uid_t rid = getuid();
	uid_t eid = geteuid();
	if (setuid(rid) == 0) {
#		if defined(__linux__)
		int fd = open(path.c_str(), O_NOATIME | O_RDONLY);
#		else
		int fd = open(path.c_str(), O_RDONLY);
#		endif
		if (fd != -1) {
			close(fd);
			setuid(eid);
			return true;
		}
		setuid(eid);
	}
	return false;
}

bool can_open(const std::string && path) { 
	return can_open(path);
}

} // namespace system
} // namespace blink