/*
 * Copyright (c) 2014-2016 Kareem Alkaseer. All rights reserved.
 */


#ifndef POINTTOALLOCATOR_H_
#define POINTTOALLOCATOR_H_

#include <limits>

template<typename T, void * allocatedPointer, uint32 allocatedSize, typename Deallocator>
class PointToAllocator {
public :
    //    typedefs
    typedef T value_type;
    typedef value_type* pointer;
    typedef const value_type* const_pointer;
    typedef value_type& reference;
    typedef const value_type& const_reference;
    typedef std::size_t size_type;
    typedef std::ptrdiff_t difference_type;

public :
    //    convert an allocator<T> to allocator<U>
    template<typename U, void *, uint32, Deallocator>
    struct rebind {
        typedef PointToAllocator<U, void *, uint32, Deallocator> other;
    };

public :
    inline explicit PointToAllocator() {}
    inline ~PointToAllocator() {}
    inline explicit PointToAllocator(PointToAllocator const&) {}
    template<typename U>
    inline explicit PointToAllocator(PointToAllocator<U, void *, uint32, Deallocator> const&) {}

    //    address
    inline pointer address(reference r) { return &r; }
    inline const_pointer address(const_reference r) { return &r; }

    //    memory allocation
    inline pointer allocate(
        size_type cnt, typename std::allocator<void>::const_pointer = 0
    ) {
      return reinterpret_cast<pointer>(allocatedPointer);
    }
    inline void deallocate(pointer p, size_type) {
        Deallocator().deallocate(reinterpret_cast<pointer>(allocatedPointer), allocatedSize);
    }

    //    size
    inline size_type max_size() const {
        return std::numeric_limits<size_type>::max() / sizeof(T);
 }

    //    construction/destruction
    inline void construct(pointer p, const T& t) { new(p) T(t); }
    inline void destroy(pointer p) { p->~T(); }

    inline bool operator==(PointToAllocator const&) { return true; }
    inline bool operator!=(PointToAllocator const& a) { return !operator==(a); }
};    //    end of class Allocator


#endif /* POINTTOALLOCATOR_H_ */
