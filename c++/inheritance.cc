/*
 * inheritance.cc
 *
 *  Created on: Sep 3, 2016
 *      Author: Kareem Alkaseer
 */

#include <memory>
#include <iostream>

namespace H {
class shape {

public:

  shape() { }

  virtual ~shape() { }

  virtual void draw() = 0;
};

class circle
  : public shape
{

public:

  circle() { }

  virtual ~circle() { }

  virtual void draw() override { std::cout << "circle::draw\n"; }
};

class rectangle
  : public shape
{

public:

  rectangle() { }

  virtual ~rectangle() { }

  virtual void draw() override { std::cout << "rectangle::draw\n"; }
};

} // namespace H


namespace T {

template< class E >
class shape {

public:

  template<class ...Args>
  shape(Args && ...args) : m_engine{ std::forward<Args>(args)... } { }

  ~shape() { }

  void draw() {
    m_engine.draw();
  }

private:

  E m_engine;
};

class circle {

public:

  circle() { }

  void draw() { std::cout << "circle::draw\n"; }
};

class rectangle {

public:

  rectangle() { }

  void draw() { std::cout << "rectangle::draw\n"; }
};

} // namespace T


int main() {

  auto hcirc = std::shared_ptr<H::shape>(new H::circle());
  auto hrect = std::shared_ptr<H::shape>(new H::rectangle());

  T::shape<T::circle>    tcirc;
  T::shape<T::rectangle> trect;

  hcirc->draw();
  hrect->draw();

  tcirc.draw();
  trect.draw();

  return 0;
}

