/*
 * Copyright (c) 2014-2016 Kareem Alkaseer. All rights reserved.
 */

#include "filesystem.h"

namespace blink {
namespace fs {
	
#if !defined(_WIN32) && !defined(__MINGW32__)

bool is_hidden(const ::fsbase::path & p) {
  path::string_type name = p.filename().native();
  return name != ".." && name != "." && name[0] == '.';
}

bool is_directory(const std::shared_ptr< struct stat > & p) {
  return (p->st_mode & S_IFMT) == S_IFDIR;
}

bool is_regular(const std::shared_ptr< struct stat > & p) {
  return (p->st_mode & S_IFMT) == S_IFREG;
}

bool is_symbolic_link(const std::shared_ptr< struct stat > & p) {
  return (p->st_mode & S_IFMT) == S_IFLNK;
}

bool has_permissions(const std::shared_ptr< struct stat > & p, uint32_t perms) {
  return (p->st_mode & perms) != 0;
}

#endif // !defined(_WIN32) && !defined(__MINGW32__)

} // namespace fs
} // namespace blink