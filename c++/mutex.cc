/*
 * Copyright (c) 2016 Kareem Alkaseer, LLC. All rights reserved.
 */

#include "mutex.h"

#include <chrono>

namespace spline {

mutex::mutex()
: thread = 0,
  recursion = 0
{ }

mutex::~mutex() { }

void mutex::create(mutex_ptr & ptr) {
  static std::mutex m;
  std::lock_guard<std::mutex> guard{m};
  if (!ptr)
    ptr = std::make_shared<mutex>();
}

void mutex::lock(int32 timeout) {
  if (timeout > 0)
    mtx.try_lock_for(std::chrono::milliseconds(timeout));
  else
    mtx.lock();
  thread = std::this_thread::get_id();
  ++recursion;
}

void mutex::unlock() {
  if (--recursion == 0)
    thread = 0;
  mtx.unlock();
}

int32 mutex::unlock_all() {
  int32 count = recursion;
  for (int32 n = 0; n < count; ++n)
    unlock();
  return count;
}

bool mutex::holds_lock() {
  return thread == std::this_thread::get_id() && recursion > 0;
}

std::recursive_timed_mutex & mutex::mutex() { return mutex; }

} // namespace spline


