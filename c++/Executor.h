/*
 * Copyright (c) 2013-2016 Kareem Alkaseer. All rights reserved.
 */

#ifndef EXECUTORTHRIFT_H_
#define EXECUTORTHRIFT_H_

#include "BasicTypes.h"
#include "datastore/operations/ExecutorType.h"
#include "datastore/operations/Operation.h"

namespace datastore {
namespace HtThrift {

class Executor {

 public:

  typedef ContextPool         ContextPoolType;
  typedef Operation<Executor> OperationType;

  static const ExecutorType TYPE = EXC_THRIFT;

 private:

  inline static bool shouldRetryTransportError(const TransportException & e) {
    return kRetriableTransportErrors.find(e.getType()) != kRetriableTransportErrors.end();
  }

  inline static bool shouldReopenTransport(const TransportException & e) {
    return e.getType() == TransportException::NOT_OPEN;
  }

  inline static bool shouldRetryClientError(const DatastoreException & e) {
    return kRetriableClientErrors.find(e.code) != kRetriableClientErrors.end();
  }

  inline void retry(OperationType * operation) {
    operation->reset();
    operation();
  }

 public:

  int32 executeOperation(OperationType * operation) {
    try {
      operation();
      return 0;
    } catch (const TransportException & e) {
      if (shouldRetryTransportError(e)) {
        try {
          operation();
          return 0;
        } catch (const TransportException & e) {
          if (shouldRetryTransportError(e)) {
            retry(operation);
            return 0;
          } else {
            return -1;
          }
        }
      }
      return -1;
    } catch (const DatastoreException & e) {
      if (shouldRetryClientError(e)) {
        try {
          operation();
          return 0;
        } catch (const DatastoreException & e2) {
          if (shouldRetryClientError(e2)) {
            retry(operation);
            return 0;
          } else {
            return -1;
          }
        }
      }
      return -1;
    }
  }
};

} // namespace HtThrift
} // namespace datastore



#endif /* EXECUTORTHRIFT_H_ */
