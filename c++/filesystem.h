/*
 * Copyright (c) 2014-2016 Kareem Alkaseer. All rights reserved.
 */

#ifndef _BLINK_FILESYSTEM_H
#define _BLINK_FILESYSTEM_H

#include <memory>
#include <sys/stat.h>

#ifdef _BLINK_CPP_HAS_FILESYSTEM

# ifdef _BLINK_CPP_FILESYSTEM_EXPERIMENTAL
# include <experimental/filesystem>
  namespace fsbase = std::experimental::filesystem;
# else
# include <filesystem>
  namespace fsbase = std::filesystem;
# endif // _BLINK_CPP_FILESYSTEM_EXPERIMENTAL

#else
# include <boost/filesystem.hpp>
  namespace fsbase = boost::filesystem;
#endif

namespace blink {
namespace fs {
	
#if !defined(_WIN32) && !defined(__MINGW32__)
	
using path = ::fsbase::path;

bool is_hidden(const ::fsbase::path & p);

bool is_directory(const std::shared_ptr< struct stat > & p);

bool is_regular(const std::shared_ptr< struct stat > & p);

bool is_symbolic_link(const std::shared_ptr< struct stat > & p);

bool has_permissions(const std::shared_ptr< struct stat > & p, uint32_t perms);
  
#endif // !defined(_WIN32) && !defined(__MINGW32__)

} // namespace fs
} // namespace blink

#endif // _BLINK_FILESYSTEM_H