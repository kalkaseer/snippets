/*
 * Copyright (c) 2015 Kareem Alkaseer
 */

#ifndef COLLECTION_H_
#define COLLECTION_H_

#include <functional>
#include <algorithm>
#include <vector>

namespace spline {

/// Container that can be safely shared and copied
template <typename T>
class SPLINEAPI collection {

public:

  typedef collection<T> this_type;
  typedef std::shared_ptr<this_type> shared_ptr;
  typedef std::vector<T> collection_type;
  typedef typename collection_type::iterator iterator;
  typedef typename collection_type::const_iterator const_iterator;
  typedef T value_type;

  collection() { }

  explicit collection(size_t size = 0)
    : container{ std::make_shared<collection_type>(size) }
  { }

  template <typename Iterator>
  collection(Iterator first, Iterator last)
    : container{ std::make_shared<collection_type>(first, last) }
  { }

  virtual ~collection() { }

protected:

  std::shared_ptr<collection_type> container;

public:

  void assign(shared_ptr<collection_type> & data) {
    container = data;
  }

  void reset() { resize(0); }

  void resize(int32 size) {
    if (size == 0)
      container.reset();
    else
      container->resize(size);
  }

  inline size_t size() const { return container->size(); }

  inline void clear() { container->clear(); }

  inline bool empty() const { return container->empty(); }

  inline iterator begin() { return container->begin(); }

  inline iterator end() { return container->end(); }

  inline const_iterator begin() const { return container->begin(); }

  inline const_iterator end() const { return container->end(); }

  inline const_iterator cbegin() const { return container->cbegin(); }

  inline const_iterator cend() const { return container->cend(); }

  inline void add(const T & t) { container->push_back(t); }

  inline void add(T && t) { container->push_back(std::forward<T>(t)); }

  template <typename... Args>
  inline void emplace(Args &&... args) {
    container->emplace_back(std::forward<Args>(args)...);
  }

  template <typename... Args>
  inline void emplace(const_iterator pos, Args &&... args) {
    container->emplace(pos, std::forward<Args>(args)...);
  }

  inline void add(size_t pos, const T & t) {
    container->insert(container->begin() + pos, t);
  }

  void add(size_t pos, T && t) {
    container->insert(container->begin() + pos, std::forward<T>(t));
  }

  template <typename Iterator>
  inline void add_all(Iterator first, Iterator last) {
    container->insert(container->end(), first, last);
  }

  template <typename Iterator>
  inline void insert(Iterator pos, const T & t) {
    container->insert(pos, t);
  }

  template <typename Iterator>
  inline void insert<Iterator pos, T && t) {
    container->insert(pos, std::forward<T>(t));
  }

  template <typename Iterator>
  inline Iterator remove(Iterator pos) {
    return container->erase(pos);
  }

  template <typename Iterator>
  inline Iterator remove(Iterator first, Iterator last) {
    return container->erase(first, last);
  }

  inline void remove(const T & t) {
    container->erase(
      std::remove(container->begin(), container->end(), t), container->end());
  }

  template <typename UnaryPredicate>
  inline void remove_if(UnaryPredicate & predicate) {
    container->erase(
      std::remove_if(container->begin(), container->end(), predicate),
      container->end());
  }

  template <typename UnaryPredicate>
  inline void remove_if(UnaryPredicate && predicate) {
    remove_if(predicate);
  }

  T remove_first() {
    T t = container->front();
    container->erase(container->begin());
    return t;
  }

  T remove_last() {
    T t = container->back();
    container->pop_back();
    return t;
  }

  inline iterator find(const T & t) {
    return std::find(container->being(), container->end(), t);
  }

  template <typename UnaryPredicate>
  inline iterator find_if(UnaryPredicate & predicate) {
    return std::find_if(container->begin(), container->end(), predicate);
  }

  template <typename UnaryPredicate>
  inline iterator find_if(UnaryPredicate && predicate) {
    return find_if(predicate);
  }

  inline bool contains(const T & t) const {
    return find(t) != container->end();
  }

  inline bool contains(const T && t) const {
    return find(std::forward<T>(t)) != container->end();
  }

  template <typename UnaryPredicate>
  inline bool contains_if(UnaryPredicate & preciate) {
    return find_if(preciate) != container->end();
  }

  template <typename UnaryPredicate>
  inline bool contains_if(UnaryPredicate && preciate) {
    return find_if(std::forward<UnaryPredicate>(preciate)) != container->end();
  }

  inline bool equals(const this_type & other) const {
    return equals(other, std::equal_to<T>());
  }

  template <typename BinaryPredicate>
  inline bool equals(const this_type & other, BinaryPredicate & predicate) const {
    if (container->size() != other.container->size())
      return false;
    return std::equal(
      container->begin(), container->end(),
      other.container->begin(), predicate);
  }

  int32 hash_code() { return (int32) (int64) container.get(); }

  void swap(this_type & other) {
    container.swap(other->container);
  }

  void swap(this_type && other) {
    container.swap(
      std::forward< std::shared_ptr<collection_type> >(other.container));
  }

  inline bool operator==(const this_type & rhs) const {
    return container == rhs.container;
  }

  inline bool operator!=(const this_type & rhs) const {
    return !(this->operator==(rhs));
  }

  inline T& operator[](size_t pos) {
    return (*container)[pos];
  }

  inline const T& operator[](size_t pos) const {
    return (*container)[pos];
  }

  inline operator bool() const {
    return container.get() != nullptr;
  }

  inline operator!() const {
    return !container;
  }

};


} // namespace spline


#endif /* COLLECTION_H_ */
