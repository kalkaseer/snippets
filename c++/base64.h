/* Copyright @2016 Kareem Alkaseer */


#ifndef BASE_BASE64_H_
#define BASE_BASE64_H_

#include <string>
#include <cassert>

#include "ascii_utils.h"
#include "basic_types.h"
#include "generic_traits.h"

namespace blink {
namespace encoding {

namespace base64 {
namespace details {

  static constexpr char pad_mark = '=';

  static constexpr unsigned char pd = 0xfd; ///< Padding character.
  static constexpr unsigned char ws = 0xfe; ///< Whitespace character.
  static constexpr unsigned char il = 0xff; ///< Illegal base64 character.

  /** **********************************************************************
   * @brief Lookup table from indices of the base64 alphabet to the alphabet it
   * characters.
   * **********************************************************************/
  static constexpr char nibbles_table[] = {
    'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J',
    'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T',
    'U', 'V', 'W', 'X', 'Y', 'Z',
    'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j',
    'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't',
    'u', 'v', 'w', 'x', 'y', 'z',
    '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
    '+', '/'
  };

  /** **********************************************************************
   * @brief Decode lookup table from all possible values of an unsigned character, i.e.
   * a byte of binary data, to the associated base64 character.
   * @details A lookup should return: 1) the position of the table in the
   * base64 alphabet, 2) illegal mark, 3) whitespace mark or 4) padding mark.
   * **********************************************************************/
  static constexpr unsigned char decode_table[] = {
  // 0   1   2   3   4   5   6   7   8   9
     il, il, il, il, il, il, il, il, il, ws,   //   0 -  9
     ws, ws, ws, ws, il, il, il, il, il, il,   //  10 - 19
     il, il, il, il, il, il, il, il, il, il,   //  20 - 29
     il, il, ws, il, il, il, il, il, il, il,   //  30 - 39
     il, il, il, 62, il, il, il, 63, 52, 53,   //  40 - 49
     54, 55, 56, 57, 58, 59, 60, 61, il, il,   //  50 - 59
     il, pd, il, il, il,  0,  1,  2,  3,  4,   //  60 - 69
      5,  6,  7,  8,  9, 10, 11, 12, 13, 14,   //  70 - 79
     15, 16, 17, 18, 19, 20, 21, 22, 23, 24,   //  80 - 89
     25, il, il, il, il, il, il, 26, 27, 28,   //  90 - 99
     29, 30, 31, 32, 33, 34, 35, 36, 37, 38,   // 100 - 109
     39, 40, 41, 42, 43, 44, 45, 46, 47, 48,   // 110 - 119
     49, 50, 51, il, il, il, il, il, il, il,   // 120 - 129
     il, il, il, il, il, il, il, il, il, il,   // 130 - 139
     il, il, il, il, il, il, il, il, il, il,   // 140 - 149
     il, il, il, il, il, il, il, il, il, il,   // 150 - 159
     il, il, il, il, il, il, il, il, il, il,   // 160 - 169
     il, il, il, il, il, il, il, il, il, il,   // 170 - 179
     il, il, il, il, il, il, il, il, il, il,   // 180 - 189
     il, il, il, il, il, il, il, il, il, il,   // 190 - 199
     il, il, il, il, il, il, il, il, il, il,   // 200 - 209
     il, il, il, il, il, il, il, il, il, il,   // 210 - 219
     il, il, il, il, il, il, il, il, il, il,   // 220 - 229
     il, il, il, il, il, il, il, il, il, il,   // 230 - 239
     il, il, il, il, il, il, il, il, il, il,   // 240 - 249
     il, il, il, il, il, il                    // 250 - 255
  };

} // namespace details
} // namespace base64

/** *******************************************************************
 * @class Base64
 * @brief An encoder/decoder for Base64.
 * ********************************************************************/
class Base64 {

public:

  /** **********************************************************************
   * @enum DecodeOption
   * @brief Decoding options which can be supplied to the decode methods to
   * control the parsing, padding and terminating the supplied base64 encoded
   * data.
   * **********************************************************************/
  enum class DecodeOption : uint8 {

    /// Parse base64 characters only.
    parse_strict = 1 << 0,
    /// Parse base64 and whitespace characters.
    parse_white  = 1 << 1,
    /// Parse all characters.
    parse_any    = parse_strict | parse_white,
    parse_mask   = parse_any,

    pad_yes      = 1 << 2,            ///< Padding is required.
    pad_any      = 1 << 3,            ///< Padding is optional.
    pad_no       = pad_yes | pad_any, ///< Padding is disallowed.
    pad_mask     = pad_no,

    /// Must terminate at end of buffer.
    terminate_buffer = 1 << 4,
    /// May terminate at any character boundary.
    terminate_char   = 1 << 5,
    /// May terminate at a sub-character bit offset.
    terminate_any    = terminate_buffer | terminate_char,
    terminate_mask   = terminate_any,

    /// Strictest option set.
    strict           = parse_strict | pad_yes | terminate_buffer,
    /// Relaxed option set.
    lenient          = parse_any | pad_any | terminate_char,
  };

  /** **********************************************************************
   * @brief Converts {@code DecodeOption} values to an integral type. The
   * integral value returned is of the same type as the enum class itself,
   * i.e, @c uint8.
   * @param option Value to convert to an integral.
   * @return Integral value of @c option.
   * **********************************************************************/
  static constexpr uint8 integral(DecodeOption option) {
    return static_cast<uint8>(option);
  }

  /** **********************************************************************
   * This class cannot be instantiated. It is mainly used as a utility class
   * with static methods.
   * **********************************************************************/
  Base64() = delete;

  ~Base64() = delete;

  /** **********************************************************************
   * Checks whether a character is a valid base64 encoding character.
   * @param ch Character to check.
   * @return `true` if `ch` is a base64 character, `false` otherwise.
   * **********************************************************************/
  static constexpr bool is_base64(char ch) {
    return (ch >= 'A' && ch <= 'Z') ||
           (ch >= 'a' && ch <= 'z') ||
           (ch >= '0' && ch <= '9') ||
           ch == '+'                ||
           ch == '/';
  }

  /** **********************************************************************
   * @brief Checks whether given data is base64 encoded.
   * @details @c begin should point to the beginning of the data segment and
   * @c end should point to the first invalid position (past-the-end).
   * @param begin Start iterator.
   * @param end End iterator.
   * @tparam ConstIterator A forward iterator that does not modify content it
   *                       is referencing.
   * **********************************************************************/
  template< typename ConstIterator >
  static inline bool is_base64(ConstIterator begin, ConstIterator end) {
    for (; begin != end; ++begin) {
      if (!is_base64(*begin))
        return false;
    }
    return true;
  }

  template< typename ConstIterator>
  static inline std::size_t next_quantum(
    ConstIterator & iter, ConstIterator end,
    int32 parse_flags, bool illegal_pads,
    unsigned char buf[4], bool * padded
  ) {
    using namespace base64::details;
    std::size_t byte_len    = 0;
    std::size_t pad_len     = 0;

    ConstIterator pad_start = iter;

    for (; byte_len < 4 && iter != end; ++iter) {

      buf[byte_len] = decode_table[static_cast<unsigned char>(*iter)];

      if (il == buf[byte_len] || (illegal_pads && pd == buf[byte_len])) {
        if (parse_flags != integral(DecodeOption::parse_any))
          break; // ignore an illegal character
      } else if (ws == buf[byte_len]) {
        if (parse_flags == integral(DecodeOption::parse_strict))
          break; // ignore whitespace
      } else if (pd == buf[byte_len]) {
        if ((byte_len < 2 || byte_len + pad_len >= 4)) {
          if (parse_flags != integral(DecodeOption::parse_any))
            break; // ignore unexpected padding or extra padding
        } else {
          if (1 == ++pad_len)
            pad_start = iter;
        }
      } else {
        if (pad_len > 0) {
          if (parse_flags != integral(DecodeOption::parse_any))
            break; // ignore pads followed by data
          pad_len = 0;
        }
        ++byte_len;
      }
    }

    for (std::size_t i = byte_len; i < 4; ++i)
      buf[i] = 0;

    if (byte_len + pad_len == 4) {
      *padded = true;
    } else {
      *padded = false;
      if (pad_len)
        iter = pad_start; // roll back illegal padding
    }

    return byte_len;
  }

public :

  /**
   * @brief Trait type indicating whether the @c Output type given to @c encode
   * and @c decode methods is a container.
   * @details A compatible container has a @c push_back method to insert
   * data into it and a @c reserve method to reserve memory to use for the
   * output buffer. Client code should specialise this trait to allow custom
   * type to be regarded compatible.
   * @tparam T Underlying type.
   */
  template< typename T >
  struct container_trait_t : type_traits::false_type {};

private:

  /**
   * @brief Specialisation of @c container_trait_t to @c std::basic_string.
   * @tparam Args @c std::basic_string template arguments.
   */
  template< typename... Args >
  struct container_trait_t<std::basic_string<Args...>> : type_traits::true_type {};

  /**
   * @brief Specialisation of @c container_trait to @c std::vector.
   * @tparam Args @c std::vector template arguments.
   */
  template< typename... Args >
  struct container_trait_t<std::vector<Args...>> : type_traits::true_type {};

  /**
   * @brief Trait type to allow selection of the right @c container_trait_t.
   * @tparam Underlying type.
   */
  template < typename T >
  struct container_trait {
    static constexpr bool const value =
      container_trait_t<type_traits::decay_t<T>>::value;
  };

  /**
   * @brief Trait type to handle writing to different types of @c Output as given to
   * @c encode and @c decode methods.
   * @tparam Output underlying type.
   * @tparam IsContainer Whether the underlying type is a compatible container.
   */
  template< typename Output, bool IsContainer = false>
  struct output_writer {
    static inline void write(Output & out, unsigned char ch) {
      *out = ch;
      ++out;
    }
  };

  /**
   * @brief Specialisation of @c output_writer to compatible containers.
   * @tparam Output Underlying type used for output.
   */
  template< typename Output >
  struct output_writer<Output, true> {
    static inline void write(Output & container, unsigned char ch) {
      container.push_back(ch);
    }
  };

  /**
   * @brief Utility method to write to different type output based on their
   * traits.
   * @param out Output object used for writing.
   * @param ch Byte of data to write to the output.
   * @tparam Output Underlying output type.
   */
  template< typename Output >
  static inline void write_to_output(Output & out, unsigned char ch) {
    output_writer<
      Output, container_trait<
        type_traits::decay_t<Output>>::value>::write(out, ch);
  }

  /** **********************************************************************
   * @brief Decodes a base64 encoded string.
   * @param input Start iterator.
   * @param end End iterator.
   * @param out 1) An iterator that points to the start of output buffer.
   *            Should be valid for at least the length from @c input to
   *            @c end or 2) an compatible output container as mandated by
   *            @c container_trait.
   * @param flags DecodeOption flags, multiple flags are OR'd together.
   * @tparam ConstIterator A forward iterator that does not modify content it
   *                       is referencing.
   * @tparam Output Either a forward iterator that allows modifying content
   *                it is referencing or a compatible container as mandated by
   *                @c container_trait.
   * **********************************************************************/
  template< typename ConstIterator, typename Output >
  static inline bool decode_impl(
    ConstIterator input, ConstIterator end, Output & out, int32 flags
  ) {
    assert( // TODO: change to custom assert
      flags <= (integral(DecodeOption::parse_mask) |
                integral(DecodeOption::pad_mask)   |
                integral(DecodeOption::terminate_mask)));

    const int32 parse_flags     = flags & integral(DecodeOption::parse_mask);
    const int32 pad_flags       = flags & integral(DecodeOption::pad_mask);
    const int32 terminate_flags = flags & integral(DecodeOption::terminate_mask);

    assert(parse_flags != 0);     // TODO: change to custom assert
    assert(pad_flags != 0);       // TODO: change to custom assert
    assert(terminate_flags != 0); // TODO: change to custom assert

    bool ok = true;
    bool padded = false;
    unsigned char ch;
    unsigned char buf[4];

    while (input != end) {

      std::size_t len = next_quantum(
        input, end, parse_flags,
        integral(DecodeOption::pad_no) == pad_flags, buf, &padded);

      ch = (buf[0] << 2) | ((buf[1] >> 4) & 0x03);
      if (len >= 2) {
        write_to_output(out, ch);
        ch = ((buf[1] << 4) & 0xf0) | ((buf[2] >> 2) & 0xf);
        if (len >= 3) {
          write_to_output(out, ch);
          ch = ((buf[2] << 6) & 0xc0) | buf[3];
          if (len >= 4) {
            write_to_output(out, ch);
            ch = 0;
          }
        }
      }

      if (len < 4) {
        if (integral(DecodeOption::terminate_any) != terminate_flags && ch != 0)
          ok = false; // unused bits
        if (integral(DecodeOption::pad_yes) == pad_flags && !padded)
          ok = false; // padding expected but not met
        break;
      }
    }

    if (
      integral(DecodeOption::terminate_buffer) == terminate_flags && input != end
    )
      ok = false; // unused characters

    return ok;
  }

  template< typename ConstIterator, typename Output, bool IsOk = false >
  struct decoder {
    static inline bool decode(
      ConstIterator input, ConstIterator end, Output & out, int32 flags
    ) {
      return decode_impl(input, end, out, flags);
    }
  };

  template< typename ConstIterator, typename Output>
  struct decoder<ConstIterator, Output, true> {
    static inline bool decode(
      ConstIterator input, ConstIterator end, Output & container, int32 flags
    ) {
      container.reserve(end - input + container.size());
      return decode_impl(input, end, container, flags);
    }
  };

public:

  template< typename ConstIterator >
  static constexpr std::size_t encoded_size(
    ConstIterator begin, ConstIterator end
  ) {
    return ((((end - begin) + 2) / 3) * 4);
  }

private:

  template< typename ConstIterator, typename Output >
  static inline void encode_impl(
    ConstIterator input, ConstIterator end, Output & out
  ) {
    using namespace base64::details;
    unsigned char ch;

    while (input != end) {

      ch = (static_cast<unsigned char>(*input) >> 2) & 0x3f;
      write_to_output(out, nibbles_table[ch]);

      ch = (static_cast<unsigned char>(*input) << 4) & 0x3f;
      if (++input < end)
        ch |= (static_cast<unsigned char>(*input) >> 4) & 0x0f;
      write_to_output(out, nibbles_table[ch]);

      if (input < end) {
        ch = (static_cast<unsigned char>(*input) << 2) & 0x3f;
        if (++input < end)
          ch |= (static_cast<unsigned char>(*input) >> 6) & 0x03;
        write_to_output(out, nibbles_table[ch]);
      } else {
        write_to_output(out, pad_mark);
      }

      if (input < end) {
        ch = static_cast<unsigned char>(*input) & 0x3f;
        write_to_output(out, nibbles_table[ch]);
        ++input;
      } else {
        write_to_output(out, pad_mark);
      }
    }
  }

  template< typename ConstIterator, typename Output, bool IsOk = false >
  struct encoder {

    static inline void encode(
      ConstIterator input, ConstIterator end, Output & out
    ) {
      encode_impl(input, end, out);
    }
  };

  template< typename ConstIterator, typename Output >
  struct encoder< ConstIterator, Output, true > {
    static inline void encode(
      ConstIterator input, ConstIterator end, Output & container
    ) {
      container.reserve(encoded_size(input, end) + container.size());
      encode_impl(input, end, container);
    }
  };

public:


  template< typename ConstIterator, typename Output >
  static inline bool decode(
    ConstIterator input, ConstIterator end, Output & out, int32 flags
  ) {
    return decoder<
      ConstIterator,
      Output,
      container_trait<Output>::value
    >::decode(input, end, out, flags);
  }

  template< typename ConstIterator, typename Output >
  static inline void encode(
    ConstIterator input, ConstIterator end, Output & out
  ) {
    encoder<
      ConstIterator,
      Output,
      container_trait<Output>::value
    >::encode(input, end, out);
  }

  static inline std::string encode(const std::string & data) {
    std::string s;
    encode(data.cbegin(), data.cend(), s);
    return s;
  }

  template< typename ConstIterator >
  static inline std::string encode(ConstIterator input, ConstIterator end) {
    std::string s;
    encode(input, end, s);
    return s;
  }

};

} // namespace encoding
} // namespace blink

static constexpr uint8 operator|(
  const blink::encoding::Base64::DecodeOption op1,
  const blink::encoding::Base64::DecodeOption op2
) {
  return
    blink::encoding::Base64::integral(op1) |
    blink::encoding::Base64::integral(op2);
}

template< typename Integral>
static constexpr uint8 operator|(
  const blink::encoding::Base64::DecodeOption option, const Integral n
) {
  return blink::encoding::Base64::integral(option) | n;
}

template< typename Integral>
static constexpr uint8 operator|(
  const Integral n, const blink::encoding::Base64::DecodeOption option
) {
  return blink::encoding::Base64::integral(option) | n;
}

static constexpr uint8 operator&(
  const blink::encoding::Base64::DecodeOption op1,
  const blink::encoding::Base64::DecodeOption op2
) {
  return
    blink::encoding::Base64::integral(op1) &
    blink::encoding::Base64::integral(op2);
}

template< typename Integral>
static constexpr uint8 operator&(
  const blink::encoding::Base64::DecodeOption option, const Integral n
) {
  return blink::encoding::Base64::integral(option) & n;
}

template< typename Integral>
static constexpr uint8 operator&(
  const Integral n, const blink::encoding::Base64::DecodeOption option
) {
  return blink::encoding::Base64::integral(option) & n;
}

template< typename Integral>
static constexpr bool operator==(
  const blink::encoding::Base64::DecodeOption option, const Integral n
) {
  return blink::encoding::Base64::integral(option) == n;
}

template< typename Integral>
static constexpr bool operator==(
  const Integral n, const blink::encoding::Base64::DecodeOption option
) {
  return blink::encoding::Base64::integral(option) == n;
}

template< typename Integral>
static constexpr bool operator!=(
  const blink::encoding::Base64::DecodeOption option, const Integral n
) {
  return blink::encoding::Base64::integral(option) != n;
}

template< typename Integral>
static constexpr bool operator!=(
  const Integral n, const blink::encoding::Base64::DecodeOption option
) {
  return blink::encoding::Base64::integral(option) != n;
}


#endif /* BASE_BASE64_H_ */
