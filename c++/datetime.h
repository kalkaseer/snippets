/*
 * Copyright (c) 2016 Kareem Alkaseer. All rights reserved.
 */


#ifndef BASE_DATETIMES_H_
#define BASE_DATETIMES_H_

#include <cstddef>
#include <regex>
#include <chrono>
#include <iomanip>
#include "base/string_view.h"
#include "basic_types.h"
#include "base/nocopy_buf.h"

namespace blink {
namespace datetime {

extern const std::regex extended_timezone_pattern;

extern const std::regex timezone_pattern;

static inline bool
parse(const std::string & s, std::tm & t, const char * format) {
  NoCopyStringBuffer buf(
    const_cast<std::string::value_type*>(s.data()), s.size());
  std::istream in(&buf);
  in >> std::get_time(&t, format);
  return !in.fail();
}

static inline bool
parse(std::string && s, std::tm & t, const char * format) {
  return parse(std::move(s), t, format);
}

static inline bool
parse(const string_view & s, std::tm & t, const char * format) {
  NoCopyStringViewBuffer buf(
    const_cast<string_view::value_type*>(s.data()), s.size());
  std::istream in(&buf);
  in >> std::get_time(&t, format);
  return !in.fail();
}

static inline bool
parse(string_view && s, std::tm & t, const char * format) {
  return parse(std::move(s), t, format);
}

static inline bool
parse(
  const char * s, std::size_t size, std::tm & t, const char * format
) {
  NoCopyCharBuffer buf(const_cast<char*>(s), size);
  std::istream in(&buf);
  in >> std::get_time(&t, format);
  return !in.fail();
}

static constexpr int32 seconds(int32 hours, int32 minutes) {
  return (hours * 60 * 60) + ((hours > -1 ? minutes : -minutes) * 60);
}

static constexpr int32 signed_minutes(int32 hours, int32 minutes) {
  return hours > -1 ? minutes : -minutes;
}

static inline int64 seconds() {
  using namespace std;
  return chrono::duration_cast<chrono::seconds>(
    chrono::system_clock::now().time_since_epoch()).count();
}

static inline int64 seconds(std::tm & t) {
  return timegm(&t);
}

static inline int64 milliseconds() {
  using namespace std;
  return chrono::duration_cast<chrono::milliseconds>(
    chrono::system_clock::now().time_since_epoch()).count();
}

static inline int64 milliseconds(std::tm & t) {
  using namespace std;
  return chrono::duration_cast<chrono::milliseconds>(
    chrono::system_clock::from_time_t(timegm(&t)).time_since_epoch()).count();
}

inline static void utc_offset(std::tm & tm, int32 hours, int32 minutes) {
  tm.tm_isdst = -1;
  tm.tm_gmtoff = 0;
  tm.tm_hour += -hours;
  tm.tm_min += datetime::signed_minutes(-hours, minutes);
}

} // namespace datetime
} /* namespace blink */

#endif /* BASE_DATETIMES_H_ */
