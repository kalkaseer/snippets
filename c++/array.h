/*
 * Copyright (c) 2015 Kareem Alkaseer.
 */

#ifndef ARRAY_H_
#define ARRAY_H_

#include <cstring>
#include <memory>
#include <boost/assert.hpp>
#include "allocator.h"
#include "basic_types.h"

namespace spline {

template <typename T>
struct array_data {

  array_data(int32 count)
    : data(nullptr),
      size(0)
  {
    resize(count);
  }

  ~array_data() { resize(0); }

  void resize(int32 count) {
    if (count == 0) {
      free(data);
      data = nullptr;
    } else if (!data) {
      data = (T*) alloc(count * sizeof(T));
    } else {
      data = (T*) realloc(data, count * sizeof(T));
    }
    size = count;
  }

  T* data;
  int32 size;

};


/// Utility to handle sharable arrays of simple types
template <typename T>
class array {

public:

  typedef array<T> this_type;
  typedef array_data<T> data_type;

  array() : data(nullptr) { }

  array(int32 size)
    : container(std::make_shared<data_type>(size)),
      data(container.get())
  { }

  static this_type instance(int32 size) {
    this_type m;
    m.container = std::make_shared<data_type>(size);
    m.data = m.container.get();
    return m;
  }

  void resize(int32 size) {

    if (size == 0) {
      container.reset();
    } else if (!container) {
      container = std::make_shared<data_type>(size);
    } else {
      container->resize(size);
    }
    data = container.get();
  }

  void reset() { resize(0); }

  T* get() const { return data->data; }

  int32 size() const { return data->size; }

  bool equals(const this_type & other) {
    if (data->size != other.data->size)
      return false;
    return std::memcmp(data->data, other.data->data, data->size) == 0;
  }

  int32 hash_code() const { return (int32) (int64) data; }

  T& operator[](int32 i) const {
    BOOST_ASSERT(i >= 0 && i < data->size);
    return data->data[i];
  }

  operator bool() const { return container.get() != nullptr; }

  bool operator!() const { return !container; }

  bool operator==(const this_type & other) { return container == other.container; }

  bool operator!=(const this_type & other) { return container != other.container; }

protected:

  std::shared_ptr<data_type> container;
  data_type * data;
};

template <typename T>
inline std::size_t hash_code(const array<T> & value) {
  return (std::size_t) value.hash_code();
}

template <typename T>
inline bool operator==(const array<T> & lhs, const array<T> & rhs) {
  return (lhs.hash_code() == rhs.hash_code());
}

} // namespace spline



#endif /* ARRAY_H_ */
