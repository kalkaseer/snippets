/*
 * Copyright (c) 2015-2016 Kareem Alkaseer.
 */

#ifndef CONTAINER_CC_
#define CONTAINER_CC_

#include <memory>
#include <unordered_map>
#include <map>
#include <unordered_set>
#include <set>
#include <initializer_list>
#include "common.h"

namespace spline {

template <typename ContainerBase>
class SPLINEAPI container
  : public syncbase
{

public:

  typedef container<ContainerBase> this_type;
  typedef ContainerBase container_type;
  typedef std::shared_ptr<this_type> shared_ptr;
  typedef typename container_type::iterator iterator;
  typedef typename container_type::const_iterator const_iterator;
  typedef typename container_type::value_type value_type;

  explicit container()
    : base{ std::make_shared<ContainerBase>() }
  {}

  explicit container(size_t size)
    : base{ std::make_shared<ContainerBase>(size) }
  { }

  explicit container(std::initializer_list<value_type> l)
    : base{ std::make_shared<ContainerBase>(l) }
  { }

  template <typename Iterator>
  container(Iterator first, Iterator last)
    : base{ std::make_shared<ContainerBase>(first, last) }
  { }

  virtual ~container() { }

  std::shared_ptr<ContainerBase> data() { return base; }

  std::shared_ptr<ContainerBase> operator->() {
    return base;
  }

  container_type& operator*() {
    return *base;
  }

  container_type& operator=(const container_type & rhs) {
    base = rhs.base;
    return *this;
  }

  bool operator !() const {
    return !base;
  }

  operator bool() const {
    return base.get() != nullptr;
  }

protected:

  std::shared_ptr<ContainerBase> base;
};

template <
  class Key,
  class Value,
  class Hash = std::hash<Key>,
  class Equal = std::equal_to<Key>,
  class Allocator = std::unordered_map<Key, Value, Hash, Equal>::allocator_type
> using map = container< std::unordered_map<Key, Value, Hash, Equal, Allocator> >;

template <
  class Key,
  class Value,
  class Compare = std::less<Key>,
  class Allocator = std::map<Key, Value, Compare>::allocator_type
> using ordered_map = container< std::map<Key, Value, Compare, Allocator> >;

template <
  class T,
  class Hash = std::hash<T>,
  class Equal = std::equal_to<T>,
  class Allocator = std::allocator<T>
> using set = container< std::unordered_set<T, Hash, Equal, Allocator> >;

template <
  class T,
  class Compare = std::less<T>,
  class Allocator = std::allocator<T>
> using ordered_set = container< std::set<T, Compare, Allocator> >;


template <typename Map, typename Iterator>
void put_all(Map & m, Iterator first, Iterator last) {
  for (Iterator current = first; current != last; ++current)
    (*m.data())[current->first] = current->second;
}

template<typename Sequence, typename Iterator>
void add_all(Sequence & l, Iterator first, Iterator last) {
  for (Iterator current = first; current != last; ++current)
    l.data()->insert(*current);
}

template <typename HashedContainer>
inline void set_remove(
  HashedContainer & container,
  const typename HashedContainer::value_type & value
) {
  container->erase(value);
}

template <typename HashedContainer>
inline void map_remove(
  HashedContainer & container,
  const typename HashedContainer::container_type::key_type & key
) {
  container->erase(key);
}

template <typename Sequence>
inline bool sequence_contains(
  const Sequence & s, const typename Sequence::value_type & value
) {
  return s->find(value) != s->cend();
}

template <typename Map>
inline bool map_contains(
  const Map & m, const typename Map::key_type & value
) {
  return m->find(value) != m->cend();
}

template <
  class Key,
  class Value,
  class Hash = std::hash<Key>,
  class Equal = std::equal_to<Key>,
  class Allocator = std::unordered_map<Key, Value, Hash, Equal>::allocator_type
> class weak_map
  : public container< std::unordered_map<Key, Value, Hash, Equal, Allocator> >
{
  explicit weak_map()
    : container{ std::make_shared<ContainerBase>() }
  {}

  explicit weak_map(size_t size)
    : container{ std::make_shared<ContainerBase>(size) }
  { }

  explicit weak_map(std::initializer_list<value_type> l)
    : container{ std::make_shared<ContainerBase>(l) }
  { }

  template <typename Iterator>
  weak_map(Iterator first, Iterator last)
    : container{ std::make_shared<ContainerBase>(first, last) }
  { }

  void remove_weak() {
    if (!base || base->empty())
      return;
    container_type copy;
    for (iterator key = base->begin(); key != base->end(); ++key)
      if (!key->first.expired())
        copy.insert(*key);
    base->swap(copy);
  }

  iterator get(const Key & key) {
    iterator it = base->find(key);
    if (it == base->end())
      remove_weak();
    return it;
  }

  const_iterator get(const Key & key) {
    const_iterator it = base->find(key);
    if (it == base->cend())
      remove_weak();
    return it;
  }
};


} // namespace spline



#endif /* CONTAINER_CC_ */
