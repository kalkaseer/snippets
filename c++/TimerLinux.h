/*
 * Copyright (c) 2014-2016 Kareem Alkaseer. All rights reserved.
 */


#ifndef T_TIMERLINUX_H_
#define T_TIMERLINUX_H_

#include <ctime>
#include <csignal>
#include <map>
#include <pthread.h>

#include "TimerBase.h"

#define TIMER_SIGNAL SIGRTMIN
#define CLOCKID      CLOCK_REALTIME
#define TIMER_CODE   SI_TIMER

namespace T {

typedef std::map<timer_t, TimerImpl<Handler> *> __TimerMap;

extern "C" {
void timerSignalHandler(int sigNo, siginfo_t * info, void * uc) {
  if (info->si_code == TIMER_CODE) {
    __TimerMap::iterator it = timerMap.find(info->si_tid);
    if (it != timerMap.end()) {
      it->second->handler();
    }
  }
}
} // extern "C"

template <typename HandlerType>
class TimerImpl {

 public:

# if defined(HAS_INT64)
  typedef uint64 Unit;
# define TimerUnitWrapper(x) UINT64_C(x)
# else
  typedef uint32 Unit;
# define TimerUnitWrapper x ## U
# endif

  typedef HandlerType Handler;

  TimerImpl(Unit interval, Unit initial, Handler & handler)
    : interval_(interval),
      initial_(initial),
      timer_(0),
      handler_(handler)
  { }

  ~TimerImpl() {
    if (timer_) stop();
  }

  bool start() {
    sigevent_t sevent;
    itimerspec spec;
    sigset_t   mask;
    struct sigaction  sa;

    sa.sa_flags = SA_SIGINFO;
    sa.sa_sigaction = timerSignalHandler;
    sigemptyset(&sa.sa_mask);

    if (sigaction(TIMER_SIGNAL, &sa, NULL) == -1) {
      return false;
    }

    // Block timer signal temporarily.

    sigemptyset(&mask);
    sigaddset(&mask, TIMER_SIGNAL);

    if (pthread_sigmask(SIG_SETMASK, &mask, NULL) != 0) {
      return false;
    }

    // Create timer

    sevent.sigev_notify = SIGEV_SIGNAL;
    sevent.sigev_signo  = TIMER_SIGNAL;
    sevent.sigev_value.sival_ptr = &timer_;

    if (timer_create(CLOCKID, &sevent, &timer_) != 0) {
      pthread_sigmask(SIG_UNBLOCK, &mask, NULL);
      return false;
    }

    // Start the timer

    spec.it_value.tv_sec  = initial_ / NANOS_IN_SEC_U;
    spec.it_value.tv_nsec = initial_ % NANOS_IN_SEC_U;
    spec.it_interval.tv_sec  = interval_ / NANOS_IN_SEC_U;
    spec.it_interval.tv_nsec = interval_ % NANOS_IN_SEC_U;

    if (timer_settime(timer_, 0, &spec, NULL) != 0) {
      pthread_sigmask(SIG_UNBLOCK, &mask, NULL);
      return false;
    }

    timerMap[timer_] = this;

    // Unlock signal so timer notification can be delivered
    pthread_sigmask(SIG_UNBLOCK, &mask, NULL);

    return true;
  }

  bool stop() {

    bool result = timer_delete(timer_);

    sigset_t mask;
    sigemptyset(&mask);
    sigaddset(&mask, TIMER_SIGNAL);

    if (pthread_sigmask(SIG_SETMASK, &mask, NULL) != 0) {
      return false;
    }

    signal(TIMER_SIGNAL, SIG_DFL);

    if (pthread_sigmask(SIG_UNBLOCK, &mask, NULL) != 0) {
      return false;
    }

    if (result == -1)
      return false;

    timerMap.erase(timer_);
    timer_ = nullptr;

    return true;
  }

 private:

  Unit    interval_;
  Unit    initial_;
  timer_t timer_;
  Handler handler_;
};

template <typename Handler> Timer< TimerImpl< Handler > > createTimer(
  TimerImpl<Handler>::Unit interval,
  TimerImpl<Handler>::Unit initial,
  Handler &                handler
) {
  return Timer< TimerImpl< Handler > >(TimerImpl< Handler >(interval, initial, handler));
}

} // namespace T


#endif /* T_TIMERLINUX_H_ */
