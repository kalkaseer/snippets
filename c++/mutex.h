/*
 * Copyright (c) 2016 Kareem Alkaseer. All rights reserved.
 */

#ifndef DASKY_SEARCH_SYNCHRONIZE_H_
#define DASKY_SEARCH_SYNCHRONIZE_H_

#include <mutex>
#include <thread>
#include "common.h"

namespace spline {

DECLARE_SHARED_PTR(mutex);

class SPLINEAPI mutex {

public:

  mutex();

  virtual ~mutex();

  static void create(mutex_ptr & ptr);

  void lock(int32 timeout = 0);

  void unlock();

  int32 unlock_all();

  bool holds_lock();

  std::recursive_timed_mutex & mutex();


protected:

  std::recursive_timed_mutex mtx;
  std::thread::id thread;
  int32 recursion;
};

} // namespace spline

#endif /* DASKY_SEARCH_SYNCHRONIZE_H_ */
