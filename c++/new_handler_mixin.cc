/*
 * new_handler_mixin.cc
 *
 *  Created on: Sep 5, 2016
 *      Author: Kareem Alkaseer
 */

#include <new>
#include <iostream>

template< class T >
class new_handler_mixin {

public:

  using new_handler = std::new_handler;

  static new_handler set_new_handler(new_handler h);

  static void * operator new(size_t size);

private:

  static new_handler m_current_handler;
};

template< class T >
typename new_handler_mixin<T>::new_handler
new_handler_mixin<T>::set_new_handler(new_handler h) {
  new_handler old = m_current_handler;
  m_current_handler = h;
  return old;
}

template< class T >
void * new_handler_mixin<T>::operator new(size_t size) {

  new_handler global_handler = std::set_new_handler(m_current_handler);
  void * memory;
  try {
    memory = ::operator new(size);
  } catch (const std::bad_alloc &) {
    std::set_new_handler(global_handler);
    throw;
  }

  std::set_new_handler(global_handler);

  return memory;
}

template< class T >
typename new_handler_mixin<T>::new_handler
new_handler_mixin<T>::m_current_handler;



int main() {

  class test : public new_handler_mixin<test> { };

  auto handler = []() { throw std::bad_alloc{}; };

  test::set_new_handler(handler);
  test * t = new test();
  test::set_new_handler(nullptr);
  delete t;

  return 0;
}
