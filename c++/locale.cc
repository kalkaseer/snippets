#include <iostream>
#include <locale>
#include <string>
#include <codecvt>
#include <iomanip>

void global_locale() {
	
	std::wcout << "User-preferred locale is "
		         << std::locale("").name().c_str() << std::endl;

	// on startup, the global locale is the "C" locale
	std::wcout << 1000.01 << std::endl;

	// replace the C++ global locale and the C locale with the user-preferred one
	std::locale::global(std::locale(""));

	// associate the new global locale with wcout
	std::wcout.imbue(std::locale(""));

	std::wcout << 1000.01 << std::endl;
}



// utility wrapper to adapt locale-bound facets for wstring and wbuffer
// conversions.
template< class Facet >
struct deletable_facet : Facet {
	
	template< class... Args >
	deletable_facet(Args &&... args)
		: Facet(std::forward<Args>(args)...)
	{ }
	
	~deletable_facet() { }
};

void utf8_to_utf16() {
	
	// equivelant to u8"zß水🍌" or "\x7a\xc3\x9f\xe6\xb0\xb4\xf0\x9f\x8d\x8c"
	std::string data = u8"z\u00df\u6c34\U0001f34c";
	
	/*
	using supplied locale's codecvt facet based conversion
	std::ofstream("file.txt") << data; // writing
	std::wifsream fin("file.txt");     // reading
	fin.imbue(std::local("en_US.UTF-8")); // prepare the stream
	// print to std::cout
	std::cout << "The UTF-8 file contains the following UCS4 code points:\n";
	for (wchar_t c; fin >> c; )
		std::cout << "U+" << std::hex :: std::setw(4) << std::setfill('0')
		          << c << std::endl;
	*/
	
	// using standard (locale-independent) codecvt facet based conversion
	std::wstring_convert<
		deletable_facet<std::codecvt<char16_t, char, std::mbstate_t>>,
		char16_t
	> conv16;
	
		std::u16string str16 = conv16.from_bytes(data);
		
		std::cout << "UTF-8 code points:\n";
		std::cout << data << std::endl;
		
		std::cout << "UTF-8 string contains the following UTF-16 code points:\n";
		for (char16_t c : str16)
			std::cout << "U+" << std::hex << std::setw(4) << std::setfill('0')
			          << c << std::endl;
}



int main() {
	
	global_locale();
	utf8_to_utf16();
	return 0;
}