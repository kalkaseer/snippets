/*
 * Copyright (c) 2013-2016 Kareem Alkaseer.
 */

#ifndef COMMONOPERATIONS_H_
#define COMMONOPERATIONS_H_

#include "datastore/thrift/Operation.h"

namespace datastore {
namespace HtThrift {

namespace alloc {

struct GetSerializedCellsOperation : public Operation {

  GetSerializedCellsOperation(
    ContextPool       * const p,
    const std::string & n,
    const std::string & t,
    const ScanSpec    & s
  ) : Operation(p),
      ns(n),
      table(t),
      spec(s),
      result(nullptr)
  { }

  void * operator() () {
    context_->client()->get_cells_serialized(
      result, context_->getNamespace(ns), table.data(), spec);
    return &result;
  }

  const std::string ns;
  const std::string table;
  const ScanSpec    spec;
  SerializedCells   result;
};

struct GetCellsOperation : public Operation {

  GetCellsOperation(
    ContextPool       * const p,
    const std::string & n,
    const std::string & t,
    const ScanSpec    & s
  ) : Operation(p),
      ns(n),
      table(t),
      spec(s)
  { }

  void * operator() () {
    SerializedCells cells;
    Cell hcell;
    context_->client()->get_cells_serialized(
      cells, context_->getNamespace(ns), table.data(), spec);
    SerializedCellsReader reader(cells.data(), cells.size());
    while(reader.next()) {
      reader.get(hcell);
      result.push_back(hcell);
    }
    return &result;
  }

  const std::string ns;
  const std::string table;
  const ScanSpec    spec;
  Cells             result;
};

struct SetCellsOperation : public Operation {

  SetCellsOperation(
    ContextPool       * const p,
    const std::string & n,
    const std::string & t,
    const Cells       & dbc
  ) : Operation(p),
      ns(n),
      table(t),
      cells(dbc),
      result(false)
  { }

  void * operator() () {
    SerializedCellsWriter writer(1024);
    SerializedCells serialized;
    for (Cells::const_iterator it = cells.begin(); it != cells.end(); ++it) {
      writer.add(
        it->row_key, it->column_family, it->column_qualifier,
        it->timestamp, it->value, it->value_len, it->flag);
    }
    writer.finalize(0);
    serialized.append(
      (const char *) writer.get_buffer(), writer.get_buffer_length());
    context_->client()->set_cells_serialized(
      context_->getNamespace(ns), table.data(), serialized);

    result = true;
    return &result;
  }

  const std::string ns;
  const std::string table;
  const Cells       cells;
  bool              result;
};

struct CreateUniqueCell : public Operation {

  CreateUniqueCell(
    ContextPool       * const p,
    const std::string & n,
    const std::string & t,
    const Key         & k,
    const std::string & v
  ) : Operation(p),
      ns(n),
      table(t),
      key(k),
      value(v),
      result(false)
  { }

  void * operator() () {
    std::string ret;
    context_->client()->create_cell_unique(ret, context_->getNamespace(ns), table, key, value);
    result = true;
    return &result;
  }

  const std::string ns;
  const std::string table;
  const Key         key;
  const std::string value;
  bool              result;
};

} // namespace alloc

namespace unalloc {

struct GetSerializedCellsOperation : public Operation {

  GetSerializedCellsOperation(
    ContextPool       * const p,
    const std::string * const n,
    const std::string * const t,
    const ScanSpec    * const s
  ) : Operation(p),
      ns(n),
      table(t),
      spec(s),
      result(nullptr)
  { }

  void * operator() () {
    context_->client()->get_cells_serialized(
      result, context_->getNamespace(*ns), table->c_str(), *spec);
    return &result;
  }

  const std::string * ns;
  const std::string * table;
  const ScanSpec    * spec;
  SerializedCells     result;
};

struct GetCellsOperation : public Operation {

  GetCellsOperation(
    ContextPool       * const p,
    const std::string * const n,
    const std::string * const t,
    const ScanSpec    * const s
  ) : Operation(p),
      ns(n),
      table(t),
      spec(s)
  { }

  void * operator() () {
    SerializedCells cells;
    Cell hcell;
    context_->client()->get_cells_serialized(
      cells, context_->getNamespace(*ns), table->c_str(), *spec);
    SerializedCellsReader reader(cells.data(), cells.size());
    while(reader.next()) {
      reader.get(hcell);
      result.push_back(hcell);
    }
    return &result;
  }

  const std::string * ns;
  const std::string * table;
  const ScanSpec    * spec;
  Cells               result;
};

struct SetCellsOperation : public Operation {

  SetCellsOperation(
    ContextPool       * const p,
    const std::string * const n,
    const std::string * const t,
    const Cells       * const dbc
  ) : Operation(p),
      ns(n),
      table(t),
      cells(dbc),
      result(false)
  { }

  void * operator() () {
    SerializedCellsWriter writer(1024);
    SerializedCells serialized;
    for (Cells::const_iterator it = cells->begin(); it != cells->end(); ++it) {
      writer.add(
        it->row_key, it->column_family, it->column_qualifier,
        it->timestamp, it->value, it->value_len, it->flag);
    }
    writer.finalize(0);
    serialized.append(
      (const char *) writer.get_buffer(), writer.get_buffer_length());
    context_->client()->set_cells_serialized(
      context_->getNamespace(*ns), table->c_str(), serialized);

    result = true;
    return &result;
  }

  const std::string * ns;
  const std::string * table;
  const Cells       * cells;
  bool                result;
};

struct CreateUniqueCell : public Operation {

  CreateUniqueCell(
    ContextPool       * const p,
    const std::string * const n,
    const std::string * const t,
    const Key         * const k,
    const std::string * const v
  ) : Operation(p),
      ns(n),
      table(t),
      key(k),
      value(v),
      result(false)
  { }

  void * operator() () {
    std::string ret;
    context_->client()->create_cell_unique(ret, context_->getNamespace(*ns), *table, *key, *value);
    result = true;
    return &result;
  }

  const std::string * ns;
  const std::string * table;
  const Key         * key;
  const std::string * value;
  bool                result;
};

} // namespace unalloc

} /* namespace HtThrift */
} /* namespace datastore */


#endif /* COMMONOPERATIONS_H_ */
