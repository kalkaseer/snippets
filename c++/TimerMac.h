/*
 * Copyright (c) 2014-2016 Kareem Alkaseer. All rights reserved.
 * KAREEM ALKASEER PROPRIETARY/CONFIDENTIAL.
 */

#ifndef T_TIMERMAC_H_
#define T_TIMERMAC_H_

#include <dispatch/dispatch.h>

#include "TimerBase.h"

namespace T {

class TimerImpl {

  typedef uint64_t            Unit;
  typedef dispatch_function_t Handler;

 public:

  TimerImpl(Unit interval, Unit leeway, Handler handler)
    : interval_(interval),
      leeway_(leeway),
      queue_(dispatch_get_main_queue()),
      handler_(handler),
      timer_(NULL)
  { }

  ~TimerImpl() {
    if (timer_) stop();
  }

  bool start() {
    timer_ = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, queue_);
    if (timer_) {
      dispatch_source_set_timer(timer_, dispatch_walltime(NULL, 0), interval_, leeway_);
      dispatch_source_set_event_handler_f(timer_, handler_);
      dispatch_resume(timer_);
      return true;
    }
    return false;
  }

  bool stop() {
    dispatch_source_cancel(timer_);
    timer_ = nullptr;
    return true;
  }

 private:

  Unit              interval_;
  Unit              leeway_;
  dispatch_queue_t  queue_;
  Handler           handler_;
  dispatch_source_t timer_;
};

Timer<TimerImpl> createTimer(
  TimerImpl::Unit     interval,
  TimerImpl::Unit     leeway,
  TimerImpl::Handler  handler
) {
  return Timer< TimerImpl >(TimerImpl(interval, leeway, handler));
}

} // namespace T

#endif /* T_TIMERMAC_H_ */
