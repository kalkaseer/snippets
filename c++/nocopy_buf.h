/*
 * Copyright (c) 2016 Kareem Alkaseer. All rights reserved.
 */


#ifndef BASE_NOCOPY_BUF_H_
#define BASE_NOCOPY_BUF_H_

#include <streambuf>
#include "base/string_view.h"

namespace blink {

template< class CharT, class Traits = std::char_traits<CharT> >
class NoCopyBuffer
  : public std::basic_streambuf<CharT, Traits>
{

public:

  typedef CharT char_type;

  typedef Traits traits_type;

  NoCopyBuffer(char_type * data, std::streamsize n)
    : std::basic_streambuf<CharT, Traits>()
  {
    this->setg(data, data, data + n);
  }

  virtual ~NoCopyBuffer() { }

  virtual
  NoCopyBuffer<CharT, Traits> * setbuf(char_type * s, std::streamsize n) {
    this->setg(s, s, s + n);
    return this;
  }

};

typedef
  NoCopyBuffer<std::string::value_type, std::string::traits_type>
  NoCopyStringBuffer;

typedef
  NoCopyBuffer<string_view::value_type, string_view::traits_type>
  NoCopyStringViewBuffer;

typedef NoCopyBuffer<char> NoCopyCharBuffer;

} // namespace blink



#endif /* BASE_NOCOPY_BUF_H_ */
