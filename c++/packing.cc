#include <iostream>
#include <limits>
#include <cstdint>

#if (defined(__clang__) && \
  ((__clang_major__ > 3) || ((__clang_major == 3) && (__clang_minor__ >= 2))))
# define __clang_supports_int128 1
#else
# undef __clang_supports_int128
#endif
    

template< class I >
inline void to_bytes(I x, unsigned char * b) {
  int shift = 8 * (sizeof(x) - 1);
  for (int i = 0; i < sizeof(x); ++i) {
    b[i] = static_cast<unsigned char>((x >> shift) & 0xff);
    shift -= 8;
  }
}

template< class I >
inline void to_int(I & x, const unsigned char * b) {
  int shift = 8 * (sizeof(x) - 1);
  for (int i = 0; i < sizeof(x); ++i) {
    x |= static_cast<I>(b[i]) << shift;
    shift -= 8;
  }
}

#if defined(INT16_MAX)

inline void to_bytes(int16_t x, unsigned char * b) {
  b[0] = static_cast<unsigned char>((x >> 8) & 0xff);
  b[1] = static_cast<unsigned char>(x & 0xff);
}

inline void to_bytes(uint16_t x, unsigned char * b) {
  b[0] = static_cast<unsigned char>((x >> 8) & 0xff);
  b[1] = static_cast<unsigned char>(x & 0xff);
}

inline void to_int(uint16_t &x, const unsigned char * b) {
  x = (static_cast<uint16_t>(b[0]) << 8) | static_cast<uint16_t>(b[1]);
}

inline void to_int(int16_t &x, const unsigned char * b) {
  x = (static_cast<int16_t>(b[0]) << 8) | static_cast<int16_t>(b[1]);
}
#endif

#if defined(INT32_MAX)

inline void to_bytes(int32_t x, unsigned char * b) {
  b[0] = static_cast<unsigned char>((x >> 24) & 0xff);
  b[1] = static_cast<unsigned char>((x >> 16) & 0xff);
  b[2] = static_cast<unsigned char>((x >>  8) & 0xff);
  b[3] = static_cast<unsigned char>(x & 0xff);
}

inline void to_bytes(uint32_t x, unsigned char * b) {
  b[0] = static_cast<unsigned char>((x >> 24) & 0xff);
  b[1] = static_cast<unsigned char>((x >> 16) & 0xff);
  b[2] = static_cast<unsigned char>((x >>  8) & 0xff);
  b[3] = static_cast<unsigned char>(x & 0xff);
}

inline void to_int(int32_t &x, const unsigned char * b) {
  x = static_cast<int32_t>(b[0] << 24)
    | static_cast<int32_t>(b[1] << 16)
    | static_cast<int32_t>(b[2] << 8)
    | static_cast<int32_t>(b[4]);
}

inline void to_int(uint32_t &x, const unsigned char * b) {
  x =  (static_cast<uint32_t>(b[0]) << 24)
    | (static_cast<uint32_t>(b[1]) << 16)
    | (static_cast<uint32_t>(b[2]) << 8)
    | static_cast<uint32_t>(b[4]);
}
#endif

#if defined(INT64_MAX)

inline void to_bytes(int64_t x, unsigned char * b) {
  b[0] = static_cast<unsigned char>((x >> 56) & 0xff);
  b[1] = static_cast<unsigned char>((x >> 48) & 0xff);
  b[2] = static_cast<unsigned char>((x >> 40) & 0xff);
  b[3] = static_cast<unsigned char>((x >> 32) & 0xff);
  b[4] = static_cast<unsigned char>((x >> 24) & 0xff);
  b[5] = static_cast<unsigned char>((x >> 16) & 0xff);
  b[6] = static_cast<unsigned char>((x >>  8) & 0xff);
  b[7] = static_cast<unsigned char>(x & 0xff);
}

inline void to_bytes(uint64_t x, unsigned char * b) {
  b[0] = static_cast<unsigned char>((x >> 56) & 0xff);
  b[1] = static_cast<unsigned char>((x >> 48) & 0xff);
  b[2] = static_cast<unsigned char>((x >> 40) & 0xff);
  b[3] = static_cast<unsigned char>((x >> 32) & 0xff);
  b[4] = static_cast<unsigned char>((x >> 24) & 0xff);
  b[5] = static_cast<unsigned char>((x >> 16) & 0xff);
  b[6] = static_cast<unsigned char>((x >>  8) & 0xff);
  b[7] = static_cast<unsigned char>(x & 0xff);
}

inline void to_int(int64_t &x, const unsigned char * b) {
  x =   
      (static_cast<int64_t>(b[0]) << 56)
    | (static_cast<int64_t>(b[1]) << 48)
    | (static_cast<int64_t>(b[2]) << 40)
    | (static_cast<int64_t>(b[3]) << 32)
    | (static_cast<int64_t>(b[4]) << 24)
    | (static_cast<int64_t>(b[5]) << 16)
    | (static_cast<int64_t>(b[6]) << 8)
    | (static_cast<int64_t>(b[7]));
}

inline void to_int(uint64_t &x, const unsigned char * b) {
  x =  
      (static_cast<uint64_t>(b[0]) << 56)
    | (static_cast<uint64_t>(b[1]) << 48)
    | (static_cast<uint64_t>(b[2]) << 40)
    | (static_cast<uint64_t>(b[3]) << 32)
    | (static_cast<uint64_t>(b[4]) << 24)
    | (static_cast<uint64_t>(b[5]) << 16)
    | (static_cast<uint64_t>(b[6]) << 8)
    | (static_cast<uint64_t>(b[7]));
}
#endif

#if defined(__GNUC__) || defined(__clang_supports_int128)

inline void to_bytes(__int128_t & x, unsigned char * b) {
  b[0]  = static_cast<unsigned char>((x >> 120) & 0xff);
  b[1]  = static_cast<unsigned char>((x >> 112) & 0xff);
  b[2]  = static_cast<unsigned char>((x >> 104) & 0xff);
  b[3]  = static_cast<unsigned char>((x >> 96)  & 0xff);
  b[4]  = static_cast<unsigned char>((x >> 88)  & 0xff);
  b[5]  = static_cast<unsigned char>((x >> 80)  & 0xff);
  b[6]  = static_cast<unsigned char>((x >> 72)  & 0xff);
  b[7]  = static_cast<unsigned char>((x >> 64)  & 0xff);
  b[8]  = static_cast<unsigned char>((x >> 56)  & 0xff);
  b[9]  = static_cast<unsigned char>((x >> 48)  & 0xff);
  b[10] = static_cast<unsigned char>((x >> 40)  & 0xff);
  b[11] = static_cast<unsigned char>((x >> 32)  & 0xff);
  b[12] = static_cast<unsigned char>((x >> 24)  & 0xff);
  b[13] = static_cast<unsigned char>((x >> 16)  & 0xff);
  b[14] = static_cast<unsigned char>((x >>  8)  & 0xff);
  b[15] = static_cast<unsigned char>(x & 0xff);
}

inline void to_bytes(__uint128_t x, unsigned char * b) {
  b[0]  = static_cast<unsigned char>((x >> 120) & 0xff);
  b[1]  = static_cast<unsigned char>((x >> 112) & 0xff);
  b[2]  = static_cast<unsigned char>((x >> 104) & 0xff);
  b[3]  = static_cast<unsigned char>((x >> 96)  & 0xff);
  b[4]  = static_cast<unsigned char>((x >> 88)  & 0xff);
  b[5]  = static_cast<unsigned char>((x >> 80)  & 0xff);
  b[6]  = static_cast<unsigned char>((x >> 72)  & 0xff);
  b[7]  = static_cast<unsigned char>((x >> 64)  & 0xff);
  b[8]  = static_cast<unsigned char>((x >> 56)  & 0xff);
  b[9]  = static_cast<unsigned char>((x >> 48)  & 0xff);
  b[10] = static_cast<unsigned char>((x >> 40)  & 0xff);
  b[11] = static_cast<unsigned char>((x >> 32)  & 0xff);
  b[12] = static_cast<unsigned char>((x >> 24)  & 0xff);
  b[13] = static_cast<unsigned char>((x >> 16)  & 0xff);
  b[14] = static_cast<unsigned char>((x >>  8)  & 0xff);
  b[15] = static_cast<unsigned char>(x & 0xff);
}

inline void to_int(__int128_t &x, const unsigned char * b) {
  x =  
    (static_cast<__int128_t>(b[0])  << 120)
  | (static_cast<__int128_t>(b[1])  << 112)
  | (static_cast<__int128_t>(b[2])  << 104)
  | (static_cast<__int128_t>(b[3])  << 96)
  | (static_cast<__int128_t>(b[4])  << 88)
  | (static_cast<__int128_t>(b[5])  << 80)
  | (static_cast<__int128_t>(b[6])  << 72)
  | (static_cast<__int128_t>(b[7])  << 64)
  | (static_cast<__int128_t>(b[8])  << 56)
  | (static_cast<__int128_t>(b[9])  << 48)
  | (static_cast<__int128_t>(b[10]) << 40)
  | (static_cast<__int128_t>(b[11]) << 32)
  | (static_cast<__int128_t>(b[12]) << 24)
  | (static_cast<__int128_t>(b[13]) << 16)
  | (static_cast<__int128_t>(b[14]) << 8)
  | (static_cast<__int128_t>(b[15]));
}

inline void to_int(__uint128_t &x, const unsigned char * b) {
  x =  
    (static_cast<__uint128_t>(b[0])  << 120)
  | (static_cast<__uint128_t>(b[1])  << 112)
  | (static_cast<__uint128_t>(b[2])  << 104)
  | (static_cast<__uint128_t>(b[3])  << 96)
  | (static_cast<__uint128_t>(b[4])  << 88)
  | (static_cast<__uint128_t>(b[5])  << 80)
  | (static_cast<__uint128_t>(b[6])  << 72)
  | (static_cast<__uint128_t>(b[7])  << 64)
  | (static_cast<__uint128_t>(b[8])  << 56)
  | (static_cast<__uint128_t>(b[9])  << 48)
  | (static_cast<__uint128_t>(b[10]) << 40)
  | (static_cast<__uint128_t>(b[11]) << 32)
  | (static_cast<__uint128_t>(b[12]) << 24)
  | (static_cast<__uint128_t>(b[13]) << 16)
  | (static_cast<__uint128_t>(b[14]) << 8)
  | (static_cast<__uint128_t>(b[15]));
}
#endif

using namespace std;

template< class I >
void check(I i) {
  unsigned char b[sizeof(i)];
  to_bytes(i, b);
  I x;
  to_int(x, b);
  cout << "n: " << sizeof(i) << " x == i: " << boolalpha << (x == i) << '\n';
}

int main() {
  
  check(numeric_limits<int16_t>::max());
  check(numeric_limits<uint16_t>::max());
  check(numeric_limits<int32_t>::max());
  check(numeric_limits<uint32_t>::max());
  check(numeric_limits<int64_t>::max());
  check(numeric_limits<uint64_t>::max());
  
#if defined(__GNUC__) || defined(__clang_supports_int128)
  check(numeric_limits<__int128_t>::max());
  check(numeric_limits<__uint128_t>::max());
#endif
  
  return 0;
}