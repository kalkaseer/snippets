/* Split strings methods */

#include <vector>
#include <string>
#include <sstream>
#include <iostream>
#include <locale>
#include <algorithm>
#include <iterator>
#include <unordered_set>


template< class F, class... Args >
uint64_t timeit(std::size_t n, F & func, Args &&... args) {

  using namespace std::chrono;

  nanoseconds nanos;

  for (std::size_t i = 0; i < n; ++i) {
    auto start = high_resolution_clock::now();
    func(std::forward<Args>(args)...);
    auto end = high_resolution_clock::now();
    nanos = end - start;
  }

  return nanos.count();
}

// Faster than boost's split
std::vector<std::string> split_1_nodelim(const std::string & s) {
	
	using namespace std;
	vector<string> out;
	istringstream ss(s);                // we can use non-copying stream
	copy(istream_iterator<string>(ss), 
	     istream_iterator<string>(),
	     back_inserter(out));
	 return std::move(out);
}

struct whitespace : std::ctype<char> {
	
	explicit whitespace(const std::string & s)
		: std::ctype<char>{}
	{ 
		m_chars.insert(s.begin(), s.end());
	}
	
	whitespace(std::string && s)
		: std::ctype<char>{}
	{
		m_chars.insert(
			std::make_move_iterator(s.begin()),
			std::make_move_iterator(s.end()));
	}
	
	bool is(mask m, char_type c) const {
		if ((m & space) && m_chars.count(c)) {
			return true;
		}
		return std::ctype<char>::is(m, c);
	}
	
private:
	std::unordered_set<char> m_chars;
};

std::vector<std::string> split_1(const std::string & s, const std::string & delim) {
	
	using namespace std;
	vector<string> out;
	istringstream ss(s);                // we can use non-copying stream
	ss.imbue(std::locale(ss.getloc(), new whitespace(delim)));
	copy(istream_iterator<string>(ss), 
	     istream_iterator<string>(),
	     back_inserter(out));
	 return std::move(out);
}

// Can be optimised as constexpr if delimiters are known at compile-time.
// The loop in 'is' would be unrolled using templates at compile-time.
struct whitespace_string : std::ctype<char> {
	
	explicit whitespace_string(const std::string & s)
		: std::ctype<char>{}, m_chars{s}
	{ }
	
	whitespace_string(std::string && s)
		: std::ctype<char>{}, m_chars{std::move(s)}
	{ }
	
	bool is(mask m, char_type c) const {
		if ((m & space)) {
			for (auto ch : m_chars)
				if (c == ch)
					return true;
		}
		return std::ctype<char>::is(m, c);
	}
	
private:
	std::string m_chars;
};

// Fastest if multiple delimiters are needed
std::vector<std::string> split_1_2(const std::string & s, const std::string & delim) {
	
	using namespace std;
	vector<string> out;
	istringstream ss(s);                // we can use non-copying stream
	ss.imbue(std::locale(ss.getloc(), new whitespace_string(delim)));
	copy(istream_iterator<string>(ss), 
	     istream_iterator<string>(),
	     back_inserter(out));
	 return std::move(out);
}

// Fastest if only one delimiter is needed
std::vector<std::string> split_2(const std::string & s, char delim) {
	
	using namespace std;
	vector<string> out;
	string token;
	istringstream ss(s);                // we can use non-copying stream
	while (getline(ss, token, delim))
		out.push_back(token);
	return std::move(out);
}

#include <boost/algorithm/string.hpp>
std::vector<std::string> split_3(const std::string & s, const std::string & delim) {
	
	std::vector<std::string> out;
	boost::split(out, s, boost::is_any_of(delim));
	return std::move(out);
}

int main() {
	
	std::size_t loops = 10000;
	std::string s1 = "hello there,:I'd say~goodbye";
	std::string delim = " :~";
	
	std::string s2 = "hello there, I'd say goodbye";
	
	auto s1_time = timeit(loops, split_1, std::ref(s1), std::ref(delim));
	auto s1_2_time = timeit(loops, split_1_2, s1, delim);
	auto s3_time = timeit(loops, split_3, s1, delim);
	
	auto s1c_time = timeit(loops, split_1_nodelim, s2);
	auto s2c_time = timeit(loops, split_2, s2, ' ');
	
	std::string delim2{ 1, ' ' };
	auto s3c_time = timeit(loops, split_3, s2, delim2);
	
	std::cout << "split_1 (delimiter): " << s1_time << std::endl;
	std::cout << "split_1_2 (delimiter): " << s1_2_time << std::endl;
	std::cout << "split_3 (delimiter): " << s3_time << std::endl;
	
	std::cout << "split_1 (non-delimiter): " << s1c_time << std::endl;
	std::cout << "split_2 (non-delimiter): " << s2c_time << std::endl;
	std::cout << "split_3 (non-delimiter): " << s3c_time << std::endl;
	
	return 0;
}

