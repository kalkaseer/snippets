#include <boost/interprocess/shared_memory_object.hpp>
#include <boost/interprocess/mapped_region.hpp>
#include <boost/interprocess/managed_shared_memory.hpp>
#include <boost/interprocess/allocators/allocator.hpp>
#include <boost/interprocess/containers/string.hpp>
#include <iostream>

// Do not automatically link Boost.DateTime
#define BOOST_DATE_TIME_NO_LIB

#define IS_WINDOWS defined(WIN32) || defined(_WIN32) || defined(__WIN32)

namespace ipc = boost::interprocess;

struct shm_remover {
  shm_remover(const char * n) : name{ n } {}
  ~shm_remover() { ipc::shared_memory_object::remove(name); }
  
  const char * name;
};

constexpr const char * k_shm_name = "Boost.IPC";

void shared_object() {
  
  // Ensure shared memory is deleted upon exist
  shm_remover remover{ k_shm_name };
  
  // create shared memory object
#if !IS_WINDOWS
  ipc::shared_memory_object shm{
    ipc::open_or_create, k_shm_name, ipc::read_write };
#else
  // deleted when last process using it is terminated
  ipc::windows_shared_memory shm{
    ipc::open_or_create, "Boost.IPC", ipc::read_write, 1024 };
#endif
  
  // initially zero, resize
  shm.truncate(1024);
  
  // map the shared memory object into the process
  ipc::mapped_region region1{ shm, ipc::read_write };
  std::cout << std::hex << region1.get_address() << '\n';
  std::cout << std::dec << region1.get_size() << '\n';
  
  int *i1 = static_cast<int *>(region1.get_address());
  *i1 = 99; // write 99 to the beginning of the shared memory object
  
  // another mapping of the same shared memory object into the process
  ipc::mapped_region region2{ shm, ipc::read_only };
  std::cout << std::hex << region2.get_address() << '\n';
  std::cout << std::dec << region2.get_size() << '\n';
  
  // read the data at the beginning of the shared memory object
  int *i2 = static_cast<int *>(region2.get_address());
  std::cout << "read: " << *i2 << '\n';
}

void managed_memory() {
  
  // Ensure shared memory is deleted upon exist
  shm_remover remover{ k_shm_name };
  
  ipc::managed_shared_memory shm{ ipc::open_or_create, k_shm_name, 1024 };
  
  int *i = shm.find_or_construct<int>("Integer")(99);
  std::cout << "i: " << *i << '\n';
  std::pair<int *, std::size_t> p = shm.find<int>("Integer");
  if (p.first)
    std::cout << "i: " << *p.first << " size: " << p.second << '\n';
  
  int *ia = shm.construct<int>("IntArray")[10](50);
  std::cout << "ia: " << *ia << '\n';
  p = shm.find<int>("IntArray");
  if (p.first)
    std::cout << "ia: " << *p.first << " size: " << p.second << '\n';
  
  try {
    int *ia2 = shm.construct<int>("IntArray2")[4096](99);
  } catch (ipc::bad_alloc & ex) {
    std::cerr << "error: " << ex.what() << '\n';
  }
  
  bool removed = shm.destroy<int>("Integer");
  std::cout << "removed i: " << std::boolalpha << removed << '\n';
  removed = shm.destroy<int>("IntArray");
  std::cout << "removed ia: " << std::boolalpha << removed << '\n';
  removed = shm.destroy<int>("IntArray2");
  std::cout << "removed ia2: " << std::boolalpha << removed << '\n';
  
  
}

void containers() {
  
  shm_remover remover{ k_shm_name };
  
  ipc::managed_shared_memory shm{ ipc::open_or_create, k_shm_name, 1024 };
  
  using char_allocator =
    ipc::allocator<char, ipc::managed_shared_memory::segment_manager>;
  
  using string =
    ipc::basic_string<char, std::char_traits<char>, char_allocator>;
  
  string * s = shm.find_or_construct<string>("String")
    ("Hello!", shm.get_segment_manager());
  
  s->insert(5, " world");
  
  std::cout << "string: " << *s << '\n';
}

void atomic_operations() {
  
  shm_remover remover{ k_shm_name };
  ipc::managed_shared_memory shm{ ipc::open_or_create, k_shm_name, 1024 };
  
  auto f = [&]() {
    shm.construct<int>("Integer")(99);
    shm.construct<float>("Float")(3.14);
  };
  
  shm.atomic_func(f);
  
  std::cout << "integer: " << *shm.find<int>("Integer").first << '\n';
  std::cout << "float: " << *shm.find<float>("Float").first << '\n';
}

int main() {
  
  std::cout << "=> shared object:\n";
  shared_object();
  
  std::cout << "\n=> managed_memory:\n";
  managed_memory();
  
  std::cout << "\n=> containers:\n";
  containers();
  
  std::cout << "\n=> atomic operations:\n";
  atomic_operations();
  
  return 0;
}