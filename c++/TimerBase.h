/*
 * Copyright (c) 2014-2016 Kareem Alkaseer. All rights reserved.
 */
#ifndef T_TIMERBASE_H_
#define T_TIMERBASE_H_

#include <ctime>
#include <csignal>
#include "BasicTypes.h"

namespace T {

template<typename TimerImpl>
class Timer {
 public:

  typedef TimerImpl TimerImplType;

  Timer(TimerImplType impl)
    : impl_(impl)
  { }

  virtual ~Timer() { };

  bool start()    { return impl.start() };
  bool stop()     { return impl.stop() };
  void release(); { impl.release(); };

 private:

  TimerImplType impl_;
};

} /* namespace T */

#endif /* T_TIMERBASE_H_ */
