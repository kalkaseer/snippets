/* Copyright @2016 Kareem Alkaseer */

#ifndef BASE_GENERIC_TRAITS_H_
#define BASE_GENERIC_TRAITS_H_

#include <type_traits>
#include <array>
#include <string>
#include <vector>
#include <deque>
#include <queue>
#include <list>
#include <forward_list>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <stack>
#include <tuple>
#include <utility>


namespace blink {
namespace type_traits {

#if __cplusplus < 201402L
  using true_type = std::integral_constant<bool, true>;
  using false_type = std::integral_constant<bool, false>;

  template< bool B, class T, class F >
  using conditional_t = typename std::conditional<B, T, F>::type;

  template< class T>
  using decay_t = typename std::decay<T>::type;
#else
  using true_type = std::true_type;
  using false_type = std::false_type;
  template< bool B, class T, class F >
  using conditional_t = std::conditional_t< B, T, F >;
  template< class T >
  using decay_t = std::decay_t< T >;
#endif


template< typename... T>
struct is_container_helper {};

template< typename T, typename _ = void>
struct is_container : public false_type {};

template< typename T>
struct is_container<
  T,
  conditional_t<
    false,
    is_container_helper<
      typename T::value_type,
      typename T::size_type,
      typename T::allocator_type,
      typename T::iterator,
      typename T::const_iterator,
      decltype(std::declval<T>().size()),
      decltype(std::declval<T>().begin()),
      decltype(std::declval<T>().end()),
      decltype(std::declval<T>().cbegin()),
      decltype(std::declval<T>().cend())
    >,
    void
  >
> : public true_type {};

namespace details {

  template< typename T >
  struct is_stl_container : false_type {};

  template< typename T, std::size_t N>
  struct is_stl_container<std::array<T, N>> : true_type {};

  template< typename... Args>
  struct is_stl_container<std::basic_string<Args...>> : true_type {};

  template< typename... Args>
  struct is_stl_container<std::vector<Args...>> : true_type {};

  template< typename... Args>
  struct is_stl_container<std::deque<Args...>> : true_type {};

  template< typename... Args>
  struct is_stl_container<std::queue<Args...>> : true_type {};

  template< typename... Args>
  struct is_stl_container<std::list<Args...>> : true_type {};

  template< typename... Args>
  struct is_stl_container<std::forward_list<Args...>> : true_type {};

  template< typename... Args>
  struct is_stl_container<std::map<Args...>> : true_type {};

  template< typename... Args>
  struct is_stl_container<std::unordered_map<Args...>> : true_type {};

  template< typename... Args>
  struct is_stl_container<std::multimap<Args...>> : true_type {};

  template< typename... Args>
  struct is_stl_container<std::unordered_multimap<Args...>> : true_type {};

  template< typename... Args>
  struct is_stl_container<std::set<Args...>> : true_type {};

  template< typename... Args>
  struct is_stl_container<std::unordered_set<Args...>> : true_type {};

  template< typename... Args>
  struct is_stl_container<std::multiset<Args...>> : true_type {};

  template< typename... Args>
  struct is_stl_container<std::unordered_multiset<Args...>> : true_type {};

  template< typename... Args>
  struct is_stl_container<std::stack<Args...>> : true_type {};

  template< typename... Args>
  struct is_stl_container<std::priority_queue<Args...>> : true_type {};

  // Linear Ordered Push STL Container: one-dimensional STL sequence container
  // supporting ordered storage and having a push_back method.
  template< typename T >
  struct is_lop_stl_container : false_type {};

  template< typename... Args>
  struct is_lop_stl_container<std::basic_string<Args...>> : true_type {};

  template< typename... Args>
  struct is_lop_stl_container<std::vector<Args...>> : true_type {};

} // namespace details

template< typename T>
struct is_stl_container {
  static constexpr bool const value =
    details::is_stl_container<decay_t<T>>::value;
};

template< typename T>
struct is_lop_stl_container {
  static constexpr bool const value =
    details::is_lop_stl_container<decay_t<T>>::value;
};

} // namespace type_traits
} // namespace blink



#endif /* BASE_GENERIC_TRAITS_H_ */
