/*
 * Copyright (c) 2016 Kareem Alkaseer. All rights reserved.
 */


#ifndef BASE_DATETIME_IO_H_
#define BASE_DATETIME_IO_H_

#include <string>
#include <iomanip>
#include "base/string_view.h"

namespace blink {

template<class E>
class DateTimeIO {

public:

  DateTimeIO() { }

  ~DateTimeIO() { }

  static inline bool
  parse(const std::string & s, std::tm & t) {
    return engine.parse(s, t);
  }

  static inline bool
  parse(std::string && s, std::tm & t) {
    return engine.parse(std::move(s), t);
  }

  static inline bool
  parse(const string_view & s, std::tm & t) {
    return engine.parse(s, t);
  }

  static inline bool
  parse(string_view && s, std::tm & t) {
    return engine.parse(std::move(s), t);
  }

  static inline bool
  parse(const char * s, std::tm & t) {
    return engine.parse(s, t);
  }

private:

  static const E engine;
};

template< typename T >
const T DateTimeIO<T>::engine;

} /* namespace blink */

#endif /* BASE_DATETIME_IO_H_ */
