/*
 * copy_and_assignment.cc
 *
 *  Created on: Sep 4, 2016
 *      Author: Kareem Alkaseer
 */

#include <cstring>
#include <memory>

// Effective C++
// Item 11: define a copy constructor and an assignment operator for classes
// with dynamically allocated memory.

namespace faulty {

class buffer {

public:

  buffer(const char * buf);

  ~buffer();

private:

  char * data;
};

buffer::buffer(const char * buf) {
  if (buf) {
    data = new char[strlen(buf) + 1];
    strcpy(data, buf);
  } else {
    data = new char[1];
    *data = '\n';
  }
}

inline buffer::~buffer() {
  delete [] data;
}

} // namespace faulty


namespace fixed {

class buffer {

public:

  buffer(const char * buf);

  buffer(const buffer & rhs);

  buffer(buffer && rhs);

  buffer & operator=(const buffer & rhs);

  buffer & operator=(buffer && rhs);

  ~buffer();

private:

  std::shared_ptr<char> data;
};

buffer::buffer(const char * buf)
  : data{ std::shared_ptr<char>
    (new char[buf ? strlen(buf) + 1 : 1], std::default_delete<char[]>()) }
{
  if (buf) {
    strcpy(data.get(), buf);
  } else {
    data.get()[0] = '\n';
  }
}

buffer::buffer(const buffer & rhs)
  : data{ rhs.data }
{ }

buffer::buffer(buffer && rhs)
  : data{ std::move(rhs.data) }
{ }

buffer & buffer::operator=(const buffer & rhs) {
  if (this != &rhs)
    data = rhs.data;
  return *this;
}

buffer & buffer::operator=(buffer && rhs) {
  if (this != &rhs)
    data = std::move(rhs.data);
  return *this;
}

inline buffer::~buffer() { }

} // namespace fixed


int main() {

  using buffer = fixed::buffer; // change to faulty::buffer for crash

  buffer a("hello");
  buffer b("world");

  b = a; // memory leak

  buffer c("memory will leak when we assign d to this and more");
  {
    buffer d("let's crash");
    c = d;
  } // d goes out of scope, c.data points to deleted memory

  return 0;
}
