/*
 * Copyright (c) 2013-2016 Kareem Alkaseer. All rights reserved.
 */

#ifndef COMMONOPERATIONSNATIVE_H_
#define COMMONOPERATIONSNATIVE_H_

#include "datastore/native/Operation.h"
#include "datastore/native/ContextPool.h"
#include <HyperAppHelper/Unique.h>


namespace datastore {
namespace HtNative {

namespace alloc {

struct GetCellsOperation : public Operation {

  GetCellsOperation(
    ContextPool       * const p,
    const std::string & n,
    const std::string & t,
    const ScanSpec    & s
  ) : Operation(p),
      ns(n),
      table(t),
      spec(s),
      cells(0)
  { }

  Cells * operator() () {
    const NamespacePtr nPtr = context_->getNamespace(ns);
    const TablePtr tPtr = context_->getTable(nPtr, table);
    const TableScannerPtr scanner = tPtr->create_scanner(spec);
    cells.push_back(Cell());
    while(scanner->next(*(cells.rbegin()))) {
      cells.push_back(Cell());
    }
    cells.pop_back();
    return &cells;
  }

  const std::string ns;
  const std::string table;
  const ScanSpec    spec;
  Cells             cells;
};

struct SetCellsOperation : public Operation {

  SetCellsOperation(
    ContextPool       * const p,
    const std::string & n,
    const std::string & t,
    const Cells       & dbc
  ) : Operation(p),
      ns(n),
      table(t),
      cells(dbc),
      result(false)
  { }

  bool * operator() () {
    const NamespacePtr nPtr = context_->getNamespace(ns);
    const TablePtr tPtr = context_->getTable(nPtr, table);
    const TableMutatorPtr mutator = tPtr->create_mutator();
    mutator->set_cells(cells);
    try {
      mutator->flush();
    } catch (Exception & e) {
      do {
        if (!mutator->need_retry())
          throw;
      } while (!mutator->retry(kRetryTimeout));
    }
    result = true;
    return &result;
  }

  const std::string ns;
  const std::string table;
  const Cells       cells;
  bool              result;
};

struct CreateUniqueCell : public Operation {

  CreateUniqueCell(
    ContextPool       * const p,
    const std::string & n,
    const std::string & t,
    const KeySpec     & k,
    const std::string & v
  ) : Operation(p),
      ns(n),
      table(t),
      key(k),
      value(v),
      result(false)
  { }

  bool * operator() () {
    const NamespacePtr nPtr = context_->getNamespace(ns);
    const TablePtr tPtr = context_->getTable(nPtr, table);

    try {
      Hypertable::HyperAppHelper::create_cell_unique(tPtr, key, const_cast<std::string &>(value));
    } catch (const Exception & e) {
      if (e.code() == Hypertable::Error::BAD_SCAN_SPEC ||
          e.code() == Hypertable::Error::FAILED_EXPECTATION
      ) {
        result = false;
        return &result;
      } else {
        throw;
      }
    }

    result = true;
    return &result;
  }

  const std::string ns;
  const std::string table;
  const KeySpec     key;
  const std::string value;
  bool              result;
};

} // namespace alloc

} /* namespace HtNative */
} /* namespace datastore */


#endif /* COMMONOPERATIONSNATIVE_H_ */
