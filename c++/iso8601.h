/*
 * Copyright (c) 2016 Kareem Alkaseer. All rights reserved.
 */

#ifndef BASE_ISO8601FORMAT_H_
#define BASE_ISO8601FORMAT_H_

#include <regex>
#include <string>
#include <iomanip>
#include "base/string_view.h"
#include "datetime_format.h"

namespace blink {
namespace datetime {

class Iso8601 {

public:

  Iso8601();

  Iso8601(const Iso8601 &) = delete;

  Iso8601(Iso8601 &&) = delete;

  ~Iso8601();

  bool parse(const std::string & s, std::tm & t) const;

  bool parse(std::string && s, std::tm & t) const;

  bool parse(const string_view & s, std::tm & t) const;

  bool parse(string_view && s, std::tm & t) const;

  bool parse(const char * s, std::tm & t) const;

private:

  template< typename S >
  inline bool parse(S & s, std::tm & t) const;

  const Format short_date { "^\\d{8}$", "%Y%m%d" };

  const Format long_date {
    "^\\d{4}-\\d{2}-\\d{2}$", "%Y-%m-%d"
  };

  const Format short_date_time {
    "^\\d{8}T\\d{6}$", "%Y%m%dT%H%M%S"
  };

  const Format long_date_time {
    "^\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}$", "%Y-%m-%dT%H:%M:%S"
  };

  const Format * const date_time_formats[2] = {
    &short_date_time,
    &long_date_time
  };

  const Format * const date_formats[2] = {
    &short_date,
    &long_date
  };

};

} // datetime
} /* namespace blink */

#endif /* BASE_ISO8601FORMAT_H_ */
