/*
 * Copyright (c) 2016 Kareem Alkaseer. All rights reserved.
 */

#ifndef BASE_DATETIME_FORMAT_H_
#define BASE_DATETIME_FORMAT_H_

#include <sstream>
#include <string>
#include <cstring>
#include <iomanip>
#include <regex>
#include "base/string_view.h"
#include "base/nocopy_buf.h"
#include "basic_types.h"
#include "base/datetime.h"

namespace blink {
namespace datetime {

class Format {

public:

  Format(const std::string & r, const std::string & f)
    : pattern_ { r }, format_ { f }
  { }

  Format(std::string && r, std::string && f)
    : pattern_ { std::move(r) }, format_ { std::move(f) }
  { }

  Format(const Format &) = delete;

  Format(Format &&) = delete;

  ~Format() { }

  inline const std::regex & pattern() const { return pattern_; }

  inline const std::string & format() const { return format_; }

  inline bool matches(const std::string & s) const {
    return regex_match(s, pattern_);
  }

  inline bool matches(std::string && s) const {
    return regex_match(std::move(s), pattern_);
  }

  inline bool matches(const string_view & s) const {
    return regex_match(s.data(), pattern_);
  }

  inline bool matches(string_view && s) const {
    return matches(std::move(s));
  }

  inline bool matches(const char * s) const {
    return regex_match(s, pattern_);
  }

  inline bool matches(
    std::string::const_iterator begin, std::string::const_iterator end
  ) const {
    return regex_match(begin, end, pattern_);
  }

  inline bool matches(
    string_view::const_iterator begin, string_view::const_iterator end
  ) const {
    return regex_match(begin, end, pattern_);
  }

  inline std::smatch match(const std::string & s) const {
    std::smatch m;
    std::regex_match(s, m, pattern_);
    return m;
  }

  inline std::smatch match(std::string && s) const {
    return match(std::move(s));
  }

  inline std::smatch match(
    std::string::const_iterator begin, std::string::const_iterator end
  ) const {
    std::smatch m;
    std::regex_match(begin, end, pattern_);
    return m;
  }

  inline std::cmatch match(const string_view & s) const {
    std::cmatch m;
    std::regex_match(s.data(), m, pattern_);
    return m;
  }

  inline std::cmatch match(string_view && s) const {
    return match(std::move(s));
  }

  inline std::cmatch match(
    string_view::const_iterator begin, string_view::const_iterator end
  ) const {
    std::cmatch m;
    std::regex_match(begin, end, pattern_);
    return m;
  }

  inline bool parse(const std::string & s, std::tm & t) const {
    return datetime::parse(s, t, format_.data());
  }

  inline bool parse(std::string && s, std::tm & t) const {
    return parse(std::move(s), t);
  }

  inline bool parse(const string_view & s, std::tm & t) const {
    return datetime::parse(s, t, format_.data());
  }

  inline bool parse(string_view && s, std::tm & t) const {
    return parse(std::move(s), t);
  }

  inline bool parse(const char * s, std::tm & t) const {
    return datetime::parse(s, std::strlen(s), t, format_.data());
  }

  inline bool parse(const char * s, std::size_t size, std::tm & t) const {
    return datetime::parse(s, size, t, format_.data());
  }

private:

  const std::regex pattern_;
  const std::string format_;
};

} // namespace datetime
} // namespace blink

#endif /* BASE_DATETIME_FORMAT_H_ */
