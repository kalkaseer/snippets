#include <atomic>
#include <iostream>
#include <vector>
#include <thread>

struct spinlock {
	
	template< typename F, typename... Args>
	void operator()(F && f, Args &&... args) {
		
		std::atomic_flag & lock = flag();
		
		while(lock.test_and_set(std::memory_order_acquire))
			; // spin until lock is acquired
		f(std::forward<Args>(args)...);
		lock.clear(std::memory_order_release);
	}
	
private:
	
	std::atomic_flag & flag() {
		static std::atomic_flag m_lock = ATOMIC_FLAG_INIT;
		return m_lock;
	}
	
};

int main() {
	
	spinlock spin;
	
	std::vector<std::thread> threads;
	for (int i = 0; i < 10; ++i)
		threads.emplace_back(spin, [i](){ std::cout << i << std::endl; });
	
	for (auto & t : threads) t.join();
	
	return 0;
}