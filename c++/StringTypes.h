/*
 * Copyright (c) 2013-2016 Kareem Alkaseer. All rights reserved.
 */

#ifndef T_STRINGTYPES_H_
#define T_STRINGTYPES_H_

namespace T {

template<typename Type, typename Return = void, bool = true>
struct DeallocatorTraits {

  typedef char yes[1];
  typedef char  no[2];

  static yes & checkDataType(typename Type::DataType *);

  static no & checkDataType(...);

  static yes & checkSizeType(typename Type::SizeType *);

  static no & checkSizeType(...);

  template <Return (Type::*)(typename Type::DataType **,
                             typename Type::SizeType *)>
  struct FunctorCheck;

  static yes & checkFunctor(FunctorCheck<&Type::operator()> *);

  static no  & checkFunctor(...);

  static const bool value =
        sizeof( checkFunctor(0)) == sizeof(yes)
     && sizeof(checkDataType(0)) == sizeof(yes)
     && sizeof(checkDataType(0)) == sizeof(yes);

};

template<typename Type, typename Return>
struct DeallocatorTraits<Type, Return, false> {

  typedef char yes[1];
  typedef char  no[2];

  static yes & checkDataType(typename Type::DataType *);

  static no & checkDataType(...);

  template <Return (Type::*)(typename Type::DataType **)>
  struct FunctorCheck;

  static yes & checkFunctor(FunctorCheck<&Type::operator()> *);

  static no  & checkFunctor(...);

  static const bool value =
        sizeof( checkFunctor(0)) == sizeof(yes)
     && sizeof(checkDataType(0)) == sizeof(yes);

};

struct _NONE_TYPE { };

template<typename Char, typename Return, typename Size>
struct DefaultDeallocator
{
  typedef Char   DataType;
  typedef Size   SizeType;
  typedef Return ReturnType;

  void operator()(DataType ** data, SizeType * size) {
    memset(*data, 0, size);
    delete [] *data;
    *data = nullptr;
    *size = 0;
  }
};

template <
  typename Char,
  typename Return
>
struct DefaultDeallocator < Char, Return, _NONE_TYPE >
{

  typedef Char   DataType;
  typedef Return ReturnType;

  void operator()(DataType ** data) {
    delete [] *data;
    *data = nullptr;
  }
};

template<typename Char, typename Return, typename Size>
struct MallocDeallocator
{
  typedef Char   DataType;
  typedef Size   SizeType;
  typedef Return ReturnType;

  void operator()(DataType ** data, SizeType * size) {
    memset(*data, 0, *size);
    free(*data);
    *data = nullptr;
    *size = 0;
  }
};

template <
  typename Char,
  typename Return
>
struct MallocDeallocator < Char, Return, _NONE_TYPE >
{

  typedef Char   DataType;
  typedef Return ReturnType;

  void operator()(DataType ** data) {
    free(*data);
    *data = nullptr;
  }
};

template <
  typename Char,
  typename Return,
  typename Size,
  template < typename, typename, typename > class Deallocator
>
class StringData {

 public:

  typedef Char   DataType;
  typedef Size   SizeType;
  typedef Return ReturnType;

  StringData(DataType * data, SizeType size)
    : data_(data),
      size_(size),
      dealloc_()
  { }

  ~StringData() {
    dealloc_(&data_, &size_);
  }

  const DataType * data() const { return data_; }

  const SizeType size() const { return size_; }

 private:
  DataType   * data_;
  SizeType     size_;
  Deallocator<DataType, ReturnType, SizeType>  dealloc_;

  static_assert(
    DeallocatorTraits<
      Deallocator<DataType, ReturnType, _NONE_TYPE>, void, false
    >::value,
    "Deallocator fails traits expectations");

//  static const char __cassert = StaticAssert< DeallocatorTraits<
//    Deallocator<
//      DataType, ReturnType, _NONE_TYPE>, void, false>::value>::A;
};

template <
  typename Char,
  typename Return,
  template < typename, typename, typename > class Deallocator
>
class StringData < Char, Return, _NONE_TYPE, Deallocator >
{
 public:

  typedef Char   DataType;
  typedef Return ReturnType;

  StringData(DataType * data)
    : data_(data),
      dealloc_()
  { }

  ~StringData() {
    dealloc_(&data_);
  }

  const DataType * data() const { return data_; }

 private:
  DataType * data_;

  Deallocator<DataType, ReturnType, _NONE_TYPE>   dealloc_;

  static_assert(
    DeallocatorTraits<
      Deallocator<DataType, ReturnType, _NONE_TYPE>, void, false
    >::value,
    "Deallocator fails traits expectations");
};


typedef
  StringData<char, void, size_t, DefaultDeallocator>
  CharData;

typedef
  StringData<unsigned char, void, size_t, DefaultDeallocator>
  BinaryData;

typedef
  StringData<char, void, size_t, MallocDeallocator>
  MCharData;

typedef
  StringData<unsigned char, void, size_t, MallocDeallocator>
  MBinaryData;

typedef
  std::basic_string<
    signed char,
    std::char_traits<signed char>,
    std::allocator<signed char> >
  SignedString;

typedef
  std::basic_string<
    unsigned char,
    std::char_traits<unsigned char>,
    std::allocator<unsigned char> >
  BytesString;


} // namespace T
#endif /* T_STRINGTYPES_H_ */
