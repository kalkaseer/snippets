/*
 * Copyright (c) 2016 Kareem Alkaseer. All rights reserved.
 */

#include "base/iso8601.h"

#include <algorithm>
#include "generic_traits.h"

namespace blink {
namespace datetime {


template< typename T >
struct Match { };

template< typename... Args >
struct Match<std::basic_string<Args...>> {
  typedef std::smatch match;
  typedef typename std::basic_string<Args...>::const_iterator const_iterator;
  static inline std::size_t size(const std::basic_string<Args...> & s) {
    return s.size();
  }
  static inline const_iterator end(const std::basic_string<Args...> & s) {
    return s.cend();
  }
  static inline const_iterator begin(const std::basic_string<Args...> & s) {
    return s.cbegin();
  }
  static inline const char * data(const std::basic_string<Args...> & s) {
    return s.data();
  }
};

template< typename... Args >
struct Match<basic_string_view<Args...>> {
  typedef std::cmatch match;
  typedef typename basic_string_view<Args...>::const_iterator const_iterator;
  static inline std::size_t size(const basic_string_view<Args...> & s) {
    return s.size();
  }
  static inline const_iterator end(const basic_string_view<Args...> & s) {
    return s.cend();
  }
  static inline const_iterator begin(const basic_string_view<Args...> & s) {
    return s.cbegin();
  }
  static inline const char * data(const basic_string_view<Args...> & s) {
    return s.data();
  }
};

template<>
struct Match<const char *> {
  typedef std::cmatch match;
  typedef const char * const_iterator;
  static inline std::size_t size(const char * s) {
    return std::strlen(s);
  }
  static inline const_iterator end(const char * s) {
    return s + size(s);
  }
  static inline const_iterator begin(const char * s) {
    return s;
  }
  static inline const char * data(const char * s) {
    return s;
  }
};

template< typename S >
inline bool
Iso8601::parse(S & s, std::tm & t) const {

  typedef Match<type_traits::decay_t<S>> M;

  std::memset(&t, 0, sizeof(std::tm));

  // Length is greater than the length of long_date
  if (M::size(s) > 10) {
    typename M::match m;
    int32 hours = 0;
    int32 minutes = 0;
    typename M::const_iterator enditer = M::end(s);

    if (std::regex_match(M::end(s)-3, M::end(s), m, timezone_pattern)) {
          hours = std::stoi(m.str());
          enditer = M::end(s)-3;
    } else if (std::regex_match(M::end(s)-5, M::end(s), m, timezone_pattern)) {
      hours = std::stoi(m.str().substr(0, 3));
      minutes = std::stoi(m.str().substr(3));
      enditer = M::end(s)-5;
    } else if (
      std::regex_match(M::end(s)-6, M::end(s), m, extended_timezone_pattern)
    ) {
      hours = std::stoi(m.str().substr(0, 3));
      minutes = std::stoi(m.str().substr(4));
      enditer = M::end(s)-6;
    }

    for (auto format : date_time_formats) {
      if (
        format->matches(M::begin(s), enditer) &&
        format->parse(M::data(s), enditer - M::begin(s), t)
      ) {
//          if (hours | minutes) {
//            t.tm_isdst = -1;
//            t.tm_gmtoff = 0;
//            t.tm_hour += -hours;
//            t.tm_min += datetime::signed_minutes(-hours, minutes);
//          }
          datetime::utc_offset(t, hours, minutes);
          return true;
      }
    }
    return false;
  }

  if (M::size(s) == 8) {
    if (short_date.parse(s, t)) {
      t.tm_isdst = -1;
      t.tm_gmtoff = 0;
      return true;
    }
    return false;
  } else if (M::size(s) == 10) {
    if (long_date.parse(s, t)) {
      t.tm_isdst = -1;
      t.tm_gmtoff = 0;
      return true;
    }
    return false;
  }

  return false;
}

Iso8601::Iso8601() { }

Iso8601::~Iso8601() { }

bool Iso8601::parse(const std::string & s, std::tm & t) const {
  return parse<const std::string>(s, t);
}

bool Iso8601::parse(std::string && s, std::tm & t) const {
  return parse<const std::string>(std::move(s), t);
}

bool Iso8601::parse(const string_view & s, std::tm & t) const {
  return parse<const string_view>(s, t);
}

bool Iso8601::parse(string_view && s, std::tm & t) const {
  return parse<const string_view>(std::move(s), t);
}

bool Iso8601::parse(const char * s, std::tm & t) const {
  return parse<const char *>(s, t);
}

} // namespace datetime
} /* namespace blink */
