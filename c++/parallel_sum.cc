#include <future>
#include <numeric>
#include <iostream>
#include <vector>

template< class Iter>
typename Iter::value_type parallel_sum(
	Iter begin, Iter end, typename Iter::value_type init
) {
	
	auto len = end - begin;
	if (len < 1000)
		return std::accumulate(begin, end, init);
	
	Iter mid = begin + len/2;
	
	auto fut = std::async(
		std::launch::async, parallel_sum<Iter>, mid, end, init);
	
	typename Iter::value_type sum = parallel_sum(begin, mid, init);
	
	return sum + fut.get();
}

int main() {
	std::vector<int> v(1000000, 1);
	std::cout << "Sum " << parallel_sum(v.begin(), v.end(), 0) << std::endl;
}