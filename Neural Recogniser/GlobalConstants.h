/**
 * Copyright 2008 MentorLycon, LLC.
 * The author of this software is Kareem Alkaseer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _GLOBALCONSTANTS_H
#define	_GLOBALCONSTANTS_H

#ifdef	__cplusplus
extern "C" {
#endif

  #define OPERATION_SUCCESS         0
  #define OPERATION_FAILURE         -1
  #define MEMORY_EXHAUSTION         -2
  #define NONMATCHING_NETWORK       -3
  #define ILLEGAL_INDEX             -4
  #define MEM_ALLOCATION_ERROR      -5
  #define CANNOT_OPEN_FILE          -6
  #define CANNOT_WRITE_FILE         -7
  #define CANNOT_READ_FILE          -8
  #define UNRECOGNISED_FILE_FORMAT  -9
  #define NONMATCHING_LENGTHS       -10
  #define CONTAINS_NULL             1
  #define CONTAINS_NO_NULL          0
          
  static const double ON  = 0.9;
  static const double OFF = 0.1;


#ifdef	__cplusplus
}
#endif

#endif	/* _GLOBALCONSTANTS_H */

