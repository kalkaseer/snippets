/**
 * Copyright 2008 MentorLycon, LLC.
 * The author of this software is Kareem Alkaseer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include "TrainingSpace.h"
#include "Commons.h"
#include "GlobalConstants.h"

#define DEFAULT_CAPACITY  20

typedef struct tspnode_t {
  TrainingSet * set;
  struct tspnode_t * previous;
  struct tspnode_t * next;
}TSPNode;

struct trainingspace_t {
  size_t  size;
  size_t  capacity;
  size_t  tVectorLength;
  size_t  oVectorLength;
  TSPNode * current;
  TSPNode * *nodes;
  TSPNode * head;
  TSPNode * tail;
  bool   shouldFinish;   // for use with tspNext and tspPrevious to avoid cyclic references
};

void tspPrivateDeleteNode(TrainingSpace * space, size_t index);
void tspPrivateAlignNodes(TrainingSpace * space, size_t index);
int tspPrivateInsertSet(TrainingSpace * space, TrainingSet * set,
                        size_t index, size_t multiplier);

TrainingSpace * tspTrainingSpace (size_t TVLength, size_t OVLength) {
  return tspTrainingSpaceWCap(TVLength, OVLength, DEFAULT_CAPACITY);
}

TrainingSpace * tspTrainingSpaceWCap(size_t TVLength, size_t OVLength, size_t capacity) {
  
  TrainingSpace * space = (TrainingSpace *)malloc(sizeof(TrainingSpace));  
  if (!space) {
    nError("training space was not created. No enough memory.", __func__);
    return (TrainingSpace *)NULL;
  } 
  
  space->nodes = (TSPNode **)calloc(capacity, sizeof(TSPNode *));
  if (!(space->nodes)) {
    nError("training space was not created. No enough memory.", __func__);
    free(space);
    return (TrainingSpace *)NULL;
  }
  
  space->size = 0;
  space->capacity = capacity;
  space->tVectorLength = TVLength;
  space->oVectorLength = OVLength;
  space->current = NULL;
  space->head = NULL;
  space->tail = NULL;
  space->shouldFinish = false;
  
  return space;
}

TrainingSpace * tspTrainingSpaceWSets(size_t TVLength, size_t OVLength, size_t size) {
  
  TrainingSpace * space = (TrainingSpace *)malloc(sizeof(TrainingSpace));  
  if (!space) {
    nError("training space was not created. No enough memory.", __func__);
    return (TrainingSpace *)NULL;
  }
  
  if (size > DEFAULT_CAPACITY)
    space->capacity = size + 10;
  else
    space->capacity = DEFAULT_CAPACITY;
  
  space->nodes = (TSPNode **)malloc(space->capacity * sizeof(TSPNode *));
  if (!(space->nodes)) {
    nError("training space was not created. No enough memory.", __func__);
    free(space);
    return (TrainingSpace *)NULL;
  }
  
  register size_t count, i;
  TrainingSet * set;
  
  for (count = 0; count < size; count++) {
   TSPNode * node = (TSPNode *)malloc(sizeof(TSPNode));
   if (!node) {
     nError("training space was not created. No enough memory.", __func__);
     for (i = 0; i < count; i++) {
       tsDelete(&(space->nodes[i]->set));
       free(space->nodes[i]);
     }
     free(space->nodes);
     free(space);
    return (TrainingSpace *)NULL;
   }
   
   set = tsTrainingSet(TVLength, OVLength);
   if (!set) {
     nError("training space was not created. No enough memory.", __func__);
     for (i = 0; i < count; i++) {
       tsDelete(&(space->nodes[i]->set));
       free(space->nodes[i]);
     }
     free(node);
     free(space->nodes);
     free(space);
    return (TrainingSpace *)NULL;
   }   
   node->set = set;
   space->nodes[count] = node;
  }
  
  if (size != 0) {
    for (int i = 1; i < size; i++) {
      space->nodes[i]->previous = space->nodes[i-1];
      space->nodes[i-1]->next = space->nodes[i];
    }
    
    space->nodes[0]->previous = space->nodes[size-1]->next = NULL; 
  
    space->head = space->nodes[0];
    space->tail = space->nodes[size-1];
  }
  else 
    space->head = space->tail = (TSPNode *)NULL;
  
  space->size = size;
  space->tVectorLength = TVLength;
  space->oVectorLength = OVLength;
  space->current = NULL;
  space->shouldFinish = false;
  
  return space;  
}

int tspDelete (TrainingSpace ** space) {
  if (!(*space)) {
    nError("training space was not deleted. The training space given is NULL.", __func__);
    return NONEXISTING_SPACE;
  }
  
  int n = (*space)->size;
    
  for (int i = 0; i < n; i++) {
    tsDelete(&((*space)->nodes[i]->set));
    free((*space)->nodes[i]);
  }
  
  free((*space)->nodes);
  free((*space));
  
  memset((*space), 0, sizeof(TrainingSpace));
  (*space) = NULL;
  
  return OPERATION_SUCCESS;
}

int tspAddSet (TrainingSpace * space, TrainingSet * set) {
  return (tspAddSetWCap(space, set, PLAIN_ADDITION));
}

int tspAddSetWCap (TrainingSpace * space, TrainingSet * set, size_t addition) {
  if (!space) {
    nError("set was not added. Training space given is NULL.", __func__);
    return NONEXISTING_SPACE;
  }
  
  if (!set) {
    nError("set was not added. Training set given is NULL.", __func__);
    return NONEXISTING_SET;
  }
  
  TSPNode * newNode = (TSPNode *)malloc(sizeof(TSPNode));
  if(!newNode) {
    nError("set was not added, No enough memory.", __func__);
    return MEMORY_EXHAUSTION;
  }
  newNode->set = set;
  
  if (space->size == space->capacity) {
    
    switch (addition) {
      case PLAIN_ADDITION:
        addition = PLAIN_ADDITION;
        break;
      case TINY_ADDITION:
        addition = TINY_ADDITION;
        break;
      case VERY_SMALL_ADDITION:
        addition = VERY_SMALL_ADDITION;
        break;
      case SMALL_ADDITION:
        addition = SMALL_ADDITION;
        break;
      case MEDIUM_ADDITION:
        addition = MEDIUM_ADDITION;
        break;
      case LARGE_ADDITION:
        addition = LARGE_ADDITION;
        break;
      case VERY_LARGE_ADDITION:
        addition = VERY_LARGE_ADDITION;
        break;
      case SEMIHUGE_ADDITION:
        addition = SEMIHUGE_ADDITION;
        break;
      case HUGE_ADDITION:
        addition = HUGE_ADDITION;
        break;
      default:
        addition = DEFAULT_ADDITION;
    }
    
    tspSetCapacity(space, space->capacity + addition);
  }
  
  if (space->tail) {
    newNode->previous = space->tail;
    space->tail->next = newNode;
    newNode->next = NULL;
    space->tail = newNode;
    space->nodes[space->size] = newNode;
  }
  else {
    newNode->next = newNode->previous = NULL;
    space->head = space->tail = newNode;
    space->nodes[0] = newNode;
  }
  
  (space->size)++;
  
  return OPERATION_SUCCESS;
}

int tspPrivateInsertSet(TrainingSpace * space, TrainingSet * set,
        size_t index, size_t multiplier) {
  if (!space) {
    nError("set was not inserted. Training space given is NULL.", __func__);
    return NONEXISTING_SPACE;
  }
  
  if (!set) {
    nError("set was not inserted. Training set given is NULL.", __func__);
    return NONEXISTING_SET;
  }
  
  if (
          (space->tVectorLength != tsTVectorLength(set)) ||
          (space->oVectorLength != tsOVectorLength(set))
     ) {
    nError("set was not inserted. Training and output vectors\'s must match", __func__);
    return NONMATCHING_LENGTHS;
  }
    
  if (index > space->size) {
    char * s = "training set was not inserted. Illegal index, max index is ";
    char * sizeString = (char *)malloc(16 * sizeof(char));
    char * errorMsg = (char *)malloc(strlen(s) + 17);
    sprintf(sizeString, "%u", space->size);
    strcat(errorMsg, s);
    strcat(errorMsg, sizeString);
    nError(errorMsg, __func__);
    free(sizeString);
    free(errorMsg);
    return ILLEGAL_INDEX;
  }
  
  if (space->size == space->capacity) {
    TSPNode ** temp = (TSPNode **)realloc(space->nodes,
                       multiplier * space->capacity * sizeof(TSPNode *));
    for (int i = space->size; i < space->capacity; i++)
      space->nodes[i] = NULL;
    if (!temp) {
      nError("set was not inserted. Memory is exhausted.", __func__);
      return MEMORY_EXHAUSTION;
    }
    else {
      space->nodes = temp;
      space->capacity *= 2;
    }
  }
  
  TSPNode * newNode = (TSPNode *)malloc(sizeof(TSPNode));
  if (!newNode) {
    nError("set was not inserted. Memory is exhausted.", __func__);
    return MEMORY_EXHAUSTION;
  }
  
  newNode->set = set;
  
  if (index == 0) {
    newNode->next = space->head;
    newNode->previous = NULL;
    if (space->head)
      space->head->previous = newNode;
    else
      space->tail = newNode;
    space->head = newNode;
  }
  else {
    newNode->next = space->nodes[index-1]->next;
    newNode->previous = space->nodes[index-1];
    if (index != space->size)
      space->nodes[index-1]->next->previous = newNode;
    else
      space->tail = newNode;
    space->nodes[index-1]->next = newNode;
  }
  
  space->size++;
  space->current = space->tail;
  
  for (int i = 1; i <= space->size; i++) {
    space->nodes[space->size - i] = space->current;
    space->current = space->current->previous;
  }
  
  return OPERATION_SUCCESS;
}

int tspInsertSet (TrainingSpace * space, TrainingSet * set, size_t index) {
  return tspPrivateInsertSet(space, set, index, PLAIN_ADDITION);
}

void tspPrivateDeleteNode(TrainingSpace * space, size_t index) {
  tsDelete(&(space->nodes[index]->set));
  free(space->nodes[index]);
  space->nodes[index] = NULL;
}

void tspPrivateAlignNodes(TrainingSpace * space, size_t index) {
  if (index + 1 < space->capacity) 
    space->current = space->nodes[index+1];
  else
    space->current = NULL;
  
  while (space->current) {
    space->nodes[index++] = space->current;
    space->current = space->current->next;
  }
  
  space->nodes[index] = NULL;
}

int tspRemoveSet (TrainingSpace * space) {
  if(!space) {
    nError("no set was removed. Training space given is NULL.", __func__);
    return NONEXISTING_SPACE;
  }
  
  return (tspIDeleteSet(space, space->size-1));
}

int tspIDeleteSet (TrainingSpace * space, size_t index) {
  if (!space) {
    nError("set was not deleted. Training space given is NULL.", __func__);
    return NONEXISTING_SPACE;
  }
  
  if (space->size == 0) {
    nError("no set was deleted. Training space is empty.", __func__);
    return EMPTY_SPACE;
  }
  
  if (index > space->size - 1) {
    char * s = "training set was not deleted. Illegal index, max index is ";
    char * sizeString = (char *)malloc(16 * sizeof(char));
    sprintf(sizeString, "%u", space->size - 1);
    char * errorMsg = (char *)malloc(strlen(s) + 17);
    strcat(errorMsg, s);
    strcat(errorMsg, sizeString);
    nError(errorMsg, __func__);
    free(sizeString);
    free(errorMsg);
    return ILLEGAL_INDEX;
  } 
  
  if (index == 0) {
    if (space->size != 1)
      space->nodes[index+1]->previous = NULL;
    else
      space->tail = NULL;
    tspPrivateDeleteNode(space, index);
    tspPrivateAlignNodes(space, index);
    space->head = space->nodes[0];
  }  
  else if(index == space->size - 1) {
    space->nodes[index-1]->next = NULL;
    tspPrivateDeleteNode(space, index);
    space->tail = space->nodes[space->size-2];
  }
  else {
    space->nodes[index-1]->next = space->nodes[index+1];
    space->nodes[index+1]->previous = space->nodes[index-1];
    tspPrivateDeleteNode(space, index);
    tspPrivateAlignNodes(space, index);
  }
  
  (space->size)--;  
  
  return OPERATION_SUCCESS;   
}

int tspSDeleteSet (TrainingSpace * space, TrainingSet * set) {
  if (!space) {
    nError("nothing to be deleted. Training space given is NULL.", __func__);
    return NONEXISTING_SPACE;
  }
  
  int returnValue = tspIndex(space, set);
  if (returnValue < 0)
    return returnValue;
  returnValue = tspIDeleteSet(space, returnValue);
  return returnValue;
}

TrainingSet * tspFirst (TrainingSpace * space) {
  if (!space) {
    nError("nothing to be returned. Training space given is NULL.", __func__);
    return (TrainingSet *)NULL;
  }
  
  return space->head->set;
}

TrainingSet * tspLast (TrainingSpace * space) {
  if (!space) {
    nError("nothing to be returned. Training space given is NULL.", __func__);
    return (TrainingSet *)NULL;
  }
  
  return space->tail->set;
}

TrainingSet * tspSet (TrainingSpace * space, size_t index) {
  if (!space) {
    nError("nothing to be returned. Training space given is NULL", __func__);
    return (TrainingSet *)NULL;
  }
  
  if (index >= space->size) {
    nError("nothing to be returned. Index given does not exist.", __func__);
    return (TrainingSet *)NULL;
  }
  
  return space->nodes[index]->set;
  
}

TrainingSet * tspNext (TrainingSpace * space) {
  if (!space) {
    nError("nothing to be returned. Training space given is NULL.", __func__);
    return (TrainingSet *)NULL;
  }
  
  if (space->size == 0) {
    nError("nothing to be returned. Training set given is of size zero.", __func__);
    return (TrainingSet *)NULL; 
  }
  
  if (!space->shouldFinish) {
    if (!space->current) 
      space->current = space->head;
    
    TSPNode * node = space->current;
    space->current = space->current->next;
    
    if (!space->current) 
      space->shouldFinish = true;    

    return node->set;
  }
  
  space->shouldFinish = false;  
  return (TrainingSet *)NULL;
}

TrainingSet * tspPrevious (TrainingSpace * space) {
  if (!space) {
    nError("nothing to be returned. Training space given is NULL.", __func__);
    return (TrainingSet *)NULL;
  }
  
   if(!space->current) {
    space->current = space->tail;
    return space->tail->set;
  }
  
  TSPNode * node = space->current;
  space->current = space->current->previous;
  return node->set;
}

int tspSize (TrainingSpace * space) {
  if (!space) {
    nError("training space given is NULL.", __func__);
    return NONEXISTING_SPACE;
  }
  
  return space->size;
}

int tspCapacity (TrainingSpace * space) {
  if (!space) {
    nError("training space given is NULL.", __func__);
    return NONEXISTING_SPACE;
  }
  
  return space->capacity;
}

int tspSetCapacity (TrainingSpace * space, size_t capacity) {
  if (!space) {
    nError("capacity was not set. The training space given is NULL.", __func__);
    return NONEXISTING_SPACE;
  }
  
  if (capacity < space->size) {
    nError("capacity was not set. Capacity specified is less than the space's size.", __func__);
    return ILLEGAL_CAPACITY;
  }
  
  if (capacity == 0) {
    nError("capacity was not set. Capacity specified is 0.", __func__);
    return ILLEGAL_CAPACITY;
  }
  
  if (capacity == space->capacity)
    return OPERATION_SUCCESS;
  
  TSPNode ** temp = (TSPNode **)realloc(space->nodes, capacity * sizeof(TSPNode *));
  if (!temp) {
    nError("capacity was not set. Memory is exhausted.", __func__);
    return MEMORY_EXHAUSTION;
  }
  
  space->nodes = temp;
  space->capacity = capacity;
  
  return OPERATION_SUCCESS;
}

size_t tspSizeOf(void) {
  return sizeof(TrainingSpace);
}

TrainingSet * tspNull (TrainingSpace * space) {
  if (!space) {
    nError("nothing to return. Training space given is NULL.", __func__);
    return (TrainingSet *)NULL;
  }
  
  for (int i = 0; i < space->size; i++)
    if (!space->nodes[i]->set)
      return space->nodes[i]->set;
      
  return (TrainingSet *)NULL; // set does not exist in the space.
}

int tspContainsNull (TrainingSpace * space) {
  if (!space) {
    nError("nothing to return. Training space given is NULL.", __func__);
    return NONEXISTING_SPACE;
  }
  
  for (int i = 0; i < space->size; i++)
    if (!space->nodes[i]->set)
      return CONTAINS_NULL;
  
  return CONTAINS_NO_NULL;
}

int tspDeleteNulls (TrainingSpace * space) {
  if (!space) {
    nError("nothing to return. Training space given is NULL.", __func__);
    return NONEXISTING_SPACE;
  }
  
  for (int i = 0; i < space->size; i++)
     if (!space->nodes[i]->set)
       tspIDeleteSet(space, i);
  
  return OPERATION_SUCCESS;
}

int tspTrim (TrainingSpace ** space) {
  if (!(*space)) {
    nError("nothing to return. Training space given is NULL.", __func__);
    return NONEXISTING_SPACE;
  }  
  
  if (!(*space)->size)
    return tspDelete(space);
  
  TSPNode ** temp = (TSPNode **)realloc((*space)->nodes, (*space)->size * sizeof(TSPNode *));
  if (!temp) {
    nError("Error during setting training space's capacity. Training space was not changed.", __func__);
    return MEM_ALLOCATION_ERROR;
  }
  
  (*space)->nodes = temp;
  (*space)->capacity = (*space)->size;
  
  return OPERATION_SUCCESS;
}

int tspIndex(TrainingSpace * space, TrainingSet * set) {
  if (!space) {
    nError("nothing to return. Training space given is NULL.", __func__);
    return NONEXISTING_SPACE;
  }
  
  for (int i = 0; i < space->size; i++)
    if (space->nodes[i]->set == set)
      return i;
  
  return SET_IS_NOT_CONTAINED; // set does not exist in the space.
}

int tspCurrent(TrainingSpace * space) {
  if (!space) {
    nError("nothing to return. Training space given is NULL.", __func__);
    return NONEXISTING_SET;
  }
  
  if(!space->current) 
    return NULL_CURRENT;
  
  return (tspIndex(space, space->current->set));
}

int tspSetCurrent(TrainingSpace * space, size_t index) {
  if (!space) {
    nError("current can not be set. Training space given is NULL.", __func__);
    return NONEXISTING_SPACE;
  }
  
  if (index >= space->size) {
    nError("noting to be returned. Index given exceeds space's size.", __func__);
    return ILLEGAL_INDEX;
  }
  
  //////////
  // make sure it works
  // was provided for tspNext to restart when current is set by called
  /////////
  space->shouldFinish = false;
  /////////
  
  space->current = space->nodes[index];
  return OPERATION_SUCCESS;
}

int tspMemConsumed(TrainingSpace * space) {
  if (!space) {
    nError("nothing to be returned. Training space given is NULL.", __func__);
    return NONEXISTING_SPACE;
  }
  
  int size = sizeof(TrainingSpace);
  size += space->capacity * sizeof(TSPNode *);
  size += space->size * sizeof(TSPNode);
  
  return size;
}

int tspMemConsumedWSets(TrainingSpace * space) {
  if (!space) {
    nError("nothing to be returned. Training space given is NULL.", __func__);
    return NONEXISTING_SPACE;
  }
  
  int setSize = 0;
  if (space->head)
    tsMemConsumed(space->head->set);
  int size = sizeof(TrainingSpace);
  size += space->capacity * sizeof(TSPNode *);
  size += space->size * sizeof(TSPNode);
  for (int i = 0; i < space->size; i++)
    //size += tsMemConsumed(space->nodes[i]->set);
    if (space->nodes[i]->set)
      size += setSize;
  
  return size;
}
