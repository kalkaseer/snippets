/**
 * Copyright 2008 MentorLycon, LLC.
 * The author of this software is Kareem Alkaseer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _CREATEVECTOR_H
#define	_CREATEVECTOR_H

#ifdef	__cplusplus
extern "C" {
#endif

  double inVector1 [] = { 0, 0 };
  double inVector2 [] = { 0, 1 };
  double inVector3 [] = { 1, 0 };
  double inVector4 [] = { 1, 1 };
  
  double outVector1 [] = { 0 };
  double outVector2 [] = { 1 };
  double outVector3 [] = { 1 };
  double outVector4 [] = { 0 };



#ifdef	__cplusplus
}
#endif

#endif	/* _CREATEVECTOR_H */

