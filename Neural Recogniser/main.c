/**
 * Copyright 2008 MentorLycon, LLC.
 * The author of this software is Kareem Alkaseer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
  
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <time.h>
#include "Network.h"
#include "TrainingSet.h"
#include "TrainingSpace.h"
#include "Backprop.h"
#include "GlobalConstants.h"

#include "CreateNet.h"
#include "CreateSpace.h"
#include "CreateSet.h"

#define EPOCH_SIZE  62

/*
 * 
 */

Network       * globalNet = NULL;
TrainingSet   * globalSets [EPOCH_SIZE];
TrainingSpace * globalSpace = NULL;
BPTrainer     * globalTrainer = NULL;

int main(int argc, char** argv) {
  
  time_t t1 = time(NULL);
  time_t t2;
  
  //createNet();
  //printf("creating space\n");
  //createSpace();
  //printf("space was created\n");
  
  printf("\nMAIN() --------------------------\n");  
  
  /*
  printf("current.index: %d\n", tspCurrent(globalSpace));
  getchar();  
   */
  
  /*
  printf("Reterieval of sets by tspSet\n");
  printf("----------------------------\n");
  TrainingSet * set1;
  for (int index = 0; index < tspSize(globalSpace); index++) {
    set1 = tspSet(globalSpace, index);
    double * returnedTVector = tsTVector(set1);
    printf("\n%d: TV\n", index);
    for (int i = 0; i < tsTVectorLength(set1); i++)
      printf("%f ", returnedTVector[i]);
    printf("\n%d: OV\n", index);
    double * returnedOVector = tsOVector(set1);
    for (int i = 0; i < tsOVectorLength(set1); i++)
      printf("%f ", returnedOVector[i]);
  }
  printf("\n");
   */
  
  /*
  printf("Reterieval of sets by tspNext\n");
  printf("-----------------------------\n");  
  tspSetCurrent(globalSpace, 0); // resets any incomplete cycle 
  TrainingSet * set;
  int counter = 0;
  while ((set = tspNext(globalSpace)) != NULL) {
    double * returnedTVector = tsTVector(set);
    printf("#%u: TV\n", ++counter);
    for (int i = 0; i < tsTVectorLength(set); i++)
      printf("%f ", returnedTVector[i]);
    printf("\n%u: OV\n", counter);
    double * returnedOVector = tsOVector(set);
    for (int i = 0; i < tsOVectorLength(set); i++)
      printf("%f ", returnedOVector[i]);
    printf("\n\n");
  }
   */
  
  /*
  TrainingSpace * space0;
  for (int i = 0; i < 3000; i++) {
    space0 = tspTrainingSpaceWSets(globalNet, 10000);
    tspDelete(&space0);
  }
   */
  
  /*
  double inputs2  [35] = {
    0, 0, 1, 1, 1, 0, 0,
    0, 1, 0, 0, 1, 0, 0,
    0, 1, 0, 0, 1, 0, 0,
    0, 0, 0, 1, 0, 0, 0,
    1, 1, 1, 1, 1, 1, 1
  };
  
  double inputs1 [35] = {
    0, 0, 1, 1, 0, 0, 0,
    0, 0, 1, 1, 0, 0, 0,
    0, 1, 0, 1, 0, 0, 0,
    0, 0, 0, 1, 0, 0, 0,
    1, 1, 1, 1, 1, 1, 1
  };
  
  double inputs0 [35] = {
    1, 1, 1, 1, 1, 1, 1,
    1, 1, 0, 0, 0, 1, 1,
    1, 0, 0, 0, 0, 0, 1,
    1, 1, 0, 0, 0, 1, 1,
    1, 1, 1, 1, 1, 1, 1
  };
  
  double outputs2 [3] = { 0, 0, 1 };
  double outputs1 [3] = { 0, 1, 0 };
  double outputs0 [3] = { 1, 0, 0 };
  
  Network * net = nNetwork(3, 35, 7, 3);  
  TrainingSet * set2 = tsTrainingSet(net);
  tsSetTVector(set2, inputs2);
  tsSetOVector(set2, outputs2);
  TrainingSet * set1 = tsTrainingSet(net);
  tsSetTVector(set1, inputs1);
  tsSetOVector(set1, outputs1);
  TrainingSet * set0 = tsTrainingSet(net);
  tsSetTVector(set0, inputs0);
  tsSetOVector(set0, outputs0);  
  TrainingSet * sets [3] = { set2, set1, set0 };
  BPTrainer * trainer = bpTrainerWithoutSets(net, 3, sets);
   */
  
  
  //bpTrain(trainer); 
    
  /*
  
  double netOutput [3]; 
  printf("pattern 2:\n");
  nFeedforward(net, inputs2);
  nOutput(net, netOutput);
  
  printf("pattern 1:\n");
  nFeedforward(net, inputs1);
  nOutput(net, netOutput);
  
  printf("pattern 0:\n");
  nFeedforward(net, inputs0);
  nOutput(net, netOutput);
   */
   
  /*
  double in1 [] = { 0.1, 0.1 };
  double in2 [] = { 0.9, 0.1 };
  double in3 [] = { 0.1, 0.9 };
  double in4 [] = { 0.9, 0.9 };
  
  double out1 [] = { 0.1 };
  double out2 [] = { 0.9 };
  double out3 [] = { 0.9 };
  double out4 [] = { 0.1 };
  
  Network * net2 = nNetwork(3, 2, 2, 1);
  
  TrainingSet * set1 = tsTrainingSet(net2);
  TrainingSet * set2 = tsTrainingSet(net2);
  TrainingSet * set3 = tsTrainingSet(net2);
  TrainingSet * set4 = tsTrainingSet(net2);
  
  tsSetTVector(set1, in1);
  tsSetTVector(set2, in2);
  tsSetTVector(set3, in3);
  tsSetTVector(set4, in4);
  
  tsSetOVector(set1, out1);
  tsSetOVector(set2, out2);
  tsSetOVector(set3, out3);
  tsSetOVector(set4, out4);
  
  TrainingSet * sets[4] = { set1, set2, set3, set4 };
  BPTrainer * trainer = bpTrainerWithoutSets(net2, 4, sets);
  bpTrain3(trainer, 1000000, 0.1, 0.05, 0.01, 0.5);
  
  printf("--------------------\n");
  printf("training finished...\n");
  printf("--------------------\n");
  
  nSetInputs(net2, in1);
  nFeedforward(net2);
  
  nSetInputs(net2, in2);
  nFeedforward(net2);
  
  nSetInputs(net2, in3);
  nFeedforward(net2);
  
  nSetInputs(net2, in4);
  nFeedforward(net2);
   */
   
  
  /*
  Network * net = nNetwork(3, 35, 10, 10);
  BPSet * set0 = bpSet(net);
  BPSet * set1 = bpSet(net);
  BPSet * set2 = bpSet(net);
  
  set0->output = 0;
  set1->output = 1;
  set2->output = 2;
  
  double in0 [] = {
    1, 1, 1, 1, 1, 1, 1,
    1, 1, 0, 0, 0, 1, 1,
    1, 0, 0, 0, 0, 0, 1,
    1, 1, 0, 0, 0, 1, 1,
    1, 1, 1, 1, 1, 1, 1
  };
  
  double in1 [] = {
    0, 0, 1, 1, 0, 0, 0,
    0, 0, 1, 1, 0, 0, 0,
    0, 1, 0, 1, 0, 0, 0,
    0, 0, 0, 1, 0, 0, 0,
    1, 1, 1, 1, 1, 1, 1
  };
  
  double in2 [] = {
    0, 0, 1, 1, 1, 0, 0,
    0, 1, 0, 0, 1, 0, 0,
    0, 1, 0, 0, 1, 0, 0,
    0, 0, 0, 1, 0, 0, 0,
    1, 1, 1, 1, 1, 1, 1
  };
  
  sizeof(uint8_t)
  BPTrainer2 * trainer = bpTrainer2(net, 3);
  trainer->sets[0] = set0;
  trainer->sets[1] = set1;
  trainer->sets[2] = set2;
  
  bpTrain2(trainer);
  
  nSetInputs(net, in2);
  */
  
  /*
  nSetInputs(net2, tsTVector(set4));
  nFeedforward(net2);
  double out[1];
  nOutput(net2, out);
  printf("net.out: %f\n", out[0]);
  double targets[1] = { 0 };
  printf("net.MSE: %f\n", nMSE(net2, targets));
  
  double deltas[1];
  double ** grads = (double **)malloc(sizeof(double *));
  for (int i = 0; i < 1; i++)
    *(grads + i) = (double *)calloc(3 , sizeof(double));
  nOGradient(net2, targets, deltas, grads);
  
  double hdeltas[2];
  double ** hgrads = (double **)malloc(2 * sizeof(double *));
  for (int i = 0; i < 2; i++)
    *(hgrads + i) = (double *)calloc(3 , sizeof(double));
  nHGradient(net2, 1, true, hdeltas, deltas, hgrads);
   
  
  printf("delta: %f\n", deltas[0]);
  printf("gradient: %f %f %f\n", grads[0][0], grads[0][1], grads[0][2]);
  
  printf("hidden deltas: %f %f\n", hdeltas[0], hdeltas[1]);
  printf("hidden gradient: %f %f %f\n\t\t %f %f %f\n",
                hgrads[0][0], hgrads[0][1], hgrads[0][2],
                hgrads[1][0], hgrads[1][1], hgrads[1][2]
          );
  */  
  
  /*
  Network * net = nNetwork(3, 221, 4, 10);
  TrainingSet * sets[10];
  char classes[] = {
    '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'
  };
  nSetClasses(net, classes);
  register int i = 0;
  
  //ins
  double in_0 [221] = {
    OFF, OFF, OFF, OFF, ON , ON , ON , ON , ON , OFF, OFF, OFF, OFF,
    OFF, OFF, OFF, ON , OFF, OFF, OFF, OFF, OFF, ON , OFF, OFF, OFF,
    OFF, OFF, ON , OFF, OFF, OFF, OFF, OFF, OFF, OFF, ON , OFF, OFF,
    OFF, ON , OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, ON , OFF,
    OFF, ON , OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, ON , OFF,
    OFF, ON , OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, ON , OFF,
    OFF, ON , OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, ON , OFF,
    OFF, ON , OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, ON , OFF,
    OFF, ON , OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, ON , OFF,
    OFF, ON , OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, ON , OFF,
    OFF, ON , OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, ON , OFF,
    OFF, ON , OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, ON , OFF,
    OFF, ON , OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, ON , OFF,
    OFF, ON , OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, ON , OFF,
    OFF, OFF, ON , OFF, OFF, OFF, OFF, OFF, OFF, OFF, ON , OFF, OFF, 
    OFF, OFF, OFF, ON , OFF, OFF, OFF, OFF, OFF, ON , OFF, OFF, OFF, 
    OFF, OFF, OFF, OFF, ON , ON , ON , ON , ON , OFF, OFF, OFF, OFF
  };

  double in_1 [221] = {
    OFF, OFF, OFF, OFF, OFF, OFF, ON , OFF, OFF, OFF, OFF, OFF, OFF, 
    OFF, OFF, OFF, OFF, OFF, ON , ON , OFF, OFF, OFF, OFF, OFF, OFF, 
    OFF, OFF, OFF, OFF, ON , OFF, ON , OFF, OFF, OFF, OFF, OFF, OFF, 
    OFF, OFF, OFF, ON , OFF, OFF, ON , OFF, OFF, OFF, OFF, OFF, OFF,
    OFF, OFF, OFF, OFF, OFF, OFF, ON , OFF, OFF, OFF, OFF, OFF, OFF, 
    OFF, OFF, OFF, OFF, OFF, OFF, ON , OFF, OFF, OFF, OFF, OFF, OFF,
    OFF, OFF, OFF, OFF, OFF, OFF, ON , OFF, OFF, OFF, OFF, OFF, OFF,
    OFF, OFF, OFF, OFF, OFF, OFF, ON , OFF, OFF, OFF, OFF, OFF, OFF,
    OFF, OFF, OFF, OFF, OFF, OFF, ON , OFF, OFF, OFF, OFF, OFF, OFF,
    OFF, OFF, OFF, OFF, OFF, OFF, ON , OFF, OFF, OFF, OFF, OFF, OFF,
    OFF, OFF, OFF, OFF, OFF, OFF, ON , OFF, OFF, OFF, OFF, OFF, OFF,
    OFF, OFF, OFF, OFF, OFF, OFF, ON , OFF, OFF, OFF, OFF, OFF, OFF,
    OFF, OFF, OFF, OFF, OFF, OFF, ON , OFF, OFF, OFF, OFF, OFF, OFF,
    OFF, OFF, OFF, OFF, OFF, OFF, ON , OFF, OFF, OFF, OFF, OFF, OFF,
    OFF, OFF, OFF, OFF, OFF, OFF, ON , OFF, OFF, OFF, OFF, OFF, OFF,
    OFF, OFF, OFF, OFF, OFF, OFF, ON , OFF, OFF, OFF, OFF, OFF, OFF,
    OFF, OFF, OFF, ON , ON , ON , ON , ON , ON , ON , OFF, OFF, OFF
  };

  double in_2 [221] = {
    OFF, OFF, OFF, ON , ON , ON , ON , ON , ON , ON , OFF, OFF, OFF, 
    OFF, ON , ON , OFF, OFF, OFF, OFF, OFF, OFF, OFF, ON , OFF, OFF, 
    ON , OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, ON , OFF,
    ON , OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, ON , OFF, 
    OFF, ON , OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, ON ,
    OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, ON ,
    OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, ON ,
    OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, ON , OFF,
    OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, ON , OFF,
    OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, ON , OFF, OFF,
    OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, ON , OFF, OFF, OFF,
    OFF, OFF, OFF, OFF, OFF, OFF, OFF, ON , ON , OFF, OFF, OFF, OFF,
    OFF, OFF, OFF, OFF, OFF, ON , ON , OFF, OFF, OFF, OFF, OFF, OFF,
    OFF, OFF, OFF, OFF, ON , OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF,
    OFF, OFF, ON , ON , OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF,
    OFF, ON , OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF,
    ON , ON , ON , ON , ON , ON , ON , ON , ON , ON , ON , ON , ON 
  };

  double in_3 [221] = {
    OFF, OFF, OFF, ON , ON , ON , ON , ON , ON , OFF, OFF, OFF, OFF, 
    OFF, OFF, ON , OFF, OFF, OFF, OFF, OFF, OFF, ON , ON , OFF, OFF, 
    OFF, ON , OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, ON , OFF,
    OFF, ON , OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, ON , 
    OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, ON ,
    OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, ON , OFF, 
    OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, ON , OFF,
    OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, ON , OFF, OFF,
    OFF, OFF, OFF, ON , ON , ON , ON , ON , ON , ON , OFF, OFF, OFF,
    OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, ON , OFF, OFF,
    OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, ON , OFF,
    OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, ON , OFF,
    OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, ON ,
    OFF, ON , OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, ON ,
    OFF, ON , OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, ON , OFF,
    OFF, OFF, ON , OFF, OFF, OFF, OFF, OFF, OFF, ON , ON , OFF, OFF, 
    OFF, OFF, OFF, ON , ON , ON , ON , ON , ON , OFF, OFF, OFF, OFF
  };

  double in_4 [221] = {
    OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, ON , OFF, OFF, OFF, OFF,
    OFF, OFF, OFF, OFF, OFF, OFF, OFF, ON , ON , OFF, OFF, OFF, OFF,
    OFF, OFF, OFF, OFF, OFF, OFF, ON , OFF, ON , OFF, OFF, OFF, OFF,
    OFF, OFF, OFF, OFF, OFF, ON , OFF, OFF, ON , OFF, OFF, OFF, OFF,
    OFF, OFF, OFF, OFF, ON , OFF, OFF, OFF, ON , OFF, OFF, OFF, OFF, 
    OFF, OFF, OFF, ON , OFF, OFF, OFF, OFF, ON , OFF, OFF, OFF, OFF,
    OFF, OFF, ON , OFF, OFF, OFF, OFF, OFF, ON , OFF, OFF, OFF, OFF,
    OFF, ON , OFF, OFF, OFF, OFF, OFF, OFF, ON , OFF, OFF, OFF, OFF,
    ON , ON , ON , ON , ON , ON , ON , ON , ON , ON , ON , OFF, OFF, 
    OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, ON , OFF, OFF, OFF, OFF, 
    OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, ON , OFF, OFF, OFF, OFF,
    OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, ON , OFF, OFF, OFF, OFF,
    OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, ON , OFF, OFF, OFF, OFF,
    OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, ON , OFF, OFF, OFF, OFF,
    OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, ON , OFF, OFF, OFF, OFF,
    OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, ON , OFF, OFF, OFF, OFF,
    OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, ON , OFF, OFF, OFF, OFF
  };

  double in_5 [221] = {
    ON , ON , ON , ON , ON , ON , ON , ON , ON , ON , ON , ON , ON ,
    ON , OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF,
    ON , OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF,
    ON , OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF,
    ON , OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF,
    ON , OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF,
    ON , ON , ON , ON , ON , ON , ON , ON , ON , ON , OFF, OFF, OFF,
    OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, ON , OFF, OFF,
    OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, ON , OFF,
    OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, ON , OFF,
    OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, ON ,
    OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, ON ,
    OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, ON ,
    ON , OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, ON , OFF,
    OFF, ON , OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, ON , OFF,
    OFF, OFF, ON , OFF, OFF, OFF, OFF, OFF, OFF, OFF, ON , OFF, OFF, 
    OFF, OFF, OFF, ON , ON , ON , ON , ON , ON , ON , OFF, OFF, OFF
  };

  double in_6 [221] = {
    OFF, OFF, OFF, OFF, OFF, OFF, OFF, ON , ON , ON , ON , ON , ON , 
    OFF, OFF, OFF, OFF, OFF, ON , ON , OFF, OFF, OFF, OFF, OFF, OFF,
    OFF, OFF, OFF, ON , ON , OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF,
    OFF, OFF, ON , OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF,
    OFF, OFF, ON , OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF,
    OFF, ON , OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF,
    OFF, ON , OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF,
    ON , OFF, OFF, ON , ON , ON , ON , ON , ON , ON , OFF, OFF, OFF, 
    ON , ON , ON , OFF, OFF, OFF, OFF, OFF, OFF, OFF, ON , OFF, OFF, 
    ON , OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, ON , OFF,
    ON , OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, ON ,
    ON , OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, ON , 
    ON , OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, ON ,
    ON , OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, ON , OFF,
    OFF, ON , OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, ON , OFF, OFF,
    OFF, OFF, ON , OFF, OFF, OFF, OFF, OFF, OFF, ON , OFF, OFF, OFF, 
    OFF, OFF, OFF, ON , ON , ON , ON , ON , ON , OFF, OFF, OFF, OFF
  };

  double in_7 [221] = {
    ON , ON , ON , ON , ON , ON , ON , ON , ON , ON , ON , ON , OFF,
    ON , OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, ON ,
    OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, ON , OFF,
    OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, ON , OFF, OFF, 
    OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, ON , OFF, OFF, 
    OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, ON , OFF, OFF, OFF, 
    OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, ON , OFF, OFF, OFF, OFF, 
    OFF, OFF, OFF, OFF, OFF, OFF, OFF, ON , OFF, OFF, OFF, OFF, OFF,
    OFF, OFF, OFF, OFF, OFF, OFF, OFF, ON , OFF, OFF, OFF, OFF, OFF,
    OFF, OFF, OFF, OFF, OFF, OFF, ON , OFF, OFF, OFF, OFF, OFF, OFF, 
    OFF, OFF, OFF, OFF, OFF, ON , OFF, OFF, OFF, OFF, OFF, OFF, OFF,
    OFF, OFF, OFF, OFF, ON , OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, 
    OFF, OFF, OFF, OFF, ON , OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, 
    OFF, OFF, OFF, ON , OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF,
    OFF, OFF, ON , OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, 
    OFF, OFF, ON , OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF,  
    OFF, ON , OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF
  };

  double in_8 [221] = {
    OFF, OFF, OFF, ON , ON , ON , ON , ON , ON , ON , OFF, OFF, OFF, 
    OFF, OFF, ON , OFF, OFF, OFF, OFF, OFF, OFF, OFF, ON , OFF, OFF, 
    OFF, ON , OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, ON , OFF, 
    ON , OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, ON ,
    ON , OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, ON , 
    ON , OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, ON ,
    ON , OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, ON ,
    OFF, ON , OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, ON , OFF, 
    OFF, OFF, ON , ON , ON , ON , ON , ON , ON , ON , ON , OFF, OFF, 
    OFF, ON , OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, ON , OFF, 
    ON , OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, ON , 
    ON , OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, ON ,
    ON , OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, ON , 
    ON , OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, ON ,
    OFF, ON , OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, ON , OFF, 
    OFF, OFF, ON , OFF, OFF, OFF, OFF, OFF, OFF, OFF, ON , OFF, OFF, 
    OFF, OFF, OFF, ON , ON , ON , ON , ON , ON , ON , OFF, OFF, OFF
  };

  double in_9 [221] = {
    OFF, OFF, OFF, ON , ON , ON , ON , ON , ON , ON , OFF, OFF, OFF,
    OFF, OFF, ON , OFF, OFF, OFF, OFF, OFF, OFF, OFF, ON , OFF, OFF, 
    OFF, ON , OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, ON , OFF,
    ON , OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, ON ,
    ON , OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, ON ,
    ON , OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, ON ,
    ON , OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, ON , OFF,
    OFF, ON , OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, ON , OFF,
    OFF, OFF, ON , ON , ON , ON , ON , ON , ON , ON , ON , OFF, OFF, 
    OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, ON , OFF, OFF,
    OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, ON , OFF, OFF, OFF, 
    OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, ON , OFF, OFF, OFF, OFF, 
    OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, ON , OFF, OFF, OFF, OFF,
    OFF, OFF, OFF, OFF, OFF, OFF, OFF, ON , OFF, OFF, OFF, OFF, OFF,
    OFF, OFF, OFF, OFF, OFF, OFF, ON , OFF, OFF, OFF, OFF, OFF, OFF,
    OFF, OFF, OFF, OFF, OFF, ON , OFF, OFF, OFF, OFF, OFF, OFF, OFF,
    OFF, OFF, OFF, OFF, ON , OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF
  };
  
  //outs
  double out0 [] = {
    ON , OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF
  };
  
  double out1 [] = {
    OFF, ON , OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF
  };
  
  double out2 [] = {
    OFF, OFF, ON , OFF, OFF, OFF, OFF, OFF, OFF, OFF
  };
  
  double out3 [] = {
    OFF, OFF, OFF, ON , OFF, OFF, OFF, OFF, OFF, OFF
  };
  
  double out4 [] = {
    OFF, OFF, OFF, OFF, ON , OFF, OFF, OFF, OFF, OFF
  };
  
  double out5 [] = {
    OFF, OFF, OFF, OFF, OFF, ON , OFF, OFF, OFF, OFF
  };
  
  double out6 [] = {
    OFF, OFF, OFF, OFF, OFF, OFF, ON , OFF, OFF, OFF
  };
  
  double out7 [] = {
    OFF, OFF, OFF, OFF, OFF, OFF, OFF, ON , OFF, OFF
  };
  
  double out8 [] = {
    OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, ON , OFF
  };
  
  double out9 [] = {
    OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, ON
  };
  
  // surragations
  double * ins [] = {
    in_0, in_1, in_2, in_3, in_4, in_5, in_6, in_7, in_8, in_9
  };
  
  double * outs [] = {
    out0, out1, out2, out3, out4, out5, out6, out7, out8, out9
  }; 
  
  
  for (i; i < 10; i++) {
    sets[i] = tsTrainingSet(net);
    tsSetTVector(sets[i], ins[i]);
    tsSetOVector(sets[i], outs[i]);
  }
  
  int output;   
  
  
  BPTrainer * trainer = bpTrainerWithoutSets(net, 10, sets);
  bpTrain3(trainer, 10000000, 0.6, 0.1, 0.01, 0.5);
     
  
  for (int i = 0; i < 10; i++) {
    nSetInputs(net, tsTVector(sets[i]));
    nFeedforward(net);
    output = nOutputClass(net);
    printf("target: %c output class: %c\n", nClass(net, i), output);
  }
  
  nDelete(net);
  */
  
  /*
  if (nSave(net) == OPERATION_SUCCESS)
    printf("network was saved.\n");
  */
  
  /*
  Network * net2 = nLoad();
  
  printf("net2.nLayers: %d\n", nNumberOfLayers(net2));
  printf("net2.nNeurons(input) : %d\n", nNumberOfNeurons(net2, 1));
  printf("net2.nNeurons(hidden): %d\n", nNumberOfNeurons(net2, 2));
  printf("net2.nNeurons(output): %d\n", nNumberOfNeurons(net2, 3));
  
  if (net2) {
    printf("network was loaded.\n");
    for (int i = 0; i < EPOCH_SIZE; i++) {
      nSetInputs(net2, tsTVector(sets[i]));
      nFeedforward(net2);
      output = nOutputClass(net2);
      printf("target: %c output class: %c\n", nClass(i), output);
    }
  }
  
  if (net == net2)
    printf("net = net2..\n");
   */
  
  /*
   *
   * The era long learning shit
  
  globalNet = nNetwork(3, 221, 60, 62);
  createSets(globalNet, EPOCH_SIZE, globalSets);
  
  BPTrainer * trainer = bpTrainerWithoutSets(globalNet, 62, globalSets);
  
  bpTrain3(trainer, 100000000, 0.2, 0.1, 0.009, 0.5);
  
  int output;
  
  for (int i = 0; i < EPOCH_SIZE; i++) {
    nSetInputs(globalNet, tsTVector(globalSets[i]));
    nFeedforward(globalNet);
    output = nOutputClass(globalNet);
    printf("target: %c output class: %c\n", nClass(globalNet, i), output);
  }
   */
  
  /*
  globalNet = nNetwork(3, 70, 40, 62);
  createSets2(globalNet, EPOCH_SIZE, globalSets);
  
  BPTrainer * trainer = bpTrainerWithoutSets(globalNet, 62, globalSets);
  
  bpTrain3(trainer, 100000000, 0.01, 0.02, 0.0018, 0.5);
  
  int output;
  
  for (int i = 0; i < EPOCH_SIZE; i++) {
    nFeedforward(globalNet, tsTVector(globalSets[i]));
    output = nOutputClass(globalNet);
    printf("target: %c output class: %c\n", nClass(globalNet, i), output);
  }
   
  nSave(globalNet, "/home/saintme/Desktop/networks/net70_40_62-0_01-0_02-0_0018");
  printf("net was saved..\n");
  for (int i = 0; i < EPOCH_SIZE; i++) {
    nFeedforward(globalNet, tsTVector(globalSets[i]));
    output = nOutputClass(globalNet);
    printf("target: %c output class: %c\n", nClass(globalNet, i), output);
  }
  nDelete(&globalNet);
  */
  
  Network * net = nLoad("/home/saintme/Desktop/networks/net70_40_62-0_01-0_02-0_0018.net");
  if (!net) 
    printf("net was not loaded\n");
  else {
    createSets2(net, EPOCH_SIZE, globalSets);
    int output;
    
    BPTrainer * trainer = bpTrainerWithoutSets(net, 62, globalSets);
  
    bpTrain3(trainer, 100000000, 0.01, 0.02, 0.0017, 0.5);
  
    for (int i = 0; i < EPOCH_SIZE; i++) {
      nFeedforward(net, tsTVector(globalSets[i]));
      output = nOutputClass(net);
      printf("target: %c output class: %c\n", nClass(net, i), output);
    }
   
    nSave(net, "/home/saintme/Desktop/networks/net70_40_62-0_01-0_02-0_0018");
    printf("net was saved..\n");
    nDelete(&net);
  }
  
  /*
  TrainingSet * set = tsTrainingSet(2, 2);
  double * tv = tsTVector(set);
  double * ov = tsOVector(set);
  tv[0] = 0.0;
  tv[1] = 1.0;
  ov[0] = 0.0;
  ov[1] = 0.1;
  tsSave(set, "./data/ts/savedts/tsTrial1");
  */
  t2 = time(NULL);  
  printf ("\n\nTime:	%f\n", difftime(t2, t1));
  
  return (EXIT_SUCCESS);
}
