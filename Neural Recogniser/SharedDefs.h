/**
 * Copyright 2008 MentorLycon, LLC.
 * The author of this software is Kareem Alkaseer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _SHAREDDEFS_H
#define	_SHAREDDEFS_H

#ifdef	__cplusplus
extern "C" {
#endif
  
  #ifdef _REVEAL_
          
  #include <stdbool.h>

  typedef struct layer_t {
    size_t  nNeurons;
    double  * neurons;
  }Layer;

  typedef struct weight_t {
    size_t  columns, 
            rows;
    double  ** vectors;
  }Weight;

  struct network_t {
    size_t  nLayers; 
    bool    isClassesSet;
    Layer   * layers;
    Weight  * weights;
    char    * classes;
    BPTrainer * trainer;
  };
  
  #endif //__REVEAL__

#ifdef	__cplusplus
}
#endif

#endif	/* _SHAREDDEFS_H */

