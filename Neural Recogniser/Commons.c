/**
 * Copyright 2008 MentorLycon, LLC.
 * The author of this software is Kareem Alkaseer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <math.h>
#include "Commons.h"
#include "GlobalConstants.h"

 // nError () and nWarn()
 //----------------------------------------------
 
void nError (const char * s, const char * funcName) {
  fprintf(stderr, "|-> Error: %s\n|-> Emitted from %s\n", s, funcName);
}

void nWarn(const char * s, const char * funcName) {
  fprintf(stderr, "|-> Warning: %s\n|-> Emitted from %s\n", s, funcName);
}

void nFeedbackInfo(const char * s) {
  fprintf(stdout, "%s\n", s);
}
  
double sigmoid(double value) {
  return 1.0 / (1.0 + exp(-value));
}
  
int commonMax(double vector[], size_t length, ClassIdentifier * ci) {
  if (!ci) {
    return OPERATION_FAILURE;
  }
  
  double maxElement = vector[0];
  size_t maxElementIndex = 0;
  
  for (int i = 1; i < length; i++)
    if (maxElement < vector[i]) {
      maxElement = vector[i];
      maxElementIndex = i;
    }
  
  ci->value = maxElement;
  ci->position = maxElementIndex;
  
  return OPERATION_SUCCESS;
}


double dotProduct24(size_t length, double * vector1, double * vector2) {
  /*
   * Note that this optimised function does not check whether
   * the vectors given are of valid pointers (they may be null
   * pointers, for example). It is the responsibility of the 
   * caller to make sure that these pointers are vaild.
   * Note also that length is not checked against increment size, 24,
   * which can also lead to serious memory access faults if length
   * is less than the increment size.
   * This methodology was followed as not to result in performance
   * degradation of a supposedly optimising function.
   */
  
  size_t division,
         remainder;
  double sum = 0.0;
  
  division = length / 24;
  remainder = length % 24;
  
  while (division--) {
    sum += *vector1         *   *vector2;
    sum += *(vector1 + 1)   *   *(vector2 + 1);
    sum += *(vector1 + 2)   *   *(vector2 + 2);
    sum += *(vector1 + 3)   *   *(vector2 + 3);
    sum += *(vector1 + 4)   *   *(vector2 + 4);
    sum += *(vector1 + 5)   *   *(vector2 + 5);
    sum += *(vector1 + 6)   *   *(vector2 + 6);
    sum += *(vector1 + 7)   *   *(vector2 + 7);
    sum += *(vector1 + 8)   *   *(vector2 + 8);
    sum += *(vector1 + 9)   *   *(vector2 + 9);
    sum += *(vector1 + 10)  *   *(vector2 + 10);
    sum += *(vector1 + 11)  *   *(vector2 + 11);
    sum += *(vector1 + 12)  *   *(vector2 + 12);
    sum += *(vector1 + 13)  *   *(vector2 + 13);
    sum += *(vector1 + 14)  *   *(vector2 + 14);
    sum += *(vector1 + 15)  *   *(vector2 + 15);
    sum += *(vector1 + 16)  *   *(vector2 + 16);
    sum += *(vector1 + 17)  *   *(vector2 + 17);
    sum += *(vector1 + 18)  *   *(vector2 + 18);
    sum += *(vector1 + 19)  *   *(vector2 + 19);
    sum += *(vector1 + 20)  *   *(vector2 + 20);
    sum += *(vector1 + 21)  *   *(vector2 + 21);
    sum += *(vector1 + 22)  *   *(vector2 + 22);
    sum += *(vector1 + 23)  *   *(vector2 + 23);
    
    vector1 += 24;
    vector2 += 24;
  }
  
  while (remainder--)
    sum += *vector1++ * *vector2++;
  
  return sum;
}

double dotProduct16(size_t length, double * vector1, double * vector2) {
  /*
   * Note that this optimised function does not check whether
   * the vectors given are of valid pointers (they may be null
   * pointers, for example). It is the responsibility of the 
   * caller to make sure that these pointers are vaild.
   * Note also that length is not checked against increment size, 16,
   * which can also lead to serious memory access faults if length
   * is less than the increment size.
   * This methodology was followed as not to result in performance
   * degradation of a supposedly optimising function.
   */
  
  size_t division,
         remainder;
  double sum = 0.0;
  
  division = length / 16;
  remainder = length % 16;
  
  while (division--) {
    sum += *vector1         *   *vector2;
    sum += *(vector1 + 1)   *   *(vector2 + 1);
    sum += *(vector1 + 2)   *   *(vector2 + 2);
    sum += *(vector1 + 3)   *   *(vector2 + 3);
    sum += *(vector1 + 4)   *   *(vector2 + 4);
    sum += *(vector1 + 5)   *   *(vector2 + 5);
    sum += *(vector1 + 6)   *   *(vector2 + 6);
    sum += *(vector1 + 7)   *   *(vector2 + 7);
    sum += *(vector1 + 8)   *   *(vector2 + 8);
    sum += *(vector1 + 9)   *   *(vector2 + 9);
    sum += *(vector1 + 10)  *   *(vector2 + 10);
    sum += *(vector1 + 11)  *   *(vector2 + 11);
    sum += *(vector1 + 12)  *   *(vector2 + 12);
    sum += *(vector1 + 13)  *   *(vector2 + 13);
    sum += *(vector1 + 14)  *   *(vector2 + 14);
    sum += *(vector1 + 15)  *   *(vector2 + 15);
    
    vector1 += 16;
    vector2 += 16;
  }
  
  while (remainder--)
    sum += *vector1++ * *vector2++;
  
  return sum;
}

double dotProduct8(size_t length, double * vector1, double * vector2) {
  /*
   * Note that this optimised function does not check whether
   * the vectors given are of valid pointers (they may be null
   * pointers, for example). It is the responsibility of the 
   * caller to make sure that these pointers are vaild.
   * Note also that length is not checked against increment size, 8,
   * which can also lead to serious memory access faults if length
   * is less than the increment size.
   * This methodology was followed as not to result in performance
   * degradation of a supposedly optimising function.
   */
  
  size_t division,
         remainder;
  double sum = 0.0;
  
  division = length / 8;
  remainder = length % 8;
  
  while (division--) {
    sum += *vector1       * *vector2;
    sum += *(vector1 + 1) * *(vector2 + 1);
    sum += *(vector1 + 2) * *(vector2 + 2);
    sum += *(vector1 + 3) * *(vector2 + 3);
    sum += *(vector1 + 4) * *(vector2 + 4);
    sum += *(vector1 + 5) * *(vector2 + 5);
    sum += *(vector1 + 6) * *(vector2 + 6);
    sum += *(vector1 + 7) * *(vector2 + 7);
    
    vector1 += 8;
    vector2 += 8;
  }
  
  while (remainder--)
    sum += *vector1++ * *vector2++;
  
  return sum;
}

double dotProduct4(size_t length, double * vector1, double * vector2) {
  /*
   * Note that this optimised function does not check whether
   * the vectors given are of valid pointers (they may be null
   * pointers, for example). It is the responsibility of the 
   * caller to make sure that these pointers are vaild.
   * Note also that length is not checked against increment size, 4,
   * which can also lead to serious memory access faults if length
   * is less than the increment size.
   * This methodology was followed as not to result in performance
   * degradation of a supposedly optimising function.
   */
  
  size_t division,
         remainder;
  double sum = 0.0;
  
  division = length / 4;
  remainder = length % 4;
  
  while (division--) {
    sum += *vector1       * *vector2;
    sum += *(vector1 + 1) * *(vector2 + 1);
    sum += *(vector1 + 2) * *(vector2 + 2);
    sum += *(vector1 + 3) * *(vector2 + 3);
    
    vector1 += 4;
    vector2 += 4;
  }
  
  while (remainder--)
    sum += *vector1++ * *vector2++;
  
  return sum;
}
