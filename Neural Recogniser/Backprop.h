/**
 * Copyright 2008 MentorLycon, LLC.
 * The author of this software is Kareem Alkaseer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _BACKPROP_H
#define	_BACKPROP_H

#ifdef	__cplusplus
extern "C" {
#endif
  
  #include <stdlib.h>
  #include "Network.h"
  #include "TrainingSet.h"
  #include "GlobalTypes.h"
          
  #define NONEXISTING_BPTRAINTER  -21
  #define NONEXISTING_BPSET       -22          
          
  typedef struct bpset_t {
    size_t  tVectorLength;
    double  * tVector;
    size_t  output;
    Network * net;
  }BPSet;
  
  typedef struct bptrainer2_t {
    size_t  nSets;
    Network * net;
    BPSet   ** sets;
  }BPTrainer2;
          
  //typedef struct bptrainer_t BPTrainer; Declared in GlobalTypes.h
  
  BPTrainer * bpTrainerWithSets(Network * net, size_t nSets);
  BPTrainer * bpTrainerWithoutSets(Network * net, size_t size, TrainingSet * sets[size]);
  int bpDelete(BPTrainer * trainer); // change *trainer to **trainer
  int bpTrain(BPTrainer * trainer);  
  int bpNumberOfSets(BPTrainer * trainer);
  double * bpTVector(BPTrainer * trainer, size_t nSet);  
  double * bpOVector(BPTrainer * trainer, size_t nSet);
  Network * bpNetwork(BPTrainer * trainer);
  
  BPSet * bpSet(Network * net);
  int bpSetDelete(BPSet ** set);
  
  BPTrainer2 * bpTrainer2(Network * net, size_t size);
  int bpDelete2(BPTrainer2 ** trainer);
  int bpTrain2 (BPTrainer2 * trainer);
  
  double bpTrain3(
          BPTrainer * trainer,
          size_t    maxiter,    // Maximum iterations allows
          double    rate,       // Learning rate, gradient multiplier
          double    mom,        // Momentum coefficient
          double    errtol,     // Quit if error drops this low
          double    gradtol     // Quit if all gradients this small
        );
  
  double bpFindGradient(
        BPTrainer * trainer,
        double  *  outdelta,
        double  ** outgrad,
        double  ** hidgrad,
        size_t  nout,
        size_t  nhid,
        size_t  nin
      );

#ifdef	__cplusplus
}
#endif

#endif	/* _BACKPROP_H */

