/**
 * Copyright 2008 MentorLycon, LLC.
 * The author of this software is Kareem Alkaseer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <math.h>
#include <stdlib.h>
#include "LogisticSigmoid.h"
#include "Commons.h"
#include "GlobalConstants.h"

static int length;
static double maxValue;
static bool shouldInit = true;

static double actFactor,
              * actTable,
              * actDifferenceTable;

int lsInit(int len, double max) {
  if (shouldInit) {
    if (len <= 0) {
      nError("lsInit was not executed. Given length is incorrect", __func__);
      return OPERATION_FAILURE;
    }
    
    actTable = (double *)malloc(len * sizeof(double));
    if (!actTable) {
      nError("work memory could not be allocated. lsInit was not executed.", __func__);
      return MEMORY_EXHAUSTION;
    }
    
    actDifferenceTable = (double *)malloc(len * sizeof(double));
    if (!actDifferenceTable) {
      nError("work memory could not be allocated. lsInit was not executed.", __func__);
      free(actTable);
      return MEMORY_EXHAUSTION;
    }
    
    length = len;
    maxValue = max;
    
    int i;
    
    actFactor = (length - 1) / maxValue;
    for(i = 0; i < length; i++) {
      actTable[i] = 1.0 / (1.0 + exp(-((double)i) / actFactor));
      if (i)
        actDifferenceTable[i - 1] = actTable[i] - actTable[i - 1];
    }    
    shouldInit = false;
  }
  
  return OPERATION_SUCCESS;
}

double lsSigmoid(double x) {
  int i;
  double xd;
  
  if (x >= 0.0) {                                   // Handle this half of the function
    xd = x * actFactor;                             // Find location in table
    i = (int) xd;                                   // Subscript in table
    if (i >= length - 1)                            // If outside table
      return actTable[length - 1];                  // Return highest value
    return actTable[i] + (actDifferenceTable[i] * (xd - i));  // else interpolate
  }
  else {                                            // Handle the other half of function
    xd = -x * actFactor;
    i = (int) xd;
    if (i >= length - 1)
      return 1.0 - actTable[length - 1];
    return 1.0 - (actTable[i] + (actDifferenceTable[i] * (xd - i)));
  }
}

bool lsIsInitialised(void) {
  return ~shouldInit;
} 

double lsDerivative(double activation) {
  return activation * (1.0 - activation);
}
