/**
 * Copyright 2008 MentorLycon, LLC.
 * The author of this software is Kareem Alkaseer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdarg.h>
#include <time.h>
#include <math.h>
#include <inttypes.h>
#include <string.h>
#include "Network.h"
#include "Commons.h"
#include "GlobalConstants.h"
#include "LogisticSigmoid.h"

#define _REVEAL_
#include "SharedDefs.h"

#define NONEXISTING_INNER_INSTANCE  -1111

static const uint32_t NETWORK_SIGNATURE = 19181716;

int nPrivateDeleteLayers (Network * net, size_t index);
int nPrivateDeleteWeights (Network * net, size_t nWeights, size_t nRows);
void nPrivateInitWeights (Network * net);

// nNetwork() : Network Constructor
//---------------------------------
Network * nNetwork(size_t nLayers, ...) {
  
  size_t i = 0;
  
  // Initialise a variable argument list to process the paramters of the function
  va_list list;
  va_start(list, nLayers);
  
  size_t args[nLayers];
  for (i; (i < nLayers) && (list != 0); i++)
    args[i] = (va_arg(list, size_t));  
  
  va_end(list);
  
  if (i != nLayers) {
    nError("network was not created. Check your parameters to the network initialiser", __func__);
    return (Network *)NULL;
  }
  
  const char * s = "cannot allocate required memory. Network was not created";
  
  // Allocate memory for the network struct
  Network * net = (Network *)malloc(sizeof(Network));
  if (net == NULL) {
    nError(s, __func__);
    return (Network *)NULL;
  }
  
  // Intialise Network's non-pointer members
  net->nLayers = nLayers;
  
  // Allocate memory for Network's member (Layer)layers according to the
  // number of layers specified by the caller.
  net->layers = (Layer *)malloc(nLayers * sizeof(Layer));
  if (net->layers == NULL) {
    nError(s, __func__);
    free(net);
    return (Network *)NULL;
  }
  
  // Allocate memory for each struct pointed to by "layers" and intialise it
  for(i = 0; i < nLayers; i++) {
    net->layers[i].nNeurons = args[i];
    
    if (i != nLayers - 1) {
      size_t n = ++net->layers[i].nNeurons;
      net->layers[i].neurons =
            (double *)calloc(net->layers[i].nNeurons, sizeof(double));
      net->layers[i].neurons[n-1] = 1.0;
    }
    else {
      net->layers[i].neurons =
            (double *)calloc(net->layers[i].nNeurons, sizeof(double));
      /*
       * Tonight's additions
       */
      net->classes = (char *)calloc(net->layers[i].nNeurons, sizeof(char));
      if (!net->classes) {
        nError(s, __func__);
        nPrivateDeleteLayers(net, i);
        free(net);
        return (Network *)NULL;
      }
    }
    
    if (net->layers[i].neurons == NULL) {
      nError(s, __func__);
      nPrivateDeleteLayers(net, i);
      free(net);
      return (Network *)NULL;
    }
  }
  
  // Allocate memory for Network's (Weight)weights so that the number of
  // weight vectors is less than the number of layers by 1
  net->weights = (Weight *)malloc((nLayers - 1) * sizeof(Weight));
  if (net->weights == NULL) {
    nError(s, __func__);
    nPrivateDeleteLayers(net, net->nLayers);
    free(net);
    return (Network *)NULL;
  }
  
  for (i = 0; i < nLayers - 1; i++) {
    // Initialise non-pointer member of the struct
    net->weights[i].rows = net->layers[i+1].nNeurons;
    net->weights[i].columns = net->layers[i].nNeurons;  // account for bias
    
    // Allocate memory for each multidimenstional array contained in each
    // struct pointed to be "weights"
    net->weights[i].vectors = 
            (double **)calloc(net->weights[i].rows, sizeof(double *));
    if (net->weights[i].vectors == NULL) {
      nError(s, __func__);
      nPrivateDeleteLayers(net, net->nLayers);
      nPrivateDeleteWeights(net, i, 0);
      free(net);
      return (Network *)NULL;
    }
    
    for(int j = 0; j < net->weights[i].rows; j++) {
      net->weights[i].vectors[j] = 
              (double *)calloc(net->weights[i].columns, sizeof(double));
      if(net->weights[i].vectors[j] == NULL) {
        nError(s, __func__);
        nPrivateDeleteLayers(net, net->nLayers);
        nPrivateDeleteWeights(net, i, j);
        free(net);
        return (Network *)NULL;
      }
    }
  }
  nPrivateInitWeights(net);
  net->trainer = NULL;
  net->isClassesSet = false;
  lsInit(100, 10.0);
  return net;
}

// nPrivateInitWeights()
//----------------------------------------------
void nPrivateInitWeights(Network * net) {
  if (!net) {
    nError("initialisation has been cancelled. Network does not exist.", __func__);
    return;
  }
  int n, m;
  srand(time(NULL));
  for (int i = 0; i < net->nLayers - 1; i++) {
    n = net->weights[i].rows;
    for (int j = 0; j < n; j++) {
      m = net->weights[i].columns;
      for(int k = 0; k < m; k++)
        net->weights[i].vectors[j][k] = (double)(rand() % 10) * 0.1; 
    }
  }
}

// nDelete() : Network Destructor
//--------------------------------------
int nDelete(Network ** net) {
  if (!(*net)) {
    nError("network was not deleted. Network given is NULL.", __func__);
    return NONEXISTING_NETWORK;
  }
  nPrivateDeleteLayers((*net), (*net)->nLayers);
  nPrivateDeleteWeights((*net), (*net)->nLayers - 1, 0);
  free((*net)->classes);
  free((*net));
  (*net) = (Network *)NULL;
  return OPERATION_SUCCESS;
}

// nPrivateDeleteLayers()
//-------------------------------------
int nPrivateDeleteLayers (Network * net, size_t index) {  
  if(!net->layers) {
    nError("trying to delete free memory. \
The layers of the net you are trying to delete do not exist.", "");
    return NONEXISTING_INNER_INSTANCE;
  }
  for (int i = --index; i >= 0; i--)
    free(net->layers[i].neurons);
  free(net->layers);
  return OPERATION_SUCCESS;
}

// nPrivateDeleteWeights
//--------------------------------------
int nPrivateDeleteWeights (Network * net, size_t nWeights, size_t nRows) {
  if(!net->weights) {
    nError("trying to delete free memory. \
The weights of the net you are trying to delete don not exist.", "");
    return NONEXISTING_INNER_INSTANCE;
  }
  
  int rows;
  rows = (nRows == 0) ? net->weights[--nWeights].rows : nRows;
  for (int i = --rows; i >= 0; i--)
    free(net->weights[nWeights].vectors[i]);
  free(net->weights[nWeights].vectors);
  
  for (int i = --nWeights; i >= 0; i--) {
    for (int j = --net->weights[i].rows; j >= 0; j--)
      free(net->weights[i].vectors[j]);
    free(net->weights[i].vectors);
  }
  free(net->weights);
  return OPERATION_SUCCESS;
}

// nReset()
//-----------------------------------------------

int nReset(Network * net) {
  if (!net) {
    nError("network given does not exist.", __func__);
    return NONEXISTING_NETWORK;
  }
  
  for (int i = 0; i < net->nLayers; i++)
    for (int j = 0; j <net->layers[i].nNeurons; j++)
      net->layers[i].neurons[j] = 0.0;
  
  nPrivateInitWeights(net);
  
  size_t n = net->layers[net->nLayers - 1].nNeurons;
  for (int i = 0; i < n; i++)
    net->classes[i] = 0.0;
  // better use memset for that
  
  return OPERATION_SUCCESS;
}


//nPrintLayer()
//--------------------------------------------------
int nPrintLayer(Network * net, size_t layer, bool bias) {
  if (!net) {
    nError("network given does not exist.", __func__);
    return NONEXISTING_NETWORK;
  }
  
  int n;
  
  if (!bias) 
    n = net->layers[layer-1].nNeurons-1;
  else
    n = net->layers[layer-1].nNeurons;
  
  if (layer - 1 == 0)
    printf("Inputs to the Network:\n\n");
  else if (layer == net->nLayers) {
    printf("Output Layer Activations:\n\n");
    n = net->layers[layer-1].nNeurons;
  }
  else
    printf("Activations of Layer no. %d:\n\n", layer-1);
  
  printf("\t");
  for (int i = 0; i < n; i++) {
    
    printf("%3d: %.3f ", i+1, net->layers[layer-1].neurons[i]);
    if (i % 5 == 4)
      printf("\n\t");
  }
  printf("\n");
  return OPERATION_SUCCESS;
}

int nFeedforward(Network * net, double inputVec[]) {
  if (!net) {
    nError("network given does not exist.", __func__);
    return NONEXISTING_NETWORK;
  }
  
  size_t  current, previous, nlayers = net->nLayers;
  Layer   * layers = net->layers;
  Weight  * weights = net->weights;
  double  *vec1, *vec2;
  
  register int nNeurons = net->layers[0].nNeurons - 1; // Don't set for bias
  register int i, j;
     
  // Set network input vector
  for (i = 0; i < nNeurons; i++)
    net->layers[0].neurons[i] = inputVec[i];
  
  for (i = 1; i < nlayers; i++) {
    //printf("\nFeeding Forward...\nIteration no. %d\n", i);
    
    current = (i != net->nLayers - 1) 
                    ? layers[i].nNeurons - 1 : layers[i].nNeurons;
    previous = layers[i-1].nNeurons;
    
    /*
    printf("Number of neurons in layer %d: %d\n", i+1, current);
    printf("Number of neurons in layer %d: %d\n", i, previous);
    printf("Number of rows in weight %d: %d\n", i, net->weights[i-1].rows);
    printf("Number of columns in weight %d: %d\n\n", i, net->weights[i-1].columns);
     */
     
    
    for (j = 0; j < current; j++) {
      vec1 = weights[i-1].vectors[j];
      vec2 = layers[i-1].neurons;

      /*
       * code without optimisation
       *
      for (int k = 0; k < previous; k++)
        layers[i].neurons[j] +=
                weights[i-1].vectors[j][k] * layers[i-1].neurons[k];
      */
      
      layers[i].neurons[j] = dotProduct24(previous, vec1, vec2); 
      layers[i].neurons[j] = lsSigmoid(layers[i].neurons[j]);
    }
  }
  
  return OPERATION_SUCCESS;
}

int nNumberOfLayers(Network * net) {
  if (!net) {
    nError("the network given does not exist.", __func__);
    return NONEXISTING_NETWORK;
  }
  
  return net->nLayers;
}

int nNumberOfNeurons(Network * net, size_t nLayer) {
  if (!net) {
    nError("the network given does not exist.", __func__);
    return NONEXISTING_NETWORK;
  }
  
  if (nLayer > net->nLayers || nLayer == 0) {
    nError("there is no Layer at this index in the Network.", __func__);
    return ILLEGAL_INDEX;
  }
  
  return (nLayer != net->nLayers)
              ? (net->layers[nLayer-1].nNeurons - 1) 
              : (net->layers[nLayer-1].nNeurons); 
}

int nOutputVector(Network * net, double output[]) {
  if (!net) {
    nError("nothing to return. Network given is NULL.", __func__);
    return NONEXISTING_NETWORK;
  }
  
  /*
  int size = sizeof(output) / sizeof(double);
  if (size == 0) {
    nError("array given cannot be filled. Array has to have allocated memory.", __func__);
    return MEM_ALLOCATION_ERROR;
  }
   */
  int nNeurons = net->layers[net->nLayers-1].nNeurons;
  
  //bool shouldWarn = false;
  /*
  if (size > nNeurons) {
    nWarn("the number of elements in the given array exceeds the number of output elements.", __func__);
    shouldWarn = true;
  }
  else if (size < nNeurons) {
    nWarn("the number of elements in the given array is less than the number of output elements.", __func__);
    shouldWarn = true;
  }
   */
  
  for (int i = 0; i < nNeurons /*&& i < size*/; i++)
    output[i] = net->layers[net->nLayers-1].neurons[i];
  /*
  if (shouldWarn)
    return NONMATCHING_LENGTH;
   */
  
  return OPERATION_SUCCESS;
}

int nOutputClass(Network * net) {
  //printf("nOutputClass was entered..\n");
  if (!net) {
    nError("nothing to return. Network given is NULL.", __func__);
    return NONEXISTING_NETWORK;
  }
  
  ClassIdentifier ci;
  //printf("calling commonMax()..\n");
  commonMax(net->layers[net->nLayers-1].neurons, 
          net->layers[net->nLayers-1].nNeurons,
          &ci);
  //printf("nOutputClass is returning..\n");
  return net->classes[ci.position];
}

int nAttachBPTrainer(Network * net, BPTrainer * trainer) {
  if (!net) {
    nError("trainer was not attached. Network given is NULL.", __func__);
    return NONEXISTING_NETWORK;
  }
  
  if (!trainer) {
    nWarn("trainer to attach is NULL.", __func__);
  }
  
  net->trainer = trainer;
  return OPERATION_SUCCESS;
}
/*
Weight * nWeightVector(Network * net, BPTrainer * trainer, size_t index) {
  if (!net) {
    nError("nothing to be returned. Network given is NULL.", __func__);
    return (Weight *)NULL;
  }
  
  if (!trainer) {
    nError("nothing to be returned. BPTrainer given is NULL.", __func__);
    return (Weight *)NULL;
  }
  
  if (net != trainer->net) {
    nError("nothing to be returned. Network mismatch.", __func__);
    return (Weight *)NULL;
  }
  
  if (index >= net->nLayers || index == 0) {
    nError("nothing to be returned. Weight index does not exist.", __func__);
    return (Weight *)NULL;
  }
  
  return &(net->weights[index]);
}
 */

double nMSE (Network * net, double * targets) {
  
  /*
   * This funcion begs for errors because targets length can not
   * be known to the function. This due to C's handling for pointers
   * and array. 
   * The solution for this problem herein as well as in other funcions
   * will be to use a new data structure similar to C++ vector type 
   * instead of plain arrays or double pointers.
   */
  
  if (!net) {
    nError ("nothing to be returned. Network given is NULL.", __func__);
    return NAN;
  }
  
  double sum = 0.0,
         d;
  register size_t i;
  size_t  layer = net->nLayers -1;
  Layer   * outLayer = &net->layers[layer]; 
  size_t  nNeurons = outLayer->nNeurons;
  double  * outneurons = outLayer->neurons; 
  
  for (i = 0; i < nNeurons; i++) {
    d = targets[i] - outneurons[i];
    sum += d * d;
  }
  
  return sum / nNeurons;
}
  
  int nOGradient(
          Network * net,
          double  * targets,        // nout vector of output target activations
          double  * deltas,         // nout vector of computed deltas
          double  ** gradient       // nout vect of ptrs to nprev+1 neg grads
        ) {
    
    /*
     * Again, pointers' length are potential problems. This necessiates a C++
     * vector-like type implemenation.
     */
    
    if (!net) {
      nError("gradient could not be computed. Network given is NULL", __func__);
      return NONEXISTING_NETWORK;
    }
    
    register size_t i, j;
    double delta, *gradientptr;
    
    size_t  nlayers = net->nLayers;
    Layer   * layers = net->layers;
    size_t  nout = layers[nlayers-1].nNeurons;
    size_t  nprev = layers[nlayers-2].nNeurons - 1;   // Don't modify the bias
    double  * outact = layers[nlayers - 1].neurons;
    double  * prevact = layers[nlayers - 2].neurons; 
    
    for (i = 0; i < nout; i++) {
      delta = (targets[i] - outact[i]) * lsDerivative(outact[i]);
      deltas[i] = delta;
      gradientptr = gradient[i];
      for (j = 0; j < nprev; j++) 
        gradientptr[j] += delta * prevact[j];
      gradientptr[nprev] += delta; // Bias activation is always 1
    }    
    
    return OPERATION_SUCCESS;
  }
  
  int nHGradient(
          Network * net,
          size_t  layer,
          bool    shouldsavedelta,
          double  * deltas,           // ncurr vector of computed deltas
          double  * nextdeltas,       // nnext vector of next layer deltas
          double  ** gradient         // ncurr vect of ptrs to nprev+1 neg grads
        ) {
    /*
     * Again, pointers' length are potential problems. This necessiates a C++
     * vector-like type implemenation. 
     */
    
    if (!net) {
      nError("gradient could not be computed. Network given is NULL.", __func__);
      return NONEXISTING_NETWORK;
    }
    
    size_t nlayer = net->nLayers;
    
    if (layer > nlayer - 2 || layer == 0) {
      nError("gradient could not be computed. Illegal layer index", __func__);
      return ILLEGAL_INDEX;
    }
    
    /*
     * Saving work pointers to speed up execution. The time taken to initialise
     * these variables should be fractional to the time consumed by accessing
     * pointers from the network with numerous neurons.
     */
    
    Layer   * layers = net->layers;
    size_t  ncurr = layers[layer].nNeurons - 1;
    size_t  nnext = layers[layer + 1].nNeurons;
    size_t  nprev = layers[layer - 1].nNeurons - 1;     // Don't modify the bias
    double  * prevact = layers[layer - 1].neurons;
    double  * curract = layers[layer].neurons;
    double  ** nextcoefs = net->weights[layer].vectors;   // weight struct between next & current layers
    
    register size_t i, j;
    double sum, delta, *gradientptr;
    
    for (i = 0; i < ncurr; i++) {
      sum = 0.0;
      for (j = 0; j < nnext; j++)
        sum += nextdeltas[j] * nextcoefs[j][i];
      delta = sum * lsDerivative(curract[i]);
      if (shouldsavedelta)
        deltas[i] = delta;
      gradientptr = gradient[i];
      for (j = 0; j < nprev; j++)
        gradientptr[j] += delta * prevact[j];
      gradientptr[nprev] += delta;    // Bias activation is always 1
    }    
    
    return OPERATION_SUCCESS;
  }
  
  int nClass(Network * net, size_t index) {
    /*
     * This functions needs index checking to allow for graceful failure.
     */
    if(!net) {
       nError("classes were not set. Network given is NULL.", __func__);
       return NONEXISTING_NETWORK;
    }
    return net->classes[index];
  }
  
  int nAreClassesSet(Network * net) {
    if (!net) {
      nError("nothing to be returned. Network given is NULL.", __func__);
      return NONEXISTING_NETWORK;
    }
    
    return (net->isClassesSet);
  }
  
  int nSetClasses(Network * net, const char classes[]) {
    if (!net) {
      nError("classes were not set. Network given is NULL.", __func__);
      return NONEXISTING_NETWORK;
    }
    
    size_t count = net->layers[net->nLayers -1].nNeurons;
    
    for (size_t i = 0; i < count; i++)
      net->classes[i] = classes[i];
    
    net->isClassesSet = true;
    
    return OPERATION_SUCCESS;
  }
  
  int nGetClasses(Network * net, char classes[]) {
    if (!net) {
      nError("classes were not set. Network given is NULL.", __func__);
      return NONEXISTING_NETWORK;
    }
    
    if (!net->isClassesSet)
      return CLASSES_NOT_SET;
    
    size_t count = net->layers[net->nLayers -1].nNeurons;
    
    for (size_t i = 0; i < count; i++)
      classes[i] = net->classes[i];
    
    return OPERATION_SUCCESS;
  }
  
  int nClassActVector(Network * net, char ch, double classVector[]) {
    if (!net) {
      nError("classes were not set. Network given is NULL.", __func__);
      return NONEXISTING_NETWORK;
    }
    
    if (!net->isClassesSet)
      return CLASSES_NOT_SET;
    
    size_t count = net->layers[net->nLayers -1].nNeurons;
        
    for (size_t i = 0; i < count; i++)
      if (ch == net->classes[i]) {
        for (size_t j = 0; j < count; j++)
          classVector[j] = OFF;
        classVector[i] = ON;
        return OPERATION_SUCCESS;
      }
    
    return OPERATION_FAILURE;
  }
  
  int nNumberOfClasses(Network * net) {
    if (!net) {
      nError("classes were not set. Network given is NULL.", __func__);
      return NONEXISTING_NETWORK;
    }
    
    if (net->isClassesSet)
      return net->layers[net->nLayers - 1].nNeurons;

    return CLASSES_NOT_SET;
  }
  
  /*
   * ALL OF THE FOLLOWING FUNCTIONS NEED TO BE REVISED. MEMORY CHECKS SHOULD
   * BE APPENDED.
   */
  
int nSave(Network * net, const char * path) {
  if (!net) {
    nError("network was not saved. Network given is NULL.", __func__);
    return NONEXISTING_NETWORK;
  }
  
  char ext[] = ".net";
  char realpath [strlen(path) + 5];
  realpath[0] = '\0';
  strcat(realpath, path);
  strcat(realpath, ext);
  
  FILE * file = fopen(realpath, "wb");
  if (!file) {
    nError("network was not saved. Could not write to file.", __func__);
    return CANNOT_WRITE_FILE;
  }
    
  /*
  * Number of written bytes should be checked;
  */
    
  register size_t i;
  int opreturn;
  uint32_t signature = NETWORK_SIGNATURE;
  size_t  nlayers = net->nLayers;
  Layer   * layers = net->layers;
  Weight  * weights = net->weights;
  
  opreturn = fwrite(&signature, sizeof(uint32_t), 1, file);
  if (opreturn != 1) {
    nError("network was not saved. Writing error.", __func__);
    fclose(file);
    return CANNOT_WRITE_FILE;
  }
       
  fwrite(&(net->nLayers), sizeof(size_t), 1, file);
  fwrite(&(net->isClassesSet), sizeof(bool), 1, file);     
  for (i = 0; i < nlayers; i++) {
    fwrite(&(net->layers[i].nNeurons), sizeof(size_t), 1, file);
    fwrite(net->layers[i].neurons, sizeof(double), net->layers[i].nNeurons, file);
  }    
  for (i = 0; i < nlayers - 1; i++) {
  //fwrite(&(net->weights[i]), sizeof(Weight), 1, file);
  fwrite(&(net->weights[i].rows), sizeof(size_t), 1, file);
  fwrite(&(net->weights[i].columns), sizeof(size_t), 1, file);
  for(size_t j = 0; j < net->weights[i].rows; j++) 
    fwrite(net->weights[i].vectors[j], sizeof(double), net->weights[i].columns, file);
  }    
  fwrite(net->classes, sizeof(char), net->layers[nlayers - 1].nNeurons, file);
    
  fclose(file); 
  return OPERATION_SUCCESS;    
}
  
int nSaveAs(Network * net, const char * path) { 

}
  
Network * nLoad(const char * path) {    
  FILE * file = fopen(path, "rb");
  if (!file) {
    nError("network was not loaded. Could not read from file.", __func__);
    return (Network *)NULL;
  }
    
  Network * net = (Network *)calloc(1, sizeof(Network));
  if(!net) {
    nError("Network was not loaded. No enough memory.", __func__);
    fclose(file);
    return (Network *)NULL;
  }
    
  register size_t i, j, k;
  int opreturn;
  uint32_t signature;
  
  opreturn = fread(&signature, sizeof(uint32_t), 1, file);
  if (opreturn != 1) {
    nError("network was not loaded. Reading error.", __func__);
    fclose(file);
    free(net);
    return (Network *)NULL;
  }
  
  if (signature != NETWORK_SIGNATURE) {
    nError("network was not loaded. Unrecognised file format.", __func__);
    return (Network *)NULL;
  }
    
  opreturn = fread(&(net->nLayers), sizeof(size_t), 1, file);
  if (opreturn != 1) {
    nError("Network was not loaded. Reading error.", __func__);
    free(net);
    fclose(file);
    return (Network *)NULL;
  } 
  opreturn = fread(&(net->isClassesSet), sizeof(bool), 1, file);
  if (opreturn != 1) {
    nError("Network was not loaded. Reading error.", __func__);
    free(net);
    fclose(file);
    return (Network *)NULL;
  }
  
  size_t nlayers = net->nLayers;
    
  /*
  * Reading layers
  */
  net->layers = (Layer *)malloc(nlayers * sizeof(Layer));
  if(!net->layers) {
    nError("Network was not loaded. No enough memory.", __func__);
    free(net);
    fclose(file);
    return (Network *)NULL;
  }
  for(i = 0; i < nlayers; i++) {
    opreturn = fread(&(net->layers[i].nNeurons), sizeof(size_t), 1, file);
    if (opreturn != 1) {
      nError("Network was not loaded. Reading error.", __func__);
      if (i != 0)
        for (j = --i; j >= 0; j--)
          free(net->layers[j].neurons);
      free(net->layers);
      free(net);
      fclose(file);
      return (Network *)NULL;
    }
    net->layers[i].neurons = (double *)malloc(net->layers[i].nNeurons * sizeof(double));
    if (!net->layers[i].neurons) {
      nError("Network was not loaded. No enough memory.", __func__);
      if (i != 0)
        for (j = --i; j >= 0; j--)
          free(net->layers[j].neurons);
      free(net->layers);
      free(net);
      fclose(file);
      return (Network *)NULL;
    }
    opreturn = fread(net->layers[i].neurons, sizeof(double), net->layers[i].nNeurons, file);
    if (opreturn != net->layers[i].nNeurons) {
      nError("Network was not loaded. Reading error.", __func__);
      for (j = i; j >= 0; j--)
        free(net->layers[j].neurons);
      free(net->layers);
      free(net);
      fclose(file);
      return (Network *)NULL;
    }
  }
    
  /*
  * Reading weights
  */
    
  net->weights = (Weight *)malloc((nlayers - 1) * sizeof(Weight));
  if(!net->weights) {
    nError("Network was not loaded. No enough memory.", __func__);
    nPrivateDeleteLayers(net, nlayers);
    free(net);
    fclose(file);
    return (Network *)NULL;
  }
  for(i = 0; i < nlayers - 1; i++) {
    opreturn = fread(&(net->weights[i].rows), sizeof(size_t), 1, file);
    if (opreturn != 1) {
      nError("Network was not loaded. Reading error.", __func__);
      //for (j = --i; j >= 0; j--) {
        //for (k = 0; k < net->weights[j].columns; k++)
          //free(net->weights[j].vectors[k]);
        //free(net->weights[j].vectors);
      //}
      nPrivateDeleteLayers(net, net->nLayers);
      //free(net->weights);
      nPrivateDeleteWeights(net, i, 0);
      free(net);
      fclose(file);
      return (Network *)NULL;
    }
    opreturn = fread(&(net->weights[i].columns), sizeof(size_t), 1, file);
    if (opreturn != 1) {
      nError("Network was not loaded. Reading error.", __func__);
      nPrivateDeleteLayers(net, net->nLayers);
      nPrivateDeleteWeights(net, i, 0);
      free(net);
      fclose(file);
      return (Network *)NULL;
    }
    net->weights[i].vectors = (double **)malloc(net->weights[i].rows * sizeof(double *));
    if (!net->weights[i].vectors) {
      nError("Network was not loaded. No enough memory.", __func__);
      nPrivateDeleteLayers(net, net->nLayers);
      nPrivateDeleteWeights(net, i, 0);
      free(net);
      fclose(file);
      return (Network *)NULL;
    }      
    for (j = 0; j < net->weights[i].rows; j++) {
      net->weights[i].vectors[j] = (double *)malloc(net->weights[i].columns * sizeof(double));
      if (!net->weights[i].vectors[j]) {
        nError("Network was not loaded. No enough memory.", __func__);
        nPrivateDeleteLayers(net, net->nLayers);
        nPrivateDeleteWeights(net, i, j);
        free(net);
        fclose(file);
        return (Network *)NULL;
      }        
      opreturn = fread(net->weights[i].vectors[j], sizeof(double), net->weights[i].columns, file);
      if (opreturn != net->weights[i].columns) {
        nError("Network was not loaded. Reading error.", __func__);
        //for (j = i; j >= 0; j--) {
          //for (k = 0; k < net->weights[j].columns; k++)
            //free(net->weights[j].vectors[k]);
          //free(net->weights[j].vectors);
        //}
        nPrivateDeleteLayers(net, net->nLayers);
        //free(net->weights);
        nPrivateDeleteWeights(net, i, j+1);
        free(net);
        fclose(file);
        return (Network *)NULL;
      }
    }      
  }   
    
  /*
   * Reading classes
   */
  net->classes = (char *)malloc(net->layers[nlayers-1].nNeurons * sizeof(char));
  if(!net->classes) {
    nError("Network was not loaded. No enough memory.", __func__);
    nPrivateDeleteLayers(net, net->nLayers);
    nPrivateDeleteWeights(net, net->nLayers-1, 0);
    free(net);
    fclose(file);
    return (Network *)NULL;
  }
  opreturn = fread(net->classes, sizeof(char), net->layers[nlayers-1].nNeurons, file);
  if (opreturn != net->layers[nlayers - 1].nNeurons) {
    nError("Network was not loaded. Reading error.", __func__);
    nPrivateDeleteLayers(net, net->nLayers);
    nPrivateDeleteWeights(net, net->nLayers-1, 0);
    free(net);
    fclose(file);
    return (Network *)NULL;
  }    
  
  fclose(file);
  lsInit(100, 10.0);
  return net;
}
