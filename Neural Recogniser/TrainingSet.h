/**
 * Copyright 2008 MentorLycon, LLC.
 * The author of this software is Kareem Alkaseer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _TRAININGSET_H
#define	_TRAININGSET_H

#ifdef	__cplusplus
extern "C" {
#endif
  
  #include "stdlib.h"
          
  #define NONEXISTING_SET           -11
  #define NONMATCHING_LIST_LENGTH   -12
          
          
  typedef struct trainingset_t TrainingSet;
  
  
  TrainingSet * tsTrainingSet (size_t TVLength, size_t OVLength);  
  int tsDelete (TrainingSet ** set);  
  int tsTVectorLength (TrainingSet * set);  
  int tsOVectorLength (TrainingSet * set);
  double * tsTVector (TrainingSet * set);
  int tsSetTVector (TrainingSet * set, double inputs[]);
  double * tsOVector (TrainingSet * set);  
  int tsSetOVector (TrainingSet * set, double outputs[]);  
  void tsShallowCopy (TrainingSet * source, TrainingSet ** copy);  
  TrainingSet * tsDeepCopy (TrainingSet * set);  
  int tsSwap (TrainingSet ** set1, TrainingSet ** set2);  
  bool tsIsEqual (TrainingSet * set1, TrainingSet * set2);  
  size_t tsSizeOf (void);  
  int tsMemConsumed (TrainingSet * set);  
  int tsFileInit (TrainingSet * set, const char * path);  
  int tsSave (TrainingSet * set, const char * path);  
  static TrainingSet * tsLoad(const char * path);  
  
  static size_t tsSaveList
      (
          size_t length,
          TrainingSet * sets[length],
          const char  * path,
          int         * feedback
      );
  
  static size_t tsSaveVector
      (
          size_t length,
          TrainingSet * sets[length],
          const char  * path,
          int         * feedback
      );
  
  static size_t tsLoadList
      (
          size_t length,
          TrainingSet * sets[length],
          const char  * path,
          int         * feedback
      );
  
  static size_t tsAppendToList
      (
          size_t length,
          TrainingSet * sets[length],
          const char  * path,
          int         * feedback
      );
  
  /*
   * Reterieval of and setting a single TVector or OVector element by index was
   * not implemented due to the lack of an exception handling mechanism.
   * If a caller specifies an index that does not exist, it can be caught
   * by the function implementing this behaviour. However, no sentinal
   * value can be used because these elements can take any value.
   * Hence, two solutions are possible: 1) implementing an error handling
   * mechanism that is symbolic constants-oriented or 2) an exception 
   * handling mechanism. Because one of the future specificaion of this
   * project is to have an exception handling, the function was not 
   * implemented using the first method and its implementation will be
   * delayed until exception handling is implemented first.
   */


#ifdef	__cplusplus
}
#endif

#endif	/* _TRAININGSET_H */

