/**
 * Copyright 2008 MentorLycon, LLC.
 * The author of this software is Kareem Alkaseer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _LOGISTICSIGMOID_H
#define	_LOGISTICSIGMOID_H

#ifdef	__cplusplus
extern "C" {
#endif
  
  #include <stdbool.h>

  int lsInit(int len, double max);
  double lsSigmoid(double x);
  bool lsIsInitialised(void);
  inline double lsDerivative (double activation) __attribute__((always_inline));


#ifdef	__cplusplus
}
#endif

#endif	/* _LOGISTICSIGMOID_H */

