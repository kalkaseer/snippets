/**
 * Copyright 2008 MentorLycon, LLC.
 * The author of this software is Kareem Alkaseer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdlib.h>
#include <math.h>
#include "Backprop.h"
#include "Commons.h"
#include "GlobalConstants.h"

#define _REVEAL_
#include "SharedDefs.h"

#define WITH_SETS     1
#define WITHOUT_SETS  2

struct bptrainer_t {
  size_t      nSets;
  int         initialiser;
  Network     * net;
  TrainingSet ** sets;
};

BPTrainer * bpTrainerWithSets(Network * net, size_t nSets) {
  if (!net) {
    nError("training was not created. The Network given is NULL.", __func__);
    return (BPTrainer *)NULL;
  }
  
  register size_t i, j;
  size_t tvlength = nNumberOfNeurons(net, 1);
  size_t ovlength = nNumberOfNeurons(net, nNumberOfLayers(net));
  const char * s = "training was not created. No enough memory.";
  
  BPTrainer * trainer = (BPTrainer *)malloc(sizeof(BPTrainer));
  if (!trainer) {
    nError(s, __func__);
    return (BPTrainer *)NULL;
  }
  
  trainer->sets = (TrainingSet **)malloc(nSets * sizeof(TrainingSet *));
  if(!(trainer->sets)) {
    nError(s, __func__);
    free(trainer);
    return (BPTrainer *)NULL;
  }
  
  for (i = 0; i < nSets; i++) {
    trainer->sets[i] = tsTrainingSet(tvlength, ovlength);
    if (!trainer->sets[i]) {
      nError(s, __func__);
      if (i != 0) 
        for (j = --i; j >= 0; j--)
          tsDelete(&(trainer->sets[j]));
      free(trainer->sets);
      free(trainer);
      return (BPTrainer *)NULL;
    }
  }
  
  trainer->nSets = nSets;
  trainer->initialiser = WITH_SETS;
  trainer->net = net;
  
  return trainer;
}

BPTrainer * bpTrainerWithoutSets(Network * net, size_t size, TrainingSet * sets[size]) {
  if (!net) {
    nError("trainer was not created. The Network given is NULL.", __func__);
    return (BPTrainer *)NULL;
  }
  
  size_t tvlength = nNumberOfNeurons(net, 1);
  size_t ovlength = nNumberOfNeurons(net, nNumberOfLayers(net));
  const char * s = "trainer was not created. No enough memory.";
  
  BPTrainer * trainer = (BPTrainer *)malloc(sizeof(BPTrainer));
  if (!trainer) {
    nError(s, __func__);
    return (BPTrainer *)NULL;
  }
  
  trainer->sets = (TrainingSet **)malloc(size * sizeof(TrainingSet *));
  if(!(trainer->sets)) {
    nError(s, __func__);
    free(trainer);
    return (BPTrainer *)NULL;
  }
  
  trainer->net = net;
  
  size_t n = 0;  
  for (n; n < size; n++) 
    if  (
          (tvlength == tsTVectorLength(sets[n])) &&
          (ovlength == tsOVectorLength(sets[n]))
        )
      trainer->sets[n] = sets[n];
  
  trainer->nSets = n;
  trainer->initialiser = WITHOUT_SETS;
  nAttachBPTrainer(net, trainer);
  return trainer;
}

int bpDelete(BPTrainer * trainer) {
  if (!trainer) {
    nError("BPTrainer was not deleted. The BPTrainer given is NULL.", __func__);
    return NONEXISTING_BPTRAINTER;
  }
  
  if (trainer->initialiser == WITH_SETS)
    for (int i = 0; i < trainer->nSets; i++)
      free(trainer->sets[i]);
  free(trainer->sets);
  free(trainer);
  
  return OPERATION_SUCCESS;
}
        
int bpNumberOfSets(BPTrainer * trainer) {
  if (!trainer) {
    nError("nothing to be returned. BPTrainer given is NULL.", __func__);
    return NONEXISTING_BPTRAINTER;
  }
  
  return trainer->nSets;
}

Network * bpNetwork(BPTrainer * trainer) {
  if (!trainer) {
    nError("nothing to be returned. BPTrainer given is NULL.", __func__);
    return (Network *)NULL;
  }
  
  return trainer->net;
}


BPSet * bpSet(Network * net) {
  if (!net) {
    nError("network given is NULL.", __func__);
    return (BPSet *)NULL;
  }
  
  BPSet * set = (BPSet *)malloc(sizeof(BPSet));
  if (!set) {
    nError("BPSet was not created. No enough memory.", __func__);
    return (BPSet *)NULL;
  }
  
  set->tVectorLength = nNumberOfNeurons(net, 1);
  set->tVector = (double *)calloc(set->tVectorLength, sizeof(double));
  if (!set->tVector) {
    free(set);
    nError("BPSet was not created. No enough memory.", __func__);
    return (BPSet *)NULL;
  }
  
  set->net = net;
  set->output = NAN;
  
  return set;
}

int bpSetDelete(BPSet ** set) {
  if (!(*set)) {
    nError("BPSet was not deleted. BPSet given is NULL.", __func__);
    return NONEXISTING_BPSET;
  }
  
  free((*set)->tVector);
  free((*set));
  
  (*set) = NULL;
  
  return OPERATION_SUCCESS;
}

BPTrainer2 * bpTrainer2(Network * net, size_t size) {
  if (!net) {
    nError("nothing to be returned. Network given is NULL.", __func__);
    return (BPTrainer2 *)NULL;
  }
  
  BPTrainer2 * trainer = (BPTrainer2 *)malloc(sizeof(BPTrainer2));
  if (!trainer) {
    nError("trainer was not created. No enough memory.", __func__);
    return (BPTrainer2 *)NULL;
  }
  
  trainer->sets = (BPSet **)calloc(size, sizeof(BPSet *));
  if (!trainer->sets) {
    nError("trainer was not created. No enough memory.", __func__);
    free(trainer);
    return (BPTrainer2 *)NULL;
  }
  
  trainer->nSets = size;
  trainer->net = net;
  
  return trainer;
}

int bpDelete2(BPTrainer2 ** trainer) {
  if (!trainer) {
    nError("trainer was not deleted. BPTrainer2 given is NULL.", __func__);
    return NONEXISTING_BPTRAINTER;
  }
  
  free((*trainer)->sets);
  free((*trainer));
  
  return OPERATION_SUCCESS;
}

double bpTrain3(
          BPTrainer * trainer,
          size_t    maxiter,    // Maximum iterations allows
          double    rate,       // Learning rate, gradient multiplier
          double    mom,        // Momentum coefficient
          double    errtol,     // Quit if error drops this low
          double    gradtol     // Quit if all gradients this small
        ) {
  /*
   * This function has parts hard-coded for a three-layer network. This is
   * the case with how nhid and nin were obtained. The problem also applies
   * to memory allocation for work vector as well as to how weight vectors 
   * were obtained.This has to be fixed in the future.
   * Amendment needs generalisation of the backpropagation algorithm for 
   * networks that have more than three layers and substituting
   * hard-coded parts with layer-number-independent code.
   */
  
  if (!trainer) {
    nError("training can not proceed. BPTrainer given is NULL.", __func__);
    return NAN;
  }
  
  register size_t i, j, pnum, iter;
  Network * net = trainer->net;
  double  * outdelta,
          ** outgrad,
          ** hidgrad,
          ** outprev,     // output previous corrections
          ** hidprev,     // hidden previous corrections
          /* * ins, */    // TrainingSet's tVector will be used instead
          /* * targ, */   // TrainingSet's oVector will be used instead
          * cptr,
          * gptr,
          * pptr,
          ** outcoefs = net->weights[1].vectors,
          ** hidcoefs = net->weights[0].vectors,
          corr,
          error,
          maxgrad;
  
  //size_t  ntrain = trainer->nSets;  // not used, equal to trainer->nSets
  size_t  nlayers = net->nLayers;
  Layer   * layers = net->layers;
  Layer   * outlayer = &layers[nlayers - 1];
  size_t  nout = outlayer->nNeurons;
  size_t  nhid = layers[1].nNeurons;
  size_t  nin = layers[0].nNeurons;
  
  /* 
   * Note that no checks were made to ensure that memory was really
   * allocated. This still needs to be done because a memory fault
   * at this point will surely results in a crash. Free at exiting.
   */
  
  outdelta = (double *)calloc(nout, sizeof(double));
  outgrad = (double **)malloc(nout * sizeof(double *));
  for (i = 0; i < nout; i++)
    outgrad[i] = (double *)calloc(nhid, sizeof(double));
  hidgrad = (double **)malloc(nhid * sizeof(double *));
  for (i = 0; i < nhid; i++)
    hidgrad[i] = (double *)calloc(nin, sizeof(double));
  outprev = (double **)malloc(nout * sizeof(double *));
  for (i = 0; i < nout; i++)
    outprev[i] = (double *)calloc(nhid, sizeof(double));
  hidprev = (double **)malloc(nhid * sizeof(double *));
  for (i = 0; i < nhid; i++)
    hidprev[i] = (double *)calloc(nin, sizeof(double));
  
  /* 
   * Previous iteration corrections were zeroed in the memory
   * allocation part. No need to zero them out again.
   */
  
  for (iter = 0; iter < maxiter; iter++) { // Each iter is an epoch
    error = bpFindGradient(trainer, outdelta, outgrad, hidgrad, nout, nhid, nin);
    
    if (error <= errtol) {
      printf("error dropped...exiting\n");
      break;
    }
    
    maxgrad = 0.0;
    for (i = 0; i < nout; i++) {
      cptr = outcoefs[i];
      gptr = outgrad[i];
      pptr = outprev[i];
      for (j = 0; j < nhid; j++) {
        if (fabs(gptr[j]) > maxgrad)            // Keep track of gradient size
          maxgrad = fabs(gptr[j]);              // For convergence test later
        corr = rate * gptr[j] + mom * pptr[j];  // Compute correction
        cptr[j] += corr;                        // Update wiehgt per correction
        pptr[j] = corr;                         // Save it for momentum in next iteration
      }
    }
    
    for (i = 0; i < nhid; i++) {
      cptr = hidcoefs[i];
      gptr = hidgrad[i];
      pptr = hidprev[i];
      for (j = 0; j < nin; j++) {
        if (fabs(gptr[j]) > maxgrad)
          maxgrad = fabs(gptr[j]);
        corr = rate * gptr[j] + mom * pptr[j];
        cptr[j] += corr;
        pptr[j] = corr;
      }
    }
    
    /*
     * There is a problem with the gradient threshold. max gradient
     * drops to gradtol before learning is done.
     */
    
    //if (maxgrad <= gradtol) {
      //printf("gradient dropped...exiting\n");
      //break;
    //}
  }
  
  printf("iterations: %lu\n", iter);
  return error;
}

double bpFindGradient(
        BPTrainer * trainer,
        double  *  outdelta,
        double  ** outgrad,
        double  ** hidgrad,
        size_t  nout,
        size_t  nhid,
        size_t  nin
      ) {
  /* 
   * Checking for trainer is not necessary because this method should be called 
   * from inside other function which check for the trainer supplied.
   */
  
  register size_t i, j, pnum;
  double error = 0.0, *gptr;
  
  TrainingSet * set;
  size_t  ntrain = trainer->nSets;
  Network * net = trainer->net;
  double  * ins,
          * targs;
  
  for (i = 0; i < nout; i++) {      // Zero output gradient in preparation
    gptr = outgrad[i];              // for summing across this epoch
    for (j = 0; j < nhid; j++)
      gptr[j] = 0.0;
  }
  
  for (i = 0; i < nhid; i++) {      // Similary, zero hidden gradien
    gptr = hidgrad[i];
    for (j = 0; j < nin; j++)
      gptr[j] = 0.0;
  }
  
  for (pnum = 0; pnum < ntrain; pnum++) {
    set = trainer->sets[pnum];
    ins = tsTVector(set);
    targs = tsOVector(set);
    nFeedforward(net, ins);
    nOGradient(net, targs, outdelta, outgrad);
    nHGradient(net, 1, false, (double *)NULL, outdelta, hidgrad);
    error += nMSE(net, targs);
  }
  return error / (double) ntrain;
}
