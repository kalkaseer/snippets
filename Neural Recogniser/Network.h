/**
 * Copyright 2008 MentorLycon, LLC.
 * The author of this software is Kareem Alkaseer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _NETWORK_H
#define	_NETWORK_H

#ifdef	__cplusplus
extern "C" {
#endif
          
  #include <stdbool.h>
  #include <stdlib.h>
  #include "GlobalTypes.h"
          
  #define NONEXISTING_NETWORK -18
  #define NONMATCHING_LENGTH  -19
  #define CLASSES_NOT_SET     -20
  
  typedef struct network_t Network;
  
  Network * nNetwork(size_t nLayers, ...);
  int nDelete(Network ** net); 
  int nReset(Network * net);
  int nFeedforward(Network * net, double inputVec[]);
  // The following function needs to be substituted by a function that returns
  // layer's activation. This function is more of a debugging function.
  int nPrintLayer(Network * net, size_t layer, bool bias);
  int nNumberOfLayers(Network * net);
  int nNumberOfNeurons(Network * net, size_t nLayer);
  // this function begs for error. the size of the array must somehow be known to the function.
  // This amendment would allow graceful error recovery.
   
  // this function begs for error. the size of the array must somehow be known to the function.
  // This amendment would allow graceful error recovery.
  int nOutputVector(Network * net, double output[]);
  int nOutputClass(Network * net);
  int nAttachBPTrainer(Network * net, BPTrainer * trainer);
  int nClass(Network * net, size_t index);
  
  double nMSE (Network * net, double * targets);
  
  int nOGradient(
          Network * net,
          double  * targets,
          double  * deltas,
          double  ** gradient
        );  // don't call using 2D array as the gradient.
  
  int nHGradient(
          Network * net,
          size_t  layer,
          bool    shouldsavedelta,
          double  * deltas,
          double  * nextdeltas,
          double  ** gradient
        ); // don't call using 2D array as the gradient.
  
  // The following 3 funcions are prone to error because array size must
  // match the number of neurons in the output layer.
  int nSetClasses(Network * net, const char classes[]);
  int nGetClasses(Network * net, char classes[]);
  int nClassActVector(Network * net, char ch, double classVector[]);
  int nAreClassesSet(Network * net);
  int nNumberOfClasses(Network * net);
  
  int nSave(Network * net, const char * path);
  int nSaveAs(Network * net, const char * path);
  Network * nLoad(const char * path);
  

#ifdef	__cplusplus
}
#endif

#endif	/* _NETWORK_H */

