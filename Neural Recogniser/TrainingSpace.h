/**
 * Copyright 2008 MentorLycon, LLC.
 * The author of this software is Kareem Alkaseer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef _TRAININGSPACE_H
#define	_TRAININGSPACE_H

#ifdef	__cplusplus
extern "C" {
#endif

  #include "TrainingSet.h"
  #include "stdlib.h"
          
  #define PLAIN_ADDITION      1
  #define TINY_ADDITION       40
  #define VERY_SMALL_ADDITION 60
  #define SMALL_ADDITION      80
  #define MEDIUM_ADDITION     100
  #define LARGE_ADDITION      500
  #define VERY_LARGE_ADDITION 2500
  #define SEMIHUGE_ADDITION   15000
  #define HUGE_ADDITION       100000
  #define DEFAULT_ADDITION    TINY_ADDITION
          
  #define NONEXISTING_SPACE     -13
  #define EMPTY_SPACE           -14
  #define ILLEGAL_CAPACITY      -15
  #define SET_IS_NOT_CONTAINED  -16
  #define NULL_CURRENT          -17
  
  typedef struct trainingspace_t TrainingSpace;
  
  /*
   * This class needs reconsideration. Is being a linked list necessary? I think
   * it can do fine with just a dynmically allocated array.
   */
  
  TrainingSpace * tspTrainingSpace (size_t TVLength, size_t OVLength);
  TrainingSpace * tspTrainingSpaceWCap (size_t TVLength, size_t OVLength, size_t capacity);  
  TrainingSpace * tspTrainingSpaceWSets (size_t TVLength, size_t OVLength, size_t size);    
  int tspDelete (TrainingSpace ** space);
  int tspAddSet (TrainingSpace * space, TrainingSet * set);
  int tspAddSetWCap (TrainingSpace * space, TrainingSet * set, size_t multiplier);
  int tspInsertSet (TrainingSpace * space, TrainingSet * set, size_t index);  
  int tspRemoveSet (TrainingSpace * space);    
  int tspIDeleteSet (TrainingSpace * space, size_t index);
  int tspSDeleteSet (TrainingSpace * space, TrainingSet * set);
  TrainingSet * tspFirst (TrainingSpace * space);
  TrainingSet * tspLast (TrainingSpace * space);
  int tspIndex(TrainingSpace * space, TrainingSet * set);
  int tspCurrent(TrainingSpace * space);
  int tspSetCurrent(TrainingSpace * space, size_t index);
  TrainingSet * tspSet (TrainingSpace * space, size_t index);
  TrainingSet * tspNext (TrainingSpace * space);
  TrainingSet * tspPrevious (TrainingSpace * space);  // Needs to be recoded after tspNext
  int tspSize (TrainingSpace * space);
  int tspCapacity (TrainingSpace * space);
  int tspSetCapacity (TrainingSpace * space, size_t capacity); 
  TrainingSet * tspNull (TrainingSpace * space);  
  int tspContainsNull (TrainingSpace * space);  
  int tspDeleteNulls (TrainingSpace * space);
  int tspTrim (TrainingSpace ** space);
  size_t tspSizeOf(void);  
  int tspMemConsumed(TrainingSpace * space);
  int tspMemConsumedWSets(TrainingSpace * space);

#ifdef	__cplusplus
}
#endif

#endif	/* _TRAININGSPACE_H */

