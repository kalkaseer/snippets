/**
 * Copyright 2008 MentorLycon, LLC.
 * The author of this software is Kareem Alkaseer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _COMMONS_H
#define	_COMMONS_H

#ifdef	__cplusplus
extern "C" {
#endif
  
  #include <stdio.h>
  #include "GlobalTypes.h"
          
  // nError () and nWarn()
  //----------------------------------------------
  inline void nError  (const char * s, const char * funcName) __attribute__((always_inline));
  inline void nWarn(const char * s, const char * funcName) __attribute__((always_inline));
  inline void nFeedbackInfo(const char * s) __attribute__((always_inline));
  inline double sigmoid(double value) __attribute__((always_inline));
  int commonMax(double vector[], size_t length, ClassIdentifier * ci);
  
  inline double dotProduct24(
          size_t length,
          double * vector1,
          double * vector2
        ) __attribute__((always_inline));
  
  inline double dotProduct16(
          size_t length,
          double * vector1,
          double * vector2
        ) __attribute__((always_inline));
  
  inline double dotProduct8(
          size_t length,
          double * vector1,
          double * vector2
        ) __attribute__((always_inline));
  
  inline double dotProduct4(
          size_t length,
          double * vector1,
          double * vector2
        ) __attribute__((always_inline));


#ifdef	__cplusplus
}
#endif

#endif	/* _COMMONS_H */

