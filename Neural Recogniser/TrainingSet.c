/**
 * Copyright 2008 MentorLycon, LLC.
 * The author of this software is Kareem Alkaseer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <inttypes.h>
#include <ctype.h>
#include <errno.h>
#include "TrainingSet.h"
#include "Commons.h"
#include "GlobalConstants.h"


void tsPrivateClearFile(FILE * file, const char * path);

static const uint32_t TRAINING_SET_SIGNATURE        = 2928272621;
static const uint32_t TRAINING_SET_LIST_SIGNATURE   = 2928272622;
static const uint32_t TRAINING_SET_VECTOR_SIGNATURE = 2928272623;
        
struct trainingset_t {
  size_t  tVectorLength;
  size_t  oVectorLength;
  double  * tVector;
  double  * oVector;
};

// Prototypes
//-----------------------------------------------------------------------------

void fillVector(FILE * file, double * vector, const size_t length);


// Construction and Destruction
//-----------------------------------------------------------------------------

TrainingSet * tsTrainingSet (size_t TVLength, size_t OVLength) {
  
  const char * s = "training set was not created. No enough memory";
  
  TrainingSet * set = (TrainingSet *)malloc(sizeof(TrainingSet));
  if (!set) {
    nError(s, __func__);
    return (TrainingSet *)NULL;
  }
  
  set->tVectorLength = TVLength;
  set->tVector = (double *)calloc(set->tVectorLength, sizeof(double));
  if(!(set->tVector)) {
    nError(s, __func__);
    free(set);
    return (TrainingSet *)NULL;
  }
  
  set->oVectorLength = OVLength;
  set->oVector = (double *)calloc(set->oVectorLength, sizeof(double));
  if(!(set->oVector)) {
    nError(s, __func__);
    free(set->tVector);
    free(set);
    return (TrainingSet *)NULL;
  }  
  
  return set;
}

int tsDelete(TrainingSet ** set) {
  if(!(*set)) {
    nError("training set was not deleted. The training set given is NULL.", __func__);
    return NONEXISTING_SET;
  }
  
  free((*set)->tVector);
  free((*set)->oVector);
  free((*set));
  
  //(*set)->tVector = NULL;
  //(*set)->oVector = NULL;  
  (*set) = NULL;
  
  return OPERATION_SUCCESS;
}

//-----------------------------------------------------------------------------


// Get and Set member fields
//-----------------------------------------------------------------------------

int tsTVectorLength(TrainingSet * set) {
  if(!set) {
    nError("the training set given is NULL.", __func__);
    return NONEXISTING_SET;
  }
  
  return set->tVectorLength;
}

int tsOVectorLength(TrainingSet * set) {
  if(!set) {
    nError("the training set given is NULL.", __func__);
    return NONEXISTING_SET;
  }
  
  return set->oVectorLength;
}

double * tsTVector(TrainingSet * set) {
  if (!set) {
    nError("the training set given is NULL.", __func__);
    return (double *)NULL;
  }
  
  return set->tVector;
}

int tsSetTVector(TrainingSet * set, double inputs[]) {
  if (!set) {
    nError("the training set given is NULL.", __func__);
    return NONEXISTING_SET;
  }
  
  size_t n = sizeof(inputs) / sizeof(double);
  int i;
  
  for (i = 0; i < set->tVectorLength || i < n; i++)
    set->tVector[i] = inputs[i];
  
  return i;
}

double * tsOVector(TrainingSet * set) {
  if (!set) {
    nError("the training set given is NULL.", __func__);
    return (double *)NULL;
  }
  
  return set->oVector;
}

int tsSetOVector(TrainingSet * set, double outputs[]) {
  if (!set) {
    nError("the training set given is NULL.", __func__);
    return NONEXISTING_SET;
  }
  
  size_t n = sizeof(outputs) / sizeof(double);
  int i;
  
  for (i = 0; i < set->oVectorLength || i < n; i++)
    set->oVector[i] = outputs[i];
  
  return i;
}

//-----------------------------------------------------------------------------


// Asignment, Copying, Comparing and Swapping
//-----------------------------------------------------------------------------
void tsShallowCopy(TrainingSet * source, TrainingSet ** copy) {
  if (!source)
    nWarn("NULL assignment.", __func__);
  
  tsDelete(copy);
  (*copy) = source;
}

TrainingSet * tsDeepCopy(TrainingSet * set) {
  if (!set) {
    nError("copy was not created. Copying a NULL set is not permitted.", __func__);
    return (TrainingSet *)NULL;
  }
  
  int i = 0;
  TrainingSet * newSet = tsTrainingSet(set->tVectorLength, set->oVectorLength);
  
  if (!newSet)
    return (TrainingSet *)NULL;
  
  for (i; i < newSet->tVectorLength; i++)
    newSet->tVector[i] = set->tVector[i];
  
  for (i = 0; i < newSet->oVectorLength; i++) 
    newSet->oVector[i] = set->oVector[i];
  
  return newSet;
}

int tsSwap(TrainingSet ** set1, TrainingSet ** set2) {
  if (!(*set1) || !(*set2)) {
    nError("swapping was cancelled. Swapping NULL sets is not permitted.", __func__);
    return NONEXISTING_SET;
  }
  
  TrainingSet * temp = tsDeepCopy(*set1);
  tsDelete(set1);
  (*set1) = tsDeepCopy(*set2);
  tsDelete(set2);
  (*set2) = temp;
  
  return OPERATION_SUCCESS;
}

bool tsIsEqual(TrainingSet * set1, TrainingSet * set2) {
  if (!set1 || !set2) {
    nWarn("comparing NULL value.", __func__);
    return false;
  }
  
  if (
          (set1->tVectorLength != set2->tVectorLength) ||
          (set1->oVectorLength != set2->oVectorLength)
     )
    return false;
  
  register size_t i = 0;
  
  for (i; i < set1->tVectorLength; i++)
    if (set1->tVector[i] != set2->tVector[i])
      return false;
  
  for (i = 0; i < set1->oVectorLength; i++)
    if (set1->oVector[i] != set2->oVector[i])
      return false;
  
  return true;
}

//-----------------------------------------------------------------------------


// Retrieval size of the TrainingSet structure and actual memory consumed by all
// the data depenedent on a given set
//-----------------------------------------------------------------------------

size_t tsSizeOf(void) {
  return sizeof(TrainingSet);
}

int tsMemConsumed(TrainingSet * const set) {
  if (!set) {
    nError("the training set given is NULL.", __func__);
    return NONEXISTING_SET;
  }
  
  int size = (set->tVectorLength * sizeof(double));
  size += (set->oVectorLength * sizeof(double));
  size += sizeof(TrainingSet);
  
  return size;
}

//-----------------------------------------------------------------------------


// Initialisation of a TrainingSet from an unfromatted file (user's made file)
//-----------------------------------------------------------------------------

void fillVector(FILE * file, double * vector, const size_t length) {
  char * buf = (char *)malloc(sizeof(double));
  int ch;
  int previous = ' ';
  volatile int i = 0;
  volatile int j = 0;
  double num = 0.0;
  //bool shouldInitOVector = true;  
  
  while (!feof(file) && i < length) {
    ch = fgetc(file);
    //if (errno)
      //printf("error whilst reading ch.\n\tch: %d\n", ch);
    //printf("ch: %c\n", ch);
    if (isdigit(ch) || ch == '.' || ch == '-') {
      buf[j] = ch;
      j++;
    }
    else if (ch == ' ' || ch == '/' || ch == '\n' || feof(file)) {
      if (previous != ' ' && previous != '\n') {
        buf[j] = '\0';
        j = 0;
        num = strtod(buf, (char **)NULL);
        //if (errno == ERANGE)
          //printf("over/underflow\n");
        //printf("num: %f\n", num);
        vector[i] = num;
        i++;
      }
      if (ch == '/') {
        while(! isdigit(fgetc(file)))
          break;
      }
    }
    else
      ch = ' ';
    
    previous = ch;
  }
  
  free(buf);
}

int tsFileInit (TrainingSet * set, const char * path) {
  
  if (!set) {
    nError("cannot initialise a NULL set.", __func__);
    return NONEXISTING_SET;
  }
  
  FILE * file = fopen(path, "r");
  if (!file) {
    nError("set was not initialised or changed. Cannot open file for reading.", __func__);
    return CANNOT_OPEN_FILE;    
  }
  
  fillVector(file, set->tVector, set->tVectorLength);
  fillVector(file, set->oVector, set->oVectorLength);
  
  fclose(file);
  return OPERATION_SUCCESS;
}

void tsPrivateClearFile(FILE * file, const char * path) {
  fclose(file);
  file = fopen(path, "wb");
  if (file)
    fclose(file);
}

int tsSave (TrainingSet * set, const char * path) {
  if (!set) {
    nError("cannot save a NULL set.", __func__);
    return NONEXISTING_SET;
  } 
  
  char ext [] = ".bts";
  char realpath [strlen(path) + 5];
  realpath[0] = '\0';
  strcat(realpath, path);
  strcat(realpath, ext);
  
  FILE * file = fopen(realpath, "wb");
  if (!file) {
    nError("set was not initialised or changed. Cannot open file for writing.", __func__);
    return CANNOT_OPEN_FILE;    
  }
  
  int opreturn;
  uint32_t signature = TRAINING_SET_SIGNATURE;
  
  opreturn = fwrite(&signature, sizeof(uint32_t), 1, file);
  if (opreturn != 1) {
    nError("set was not saved. Writing error", __func__);
    tsPrivateClearFile(file, realpath);
    return CANNOT_WRITE_FILE;
  }
  
  opreturn = fwrite(&(set->tVectorLength), sizeof(size_t), 1, file);
  if (opreturn != 1) {
    nError("set was not saved. Writing error", __func__);
    tsPrivateClearFile(file, realpath);
    return CANNOT_WRITE_FILE;
  }
  opreturn = fwrite(&(set->oVectorLength), sizeof(size_t), 1, file);
  if (opreturn != 1) {
    nError("set was not saved. Writing error", __func__);
    tsPrivateClearFile(file, realpath);
    return CANNOT_WRITE_FILE;
  }
  opreturn = fwrite(set->tVector, sizeof(double), set->tVectorLength, file);
  if (opreturn != set->tVectorLength) {
    nError("set was not saved. Writing error", __func__);
    tsPrivateClearFile(file, realpath);
    return CANNOT_WRITE_FILE;
  }
  opreturn = fwrite(set->oVector, sizeof(double), set->oVectorLength, file);
  if (opreturn != set->oVectorLength) {
    nError("set was not saved. Writing error", __func__);
    tsPrivateClearFile(file, realpath);
    return CANNOT_WRITE_FILE;
  }
  
  fclose(file);
  return OPERATION_SUCCESS;
}

static TrainingSet * tsLoad(const char * path) {    
  FILE * file = fopen(path, "rb");
  if (!file) {
    nError("set was not initialised or changed. Cannot open file for reading.", __func__);
    return (TrainingSet *)NULL;    
  }
  
  TrainingSet * set = (TrainingSet *)malloc(sizeof(TrainingSet));
  if (!set) {
    nError("set was not loaded. No enough memory.", __func__);
    fclose(file);
    return (TrainingSet *)NULL;
  }
  
  int opreturn;
  uint32_t signature;
  
  opreturn = fread(&signature, sizeof(uint32_t), 1, file);
  if (opreturn != 1) {
    nError("set was not saved. Reading error", __func__);
    fclose(file);
    free(set);
    return (TrainingSet *)NULL;
  }
  
  opreturn = fread(&(set->tVectorLength), sizeof(size_t), 1, file);
  if (opreturn != 1) {
    nError("set was not saved. Reading error", __func__);
    fclose(file);
    free(set);
    return (TrainingSet *)NULL;
  }
  opreturn = fread(&(set->oVectorLength), sizeof(size_t), 1, file);
  if (opreturn != 1) {
    nError("set was not saved. Reading error", __func__);
    fclose(file);
    free(set);
    return (TrainingSet *)NULL;
  }
  set->tVector = (double *)malloc(set->tVectorLength * sizeof(double));
  if (!set->tVector) {
    nError("set was not loaded. No enough memory.", __func__);
    fclose(file);
    free(set);
    return (TrainingSet *)NULL;
  }
  opreturn = fread(set->tVector, sizeof(double), set->tVectorLength, file);
  if (opreturn != set->tVectorLength) {
    nError("set was not saved. Reading error", __func__);
    fclose(file);
    free(set->tVector);
    free(set);
    return (TrainingSet *)NULL;
  }
  set->oVector = (double *)malloc(set->oVectorLength * sizeof(double));
  if (!set->oVector) {
    nError("set was not loaded. No enough memory.", __func__);
    fclose(file);
    free(set->tVector);
    free(set);
    return (TrainingSet *)NULL;
  }
  opreturn = fread(set->oVector, sizeof(double), set->oVectorLength, file);
  if (opreturn != set->oVectorLength) {
    nError("set was not saved. Reading error", __func__);
    fclose(file);
    free(set->tVector);
    free(set->oVector);
    free(set);
    return (TrainingSet *)NULL;
  }
  
  fclose(file);
  return set;
}

static size_t tsSaveList(size_t length, TrainingSet * sets[length], const char * path, int * feedback) {
  char ext [6] = ".btsl";
  char realpath [strlen(path) + 6];
  realpath[0] = '\0';
  strcat(realpath, path);
  strcat(realpath, ext);
  
  FILE * file = fopen(realpath, "wb");
  if (!file) {
    nError("set list was not saved. File could not be opened for writting.", __func__);
    *feedback = CANNOT_OPEN_FILE;
    return 0;
  }
  
  register size_t i;
  size_t count = 0;
  int opreturn;
  uint32_t signature = TRAINING_SET_LIST_SIGNATURE;
  bool containsNull = false;
  
  opreturn = fwrite(&signature, sizeof(uint32_t), 1, file);
  if (opreturn != 1) {
    nError("set list was not saved. Writting error.", __func__);
    *feedback = CANNOT_WRITE_FILE;
    tsPrivateClearFile(file, realpath);
    return 0;
  }
  
  for (i = 0; i < length; i++)    // Obtain the number of non-null TrainingSets
    if (sets[i])
      count++;
  
  if (count != length)
    containsNull = true;
  
  opreturn = fwrite(&count, sizeof(size_t), 1, file);
  if (opreturn != 1) {
    nError("set list was not saved. Writting error.", __func__);
    *feedback = CANNOT_WRITE_FILE;
    tsPrivateClearFile(file, realpath);
    return 0;
  }
  
  for (i = 0; i < length; i++) 
    if (sets[i]) {      
      opreturn = fwrite(&(sets[i]->tVectorLength), sizeof(size_t), 1, file);
      if (opreturn != 1) {
        nError("set was not saved. Writing error", __func__);
        tsPrivateClearFile(file, realpath);
        *feedback = CANNOT_WRITE_FILE;
        return 0;
      }
      opreturn = fwrite(&(sets[i]->oVectorLength), sizeof(size_t), 1, file);
      if (opreturn != 1) {
        nError("set was not saved. Writing error", __func__);
        tsPrivateClearFile(file, realpath);
        *feedback = CANNOT_WRITE_FILE;
        return 0;
      }
      opreturn = fwrite(sets[i]->tVector, sizeof(double), sets[i]->tVectorLength, file);
      if (opreturn != sets[i]->tVectorLength) {
        nError("set was not saved. Writing error", __func__);
        tsPrivateClearFile(file, realpath);
        *feedback = CANNOT_WRITE_FILE;
        return 0;
      }
      opreturn = fwrite(sets[i]->oVector, sizeof(double), sets[i]->oVectorLength, file);
      if (opreturn != sets[i]->oVectorLength) {
      nError("set was not saved. Writing error", __func__);
      tsPrivateClearFile(file, realpath);
      *feedback = CANNOT_WRITE_FILE;
      return 0;
      }      
    }   
  
  if (containsNull)
    *feedback = CONTAINS_NULL;
  else
    *feedback = CONTAINS_NO_NULL;

  fclose(file);
  return count;
}


static size_t tsLoadList(size_t length, TrainingSet * sets[length], const char * path, int * feedback) {
  FILE * file = fopen(path, "rb");
  if (!file) {
    nError("set list was not loaded. Could not open file for reading.", __func__);
    *feedback = CANNOT_OPEN_FILE;
    return 0;
  }
  
  size_t readlen;     // Length of set read from file. Used as a counter later.
  int opreturn;
  uint32_t signature;
  register size_t i;
  
  opreturn = fread(&signature, sizeof(uint32_t), 1, file);
  if (opreturn != 1) {
    nError("set list was not loaded. Reading error.", __func__);
    *feedback = CANNOT_READ_FILE;
    fclose(file);
    return 0;
  }
  
  if (signature != TRAINING_SET_LIST_SIGNATURE) {
    nError("set list was not loaded. File given is not a TrainingSet list binary file.", __func__);
    *feedback = UNRECOGNISED_FILE_FORMAT;
    fclose(file);
    return 0;    
  }
  
  opreturn = fread(&readlen, sizeof(size_t), 1, file);
  if (opreturn != 1) {
    nError("set list was not loaded. Reading error.", __func__);
    *feedback = CANNOT_READ_FILE;
    fclose(file);
    return 0;
  }
  
  if (readlen != length) {
    nError("set was not loaded. Length provided does not match save list's length.", __func__);
    *feedback = NONMATCHING_LIST_LENGTH;
    fclose(file);
    return 0;
  }
  
  for (i = 0; i < length; i++) {
    
    sets[i] = (TrainingSet *)malloc(sizeof(TrainingSet));
    if (!sets[i]) {
      nError("set list was not loaded. No enough memory", __func__);
      *feedback = MEMORY_EXHAUSTION;
      for (readlen = 0; readlen < i; readlen++) {
        free(sets[readlen]->tVector);
        free(sets[readlen]->oVector);
        free(sets[readlen]);
      }
      fclose(file);
      return 0;
    }
    
    opreturn = fread(&(sets[i]->tVectorLength), sizeof(size_t), 1, file);
    if (opreturn != 1) {
      nError("set list was not loaded. Reading error.", __func__);
      *feedback = CANNOT_READ_FILE;
      for (readlen = 0; readlen < i; readlen++) {
        free(sets[readlen]->tVector);
        free(sets[readlen]->oVector);
        free(sets[readlen]);
      }
      free(sets[readlen]);  // free current failing set
      fclose(file);
      return 0;
    }

    opreturn = fread(&(sets[i]->oVectorLength), sizeof(size_t), 1, file);
    if (opreturn != 1) {
      nError("set list was not loaded. Reading error.", __func__);
      *feedback = CANNOT_READ_FILE;
      for (readlen = 0; readlen < i; readlen++) {
        free(sets[readlen]->tVector);
        free(sets[readlen]->oVector);
        free(sets[readlen]);
      }
      free(sets[readlen]);  // free current failing set
      fclose(file);
      return 0;
    }

    sets[i]->tVector = (double *)malloc(sets[i]->tVectorLength * sizeof(double));
    if (!sets[i]->tVector) {
      nError("set list was not loaded. No enough memory.", __func__);
      *feedback = MEMORY_EXHAUSTION;
      for (readlen = 0; readlen < i; readlen++) {
        free(sets[readlen]->tVector);
        free(sets[readlen]->oVector);
        free(sets[readlen]);
      }
      free(sets[readlen]);  // free current failing set
      fclose(file);
      return 0;
    }

    opreturn = fread(sets[i]->tVector, sizeof(double), sets[i]->tVectorLength, file);
    if (opreturn != sets[i]->tVectorLength) {
      nError("set list was not loaded. Reading error.", __func__);
      *feedback = CANNOT_READ_FILE;
      for (readlen = 0; readlen < i; readlen++) {
        free(sets[readlen]->tVector);
        free(sets[readlen]->oVector);
        free(sets[readlen]);
      }
      free(sets[readlen]->tVector);
      free(sets[readlen]);  // free current failing set
      fclose(file);
      return 0;
    }

    sets[i]->oVector = (double *)malloc(sets[i]->oVectorLength * sizeof(double));
    if(!sets[i]->oVector) {
      nError("set list was not loaded. No enough memory.", __func__);
      *feedback = MEMORY_EXHAUSTION;
      for (readlen = 0; readlen < i; readlen++) {
        free(sets[readlen]->tVector);
        free(sets[readlen]->oVector);
        free(sets[readlen]);
      }
      free(sets[readlen]->tVector);
      free(sets[readlen]);  // free current failing set
      fclose(file);
      return 0;
    }

    opreturn = fread(sets[i]->oVector, sizeof(double), sets[i]->oVectorLength, file);
    if (opreturn != sets[i]->oVectorLength) {
      nError("set list was not loaded. Reading error.", __func__);
      *feedback = CANNOT_READ_FILE;
      for (readlen = 0; readlen < i; readlen++) {
        free(sets[readlen]->tVector);
        free(sets[readlen]->oVector);
        free(sets[readlen]);
      }
      free(sets[readlen]->tVector);
      free(sets[readlen]->oVector);
      free(sets[readlen]);  // free current failing set
      fclose(file);
      return 0;
    }
  }
  
  fclose(file);
  return i;
}

static size_t tsAppendToList(size_t length, TrainingSet * sets[length], const char * path, int * feedback) {
  
}

static size_t tsSaveVector(size_t length, TrainingSet * sets[length], const char * path, int * feedback) {

}

//-----------------------------------------------------------------------------
