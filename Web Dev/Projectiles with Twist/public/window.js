/*
* Copyright (c) 2016, Kareem Alkaseer
* All rights reserved.
* 
* Redistribution and use in source and binary forms, with or without modification, are permitted
* provided that the following conditions are met:
* 
* 1. Redistributions of source code must retain the above copyright notice, this list of
* conditions and the following disclaimer.
* 
* 2. Redistributions in binary form must reproduce the above copyright notice, this list of
* conditions and the following disclaimer in the documentation and/or other materials
* provided with the distribution.
* 
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS
* OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
* COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
* EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
* GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
* ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
* OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/

var autoMode = false;

var enterFullscreen = function() {
  
  var el = document.documentElement
  
  if (el.requestFullscreen)
    el.requestFullscreen();
  else if (el.webkitRequestFullScreen)
    el.webkitRequestFullScreen();
  else if (el.mozRequestFullScreen)
    el.mozRequestFullScreen();
  else if (el.msRequestFullScreen)
    el.msRequestFullScreen();
}

var setupCanvas = function() {
  
  var canvas = document.getElementById("canvas");
  var windowRatio = window.innerHeight / window.innerWidth;
  var width = window.innerWidth;
  var height = width * windowRatio;
  
  canvas.style.width = width + 'px';
  canvas.style.height = height + 'px';
}

window.onload = setupCanvas;

window.addEventListener('resize', setupCanvas, true);

window.addEventListener('keydown', function(event) {
  if (event.ctrlKey || event.metaKey) {
    switch (String.fromCharCode(event.which).toLowerCase()) {

    case 'a':
      event.preventDefault();
      autoMode = !autoMode;
      break;
      
    case 'f':
      event.preventDefault();
      enterFullscreen();
      break;
    }
  }
});
