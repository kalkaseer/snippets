/*
* Copyright (c) 2016, Kareem Alkaseer
* All rights reserved.
* 
* Redistribution and use in source and binary forms, with or without modification, are permitted
* provided that the following conditions are met:
* 
* 1. Redistributions of source code must retain the above copyright notice, this list of
* conditions and the following disclaimer.
* 
* 2. Redistributions in binary form must reproduce the above copyright notice, this list of
* conditions and the following disclaimer in the documentation and/or other materials
* provided with the distribution.
* 
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS
* OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
* COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
* EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
* GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
* ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
* OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/

var maxHeight;
var maxWidth;

var projectileSize = 4;
var projectileSet = new Set();

var initView = function() {
  
  maxHeight = view.size.height - projectileSize;
  maxWidth = view.size.width - projectileSize;
}

window.addEventListener('resize', initView, true);

initView();

var intro = new PointText({
  point: view.center - [0,200],
  justification: 'center',
  fontSize: 30,
  font: 'Roboto',
  fillColor: '#4a2c2c',
  data: {
    lines: [
      'Welcome!',
      'I used Node.js and Paper.js to get this done.',
      'Enjoy!',
      '\n',
      'Ctrl+A: toggle firing balls automatically',
      'Ctrl+F: fullscreen',
      '\n',
      'Kareem'
    ],
    current: 0,
    letter: 0,
    finish: 0
  }
});

intro.onFrame = function(event) {
  
  if (intro.data.finish === 0 && event.count % 8 == 0) {
    var line = intro.data.lines[intro.data.current];
    intro.content += line[intro.data.letter];
    ++intro.data.letter;
    if (intro.data.letter >= line.length) {
      ++intro.data.current;
      intro.data.letter = 0;
      intro.content += '\n';
    }
  }
  
  if (intro.data.finish === 0) {
    if (intro.data.current == intro.data.lines.length)
      intro.data.finish = event.time;
  } else if (intro.data.finish + 1.0 - event.time < 0) {
    intro.remove();
    intro = undefined;
    new app();
  }
}

var app = function() {
 
  var Constants = {
    "g": 9.8,
    "timeDelta": 0.045
  };

  function randomColor(brightness) {
    return new Color(
      Math.random() + (brightness * 0.01), 
      Math.random() + (brightness * 0.01), 
      Math.random() + (brightness * 0.01));
  }

  function Projectile(point, angle, launchVelocity) {
  
    var color = randomColor(2);
  
    this.ball = new Path.Circle({
      radius: projectileSize,
      center: point,
      fillColor: color
    });
  
    this.path = new Path(point, point);
    this.path.strokeColor = color;
  
    this.angle = angle * Math.PI / 180.0;
    this.launchVelocity = launchVelocity;
    this.time = 0.0;
    this.horVelocity = this.launchVelocity * Math.cos(this.angle);
    this.initialVerVelocity = this.launchVelocity * Math.sin(this.angle);
  }

  Projectile.prototype.verVelocity = function() {
    return this.initialVerVelocity - (Constants.g * this.time);
  }

  Projectile.prototype.theta = function() {
    return Math.atan(this.verVelocity() / this.horVelocity);
  }

  Projectile.prototype.velocity = function() {
    var vy = this.verVelocity();
    return Math.sqrt((this.horVelocity * this.horVelocity) + (vy * vy));
  }

  Projectile.prototype.remove = function() {
    projectileSet.delete(this);
    this.ball.remove();
  }

  Projectile.prototype.bounce = function() {
  
    this.rate = -this.verVelocity() * 0.5;
    this.counter = 0.0;
  
    this.path.remove();
  
    this.update = function() {
    
      if (this.counter - 0.05 < 0) {
        this.counter += 0.025;
        return;
      }
    
      this.counter = 0
    
      if (this.ball.position.y >= maxHeight) {
        this.ball.position = [
          this.ball.position.x, 
          maxHeight - (this.rate * this.time * 2)
        ];
      } else  { 
       this.ball.position = [ this.ball.position.x, maxHeight ];
      }
    
      this.time += Constants.timeDelta;
      this.rate -= 0.009 * this.rate * Constants.g * this.time;

      if (this.rate - 0.05 <= 0) {
        this.remove();
      }
    };
  }

  Projectile.prototype.update = function() {
  
    if (this.ball.position.x >= maxWidth-projectileSize && this.ball.position.y < maxHeight) {
    
      this.path.remove();
    
      this.ball.position = new Point(
        maxWidth,
        this.ball.position.y + 0.5 * Constants.g * this.time * this.time);
    
     if (Math.ceil(this.ball.position.y) > Math.round(maxHeight *0.95)) {
       this.remove();
     }
  
    } else {
    
      var d = this.ball.position + [
        this.horVelocity * this.time,
        -this.initialVerVelocity * Math.sin(this.angle) * this.time 
          + (0.5 * Constants.g * this.time * this.time)
      ];

      if (d.y <= projectileSize) {
      
        this.path.remove();
      
        this.ball.position = new Point(
          d.x + 0.5 * Constants.g * this.time * this.time,
          projectileSize);
      
      } else if (d.x >= maxWidth) {
      
        if (d.y >= maxHeight) {
        
          this.ball.position = maxHeight - projectileSize;
          this.bounce();
          return;
        
        } else {
        
          this.path.remove();
          d.x = maxWidth;
          this.time = 0;
        }
      
        this.ball.position = d;
    
      } else if (d.y >= maxHeight) {
      
        this.bounce();
        return;
      
      } else {
      
        this.path.add(this.ball.position);
        this.path.add(d);
        this.ball.position = d;
    
      } 
    }
  
    this.time += Constants.timeDelta;
  }

  Projectile.prototype.position = function() {
    return this.ball.position;
  }

  view.onFrame = function(event) {
    projectileSet.forEach(function(p) {
      p.update();
    });
    
    if (autoMode === true  && Math.floor(event.time) % 2 == 0)
      for (var i = 0; i < 2; ++i) randomFire();
  }

  function mousePosition(point) {
    var rect = view.element.getBoundingClientRect();
    var xscale = maxWidth / rect.width;
    var yscale = maxHeight / rect.height;

    return new Point(
      (point.x - rect.left) * xscale,
      (point.y - rect.top) * yscale
    );
  }

  view.onMouseDown = function(event) {
    var p = new Projectile(
      mousePosition(event.point),
      Math.random() * 60.0 + 25.0,
      Math.random() * 13.0 + 4.0);
    projectileSet.add(p);
  }
  
  function randomFire() {

    var p = new Projectile(
      new Point (Math.random() * maxWidth, Math.random() * maxHeight),
      Math.random() * 60.0 + 25.0,
      Math.random() * 13.0 + 4.0);
    projectileSet.add(p);
  }
  
}