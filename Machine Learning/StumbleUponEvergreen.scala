/**
  * Copyright ©2016 MentorLycon, LLC
  * Author: Kareem Alkaseer
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  *      http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */

package com.mentorlycon.ml.classification

import java.io.File

import org.apache.spark.mllib.feature.{Normalizer, IDF, HashingTF, StandardScaler}
import org.apache.spark.mllib.evaluation.BinaryClassificationMetrics
import org.apache.spark.mllib.linalg
import org.apache.spark.mllib.optimization.SquaredL2Updater
import org.apache.spark.mllib.regression.LabeledPoint
import org.apache.spark.mllib.linalg.Vectors
import org.apache.spark.mllib.tree.DecisionTree
import org.apache.spark.mllib.tree.configuration.Algo
import org.apache.spark.mllib.tree.impurity.Gini
import org.apache.spark.mllib.tree.model.DecisionTreeModel
import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.mllib.classification._

import scala.collection.mutable
import scala.util.parsing.json.JSON

/**
  * Author: Kareem Alkaseer
  * Date: 27/08/16
  *
  * Full
  * svm, area under PR: 77.0287%, area under ROC: 59.9609%, accuracy: 61.3472%
  * logistic regression, area under PR: 82.7383%, area under ROC: 75.2593%, accuracy: 74.9914%
  * naive bayes, area under PR: 79.2562%, area under ROC: 64.3104%, accuracy: 65.6649%
  *
  * Partial
  * svm, area under PR: 77.0287%, area under ROC: 59.9609%, accuracy: 61.3472%
  * logistic regression, area under PR: 82.7383%, area under ROC: 75.2593%, accuracy: 74.9914%
  * naive bayes, area under PR: 79.2562%, area under ROC: 64.3104%, accuracy: 65.6649%
  *
  * Aggregate
  * svm, area under PR: 75.7111%, area under ROC: 51.3019%, accuracy: 53.3333%
  * logistic regression, area under PR: 77.6156%, area under ROC: 68.0168%, accuracy: 68.1865%
  * naive bayes, area under PR: 75.0909%, area under ROC: 63.9425%, accuracy: 64.2142%
  * decision trees, area under PR: 72.4636%, area under ROC: 61.3582%, accuracy: 39.4667%
  */

object StumbleUponEvergreen extends App {

  object Run {
    sealed trait Type
    case object Aggregate extends Type
    case object Partial extends Type
    case object Full extends Type
  }

  private val sc = new SparkContext(
    new SparkConf()
      .setMaster("local[2]")
      .set("spark.driver.memory", "8gb")
      .set("spark.ui.port", "4040")
      .set("spark.rdd.compress", "true")
      .set("spark.driver.extraLibraryPath", "lib")
      .set("spark.executor.extraLibraryPath", "lib")
      .setAppName("StumbleUponEvergreen"))
  sc.requestExecutors(2)

  private val runType: Run.Type = Run.Partial

  private val dataDir = new File("data/StumbleUponEvergreenClassificationChallenge").getAbsolutePath

  private val trainSetPath = dataDir + "/train_noheader.tsv"

  private val runDir = runType match {
    case Run.Aggregate => dataDir + "/aggregate"
    case Run.Partial => dataDir + "/partial"
    case Run.Full => dataDir + "/full"
  }

  private val maxIterations = 13

  private val maxTreeDepth = 20


  var data: RDD[LabeledPoint] = null

  var testData: RDD[LabeledPoint] = null

  var nbData: RDD[LabeledPoint] = null

  var nbTestData: RDD[LabeledPoint] = null


  private var lrModel: LogisticRegressionModel = null

  private var svmModel: SVMModel = null

  private var nbModel: NaiveBayesModel = null

  private var dtModel: DecisionTreeModel = null


  // model_name -> (Area under PR, Area under ROC, Accuracy)
  private val results: mutable.HashMap[String, (Double, Double, Double)] =
    new mutable.HashMap[String, (Double, Double, Double)]()


  def parseRecords(
    records: RDD[Array[String]], categories: Map[String, Int]
  ): RDD[(Int, Array[Double], linalg.SparseVector)] =
  {

    val textRdd = records.map { r =>
      val text = r(2).replaceAll("(\"[{])", "{").replaceAll("\"\"", "\"").replaceAll("}\"", "}")
      val json = JSON.parseFull(text).asInstanceOf[Option[Map[String, String]]]
      json match {
        case Some(e) =>
          e.getOrElse("title", "") match {
            case null =>
              e.getOrElse("body", "") match {
                case null => Seq("")
                case s => s.split(" ").toSeq
              }
            case s1 =>
              e.getOrElse("body", "") match {
                case null => s1.split(" ").toSeq
                case s2 => s1.split(" ").toSeq ++ s2.split(" ").toSeq
              }
          }
        case None => Seq("")
      }
    }

    val tf = new HashingTF().transform(textRdd)
    var tfidf = new IDF().fit(tf).transform(tf)

    val rdd = records.map { r =>
      val trimmed = r.map(_.replaceAll("\"", ""))
      val label = trimmed(r.length - 1).toInt
      val categoryIndex = categories(r(3))
      val categoryFeatures = Array.ofDim[Double](categories.size)
      categoryFeatures(categoryIndex) = 1.0
      val otherFeatures = trimmed.slice(4, r.length - 1)
        .map(d => if (d == "?") 0.0 else d.toDouble)
      val features = categoryFeatures ++ otherFeatures

      (label, features)
    }

    if (runType == Run.Full || runType == Run.Partial) {
      var maxVectorLength = 0
      tfidf.foreach(v => maxVectorLength = Math.max(v.size, maxVectorLength))

      tfidf = tfidf.map { v =>
        if (v.size < maxVectorLength) {
          Vectors.sparse(maxVectorLength, v.toArray.toList.zipWithIndex.map(t => (t._2, t._1)))
        } else {
          v.toSparse
        }
      }
    }

    val normalizer = new Normalizer()

    rdd.zip(tfidf).map { r =>
      (r._1._1, r._1._2, normalizer.transform(r._2).toSparse)
    }

  }

  def top(n: Int, list: List[Double]) : List[Double] = {

    def update(current: List[Double], el: Double) : List[Double] = {
      if (el < current.head)
        (el :: current.tail).sortWith(_ > _)
      else
        current
    }

    (list.take(n).sortWith(_ > _) /: list.drop(n)) (update(_,_))
  }

  def parse(path: String, categories: Map[String, Int]) = {

    var file = new File(runDir + "/raw.txt")

    val rdd = {
      if (file.exists) {
        sc.objectFile(file.getAbsolutePath).asInstanceOf[RDD[(Int, Array[Double], linalg.Vector)]]
      } else {
        val r = parseRecords(sc.textFile(path).map(_.split("\t")), categories)
        r.saveAsObjectFile(file.getAbsolutePath)
        r
      }
    }


    file = runType match {
      case Run.Aggregate => new File(runDir + "/tfidfFactor.txt")
      case Run.Partial => new File(runDir + "/tfidfFactor.txt")
      case _ => null
    }

    val tfidfScaler = runType match {
      case Run.Aggregate =>
        new StandardScaler().fit(rdd.map(_._3))
      case _ => null
    }


    val tfidftFactor = {

      if (file != null && file.exists()) {

        sc.objectFile(file.getAbsolutePath).asInstanceOf[RDD[Array[Double]]]

      } else {

        runType match {

          case Run.Aggregate => {

            val r = rdd.map { r =>
              val scaled = tfidfScaler.transform(r._3).toArray.toList
              val t = top(10, scaled)
              val mean = t.sum / t.size
              val std = Math.sqrt(t.map(d => (d - mean) * (d - mean)).sum / t.size)
              Array(scaled.sum / scaled.size, std * mean)
            }
            r.saveAsObjectFile(file.getAbsolutePath)
            r
          }

          case Run.Partial => {

            val r = rdd.map { r =>
              val list = r._3.toArray.toList
              (list.sum/list.size :: top(10, list)).toArray
            }
            r.saveAsObjectFile(file.getAbsolutePath)
            r
          }

          case _ => null

        }

      }

    }


    file = new File(runDir + "/processed.txt")

    val dataRdd = {

      if (file.exists()) {

        sc.objectFile(file.getAbsolutePath).asInstanceOf[RDD[LabeledPoint]]

      } else {

        runType match {

          case Run.Aggregate => {

            var dataRdd = rdd.zip(tfidftFactor).map { r =>
              LabeledPoint(r._1._1, Vectors.dense(r._1._2 ++ r._2))
            }

            val vectors = dataRdd.map(point => point.features)
            val scaler = new StandardScaler().fit(vectors)

            dataRdd = dataRdd.map { point =>
              LabeledPoint(point.label, scaler.transform(point.features))
            }

            dataRdd.saveAsObjectFile(file.getAbsolutePath)
            dataRdd
          }

          case Run.Partial | Run.Full => {

            var features = rdd.map(r => (r._1, Vectors.dense(r._2), r._3))

            val scaler = new StandardScaler().fit(features.map(_._2))

            features = features.map( r=> (r._1, scaler.transform(r._2), r._3))

            val dataRdd = features.map { r =>
              var list: List[(Int, Double)] = Nil
              r._3.foreachActive((i, d) => list = (r._2.size + i, d) :: list)
              val alist = r._2.toArray.toList.zipWithIndex.map { case (d, i) => (i, d) :: list }
              LabeledPoint(r._1, Vectors.sparse(r._2.size + r._3.size, list.toArray))
            }

            dataRdd.saveAsObjectFile(file.getAbsolutePath)
            dataRdd
          }

        }
      }
    }

    dataRdd.persist

    dataRdd
  }

  init

  def init = {


    val categories = sc.textFile(trainSetPath)
      .map(_.split("\t"))
      .map(r => r(3))
      .distinct.collect.zipWithIndex.toMap

    val splits = parse(trainSetPath, categories).randomSplit(Array(0.6, 0.4), 11)

    data = splits(0)

    testData = splits(1)
  }


  def accuracy(data: RDD[LabeledPoint], model: ClassificationModel, fn: (Double) => Double): Double = {
    data.map { point =>
      if (fn(model.predict(point.features)) == point.label) 1 else 0
    }.sum / data.count()
  }

  def dtAccuracy(): Double = {
    val dtCorrect = testData.map { point =>
      val predicted = if (dtModel.predict(point.features) > 0.5) 1 else 0
      if (predicted == point.label) 1 else 0
    }.sum
    dtCorrect / data.count
  }


  def metrics(data: RDD[LabeledPoint], model: ClassificationModel, fn: (Double) => Double) : (Double, Double, Double) = {

    val acc = accuracy(data, model, fn)

    val scoresAndLabels = data.map { point =>
      (fn(model.predict(point.features)), point.label)
    }
    val m = new BinaryClassificationMetrics(scoresAndLabels)
    (m.areaUnderPR(), m.areaUnderROC(), acc)
  }

  def dtMetrics(): (Double, Double, Double) = {

    val scoresAndLabels = testData.map { point =>
      val score = dtModel.predict(point.features)
      (if (score > 0.5) 1.0 else 0.0, point.label)
    }
    val m = new BinaryClassificationMetrics(scoresAndLabels)
    (m.areaUnderPR, m.areaUnderROC, dtAccuracy())
  }

  lrModel = {

    val file = new File(runDir + "/logistic_regression_model")
    if (file.exists()) {
      LogisticRegressionModel.load(sc, file.getAbsolutePath)
    } else {
      val m = new LogisticRegressionWithLBFGS()
      m.setNumClasses(2)
      m.optimizer
        .setNumIterations(maxIterations)
      val model = m.run(data)
      model.save(sc, file.getAbsolutePath)
      model
    }
  }

  results.put("logistic regression", metrics(testData, lrModel, (d: Double) => d))

  svmModel = {
    val file = new File(runDir + "/svm_model")
    if (file.exists()) {
      SVMModel.load(sc, file.getAbsolutePath)
    } else {
      val m = new SVMWithSGD
      m.optimizer
        .setUpdater(new SquaredL2Updater)
        .setNumIterations(maxIterations)
      val model = m.run(data)
      model.save(sc, file.getAbsolutePath)
      model
    }
  }

  results.put("svm", metrics(testData, svmModel, (d: Double) => d))

  if (runType == Run.Aggregate) {

      dtModel = {
        val file = new File(runDir + "/decision_trees_model")
        if (file.exists()) {
          DecisionTreeModel.load(sc, file.getAbsolutePath)
        } else {
          val model = DecisionTree.train(data, Algo.Classification, Gini, maxTreeDepth)
          model.save(sc, file.getAbsolutePath)
          model
        }
      }

      results.put("decision trees", dtMetrics())
  }


  nbData = data.map { point =>
    LabeledPoint(
      point.label,
      Vectors.dense(
        point.features.toArray.map(d => if (d < 0.0) 0.0 else d))
    )
  }
  nbData.persist

  nbTestData = testData.map { point =>
    LabeledPoint(
      point.label,
      Vectors.dense(
        point.features.toArray.map(d => if (d < 0.0) 0.0 else d))
    )
  }
  nbTestData.persist

  testData.unpersist()
  data.unpersist()
  testData = null
  data = null

  nbModel = {
    val file = new File(runDir + "/naive_bayes_model")
    if (file.exists()) {
      NaiveBayesModel.load(sc, file.getAbsolutePath)
    } else {
      val model = NaiveBayes.train(nbData, maxIterations)
      model.save(sc, file.getAbsolutePath)
      model
    }
  }

  results.put("naive bayes", metrics(nbTestData, nbModel, (d: Double) => if (d > 0.5) 1.0 else 0.0))

  nbData.unpersist()
  nbTestData.unpersist()
  nbModel = null
  nbTestData = null
  nbData = null


  results.foreach {
    case (name, (pr, roc, accuracy)) =>
      println(
        f"$name, " +
          f"area under PR: ${pr * 100.0}%2.4f%%, " +
          f"area under ROC: ${roc * 100.0}%2.4f%%, " +
          f"accuracy: ${accuracy * 100.0}%2.4f%%")
  }


  sc.stop()
}
