#!/usr/bin/python

# Digit classification on the MNIST dataset using softmax regression.
# where evidance for a class i is defined as
#   evidance_i = Sum_j W_i,j x_j + b_i
# where W_i is the weights vector, b_i is the bias for class_i, j is an index
# over the pixels in the input vector x (image x).
# The predicated probabilities y is then defined as
# y = softmax(evidance)
# softmax(x) = normalise(exp(x))
#            = exp(x_i) / Sum_j exp(x_j)
# the final form is
# y = softmax(W x + b)

import tensorflow as tf
import numpy as np
from tensorflow.examples.tutorials.mnist import input_data

# import and read the MNIST dataset
mnist = input_data.read_data_sets("MNIST_data/", one_hot=True)

# placeholder for any number of mnist impages each flattened as a 784D vector
# that is a 2D tensor with shape [*, 784]
x = tf.placeholder(tf.float32, [None, 784])

# the weight vector, a tensor with shape[784, 10] initialised as zeros
# the shape is as such because we have 784D input vectors to be multiplied by
# the weight vector to produce 10D output vector classes where each element
# in the output vector corresponds to an evidance of a class
w = tf.Variable(tf.zeros([784, 10]))

# the bias vector, a tensor of share [10] because we need to add the bias 
# vector to the output vector
b = tf.Variable(tf.zeros([10]))

# the model
y = tf.nn.softmax(tf.matmul(x, w) + b)


# implement cross-entropy for model assessment
# H_target (y) = - Sum_i target_i log(y_i)

y_ = tf.placeholder(tf.float32, [None, 10])

cross_entropy = tf.reduce_mean(
  -tf.reduce_sum(y_ * tf.log(y), reduction_indices=[1]))

# backpropagation
step = tf.train.GradientDescentOptimizer(0.5).minimize(cross_entropy)

init = tf.initialize_all_variables()

with tf.Session() as sess:
  sess.run(init)
  
  for i in range(1000):
    xs, ys = mnist.train.next_batch(100)
    sess.run(step, feed_dict={x: xs, y_: ys})
  
  correct_predictions = tf.equal(tf.argmax(y, 1), tf.argmax(y_, 1))
  accuracy = tf.reduce_mean(tf.cast(correct_predictions, tf.float32))
  
  print(sess.run(
    accuracy, feed_dict={x: mnist.test.images, y_: mnist.test.labels}))