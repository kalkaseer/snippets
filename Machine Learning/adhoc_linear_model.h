/**
 * Copyright 2016 Kareem Alkaseer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ADHOC_LINEAR_MODEL_H_
#define ADHOC_LINEAR_MODEL_H_

#define _USE_MATH_DEFINES

#include <cmath>
#include <string>
#include <vector>
#include <istream>
#include <sstream>
#include <unordered_map>
#include <algorithm>
#include <iterator>
#include <numeric>
#include <limits>

/* Earth radius in Km */
#define EARTH_RADIUS 6371

using namespace std;

static std::vector<std::string> split(const std::string & s, char delim) {

  using namespace std;
  vector<string> out;
  string token;
  istringstream ss(s);
  while (getline(ss, token, delim))
    out.push_back(token);
  return std::move(out);
}

static double to_radians(double degrees) { return degrees * (M_PI / 180); }

class row {
public:

  explicit row(vector<string> features1, vector<string> features2, bool matches)
    : match{ matches }
  {
    data.push_back(stod(features1[0]));                             // age1
    data.push_back(stod(features2[0]));                             // age2
    data.push_back(stod(features1[0]) * stod(features1[0]));        // drinker
    data.push_back(location_proximity(features1[2], features2[2]));
    data.push_back(interests(
      features1.begin() + 3, features1.end(),
      features2.begin() + 3, features2.end()));
  }

  explicit row(size_t size, double x)
    : data(size, x), match{ false }
  { }

  row() : match{ false } { }

  static vector<row> load(istream s) {
    vector<row> rows;
    string line1, line2;
    while (true) {
      getline(s, line1);
      if (s.good()) {
        getline(s, line2);
        if (s.good()) {
          std::vector<string> row2 = split(line2, ',');
          bool match =  stoi(row2.back());
          row2.pop_back();
          rows.push_back(row(split(line1, ','), row2, match));
        } else {
          break;
        }
      }
      else
        break;
    }
    return rows;
  }

  // Haversine distance
  double location_proximity(string s1, string s2) {
    vector<string> v1 = split(string{ s1.begin() + 1, s1.end() - 1 }, ',');
    vector<string> v2 = split(string{ s2.begin() + 1, s2.end() - 1 }, ',');
    double latitude1  = stod(v1[0]);
    double longitude1 = stod(v1[1]);
    double latitude2  = stod(v2[0]);
    double longitude2 = stod(v2[1]);
    double lat_diff = to_radians(latitude1 - latitude2);
    double lng_diff = to_radians(longitude1 - longitude2);
    double a = sin(lat_diff / 2) * sin (lat_diff / 2) +
      cos(to_radians(latitude1)) * cos(to_radians(latitude2)) *
      sin(lng_diff / 2) * sin(lng_diff / 2);
    double c = 2 * atan2(sqrt(a), sqrt(1-a));
    return EARTH_RADIUS * c;
  }

  double static interests(
    vector<string>::iterator begin1, vector<string>::iterator end1,
    vector<string>::iterator begin2, vector<string>::iterator end2
  ) {
    sort(begin1, end1);
    sort(begin2, end2);
    vector<string> intersection;
    set_intersection(begin1, end1, begin2, end2, back_inserter(intersection));
    return static_cast<double>(intersection.size());
  }

  double dot(const row & other) {
    return inner_product(data.begin(), data.end(), other.data.begin(), 0);
  }

  size_t size() { return data.size(); }

  double & operator[](size_t n) { return data[n]; }

  vector<double>::iterator begin() { return data.begin(); }
  vector<double>::iterator end() { return data.end(); }

  vector<double> data;

  bool match;
};

class dataset {
public:

  explicit dataset(vector<row> rows)
    : data{ rows },
      low{ data[0].size(), numeric_limits<double>::max() },
      high{ data[0].size(), numeric_limits<double>::min() }
  {
    // find global minimum and maximum values for each feature to scale the set
    for (auto & r : data) {
      for (size_t i = 0; i < r.size(); ++i) {
        if (r[i] < low[i]) low[i] = r[i];
        if (r[i] > high[i]) high[i] = r[i];
      }
    }

    for (auto & r : data)
      scale(r);
  }

  // scale each feature in each row to be between [0,1]
  void scale(row & r) {
    for (size_t i = 0; i < r.size(); ++i)
      r[i] = (r[i] - low[i]) / (high[i] - low[i]);
  }
  
  size_t row_size() { return data[0].size(); }

  vector<row> data;
  row low;
  row high;
};

class linear_model {
public:

  explicit linear_model() { }

  void train(dataset & ds) {

   averages.clear();
   averages.emplace(true, row{ ds.row_size(), 0.0 });
   averages.emplace(false, row{ ds.row_size(), 0.0 });
   int match_count = 0;
   int nonmatch_count = 0;

   for (auto & row : ds.data) {
     for (size_t i = 0; i < ds.row_size(); ++i)
       averages[row.match][i] += row[i];
     if (row.match)
       ++match_count;
     else
       ++nonmatch_count;
   }

   for (auto it = averages.begin(); it != averages.end(); ++it) {
     int count = it->first ? match_count : nonmatch_count;
     for (auto & feature : it->second)
       feature /= count;
   }
  }

  bool classify(row point) {
    double b =
      (averages[false].dot(averages[false]) -
       averages[true].dot(averages[true])
      ) / 2;
    double y = point.dot(averages[false]) - point.dot(averages[true]) + b;
    return y > 0;
  }

  unordered_map<bool, row> averages;
};



#endif /* ADHOC_LINEAR_MODEL_H_ */
