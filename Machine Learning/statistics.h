/**
 * Copyright 2016 Kareem Alkaseer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef STATISTICS_H_
#define STATISTICS_H_

#include <functional>
#include <algorithm>
#include <numeric>
#include <cmath>
#include <vector>

template< class T >
struct to_double {
  using real_type = T;
  double operator()(const T & a) { return static_cast<double>(a); }
};

template<
  class RAIterator,
  class Comparator  = std::greater<typename RAIterator::value_type>,
  class Plus        = std::plus<typename RAIterator::value_type>,
  class Minus       = std::minus<typename RAIterator::value_type>,
  class Multiply    = std::multiplies<typename RAIterator::value_type>,
  class Divide      = std::divides<typename RAIterator::value_type>,
  class Real        = to_double<typename RAIterator::value_type> >
class statistics {

public:

  using iterator = RAIterator;

  using comparator = Comparator;

  using minus = Minus;

  using plus = Plus;

  using multiply = Multiply;

  using divide = Divide;

  using real = Real;

  using value_type = typename iterator::value_type;


  statistics() { }

  statistics(iterator first, iterator last)
    : m_first{ first }, m_last{ last }
  { }


  value_type median() {

    if (m_median_value.ok)
      return m_median_value.value;

    size_t length = m_last - m_first;

    if ((length & 1) == 0) {

      std::nth_element(m_first, m_first + length / 2, m_last);
      m_median_upper = optional_iterator{ m_first + length / 2 };
      m_median_lower = optional_iterator{ m_median_upper.value - 1 };
      m_median_value = optional_value{
        divide{}(plus{}(*m_median_upper.value, *m_median_lower.value), 2) };

    } else {

      std::nth_element(m_first, m_first + length / 2, m_last);
      m_median_upper = m_median_lower = optional_iterator{
        m_first + length / 2 };
      m_median_value = optional_value{ *m_median_upper.value };
    }

    return m_median_value.value;
  }

  iterator nth(std::size_t n) {
    std::nth_element(m_first, m_first + n, m_last);
    return m_first + n;
  }

  iterator q1() {

    if (m_q1.ok)
      return m_q1.value;

    if (! m_median_value.ok)
      median();

    std::nth_element(
      m_first,
      m_first + (m_median_lower.value - m_first) / 2,
      m_median_lower.value);
    m_q1 = optional_iterator{
      m_first + (m_median_lower.value - m_first) / 2 };
    return m_q1;
  }

  iterator q3() {

    if (m_q3.ok)
      return m_q3.value;

    if (! m_median_value.ok)
      median();

    std::nth_element(
      m_median_upper.value,
      m_median_upper.value + (m_last - m_median_upper.value) / 2,
      m_last);
    m_q3 = optional_iterator{
      m_median_upper.value + (m_last - m_median_upper.value) / 2 };
    return m_q3;
  }

  size_t iqr() {
    return minus{}(*q3(), *q1());
  }

  template< class Vector = std::vector<value_type> >
  Vector minor_outliers() {

    auto iqrv = iqr();
    auto lower = *q1() - (iqrv * 1.5);
    auto upper = *q3() + (iqrv * 1.5);
    std::vector<value_type> result;
    std::copy_if(
      m_first, m_last, std::back_inserter(result),
      [lower, upper](const value_type & a) { return a < lower || a > upper; }
    );
    return std::move(result);
  }

  template< class Vector = std::vector<value_type> >
  Vector major_outliers() {

    auto iqrv = iqr();
    auto lower = *q1() - (iqrv * 3.0);
    auto upper = *q3() + (iqrv * 3.0);
    std::vector<value_type> result;
    std::copy_if(
      m_first, m_last, std::back_inserter(result),
      [lower, upper](const value_type & a) { return a < lower || a > upper; }
    );
    return std::move(result);
  }

  value_type mean() {
    if (m_mean_value.ok)
      return m_mean_value.value;
    m_mean_value = optional_value{
      divide{}(std::accumulate(m_first, m_last, 0, plus{}), (m_last - m_first)) };
    return m_mean_value.value;
  }

  template< class Vector = std::vector<value_type> >
  value_type stddev() {

    if (m_std_dev.ok)
      return m_std_dev.value;

    real r{};
    auto add = plus{};
    auto subtract = minus{};
    auto times = multiply{};
    auto m = mean();
    auto length = m_last - m_first;
    Vector v(length);
    std::transform(
      m_first, m_last, v.begin(),
      [&r, &subtract, &m](const value_type & x) { return r(subtract(x, m)); });
    value_type product = std::inner_product(
      v.begin(), v.end(), v.begin(), 0, add, times);
    // -1 for sample standard deviation
    m_std_dev = optional_value{ std::sqrt(product / (length - 1)) };
    return m_std_dev.value;
  }

  value_type stddev_insensitive() {

    if (m_std_dev.ok)
      return m_std_dev.value;

    real r{};
    auto add = plus{};
    auto subtract = minus{};
    auto m = mean();
    typename real::real_type s = 0;
    std::for_each(m_first, m_last, [&](const value_type & x) {
      s = add(s, std::pow(r(subtract(x, m)), 2));
    });
    // -1 for sample standard deviation
    m_std_dev = optional_value{
      std::sqrt(1.0 / (m_last - m_first - 1) * s) };
    return m_std_dev.value;
  }

private:

  template< class T >
  struct optional {
    explicit optional() : ok{ false }, value{} { }
    explicit optional(T && v) : ok { true }, value{ std::forward<T>(v) } { }
    explicit optional(T & v) : ok { true }, value{ v } { }
    operator T() { return value; }
    bool ok;
    T value;
  };

  using optional_value = optional<value_type>;
  using optional_iterator = optional<iterator>;

  iterator          m_first;
  iterator          m_last;
  optional_iterator m_median_lower;
  optional_iterator m_median_upper;
  optional_iterator m_q1;
  optional_iterator m_q3;
  optional_value    m_median_value;
  optional_value    m_mean_value;
  optional_value    m_std_dev;
};


#endif /* STATISTICS_H_ */
