/**
 * Copyright 2016 Kareem Alkaseer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <vector>
#include <iostream>
#include "statistics.h"

template< class Iter >
void print(Iter begin, Iter end) {
  std::for_each(begin, end, [](const typename Iter::value_type & a) {
    std::cout << a << " ";
  });
}

int main() {

  using real = double;
  using vector = std::vector<real>;
  vector v{5, 6, 4, 3, 2, 6, 7, 9, 3};
  statistics<vector::iterator> s{ v.begin(), v.end() };

  std::cout << "median:       " << s.median() << std::endl;
  std::cout << "mean:         " << s.mean() << std::endl;
  std::cout << "std dev:      " << s.stddev<>() << std::endl;
  std::cout << "1st quartile: " << *s.q1() << std::endl;
  std::cout << "3rd quartile: " << *s.q3() << std::endl;
  auto outliers = s.minor_outliers<>();
  print(outliers.begin(), outliers.end());
  outliers = s.major_outliers<>();
  print(outliers.begin(), outliers.end());

  return 0;
}

