/*
 * dynamic_programming.cc
 *
 *  Created on: Sep 4, 2016
 *      Author: Kareem Alkaseer
 */

#include <vector>
#include <iostream>
#include <limits>
#include <algorithm>
#include <iterator>
#include <unordered_map>
#include <unordered_set>
#include <set>
#include <initializer_list>
#include <utility>

/*
 * Given a set of coins with different values, for example 1, 3, 5,
 * find the minimum number for coins that sum up to a specific amount, say 11.
 */
size_t coins_to_sum(const std::vector<size_t> & coins, size_t sum) {

  std::vector<size_t> counts(sum + 1, std::numeric_limits<size_t>::max());

  counts[0] = 0;

  for (size_t i = 1; i < counts.size(); ++i) {
    for (size_t j = 0; j < coins.size(); ++j) {
      if (coins[j] <= i && counts[i - coins[j]] + 1 < counts[i])
        counts[i] = counts[i - coins[j]] + 1;
    }
  }

  return counts[sum];
}

/*
 * Non-decreasing subsequence
 */
size_t nondecreasing_subsequence(const std::vector<int> & x) {

  std::vector<size_t> counts(x.size(), 1);

  size_t max = 1;

  for (size_t i = 1; i < x.size(); ++i) {
    if (x[i] >= x[i - 1])
      counts[i] += counts[i - 1];
    else
      counts[i] = counts[i - 1];
    if (max < counts[i])
      max = counts[i];
  }

  return max;
}

/* ***************************************************************************
 * Undirected graph shortest path
 * **************************************************************************/
template< class V >
struct graph {

public:

  using adjacent_type = std::unordered_set<V>;

  using path_type = std::set<V>;

  using initializer_list = std::initializer_list<
    std::pair< const V, adjacent_type > >;

  graph(const initializer_list & il)
    : m_data(il)
  { }

  graph(initializer_list && il)
    : m_data(il)
  { }

  inline path_type shortest_path(const V & start, const V & end) {
    path_type path;
    return shortest_path(start, end, path);
  }

private:

  using map_type = std::unordered_map<V, adjacent_type>;
  using agg_path_type = std::unordered_map<V, path_type>;
  using agg_map_type = std::unordered_map<V, agg_path_type>;


  template< class Container >
  inline bool unseen(const Container & s, const V & e) {
    return s.find(e) == s.end();
  }

  template< class T >
  std::ostream & print(const T & a, bool newline = true) {
    for (auto & e : a)
      std::cout << e << " ";
    if (newline)
      std::cout << std::endl;
    return std::cout;
  }

  path_type shortest_path(const V & start, const V & end, path_type & cpath) {

    cpath.insert(start);

    if (start == end)
      return cpath;

    if (unseen(m_data, start))
      return path_type{};

    path_type shortest;

    for (auto & v : m_data[start]) {
      if (unseen(cpath, v)) {

        path_type ccpath{ cpath };
        path_type newpath = shortest_path(v, end, ccpath);

        if (
          !newpath.empty() &&
          (shortest.empty() || newpath.size() < shortest.size())
        ) {
          shortest = std::move(newpath);
        }
      }
    }

    return std::move(shortest);
  }

  map_type m_data;
};

/* ************************************************************************* */

/* ************************************************************************* */

/*
 * Accumulate the number of max items
 */
template< class T >
struct accumulator {

  using grid_type = std::vector<size_t>;

  using initializer_list = std::initializer_list< std::initializer_list<T> >;

  explicit accumulator(const initializer_list & il, const T & item)
    : height{ il.size() }, width{ il.begin()->size() },
      value{ item },
      grid{ width * height }
  {
    init(il);
  }

  explicit accumulator(initializer_list && il, T && item)
    : height{ il.size() }, width{ il.begin()->size() },
      value{ item },
      grid{ width * height }
  {
    init(il);
  }

  const size_t & operator()(size_t i, size_t j) {
    return grid[height * i + j];
  }

  size_t max() {
    return this->operator()(height - 1, width - 1);
  }

  void print() {
    for(auto i = 0; i < height; ++i) {
      for (auto j = 0; j < width; ++j)
        std::cout << at(i, j) << " ";
      std::cout << "\n";
    }
  }

private:

  size_t & at(size_t i, size_t j) {
    return grid[width * i + j];
  }

  void init(const initializer_list & il) {

    size_t i = 1;
    size_t j;

    at(0, 0) = *il.begin()->begin() == value ? 1 : 0;

    auto it = il.begin()->begin();
    ++it;
    for ( ; it != il.begin()->end(); ++it, ++i) {
      at(0, i) = at(0, i-1);
      if (*it == value) ++at(0, i);
    }


    i = 1;

    auto row = il.begin();
    ++row;
    for ( ; row != il.end(); ++row, ++i) {
      j = 0;
      for (auto col = row->begin(); col != row->end(); ++col, ++j) {

        if (j > 0)
          at(i, j) = std::max(at(i-1, j), at(i, j-1));
        else
          at(i, j) = at(i-1, j);

        if (*col == value) ++at(i, j);

      }
    }
  }


  size_t height;
  size_t width;
  T value;
  grid_type grid;
};

/* ************************************************************************* */

int main() {

  {
    std::vector<size_t> coins{ 1, 3, 5 };
    size_t sum = 11;
    auto res = coins_to_sum(coins, sum);
    std::cout << "minimum coins summing " << sum << ": " << res << '\n';
  }

  {
    std::vector<int> x{ 5, 3, 4, 8, 6, 7 };
    auto res = nondecreasing_subsequence(x);
    std::cout << "nondecreasing subsequence length: " << res << '\n';
  }

  {
    using adjacent_type = graph<std::string>::adjacent_type;
    graph<std::string> g {
      {
        { "v1", adjacent_type{ "v2", "v3", "v4" } },
        { "v2", adjacent_type{ "v1", "v3" } },
        { "v3", adjacent_type{ "v1", "v5", "v6" } },
        { "v4", adjacent_type{ "v1" } },
      }
    };

    auto find_and_print = [&g](auto & v1, auto & v2) {
      auto path = g.shortest_path(v1, v2);
      std::cout << v1 << ':' << v2 << " => ";
      for (auto & v : path) std::cout << v << " ";
      std::cout << '\n';
    };

    find_and_print("v1", "v3");
    find_and_print("v1", "v4");
    find_and_print("v1", "v6");
    find_and_print("v2", "v6");
  }

  {

    char i = '*';
    char n = '-';

    accumulator<decltype(i)> m {
      {
        { i, n, i, n, n, n },
        { n, i, n, i, i, n },
        { i, n, n, i, n, n }
      },
      i
    };

    std::cout << m(1,1) << '\n';
    std::cout << m.max() << '\n';

    m.print();
  }

  return 0;
}
