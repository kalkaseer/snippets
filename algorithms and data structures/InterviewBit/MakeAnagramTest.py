from collections import Counter

def solve(s):

    if len(s) % 2 != 0:
        return -1
    
    mid = len(s) // 2
    s1 = s[:mid]
    s2 = s[mid:]
    
    m1 = Counter(s1)
    m2 = Counter(s2)
    
    diff = 0
    
    for c in m2.iterkeys():
        if c not in m1.iterkeys():
            diff += m2[c]
        elif m2[c] > m1[c]:
            diff += m2[c] - m1[c]
    
    return diff

testcases = [
    'aaabbb',
    'ab',
    'abc',
    'mnop',
    'xyyx'
]

testcases2 = [
    'hhpddlnnsjfoyxpciioigvjqzfbpllssuj',
    'xulkowreuowzxgnhmiqekxhzistdocbnyozmnqthhpievvlj',
    'dnqaurlplofnrtmh',
'aujteqimwfkjoqodgqaxbrkrwykpmuimqtgulojjwtukjiqrasqejbvfbixnchzsahpnyayutsgecwvcqngzoehrmeeqlgknnb',
    'lbafwuoawkxydlfcbjjtxpzpchzrvbtievqbpedlqbktorypcjkzzkodrpvosqzxmpad',
    'drngbjuuhmwqwxrinxccsqxkpwygwcdbtriwaesjsobrntzaqbe',
    'ubulzt',
    'vxxzsqjqsnibgydzlyynqcrayvwjurfsqfrivayopgrxewwruvemzy',
    'xtnipeqhxvafqaggqoanvwkmthtfirwhmjrbphlmeluvoa',
    'gqdvlchavotcykafyjzbbgmnlajiqlnwctrnvznspiwquxxsiwuldizqkkaawpyyisnftdzklwagv'
]

outputs = [
    10,
    13,
    5,
    26,
    15,
    -1,
    3,
    13,
    13,
    -1
]