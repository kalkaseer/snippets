# MaxNonNegativeSubArray
#
# https://www.interviewbit.com/problems/max-non-negative-subarray/
#
# Find out the maximum sub-array of non negative numbers from an array.
# The sub-array should be continuous. That is, a sub-array created by choosing 
# second and fourth element and skipping the third element is invalid.
# 
# Maximum sub-array is defined in terms of the sum of the elements in the
# sub-array. Sub-array A is greater than sub-array B if sum(A) > sum(B).
# 
# Example:
# 
# A : [1, 2, 5, -7, 2, 3]
# The two sub-arrays are [1, 2, 5] [2, 3].
# The answer is [1, 2, 5] as its sum is larger than [2, 3]
# 
# NOTE: If there is a tie, then compare with segment's length and return
# segment which has maximum length
# NOTE 2: If there is still a tie, then return the segment with minimum 
# starting index

# @param A : list of integers
# @return a list of integers
def solve(A):
    
    x = y = fsum = 0
    a = b = acc = 0
    i = 0
    
    while i < len(A):
        
        if A[i] >= 0:
            b += 1
            acc += A[i]
        
            if acc > fsum or (acc == fsum and b - a > y - x):
                fsum = acc
                x = a
                y = b
            
        else:
            a = b = i + 1
            acc = 0
        
        i += 1
    
    if x < y: return A[x:y]
    return []


def start():
    tests = [
        [1, 2, 5, -7, 2, 3],
        [ -846930886, -1714636915, 424238335, -1649760492 ],
        [1, 2, 5, -7, 2, 3, 4, 5],
        [1, 2, 5, -7, 1, 2, 5],
        [1, 2, 5, -7, 1, 2, 5, 0]
    ]
    
    for t in tests:
        print t, '\n=> ', solve(t)