testcases = [
    [
        [1, 1, 0, 0],
        [1, 1, 1, 0],
        [0, 1, 1, 0],
        [0, 0, 0, 1]
    ],
    [
        [1, 0, 0 ,0, 0],
        [0, 1, 0, 0, 0],
        [0, 0, 1, 0, 0],
        [0, 0, 0, 1, 0],
        [0, 0, 0, 0, 1]
    ],
    [
        [1, 0, 0, 1, 0],
        [0, 1, 1, 0, 0],
        [0, 1, 1, 0, 0],
        [1, 0, 0, 1, 0],
        [0, 0, 0, 0, 1]
    ]
]

from collections import defaultdict

def visit(graph, node, visited):
    for connected in graph[node]:
        if connected not in visited:
            visited.add(connected)
            visit(graph, connected, visited)

def solve(m):
    c = 0
    graph = defaultdict(set)
    for i in xrange(len(m)):
        graph[i].add(i)
        for j in xrange(i+1, len(m)):
            if m[i][j] == 1:
                graph[i].add(j)

    visited = set()
    for k,v in graph.iteritems():
        if k not in visited:
            c += 1
            visited.add(k)
            v.remove(k)
            for connected in v:
                visit(graph, connected, visited)
    
    return c

