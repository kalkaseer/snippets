# Max Sum Contiguous Subarray
#
# https://www.interviewbit.com/problems/max-sum-contiguous-subarray/
# https://www.youtube.com/watch?v=ohHWQf1HDfU
# Kadane's algorithm

def solve(a):
    
    # get the max sum

    max_ending_here = max_so_far = a[0]
    for x in a[1:]:
        max_ending_here = max(x, max_ending_here + x)
        max_so_far = max(max_so_far, max_ending_here)
    return max_so_far


from sys import maxint

def find_subarray(a):
    
    # get the subarray
    
    start = end = -1
    maxsum = -1
    tmpsum = 0
    tmpstart = 0
    max_so_far = -maxint - 1
    
    for i in xrange(len(a)):
        tmpsum += a[i]
        if tmpsum > max_so_far:
            max_so_far = tmpsum
            start = tmpstart
            end = i + 1
        
        if tmpsum < 0:
            tmpsum = 0
            tmpstart = i + 1
    
    return a[start:end]
