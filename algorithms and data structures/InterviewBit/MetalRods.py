def profit(price, L, cut_cost, lengths):
    p = 0;
    for x in lengths:
        p += ((x//L) * price * L) - ((x//L) * cut_cost)
    return p

def solve(unit_price, cut_cost, lengths):
    current_profit = 0
    else:
    for L in xrange(1, max(lengths)):
        p = profit(unit_price, L, cut_cost, lengths)
        if p > current_profit:
            current_profit = p
    
    return current_profit
