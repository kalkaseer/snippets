# MinStepsInInfiniteGrid
#
# https://www.interviewbit.com/problems/min-steps-in-infinite-grid/
# 
# Note that because the order of covering the points is already defined, the 
# problem just reduces to figuring out the way to calculate the distance between
# 2 points (A, B) and (C, D).
# 
# Note that what only matters is X = abs(A-C) and Y = abs(B-D).
# 
# While X and Y are positive, you will move along the diagonal and X and Y would
# both reduce by 1.
# When one of them becomes 0, you would move so that in each step the
# remaining number reduces by 1.
# 
# In other words, the total number of steps would correspond to max(X, Y).

tests = [
    [ [0, 1, 1], [0, 1, 2] ],
    [ [1, -2], [1, 7] ],
    [ [2, -7, -13], [2, 1, -5] ],
    [ [2, -7], [2, 1] ]
]

from math import sqrt

def solve(X, Y):
    steps = 0
    for i in xrange(len(X) - 1):
        steps += max(abs(X[i+1] - X[i]), abs(Y[i+1] - Y[i]))
    return steps


def start():
    for t in tests:
        print t, '\n=>', solve(t[0], t[1])