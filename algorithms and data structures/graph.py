# Author: Kareem Alkaseer 

from collections import deque
from priority_dict import priority_dict
import heapq


class Vertex(object):
    
    def __init__(self, value):
        self.value = value
    
    def __repr__(self):
        return str(self.value)

class Graph(object):
    
    def __init__(self):
        self.data = { }
        self.radius = None
        self.diameter = None
        self._lengths = { }
    
    def add(self, v, vertices=None):
        r = self.data.get(v)
        if r and vertices: 
            r.extend(vertices)
        else:
            self.data[v] = vertices if vertices else []
    
    def path(self, start, end, cpath=[]):
        cpath = cpath + [start]
        if start == end:
            return cpath
        s = self.data.get(start)
        if not s:
            return None
        for node in s:
            if node not in cpath:
                newpath = self.path(node, end, cpath)
                if newpath:
                    return newpath
        return None
    
    def all_paths(self, start, end, cpath=[]):
        print '\t'*len(cpath), start, end, ': ', 'original cpath = ', cpath
        cpath = cpath + [start]
        print '\t'*len(cpath), start, end, ': ', 'cpath = ', cpath
        if start == end:
            print '\t'*len(cpath), 'returning', [cpath]
            return [cpath]
        s = self.data.get(start)
        if not s:
            return []
        paths = []
        print '\t'*len(cpath), start, end, ': ', 'paths = []'
        for node in s:
            if node not in cpath:
                newpaths = self.all_paths(node, end, cpath)
                if newpaths: paths.extend(newpaths)
        return paths
    
    def shortest_path(self, start, end, cpath=[]):
        cpath = cpath + [start]
        if start == end:
            return cpath
        s = self.data.get(start)
        if not s:
            return None
        shortest = None
        for node in s:
            if node not in cpath:
                newpath = self.shortest_path(node, end, cpath)
                if newpath and (not shortest or len(newpath) < len(shortest)):
                    shortest = newpath
        return shortest
    
    # Time complexity: O(V + E)
    # Traverse connected vertices only
    def breadth(self, v, f=lambda x: False):

        if not self.data.has_key(v):
            return None

        visited = set([])
        q = deque()

        visited.add(v)
        q.appendleft(v)

        while len(q):
            v = q.pop()
            for w in self.data.get(v):
                if not w in visited:
                    if f(v): return
                    visited.add(w)
                    q.appendleft(w)
    
    def depth(self, v, f=lambda x: False):
    
        if not self.data.has_key(v):
            return None

        visited = set([])
    
        q = deque()
        q.append(v)
    
        while len(q):
            v = q.pop()
            if not v in visited:
                if f(v): return
                visited.add(v)
                adj = self.data.get(v)
                if not adj: continue
                for w in adj:
                    q.append(w)
    
    def direct(self, source, target):
        return 1 if target in self.data.get(source) else 0
    
    def length(self, source, target):
        p = self.shortest_path(source, target)
        return len(p) - 1 if p else 0
    
    # def lengths(self):
#         return self._lengths
#
#     def calclengths(self):
#         if not self.data:
#             self._lengths = { }
#             return self._lengths
#         self._lengths.clear()
#         for v in self.data.iterkeys():
#             for u in (x for x in self.data.iterkeys() if x != v):
#                 self._lengths[(v, u)] = self.length(u, v)
#         return self._lengths
    
    def eccentricity(self, v):
        dist = 0
        for u in [x for x in self.data.iterkeys() if x != v]:
            d = self.length(v, u)
            if d > dist: dist = d
        return dist
        
    def calcradius(self):
        if not self.data:
            self._radius = 0
            return self._radius
        it = self.data.iterkeys()
        r = self.eccentricity(next(it))
        for v in it:
            e = self.eccentricity(v)
            if e < r: r = e
        self.radius = r
        return r
    
    def calcdiameter(self):
        if not self.data:
            self.diameter = 0
            return self.diameter
        it = self.data.iterkeys()
        dia = self.eccentricity(next(it))
        for v in it:
            d = self.eccentricity(v)
            if d > dia: dia = d
        self.diameter = dia
        return dia
    
def depth(g, v, f=lambda x: False, visited=set([])):
    
    print v
    if not g.data.has_key(v):
        return None
    visited.add(v)
    
    for a in g.data.get(v):
        if f(a): return
        if not a in visited:
            visited.add(a)
            g.depth(a, f, visited)

# G: { 
#    v1: { u1: weight1, u2: weight2, ... }, ...
# }

# Dijkstra algorithm
# implementation for dense graphs and small (~ > 2000) dense/sparse graphs
def shortest_paths(g, start, end):
   
    dist = {} # distances
    prev = {} # previous vertices
    q = priority_dict() # min-heap
    
    q[start] = 0
    
    print q
    for v, adj in q.sorted_iter():
        dist[v] = adj
        if v == end: break
        
        for u in g[v]:
            print d[v], g[v][u], g
            uvdist = dist[v] + g[v][u]
            if u in dist:
                if uvdist < dist[u]:
                    raise ValueError, "already resolved"
                elif u in q or vudist < q[u]:
                    q[u] = uvdist
                    prev[u] = v
        
        return (dist, prev)

# Dijkstra algorithm for finding one shortest path
def shortest_path(g, start, end):
    dist, prev = shortest_paths(g, start, end)
    path = []
    while True:
        path.append(end)
        if end == start:
            break
        end = prev[end]
    path.reverse()
    return path

# Modified # Dijkstra algorithm
# implmenetation for large sparse graphs
def lsp_shortest_paths(g, start, end):
    def flatten(alist): # flatten a list of the form [0, [1, [2, []]]]
        while len(alist) > 0:
            yield alist[0]
            alist = alist[1]
    
    q = [(0, start, ())] # heap of (cost, path_head, path_rest)
    visited = set()
    while True:
        (cost, v, path) = heapq.heappop(q)
        if v not in visited:
            visited.add(v)
            if v == end:
                return list(flatten(path))[::01] + [v]
            path = (v, path)
            for (u, ucost) in g[v].iteritems():
                if u not in visited:
                    heapq.heappush(q, (cost + ucost, u, path))

def dijkstra(g, source):
    
    q = set([])
    dist = {}
    prev = {}
    
    for v in g.data.iterkeys():
        dist[v] = None # infinity
        prev[v] = None # undefined
        q.add(v)
    
    dist[source] = 0
    
    print 'initial q:', q
    while len(q):
        u = None
        mindist = next(iter(dist.itervalues())) # first distance
        for v in q:
            print v, mindist, dist[v]
            if (dist[v] is not None and dist[v] <= mindist) or \
            mindist is None and dist[v] is not None:
                q.remove(v)
                u = v
                print 'u removed', q
                mindist = dist[v]
                break
        adj = g.data.get(u)
        print 'adj', adj
        if adj:
            for v in adj:
                if v in q:
                    print '\t', v, g.direct(u, v)
                    alt = dist[u] + g.direct(u, v)
                    if not dist[v] or alt < dist[v]:
                        dist[v] = alt
                        prev[v] = u
        print 'q:', q
        print 'dist:', dist
    return dist, prev

def populate():
    g=Graph()

    d={
        'a': Vertex('A'),
        'b': Vertex('B'),
        'c': Vertex('C'),
        'd': Vertex('D'),
        'e': Vertex('E'),
        'f': Vertex('F'),
        'g': Vertex('G')
    }

    g.add(d['a'], [ d['b'], d['c'] ])
    g.add(d['b'], [ d['c'], d['d'] ])
    g.add(d['c'], [ d['d'] ])
    g.add(d['d'], [ d['c'] ])
    g.add(d['e'], [ d['f'] ])
    g.add(d['f'], [ d['c'] ])
    
    return g, d

def populate2():
    g=Graph()
    d= {
        0: Vertex(0),
        1: Vertex(1),
        2: Vertex(2),
        3: Vertex(3),
    }
    g.add(d[0], [ d[1], d[2] ])
    g.add(d[1], [ d[2] ])
    g.add(d[2], [ d[0], d[3] ])
    g.add(d[3], [ d[3] ])
    return g, d

def populate3():
    g=Graph()
    g.add(d['a'], [ d['b'], d['c'], d['e'] ])
    g.add(d['b'], [ d['d'], d['f'] ])
    g.add(d['c'], [ d['g'] ])
    g.add(d['e'], [ d['f'] ])
    return g

def populate4():
    g=Graph()
    g.add(d['a'], [ d['b'], d['c'], d['d'] ])
    g.add(d['b'], [ d['a'], d['e'] ])
    g.add(d['c'], [ d['a'], d['f'] ])
    g.add(d['d'], [ d['a'], d['e'], d['f'] ])
    g.add(d['e'], [ d['b'], d['d'], d['g'] ])
    g.add(d['f'], [ d['c'], d['d'], d['g'] ])
    g.add(d['g'], [ d['e'], d['f'] ])
    return g

g1, d = populate()
g2, d2 = populate2()
g3 = populate3()
g4 = populate4()

wg = {
    'a': { 'b':10, 'd':5 },
    'b': { 'c':1, 'd':2 },
    'c': { 'e':4 },
    'd': { 'b':3, 'c':9, 'e':2 },
    'e': { 'a':7, 'c':6 }
}