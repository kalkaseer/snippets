/* Author: Kareem Alkaseer */

#include <cstdint>
#include <string>
#include <utility>
#include <iostream>
#include <cmath>
#include <cfloat>
#include "timeit.h"


void reverse(std::string & s) {
	for (int i = 0, j = s.size()-1; i < j; ++i, --j)
		std::swap(s[i], s[j]);
}

template< class Iter >
static inline void reverse(Iter first, Iter last) {
	
	while (first < last) {
		*first ^= *last;
		*last  ^= *first;
		*first ^= *last;
		++first;
		--last;
	}
}

std::string itoa_1(int n) {
	
	std::string s;
	int sign;
		
	if ((sign = n) < 0) {
		n = -n;
	}
	
	do {
		s.push_back(n % 10 + '0');
	} while ((n /= 10) > 0);
		
	if (sign < 0) {
		s.push_back('-');
	}
	
	reverse(s);
	
	return s;
}

static const char itoa_alphabet[] =
	"zyxwvutsrqponmlkjihgfedcba9876543210123456789abcdefghijklmnopqrstuvwxyz";

template< class T >
char * itoa_1(char * s, T n, base b) {

	char * p = s;
	char * a = s;

	T tmpval;
	
	uint8_t bint = integer(b);

	do {
		tmpval = n;
		n /= bint;
		*p++ = itoa_alphabet[35 + (tmpval - n * bint)];
	} while (n);

	if (tmpval < 0) *p++ = '-';
	*p-- = '\0';
	
	reverse(a, p);

	return s;
}

template< class T >
char * itoa_1(char ** s, T n, base b) {
	
	*s = new char[length(n, b) + 1];
	return itoa_1(*s, n, b);
}

template< class T >
std::string itoa_2(T n, base b) {

	std::string s;
	s.reserve(length(n, b));

	std::string::value_type tmpchar;
	
	uint8_t bint = integer(b);

	T tmpval;

	do {
		tmpval = n;
		n /= bint;
		s.push_back(itoa_alphabet[35 + (tmpval - n * bint)]);
	} while (n);

	if (tmpval < 0) s.push_back('-');
	std::reverse(std::begin(s), std::end(s));

	return s;
}

template< class T >
std::string itoa_3(T n, base b) {

	std::string s;

	std::string::value_type tmpchar;
	
	uint8_t bint = integer(b);

	T tmpval;

	do {
		tmpval = n;
		n /= bint;
		s.push_back(itoa_alphabet[35 + (tmpval - n * bint)]);
	} while (n);

	if (tmpval < 0) s.push_back('-');
	std::reverse(std::begin(s), std::end(s));

	return s;
}

enum class precision : uint8_t {
	f32,
	f64,
	f80,
};

static long double quadouble_precision = 0.00000000000000000000000000000001;
static double      double_precision    = 0.00000000000000000000001;
static float       float_precision     = 0.00000000000001;

static inline long double fp_precision(precision p) {

	return p == precision::f32 ? float_precision : p == precision::f64 ?
		double_precision : quadouble_precision;
}

template< class CharT, class T >
char * ftoa(CharT ** s, T n, precision digits = precision::f32) {
	
	if (std::isnan(n)) {
	
		*s = new CharT[4];
		std::strcpy(*s, "nan");
	
	} else if (std::isinf(n)) {
		
		*s = new CharT[4];
		std::strcpy(*s, "inf");
		
	} else if (n == static_cast<T>(0.0)) {
		
		*s = new CharT[2];
		std::strcpy(*s, "0");
		
	} else {
		
		*s = new CharT[32];
		
		int32_t digit;
		int32_t m;
		int32_t k;
		
		bool neg = n < 0;
		if (neg) n = -n;
		
		CharT * p = *s;
		if (neg) *p++ = '-';
		
		m = log10(n);
		
		bool ine = m >= 14 || (neg && m >= 9) || m < -9;
		
		if (ine) {
			if (m < 0) m -= static_cast<T>(1.0);
			n = n / pow(10.0, m);
			k = m;
			m = 0;
		}
		
		if (m < 1.0) m = 0;
		
		while (n > fp_precision(digits) || m >= 0) {
			
			T weight = pow(static_cast<T>(10.0), m);
			if (weight > 0 && !std::isinf(weight)) {
				digit = floor(n / weight);
				n -= (digit * weight);
				*p++ = '0' + digit;
			}
			
			if (m == 0 && n > 0)
				*p++ = '.';
			
			--m;
		}
		
		if (ine) {
			
			int32_t i;
			int32_t j;
		
			*p++ = 'e';
			if (k > 0) {
				*p++ = '+';
			} else {
				*p++ = '-'; 
				k = -k;
			}
			
			m = 0;
			
			while (k > 0) {
				*p++ = '0' + k % 10;
				k /= 10;
				++m;
			}
			
			p -= m;
			reverse(p, p + m - 1);
			p += m;
		}
		
		*p = '\0';
	}
	
	return *s;
}

int main() {
	
	using namespace std;
	
	uint64_t n;
	std::cout << "N: ";
	std::cin >> n;
	
	char * s;
	char * s2;
	
	std::cout << "1: s: " << itoa_1(&s, n, base::decimal) << std::endl;
	std::cout << "2: s: " << itoa_2(n, base::decimal) << std::endl;
	delete [] s;
	
	std::string result;
	uint64_t nanos;
	
	std::cout << "F1: s: " << ftoa(&s, -233.18446723153234743453134) << std::endl;
	delete [] s;
	
	return 0;
}