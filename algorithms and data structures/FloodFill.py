# Flood Fill
#
# http://www.geeksforgeeks.org/flood-fill-algorithm-implement-fill-paint/

def flood_fill(matrix, x, y, prevC, newC):
  if x < 0 or x >= len(matrix) or y < 0 or y >= len(matrix[0]):
    return
  if matrix[x][y] != prevC:
    return
  matrix[x][y] = newC
  flood_fill(matrix, x+1, y, prevC, newC)
  flood_fill(matrix, x-1, y, prevC, newC)
  flood_fill(matrix, x, y+1, prevC, newC)
  flood_fill(matrix, x, y-1, prevC, newC)

def solve(matrix, x, y, newC):
  prevC = matrix[x][y]
  flood_fill(matrix, x, y, prevC, newC)
  

testcases = [
  [
    [ 1, 1, 1, 1, 1, 1, 1, 1 ],
    [ 1, 1, 1, 1, 1, 1, 0, 0 ],
    [ 1, 0, 0, 1, 1, 0, 1, 1 ],
    [ 1, 3, 3, 3, 3, 0, 1, 0 ],
    [ 1, 1, 1, 3, 3, 0, 1, 0 ],
    [ 1, 1, 1, 3, 3, 3, 3, 0 ],
    [ 1, 1, 1, 1, 1, 3, 1, 1 ],
    [ 1, 1, 1, 1, 1, 3, 3, 1 ]
  ]
]