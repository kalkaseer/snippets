# coding: utf-8
# Author: Kareem Alkaseer

import time, random

'''
Given an array of distinct integers, find length of the longest subarray
which contains numbers that can be arranged in a continuous sequence.

The important thing to note in question is, it is given that all elements
are distinct. If all elements are distinct, then a subarray has contiguous 
elements if and only if the difference between maximum and minimum elements 
in subarray is equal to the difference between last and first indexes of
subarray. So the idea is to keep track of minimum and maximum element in 
every subarray. 
'''
# Time complexity: O(n^2)
# Space complexity: O(1)
def find_contiguous(arr):
    
    maxlen = 1
    i = 0
    while i < len(arr) - 1:
        emin = emax = arr[i]
        j = i + 1
        while j < len(arr):
            emin = min((emin, arr[j]))
            emax = max((emax, arr[j]))
            if emax - emin == j - i:
                maxlen = max((maxlen, emax - emin + 1))
            j += 1
        i += 1
    return maxlen
    
'''
1 2 5 6 7
a = 1, b = 1
b = 2
len = 2, a = b = 5
b = 6
b = 7
len = 3

-3, -2, -1, 1, 2, 5, 6, 7, 8, 9, 10, 13
a = -3, b = -3
b = -3
b = -1
len = 3, a = 1, b = 1
b = 2
len = b = 5
b = 6
b = 7
b = 8
b = 9
b = 10
len = 

'''

# Time complexity: O(log n) + O(n)
# Space complexity: O(1)
def find_contiguous2(arr):
    
    arr.sort()
    a = b = arr[0]
    length = 1
    i = 1
    k = 0
    while i < len(arr):
        b = arr[i]
        if arr[i] - arr[i-1] != 1:
            length = max(length, i-k)
            k = i
            a = b
        i += 1
    if k != i: length = max(length, i-k)
    return length
    
def test_find_contiguous():
    
    arr10 = [x for x in [random.randint(1, 1000) for i in xrange(1000)]]
    arr11 = list(arr10)
    arr20 = [x for x in [random.randint(-1000, 1000) for i in xrange(1000)]]
    arr21 = list(arr20)
    
    start = time.time()
    find_contiguous(arr10)
    end = time.time();
    print 'v1 #1:', end-start
    
    start = time.time()
    find_contiguous2(arr11)
    end = time.time();
    print 'v2 #1:', end-start
    
    start = time.time()
    find_contiguous(arr20)
    end = time.time();
    print 'v1 #2:', end-start
    
    start = time.time()
    find_contiguous2(arr21)
    end = time.time();
    print 'v2 #2:', end-start

   
'''
http://www.geeksforgeeks.org/find-smallest-value-represented-sum-subset-given-array/
Given a sorted array (sorted in non-decreasing order) of positive numbers,
find the smallest positive integer value that cannot be represented as sum of
elements of any subset of given set.

Let the input array be arr[0..n-1]. We initialize the result as 1 
(smallest possible outcome) and traverse the given array. Let the smallest 
element that cannot be represented by elements at indexes from 0 to (i-1) 
be ‘b’, there are following two possibilities when we consider 
element at index i:

1) We decide that ‘b’ is the final result: If arr[i] is greater than ‘b’, 
then we found the gap which is ‘b’ because the elements after arr[i] are also
going to be greater than ‘b’.

2) The value of ‘b’ is incremented after considering arr[i]: The value of ‘b’ 
is incremented by arr[i] bcause if elements from 0 to (i-1) can represent
1 to ‘b-1′, then elements from 0 to i can represent from 1 to ‘b + arr[i] – 1′
be adding ‘arr[i]’ to all subsets that represent 1 to ‘b’) 
'''
# Time complixty: O(n)
# Space complixty: O(1)
def smallest_excluded_int(ar):
    
    b = 1
    i = 0
    while i < len(ar) and ar[i] <= b:
        b = b + ar[i]
        i += 1
    return b 


'''
http://www.geeksforgeeks.org/minimum-length-subarray-sum-greater-given-value/
Given an array of integers and a number x, find the smallest subarray with
sum greater than the given value.

Examples:
arr[] = {1, 4, 45, 6, 0, 19}
   x  =  51
Output: 3
Minimum length subarray is {4, 45, 6}

arr[] = {1, 10, 5, 2, 7}
   x  = 9
Output: 1
Minimum length subarray is {10}

arr[] = {1, 11, 100, 1, 0, 200, 3, 2, 1, 250}
    x = 280
Output: 4
Minimum length subarray is {100, 1, 0, 200}

'''
# Time complexity O(n), with average bounds better than the function
# smallest_subarray_ge2 especially for large arrays
def smallest_subarray_ge(ar, n):
    
    currsum = 0
    start = end = 0
    length = 1
    a = b = 0
    
    while end < len(ar):
        
        if currsum < n:
            while end < len(ar):

                currsum += ar[end]
                if currsum > n:
                    b = end + 1
                    end += 1
                    
                    k = end - (b + a)
                    while k < end:
                        
                        if currsum - ar[k] > n:
                            currsum -= ar[k]
                            a += 1
                        else:
                            break
                        k += 1
                    
                    break
                else:
                    end += 1
    
        k = a
        while k < end and end < len(ar):
            
            if currsum - ar[k] + ar[end] > n and currsum - ar[k] + ar[end] < currsum:
                currsum -= ar[k]
                currsum += ar[end]
                a += 1
                b = end
                k += 1
            else:
                break
        end +=1
    
            
    return a, b

# Time complexity O(n)
def smallest_subarray_ge2(ar, n):
    
    currsum = 0
    minlen = len(ar)
    start = 0
    end = 0
    
    m = k = 0
    
    while end < len(ar):
        while currsum <= n and end < len(ar):
            currsum += ar[end]
            end += 1
        while currsum > n and start < len(ar):
            if end - start < minlen:
                minlen = end - start
                m = start
                k = end
            currsum -= ar[start]
            start += 1
            
    return m, k


import heap

# Sorting the array and returning the kth element requires O(n log n) using
# efficent sorting algorithm
# A min-heap could be used (heap-select method) to extract the kth smallest
# element by calling extract_min() k times. This requires O( n + k log n).
# Using a max-heap will reduce to O( k + (n - k) log k).
class ksmallest(object):
    
    # Naive method: O(n log n)
    @staticmethod
    def naive(a, k):
        if not a: return None
        a.sort()
        return a[k - 1]
    
    # Quickselect method: average O(n) worst O(n^2)
    # Assumes distinct values
    # Pick a pivot element and move it to its sorted position within the array
    # then partition the array around it. The algorithm avoids doing a complete
    # quicksort and avoids recurring for left- and right-hand sides of the pivot
    # but only for one of them based on the position of the pivot.
    @staticmethod
    def quickselect(a, k, low = 0, high = -1):
        if not a: return None
        if high < low: high = len(a) - 1
        if k > 0 and k <= high - low + 1:
            # pivot position in sorted array
            pos = ksmallest._quickselect_parition(a, low, high)
            if pos - low == k - 1:
                return a[pos]
            if pos - low > k - 1:
                return ksmallest.quickselect(a, k, low, pos - 1)
            return ksmallest.quickselect(a, k - pos + low - 1, pos + 1, high)
            
    
    # Standard quicksort paritioning: pivot is last element, move all smaller
    # elements to the left and greater elements to the right.
    @staticmethod
    def _quickselect_parition(a, low, high):
        x = a[high]
        i = low
        j = low
        while j < high:
            if a[j] <= x:
                a[i], a[j] = a[j], a[i]
                i += 1
            j += 1
        a[i], a[high] = a[high], a[i]
        return i
    
    # Randomised quickselect method: average O(n) worst O(n^2)
    # Typically like the quickselect method but uses a pseudorandom generator
    # to generate a pivot between low and high.
    # THIS WORKS BETTER THAN BANALANCED QUICKSORT METHOD IN PRACTICAL
    # SITUATIONS BECAUSE OF THE BALANCED METHOD's LARGE AMOUNT OF CONSTANTS.
    @staticmethod
    def quickselect_random(a, k, low = 0, high = -1):
        if not a: return None
        if high < low: high = len(a) - 1
        if k > 0 and k <= high - low + 1:
            # pivot position in sorted array
            pos = ksmallest._rquickselect_parition(a, low, high)
            if pos - low == k - 1:
                return a[pos]
            if pos - low > k - 1:
                return ksmallest.quickselect_random(a, k, low, pos - 1)
            return ksmallest.quickselect_random(a, k - pos + low - 1, pos + 1, high)
            
    
    @staticmethod
    def _rquickselect_parition(a, low, high):
        pivot = random.randint(0, high - low) # [low, high]
        a[pivot + low], a[high] = a[high], a[pivot + low]
        x = a[high]
        i = low
        j = low
        while j < high:
            if a[j] <= x:
                a[i], a[j] = a[j], a[i]
                i += 1
            j += 1
        a[i], a[high] = a[high], a[i]
        return i
    
    # Balanced quickselect method: Worst O(n)
    # The algorithm core is similar to quickselect but it partitions the array
    # in a balanced way. After the parition is applied the quickselect method is
    # applied
    # The algorithm's worst-case complexity is inviting but it has a large
    # amount of constants as we shall see, so the randomised quickselect is often
    # preferred in real life.
    #
    # 1) Divide a into floor(n/5) groups, the last group may have less than
    #    5 elements.
    # 2) Sort the groups and find their medians. Create an auxiliary array to
    #    hold the floor(n/5) medians.
    # 3) Recur to find the median of medians.
    # 4) Partition a around the median of medians and mark the
    #    pivot's postition pos.
    # 5) If pos == k: return median of medians
    #    If pos <  k: return thismethod(a[ low, pos - 1], k)
    #    Else:        return thismethod(a[ pos + 1, high], k)
    @staticmethod
    def quickselect_balanced(a, k, low = 0, high = -1):
        if not a: return None
        if high < low: high = len(a) - 1
        if k <= 0 or k > high - low + 1: return None
        
        n = high - low + 1 # number of elements
        ngroups = (n + 4) / 5 # floor(n/5)
        
        # divide into floor(n/5)ceil(n/5) groups
        medians = []
        i = 0
        while i < ngroups:
            start = low + i * 5
            medians.append(ksmallest._findmedian(a[start : start + 6]))
            i += 1
        
        if i * 5 < n: # if last group contains less than 5 elements
            start = low + i * 5
            medians[i] = ksmallest._findmedian(a[start : (start%5) + 1])
        
        supermedian = medians[0] if i == 1 else \
            ksmallest.quickselect_balanced(medians, 0, i - 1, i / 2)
        
        pos = ksmallest._quickbalanced_parition(a, low, high, supermedian)
        
        if pos - low == k - 1:
            return a[pos]
        if pos - low > k - 1: # recur to the left
            return ksmallest.quickselect_balanced(a, k, low, pos - 1)
        return ksmallest.quickselect_balanced(a, k - pos + low - 1, pos + 1, high)
    
    @staticmethod
    def _findmedian(a):
        a.sort()
        return a[len(a) / 2]
    
    @staticmethod
    def _quickbalanced_parition(a, low, high, x):
        i = low
        while i < high:
            if a[i] == x: break
            i += 1
        a[i], a[high] = a[high], a[i]
        i = low
        j = low
        while j < high:
            if a[j] <= x:
                a[i], a[j] = a[j], a[i]
                i += 1
            j += 1
        a[i], a[high] = a[high], a[i]
        return i
        
    # Max-heap method: O( k + (n - k) log k)
    # 1) build max-heap of the first k elements (requires O(k))
    # 2) for each element after the kth element to the end of the array:
    #      compare the element to the max-heap root
    #      if element < root: replace root and heapify
    @staticmethod
    def maxheap(a, k):
        if not a: return None
        h = heap.maxheap(a[:k])
        i = k
        while i < len(a):
            if a[i] < h.peek():
                h.setroot(a[i])
            i += 1
        return h.peek()
    
    # Min-heap method: O( n + k log n )
    @staticmethod
    def minheap(a, k):
        if not a: return None
        h = heap.minheap(a)
        i = 0
        while i < k-1:
            h.pop()
            i += 1
        return h.peek()
    
    @staticmethod
    def test():
        a=[random.randint(0,1000000) for r in xrange(1000000)]
        a = set(a)
        a = list(a)
    
        start = time.time()
        ksmallest.naive(list(a), 500000)
        end = time.time();
        print 'naive:', end-start
        
        # start = time.time()
 #        ksmallest.quickselect(list(a), 500000)
 #        end = time.time();
 #        print 'quickselect:', end-start
    
        start = time.time()
        ksmallest.quickselect_random(list(a), 500000)
        end = time.time();
        print 'quickselect_random:', end-start
    
        # start = time.time()
#         ksmallest.quickselect_balanced(list(a), 500000)
#         end = time.time();
#         print 'quickselect_balanced:', end-start
    
        start = time.time()
        ksmallest.minheap(list(a), 500000)
        end = time.time();
        print 'minheap:', end-start
    
        start = time.time()
        ksmallest.maxheap(list(a), 500000)
        end = time.time();
        print 'maxheap:', end-start
    
    # a, b are arrays, k is a 1-index position
    # If the arrays are sorted then only the first k values are relevant
    # A modified binary search solves the problem in O(log k)
    # For each element n = a[i], 0 <= i < k look up for smallest element in a
    # and smallest element m = b[ k - a - 1], return the min(n, m).
    '''
    Sample run
    A = [1, 3, 4, 5, 9]
    B = [2, 3, 4, 5, 7, 8, 9]

    k = 2
    alen = 5
    blen = 7
    pmin = max( 0, 2-5-1= -2 ) = 0
    pmax = min( 2-1 = 1, 7-1 = 6 ) = 6

    loop:
      6 - 0 = 6:
        d = 6
        l = 3
        a = B[l = 3] = 5
        b = A[k - l + 1 = 2 - 3 + 1 = 0] = 2
        pmin = 3
        pmax = 3 - 1 = 2

      3 - 3 = 0:
        break

      return min(5,2) = 2
  
    '''
    @staticmethod
    def in_two(A, B, k):

        if k > len(A) + len(B):
            return None

        a.sort()
        b.sort()

        if k == 1:
            return min(A[0], B[0])

        if k == len(A) + len(B):
            return max(a[-1], b[-1])

        pmin = max(0, k - len(B) - 1)
        pmax = min(k - 1, len(A) - 1)

        while pmax - pmin > 0:

            d = pmin + pmax
            l = pmin + (d + 1) // 2
            a = B[l]
            b = A[l - l + 1]
            if a == b:
                break
            if a < b:
                pmin = l
            else:
                pmax = 1 - 1
        if l < len(A) - 1:
            return min(a, b)
        return max(a, B[k - l])

# Given an array and a number 'k', count the distinct pairs whose difference is
# 'k'
def kdiff(arr, diff):
    
    if not arr or diff <= 0: return None
    
    k = 0
    aset = set(arr)
    
    for n in aset:
        d = n + diff
        if d in aset:
            k += 1
    
    return k

#######################

def sum_exists(s, arr):
    aset = set([])
    for n in arr:
        diff = s - n
        if diff in aset:
            return True
        else:
            aset.add(n)
    return False

def test_sum():
    s = int(raw_input())
    run = int(raw_input())
    arr = []
    for i in xrange(run):
        arr.append(int(raw_input()))
    if sum_exists(s, arr):
        print 1
    else:
        print 0

#######################

'''
A multiset or a bag is a collection of elements that can be repeated.
Contrast with a set, where elements cannot be repeated.
Multisets can be intersected just like sets can be intersected.

Input :

A = [0,1,1,2,2,5]
B = [0,1,2,2,2,6]

Output :
A ∩ B = C = [0,1,2,2]

Input :
A = [0,1,1]
B = [0,1,2,3,4,5,6]

Output
A ∩ B = C = [0,1] 
'''

def multiset_intersect(a, b):
    
    if not a or not b: return None
    
    result = []
    i = 0
    j = 0
    
    while i < len(a) - 1 and j < len(b) - 1:
        if a[i] == b[j]:
            result.append(a[i])
            i += 1
            j += 1
        elif a[i] < b[j]:
            i += 1
        else:
            j += 1
    
    return result

#######################

def delta_encode(n1, n2):
    d = n2 - n1
    if d < -127 or d >127:
        return '-128 ' + str(d)
    else:
        return str(d)

# 25626 25757 24367 24267 16 100 2 7277
def delta_encode_test():
    numbers = raw_input().split()
    result = numbers[0]
    for i in range(1, len(numbers)):
        result = ' '.join((result, delta_encode(int(numbers[i-1]), int(numbers[i]))))
    return result