# Interval Tree
# Author: Kareem Alkaseer
#
# Optimised for interval overlap search, removal and addition of intervals
# 
# for simplicity insertion of a binary search tree is used, ideally, it should
# be like a self-balancing insertion like AVL or red-black trees. Same applies
# to deletion.
#
# Every node has [low, high] and max where max is the maximum value in the
# subtree. Nodes are ordered according to low.

# Interval search(root, x):
#
# 1. If interval x overlaps with root's interval, return root's interval
# 2. If left child of root is not nil and max in left child of root is 
#    greater than x's low value, recur into left child of root
# 3. Else recur into the right child of root
#
# https://en.wikipedia.org/wiki/Interval_tree


class Node(object):
  
  def __init__(self, low, high, left=None, right=None):
    self.low = low
    self.high = high
    self.max = high
    self.left = left
    self.right = right
  

def insert(root, low, high):
  
  if root is None:
    return Node(low, high)
  
  if low < root.low:
    root.left = insert(root.left, low, high)
  else:
    root.right = insert(root.right, low, high)
  
  if root.max < high:
    root.max = high
  
  return root


def is_overlap(lo1, hi1, lo2, hi2):
  return lo1 <= hi2 and lo2 <= hi1


def search_overlaps(root, low, high):
  
  if not root:
    return
  
  if is_overlap(root.low, root.high, low, high):
    return root.low, root.high
  
  if root.left and root.left.max >= low:
    return search_overlaps(root.left, low, high)
  
  return search_overlaps(root.right, low, high)


def all_overlaps(root, low, high, result=None):
  
  if not root:
    return
  
  if result is None:
    result = []
  
  
  r = search_overlaps(root, low, high)
  if r:
    all_overlaps(root.left, low, high, result)
    result.append((root.low, root.high))
    all_overlaps(root.right, low, high, result)
  
  return result


def inorder(root): 
  if not root:
    return
  inorder(root.left)
  print root.low, ', ', root.high, ', ', root.max
  inorder(root.right)

def test():
  intervals = [
    (15,20), (10,30), (17,19), (5,20), (12,15), (30,40)
  ]
  
  root = None
  for i in intervals:
    root = insert(root, i[0], i[1])
  
  inorder(root)
  
  x = (6,7)
  interval = search_overlaps(root, x[0], x[1])
  if interval:
    print x, 'overlaps with', interval
  else:
    print 'no overlaps'
  
  
  print 'find all overlaps'
  res = all_overlaps(root, x[0], x[1])
  print 'overlaps:', res


def test2():
  intervals = [
    (1,5), (3,5), (2,6), (10,15), (5,6), (4,100)
  ]
  
  root = None
  for i in intervals:
    root = insert(root, i[0], i[1])
    
  print 'all overlapping intervals'
  for i in intervals:
    res = all_overlaps(root, i[0], i[1])
    if res:
      print i
      print '\t', res
  