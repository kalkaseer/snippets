/* Author: Kareem Alkaseer */

#include <iostream>
#include <chrono>
#include <vector>
#include <cmath>
#include <gmpxx.h>

// Recurrsion
// Time complexity: T(n-1) + T(n-2) which is ~ O(exp(n))
// Space complexity: O(1) if function call stack is not considered, O(n) otherwise
// Taking into account integer arithmetic:
// If if two n-integers cannot be added in one instruction, time complexity
// becomes nF(n) ~ O(n * exp(n))

mpz_class fib1(unsigned int n) {
	
	mpz_class result;
	if (n <= 1) {
		result = n;
		return result;
	}
	return fib1(n-1) + fib1(n-2);
}

// Dynamic programming
// Time complexity: O(n)
// Space complexity: O(n)
// Taking into account integer arithmetic:
// If if two n-integers cannot be added in one instruction, time complexity
// becomes nF(n) ~ O(n * n) = O(n^2) which is still polynomial
// for m-integers: mFn ~ O(m * n), the nth Fibinacci number is bit-length 0.694n

mpz_class fib2(unsigned int n) {
	
	std::vector<mpz_class> f;
	f.reserve(n + 1);
	f.emplace_back(0);
	f.emplace_back(1);

	for (unsigned int i = 2; i <= n; ++i)
		f.emplace_back(f[i - 1] + f[ i - 2]);
	
	return f[n];
}

// Space optimised dynamic programming
// We only need to store the previous two numbers instead of the whole array.
// Space complexity: O(1)
// In real life, this is much faster

mpz_class fib3(unsigned int n) {
	
	mpz_class a = 0, b = 1, c;
	
	if (n == 0) return 0;
	
	for (unsigned int i = 2; i <= n; ++i) {
		c = a + b;
		a = b;
		b = c;
	}
	
	return b;
}

// The closed form of a Fibonacci number is
//       1 1      F_n+1 F_n
// power 1 0, n = F_n   F_n-1

// 2x2 matrix.
struct F {
	
	mpz_class r0[2] = {1, 1};
	mpz_class r1[2] = {1, 0};
	
	void multiply(F & m) {
	
		mpz_class a = r0[0] * m.r0[0] + r0[1] * m.r1[0];
		mpz_class b = r0[0] * m.r0[1] + r0[1] * m.r1[1];
		mpz_class c = r1[0] * m.r0[0] + r1[1] * m.r1[0];
		mpz_class d = r1[0] * m.r0[1] + r1[1] * m.r1[1];
	
		r0[0] = a;
		r0[1] = b;
		r1[0] = c;
		r1[1] = d;
	}
	
	void multiply(double d) {
		r0[0] *= d;
		r0[1] *= d;
		r1[0] *= d;
		r1[1] *= d;
	}
	
	void add(F & m) {
		r0[0] += m.r0[0];
		r0[1] += m.r0[1];
		r1[0] += m.r1[0];
		r1[1] += m.r1[1];
	}
};

inline F & operator* (F & f, F & m) {
	f.multiply(m);
	return f;
}

inline F & operator* (F & f, double d) {
	f.multiply(d);
	return f;
}

inline F & operator* (double d, F & f) {
	f.multiply(d);
	return f;
}

inline F & operator+ (F & f, F & m) {
	f.add(m);
	return f;
}

inline std::ostream & operator<< (std::ostream & os, F & f) {
	os << std::endl;
	os << f.r0[0] << " " << f.r0[1] << std::endl;
	os << f.r1[0] << " " << f.r1[1] << std::endl;
	os << std::endl;
	return os;
}

// Raise 2x2 matrix to power n
void power(F & f, unsigned int n) {
	
	F m;
	for (unsigned int i = 2; i <= n; ++i) {
		f.multiply(m);
	}
}

// Time complexity O(n) or O(m * n) for the general case of m-integers
// Space Complexity: O(1)
// Because of the naive way we do matrix multiplication, on average,
// this is slower that fib3
mpz_class fib4(unsigned int n) {
	
	if (n == 0) return 0;
	
	F f;
	power(f, n - 1);
	return f.r0[0];
}


// Optimised version of power: raise 2x2 matrix to power n
// Uses divide and conquer for achieving O(log n) time complexity.
void power_optimized(F & f, unsigned int n) {
	
	if (n <= 1) return;
	power(f, n/2);
	f.multiply(f);
	if (n % 2 != 0) {
		F m;
		f.multiply(m);
	}
}

// Time complexity O(log n) or O(m * log n) for the general case of m-integers
// Space Complexity: O(1)
mpz_class fib5(unsigned int n) {
	
	if (n == 0) return 0;
	
	F f;
	power_optimized(f, n - 1);
	return f.r0[0];
}

// Using eigenvalues to calculate the nth power.
void multiply_optimized(F & f, unsigned int n) {
	F i = F();
	f.r0[0] = 1; f.r0[1] = 0;
	f.r1[0] = 0; f.r1[1] = 1;
	
	double l1 = 1.618034;
	double l2 = -0.618034;
	double diff = l2 - l1;
	
	( (pow(l2, n) - pow(l1, n)) / diff) * f +
		( ( (l2 * pow(l1, n)) - (l1 * pow(l2, n)) ) / diff ) * i;
}

// This is best of large number until a floating point exception occurs.
mpz_class fib6(unsigned int n) {
	
	if (n == 0) return 0;
	
	F f;
	multiply_optimized(f, n);
	return f.r1[1];
}

int main() {
	
	using namespace std;
	using namespace std::chrono;
	
	int n;
	mpz_class result;
	
	cout << "N: ";
	cin >> n;
	
	auto start = high_resolution_clock::now();
	//result = fib1(n);
	auto end = high_resolution_clock::now();
	auto diff = end - start;
	//cout << "1: F(n) = " << result << " time: " << diff.count() << endl;
	
	start = high_resolution_clock::now();
	result = fib2(n);
	end = high_resolution_clock::now();
	diff = end - start;
	cout << "2: F(n) = " << result << " time: " << diff.count() << endl;
	
	start = high_resolution_clock::now();
	result = fib3(n);
	end = high_resolution_clock::now();
	diff = end - start;
	cout << "3: F(n) = " << result << " time: " << diff.count() << endl;

	start = high_resolution_clock::now();
	result = fib4(n);
	end = high_resolution_clock::now();
	diff = end - start;
	cout << "4: F(n) = " << result << " time: " << diff.count() << endl;

	start = high_resolution_clock::now();
	result = fib5(n);
	end = high_resolution_clock::now();
	diff = end - start;
	cout << "5: F(n) = " << result << " time: " << diff.count() << endl;

	start = high_resolution_clock::now();
	result = fib6(n);
	end = high_resolution_clock::now();
	diff = end - start;
	cout << "6: F(n) = " << result << " time: " << diff.count() << endl;
	
	return 0;
}