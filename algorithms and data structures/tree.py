# coding: utf-8

from collections import deque

class BinaryNode(object):
    
    def __init__(self, data):
        self._data   = data
        self._right  = None
        self._left   = None
        self._parent = None
    
    @property
    def data(self):
        return self._data
    
    @property
    def data(self, value):
        self._data = data
    
    @property
    def right(self):
        return self._right
    
    @property
    def right(self, data):
        self._right = BinaryNode(data)
        return self.right
    
    def set_right(self, node):
        self._rigght = node
        return self.right
    
    @property
    def left(self):
        return self._left
    
    @property
    def left(self, data):
        self._left = BinaryNode(data)
        return self.left
    
    def set_left(self, node):
        self._left = node
        return self.left
    
    @property
    def parent(self):
        return self._parent
    
    @property
    def parent(self, node):
        self._parent = node
        return self.parent


class Tree(object):
    
    def __init__(self):
        self._root = None
    
    @property
    def root(self):
        return self._root
    
    @property
    def root(self, data):
        self._root = BinaryNode(data)
        return self.root
    
    # insert level order
    def insert(self, values, node = None):
    
        q = deque()
        if not node:
            self.root(next(values))
            node = self.root
    
        q.append(node)
    
        for data in values:
            parent = q.popleft()
            if parent.left:
                parent.left.data = data
            else:
                parent.left = data
                parent.left.parent = parent
            q.append(parent.left)
        
            try:
                if parent.right:
                    parent.right.data = next(value)
                else:
                    parent.right = next(value)
                    parent.right.parent = parent
                q.append(parent.right)
            except:
                break

    def find(self, data, useroot=True):
    
        if not node and useroot: node = self._root
        if not node: return None
    
        if node.data == data: return node
        n =  self.find(node.left, data, False) 
        return n if n else self.find(node.right, data, False)
    
    def remove(self, node=None, data=None, remove_successor=None):
        
        if not node and data:
            node = self.find(data)
        
        if not node: return
        
        is_left = True
        if node.parent:
            if node == node.parent.right:
                is_left = False
        
        if not node.right and not node.left:
            if node.parent:
                self._remove_from_parent(node.parent, is_left)
        else:
            if node.left: self.remove(node.left)
            if node.right: self.remove(node.right)
            if node.parent: self._remove_from_parent(node.parent, is_left)
    
    def _remove_from_parent(self, parent, is_left):
        if is_left:
            parent.set_left(None)
        else:
            parent.set_right(None)
    
    def size(self, node = None):
        """
        Number of nodes in a tree starting at node.
        """
        if not node: node = self._root
        if not node:
            return 0
        else:
            return self.size(node.left) + 1 + size(node.right)
    
    def height(node = None):
        if not node: node = self._root
        if node:
            lh = height(node.left)
            rh = height(node.right)
            return lh + 1 if lh > rh else rh + 1
        return 0
    
    def max_depth(self):
        """
        Number of nodes along the longest path from the root down to the
        furthest leaf.
        """
        
        if not node: return 0
        
        ldepth = max_depth(node.left)
        rdepth = max_depth(node.right)
        if ldepth > rdepth: return ldepth + 1
        return rdepth + 1
    
    def preorder(self, visit, node = None):
        
        if not node: node = self._root
        # visit(current); preorder(left); preorder(right)
        s = []
        current = node
        while s or current:
            if current:
                visit(current)
                if current.right:
                    s.append(current.right)
                current = current.left
            else:
                current = s.pop()
    
    def inorder(self, visit, node = None):
        
        if not node: node = self._root
        
        # inorder(left); visit(current); preorder(right)
        s = []
        current = node
        while s or current:
            if current:
                s.append(current)
                current = current.left
            else:
                current = s.pop()
                visit(current)
                current = current(right)
    
    def postorder(self, visit, node = None):
        
        if not node: node = self._root
        
        # postorder(left); postorder(right); visit(current)
        s = []
        last = None
        peel = None
        current = node
        while s or current:
            if current:
                s.append(current)
                current = current.left
            else:
                peek = s[len(s) - 1]
                if peek.right and last != peek.right:
                    current = peek.right
                else:
                    visit(peek)
                    last = s.pop()
    
    def levelorder(self, visit, node = None):
        
        if not node: node = self._root
        
        if node:
            current = node
            q = deque()
            q.append(current)
            while q:
                current = q.popleft()
                visit(current)
                if current.left:
                    q.append(current.left)
                if current.right:
                    q.append(current.right)
    
    # Given a tree and a sum, return true if there is a path from the root
    # down to a leaf, such that adding up all the values along the path
    # equals the given sum.
    #
    # Strategy: subtract the node value from the sum when recurring down,
    # and check to see if the sum is 0 when you run out of tree.
    
    def has_sum(self, sum, node = None):
        
        if not node: node = self._root
        
        if not node:
            return None
        
        diff = sum - node.data
        return has_sum(node.left, diff) or has_sum(node.right, diff)
    
    def print_paths(self, node = None):
        if not node: node = self._root
        data = []
        self._print_paths(data, node)
    
    def print_paths(self, data, node = None):
        
        if not node: node = self._root
        if not node: return None
        data.append(node.data)
        
        if not node.left and not node.right:
            print data
            data.clear()
        else:
            self._print_paths(node.left, data)
            self._print_paths(node.right, data)
    
    # Inefficient
    # Useful if there are no known (aribtrary) max and min values.
    # Requires data to implement: operators > and <
    def is_bst(self, node = None):

        if not node: node = self._root
        if not node: return True

        if node.left and self.min_value(node.left) > node.data:
            return False

        if node.right and self.max_value(node.right) <= node.data:
            return False

        if not selfis_bst(node.left) or not self.is_bst(node.right):
            return False

        return True
    
    # Efficent for aribtrary known bounds such as INT_MIN and INT_MAX.
    # Requires data to implement:
    # operators > and <
    # operator + or provide 'incerment'
    def is_bst_with_bounds(self, min, max, node = None, increment=None):
        if not node: node = self._root
        if not node: return True
        if node.data < min or node.data > max:
            return False
        if not increment:
            return \
                self.is_bst_with_bounds(node.left, min, node.data) and \
                self.is_bst_with_bounds(node.right, node.data + 1, max)
        else:
            return \
                self.is_bst_with_bounds(node.left, min, node.data) and \
                self.is_bst_with_bounds(node.right, increment(node.data), max)
    

class BST(Tree):
    
    def __init__(self):
        super(BST, self).__init__()
        self._delete_successor = True
    
    def insert(self, data):
        
        if not self.root:
            self.root = data
        
        node = self.root
        while node:
            if data < node.data:
                if not node.left:
                    node.left = data
                    return
                else:
                    node = node.left
            else:
                if not node.right:
                    node.right = data
                    return
                else:
                    node = node.right
    
    def find(self, data, node=None):
        
        if not node:
            node = self.root
            
        while node:
            if node.data == value:
                return node
            elif node.data > value:
                node = node.left
            else:
                node = node.right
        
        return None
    
    def remove(self, node=None, data=None, remove_successor=None):
        
        if data: node = self.find(data)
        if not node: return
        if not remove_successor:
            remove_successor = self._delete_successor
        data = node.data
        self._remove(node, data, remove_successor)
    
    def _remove(self, node, data, remove_successor):
        if data < node.data:
            if node.left:
                self._remove(node.left, data, not remove_successor)
        elif data > node.data:
            if node.right:
                self._remove(node.right, data, not remove_successor)
        else:
            if node.left and node.right:
                if remove_successor:
                    xnode = self.min_value(node.right)
                else:
                    xnode = self.min_value(node.left)
                node.data = xnode.data
                self._remove(xnode, xnode.data, not remove_successor)
            
            elif node.left:
                self._replace_parent(node, node.left)
            elif node.right:
                self._replace_parent(node, node.right)
            else:
                self._replace_parent(node, None)
    
    def _replace_parent(self, parent, child=None):
        
        if parent.parent:
            if parent == parent.parent.left:
                parent.parent.left = child
            else:
                parent.parent.right = child
        if node:
            child.parent = parent.parent
    
    def min_value(self, node = None):
        """
        Minimum data value in the tree.
        """
        
        if not node: node = self._root
        if not node: return None
        
        current = node
        while current.left:
            current = current.left
        return current.data
    
    def max_value(self, node = None):
        
        if not node: node = self._root
        if not node: return None
        
        current = node
        while current.right:
            current = current.right
        return current.data
        
    
# Checks if two trees are structurally identical.
def same_tree(node1, node2):
    
    if not node1 and node2:
        return True
    elif node1 and node2:
        return node1.data == node2.data and \
            same_tree(node1.left, node2.left) and \
            same_tree(node1.right, node2.right)
    else:
        return False

# For the key values 1...numKeys, how many structurally unique
# binary search trees are possible that store those keys.
# Strategy: consider that each value could be the root.
# Recursively find the size of the left and right subtrees.

def count_trees(num_keys):
    
    if num_keys <= 1: return 1
    
    sum = 0
    root = 1
    
    while root <= num_key:
        left = count_trees(root - 1)
        right = count_trees(num_keys -root)
        
        sum += left * right
    
    return sum
    
'''
 Change a tree so that the roles of the
 left and right pointers are swapped at every node.
 So the tree...
       4
      / \
     2   5
    / \ 
   1   3
 is changed to...
       4
      / \
     5   2
        / \ 
       3   1
'''
def mirror(node):
    
    if not node: return
    
    # mirror sub-trees
    mirror(node.left)
    mirror(node.right)
    
    # swap this node's children
    temp = node.left
    node.left = node.right
    node.right = temp

'''
For each node in a binary search tree,
create a new duplicate node, and insert
the duplicate as the left child of the original node.
The resulting tree should still be a binary search tree.
 
So the tree...

   2
  / \
 1   3

Is changed to...

       2
      / \
     2   3
    /   /
   1   3
  /
 1 
'''
def double_tree(node):
    
    if not node: return
    
    double_tree(node.left)
    double_tree(node.right)
    
    # duplicate this node's children to its left
    old_left = node.left
    node.left = node.data
    node.left.set_left(old_left)