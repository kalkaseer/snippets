/* Copyright © Kareem Alkaseer 2016 */

#include <stack>
#include <iostream>
#include <chrono>

using namespace std;
using namespace std::chrono;

bool matches(const char open, const char close) {
	switch (open) {
		case '(': return close == ')';
		case '[': return close == ']';
		case '{': return close == '}';
		default: return false;
	}
}

bool is_balanced1(const string & s) {
	stack<char> k;
	for(const char ch : s) {
		switch(ch) {
			case '(':
			case '[':
			case '{':
				k.push(ch);
				break;
			case ')':
			case ']':
			case '}':
				if (k.empty() || !matches(k.top(), ch))
					return false;
				else
					k.pop();
		}
	}
	return k.empty();
}

// Much faster but wouldn't detect irregular patterns
bool is_balanced2(const string & s) {
	int k = 0;
	for (const char ch : s) {
		switch(ch) {
			case '(':
			case '[':
			case '{':
				++k;
				break;
			case ')':
			case ']':
			case '}':
				--k;
		}
	}
	return k == 0;
}

int main() {
	string experssion;
	cout << "Enter an experssion: ";
	cin >> experssion;
	
	high_resolution_clock::time_point t1 = high_resolution_clock::now();
	bool result = is_balanced1(experssion);
	high_resolution_clock::time_point t2 = high_resolution_clock::now();
	
	auto duration = duration_cast<microseconds>(t2 - t1).count();
	cout << "#1 " << result << " duration " << duration << " microseconds\n";
	
	t1 = high_resolution_clock::now();
	is_balanced2(experssion);
	t2 = high_resolution_clock::now();
	
	duration = duration_cast<microseconds>(t2 - t1).count();
	cout << "#2 " << result << " duration " << duration << " microseconds\n";
	
	return 0;
}