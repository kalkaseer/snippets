# Reverse a "normal" characters in a string without affecting spcial characters
# Algorithm:
# s: a string
# n: length of s
# l: 0
# r: n - 1
# while l < r:
#   if s[l] is not normal: l++
#   else if s[r] is not normal: r--
#   else:
#      swap(s[l], s[r])
#      l++
#      r--

def reverse_normal(s, stringize=True, is_special = lambda x: not x.isalpha()):
    
    alist = [x for x in s]
    l = 0
    r = len(alist) - 1
    
    while l < r:
        if is_special(alist[l]): l += 1
        elif is_special(alist[r]): r -= 1
        else:
            alist[l], alist[r] = alist[r], alist[l]
            l += 1
            r -= 1
    
    return ''.join(alist) if stringize else alist

#######################

def is_anagram(s1, s2):
    c1 = [0] * 26
    c2 = [0] * 26
    
    for i in range(len(s1)):
        if (s1[i]) == ' ': continue
        pos = ord(s1[i]) - ord('a')
        c1[pos] = c1[pos] + 1
    
    for i in range(len(s2)):
        if (s2[i]) == ' ': continue
        pos = ord(s2[i]) - ord('a')
        c2[pos] = c2[pos] + 1
    
    j = 0
    ok = True
    
    while j < 26 and ok:
        if c1[j] == c2[j]:
            j += 1
        else:
            ok = False
    return ok

'''
words = [
    'pear',
    'amleth',
    'reap',
    'dormitory',
    'tinsel',
    'dirty room',
    'hamlet',
    'listen',
    'silent',
]
'''

def anagram_test():
    result = []
    stoplist = set([])
    words = []
    while True:
        try:
            words.append(raw_input())
        except EOFError:
            break;
    for w1 in words:
        if w1 in stoplist: continue
        stoplist.add(w1)
        alist = [w1]
        for w2 in words:
            if w2 not in alist:
                if is_anagram(w1, w2):
                    alist.append(w2)
                    stoplist.add(w2)
        result.append(alist)
    for r in result:
        r.sort()
    for r in result:
        s = r[0]
        for i in range(1, len(r)):
            s = ''.join((s, ',', r[i]))
        print s

#######################

# input: Featuring stylish rooms and moorings for recreation boats, Room Mate
# Aitana is a designer hotel built in 2013 on an island in the IJ River ...

# output: Featuring stylish rooms and (max n without broken words)

from itertools import islice

def smart_substr(s, n = 30):
    ch = None
    while ch != ' ' and n > 0:
        n -= 1
        ch = s[n]
    if n == 0:
        return None
    ls = s[:n]
    ls = ls.split(' ')
    sls = islice(ls, 0, len(ls))
    result = sls.next()
    for v in sls:
        result = ''.join((result, ' ', v))
    return result