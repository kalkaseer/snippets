/*
 * open_addressed_hashtable.c
 *
 *  Created on: May 15, 2016
 *      Author: kalkaseer
 */

#include <stdlib.h>
#include <string.h>
#include "open_addressed_hashtable.h"

/* Sentinel memory address for removed elements */
static char vacated;

struct open_hashtable_t {
  size_t       positions;
  size_t       size;
  void       * vacated;
  hash_fn      h1;
  hash_fn      h2;
  match_fn     match;
  destroy_fn   destroy;
  void      ** data;
};

open_hashtable_ptr open_hashtable_new() {
  return (open_hashtable_ptr) malloc(sizeof(open_hashtable));
}

bool open_hashtable_init(
  open_hashtable_ptr table, size_t positions, hash_fn h1, hash_fn h2,
  match_fn match, destroy_fn destroy
) {
  if ((table->data = (void **) calloc(positions, sizeof(void *))) == NULL)
    return false;
  table->positions = positions;
  table->size = 0;
  table->vacated = &vacated;
  table->h1 = h1;
  table->h2 = h2;
  table->match = match;
  table->destroy = destroy;
  return true;
}

void open_hashtable_destroy(open_hashtable_ptr table) {
  size_t i;
  if (table->destroy != NULL) {
    for (i = 0; i < table->positions; ++i)
      if (table->data[i] != NULL && table->data[i] != table->vacated)
        table->destroy(table->data[i]);
  }
  free(table->data);
  memset(table, 0, sizeof(open_hashtable));
}

bool open_hashtable_insert(open_hashtable_ptr table, data_ptr data) {
  data_writable tmp;
  size_t pos;
  size_t i;

  if (table->size == table->positions)
    return false;

  tmp = (data_writable) data;
  if (open_hashtable_get(table, &tmp))
    return true;

  /* use double hashing */
  for (i = 0;i < table->positions; ++i) {
    pos = (table->h1(data) + (i * table->h2(data))) % table->positions;
    if (table->data[pos] == NULL || table->data[pos] == table->vacated) {
      table->data[pos] = (data_writable) data;
      ++(table->size);
      return true;
    }
  }

  return false;
}

bool open_hashtable_remove(open_hashtable_ptr table, data_sink data) {
  size_t pos, i;
  for (i = 0; i < table->positions; ++i) {
    pos = (table->h1(*data) + (i * table->h2(*data))) % table->positions;
    if (table->data[pos] == NULL) {
      return false;
    } else if (table->data[pos] == table->vacated) {
      continue;
    } else if (table->match(table->data[pos], *data)) {
      *data = table->data[pos];
      table->data[pos] = table->vacated;
      --(table->size);
      return true;
    }
  }
  return false;
}

bool open_hashtable_get(const open_hashtable_ptr table, data_sink data) {
  size_t pos, i;
  for (i = 0; i < table->positions; ++i) {
    pos = (table->h1(*data) + (i * table->h2(*data))) % table->positions;
    if (table->data[pos] == NULL) {
      return false;
    } else if (table->match(table->data[pos], *data)) {
      *data = table->data[pos];
      return true;
    }
  }
  return false;
}

size_t open_hashtable_size(const open_hashtable_ptr table) {
  return table->size;
}
