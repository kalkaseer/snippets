/*
 * binary_tree.c
 *
 *  Created on: May 15, 2016
 *      Author: kalkaseer
 */

#include <stdlib.h>
#include <string.h>
#include "binary_tree.h"
#include "private/binary_tree_private.h"
#include "stack.h"
#include "queue.h"

bitree_ptr bitree_new() {
  return (bitree_ptr) malloc(sizeof(bitree));
}

void bitree_init(bitree_ptr tree, destroy_fn destroy) {
  tree->size = 0;
  tree->destroy = destroy;
  tree->root = NULL;
}

void bitree_destroy(bitree_ptr tree) {
  bitree_remove_left(tree, NULL);
  memset(tree, 0, sizeof(bitree));
}

bool bitree_insert_left(bitree_ptr tree, bitree_node_ptr node, data_ptr data) {
  bitree_node_ptr new_node;
  bitree_node_ptr * pos;

  if (node == NULL) {
    if (tree->size > 0)
      return false;
    pos = &tree->root;
  } else {
    if (node->left != NULL)
      return false;
    pos = &node->left;
  }

  if ((new_node = (bitree_node_ptr) malloc(sizeof(bitree_node))) == NULL)
    return false;

  new_node->data = (data_writable) data;
  new_node->left = NULL;
  new_node->right = NULL;
  *pos = new_node;

  ++(tree->size);
  return true;
}

bool bitree_insert_right(bitree_ptr tree, bitree_node_ptr node, data_ptr data) {
  bitree_node_ptr new_node;
  bitree_node_ptr * pos;

  if (node == NULL) {
    if (tree->size > 0)
      return false;
    pos = &tree->root;
  } else {
    if (node->right != NULL)
      return false;
    pos = &node->right;
  }

  if ((new_node = (bitree_node_ptr) malloc(sizeof(bitree_node))) == NULL)
    return false;

  new_node->data = (data_writable) data;
  new_node->left = NULL;
  new_node->right = NULL;
  *pos = new_node;

  ++(tree->size);
  return true;
}

void bitree_remove_left(bitree_ptr tree, bitree_node_ptr node) {
  bitree_node_ptr * pos;
  if (tree->size == 0)
    return;

  pos = node == NULL ? pos = &tree->root : &node->left;

  if (*pos != NULL) {
    bitree_remove_left(tree, *pos);
    bitree_remove_right(tree, *pos);
    if (tree->destroy != NULL) {
      tree->destroy((*pos)->data);
    }

    free(*pos);
    *pos = NULL;
    --(tree->size);
  }
}

void bitree_remove_right(bitree_ptr tree, bitree_node_ptr node) {
  bitree_node_ptr * pos;
  if (tree->size == 0)
    return;

  pos = node == NULL ? pos = &tree->root : &node->right;

  if (*pos != NULL) {
    bitree_remove_left(tree, *pos);
    bitree_remove_right(tree, *pos);
    if (tree->destroy != NULL) {
      tree->destroy((*pos)->data);
    }

    free(*pos);
    *pos = NULL;
    --(tree->size);
  }
}

bool bitree_merge(
  bitree_ptr merge, bitree_ptr left, bitree_ptr right, data_ptr data
) {
  bitree_init(merge, left->destroy);
  if (! bitree_insert_left(merge, NULL, data)) {
    bitree_destroy(merge);
    return false;
  }

  bitree_root(merge)->left = bitree_root(left);
  bitree_root(merge)->right = bitree_root(right);

  merge->size = merge->size + left->size + right->size;

  left->root = NULL;
  left->size = 0;
  right->root = NULL;
  right->size = 0;

  return true;
}

size_t bitree_size(bitree_ptr tree) {
  return tree->size;
}

bitree_node_ptr bitree_root(bitree_ptr tree);

bool bitree_is_leaf(bitree_node_ptr node) {
  return node->left == NULL && node->right == NULL;
}

bool bitree_is_eob(bitree_node_ptr node) {
  return node == NULL;
}

data_writable bitree_data(bitree_node_ptr node) {
  return node->data;
}

bitree_node_ptr bitree_left(bitree_node_ptr node) {
  return node->left;
}

bitree_node_ptr bitree_right(bitree_node_ptr node) {
  return node->right;
}

bool bitree_preorder(bitree_node_ptr node, visit_bitree_fn visit) {
  linked_stack_ptr s = linked_stack_new();
  if (s == NULL)
    return false;
  linked_stack_init(s, NULL);
  while (linked_stack_size(s) != 0 || node != NULL) {
    if (node) {
      visit(node);
      if (node->right)
        linked_stack_push(s, node->right);
      node = node->left;
    } else {
      linked_stack_pop(s, (data_sink) &node);
    }
  }
  linked_stack_destroy(s);
  free(s);
  return true;
}

bool bitree_inorder(bitree_node_ptr node, visit_bitree_fn visit) {
  linked_stack_ptr s = linked_stack_new();
  if (s == NULL)
    return false;
  linked_stack_init(s, NULL);
  while (linked_stack_size(s) != 0 || node != NULL) {
    if (node) {
      linked_stack_push(s, node);
      node = node->left;
    } else {
      linked_stack_pop(s, (data_sink) &node);
      visit(node);
      node = node->right;
    }
  }
  linked_stack_destroy(s);
  free(s);
  return true;
}

bool bitree_postorder(bitree_node_ptr node, visit_bitree_fn visit) {
  bitree_node_ptr peek = NULL;
  bitree_node_ptr last = NULL;
  linked_stack_ptr s = linked_stack_new();
  if (s == NULL)
    return false;
  linked_stack_init(s, NULL);
  while (linked_stack_size(s) != 0 || node != NULL) {
    if (node) {
      linked_stack_push(s, node);
      node = node->left;
    } else {
      peek = (bitree_node_ptr) linked_stack_peek(s);
      if (peek->right && last != peek->right) {
        node = peek->right;
      } else {
        visit(peek);
        linked_stack_pop(s, (data_sink) &last);
      }
    }
  }
  linked_stack_destroy(s);
  free(s);
  return true;
}

bool bitree_levelorder(bitree_node_ptr node, visit_bitree_fn visit) {
  queue_ptr q = queue_new();
  if (q == NULL)
    return false;
  queue_init(q, NULL);
  queue_push(q, node);
  while (queue_size(q) != 0) {
    queue_pop(q, (data_sink) &node);
    visit(node);
    if (node->left)
      queue_push(q, node->left);
    if (node->right)
      queue_push(q, node->right);
  }
  queue_destroy(q);
  free(q);
  return true;
}

void bitree_traverse_preorder(bitree_node_ptr node, visit_bitree_fn visit) {
  visit(node);
  if (node->left) bitree_preorder(node->left, visit);
  if (node->right) bitree_preorder(node->right, visit);
}

void bitree_traverse_inroder(bitree_node_ptr node, visit_bitree_fn visit) {
  if (node->left) bitree_preorder(node->left, visit);
  visit(node);
  if (node->right) bitree_preorder(node->right, visit);
}

void bitree_traverse_postorder(bitree_node_ptr node, visit_bitree_fn visit) {
  if (node->left) bitree_preorder(node->left, visit);
  if (node->right) bitree_preorder(node->right, visit);
  visit(node);
}

void bitree_traverse_levelorder(bitree_node_ptr node, visit_bitree_fn visit) {
  size_t h = bitree_height_at_node(node);
  size_t i;
  for (i = 0; i <= h; ++i)
    bitree_traverse_level(node, i, visit);
}

void bitree_traverse_level(
  bitree_node_ptr node, size_t level, visit_bitree_fn visit
) {
  if (node) {
    if (level == 0) {
      visit(node);
    } else {
      bitree_traverse_level(node->left, level - 1, visit);
      bitree_traverse_level(node->right, level - 1, visit);
    }
  }
}

size_t bitree_height_at_node(bitree_node_ptr node) {
  size_t lh = bitree_height_at_node(node->left);
  size_t rh = bitree_height_at_node(node->right);
  return lh > rh ? ++lh : ++rh;
}

size_t bitree_height(bitree_ptr tree) {
  return bitree_height_at_node(tree->root);
}



