/*
 * chained_hashtable.c
 *
 *  Created on: May 5, 2016
 *      Author: kalkaseer
 */

/* uncomment next to disable assertions */
/* #define NDEBUG */
#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include "chained_hashtable.h"
#include "list.h"
#include "private/list_private.h"

struct hashtable_t {
  size_t     buckets;
  size_t     size;
  hash_fn    hash;
  match_fn   match;
  destroy_fn destroy;
  list_ptr   table;
};

hashtable_ptr htable_new() {
  return (hashtable_ptr) malloc(sizeof(hashtable));
}

/* Complexity O(m) m: is the number of buckets */
bool htable_init(
  hashtable_ptr t, size_t buckets,
  hash_fn hash, match_fn match, destroy_fn destroy
) {
  assert(buckets > 0);
  size_t i;
  if ((t->table = (list_ptr) malloc(buckets * sizeof(list))) == NULL)
    return false;
  t->buckets = buckets;
  t->hash = hash;
  t->match = match;
  t->destroy = destroy;
  t->size = 0;
  for (i = 0; i < buckets; ++i)
    list_init(&t->table[i], destroy);
  return true;
}

/* Complexity O(m) m: is the number of buckets */
void htable_destroy(hashtable_ptr t) {

  for (size_t i = 0; i < t->buckets; ++i)
    list_destroy(&t->table[i]);
  free(t->table);
  memset(t, 0, sizeof(hashtable));
}

/* Complexity O(1) */
bool htable_insert(hashtable_ptr t, data_ptr data) {

  data_writable tmp = (data_writable) data;
  size_t bucket;

  if (htable_get(t, &tmp))
    return true;
  bucket = t->hash(data) & (t->buckets - 1); /* m % n */

  if (list_insert(&t->table[bucket], NULL, data)) {
    ++(t->size);
    return true;
  }

  return false;
}

/* Complexity O(1) */
bool htable_remove(hashtable_ptr t, data_sink data) {

  list_node_ptr node;
  list_node_ptr prev = NULL;

  size_t bucket = t->hash(*data) & (t->buckets - 1); /* m % n */

  for (
    node = list_head(&t->table[bucket]);
    node != NULL;
    node = list_next(node)
  ) {
    if (t->match(*data, list_data(node))) {
      if (list_remove(&t->table[bucket], prev, data)) {
        --(t->size);
        return true;
      } else {
        return false;
      }
    }
    prev = node;
  }

  return false;
}

/* Complexity O(1) */
bool htable_get(hashtable_ptr t, data_sink data) {

  list_node_ptr node;
  size_t bucket = t->hash(*data) & (t->buckets - 1); /* m % n */
  for (
    node = list_head(&t->table[bucket]);
    node != NULL;
    node = list_next(node)
  ) {
    if (t->match(*data, list_data(node))) {
      *data = list_data(node);
      return true;
    }
  }

  return false;
}

/* Complexity O(1) */
size_t htable_size(hashtable_ptr t) {
  return t->size;
}
