/*
 * set.c
 *
 *  Created on: May 4, 2016
 *      Author: kalkaseer
 */

#include "set.h"
#include "private/list_private.h"

#if 1 // defined(USE_LINKED_SET) && USE_LINKED_SET == 1

void set_init(set_ptr s, match_fn match, destroy_fn destroy) {
  list_init(s, destroy);
  list_set_match(s, match);
}

bool set_insert(set_ptr s, data_ptr data) {
  if (set_contains(s, data))
    return true;
  return list_insert(s, list_tail(s), data);
}

bool set_remove(set_ptr s, data_sink data) {
  list_node_ptr prev = NULL;
  list_node_ptr member;
  for (
    member = list_head(s);
    member != NULL;
    member = list_next(member)
  ) {
    if (list_get_match(s)(*data, list_data(member)))
      break;
    prev = member;
  }

  if (!prev)
    return false;

  return list_remove(s, prev, data);
}

bool set_union(set_ptr out, const set_ptr s1, const set_ptr s2) {
  list_node_ptr member;
  void * data;
  set_init(out, s1->match, s1->destroy);

  for (member = list_head(s1); member != NULL; member = list_next(member)) {
    data = list_data(member);
    if (!list_insert(out, list_tail(out), data)) {
      set_destroy(out);
      return false;
    }
  }

  for (member = list_head(s2); member != NULL; member = list_next(member)) {
    if (!set_contains(s1, list_data(member))) {
      data = list_data(member);
      if (!list_insert(out, list_tail(out), data)) {
        set_destroy(out);
        return false;
      }
    }
  }

  return true;
}

bool set_intersection(set_ptr out, const set_ptr s1, const set_ptr s2) {
  list_node_ptr member;
  void * data;
  set_init(out, s1->match, s1->destroy);

  for (member = list_head(s1); member != NULL; member = list_next(member)) {
    if (set_contains(s2, list_data(member))) {
      data = list_data(member);
      if (!list_insert(out, list_tail(out), data)) {
        set_destroy(out);
        return false;
      }
    }
  }

  return true;
}

bool set_difference(set_ptr out, const set_ptr s1, const set_ptr s2) {
  list_node_ptr member;
  void * data;
  set_init(out, s1->match, s1->destroy);

  for (member = list_head(s1); member != NULL; member = list_next(member)) {
    if (!set_contains(s2, list_data(member))) {
      data = list_data(member);
      if (!list_insert(out, list_tail(out), data)) {
        set_destroy(out);
        return false;
      }
    }
  }

  return true;
}

bool set_contains(const set_ptr s, data_ptr data) {
  for (
    list_node_ptr member = list_head(s);
    member != NULL;
    member = list_next(member)
  ) {
    if (list_get_match(s)(data, list_data(member)))
      return true;
  }
  return false;
}

bool set_is_subset(const set_ptr s1, const set_ptr s2) {
  if (set_size(s1) > set_size(s2))
    return false;
  for (
    list_node_ptr member = list_head(s1);
    member != NULL;
    member = list_next(member)
  ) {
    if (!set_contains(s2, member))
      return false;
  }
  return true;
}

bool set_equal(const set_ptr s1, const set_ptr s2) {
  if (set_size(s1) != set_size(s2))
    return false;
  return set_is_subset(s1, s2);
}

set_element_ptr set_next(const set_ptr s, const set_element_ptr e) {
  return list_next(e);
}

#else

#endif


