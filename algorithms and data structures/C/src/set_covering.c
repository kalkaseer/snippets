/*
 * set_covering.c
 *
 *  Created on: May 4, 2016
 *      Author: kalkaseer
 */

#include <stdlib.h>
#include "set_covering.h"

bool cover(set_ptr s, set_ptr p, set_ptr c) {

  set_ptr intersection = set_new();
  if (intersection == NULL)
    return false;
  keyed_set * subset;
  set_element_ptr element;
  set_element_ptr max_element;
  void * data;
  size_t max_size;

  set_init(c, set_get_match(p), set_get_destroy(p));

  while (set_size(s) > 0 && set_size(p) > 0) {

    max_size = 0;

    for (
      element = set_first(p);
      element != NULL;
      element = set_next(p, element)
    ) {
      if (!set_intersection(
        intersection,
        ((keyed_set *) set_data(element))->value,
        s)
      ) {
        set_destroy(c);
        return false;
      }

      if (set_size(intersection) > max_size) {
        max_element = element;
        max_size = set_size(intersection);
      }

      set_destroy(intersection);
    }

    if (max_size == 0)
      return false;

    subset = (keyed_set *) set_data(max_element);
    if (!set_insert(c, subset)) {
      set_destroy(c);
      return false;
    }

    for (
      element = set_first(subset->value);
      element != NULL;
      element = set_next(subset->value, element)
    ) {
      data = set_data(element);
      if (set_remove(s, (void **) &data) && set_get_destroy(s))
        set_get_destroy(s)(data);
    }

    if (!set_remove(p, (void **) &subset))
      return false;
  }

  free(intersection);
  return set_size(s) == 0;
}
