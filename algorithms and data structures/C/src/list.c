/*
 * list.c
 *
 *  Created on: Apr 30, 2016
 *      Author: kalkaseer
 */

#include "list.h"

#include <stdlib.h>
#include <string.h>
#include "private/list_private.h"

/* ****************************************************************************
 * Singly-Linked list
 * ***************************************************************************/

list_ptr list_new() {
  return (list_ptr) malloc(sizeof(list));
}

void list_init(list_ptr l, destroy_fn destroy) {
  l->size = 0;
  l->destroy = destroy;
  l->match = NULL;
  l->head = NULL;
  l->tail = NULL;
}

void list_destroy(list_ptr l) {
  void * data;
  while (list_size(l) > 0) {
    if (list_remove(l, NULL, (void **) &data) && l->destroy != NULL)
      l->destroy(data);
  }
  memset(l, 0, sizeof(list));
}

bool list_insert(list_ptr l, list_node_ptr node, const void * data) {
  list_node_ptr new_node;
  if ((new_node = list_node_make()) == NULL)
    return false;
  new_node->data = (void *) data;
  if (node == NULL) { // inserting at head
    if (list_size(l) == 0)
      l->tail = new_node;
    new_node->next = l->head;
    l->head = new_node;
  } else {
    if (node->next == NULL)
      l->tail = new_node;
    new_node->next = node->next;
    node->next = new_node;
  }
  ++(l->size);
  return true;
}

bool list_remove(list_ptr l, list_node_ptr node, void ** data) {

  list_node_ptr old_node;

  if (l->size == 0)
    return false;

  if (node == NULL) {
    *data = l->head->data;
    old_node = l->head;
    l->head = l->head->next;
    if (l->size == 1)
      l->tail = NULL;
  } else {
    if (node->next == NULL)
      return false;
    *data = node->next->data;
    old_node = node->next;
    node->next = node->next->next;
    if (node->next == NULL)
      l->tail = node;
  }
  free(old_node);
  --(l->size);
  return true;
}

size_t list_size(list_ptr l) {
  return l->size;
}

bool list_empty(list_ptr l) {
  return l->size == 0;
}

list_node_ptr list_head(const list_ptr l) {
  return l->head;
}

list_node_ptr list_tail(const list_ptr l) {
  return l->tail;
}

bool list_is_head(const list_ptr l, const list_node_ptr node) {
  return l->head == node;
}

bool list_is_tail(const list_node_ptr node) {
  return node->next == NULL;
}

void * list_data(const list_node_ptr node) {
  return node->data;
}

list_node_ptr list_next(const list_node_ptr node) {
  return node->next;
}

destroy_fn list_get_destroy(list_ptr l) {
  return l->destroy;
}

void list_set_destroy(list_ptr l, destroy_fn destroy) {
  l->destroy = destroy;
}

match_fn list_get_match(list_ptr l) {
  return l->match;
}

void list_set_match(list_ptr l, match_fn match) {
  l->match = match;
}


/* ****************************************************************************
 * Doubly-Linked list
 * ***************************************************************************/

dlist_ptr dlist_new() {
  return (dlist_ptr) malloc(sizeof(dlist));
}

void dlist_init(dlist_ptr dl, destroy_fn destroy) {

  dl->size = 0;
  dl->destroy = destroy;
  dl->match = NULL;
  dl->head = NULL;
  dl->tail = NULL;
}

void dlist_destroy(dlist_ptr dl) {

  void * data;

  while (dlist_size(dl) > 0) {
    if (dlist_remove(dl, NULL, (void **) &data) && dl->destroy != NULL)
      dl->destroy(data);
  }

  memset(dl, 0, sizeof(dlist));
}

bool dlist_insert(dlist_ptr dl, dlist_node_ptr node, const void * data) {

  dlist_node_ptr new_node;

  if ((new_node = dlist_node_make()) == NULL)
    return false;

  new_node->data = (void *) data;

  if (dlist_empty(dl)) {
    dl->head = new_node;
    dl->tail = new_node;
    new_node->next = NULL;
    new_node->prev = NULL;
  } else if (node == NULL) { // insert at head
    new_node->prev = NULL;
    new_node->next = dl->head;
    dl->head->prev = new_node;
    dl->head = new_node;
  } else {
    new_node->prev = node;
    new_node->next = node->next;
    if (node->next == NULL)
      dl->tail = new_node;
    else
      node->next->prev = new_node;
    node->next = new_node;
  }

  ++(dl->size);
  return true;
}

bool dlist_insert_before(dlist_ptr dl, dlist_node_ptr node, const void * data) {

  dlist_node_ptr new_node;

  if ((new_node = dlist_node_make()) == NULL)
    return false;

  new_node->data = (void *) data;

  if (dlist_empty(dl)) {
    dl->head = new_node;
    dl->tail = new_node;
    new_node->next = NULL;
    new_node->prev = NULL;
  } else if (node == NULL) { // insert at head
    new_node->prev = NULL;
    new_node->next = dl->head;
    dl->head->prev = new_node;
    dl->head = new_node;
  } else {
    new_node->next = node;
    new_node->prev = node->prev;
    if (node->prev == NULL)
      dl->head = new_node;
    else
      node->prev->next = new_node;
    node->prev = new_node;
  }

  ++(dl->size);
  return true;
}

bool dlist_remove(dlist_ptr dl, dlist_node_ptr node, void ** data) {

  if (node == NULL || dlist_empty(dl))
    return false;

  *data = node->data;

  if (dl->head == node) {
    dl->head = node->next;
    if (dl->head == NULL)
      dl->tail = NULL;
    else
      dl->head->prev = NULL;
  } else {
    node->prev->next = node->next;
    if (node->next == NULL)
      dl->tail = node->prev;
    else
      node->next->prev = node->prev;
  }

  free(node);
  --(dl->size);
  return true;
}

size_t dlist_size(dlist_ptr dl);

bool dlist_empty(dlist_ptr dl) {
  return dl->size == 0;
}

dlist_node_ptr dlist_head(const dlist_ptr dl) {
  return dl->head;
}

dlist_node_ptr dlist_tail(const dlist_ptr dl) {
  return dl->tail;
}

bool dlist_is_head(const dlist_node_ptr node) {
  return node->prev == NULL;
}

bool dlist_is_tail(const dlist_node_ptr node) {
  return node->next == NULL;
}

void * dlist_data(const dlist_node_ptr node) {
  return node->data;
}

dlist_node_ptr dlist_next(const dlist_node_ptr node) {
  return node->next;
}

dlist_node_ptr dlist_prev(const dlist_node_ptr node) {
  return node->prev;
}

/* ****************************************************************************
 * Circular list
 * ***************************************************************************/

clist_ptr clist_new() {
  return (clist_ptr) malloc(sizeof(clist));
}

void clist_init(clist_ptr l, destroy_fn destroy) {
  l->size = 0;
  l->destroy = destroy;
  l->match = NULL;
  l->head = NULL;
}

void clist_destroy(clist_ptr cl) {
  void * data;
  while (clist_size(cl) > 0) {
    if (clist_remove(cl, NULL, (void **) &data) && cl->destroy != NULL)
      cl->destroy(data);
  }
  memset(cl, 0, sizeof(list));
}

bool clist_insert(clist_ptr cl, clist_node_ptr node, const void * data) {

  clist_node_ptr new_node;
  if ((new_node = clist_node_make()) == NULL)
    return false;

  new_node->data = (void *) data;

  if (clist_empty(cl)) {
    new_node->next = new_node;
    cl->head = new_node;
  } else {
    new_node->next = node->next;
    node->next = new_node;
  }
  ++(cl->size);
  return true;
}

bool clist_remove(clist_ptr cl, clist_node_ptr node, void ** data) {

  clist_node_ptr old_node;
  if (clist_empty(cl))
    return false;

  *data = node->next->data;

  if (node->next == node) {
    old_node = node;
    cl->head = NULL;
  } else {
    old_node = node->next;
    node->next = node->next->next;
  }
  free(old_node);
  --(cl->size);
  return true;
}

size_t clist_size(const clist_ptr cl) {
  return cl->size;
}

bool clist_empty(clist_ptr cl) {
  return cl->size == 0;
}

clist_node_ptr clist_head(const clist_ptr cl) {
  return cl->head;
}

bool clist_is_head(const clist_ptr cl, const clist_node_ptr node) {
  return cl->head == node;
}

void * clist_data(const clist_node_ptr node) {
  return node->data;
}

clist_node_ptr clist_next(const clist_node_ptr node) {
  return node->next;
}
