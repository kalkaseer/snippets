/*
 * graph.c
 *
 *  Created on: Aug 7, 2016
 *      Author: kalkaseer
 */

#include <stdlib.h>
#include "graph.h"
#include "set.h"

struct adjacency_list_t {
  data_writable vertex;
  set           adjacent;
};

struct graph_t {
  size_t     vcount;
  size_t     ecount;
  match_fn   match;
  destroy_fn destroy;
  list       adjacent_lists;
};


typedef enum _vertex_color {
  white, gray, black
} vertex_color_t;


void graph_init(graph_ptr g, match_fn match, destroy_fn destroy) {

  g->vcount  = 0;
  g->ecount  = 0;
  g->match   = match;
  g->destroy = destroy;

  list_init(&g->adjacent_lists, NULL);
}

void graph_destroy(graph_ptr g) {

  adjacency_list_ptr alist;

  while (list_size(&g->adjacent_lists)) {

    if (list_remove(&g->adjacent_lists, NULL, (data_sink) &alist)) {
      set_destroy(&alist->adjacent);
      if (g->destroy)
        g->destroy(alist->vertex);
      free(alist);
    }
  }

  list_destroy(&g->adjacent_lists);
  memset(g, 0, sizeof(graph));
}

bool graph_insert_vertex(graph_ptr g, data_ptr v) {

  list_node_ptr      node;
  adjacency_list_ptr alist;

  /* no duplicate vertices */
  for (node = list_head(&g->adjacent_lists); node; node = list_next(node)) {
    if (g->match(v, ((adjacency_list_ptr) list_data(node))->vertex))
      return false;
  }

  if ((alist = (adjacency_list_ptr) malloc(sizeof(adjacency_list))) == NULL)
    return false;

  if (! list_insert(&g->adjacent_lists, list_tail(&g->adjacent_lists), alist))
    return false;

  ++(g->vcount);
  return true;
}

bool graph_insert_edge(graph_ptr g, data_ptr v1, data_ptr v2) {

  list_node_ptr node;

  /* v1 and v2 must be in graph */
  for (node = list_head(&g->adjacent_lists); node; node = list_next(node)) {
    if (g->match(v2, ((adjacency_list_ptr) list_data(node))->vertex))
      break;
  }

  if (! node)
    return false;

  for (node = list_head(&g->adjacent_lists); node; node = list_next(node)) {
    if (g->match(v1, ((adjacency_list_ptr) list_data(node))->vertex))
      break;
  }

  if (! node)
    return false;

  if (! set_insert(&((adjacency_list_ptr) list_data(node))->adjacent, v2))
    return false;

  ++(g->ecount);
  return true;
}

bool graph_erase_vertex(graph_ptr g, data_sink v) {

  list_node_ptr       node, tmp, prev;
  adjacency_list_ptr  alist;
  bool                found;

  prev = NULL;
  bool = false;

  for (node = list_head(&g->adjacent_lists); node; node = list_next(node)) {

    /* do not remove a vertex if it is in an adjacency list */
    if (set_is_member(&((adjacency_list_ptr) list_data(node))->adjacent), *v)
      return false;

    if (g->match(*v, ((adjacency_list_ptr) list_data(node))->vertex)) {
      tmp = node;
      found = true;
    }

    if (! found)
      prev = node;
  }

  if (! found)
    return false;

  /* remove a vertex only if its adjacency list is empty */
  if (set_size(&((adjacency_list_ptr) list_data(tmp))->adjacent))
    return false;

  if (! list_remove(&g->adjacent_lists, prev, (data_sink) &alist))
    return false;

  *v = alist->vertex;
  free(alist);

  --(g->vcount);
  return true;
}

bool graph_erase_edge(graph_ptr g, data_ptr v1, data_sink v2) {

  list_node_ptr node;

  /* find adjacency list for v1 */
  for (node = list_head(&g->adjacent_lists); node; node = list_next(node)) {
    if (g->match(v1, ((adjacency_list_ptr) list_data(node))->vertex))
      break;
  }

  if (! node)
    return false;

  /* remove v2 from adjacency list of v1 */
  if (! set_remove(&((adjacency_list_ptr) list_data(node))->adjacent), v2)
    return false;

  --(g->ecount);
  return true;
}

bool graph_adjacency_list(
  const graph_ptr g, data_ptr v, adjacency_list_sink alist
) {
  list_node_ptr node, prev;
  prev = NULL;

  for (node = list_head(&g->adjacent_lists); node; node = list_next(node)) {

    if (g->match(v, ((adjacency_list_ptr) list_data(node))->vertex))
      break;

    prev = node;
  }

  if (! node)
    return false;

  *alist = list_data(node);
  return true;
}

bool graph_is_adjacent(const graph_ptr g, data_ptr v1, data_ptr v2) {

  list_node_ptr node, prev;
  prev = NULL;

  for (node = list_head(&g->adjacent_lists); node; node = list_next(node)) {
    if (g->match(v1, ((adjacency_list_ptr) list_data(node))->vertex))
      break;

    prev = node;
  }

  if (! node)
    return false;

  return set_contains(&((adjacency_list_ptr) list_data(node))->adjacent);
}

list_ptr graph_structure(const graph_ptr g) {
  return &g->adjacent_lists;
}

size_t graph_vertex_count(const graph_ptr g) {
  return g->vcount;
}

size_t graph_edge_count(const graph_ptr g) {
  return g->ecount;
}

bool graph_bfs(graph_ptr g, search_vertex_ptr start, list_ptr result) {

  queue              q;
  adjacency_list_ptr alist, clr_alist;
  search_vertex_ptr  adjv, clrv;
  list_node_ptr      node, member;

  queue_init(&q, NULL);

  if (! graph_adjacency_list(g, start, &clr_alist)) {
    queue_destroy(&q);
    return false;
  }

  if (! queue_push(&q, clr_alist)) {
    queue_destroy(&q);
    return false;
  }

  for (node = list_head(&g->adjacent_lists); node; node = list_next(node)) {

    clrv = ((adjacency_list_ptr) list_data(node))->vertex;

    if (g->match(clrv, start)) {
      clrv->color = gray;
      clrv->hops = 0;
    } else {
      clrv->color = white;
      clrv = -1;
    }
  }

  while (queue_size(&q)) {

    alist = queue_peek(&q);

    for (member = list_head(&alist->adjacent); member; member = list_next(member)) {
      adjv = list_data(member);
      if (! graph_adjacency_list(g, adjv, &clr_alist)) {
        queue_destroy(&q);
        return false;
      }

      clrv = clr_alist->vertex;

      if (clrv->color == white) {
        clrv->color = gray;
        clrv->hops = ((search_vertex_ptr) alist->vertex)->hops + 1;
        if (! queue_push(&q, clr_alist)) {
          queue_destroy(&q);
          return false;
        }
      }
    }

    if (! queue_pop(&q, (data_sink) &alist)) {
      ((search_vertex_ptr) alist->vertex)->color = black;
    } else {
      queue_destroy(&q);
      return false;
    }
  }

  queue_destroy(&q);

  list_init(result, NULL);

  for (node = list_head(&g->adjacent_lists); node; node = list_next(node)) {

    clrv = ((adjacency_list_ptr) list_data(node))->vertex;

    if (clrv->hops != -1) {
      if (! list_insert(result, list_tail(result), clrv)) {
        list_destroy(result);
        return false;
      }
    }
  }

  return true;
}

static bool dfs(graph_ptr g, adjacency_list_ptr alist, list_ptr result) {

  adjacency_list_ptr clr_alist;
  search_vertex_ptr  clrv, adjv;
  list_node_ptr      node;

  ((search_vertex_ptr) alist->vertex)->color = gray;

  for (node = list_head(&alist); node; node = list_next(node)) {

    adjv = list_data(node);

    if (! graph_adjacency_list(g, adjv, &clr_alist))
      return false;

    clrv = clr_alist->vertex;

    if (clrv->color == white) {
      if (! dfs(g, clr_alist, result))
        return false;
    }
  }

  ((search_vertex_ptr) alist->vertex)->color = black;

  if (! list_insert(result, NULL, (search_vertex_ptr) alist->vertex))
    return false;

  return true;
}

bool graph_dfs(graph_ptr g, list_ptr result) {

  search_vertex_ptr vertex;
  list_node_ptr     node;

  for (node = list_head(&g->adjacent_lists); node; node = list_next(node)) {
    vertex = ((adjacency_list_ptr) list_data(node))->vertex;
    vertex->color = white;
  }

  list_init(result, NULL);

  for (node = list_head(&g->adjacent_lists); node; node = list_next(node)) {
    /* ensure that every component of unconnected graphs are searched */
    vertex = ((adjacency_list_ptr) list_data(node))->vertex;
    if (vertex->color == white) {
      if (! dfs(g, (adjacency_list_ptr) list_data(node), result)) {
        list_destroy(result);
        return false;
      }
    }
  }

  return true;
}
