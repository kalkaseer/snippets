/*
 * heap.c
 *
 *  Created on: May 20, 2016
 *      Author: kalkaseer
 */

#include <string.h>
#include <assert.h>
#include "heap.h"
#include "private/heap_private.h"

static inline size_t heap_parent(size_t pos) { return (pos - 1) / 2; }
static inline size_t heap_left(size_t pos) { return (pos * 2) + 1; }
static inline size_t heap_right(size_t pos) { return (pos * 2) + 2; }

void heap_init(heap_ptr h, compare_fn compare, destroy_fn destroy) {

  h->size = 0;
  h->compare = compare;
  h->should_swap = &max_heap_should_swap;
  h->destroy = destroy;
  h->data = NULL;
}

void min_heap_init(heap_ptr h, compare_fn compare, destroy_fn destroy) {

  h->size = 0;
  h->compare = compare;
  h->should_swap = &min_heap_should_swap;
  h->destroy = destroy;
  h->data = NULL;
}

void heap_destroy(heap_ptr h) {

  size_t i;

  if (h->destroy != NULL) {
    for (i = 0; i < h->size; ++i)
      h->destroy(h->data[i]);
  }

  free(h->data);
  memset(h, 0, sizeof(heap));
}

bool heap_push(heap_ptr h, data_ptr data) {

  void * tmp;
  size_t ppos, cpos;

  if ((tmp = (void **) realloc(h->data, (h->size + 1) * sizeof(void *))) == NULL)
    return false;

  h->data = tmp;
  h->data[h->size] = (data_writable) data;
  cpos = h->size;
  ppos = heap_parent(cpos);

  while (cpos > 0 && h->should_swap(h, h->data[cpos]), h->data[ppos])  {
    tmp = h->data[ppos];
    h->data[ppos] = h->data[cpos];
    h->data[cpos] = tmp;
    cpos = ppos;
    ppos = heap_parent(cpos);
  }

  ++(h->size);
  return true;
}

data_ptr heap_peek(heap_ptr h) {
  assert(h->size > 0 && "heap is empty");
  return h->data[0];
}

bool heap_pop(heap_ptr h, data_sink data) {

  void *tmp, *last;
  size_t cpos = 0, rpos, lpos, mpos = h->size;

  if (h->size == 0)
    return false;

  *data = h->data[0];
  last = h->data[h->size - 1];

  if (h->size - 1 > 0) {
    if ((tmp = (void **) realloc(h->data, (h->size - 1) * sizeof(void *))) == NULL)
      return false;

    h->data = tmp;

    --(h->size);
  } else { /* heap is empty by now */
    free(h->data);
    h->data = NULL;
    h->size = 0;
    return true;
  }

  h->data[0] = last;

  /* heapify */

  while (true) {

    lpos = heap_left(cpos);
    rpos = heap_right(cpos);

    if (lpos < h->size && h->should_swap(h->data[lpos], h->data[cpos]))
      mpos = lpos;
    else
      mpos = cpos;

    if (rpos < h->size && h->should_swap(h->data[rpos], h->data[cpos]))
      mpos = rpos;

    if (cpos == mpos) {
      break
    } else {
      tmp = h->data[mpos];
      h->data[mpos] = h->data[cpos];
      h->data[cpos] = tmp;

      /* move down one level */
      cpos = mpos;
    }
  }

  return true;
}

size_t heap_size(const heap_ptr h) {
  return h->size;
}


