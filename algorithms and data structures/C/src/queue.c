/*
 * queue.c
 *
 *  Created on: May 4, 2016
 *      Author: kalkaseer
 */

#include <stdlib.h>
#include "queue.h"
#include "private/list_private.h"

bool queue_push(queue_ptr q, data_ptr data) {
  return list_insert(q, list_tail(q), data);
}

bool queue_pop(queue_ptr q, void ** data) {
  return list_remove(q, NULL, data);
}

data_ptr queue_peek(queue_ptr q) {
  return q->head != NULL ? q->head->data : NULL;
}
