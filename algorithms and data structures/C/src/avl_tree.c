/*
 * avl_tree.c
 *
 *  Created on: May 19, 2016
 *      Author: kalkaseer
 */

#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include "avl_tree.h"
#include "private/binary_tree_private.h"
#include "bits.h"

#define AVL_BALANCED    1
#define AVL_LEFT_HEAVY  2
#define AVL_RIGHT_HEAVY 4

struct bstree_node_t {
  void       * data;
  uint_fast8_t bits;
};

struct bstree_t {
  bitree_ptr tree;
  compare_fn compare;
};

static inline void avl_set_hidden(bstree_node_ptr node) {
  BIT_SET(node->bits, 0);
}

static inline void avl_clear_hidden(bstree_node_ptr node) {
  BIT_CLEAR(node->bits, 0);
}

static inline bool avl_is_hidden(bstree_node_ptr node) {
  return BIT_CHECK(node->bits, 0);
}

static inline void avl_set_balanced(bstree_node_ptr node) {
  BIT_SET(node->bits, 1);
}

static inline void avl_set_left_heavy(bstree_node_ptr node) {
  BIT_SET(node->bits, 2);
}

static inline bool avl_is_left_heavy(bstree_node_ptr node) {
  return BIT_CHECK(node->bits, 2);
}

static inline void avl_set_right_heavy(bstree_node_ptr node) {
  BIT_SET(node->bits, 3);
}

static inline bool avl_is_right_heavy(bstree_node_ptr node) {
  return BIT_CHECK(node->bits, 3);
}

static int_fast32_t avl_factor(bstree_node_ptr node) {
  return node->bits >> 1;
}

static inline bstree_node_ptr avl_data(bitree_node_ptr node) {
  return (bstree_node_ptr) node->data;
}

static void avl_destroy_right(bstree_ptr tree, bitree_node_ptr node);

static void avl_rotate_left(bitree_node_ptr * node) {

  bitree_node_ptr left = (*node)->left;
  bitree_node_ptr grandchild;

  if (avl_is_left_heavy(avl_data(left))) {
    /* perform LL rotation */
    (*node)->left = left->right;
    left->right = *node;
    avl_set_balanced(avl_data(*node));
    avl_set_balanced(avl_data(left));
    *node = left;
  } else {
    /* perform LR rotation */
    grandchild = left->right;
    left->right = grandchild->left;
    grandchild->left = left;
    (*node)->left = grandchild->right;
    grandchild->right = *node;

    switch (avl_factor(avl_data(grandchild))) {
      case AVL_BALANCED:
        avl_set_balanced(avl_data(*node));
        avl_set_balanced(avl_data(left));
        break;

      case AVL_LEFT_HEAVY:
        avl_set_right_heavy(avl_data(*node));
        avl_set_balanced(avl_data(left));
        break;

      case AVL_RIGHT_HEAVY:
        avl_set_balanced(avl_data(*node));
        avl_set_left_heavy(avl_data(left));
        break;
    }

    avl_set_balanced(avl_data(grandchild));
    *node = grandchild;
  }
}

static void avl_rotate_right(bitree_node_ptr * node) {

  bitree_node_ptr right = (*node)->right;
  bitree_node_ptr grandchild;

  if (avl_is_right_heavy(avl_data(right))) {
    /* perform RR rotation */
    (*node)->right = right->left;
    right->left = *node;
    avl_set_balanced(avl_data(*node));
    avl_set_balanced(avl_data(right));
    *node = right;
  } else {
    /* perform RL rotation */
    grandchild = right->left;
    right->left = grandchild->right;
    grandchild->right = right;
    (*node)->right = grandchild->left;
    grandchild->left = *node;

    switch (avl_factor(avl_data(grandchild))) {
      case AVL_BALANCED:
        avl_set_balanced(avl_data(*node));
        avl_set_balanced(avl_data(right));
        break;

      case AVL_LEFT_HEAVY:
        avl_set_balanced(avl_data(*node));
        avl_set_right_heavy(avl_data(right));
        break;

      case AVL_RIGHT_HEAVY:
        avl_set_left_heavy(avl_data(*node));
        avl_set_balanced(avl_data(right));
        break;
    }

    avl_set_balanced(avl_data(grandchild));
    *node = grandchild;
  }
}

static void avl_destroy_left(bstree_ptr tree, bitree_node_ptr node) {

  bitree_node_ptr *pos;
  if (bstree_size(tree) == 0)
    return;

  if (node == NULL)
    pos = &tree->tree->root;
  else
    pos = &node->left;

  if (*pos != NULL) {
    avl_destroy_left(tree, *pos);
    avl_destroy_right(tree, *pos);

    if (tree->tree->destroy != NULL) {
      tree->tree->destroy(avl_data(*pos)->data);
    }

    free((*pos)->data);
    free(*pos);
    *pos = NULL;

    --(tree->tree->size);
  }
}

static void avl_destroy_right(bstree_ptr tree, bitree_node_ptr node) {

  bitree_node_ptr *pos;
  if (bstree_size(tree) == 0)
    return;

  if (node == NULL)
    pos = &tree->tree->root;
  else
    pos = &node->right;

  if (*pos != NULL) {

    avl_destroy_left(tree, *pos);
    avl_destroy_right(tree, *pos);

    if (tree->tree->destroy != NULL) {
      tree->tree->destroy(avl_data(*pos)->data);
    }

    free((*pos)->data);
    free(*pos);
    *pos = NULL;

    --(tree->tree->size);
  }
}

static bool avl_insert(
  bstree_ptr tree, bitree_node_ptr *node, data_ptr data, bool *balanced
) {
  bstree_node_ptr bst_node;
  int cmpval;
  bool result;

  if (bitree_is_eob(*node)) {

    if ((bst_node = (bstree_node_ptr) malloc(sizeof(bstree_node))) == NULL)
      return false;

    avl_set_balanced(bst_node);
    avl_clear_hidden(bst_node);
    bst_node->data = (void *) data;

  } else { /* ! if bitree_is_eob(*node) */

    cmpval = tree->compare(data, avl_data(*node)->data);
    if (cmpval < 0 ) {

      /* insert to the left */
      if (bitree_is_eob((*node)->left)) {
        if ((bst_node = (bstree_node_ptr) malloc(sizeof(bstree_node))) == NULL)
          return false;
        avl_set_balanced(bst_node);
        avl_clear_hidden(bst_node);
        bst_node->data = (void *) data;

        if (! bitree_insert_left(tree->tree, *node, bst_node))
          return false;

        *balanced = false;

      } else { /* ! bitree_is_eob(bitree_left(*node)) */
        if (! (result = avl_insert(tree, &((*node)->left), data, balanced)))
          return false;
      }

      /* Ensure balance */
      if (! (*balanced)) {
        switch (avl_factor(avl_data(*node))) {

          case AVL_BALANCED:
            avl_set_left_heavy(avl_data(*node));
            break;

          case AVL_LEFT_HEAVY:

            avl_rotate_left(node);
            *balanced = true;
            break;

          case AVL_RIGHT_HEAVY:

            avl_set_balanced(avl_data(*node));
            *balanced = true;
            break;
        } /* switch avl_factor */
      } /* (*balanced) */

    } else if (cmpval > 0) {

      /* insert to the right */

      if (bitree_is_eob((*node)->right)) {

        if ((bst_node = (bstree_node_ptr) malloc(sizeof(bst_node))) == NULL)
          return false;
        avl_set_balanced(bst_node);
        avl_clear_hidden(bst_node);
        bst_node->data = (void *) data;

        if (! bitree_insert_right(tree->tree, *node, bst_node))
          return false;

        *balanced = false;

      } else { /* ! bitree_is_eob(bitree_right(*node)) */
        if (! (result = avl_insert(tree, &((*node)->right), data, balanced)))
          return false;
      }

      if (! (*balanced)) {
        switch (avl_factor(avl_data(*node))) {

          case AVL_BALANCED:

            avl_set_right_heavy(avl_data(*node));
            break;

          case AVL_LEFT_HEAVY:

            avl_set_balanced(avl_data(*node));
            *balanced = true;
            break;

          case AVL_RIGHT_HEAVY:

            avl_rotate_right(node);
            *balanced = true;
            break;
        } /* switch */
      } /* (*balanced) */

    } else { /* cmpval == 0 */

      if (! avl_is_hidden(avl_data(*node)))
        return false;
      else {
        if (tree->tree->destroy != NULL)
          tree->tree->destroy(avl_data(*node)->data);
        avl_data(*node)->data = (void *) data;
        avl_set_hidden(avl_data(*node));
        *balanced = true;
      }

    } /* cmpval == 0 */

  } /* ! if bitree_is_eob(*node) */

  return true;
}

static bool avl_hide(bstree_ptr tree, bitree_node_ptr node, data_ptr data) {

  int cmpval;

  if (bitree_is_eob(node))
    return false;

  cmpval = tree->compare(data, avl_data(node)->data);

  if (cmpval < 0) {
    return avl_hide(tree, node->left, data);
  } else if (cmpval > 0) {
    return avl_hide(tree, node->right, data);
  } else { /* cmpval == 0 */
    avl_set_hidden(avl_data(node));
    return true;
  }
}

static bool avl_get(bstree_ptr tree, bitree_node_ptr node, data_sink data) {

  int cmpval;

  if (bitree_is_eob(node))
    return false;

  cmpval = tree->compare(*data, avl_data(node)->data);

  if (cmpval < 0) {
    return avl_get(tree, node->left, data);
  } else if (cmpval > 0) {
    return avl_get(tree, node->right, data);
  } else {
    if (! avl_is_hidden(avl_data(node))) {
      *data = avl_data(node)->data;
      return true;
    }
    return false;
  }
}

bstree_ptr bstree_new() {
  return (bstree_ptr) malloc(sizeof(bstree));
}

bool bstree_init(bstree_ptr tree, compare_fn compare, destroy_fn destroy) {
  if ((tree->tree = bitree_new()) == NULL)
    return false;
  bitree_init(tree->tree, destroy);
  tree->compare = compare;
  return true;
}

void bstree_init_with_raw_tree(
  bstree_ptr tree, bitree_ptr btree, compare_fn compare, destroy_fn destroy
) {
  bitree_init(btree, destroy);
  tree->tree = btree;
  tree->compare = compare;
}

void bstree_destroy(bstree_ptr tree) {
  avl_destroy_left(tree, NULL);
  free(tree->tree);
  memset(tree, 0, sizeof(bstree));
}

bool bstree_insert(bstree_ptr tree, data_ptr data) {
  bool balanced = false;
  return avl_insert(tree, &(tree->tree->root), data, &balanced);
}

bool bstree_remove(bstree_ptr tree, data_ptr data) {
  return avl_hide(tree, tree->tree->root, data);
}

bool bstree_get(bstree_ptr tree, data_sink data) {
  return avl_get(tree, tree->tree->root, data);
}

size_t bstree_size(bstree_ptr tree) {
  return tree->tree->size;
}

