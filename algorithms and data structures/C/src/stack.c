/*
 * stack.c
 *
 *  Created on: May 4, 2016
 *      Author: kalkaseer
 */

#include "stack.h"
#include <stdlib.h>
#include <string.h>
#include "private/list_private.h"

struct stack_t {
  size_t      size;
  size_t      capacity;
  data_ptr  * data;
  destroy_fn  destroy;
};

stack_ptr stack_new() {
  return (stack_ptr) malloc(sizeof(stack));
}

bool stack_init(stack_ptr s, destroy_fn destroy, size_t size) {
  s->size = 0;
  s->capacity = 0;
  if ((s->data = malloc(size * sizeof(void*))) == NULL)
    return false;
  s->destroy = destroy;
  return true;
}

void stack_destory(stack_ptr s) {
  void * data;
  while (s->size > 0) {
    if (stack_pop(s, (void **) &data) && s->destroy != NULL)
      s->destroy(data);
  }
  memset(s, sizeof(stack), 0);
}

bool stack_push(stack_ptr s, data_ptr data) {
  if (s->size < s->capacity) {
    s->data[s->size++ * sizeof(void*)] = data;
    return true;
  }
  return false;
}

bool stack_pop(stack_ptr s, void ** data) {
  if (s->size != 0) {
    *data = (void *) s->data[--(s->size) * sizeof(void*)];
    s->data[s->size * sizeof(void*)] = NULL;
    return true;
  }
  return false;
}

data_ptr stack_peek(stack_ptr s) {
  if (s->size != 0) {
    return s->data[(s->size - 1) * sizeof(void*)];
  }
  return NULL;
}

size_t stack_size(stack_ptr s) {
  return s->size;
}

bool linked_stack_push(linked_stack_ptr s, data_ptr data) {
  return list_insert(s, NULL, data);
}

bool linked_stack_pop(linked_stack_ptr s, void ** data) {
  return list_remove(s, NULL, data);
}

data_ptr linked_stack_peek(linked_stack_ptr s) {
  return s->head != NULL ? s->head->data : NULL;
}

