/*
 * avl_tree.h
 *
 *  Created on: May 19, 2016
 *      Author: kalkaseer
 */

#ifndef C_AVL_TREE_H_
#define C_AVL_TREE_H_

#include <stdbool.h>
#include "common.h"
#include "binary_tree.h"

struct bstree_node_t;
typedef struct bstree_node_t bstree_node;
typedef bstree_node * bstree_node_ptr;

struct bstree_t;
typedef struct bstree_t bstree;
typedef bstree * bstree_ptr;

bstree_ptr bstree_new();

/* O(1) */
bool bstree_init(bstree_ptr tree, compare_fn compare, destroy_fn destroy);

void bstree_init_with(
  bstree_ptr tree, bitree_ptr btree, compare_fn compare, destroy_fn destroy);

/* O(n) */
void bstree_destroy(bstree_ptr tree);

/* O(1.5k * log n) */
bool bstree_insert(bstree_ptr tree, data_ptr data);

/* O(log n) */
bool bstree_remove(bstree_ptr tree, data_ptr data);

/* O(log n) */
bool bstree_get(bstree_ptr tree, data_sink data);

/* O(1) */
size_t bstree_size(bstree_ptr);

#endif /* C_AVL_TREE_H_ */
