/*
 * set_covering.h
 *
 *  Created on: May 4, 2016
 *      Author: kalkaseer
 */

#ifndef C_SET_COVERING_H_
#define C_SET_COVERING_H_

#include "set.h"

/*
 * Set covering is a problem such that:
 * given a set S and a set P of subsets of S, find the smallest set C of
 * subsets of S in P such that each element in S is contained in at least one
 * subset in C.
 *
 * Example: given a skill set, S, and a set of sets of skills possessed by
 * different people, P, find the smallest of people containing all required
 * skills.
 * S = { a, b, c, d, e, f, g, h, i, j, k, l }
 * P = { A1, …, A7 }
 *  A1 = { a, b, c, d },
 *  A2 = { e, f, g, h, i },
 *  A3 = { j, k, l },
 *  A4 = { a, e },
 *  A5 = { b, f, g },
 *  A6 = { c, d, g, h, k, l },
 *  A7 = { l }
 * C = { A1, A2, A3 }
 *
 * Approximate greedy algorithms for set covering with complexity bound of
 * O(n ^ 3) where n is the number of elements in the initial S.
 * This happens when there is exactly one subset for each the P subsets for
 * each element in S because there set intersection is O(n) and the inner loop
 * of the algorithm takes O(n^2).
 *
 * The algorithm tries to cover as many elements of S as early as possible. It
 * might not produce the optimal result but it has an upper complexity bound.
 * For the problem above it will produce: C = { A6, A2, A1, A3 }.
 */

typedef struct keyed_set_t {
  void *  key;
  set_ptr value;
} keyed_set;

bool cover(set_ptr s, set_ptr p, set_ptr c);


#endif /* C_SET_COVERING_H_ */
