/*
 * binary_tree.h
 *
 *  Created on: May 15, 2016
 *      Author: kalkaseer
 */

#ifndef C_BINARY_TREE_H_
#define C_BINARY_TREE_H_

#include <stdbool.h>
#include <stddef.h>
#include "common.h"

struct bitree_node_t;
typedef struct bitree_node_t bitree_node;
typedef bitree_node * bitree_node_ptr;

struct bitree_t;
typedef struct bitree_t bitree;
typedef bitree * bitree_ptr;

typedef void (*visit_bitree_fn)(bitree_node_ptr);

bitree_ptr bitree_new();

/* O(1) */
void bitree_init(bitree_ptr tree, destroy_fn destroy);

/* O(n) */
void bitree_destroy(bitree_ptr tree);

/* O(1) */
bool bitree_insert_left(bitree_ptr tree, bitree_node_ptr node, data_ptr data);

/* O(1) */
bool bitree_insert_right(bitree_ptr tree, bitree_node_ptr node, data_ptr data);

/* O(n). Postorder traversal */
void bitree_remove_left(bitree_ptr tree, bitree_node_ptr node);

/* O(n). Postorder traversal */
void bitree_remove_right(bitree_ptr tree, bitree_node_ptr node);

/* O(1) */
bool bitree_merge(
  bitree_ptr merge, bitree_ptr left, bitree_ptr right, data_ptr data);

/* O(1) */
size_t bitree_size(bitree_ptr tree);

/* O(1) */
bitree_node_ptr bitree_root(bitree_ptr tree);

/* O(1) */
bool bitree_is_leaf(bitree_node_ptr node);

/* O(1) */
bool bitree_is_eob(bitree_node_ptr node);

data_writable bitree_data(bitree_node_ptr node);

/* O(1) */
bitree_node_ptr bitree_left(bitree_node_ptr node);

/* O(1) */
bitree_node_ptr bitree_right(bitree_node_ptr node);

/* O(1) */
bool bitree_preorder(bitree_node_ptr node, visit_bitree_fn visit);

/* O(1) */
bool bitree_inorder(bitree_node_ptr node, visit_bitree_fn visit);

/* O(1) */
bool bitree_postorder(bitree_node_ptr node, visit_bitree_fn visit);

/* O(1) */
bool bitree_levelorder(bitree_node_ptr node, visit_bitree_fn visit);

/* O(n) */
void bitree_traverse_preorder(bitree_node_ptr node, visit_bitree_fn visit);

/* O(n) */
void bitree_traverse_inroder(bitree_node_ptr node, visit_bitree_fn visit);

/* O(n) */
void bitree_traverse_postorder(bitree_node_ptr node, visit_bitree_fn visit);

/* Worst case: O(n^2) */
void bitree_traverse_levelorder(bitree_node_ptr node, visit_bitree_fn visit);

void bitree_traverse_level(
  bitree_node_ptr node, size_t level, visit_bitree_fn visit);

size_t bitree_height_at_node(bitree_node_ptr node);

size_t bitree_height(bitree_ptr tree);

#endif /* C_BINARY_TREE_H_ */
