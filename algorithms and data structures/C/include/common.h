/*
 * common.h
 *
 *  Created on: May 4, 2016
 *      Author: kalkaseer
 */

#ifndef C_COMMON_H_
#define C_COMMON_H_

#include <stdbool.h>
#include <stddef.h>

typedef const void * data_ptr;

typedef void * data_writable;

typedef void ** data_sink;

typedef void (*destroy_fn)(void * data);

typedef bool (*match_fn)(data_ptr key1, data_ptr key2);

typedef int (*compare_fn)(data_ptr key1, data_ptr key2);

typedef size_t (*hash_fn)(data_ptr);


#endif /* C_COMMON_H_ */
