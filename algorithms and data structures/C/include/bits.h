/*
 * bits.h
 *
 *  Created on: May 20, 2016
 *      Author: kalkaseer
 */

#ifndef C_BITS_H_
#define C_BITS_H_


#define BIT_SET(number, bit) ((number) |= ((uintmax_t) 1 << (bit)))
#define BIT_CLEAR(number, bit) ((number) &= ~((uintmax_t) 1 << (bit)))
#define BIT_FLIP(number, bit) ((number) ^= ((uintmax_t) 1 << (bit)))
#define BIT_CHECK(number, bit) ((((number) >> (bit)) & (uintmax_t) 1))
#define BIT_NSET(number, bit, flag) ((number) ^= (-(flag) ^ (number)) & ((uintmax_t) 1 << (bit)))

#define BITMASK_SET(number, mask) ((number) |= (mask))
#define BITMASK_CLEAR(number, mask) ((number) &= (~mask))
#define BITMASK_FLIP(number, mask) ((number) ^= (mask))
#define BITMASK_CHECK(number, mask) (((number) & (mask)) == (mask))


#endif /* C_BITS_H_ */
