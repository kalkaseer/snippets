/*
 * heap.h
 *
 *  Created on: May 20, 2016
 *      Author: kalkaseer
 */

#ifndef C_HEAP_H_
#define C_HEAP_H_

#include <stdbool.h>
#include "common.h"

struct heap_t;
typedef struct heap_t heap;
typedef heap * heap_ptr;

/* O(1) */
void heap_init(heap_ptr h, compare_fn compare, destroy_fn destroy);

void min_heap_init(heap_ptr h, compare_fn compare, destroy_fn destroy);

/* O(n) */
void heap_destroy(heap_ptr h);

/* O(log n) */
bool heap_push(heap_ptr h, data_ptr data);

/* O(1) */
data_ptr heap_peak(heap_ptr h);

/* O(log n) */
bool heap_pop(heap_ptr h, data_sink data);

/* O(1) */
size_t heap_size(const heap_ptr h);


#endif /* C_HEAP_H_ */
