/*
 * open_addressed_hashtable.h
 *
 *  Created on: May 15, 2016
 *      Author: kalkaseer
 */

#ifndef C_OPEN_ADDRESSED_HASHTABLE_H_
#define C_OPEN_ADDRESSED_HASHTABLE_H_

#include <stdbool.h>
#include "common.h"

struct open_hashtable_t;
typedef struct open_hashtable_t open_hashtable;
typedef open_hashtable * open_hashtable_ptr;

open_hashtable_ptr open_hashtable_new();

/* Complexity: O(n) */
bool open_hashtable_init(
  open_hashtable_ptr table, size_t positions, hash_fn h1, hash_fn h2,
  match_fn match, destroy_fn destroy);

/* Complexity: O(n) */
void open_hashtable_destroy(open_hashtable_ptr table);

/* Complexity: O(1) */
bool open_hashtable_insert(open_hashtable_ptr table, data_ptr data);

/* Complexity: O(1) */
bool open_hashtable_remove(open_hashtable_ptr table, data_sink data);

/* Complexity: O(1) */
bool open_hashtable_get(const open_hashtable_ptr table, data_sink data);

/* Complexity: O(1) */
size_t open_hashtable_size(const open_hashtable_ptr table);

#endif /* C_OPEN_ADDRESSED_HASHTABLE_H_ */
