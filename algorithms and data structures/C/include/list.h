/*
 * list.h
 *
 *  Created on: Apr 30, 2016
 *      Author: kalkaseer
 */

#ifndef LIST_H_
#define LIST_H_

#include <stdbool.h>
#include "common.h"

struct list_t;
typedef struct list_t list;
typedef list * list_ptr;

struct list_node_t;
typedef struct list_node_t list_node;
typedef list_node * list_node_ptr;

list_ptr list_new();

/**
 * Initialises a list before it can be used with any other operation. `destroy`
 * is a function to be called upon calling `list_destory`. The function should
 * free each element's dynamically allocated memory as well as the memory for
 * the list itself. If memory should not be destroyed, `destroy` should be set
 * to `NULL`.
 *
 * Complexity O(1).
 */
void list_init(list_ptr l, destroy_fn destroy);

/**
 * Destroys a list previously initialised by `list_init` by deallocating memory.
 * for each element and the list structure itself using the function `destroy`
 * passed to `list_init`, After calling this function, the list should not be
 * used again unless `list_init` is called on `l` again.
 *
 * Complexity O(n).
 */
void list_destroy(list_ptr l);

/**
 * Inserts an new node containing `data` next to `node`. If `node` is `NULL`,
 * the new node is inserted at the head of the list. `data` should remain valid
 * as long as the newly created node is in the list. The newly created node
 * does not take ownership of the memory associated with `data`.
 *
 * Complexity O(1).
 *
 * @return true if successful, false otherwise.
 */
bool list_insert(list_ptr l, list_node_ptr node, const void * data);

/**
 * Removes the node next to `node` in `l`. If `node` is `NULL`, the head of the
 * list is removed. Upon return, `data` will point to the data that was stored
 * in the removed node. This function does not destroy node's data.
 *
 * Complexity O(1).
 *
 * @return true if successful, false otherwise.
 */
bool list_remove(list_ptr l, list_node_ptr node, void ** data);

/**
 * Size of the list.
 *
 * Complexity O(1).
 *
 */
size_t list_size(const list_ptr l);

/* Complexity O(1) */
bool list_empty(list_ptr l);

/**
 * Returns the head node of the list or `NULL` if the list is empty.
 *
 * Complexity O(1).
 */
list_node_ptr list_head(const list_ptr l);

/**
 * Returns the tail node of the list or `NULL` if the list is empty.
 *
 * Complexity O(1).
 */
list_node_ptr list_tail(const list_ptr l);

/**
 * Whether a list node is a head.
 *
 * Complexity O(1).
 *
 * @return true if head, false otherwise.
 */
bool list_is_head(const list_ptr l, const list_node_ptr node);

/**
 * Whether a list node is a tail.
 *
 * Complexity O(1).
 *
 * @return true if head, false otherwise.
 */
bool list_is_tail(const list_node_ptr node);

/**
 * Returns data stored in a list node or `NULL` if node is empty.
 *
 * Complexity O(1).
 */
void * list_data(const list_node_ptr node);

/**
 * Returns the node next to `node` or `NULL` if `node` is last in list.
 *
 * Complexity O(1).
 */
list_node_ptr list_next(const list_node_ptr node);

destroy_fn list_get_destroy(list_ptr l);

void list_set_destroy(list_ptr l, destroy_fn destroy);

match_fn list_get_match(list_ptr l);

void list_set_match(list_ptr l, match_fn match);

struct dlist_t;
typedef struct dlist_t dlist;
typedef dlist * dlist_ptr;

struct dlist_node_t;
typedef struct dlist_node_t dlist_node;
typedef dlist_node * dlist_node_ptr;

dlist_ptr dlist_new();

/* Complexity O(1) */
void dlist_init(dlist_ptr dl, destroy_fn destroy);

/* Complexity O(n) */
void dlist_destroy(dlist_ptr dl);

/* Complexity O(1) */
bool dlist_insert(dlist_ptr dl, dlist_node_ptr node, const void * data);

/* Complexity O(1) */
bool dlist_insert_before(dlist_ptr dl, dlist_node_ptr, const void * data);

/**
 * Remove the node denoted by `node` from the list.
 *
 * Complexity O(1)
 *
 * @return true if successful, false otherwise.
 */
bool dlist_remove(dlist_ptr dl, dlist_node_ptr node, void ** data);

/* Complexity O(1) */
size_t dlist_size(dlist_ptr dl);

/* Complexity O(1) */
bool dlist_empty(dlist_ptr dl);

/* Complexity O(1) */
dlist_node_ptr dlist_head(const dlist_ptr dl);

/* Complexity O(1) */
dlist_node_ptr dlist_tail(const dlist_ptr dl);

/* Complexity O(1) */
bool dlist_is_head(const dlist_node_ptr node);

/* Complexity O(1) */
bool dlist_is_tail(const dlist_node_ptr node);

/* Complexity O(1) */
void * dlist_data(const dlist_node_ptr node);

/* Complexity O(1) */
dlist_node_ptr dlist_next(const dlist_node_ptr node);

/* Complexity O(1) */
dlist_node_ptr dlist_prev(const dlist_node_ptr node);


struct clist_t;
typedef struct clist_t clist;
typedef clist * clist_ptr;

struct clist_node_t;
typedef struct clist_node_t clist_node;
typedef clist_node * clist_node_ptr;

clist_ptr clist_new();

void clist_init(clist_ptr cl, destroy_fn destroy);

void clist_destroy(clist_ptr cl);

bool clist_insert(clist_ptr cl, clist_node_ptr node, const void * data);

bool clist_remove(clist_ptr cl, clist_node_ptr node, void ** data);

size_t clist_size(const clist_ptr cl);

bool clist_empty(clist_ptr cl);

clist_node_ptr clist_head(const clist_ptr cl);

bool clist_is_head(const clist_ptr cl, const clist_node_ptr node);

void * clist_data(const clist_node_ptr node);

clist_node_ptr clist_next(const clist_node_ptr node);

#endif /* LIST_H_ */
