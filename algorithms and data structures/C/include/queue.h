/*
 * queue.h
 *
 *  Created on: May 4, 2016
 *      Author: kalkaseer
 */

#ifndef C_QUEUE_H_
#define C_QUEUE_H_

#include "list.h"
#include "stack.h"
#include <stdbool.h>

typedef list queue;
typedef list_ptr queue_ptr;

#define queue_new list_new
#define queue_init list_init
#define queue_destroy list_destroy
#define queue_size list_size

bool queue_push(queue_ptr q, data_ptr data);

bool queue_pop(queue_ptr q, void ** data);

data_ptr queue_peek(queue_ptr q);


#endif /* C_QUEUE_H_ */
