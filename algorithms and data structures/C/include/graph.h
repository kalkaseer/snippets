/*
 * graph.h
 *
 *  Created on: Aug 7, 2016
 *      Author: kalkaseer
 */

#ifndef C_GRAPH_H_
#define C_GRAPH_H_

#include "common.h"
#include "list.h"

struct adjacency_list_t;
typedef struct adjacency_list_t adjacency_list;
typedef adjacency_list * adjacency_list_ptr;
typedef adjacency_list ** adjacency_list_sink;

struct graph_t;
typedef struct graph_t graph;
typedef graph * graph_ptr;

typedef struct _search_vertex {
  data_ptr     data;
  int          color;
  int          hops;
} search_vertex;

typedef search_vertex * search_vertex_ptr;


/* O(1) */
void graph_init(graph_ptr g, match_fn match, destroy_fn destroy);

/* O(V + E) */
void graph_destroy(graph_ptr g);

/* O(V) */
bool graph_insert_vertex(graph_ptr g, data_ptr v);

/* O(V) */
bool graph_insert_edge(graph_ptr g, data_ptr v1, data_ptr v2);

/* O(V + E) */
bool graph_erase_vertex(graph_ptr g, data_sink v);

/* O(V) */
bool graph_erase_edge(graph_ptr g, data_ptr v1, data_sink v2);

/* O(V) */
bool graph_adjacency_list(
  const graph_ptr g, data_ptr v, adjacency_list_sink alist);

/* O(V) */
bool graph_is_adjacent(const graph_ptr g, data_ptr v1, data_ptr v2);

/* O(1) */
list_ptr graph_structure(const graph_ptr g);

/* O(1) */
size_t graph_vertex_count(const graph_ptr g);

/* O(1) */
size_t graph_edge_count(const graph_ptr g);

/* O(V + E) */
bool graph_bfs(graph_ptr g, search_vertex start, list_ptr result);

/* O(V + E) */
bool graph_dfs(graph_ptr g, list_ptr result);

#endif /* C_GRAPH_H_ */
