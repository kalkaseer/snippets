/*
 * chained_hashtable.h
 *
 *  Created on: May 5, 2016
 *      Author: kalkaseer
 */

#ifndef C_CHAINED_HASHTABLE_H_
#define C_CHAINED_HASHTABLE_H_

#include <stdbool.h>
#include <stddef.h>
#include "common.h"

struct hashtable_t;
typedef struct hashtable_t hashtable;
typedef hashtable * hashtable_ptr;

typedef hashtable hashset;
typedef hashtable_ptr hashset_ptr;

hashtable_ptr htable_new();

/* Complexity O(m) m: is the number of buckets */
bool htable_init(
  hashtable_ptr t, size_t buckets,
  hash_fn hash, match_fn match, destroy_fn destroy);

/* Complexity O(m) m: is the number of buckets */
void htable_destroy(hashtable_ptr t);

/* Complexity O(1) */
bool htable_insert(hashtable_ptr t, data_ptr data);

/* Complexity O(1) */
bool htable_remove(hashtable_ptr t, data_sink data);

/* Complexity O(1) */
bool htable_get(hashtable_ptr t, data_sink data);

/* Complexity O(1) */
size_t htable_size(hashtable_ptr t);


#endif /* C_CHAINED_HASHTABLE_H_ */
