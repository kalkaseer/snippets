/*
 * set.h
 *
 *  Created on: May 4, 2016
 *      Author: kalkaseer
 */

#ifndef C_SET_H_
#define C_SET_H_

#include <stdbool.h>
#include <stddef.h>
#include "common.h"

#if 1 // defined(USE_LINKED_SET) && USE_LINKED_SET == 1

#include "list.h"

typedef list set;
typedef list_ptr set_ptr;
typedef list_node_ptr set_element_ptr;

#define set_new list_new
#define set_destroy list_destroy
#define set_size list_size
#define set_get_destroy list_get_destroy
#define set_set_destroy list_set_destroy
#define set_get_match list_get_match
#define set_set_match list_set_match
#define set_first list_head
#define set_data list_data

void set_init(set_ptr s, match_fn match, destroy_fn destroy);

/* Complexity: O(n) */
bool set_insert(set_ptr s, data_ptr data);

/* Complexity: O(n) */
bool set_remove(set_ptr s, data_sink data);

/* Complexity: O(mn) */
bool set_union(set_ptr out, const set_ptr s1, const set_ptr s2);

/* Complexity: O(mn) */
bool set_intersection(set_ptr out, const set_ptr s1, const set_ptr s2);

/* Complexity: O(mn) */
bool set_difference(set_ptr out, const set_ptr s1, const set_ptr s2);

/* Complexity: O(n) */
bool set_contains(const set_ptr s, data_ptr data);

/* Complexity: O(mn) */
bool set_is_subset(const set_ptr s1, const set_ptr s2);

/* Complexity: O(mn) */
bool set_equal(const set_ptr s1, const set_ptr s2);

set_element_ptr set_next(const set_ptr s, const set_element_ptr e);

#elif defined(USE_HASH_SET) && USE_HASH_SET == 1

/* TODO */

#endif

#endif /* C_SET_H_ */
