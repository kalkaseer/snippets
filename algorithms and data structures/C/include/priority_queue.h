/*
 * priority_queue.h
 *
 *  Created on: Aug 7, 2016
 *      Author: kalkaseer
 */

#ifndef C_PRIORITY_QUEUE_H_
#define C_PRIORITY_QUEUE_H_

#include "heap.h"

typedef heap pqueue;

typedef heap_ptr pqueue_ptr;

#define pqueue_init heap_init;

#define pqueue_destory heap_destroy;

#define pqueue_push heap_push;

#define pqueue_pop heap_pop;

#define pqueue_peek heap_peek;

#define pqueue_size heap_size;

#endif /* C_PRIORITY_QUEUE_H_ */
