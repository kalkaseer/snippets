/*
 * stack.h
 *
 *  Created on: May 4, 2016
 *      Author: kalkaseer
 */

#ifndef C_STACK_H_
#define C_STACK_H_

#include <stdbool.h>
#include <stddef.h>
#include "common.h"
#include "list.h"

struct stack_t;
typedef struct stack_t stack;
typedef stack * stack_ptr;

stack_ptr stack_new();

bool stack_init(stack_ptr s, destroy_fn destroy, size_t size);

void stack_destory(stack_ptr s);

bool stack_push(stack_ptr s, data_ptr data);

bool stack_pop(stack_ptr s, void ** data);

data_ptr stack_peek(stack_ptr s);

size_t stack_size(stack_ptr s);


typedef list linked_stack;
typedef list_ptr linked_stack_ptr;

#define linked_stack_new list_new
#define linked_stack_init list_init
#define linked_stack_destroy list_destroy
#define linked_stack_size list_size

bool linked_stack_push(linked_stack_ptr s, data_ptr data);

bool linked_stack_pop(linked_stack_ptr s, void ** data);

data_ptr linked_stack_peek(linked_stack_ptr s);

#endif /* C_STACK_H_ */
