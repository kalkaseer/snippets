/*
 * heap_private.h
 *
 *  Created on: May 20, 2016
 *      Author: kalkaseer
 */

#ifndef C_PRIVATE_HEAP_PRIVATE_H_
#define C_PRIVATE_HEAP_PRIVATE_H_

#include <stdlib.h>

typedef bool (*heap_should_swap_fn)
  (struct heap_t * h, const void * parent, const void * child);

struct heap_t {
  size_t              size;
  compare_fn          compare;
  heap_should_swap_fn should_swap;
  destroy_fn          destroy;
  void                ** data;
};

static inline bool max_heap_should_swap(
  struct heap_t * h, const void * parent, const void * child
) {
  return h->compare(parent, child) > 0;
}

static inline bool min_heap_should_swap(
  struct heap_t * h, const void * parent, const void * child
) {
  return h->compare(parent, child) < 0;
}


#endif /* C_PRIVATE_HEAP_PRIVATE_H_ */
