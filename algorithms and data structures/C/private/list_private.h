/*
 * list_private.h
 *
 *  Created on: May 20, 2016
 *      Author: kalkaseer
 */

#ifndef C_PRIVATE_LIST_PRIVATE_H_
#define C_PRIVATE_LIST_PRIVATE_H_

#include <stdlib.h>

struct list_node_t {
  void      * data;
  list_node * next;
};

struct list_t {
  size_t        size;
  match_fn      match;
  destroy_fn    destroy;
  list_node_ptr head;
  list_node_ptr tail;
};

struct dlist_node_t {
  void          * data;
  dlist_node_ptr  next;
  dlist_node_ptr  prev;
};

struct dlist_t {
  size_t         size;
  match_fn       match;
  destroy_fn     destroy;
  dlist_node_ptr head;
  dlist_node_ptr tail;
};

struct clist_node_t {
  void       * data;
  clist_node * next;
};

struct clist_t {
  size_t         size;
  match_fn       match;
  destroy_fn     destroy;
  clist_node_ptr head;
};

static inline struct list_t * list_make() {
  return (struct list_t *) malloc(sizeof(struct list_t));
}

static inline struct list_node_t * list_node_make() {
  return (struct list_node_t *) malloc(sizeof(struct list_node_t));
}

static inline struct dlist_t * dlist_make() {
  return (struct dlist_t *) malloc(sizeof(struct dlist_t));
}

static inline struct dlist_node_t * dlist_node_make() {
  return (struct dlist_node_t *) malloc(sizeof(struct dlist_node_t));
}

static inline struct clist_t * clist_make() {
  return (struct clist_t *) malloc(sizeof(struct clist_t));
}

static inline struct clist_node_t * clist_node_make() {
  return (struct clist_node_t *) malloc(sizeof(struct clist_node_t));
}


#endif /* C_PRIVATE_LIST_PRIVATE_H_ */
