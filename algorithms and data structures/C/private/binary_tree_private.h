/*
 * bitree_private.h
 *
 *  Created on: May 20, 2016
 *      Author: kalkaseer
 */

#ifndef C_PRIVATE_BINARY_TREE_PRIVATE_H_
#define C_PRIVATE_BINARY_TREE_PRIVATE_H_


struct bitree_node_t {
  void          * data;
  bitree_node_ptr left;
  bitree_node_ptr right;
};

struct bitree_t {
  size_t          size;
  destroy_fn      destroy;
  bitree_node_ptr root;
};

static inline struct bitree_node_t * bitree_node_make() {
  return (struct bitree_node_t *) malloc(sizeof(struct bitree_node_t));
}

static inline struct bitree_t * bitree_make() {
  return (struct bitree_t *) malloc(sizeof(struct bitree_t));
}


#endif /* C_PRIVATE_BINARY_TREE_PRIVATE_H_ */
