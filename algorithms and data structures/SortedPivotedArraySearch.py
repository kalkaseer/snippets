# Find element in a non-decreasingly sorted and rotated array in O(log n)
#
# [3 4 5 1 2], [5 1 2 3 4]
#
# The idea is to find the pivot element then use binary search
# The pivot element is the only element whose next element is smaller
#
# 1. find pivot and split the array
# 2. do binary search in the two halves
#  a) if a[i] > a[0], search in left half
#  b) else search in right half
# 3. if element is found return index, else return -1

def find_pivot(a, lo, hi):
  
  if hi < lo: return -1
  if hi == lo: return lo
  
  mid = (lo + hi) // 2
  
  if mid < hi and a[mid] > a[mid+1]:
    return mid
  if mid > lo and a[mid] < a[mid-1]:
    return mid-1
  if a[lo] >= a[mid]:
    return find_pivot(a, low, mid - 1)
  return find_pivot(a, mid+1, hi)


def binary_search(a, lo, hi, key):
  
  while lo <= hi:
    
    mid = (lo + hi) // 2
    
    if key == a[mid]:
      return mid
    
    if a[mid] < key:
      lo = mid + 1
    else:
      hi = mid - 1
  
  return -1
    


def pivoted_binary_search(a, key):
  
  hi = len(a) - 1
  pivot = find_pivot(a, 0, hi)
  
  # array is not rotated
  if pivot == -1:
    return binary_search(a, 0, hi, key)
  
  if a[pivot] == key:
    return pivot
  if a[0] <= key:
    return binary_search(a, 0, pivot-1, key)
  return binary_search(a, pivot+1, hi, key)


def test():
  a = [5, 6, 7, 8, 9, 10, 1, 2, 3]
  key = 3
  assert pivoted_binary_search(a, key) == 8
  