# Manasa and Stones
#
# https://www.hackerrank.com/challenges/manasa-and-stones

# Combination without repetition 

def solution(n, a, b):
    s = set()
    for i in xrange(n):
        s.add(a * i + b * (n - i - 1))
    return s

def start():
    t = input()
    for _ in xrange(t):
        n = input()
        a = input()
        b = input()
        print '=>', sorted(list(solution(n, a, b)))

if __name__ == '__main__':
    start()