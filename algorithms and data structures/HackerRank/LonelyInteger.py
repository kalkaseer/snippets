# Lonely Integer
#
# https://www.hackerrank.com/challenges/lonely-integer

def lonely_integer(a):
    acc = 0
    for n in a:
        acc ^= n
    return acc

def start():
    length = input()
    a = map(int, raw_input().split())
    print lonely_integer(a)

if __name__ == '__main__':
    start()