def solve(grid, start, end):

  from collections import deque
  
  deadend = -1
  unvisited = 0
  
  distances = [0] * len(grid)
  for i in xrange(len(grid)):
    distances[i] = []
    for p in grid[i]:
      if p == 'X':
        distances[i].append(deadend)
      else:
        distances[i].append(unvisited)
  
  q = deque([start])
  distances[start[0]][start[1]] = 0
  
  while q:
    p = q.popleft()
    x, y = p
    d = distances[x][y] + 1
    
    if ((x, y) == end):
      break
    
    while x > 0:
      x -= 1
      if distances[x][y] == unvisited:
        distances[x][y] = d
        q.append((x,y))
      else:
        break
    
    x = p[0]
    while x < len(distances)-1:
      x += 1
      if distances[x][y] == unvisited:
        distances[x][y] = d
        q.append((x,y))
      else:
        break
    
    x = p[0]
    while y > 0:
      y -= 1
      if distances[x][y] == unvisited:
        distances[x][y] = d
        q.append((x,y))
      else:
        break
    
    y = p[1]
    while y < len(distances)-1:
      y += 1
      if distances[x][y] == unvisited:
        distances[x][y] = d
        q.append((x,y))
      else:
        break
  
  return distances[end[0]][end[1]]
  
if __name__ == '__main__':
    n = input()
    grid = []
    for _ in xrange(n):
        grid.append(raw_input().strip())
    x1, y1, x2, y2 = map(int, raw_input().strip().split(' '))

    print solve(grid, (x1,y1), (x2,y2))
    
    
testcases = [
  (['.X.', '.X.', '...'], (0,0), (0,2), 3),
  (['...', 'X..', '.X.'], (0,0), (0,2), 2),
  (['...X..', 'X.X...', '..X...', '.X....', '...X..'], (0,0), (4,4), 8)
  ([
    '...X.......',
    'X.X...XX...',
    '..X..X..X.X',
    '.X..X.....X',
    '...X.......',
    '...X.......',
    'X.X...XX...',
    '..X..X..X.X',
    '.X..X.....X',
    '...X.......',
    '...X.......'
  ], (0,0),(10,10), 15)
]