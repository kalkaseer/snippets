#coding: utf-8

# Diagonal Difference
#
# https://www.hackerrank.com/challenges/diagonal-difference
#
# Given a square matrix of size , calculate the absolute difference between
# the sums of its diagonals.
#
# Input Format
# The first line contains a single integer, N. The next N lines denote the
# matrix's rows, with each line containing space-separated integers
# describing the columns.

def _diagonal_difference(v):
    diff = 0
    length = len(v)
    for i in xrange(length):
        diff += v[i][i]
        diff -= v[i][length - i - 1]
    return abs(diff)

def diagonal_difference(s):
    lines = s.split('\n')
    v = []
    n = int(lines[0])
    for i in xrange(1, n):
        e = map(int, lines[i].split())
        v.append(e)
    return _diagonal_difference(v)

if __name__ == '__main__':
    n = input()
    v = []
    for _ in xrange(n):
        e = map(int, raw_input().split())
        v.append(e)
    print _diagonal_difference(v)