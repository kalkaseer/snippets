# Encryption
#
# https://www.hackerrank.com/challenges/encryption

from math import sqrt, floor, ceil

def solve(s):
  
  L = len(s)
  LR = sqrt(L)
  F = int(floor(LR))
  C = int(ceil(LR))
  
  print '(F, C)', (floor(LR), ceil(LR))
  
  grid = []
  
  row = 0
  
  written = 0
  
  while written < L:
    column = [s[i+written] for i in xrange(F) if i+written < L]
    written += len(column)
    grid.append(column)
    
  
  return grid
  
def solve2(s):
  
  L = len(s)
  root = sqrt(L)
  height = int(ceil(root))
  
  result = ''
  
  for i in xrange(height):
    j = i
    while j < L:
      result += s[j]
      j += height
    result += ' '
  
  return result

def test():
  s = 'if man was meant to stay on the ground god would have given us roots'
  s = s.replace(' ', '')
  res = solve(s)
  for i in res:
    print ''.join(i),

def start():
  s = raw_input().strip()
  # res = solve(s)
  #   for i in res: print ''.join(i),
  print solve2(s)