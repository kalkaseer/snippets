# Utopian Tree
#
# https://www.hackerrank.com/challenges/utopian-tree

def growth(cycles):
    m = 1
    if cycles == 0:
        return m
    if cycles % 2 == 1:
        m *= 2
    for _ in xrange(cycles / 2):
        m *= 2
        m += 1
    
    return m


def start():
    length = input()
    for _ in xrange(length):
        print growth(input())


if __name__ == '__main__':
    start()