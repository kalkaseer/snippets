# Shelock and GCD
#
# https://www.hackerrank.com/challenges/sherlock-and-gcd

# GCD is commutative: GCD(a,b,c,d) == GCD(GCD(GCD(a,b), c), d)

def gcd(a, b):
    if a % b == 0:
        return b
    else:
        return gcd(a, a % b)

def solution(v):
    return reduce(lambda x, y: gcd(x, y), v)

def start():
    t = input()
    for _ in xrange(t):
        n = input()
        v = map(int, raw_input().split())
        if len(v) < 2:
            print '=> NO'
        elif solution(v) == 1:
            print '=> YES'
        else:
            print '=> NO'

if __name__ == '__main__':
    start()