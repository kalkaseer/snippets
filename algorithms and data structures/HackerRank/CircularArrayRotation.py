# CircularArrayRotation
#
# https://www.hackerrank.com/challenges/circular-array-rotation


def solve(a, k, x):
    return a[(x - k) % len(a)]

def start():
    n, k, q = map(int, raw_input().split())
    a = map(int, raw_input().split())
    for _ in xrange(q):
        x = input()
        print solve(a, k, x)

if __name__ == '__main__':
    start()