# Library Fine
#
# https://www.hackerrank.com/challenges/library-fine
#
# Your local library needs your help! Given the expected and actual
# return dates for a library book, create a program that calculates the
# fine (if any). The fee structure is as follows:
#     If the book is returned on or before the expected return date, no fine
#     will be charged
#
#     If the book is returned after the expected return day but still
#     within the same calendar month and year as the expected return date,
#     fine = 15 * number of late days
# 
#     If the book is returned after the expected return month but still
#     within the same calendar year as the expected return date,
#     fine = 500 * number of late months
# 
#     If the book is returned after the calendar year in which it
#     was expected, there is a fixed fine of 10000.
# 
# Input Format
# 
# The first line contains space-separated integers denoting the respective
# day, month, and year on which the book was actually returned.
# The second line contains space-separated integers denoting the respective
# day, month, and year on which the book was expected to be returned (due date).
# 
# 
# Constraints
#   1 <= D <= 31
#   1 <= M <= 12
#   1 <= Y <= 3000
#   All dates are valid Gregorian calendar dates.



from datetime import date

def library_fine(actual, due):
    a = date(actual[2], actual[1], actual[0])
    d = date(due[2], due[1], due[0])
    if a.year > d.year: return 10000
    if a.year < d.year: return 0
    if a.month > d.month: return (a.month - d.month) * 500
    if a.month < d.month: return 0
    if a.day > d.day: return (a.day - d.day) * 15
    return 0


if __name__ == '__main__':
    actual = map(int, raw_input().split())
    due = map(int, raw_input().split())
    print library_fine(actual, due)