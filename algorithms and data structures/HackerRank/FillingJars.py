# Filling Jars
#
# https://www.hackerrank.com/challenges/filling-jars


def solution(jars_count, operations):
    acc = 0
    for op in operations:
        acc += (abs(op[0] - op[1]) + 1) * op[2]
    return acc // jars_count


def start():
    jars_count, operations_count = map(int, raw_input().split())
    operations = []
    for _ in xrange(operations_count):
        operations.append(map(int, raw_input().split()))
    print solution(jars_count, operations)
    

if __name__ == '__main__':
    start()