# Kaprekar numbers
# 
# https://www.hackerrank.com/challenges/kaprekar-numbers

from math import pow

def is_kaprekar(n):
  square = str(long(pow(n,2)))
  if len(square) > 1:
    mid = len(square) // 2
    l = int(square[:mid])
    r = int(square[mid:])
    return l+r == n
  else:
    return int(square) == n

def start():
  start = input()
  end = input()
  
  found = False
  for x in xrange(start, end+1):
    if is_kaprekar(x):
      found = True
      print x,
  
  if not found: print 'INVALID RANGE',