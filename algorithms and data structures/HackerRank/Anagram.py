# Anagram
#
# https://www.hackerrank.com/challenges/anagram

from collections import defaultdict

def solve(s):
    
    if len(s) % 2 == 1:
        return -1
    
    mid = len(s) // 2
    s1 = s[:mid]
    s2 = s[mid:]
    
    m1 = defaultdict(int)
    m2 = defaultdict(int)
    
    for c in s1: m1[c] += 1
    for c in s2: m2[c] += 1
    
    diff = 0
    
    for k in m2.iterkeys():
        if k not in m1:
            diff += m2[k]
        else:
            diff += max(0, m2[k] - m1[k])
    
    return diff


def start():
    t = input()
    for _ in xrange(t):
        s = raw_input().strip()
        print solve(s)


if __name__ == '__main__':
    start()