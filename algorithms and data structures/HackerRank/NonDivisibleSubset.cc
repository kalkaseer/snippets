#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
#include <string>

using namespace std;

/*
 * Non Divisible Subset
 *
 * https://www.hackerrank.com/challenges/non-divisible-subset
 *
 * For numbers n1 and n2:
 *   n1 % k == r, n2 % k = k - r
 * if (n1 + n2) % k = 0
 * 
 * Iterating over set S and calculating the two sets:
 *  1) numbers with remainder of r and 2) numbers with remainder of k-r
 * The maximum size of S' is the size of larger set in these two sets.
 * When k = even, the sets will be centered around k/2, if a remainder is
 * equal to k/2 we need to increment the size by 1. Also we need to single
 * consider numbers that divide k. They will be out of those two sets above.
 * We need to increment the size by one if S includes any such number.
 */

int main() {
    
    size_t n;
    size_t k;
    string s;
    vector<size_t> v;
    
    getline(cin, s);
    
    size_t start = 0;
    for(size_t i = 0; i < s.size(); ++ i) {
        if (s[i] == ' ') {
            n = stoull(string(s.begin() + start, s.begin() + i));
            start = i + 1;
        }
    }
    k = stoull(string(s.begin() + start, s.end()));
    
    getline(cin, s);
    
    start = 0;
    for(size_t i = 0; i < s.size(); ++ i) {
        if (s[i] == ' ') {
            v.push_back(stoull(string(s.begin() + start, s.begin() + i)));
            start = i + 1;
        }
    }
    v.push_back(stoull(string(s.begin() + start, s.end())));
    
    
    vector<size_t> u(k, 0);
    
    size_t m{ 0 };
    
    for (auto x : v)
        u[ x % k ] += 1;
    
    for (size_t i = 1; i < (k+1) / 2; ++i)
        m += max(u[i], u[k-i]);
    
    if (k % 2 == 0 && u[k/2])
        ++m;
    
    if (u[0])
        ++m;
    
    cout << m;
    
    return 0;
}