# Sherlock and Squares
#
# https://www.hackerrank.com/challenges/sherlock-and-squares


from math import *

def solution(a, b):
    return int(floor(sqrt(b)) - ceil(sqrt(a))) + 1


def start():
    n = input()
    for _ in xrange(n):
        a, b = map(int, raw_input().strip().split())
        print solution(a, b)


if __name__ == '__main__':
    start()