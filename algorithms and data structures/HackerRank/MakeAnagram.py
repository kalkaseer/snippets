# Make Anagram
#
# Alice recently started learning about cryptography and found that anagrams are
# very useful. Two strings are anagrams of each other if they have same character
# set. For example strings"bacdc" and "dcbac" are anagrams, while strings "bacdc"
# and "dcbad" are not.
# 
# Alice decides on an encryption scheme involving 2 large strings where encryption
# is dependent on the minimum number of character deletions required to make the
# two strings anagrams. She need your help in finding out this number.
# 
# Given two strings (they can be of same or different length) help her in finding
# out the minimum number of character deletions required to make two strings
# anagrams. Any characters can be deleted from any of the strings.

from collections import defaultdict

def solve(s1, s2):
    m1 = defaultdict(int)
    m2 = defaultdict(int)
    
    for c in s1: m1[c] += 1
    for c in s2: m2[c] += 1
    
    diff = 0
    
    for c in m1.iterkeys():
        if c not in m2:
            diff += m1[c]
        else:
            diff += max(0, m1[c] - m2[c])
    
    for c in m2.iterkeys():
        if c not in m1:
            diff += m2[c]
        else:
            diff += max(0, m2[c] - m1[c])
    
    return diff


def start():
    s1, s2 = raw_input(), raw_input()
    print '=>', solve(s1, s2)


if __name__ == '__main__':
    start()