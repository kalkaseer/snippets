# Maximizing XOR
#
# https://www.hackerrank.com/challenges/maximizing-xor

def max_xor(l, r):
    mx = 0
    for low in xrange(l, r+1):
        for high in xrange(low, r+1):
            mx = max(mx, low ^ high)
    return mx

def start():
    l = input()
    r = input()
    print max_xor(l, r)

if __name__ == '__main__':
    start()