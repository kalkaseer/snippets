# Sherlock and Pairs
#
# https://www.hackerrank.com/challenges/sherlock-and-pairs


# Count all integers in array, then there are choose(k,2)*2 for each integer occuring
# more than once. Note that (i, j) is different from (j, i), it is counted twice.

from collections import defaultdict

def solve(a):
    m = defaultdict(int)
    count = 0
    for k in a:
        m[k] += 1
    for c in m.itervalues():
        if c != 1:
            count += (c * (c-1))
    return count


def start():
    t = input()
    for _ in xrange(t):
        _ = input()
        a = map(int, raw_input().split())
        print '=>', solve(a)


if __name__ == '__main__':
    start()