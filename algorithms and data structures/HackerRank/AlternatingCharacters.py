# Alternating Characters
#
# https://www.hackerrank.com/challenges/alternating-characters

def make_string(s):
    cnt = 0
    for i in xrange(1, len(s)):
        if s[i] == s[i-1]:
            cnt += 1
    return cnt


def start():
    n = input()
    for _ in xrange(n):
        print make_string(raw_input())


if __name__ == '__main__':
    start()