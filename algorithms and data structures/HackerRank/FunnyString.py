# Funny String
#
# https://www.hackerrank.com/challenges/funny-string

def solve(s):
    length = len(s)
    for i in xrange(length // 2):
        if abs(ord(s[i]) - ord(s[i+1])) != abs(ord(s[length-i-1]) - ord(s[length-i-2])):
            return False
    return True


def start():
    t = input()
    for _ in xrange(t):
        s = raw_input()
        print '=>', solve(s)


if __name__ == '__main__':
    start()