# Two Strings
#
# https://www.hackerrank.com/challenges/two-strings


def solve(s1, s2):
    aset1 = set(s1)
    aset2 = set(s2)
    if set.intersection(aset1, aset2):
        return 'YES'
    return 'No'


def start():
    t = input()
    for _ in xrange(t):
        print solve(raw_input().strip(), raw_input().strip())


if __name__ == '__main__':
    start()