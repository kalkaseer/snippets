# Jumps on the Clounds
#
# https://www.hackerrank.com/challenges/jumping-on-the-clouds

def solve(clouds):
  steps = 0
  i = 0
  while i < len(clouds) - 1:
    if i+2 < len(clouds) and clouds[i+2] == 0:
      i += 2
    elif clouds[i+1] == 1:
      i += 2
    else:
      i += 1
    
    steps += 1
  
  return steps