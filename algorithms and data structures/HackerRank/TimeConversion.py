# Time Conversion
#
# https://www.hackerrank.com/challenges/time-conversion
#
# Given a time in AM/PM format, convert it to military (-hour) time.
# 
# Note: Midnight is on a -hour clock, and on a -hour clock.
# Noon is on a -hour clock, and on a -hour clock.
#
# Input Format
# A single string containing a time in 12-hour clock format
# (i.e.: hh:mm:ssAM or hh:mm:ssPM), where 01 <= hh <=12.

from datetime import datetime

def convert_time(timestr):
    return datetime.strptime(timestr, '%I:%M:%S%p').strftime('%H:%M:%S')