# Angry Professor
#
# https://www.hackerrank.com/challenges/angry-professor

def solve(threshold, records):
    count = 0
    for r in records:
        if r <= 0:
            count += 1
    if count < threshold:
        return True
    return False


def start():
    t = input()
    for _ in xrange(t):
        _, k = map(int, raw_input().split())
        records = map(int, raw_input().split())
        print '=>', 'YES' if solve(k, records) else 'NO'


if __name__ == '__main__':
    start()