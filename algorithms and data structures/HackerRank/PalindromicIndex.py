# Palindromic Index
#
# https://www.hackerrank.com/challenges/palindrome-index

def isPalindrome(s):
    for i in xrange(len(s) // 2):
        if s[i] != s[len(s) - i - 1]:
            return False
        return True

def palindromicIndex(s):
    for i in xrange(len(s) + 1 // 2):
        if s[i] != s[len(s) - i - 1]:
            if isPalindrome(s[:i] + s[i+1:]):
                return i
            else:
                return len(s) - i - 1
    return -1

def start():
    t = input()
    for _ in xrange(t):
        print palindromicIndex(raw_input().strip())

if __name__ == '__main__':
    start()