# coding: utf-8

# Matrix Rotation
# 
# https://www.hackerrank.com/challenges/matrix-rotation-algo


'''
a00 ← a01 ← a02 ← a03 ← a04 ← a05
↓                              ↑
a10   a11 ← a12 ← a13 ← a14   a15
↓      ↓                 ↑     ↑
a20   a21   a22 ← a23   a24   a25
↓      ↓     ↓     ↑     ↑     ↑
a30   a31   a32 → a33   a34   a35
↓      ↓                 ↑     ↑
a40   a41 → a42 → a43 → a44   a45
↓                              ↑
a50 → a51 → a52 → a53 → a54 → a55


a00 ← a01 ← a02 ← a03
↓                  ↑ 
a10   a11 ← a12   a13
↓      ↓     ↑     ↑ 
a20   a21   a22   a23
↓      ↓     ↑     ↑ 
a30   a31 → a32   a33
↓                  ↑ 
a40 → a41 → a42 → a43
'''

nomove = 0
up = 1
left = 2
down = 3
right = 4

def min_layer(n, m):
    i = n // 2 if n % 2 == 0 else n // 2 + 1
    j = m // 2 if m % 2 == 0 else m // 2 + 1
    return i, j

def make_matrix(n, m):
    return [[0 for i in xrange(m)] for i in xrange(n)]

def solve(matrix, k):
    
    n = len(matrix)
    m = len(matrix[0])
    
    width  = m
    height = n
    
    new_matrix = make_matrix(n, m)
    
    min_i, min_j = min_layer(n, m)
    
    def first_pos(dir):
        if dir == up:
            return (n-height) // 2 if (n-height)%2 == 0 else (n-height) // 2 + 1
        return (m-width) // 2 if (m-width)%2 == 0 else (m-width) // 2 + 1
    
    def last_pos(dir):
        if dir == right:
            return (m+width) // 2 - 1
        return (n+height) // 2 - 1
    
    def move(pos, direction, k):
        if direction == up:
            first = first_pos(up)
            if pos[0] - k >= first:
                i = pos[0] - k
                j = pos[1]
                return (i, j), nomove, 0
            else:
                i = first
                return (i, pos[1]), left, k - pos[0] + i
        elif direction == left:
            first = first_pos(left)
            if pos[1] - k >= first:
                i = pos[0]
                j = pos[1] - k
                return (i, j), nomove, 0
            else:
                j = first
                return (pos[0], j), down, k - pos[1] + j
        elif direction == right:
            last = last_pos(right)
            if pos[1] + k <= last:
                i = pos[0]
                j = pos[1] + k
                return (i, j), nomove, 0
            else:
                j = last
                return (pos[0], j), up, k + pos[1] - j
        else:
            last = last_pos(down)
            if pos[0] + k <= last:
                i = pos[0] + k
                j = pos[1]
                return (i, j), nomove, 0
            else:
                i = last
                return (i, pos[1]), right, k + pos[0] - i
    
    def full_move(pos, direction, k):
        npos, dir, rem = move(pos, direction, k)
        while dir != nomove:
            npos, dir, rem = move(npos, dir, rem)
        return npos
    
    def process(pos, width, height):
        for p, dir in increment(pos):
            rpos = full_move(p, dir, k)
            new_matrix[rpos[0]][rpos[1]] = matrix[p[0]][p[1]]
    
    def increment(pos):
        
        yield pos, up
    
        dir = up
        p = pos
    
        for _ in xrange(height - 1):
            p, _, _ = move(p, dir, 1)
            yield p, dir
        dir = left
        for _ in xrange(width - 1):
            p, _, _ = move(p, dir, 1)
            yield p, dir
        dir = down
        for _ in xrange(height - 1):
            p, _, _ = move(p, dir, 1)
            yield p, dir
        dir = right
        for _ in xrange(width - 2):
            p, _, _ = move(p, dir, 1)
            yield p, dir
    
    
    pos = (height - 1, width - 1)
    while pos[0] >= min_i:
        process(pos, width, height)
        width -= 2
        height -= 2
        pos = (pos[0] - 1, pos[1] - 1)
    
    return new_matrix

testcase = '''4 4 1
1 2 3 4
5 6 7 8
9 10 11 12
13 14 15 16'''

testcase2 = '''5 4 7
1 2 3 4
7 8 9 10
13 14 15 16
19 20 21 22
25 26 27 28'''

def print_matrix(m):
    for r in m:
        s = ''.join(['%3d ' for x in r])
        print s % tuple(r)

def start():
    
    matrix = []
    n, m, k = map(int, raw_input().strip().split())
    for i in xrange(n):
        matrix.append(map(int, raw_input().strip().split()))
    print_matrix(matrix)
    print '\n'
    print_matrix(solve(matrix, k))

def test():
    matrix = []
    lines = testcase2.split('\n')
    n, m, k = map(int, lines[0].strip().split())
    for i in xrange(1, len(lines)):
        matrix.append(map(int, lines[i].strip().split()))
    print_matrix(matrix)
    print '\n'
    print_matrix(solve(matrix, k))