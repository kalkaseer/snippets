# Plus Minus
#
# https://www.hackerrank.com/challenges/plus-minus
#
# Given an array of integers, calculate which fraction of its elements are
# positive, which fraction of its elements are negative, and which fraction
# of its elements are zeroes, respectively. Print the decimal value of each
# fraction on a new line.
#
# Note: This challenge introduces precision problems. The test cases are
# scaled to six decimal places, though answers with absolute error of up
# to are acceptable.
#
# Input Format
#
# The first line contains an integer, N, denoting the size of the array.
# The second line contains space-separated integers describing an array
# of numbers

def _plus_minus(a):
    pos, neg, zero = 0.0, 0.0, 0.0
    length = len(a)
    for v in a:
        if v > 0:   pos  += 1
        elif v < 0: neg  += 1
        else:       zero += 1
    return round(pos/length, 4), round(neg/length, 4), round(zero/length, 4)

def plus_minus(s):
    lines = s.split('\n')
    a = map(int, lines[1].split())
    return _plus_minus(a)


if __name__ == '__main__':
    n = input()
    a = map(int, raw_input().split())
    r = _plus_minus(a)
    for i in r:
        print round(i, 4)