# Missing Numbers
# 
# https://www.hackerrank.com/challenges/missing-numbers

from collections import defaultdict

def solve(a1, a2):
    
    d1 = defaultdict(int)
    d2 = defaultdict(int)
    
    for e in a1: d1[e] += 1
    for e in a2: d2[e] += 1
    
    missing = []
    for e in d2.iterkeys():
        if e not in d1 or d2[e] > d1[e]:
            missing.append(e)
    
    missing.sort()
    return missing


def another_solve(a1, a2):
    
    a1count = [0] * 101
    a2count = [0] * 101
    offset = min(a2)
    
    for e in a2: a2count[e - offset] += 1
    for e in a1: a1count[e - offset] += 1
    
    missing = []
    
    for i in xrange(101):
        if a2count != a1count:
            missing.append(i + offset)

def start():
    a1 = map(int, raw_input().strip().split())
    a2 = map(int, raw_input().strip().split())
    print solve(a1, a2)


if __name__ == '__main__':
    start()