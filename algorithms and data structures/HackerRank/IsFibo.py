# coding: utf-8

# Is Fibo
#
# https://www.hackerrank.com/challenges/is-fibo

# N is a Fibbonaci number only if 5 N^2 + 4 or 5N^2 – 4 is a square number

from math import sqrt

def is_perfect_square(n):
    if n < 0:
        return False
    m = long(sqrt(n) + 0.5)
    return m * m == n
    
def solve(n):
    k = 5 * (n**2)
    return is_perfect_square(k + 4) or is_perfect_square(k - 4)

def start():
    t = input()
    for _ in xrange(t):
        n = input()
        print '=>', solve(n)

if __name__ == '__main__':
    start()