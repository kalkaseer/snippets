# Flipping Bits
#
# https://www.hackerrank.com/challenges/flipping-bits


def flip(n):
    return n ^ (2**32 - 1) # 32bit of 1's

def start():
    length = input()
    for _ in xrange(length):
        print flip(input())

if __name__ == '__main__':
    start()