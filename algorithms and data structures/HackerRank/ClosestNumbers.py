# Closest Numbers
#
# https://www.hackerrank.com/challenges/closest-numbers

from sys import maxint

def solve(a):
    a.sort()
    diff = maxint
    pairs = []
    
    for i in xrange(len(a) - 1):
        cdiff = a[i+1] - a[i]
        if cdiff < diff:
            diff = cdiff
            pairs = [(a[i], a[i+1])]
        elif cdiff == diff:
            pairs.append((a[i], a[i+1]))
    
    return pairs


def start():
    n = input()
    a = map(int, raw_input().split())
    res = solve(a)
    if res:
      for i in res:
        print i[0], i[1],


if __name__ == '__main__':
    start()