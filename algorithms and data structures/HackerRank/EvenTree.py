# Even Tree
#
# https://www.hackerrank.com/challenges/even-tree


def solve(edges):
  
  from collections import defaultdict
  
  v = [0] * (len(edges) + 2)
  d = defaultdict(int)
  
  for e in edges:
    child, parent = e
    v[child] = parent
    if parent not in d:
      d[parent] = 1
    if child not in d:
      d[child] = 1
    d[parent] += 1
  
  res = 0
  
  for k in xrange(len(v) - 1, 1, -1):
    if d[k] % 2 == 0:
      is_even = False
      for i in xrange(len(v)-1, 1, -1):
        if v[i] == i:
          if d[i] % 2 == 0:
            is_even = True
            break

      if not is_even:
        res += 1
        parent = v[k]
        v[k] = 0
        d[parent] -= 1
  
  return res