# Staircase
#
# https://www.hackerrank.com/challenges/staircase
#
# Consider a staircase of size :
#
#    #
#   ##
#  ###
# ####
#
# Observe that its base and height are both equal to , and the image is
# drawn using # symbols and spaces. The last line is not preceded by
# any spaces.
#
# Write a program that prints a staircase of size .

def staircase(n):
    for i in xrange(n):
        s = ''
        k = n - i - 1
        for _ in xrange(k):
            s += ' '
        for _ in xrange(n - k):
            s += '#'
        print s


def staircase2(n):
    for i in xrange(1, n + 1):
        print ('#' * i).rjust(n)