# Sherlock and Valid String
#
# https://www.hackerrank.com/challenges/sherlock-and-valid-string


def solve(s):
    from collections import Counter
    charmap = Counter(s)
    freqmap = Counter(charmap.values())
    if len(freqmap) == 1:
        return True
    if len(freqmap) == 2:
        for v in freqmap.values():
            if v == 1:
                return True
    return False


def start():
    print solve(raw_input().strip())


if __name__ == '__main__':
    start()