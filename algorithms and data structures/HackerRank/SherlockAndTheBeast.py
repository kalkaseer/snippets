# Sherlock and the Beast
#
# https://www.hackerrank.com/challenges/sherlock-and-the-beast

def divide(n):
  fives = n
  while fives >= 0 and (n - fives) <= n:
    if fives % 3 == 0 and (n-fives) % 5 == 0:
      return fives
    fives -= 5
  return -1

def solve(n):
  fives = divide(n)
  if fives == -1: return '-1'
  return ('5'*fives) + ('3'*(n-fives))

