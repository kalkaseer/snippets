# Panagrams
#
# https://www.hackerrank.com/challenges/pangrams

def solve(s):
    a = [0] * 26
    d = ord('a')
    for c in s:
        if c != ' ':
            a[ord(c.lower()) - d] += 1
    for k in a:
        if k == 0:
            return False
    return True


def start():
    s = raw_input()
    print '=>', 'panagram' if solve(s) else 'not panagram'


if __name__ == '__main__':
    start()