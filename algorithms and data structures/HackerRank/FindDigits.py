# Find Digits
#
# https://www.hackerrank.com/challenges/find-digits

def solve(n):
    count = 0
    for i in list(str(n)):
        if int(i) != 0 and n % int(i) == 0:
            count += 1
    return count


def start():
    n = input()
    print solve(n)


if __name__ == '__main__':
    start()