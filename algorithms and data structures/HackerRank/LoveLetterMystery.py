# Love Letter Mystery
#
# https://www.hackerrank.com/challenges/the-love-letter-mystery


def make_palindromic(s):
    reductions = 0
    for i in range(len(s) // 2):
        reductions += abs(ord(s[i]) - ord(s[-1-i]))
    return reductions

def start():
    n = input()
    for _ in xrange(n):
        print make_palindromic(raw_input())


if __name__ == '__main__':
    start()