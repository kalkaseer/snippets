# Circle City
#
# https://www.hackerrank.com/challenges/circle-city

# Gauss Circle Problem
# http://mathworld.wolfram.com/GausssCircleProblem.html

from math import floor, sqrt, ceil

def gauss_circle(r):
    m = 1 + (4 * int(floor(r)))
    s = 0
    for i in xrange(1, int(floor(r))):
        s += int(floor(sqrt(r*r - i*i)))
    
    return m + 4*s


def solve(d, k):
    # check that number of points on the circumference, if equal to or greater
    # than k, then all protected.
    count = 0
    for x in xrange(int(ceil(sqrt(d)))):
        if sqrt(d - x*x).is_integer():
            count += 4
    return count <= k


def start():
    t = input()
    for _ in xrange(t):
        d, k = map(int, raw_input().strip().split())
        print 'possible' if solve(d,k) else 'impossible'


if __name__ == '__main__':
    start()