# ACM-ICPC Team
#
# https://www.hackerrank.com/challenges/acm-icpc-team

def setBitsCount(n):
    n = n - ((n >> 1) & 0x55555555)
    n = (n & 0x33333333) + ((n >> 2) & 0x33333333)
    return (((n + (n >> 4)) & 0x0F0F0F0F) * 0x01010101) >> 24


# def isBitSet(n, pos):
#     return True if n & (1 << pos) > 0 else False

# TODO BUGGY
def solution(records):
    
    max_topics = 0
    max_teams = 0
    
    for i in xrange(len(records)):
        for j in xrange(i + 1, len(records)):
            if i == j: continue
            current_topics = setBitsCount(records[i] | records[j])
            print current_topics
        
            if current_topics > max_topics:
                max_teams = 1
                max_topics = current_topics
            elif current_topics == max_topics:
                max_teams += 1
                
    return max_topics, max_teams

test_case = [
    int('10101', 2),
    int('11100', 2),
    int('11010', 2),
    int('00101', 2),
]

'''
#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;
 
 
 
int acmIcpc(string &str1, string &str2, int length){
    int topicsKnown = 0;
    for (int i=0; i<length; i++){
        if (str1.at(i) == '1' or str2.at(i) == '1'){
            topicsKnown+= 1;
        }
    }
    return topicsKnown;
}
int main() {
    string str[500] = {};
    int N, M;
    cin>>N>>M;
    for (int i=0;i<N;i++) {
        cin>>str[i];
    }
     
    int maxKnown = 0, teamsCnt = 0;
     
    for (int i=0;i<N-1;i++) {
        for (int j=i+1;j<N;j++) {
            int knownForThisCombo = acmIcpc(str[i], str[j], M);
            if (knownForThisCombo > maxKnown){
                maxKnown = knownForThisCombo;
                teamsCnt = 1;
            } else if (knownForThisCombo == maxKnown){
                teamsCnt += 1;
            }
        }
    }
     
    cout << maxKnown << endl;
    cout << teamsCnt << endl;
    return 0;
}
'''

def start():
    persons, topics = map(int, raw_input().split())
    records = []
    for _ in xrange(persons):
        records.append(int(raw_input(), 2))
    max_topics, max_teams = solution(records, topics)
    print max_topics, max_teams


if __name__ == '__main__':
    start()