# Chocolate Feast
#
# https://www.hackerrank.com/challenges/chocolate-feast

def solution(n, c, m):
    bars = n // c
    acc = bars
    while bars >= m:
        rem = bars % m
        bars = bars // m
        acc += bars
        bars += rem
    return acc

def start():
    t = input()
    for _ in xrange(t):
        n, c, m = map(int, raw_input().strip().split(' '))
        print solution(n, c, m)


if __name__ == '__main__':
    start()

# acc 3
#   bars = 3 / 2 = 1
#   acc = 4
#   rem = 1 % 2