# https://www.hackerrank.com/challenges/cut-the-sticks

import sys 

def solve(sticks):
  
  sticks_left = 0
  it = xrange(len(sticks))
  
  while sticks_left != len(sticks):
    
    mn = sys.maxint
    for x in sticks:
      if x != 0:
        mn = min(x, mn)
    
    count = 0
    sticks_left = len(sticks)
    
    for i in it:
      if sticks[i] != 0:
        count += 1
        sticks[i] -= mn
        if sticks[i] != 0:
          sticks_left -= 1
    
    print count


def solve2(sticks):
  
  while sticks:
    print len(sticks)
    sticks.sort()
    min = sticks[0]
    sticks = filter(lambda x: x > min, sticks)
