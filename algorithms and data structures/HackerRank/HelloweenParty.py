# Helloween Party
#
# https://www.hackerrank.com/challenges/halloween-party

def solution(k):
    half = (k // 2) 
    return half * (k - half)

def start():
    k = input()
    for _ in xrange(k):
        print '=>', solution(input())

if __name__ == '__main__':
    start()