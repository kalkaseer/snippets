# Insertion Sort
#
# https://www.hackerrank.com/challenges/insertion-sort

# Solution by means of Binary Indexed Tree
# https://www.topcoder.com/community/data-science/data-science-tutorials/binary-indexed-trees/

class BinaryIndexedTree(object):
    
    def __init__(self, size):
        self.size = size
        self.tree = [0] * size
        
    def update(self, idx, val):
        while idx <= self.size:
            self.tree[idx] += val
            idx += (idx & -idx)
    
    def read(self, idx):
        sum = 0
        while idx > 0:
            sum += self.tree[idx]
            idx -= (idx & -idx)
        return sum


def solve(a):
    count = 0
    bit = BinaryIndexedTree(10**7 + 1)
    for val in reversed(a):
        bit.update(val, 1)
        count += bit.read(val - 1)
    return count


def start():
    t = input()
    for _ in xrange(t):
        n = input()
        a = map(int, raw_input().split())
        print '=>', solve(a)


if __name__ == '__main__':
    start()