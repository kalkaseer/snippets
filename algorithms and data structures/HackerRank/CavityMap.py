# Cavity Map
#
# https://www.hackerrank.com/challenges/cavity-map

def solve(grid):
  
  n = len(grid)
  
  res = []
  
  for i in xrange(1, n-1):
    
    row = []
    row.append(grid[i][0])
    
    for j in xrange(1, n-1):
      a = grid[i][j]
      
      if a > grid[i][j-1] and a > grid[i][j+1] and \
         a > grid[i-1][j] and a > grid[i+1][j]:
         row.append('X')
      else:
         row.append(a)
      
    row.append(grid[i][n-1])
    res.append(''.join(row))
  
  return res

def start():
  grid = []
  grid.append('1112')
  grid.append('1912')
  grid.append('1892')
  grid.append('1234')
  res = solve(grid)
  for i in res: print i