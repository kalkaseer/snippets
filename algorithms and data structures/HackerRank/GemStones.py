# Gem Stones
# 
# https://www.hackerrank.com/challenges/gem-stones

from collections import defaultdict

def solve(slist):
    aset = set(slist[0])
    for i in xrange(1, len(slist)):
        aset &= set(slist[i])
    return len(aset)

def start():
    n = input()
    slists = []
    for _ in xrange(n):
        slists.append(raw_input().strip())
    print '=>', solve(slists)

if __name__ == '__main__':
    start()