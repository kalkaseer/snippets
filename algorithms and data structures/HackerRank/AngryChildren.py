# Angry Children
#
# https://www.hackerrank.com/challenges/angry-children

def solve(k, a):
    diff = 10**9
    for i in xrange(len(a) - k + 1):
        diff = min(diff, a[i + k - 1] - a[i])
    return diff


def start():
    n = input()
    k = input()
    a = []
    for _ in xrange(n):
        a.append(input())
    a.sort()
    print '=>', solve(k, a)


if __name__ == '__main__':
    start()

