# Game of Thrones I
#
# 


def is_palindrome(s):
    ok = True
    odd_count = 0
    letter_count = [0] * 26
    
    for c in s:
        letter_count[ord(c) - ord('a') + 1] += 1
    
    for count in letter_count:
        odd_count += count % 2
    
    if odd_count > 1:
        ok = False
    
    return ok


def start():
    print 'YES' if is_palindrome2(raw_input()) else 'NO'


if __name__ == '__main__':
    start()