# QuickestWayUp (snakes and ladders)
#
# https://www.hackerrank.com/challenges/the-quickest-way-up

def solve(ladders, snakes):
  
  jumps = {}
  
  for i in xrange(len(ladders)):
    jumps[ladders[i][0]] = ladders[i][1]
  
  for i in xrange(len(snakes)):
    jumps[snakes[i][0]] = snakes[i][1]
  
  final = 100
  
  # start from 0 to make things 1-index based
  q = {1}
  path = { 1: ()}

  while len(q) > 0:

    n = q.pop()
    p = path[n] + (n,)

    for j in xrange(n+1, n+7):

      if j > final: break
      if j in jumps: j = jumps[j]
      if j not in path or len(path[j]) > len(p):
        q.add(j)
        path[j] = p

  return len(path[100]) if 100 in path else -1


testcases = [
  [
    [(32,62), (42,68), (12,98) ], # ladders
    [(95,13), (97,25), (93,37), (79,27), (75,19), (49,47), (67,17) ] # snakes
  ],
  [
    [(8,52), (6,80), (26,42), (2, 72)], # ladders
    [(51,19), (39,11), (37,29), (81,3), (59,5),
     (79,23), (53,7), (43,33), (77,21)] # snakes
  ],
  [
    [(5,66), (9,88)],
    [(67,8)]
  ],
  [
    [(3,90)],
    [(99,10), (97,20), (98,30), (96,40), (95,50), (94,60), (93,70)]
  ],
  [
    [(3,54),(37,100)],
    [(56,33)]
  ],
  [
    [(3,57), (8,100)],
    [(88,44)]
  ],
  [
    [(7,98)],
    [(99,1)]
  ]
]

testresults = [ 3, 5, 4, -1, 3, 2, 2 ]

def test():
  
  for i in xrange(len(testcases)):
    t = testcases[i]
    res = solve(t[0], t[1])
    print '===================='
    print i, res
    assert res == testresults[i]

def start():
  t = input()
  for _ in xrange(t):
    ladders = []
    snakes = []
    n = input()
    for _ in xrange(n):
      ladders.append(map(int, raw_input().split()))
    n = input()
    for _ in xrange(n):
      snakes.append(map(int, raw_input().split()))
    print solve(ladders, snakes)