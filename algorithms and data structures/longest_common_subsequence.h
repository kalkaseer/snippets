/*
 * longest_common_subsequence.h
 *
 *  Created on: Aug 8, 2016
 *      Author: Kareem Alkaseer
 */

#ifndef LONGEST_COMMON_SUBSEQUENCE_H_
#define LONGEST_COMMON_SUBSEQUENCE_H_

#include <string>
#include <vector>
#include <algorithm>
#include <iostream>

using namespace std;

/*
 * A subsequence is a sequence that has the same relative order and can be
 * contiguous or not contiguous. For example, "abcdefg", two possible
 * subsequences are "abc" and "ace".
 */

/*
 * Naive recursive solution.
 *
 * In a sequence of length n, there are 2^n different subsequences. We try
 * to find each of the subsequences of the shorter string in the longer strong.
 * As we keep going we shorten the subsequence.
 *
 * The recursion cases are:
 * 1. longest common subsequence of x[1, n-1] and y[1, m]
 * 2. longest common subsequence of x[1, n] and y[1, m-1]
 * 3. if x[n] == y[m], longest common subsequence of x[1, n-1] and y[1, m-1]
 *    followed by the common character at the end.
 *
 * Base case: sequence is empty
 *
 * Complexity O(2^n)
 *
 *
 *                            (x, y)
 *                             / \
 *                      (x, y-1)
 *       |-----------------/\-----------------------------|
 *    (x, y-2)                                        (x-1, y-1)
 *     / \                                               /
 * (x, 0) (x-1, 0)                             (x-1, y-2)
 *   *        *                                   /\
 *        (x-1, y-2)                     (x-2, y-1)
 *           /  \                            /\
 *    (x-1, 0)   (x-2, y-2)      (x-2, y-1)     (x-2, y-2)
 *        *        / \          /         \         ~
 *          (x-2, 0)  (0, y-2) (x-2, y-2) (x-2, y-2)
 *              *       *          ~          ~
 *
 */

// TODO: does not always produce correct results.
namespace recursive {
// x & y 1 past the end position
size_t lcs_length(const string & s1, size_t x, const string & s2, size_t y) {

  if (x == 0 || y == 0)
    return 0;

  size_t xx = x - 1;
  size_t yy = y - 1;

  if (s1[x] == s2[y]) // last elements are equal
    return lcs_length(s1, xx, s2, yy) + 1;
  else
    return max(lcs_length(s1, xx, s2, y), lcs_length(s2, x, s2, yy));
}
} // namespace recursive

/*
 * It is clear that we solve the sub-problem multiple times. Using dynamic
 * programming we can store the solution to the sub-problem and consult the
 * table instead of going further into the recursion tree.
 *
 * Complexity: O(nm)
 */

struct matrix {
  matrix(size_t w, size_t h)
    : width{ w }, height{ h }, v{ width * height, 0 }
  { }

  inline void set(size_t i, size_t j, size_t val) {
    v[width * i + j] = val;
  }

  inline size_t get(size_t i, size_t j) {
    return v[width * i + j];
  }

  size_t width;
  size_t height;
  vector<size_t> v;
};

struct lcs {

  lcs(const string & x, const string & y)
    : s1{ y }, s2{ x }, m{ s2.size() + 1, s1.size() + 1 }
  {}

  inline void set(size_t i, size_t j, size_t v) {
    m.set(i, j, v);
  }

  inline size_t get(size_t i, size_t j) {
    return m.get(i, j);
  }

  inline size_t longest_length() {

    for (size_t i = 0; i < s1.size() + 1; ++i) {
      for (size_t j = 0; j < s2.size() + 1; ++j) {

        if (i == 0 || j == 0)
          set(i, j, 0);

        else if (s1[i - 1] == s2[j - 1])
          set(i, j, get(i-1, j-1) + 1);

        else
          set(i, j, max(get(i-1, j), get(i, j-1)));
      }
    }

    return get(s1.size(), s2.size());
  }

  inline string longest_string() {

    std::string s;
    longest_string(s1.size(), s2.size(), s);
    return std::move(s);
  }

  void longest_string(size_t i, size_t j, string & s) {

    if (i == 0 || j == 0) {
      return;

    } else if (s1[i - 1] == s2[j - 1]) {
      longest_string(i - 1, j - 1, s);
      s.push_back(s1[i - 1]);

    } else if (i - 1 > 0 && j - 1 > 0 && get(i-1, j) > get(i, j-1)) {
      longest_string(i-1, j, s);

    } else if (j - 1 > 0) {
      longest_string(i, j-1, s);
    }
  }

  const string s1;
  const string s2;
  matrix m;
};

/* Longest Increasing Subsequence */

void lis(const vector<int> & x, vector<int> & v) {

  vector<int> p;
  p.resize(x.size());
  size_t lo, hi;

  if (x.empty()) return;

  v.push_back(0);

  for (size_t i = 1; i < x.size(); ++i) {

    if (x[v.back()] < x[i]) {
      p[i] = v.back();
      v.push_back(i);
      continue;
    }

    for (lo = 0, hi = v.size() - 1; lo < hi; ) {
      size_t mid = (lo + hi) / 2;
      if (x[v[mid]] < x[i])
        lo = mid + 1;
      else
        hi = mid;
    }

    if (x[i] < x[v[lo]]) {
      if (lo > 0) p[i] = v[lo - 1];
      v[lo] = i;
    }
  }

  for (lo = v.size(), hi = v.back(); --lo; hi = p[hi])
    v[lo] = hi;
}

#endif /* LONGEST_COMMON_SUBSEQUENCE_H_ */
