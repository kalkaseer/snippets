# Maximum contiguous subarray

# Divide and conquer approach takes O(n log n)
#
# 1. Divide array into two halves
# 2. return the maximum of:
#   a. maximum subarray in left half
#   b. maximum subarray in right half
#   c. maximum subarray that crosses the midpoint

from sys import maxint

def max_crossing_sum(a, lo, mid, hi):
  
  sum = 0
  left_sum = -maxint - 1
  
  for i in xrange(mid, lo-1, -1):
    sum += a[i]
    if sum > left_sum:
      left_sum = sum
  
  sum = 0
  right_sum = -maxint - 1
  for i in xrange(mid+1, hi+1):
    sum += a[i]
    if sum > right_sum:
      right_sum = sum
  
  return left_sum + right_sum


def max_subarray(a, lo, hi):
  
  if lo == hi:
    return a[lo]
  
  mid = (lo + hi) // 2
  
  return max(
    max_subarray(a, lo, mid),
    max_subarray(a, mid+1, hi),
    max_crossing_sum(a, lo, mid, hi))



# Kadane's algorithm solves the largest contiguous subarray in O(n)

def max_subarray_kadane(a):

  max_so_far = 0
  max_ending_here = 0
  
  for n in a:
    max_ending_here = max(0, max_ending_here + n)
    max_so_far = max(max_so_far, max_ending_here)
  
  return max_so_far


# Kadane's algorithm does not work for arrays with all negative numbers
# here is a generalisation
def generalised_kadane(a):
  
  max_so_far = max_ending_here = a[0]
  
  for x in a[1:]:
    max_ending_here = max(x, max_ending_here + x)
    max_so_far = max(max_so_far, max_ending_here)
  
  return max_so_far