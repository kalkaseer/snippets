/* Copyright © Kareem Alkaseer 2016 */

#include <vector>
#include <array>
#include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <utility>
#include <chrono>

// Insertion sort
// T must have a copy constructor, operator=, operator<
// O(n^2), Theta(n)

template< class T >
void insertionSort(std::vector<T> & a) {
	
	for (int i = 0; i < a.size(); ++i) {
		
		T tmp = a[i];
		
		int j = i;
		
		for (; j > 0 && tmp < a[j - 1]; --j)
			a[j] = a[j - 1];
		a[ j ] = tmp;
	}
}

template< class T >
void insertionSort(Std::vector<T> & a) {
  
  for (int i = 1; i < size; ++i) {
    T tmp = a[i];
    int j = i - 1;
    for (; j >= 0 && a[j] > tmp; --i)
      a[i+1] = a[i];
    a[j + 1] = tmp;
  }
}

template< class T >
void insertionSort(std::vector<T> & a, size_t low, size_t high) {
	
	for (size_t i = low; i < high; ++i) {
		
		T tmp = a[i];
		
		int j = i;
		
		for (; j > low && tmp < a[j - 1]; --j)
			a[j] = a[j - 1];
		a[ j ] = tmp;
	}
}

// Shellsort
// Worst case: O(n^2) if N
//    N is a power of 2 and large elements are in even-indexed positions
// O(N^(3/2)) if N is a power of two
// A complexity of O(N^(3/2)) could be achieved when gap / 2 becomes even we add 1 to the gap, in this case average complexity "seems to be" O(N ^ (5/4))
// Another sequence that heurestically reduces complexity to seemingly O(N ^ (7/6)) is by dividing gap by 2.2
// T must have a copy constructor, operator=, operator<

template< class T >
void shellsort(std::vector<T> & a) {
	
	for (
		int gap = a.size() / 2;
		gap > 0;
		gap = gap == 2 ? 1 : static_cast<int>(gap/2.2)
	) {
		for (int i = gap; i < a.size(); ++i) {
			T tmp = a[i];
			int j = i;
			for (; j >= gap && tmp < a[ j - gap]; j -= gap)
				a[j] = a[j - gap];
			a[j] = tmp;
		}
	}
}

// Megasort
// Average time complexity: O(n log n)
// Space complexity: O(n)

template< class T >
void merge(
	std::vector<T> & a, std::vector<T> & b,
	int leftPos, int rightPos, int rightEnd
) {
	int leftEnd = rightPos - 1;
	int pos = leftPos;
	int count = rightEnd - leftPos  + 1;
	
	while (leftPos <= leftEnd && rightPos <= rightEnd) {
		if (a[leftPos] <= a[rightPos])
			b[pos++] = a[leftPos++];
		else
			b[pos++] = a[rightPos++];
	}
	// Copy rest of the left half
	while (leftPos <= leftEnd)
		b[pos++] = a[leftPos++];
	
	// Copy rest of the right half
	while (rightPos <= rightEnd)
		b[pos++] = a[rightPos++];
	
	// Copy b into a.
	for (int i = 0; i < count; ++i, --rightEnd)
		a[rightEnd] = b[rightEnd];
}

template< class T >
void mergesort(
	std::vector<T> & a, std::vector<T> & b, int left, int right
) {
	if (left < right) {
		int centre = (left + right) / 2;
		mergesort(a, b, left, centre);
		mergesort(a, b, centre + 1, right);
		merge(a, b, left, centre + 1, right);
	}
}

template< class T >
void mergesort(std::vector<T> & a) {
	std::vector<T> tmp(a.size());
	mergesort(a, tmp, 0, a.size() - 1);
}

// Quicksort
// Average time complexity: O(n log n)
// Worst time complexity: O(n ^ 2) which can be reduced to O(n log n)
// when combined with other algorithms.
// Space complexity: O(1)
// We avoid the worst case by choosing an appropriate pivot and
// median-of-threes partitioning strategy as well as a cutoff on sub-array
// size that indicates whether to apply a quicksort or an insertion sort
// That gurantees we don't hit the worst case.

constexpr size_t kQuicksortCutoff = 10;

template< class T >
void quicksort(std::vector<T> & a, int low, int high) {
	
	using namespace std;

	if (low + kQuicksortCutoff <= high) {
		
		// Apply median-of-three
		int middle = (low + high ) / 2;
		
		if (a[middle] < a[low])
			swap(a[low], a[middle]);
		
		if (a[high] < a[low])
			swap(a[low], a[high]);
		
		if (a[high] < a[middle])
			swap(a[middle], a[high]);
		
		T pivot = a[middle];
		
		// Place the pivot at high - 1. It is guaranteed to be less.
		swap(a[middle], a[high - 1]);
		
		// Partition
		int i = low;
		
		for (int j = high - 1; ; ) {
			while (a[++i] < pivot) { }
			while (a[--j] > pivot) { }
			if (i < j)
				swap(a[i], a[j]);
			else
				break;
		}
		swap(a[i], a[high - 1]); // Restore the pivot
		
		quicksort(a, low, i - 1);
		quicksort(a, i + 1, high);
		
	} else {
		insertionSort(a, low, high);
	}
}

template< class T >
void quicksort(std::vector<T> & a) {
	quicksort(a, 0, a.size() - 1);
}

bool test(std::vector<int> & sorted, std::vector<int> & a) {
	for (int i = 0; i < sorted.size(); ++i) {
		if (sorted[i] != a[i])
			return false;
	}
	return true;
}

template< class Iter >
void heapsort(Iter begin, Iter end) {
	
	std::make_heap(begin, end);
	std::sort_heap(begin, end);
}

int main() {
	
	using namespace std;
	using namespace std::chrono;
	
	std::vector<int> a = { 1, 10, 12, 4, 3, 2, 8, 5, 2, 7, 4, 9, 8, 10, 11, 12, 25,1, 10, 12, 4, 3, 2, 8, 5, 2, 7, 4, 9, 8, 10, 11, 12, 25,1, 10, 12, 4, 3, 2, 8, 5, 2, 7, 4, 9, 8, 10, 11, 12, 25,1, 10, 12, 4, 3, 2, 8, 5, 2, 7, 4, 9, 8, 10, 11, 12, 25,1, 10, 12, 4, 3, 2, 8, 5, 2, 7, 4, 9, 8, 10, 11, 12, 25};
	
	std::vector<int> b;
	
	std::vector<int> sorted;
	
	sorted = a;
	auto start = high_resolution_clock::now();
	std::sort(sorted.begin(), sorted.end());
	auto end = high_resolution_clock::now();
	auto diff = end - start;
	milliseconds millis = chrono::duration_cast<milliseconds>(diff);
	cout << "standard sort: result: " << " time: " << millis.count() << endl;
	
	b = a;
	start = high_resolution_clock::now();
	//insertionSort(b);
	end = high_resolution_clock::now();
	diff = end - start;
	millis = chrono::duration_cast<milliseconds>(diff);
	cout << "insertion sort: result: " << (test(sorted, b) ? "ok" : "failed");
	cout << " time: " << millis.count() << endl;
	
	b = a;
	start = high_resolution_clock::now();
	shellsort(b);
	end = high_resolution_clock::now();
	diff = end - start;
	millis = chrono::duration_cast<milliseconds>(diff);
	cout << "shell sort: result: " << (test(sorted, b) ? "ok" : "failed");
	cout << " time: " << millis.count() << endl;
	
	b = a;
	start = high_resolution_clock::now();
	mergesort(b);
	end = high_resolution_clock::now();
	diff = end - start;
	millis = chrono::duration_cast<milliseconds>(diff);
	cout << "merge sort: result: " << (test(sorted, b) ? "ok" : "failed");
	cout << " time: " << millis.count() << endl;

	b = a;
	start = high_resolution_clock::now();
	quicksort(b);
	for (int i : sorted) cout << i << " "; cout << endl;
	for (int i : b) cout << i << " "; cout << endl;
	end = high_resolution_clock::now();
	diff = end - start;
	millis = chrono::duration_cast<milliseconds>(diff);
	cout << "quick sort: result: " << (test(sorted, b) ? "ok" : "failed");
	cout << " time: " << millis.count() << endl;
}