from math import log, floor, ceil, sqrt
    
def isprime(n):
    for i in xrange(2, int(sqrt(n)) + 1):
        if n % i == 0: return False
    return True

def nextprime(n):
    while True:
        n += 1
        if isprime(n): return n

def factorize(n, first = 2):
    if n == first: return [first]
    m = n % first
    if m == 0:
        m = n / first
        if isprime(m): return [first, m]
        factors = [first]
        factors.extend(factorize(m))
        return factors
    else:
        return factorize(n, nextprime(first))

def generate_prime_sieve(n):
    
    """Generate the primes less than `n` using the Sieve of Eratosthenes."""
    a = [True] * n
    a[0] = a[1] = False
    for i, isprime in enumerate(a):
        if isprime:
            yield i
            for j in range(i * i, n, i):
                a[j] = False

# another method
# O(log log n)
def sieve(n):
    sieve = [True] * (n + 1)
    sieve[0] = sieve[1] = False
    i = 2
    while i * i <= n:
        if sieve[i]:
            k = i * i
            while k <= n:
                sieve[k] = False
                k += i
        i += 1
    return sieve

def generate_prime_wilson(n):
    
    """Generate the primes less than `n` using Wilson's theorem."""
    fac = 1
    for i in range(2, n):
        fac *= i - 1
        if (fac + 1) % i == 0:
            yield i

# factorise using sieves
# remember the smallest prime that divides every composite number up to n
def generate_smallest_prime_dividers(n):
    F=[0]*(n+1)
    i=2
    while i * i <= n:
        if F[i] == 0:
            k = i * i
            while k <= n:
                if F[k] == 0:
                    F[k] = i
                k += i
        i += 1
    return F

# prime factorisation
# O(log n)
def factorize2(n):
    primes = []
    divs = generate_smallest_prime_dividers(n)
    while divs[n] > 0:
        primes += [divs[x]]
        x /= divs[x]
    primes += [x]
    return primes