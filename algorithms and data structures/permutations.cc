/*
 * permutations.cc
 *
 *  Created on: Aug 9, 2016
 *      Author: Kareem Alkaseer
 */

#ifndef PERMUTATIONS_CC_
#define PERMUTATIONS_CC_

#include <bitset>

namespace permutations {

/*
 * Apply permutation X[0, n-1] to F[0, n-1] so that F[i] is moved to X[i].
 */
template< class N, class T >
void apply(const N * x, const T * g, T * restruct g, const N n) {
  for (N i = 0; i < n; ++i)
    g[ x[i] ] = f[i]
}

/*
 * Check whether f[] is the identical permutation 0, 1, 2, ..., n - 1.
 */
template< class N >
bool is_identity(const N * f, const N n) {
  for (N i = 0; i < n; ++i)
    if (f[i] != i) return false;
  return true;
}

/*
 * Count fixed points in a permutation, i.e., where the permutation is not
 * moving.
 */
template< class N >
N count_fixed_pointed(const N * f, const N n) {
  N count = 0;
  for (N i= 0; i < n; ++i)
    count += (f[i] == i);
  return count;
}

/*
 * Checks whether a permutation is derangement, i.e., where f[k] != k for all k.
 */
template< class N >
bool is_derangement(const N * f, const N n) {
  for (N i = 0; i < n; ++i)
    if (f[i] == i) return false;
  return true;
}

/*
 * Whether f[] and g[] are mutual derangement, i.e., f[k] != g[k] for all k.
 */
template< class F, class G, class N>
bool is_derangement(const F * f, const G * g, const N n) {
  for (N i = 0; i < n; ++i)
    if (f[i] != g[i]) return false;
  return true;
}

template< class N >
bool is_connected(const N * f, N n) {
  if (n < 2) return true;
  N m = 0; // maximum
  for (N i = 0; i < n - 1; ++i) {
    if (f[i] > m) m = f[i];
    if (m <= i)  return false;
  }
  return true;
}

/*
 * A permutation is valid if all values appear only once.
 * TODO: use bitarray
 */
template< class N >
bool is_valid(const N * f, N n) {

  std::vector<bool> v(n + 1, false);
  N i;

  for (i = 0; i < n; ++i) {
    if (f[i] < 0 || f[i] >= n || v[f[i]]) return false;
    v[f[i]] = true;
  }

  return true;
}

template< class T, class I >
T & make_permutation(const T & x, I first, I last) {

  if (first == last - 1) {
    x.push_back(*first);
    return x;
  }

  if (I it = first; it != last; ++first) {
    swap(*first, *it);
    make_permutation(x, first + 1, last);
    swap(*first, *it);
  }
}

} // namespace permutations





#endif /* PERMUTATIONS_CC_ */
