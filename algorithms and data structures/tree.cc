/**
 * Copyright 2016 Kareem Alkaseer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

//============================================================================
// Name        : tree.cpp
// Author      : Kareem Alkaseer
// Version     : 1.0.0
// Description : Multiple tree structures and algorithms.
//============================================================================

#include <iostream>
#include <stack>
#include <queue>
#include <deque>
#include <set>
#include <list>
#include <memory>
#include <cstdint>
#include <cassert>
#include <random>
#include <boost/pool/object_pool.hpp>
#include <boost/pool/pool_alloc.hpp>

using std::shared_ptr;
using std::make_shared;
using std::multiset;
using std::list;

using std::cout;
using std::cin;
using std::endl;

template< class T>
using stack = std::stack<T, std::deque<T, boost::fast_pool_allocator<T> > >;

template< class T>
using queue = std::queue< T, std::deque<T, boost::fast_pool_allocator<T> > >;

template< class NodePointer >
size_t tree_height(NodePointer node);

template< class Tree >
size_t tree_height(const Tree & tree);

enum class order : uint8_t {
  in,
  pre,
  post,
  level,
};

enum class iterator_direction : int8_t {
  alterante = 0,
  left      = -1,
  right     = 1,
};

inline iterator_direction cross(iterator_direction d) {
  return static_cast<iterator_direction>(-static_cast<int8_t>(d));
}

/* ******************************************************************
 TreeOperations concept

  template< class T >
  static void insert(T &, const T::value_type &);

  template< class T >
  static void insert(T &, T::value_type &&);

  template< class T, class Iter >
  static void insert(T &, Iter &, Iter &);

  template< class T, class Iter >
  static void insert(T & t, T::iterator &, order, Iter &, Iter &);

  template< class T >
  static T::iterator find(T &, T::iterator &, const T::value_type &);
* ******************************************************************/

template<
  class T, class Operations, uint16_t node_alignment = 64
> struct basic_tree {

public:

  template< class NT >
  struct iterator_t;

  struct node;


  typedef basic_tree< T, Operations, node_alignment > self_type;

  typedef shared_ptr< self_type > pointer;

  typedef T value_type;

  typedef T * value_pointer;

  typedef T & value_reference;

  typedef self_type & reference;

  typedef node node_type;

  typedef shared_ptr< node_type > node_pointer;

  typedef node_type & node_reference;

  typedef node_type * plain_node_pointer;

  typedef iterator_t< node_type > iterator;

  typedef iterator_t< const node_type > const_iterator;

  typedef Operations operations;


  explicit basic_tree()
    : destroyed_{ false }
  { }

  explicit basic_tree(const value_type & value)
    : root_{ make_node(value) }, destroyed_ { false }
  { }

  explicit basic_tree(value_type && value)
    : root_{ make_node(std::forward<value_type>(value)) },
      destroyed_ { false }
  { }

  node_pointer root() { return root_; }

  void root(const value_type & value) {
    root_ = make_node(value);
  }

  void root(value_type && value) {
    root_ = make_node(std::forward(value));
  }

  void root(node_pointer && newroot) { root_ = newroot; }

  value_reference value() { return root()->value(); }

  bool empty() { return root_ ? true : false; }

  bool height() { return tree_height(root_); }

  template< class Actions >
  void traverse(order o, Actions & actions) {
    iterate(root(), o, actions);
  }

  iterator find(const value_type & value) {
    return operations::find(*this, root_, value);
  }

  iterator find(value_type && value) {
    return operations::find(*this, root_, value);
  }

  iterator find(iterator it, const value_type & value) {
    return operations::find(*this, *it, value);
  }

  iterator find(iterator it, value_type && value) {
    return operations::find(*this, *it, value);
  }

  void remove(node_pointer node) {

    if (node) {
      node->clear();
      node.reset();
    }
  }

  void destroy() { destroyed_ = true; }

  bool destroyed() { return destroyed_; }

  iterator begin() { return iterator{ root_ }; }

  iterator end() { return iterator{}; }

  const_iterator cbegin() const { return const_iterator{ root_ }; }

  const_iterator cend() const { return const_iterator{}; }

  template< class Iter >
  void insert(iterator & it, order o, Iter begin, Iter end) {
    operations::insert(*this, it, o, begin, end);
  }

  void insert(const value_type & data) {
    operations::insert(*this, data);
  }

  void insert(value_type && data) {
    operations::insert(*this, std::forward<value_type>(data));
  }

  template< class Iter >
  void insert(Iter begin, Iter end) {
    operations::insert(*this, begin, end);
  }


  struct node {

    node(basic_tree & parent, const T & d) : parent_(parent), data_(d) { }

    node(basic_tree & parent, T && d) : parent_(parent), data_(d) { }

    node(const node_type & rhs) = default;

    node(node_type && rhs) = default;

    value_reference value() { return data_; }

    node_pointer left() { return left_; }

    node_pointer right() { return right_; }

    value_reference left_value() {  return left_->value(); }

    value_reference right_value() { return right_->value(); }

    void left_value(const value_type & value) {
      left_->data_ = value;
    }

    void left_value(value_type && value) {
      left_->data_ = value;
    }

    void right_value(const value_type & value) {
      right_->data_ = value;
    }

    void right_value(value_type && value) {
      right_->data_ = value;
    }

    bool has_left() { return left_ ? true : false; }

    bool has_right() { return right_ ? true : false; }

    node_pointer left(const value_type & value) {
      left_ = parent_.make_node(value);
      return left_;
    }

    node_pointer right(const value_type & value) {
      right_ = parent_.make_node(value);
      return right_;
    }

    node_pointer left(value_type && value) {
      left_ = parent_.make_node(std::forward<value_type>(value));
      return left_;
    }

    node_pointer right(value_type && value) {
      right_ = parent_.make_node(std::forward<value_type>(value));
      return right_;
    }

    void assign(const value_type & lvalue, const value_type & rvalue) {
      left_ = parent_.make_node(lvalue);
      right_ = parent_.make_node(rvalue);
    }

    void assign(value_type && lvalue, value_type && rvalue) {
      left_ = parent_.make_node(std::forward<value_type>(lvalue));
      right_ = parent_.make_node(std::forward<value_type>(rvalue));
    }

    void clear() {
      left_.reset();
      right_.reset();
    }

  private:

    basic_tree &       parent_;
    T            data_;
    node_pointer left_;
    node_pointer right_;
  };

  template< class NT >
  struct iterator_t
    : public std::iterator<
        std::forward_iterator_tag,
        typename std::remove_cv<NT>::type,
        std::ptrdiff_t,
        T *,
        T &
      >
  {

    typedef NT value_type;

    typedef iterator_t< NT > self_type;

    iterator_t() // gives end.
      : pointer{ }, direction_{ iterator_direction::left }
    { init(); }

    iterator_t(iterator_direction dir)
      : pointer{ }, direction_{ dir }
    { init(); }

    iterator_t(iterator_direction && dir)
      : pointer{ }, direction_{ dir }
    { init(); }

    explicit iterator_t(
      const node_pointer & node,
      iterator_direction dir = iterator_direction::left
    )
      : pointer{ node }, direction_{ dir }
    { init(); }

    explicit iterator_t(const node_pointer & node, iterator_direction && dir)
      : pointer{ node }, direction_{ dir }
    { init(); }

    void swap(self_type & other) noexcept {
      std::swap(pointer, other.pointer);
    }

    self_type & operator++() {
      assert(pointer && "Out of bounds iterator increment.");
      (this->*increment)();
      return *this;
    }

    self_type & operator++(int) {
      assert(pointer && "Out of bounds iterator increment.");
      self_type tmp(*this);
      (this->*increment)();
      return tmp;
    }

    self_type & operator--() {
      assert(previous_ && "Out of bounds iterator decrement.");
      pointer = previous_;
      previous_.reset();
      return *this;
    }

    self_type & operator--(int) {
      self_type tmp(*this);
      this->operator--();
      return tmp;
    }

    template< class OtherType >
    bool operator == (const iterator_t<OtherType> & rhs ) const {
      return pointer == rhs.pointer;
    }

    template< class OtherType >
    bool operator != (const iterator_t<OtherType> & rhs ) const {
      return pointer != rhs.pointer;
    }

    node_reference operator*() const {
      assert(pointer && "Invalid iterator dereference.");
      return *pointer;
    }

    node_pointer operator->() const {
      assert(pointer && "Invalid iterator dereference.");
      return pointer;
    }

    operator iterator_t<const NT>() const {
      return iterator_t<const NT>(pointer);
    }

    operator bool() const {
      return pointer ? true : false;
    }

    node_pointer adjacent() {
      assert(previous_ && "Invalid iterator dereference.");
      this->operator--();
      cross_direction();
      this->operator++();
      return pointer;
    }

    node_pointer previous() {
      return previous_;
    }

    node_pointer get() {
      return pointer;
    }

    iterator_direction direction() { return direction_; }

    void direction(iterator_direction dir) {
      direction_ = dir;
      init();
    }

    void cross_direction() {
      direction(cross(direction_));
    }

    void goleft() {
      previous_ = pointer;
      pointer = pointer->left();
    }

    void goright() {
      previous_ = pointer;
      pointer = pointer->right();
    }

    self_type & goadjacent() {
      assert(previous_ && "Invalid iterator dereference.");
      this->operator--();
      cross_direction();
      this->operator++();
      return *this;
    }

  private:

    void (self_type::* increment)(void);

    void left_increment() {
      cout << "left increment: left ok: " << std::boolalpha << pointer->has_left() << endl; // todo: remove
      previous_ = pointer;
      pointer = pointer->left();
      if (direction_ == iterator_direction::alterante)
        increment = &self_type::right_increment;
    }

    void right_increment() {
      cout << "right increment: right ok: " << std::boolalpha << pointer->has_right() << endl; // todo: remove
      previous_ = pointer;
      pointer = pointer->right();
      if (direction_ == iterator_direction::alterante)
        increment = &self_type::left_increment;
    }

    void init() {
      if (direction_ == iterator_direction::left)
        increment = &self_type::left_increment;
      else if (direction_ == iterator_direction::right)
        increment = &self_type::right_increment;
    }

    node_pointer pointer;
    node_pointer previous_;

    iterator_direction direction_;
  };

private:

  node_pointer make_node(const value_type & value) {
    return node_pointer(pool.construct(*this, value),
      std::bind(&self_type::deallocate, this, std::placeholders::_1));
  }

  node_pointer make_node(value_type && value) {
    return node_pointer(pool.construct(*this, std::forward<value_type>(value)),
      std::bind(&self_type::deallocate, this, std::placeholders::_1));
  }

  void deallocate(plain_node_pointer node) {
    if (!destroyed_) pool.destroy(node);
  }

  template< uint16_t alignment >
  struct aligned_allocator {

    typedef size_t size_type;
    typedef ptrdiff_t difference_type;

    static char * malloc(const size_type bytes) {
      void * result;
      if (posix_memalign(&result, alignment, bytes) != 0)
        result = nullptr;
      return reinterpret_cast<char *>(result);
    }

    static void free(char * const block) {
      std::free(block);
    }
  };

  typedef aligned_allocator< node_alignment > allocator;

  boost::object_pool< node_type, allocator > pool;

  node_pointer root_;

  bool destroyed_;
};


template< class NodePointer, class Actions >
void traverse_pre(NodePointer & node, Actions & actions) {
  if (node) {
    actions.visit(node);
    traverse_pre(node->left);
    traverse_pre(node->right);
  }
}

template< class NodePointer, class Actions >
void traverse_in(NodePointer & node, Actions & actions) {
  if (node) {
    traverse_in(node->left);
    actions.visit(node);
    traverse_in(node->right);
  }
}

template< class NodePointer, class Actions >
void traverse_post(NodePointer & node, Actions & actions) {
  if (node) {
    traverse_post(node->left);
    traverse_post(node->right);
    actions.visit(node);
  }
}

// Worst case: O(n^2)
template< class NodePointer, class Actions >
void traverse_level(NodePointer & node, size_t level, Actions & actions) {
  if (node) {
    if (level == 0) {
      actions.visit(node);
    } else if (level > 1) {
      traverse_level(node->left(), level - 1);
      traverse_level(node->right(), level - 1);
    }
  }
}

template< class NodePointer, class Actions >
void traverse_level(NodePointer & node, Actions & actions) {
  size_t h = tree_height(node);
  for (int i = 0; i <= h; ++i)
    traverse_level(node, i, actions);
}

template< class NodePointer, class Actions >
void traverse(NodePointer & node, order o, Actions & actions) {
  switch (o) {
    case order::pre:   traverse_pre(node, actions); break;
    case order::in:    traverse_in(node, actions); break;
    case order::post:  traverse_post(node, actions); break;
    case order::level: traverse_level(node, actions); break;
  }
}

template< class NodePointer >
size_t tree_height(NodePointer & node) {
  if (node) {
    size_t lh = tree_height(node->left());
    size_t rh = tree_height(node->right());
    return lh > rh ? ++lh : ++rh;
  }
  return 0;
}

template< class Tree >
size_t tree_height(const Tree & tree) {
  return tree_height(tree.root());
}

template< class NodePointer, class Actions >
void iterate_pre(NodePointer & node, Actions & actions) {
  stack<NodePointer> s;
  while (!s.empty() || node) {
    if (node) {
      actions.visit(node);
      if (node->right())
        s.push(node->right());
      node = node->left();
    } else {
      node = s.top();
      s.pop();
    }
  }
}

template< class NodePointer, class Actions >
void iterate_in(NodePointer & node, Actions & actions) {
  stack<NodePointer> s;
  while (!s.empty() || node) {
    if (node) {
      s.push(node);
      node = node->left();
    } else {
      node = s.top();
      s.pop();
      actions.visit(node);
      node = node->right();
    }
  }
}

template< class NodePointer, class Actions >
void iterate_post(NodePointer & node, Actions & actions) {
  stack<NodePointer> s;
  NodePointer last;
  NodePointer peek;
  while (!s.empty() || node) {
    if (node) {
      s.push(node);
      node = node->left();
    } else {
      peek = s.top();
      if (peek->right() && last != peek->right()) {
        node = peek->right();
      } else {
        actions.visit(peek);
        last = s.top();
        s.pop();
      }
    }
  }
}

// Time complexity: O(n)
template< class NodePointer, class Actions >
void iterate_level(NodePointer & node, Actions & actions) {
  if (node) {
    queue<NodePointer> q;
    q.push(node);
    while (!q.empty()) {
      node = q.front();
      q.pop();
      actions.visit(node);
      if (node->left())
        q.push(node->left());
      if (node->right())
        q.push(node->right());
    }
  }
}

template< class NodePointer, class Actions >
void iterate(NodePointer & node, order o, Actions & actions) {
  switch (o) {
    case order::pre:   iterate_pre(node, actions); break;
    case order::in:    iterate_in(node, actions); break;
    case order::post:  iterate_post(node, actions); break;
    case order::level: iterate_level(node, actions); break;
  }
}

template< class NodePointer, class Actions >
void iterate(NodePointer && node, order o, Actions & actions) {
  iterate(node, o, actions);
}

struct basic_operations {

  template< class T >
  static void insert(T & t, const typename T::value_type & data) { }

  template< class T >
  static void insert(T & t, typename T::value_type && data) { }

  template< class T, class Iter >
  static void insert(T & t, Iter & begin, Iter & end) { }

  // Time complexity: O(n)
  // Space complexity: O(n/2)
  template< class T, class Iter >
  static void insert_levelorder(
    T & t, typename T::iterator & it, Iter begin, Iter end
  ) {

    queue<typename T::plain_node_pointer> q;

    if (!it) {
      t.root(*begin);
      q.push(t.root().get());
      it = t.begin();
      ++begin;
    }

    while (begin != end) {

      typename T::plain_node_pointer parent = q.front();
      q.pop();
      if (parent->has_left())
        parent->left_value(*begin);
      else
        parent->left(*begin);
      q.push(parent->left().get());
      ++begin;
      if (begin != end) {
        if (parent->has_right())
          parent->right_value(*begin);
        else
          parent->right(*begin);
        q.push(parent->right().get());
        ++begin;
      }
    }

    cout << "queue.size: " << q.size() << endl; // todo: remove
  }

  template< class T, class Iter >
  static void insert(
    T & t, typename T::iterator & it, order o, Iter & begin, Iter & end
  ) {
    switch (o) {
      case order::level: insert_levelorder(t, it, begin, end); return;
    }
  }

  template< class T >
  static typename T::iterator find(
    T & t, typename T::node_pointer node, const typename T::value_type & value
  ) {
    if (node || (node = t.root())) {
      if (node->value() == value) {
        return typename T::iterator(node);
      }
      typename T::iterator it = find(t, node->left(), value);
      return it != t.end() ? it : find(t, node->right(), value);
    }
    return typename T::iterator();
  }
};

struct bst_operations {

  template< class T >
  static void insert(T & t, const typename T::value_type & data) {

    typename T::iterator it = t.begin();
    if (it) {
      while (true) {

        if (data < it->value()) { // go left

          if (!it->has_left()) {
            it->left(data);
            return;
          }
          it.goleft();

        } else { // go right

          if (!it->has_right()) {
            it->right(data);
            return;
          }
          it.goright();
        }
      }
    } else {
      t.root(data);
    }
  }

  template< class T >
  static void insert(T & t, typename T::value_type && data) {
    insert(t, data);
  }

  template< class T, class Iter >
  static void insert(T & t, Iter & begin, Iter & end) {
    while (begin != end) {
      insert(t, *begin);
      ++begin;
    }
  }

  template< class T, class Iter >
  static void insert(
    T & t, typename T::iterator & it, order o, Iter & begin, Iter & end
  ) { /* no op */ }

  template< class T >
  static typename T::iterator find(
    T & t, typename T::node_pointer node, const typename T::value_type & value
  ) {
    if (!node)
      node = t.root();

    while (node) {
      if (node->value() == value)
        return typename T::iterator(node);
      if (node->value() > value)
        node = node->left();
      else
        node = node->right();
    }

    return typename T::iterator();
  }

};

// multiset is a self-balancing binary search tree which allows duplicates.
// Elements will be sorted in descending order.
template< class Iter >
shared_ptr< multiset< typename std::iterator_traits<Iter>::value_type > >
binary_tree_sort(Iter first, Iter last) {
  return make_shared(
    new multiset< typename std::iterator_traits<Iter>::value_type > >(
      first, last)
    );
}

template< class T >
using tree = basic_tree< T, basic_operations >;

template< class T >
using binary_search_tree = basic_tree<T, bst_operations >;

using bst = binary_search_tree<int>;
using iterator = bst::iterator;

template< class T >
struct node_actions {
  void visit(typename T::node_pointer & node) {
    cout << node->value() << " ";
  }
};

int main() {

  node_actions<tree<int>> tree_actions;
  node_actions<bst> bst_actions;

  unsigned int n;
  cout << "N: "; cin >> n;

  tree<int> t;
  tree<int>::iterator it = t.begin();

  bst t2;

  {
    cout << "constructing list" << endl;
    std::list<int, boost::fast_pool_allocator<int> > v;
    for (int i = 0; i < n; ++i) {
      v.push_back(i);
    }
    cout << "constructing tree" << endl;
    t.insert(it, order::level, v.cbegin(), v.cend());
    t2.insert(v.cbegin(), v.cend());
  }

  cout << "traversing preorder" << endl;
  t.traverse(order::pre, tree_actions); cout << endl;
  t2.traverse(order::pre, bst_actions); cout << endl;
  cout << "traversing inorder" << endl;
  t.traverse(order::in, tree_actions); cout << endl;
  t2.traverse(order::in, bst_actions); cout << endl;
  cout << "traversing postorder" << endl;
  t.traverse(order::post, tree_actions); cout << endl;
  t2.traverse(order::post, bst_actions); cout << endl;
  cout << "traversing levelorder" << endl;
  t.traverse(order::level, tree_actions); cout << endl;
  t2.traverse(order::level, bst_actions); cout << endl;

  cout << std::boolalpha << (t.find(3) != t.end()) << endl;
  cout << std::boolalpha << (t2.find(3) != t.end()) << endl;

  t.destroy();
  t2.destroy();

//  print(it);
//
//  it->left(1);
//  it->right(2);
//  print(it);
//
//  ++it;
//  it->assign(3, 4);
//  print(it);
//
//  it.adjacent()->assign(5, 6);
//  print(it);

//   t.allocate(n);
//   it->left(8); it->right(2);
//  it.direction(iterator_direction::alterante);
//  ++it; it->left(6); it->right(7);
//  for (int i = 0; i < n; ++i) {
//    ++it; it->left(mtrand()); it->left(mtrand());
//  }
//  t.traverse(order::pre, actions); cout << endl;
//  cout << std::boolalpha << (t.find(3) != t.end()) << endl;
//  t.remove(t.root()->left());
//  t.traverse(order::in, actions); cout << endl;

  return 0;
}
