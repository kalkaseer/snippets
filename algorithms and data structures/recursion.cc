/*
 * recursion.cc
 *
 *  Created on: Sep 4, 2016
 *      Author: Kareem Alkaseer
 */

#include <unordered_map>
#include <string>
#include <vector>
#include <tuple>
#include <iostream>

using employee_chart = std::unordered_map<std::string, std::string>;

namespace local {

struct managed_employess_counter {

  managed_employess_counter(const employee_chart & c)
    : chart{ c }
  { }

  size_t operator() (const std::string & manager) {

    size_t count{ 0 };

    for (auto & e : chart) {
      if (e.second == manager) {
        ++count;
        count += operator()(e.first);
      }
    }

    return count;
  }

private:

  const employee_chart & chart;
};


} // namespace local


namespace global {

struct managed_employess_counter {

  explicit managed_employess_counter(const employee_chart & c)
    : chart{ c }
  { }

  size_t operator()(const std::string & manager) {
    size_t n{ 0 };
    count(manager, n);
    return n;
  }

private:

  void count(const std::string & manager, size_t & n) {
    for (auto & e : chart) {
      if (e.second == manager) {
        ++n;
        count(e.first, n);
      }
    }
  }


  const employee_chart & chart;
};

} // namespace global


/* ************************************************************************* */

namespace maze {

constexpr char o{ '*' };

using maze_type = std::vector<std::vector<int>>;

using point = std::tuple<size_t, size_t>;

inline point make_point(size_t x, size_t y) {
  return std::make_tuple(x, y);
}

struct solver {

  explicit solver(maze_type & m)
    : maze(m), width{ m[0].size() }, height{ m.size() }
    { }

  inline bool operator()(point a, point b) {
    return explore(a, b);
  }

private:

  bool explore(point a, const point & b) {
    size_t x = std::get<0>(a);
    size_t y = std::get<1>(a);

    // size_t cannot go beyond 0 and if it overflows the upper condition
    // would suffice
    if (x > height - 1 || y > width - 1)
      return false;

    if (maze[x][y] == o)
      return false;

    if (a == b)
      return true;

    maze[x][y] = o;

    if (explore(make_point(x, y-1), b)) return true;
    if (explore(make_point(x, y+1), b)) return true;
    if (explore(make_point(x-1, y), b)) return true;
    if (explore(make_point(x+1, y), b)) return true;

    return false;
  }

  maze_type & maze;
  size_t width;
  size_t height;
};

} // namespace maze

/* ************************************************************************* */

int main() {

  using counter = global::managed_employess_counter;

  employee_chart chart = {
    { "betty", "sam" },
    { "bob",   "sally" },
    { "dilbert", "nathan" },
    { "joseph", "sally" },
    { "nathan", "veronica" },
    { "sally", "veronica" },
    { "sam", "joseph" },
    { "susan", "bob" },
    { "veronica", "" }
  };

  counter c{ chart };

  std::cout << "veronica manages " << c("veronica") << " people\n";

  std::cout << "sam manages " << c("sam") << " people\n";

  std::cout << "sally manages " << c("sally") << " people\n";

  std::cout << "betty manages " << c("betty") << " people\n";

  using maze::o;

  maze::maze_type m = {
    { o, o, o, o, o, 0, 0, 0 },
    { o, 0, 0, 0, o, 0, 0, 0 },
    { o, 1, o, o, o, 0, 0, 0 },
    { o, 0, 0, 0, o, o, o, o },
    { o, 0, 0, 0, 0, 0, 0, o },
    { o, 0, o, 0, 0, 0, 0, o },
    { o, 0, 0, 0, o, 0, 0, o },
    { o, o, o, o, o, 0, 2, o },
    { 0, 0, 0, 0, o, o, o, o },
  };

  maze::solver s{ m };

  auto a = maze::make_point(2,1);
  auto b = maze::make_point(7,6);

  std::cout << "maze solvable: " << s(a, b) << '\n';


  return 0;
}
