/*
 * factory_method.h
 *
 *  Created on: May 3, 2016
 *      Author: Kareem Alkaseer
 */

#ifndef DESIGN_PATTERNS_FACTORY_METHOD_H_
#define DESIGN_PATTERNS_FACTORY_METHOD_H_

#include <utility>
#include <string>
#include <iostream>

class pdf_document;
class rtf_document;
class odf_document;

class document {

public:

  static document * make_pdf(std::string name);

  static document * make_rtf(std::string name);

  static document * make_odf();

  document(std::string name)
    : m_name{ name }
  { }

  virtual ~document() { }

  virtual void print() = 0;

  std::string name() { return m_name; }

private:

  static odf_document odf_doc; // factory method need not create new objects

  std::string m_name;
};

struct pdf_document
  : public document
{
  explicit pdf_document(std::string name)
    : document{ name }
  { }

  void print() override { std::cout << "pdf document" << std::endl; }
};

struct rtf_document
  : public document
{
  explicit rtf_document(std::string name)
    : document{ name }
  { }

  void print() override { std::cout << "rtf document" << std::endl; }
};

struct odf_document
  : public document
{
  explicit odf_document(std::string name)
    : document{ name }
  { }

  void print() override { std::cout << "odf document" << std::endl; }
};

odf_document document::odf_doc{ "odf document name" };

document * document::make_pdf(std::string name) {
  return std::move(new pdf_document(name));
}

document * document::make_rtf(std::string name) {
  return std::move(new rtf_document(name));
}

document * document::make_odf() {
  return &odf_doc;
}


#endif /* DESIGN_PATTERNS_FACTORY_METHOD_H_ */
