/*
 * builder.h
 *
 *  Created on: May 3, 2016
 *      Author: Kareem Alkaseer
 */

#ifndef DESIGN_PATTERNS_BUILDER_H_
#define DESIGN_PATTERNS_BUILDER_H_

#include <string>
#include <sstream>
#include <vector>

namespace simple {

struct item {
  item() : popularity{0}, name{}, creator{} { }
  int popularity;
  std::string name;
  std::string creator;
};

struct builder {
  builder() : m_item { new item() } { }
  item * create() { return m_item; }
  void popularity(int p) { m_item->popularity = p; }
  void name(std::string n) { m_item->name = n; }
  void creator(std::string c) { m_item->creator = c; }
private:
  item * m_item;
};

} // namespace simple

class configuration {

public:

  configuration()
    : m_filename{}, m_queue_size{0}, m_sockets{0},
      m_open_mode{std::ios::binary | std::ios::in}
  { }

  void sockets(int count) { m_sockets = count; }

  void queue(int size) { m_queue_size = size; }

  void file(std::string filename, std::ios::open_mode mode) {
    m_filename = filename;
    m_open_mode = mode;
  }

private:

  std::string m_filename;
  int m_queue_size;
  int m_sockets;
  std::ios::open_mode m_open_mode;
};

/* Interface */
struct builder {
  virtual ~builder() { }
  virtual configuration * create() = 0;
  virtual void sockets(int count) = 0;
  virtual void queue(int size) = 0;
  virtual void file(std::string filename) = 0;
};

class linux_builder : public builder {

public:

  linux_builder() : m_config{ new configuration() } { }

  configuration * create() override { return m_config; }

  void sockets(int count = 10) override {
    m_config->sockets(count < 1 || count > 10 ? 10 : count);
  }

  void queue(int size = 20) override {
    m_config->queue(size < 1 || size > 20 ? 20 : size);
  }

  void file(std::string filename) override {
    m_config->file(filename, std::ios::in);
  }

private:
  configuration * m_config;
};

class bsd_builder : public builder {

public:

  bsd_builder() : m_config{ new configuration() } { }

  configuration * create() override { return m_config; }

  void sockets(int count = 5) override {
    m_config->sockets(count > 0 ? 5 * count : count == 0 ? 5 : -1);
  }

  void queue(int size = 20) override {
    m_config->queue(size > 20 || size <= 0 ? 20 : size);
  }

  void file(std::string filename) override {
    m_config->file(filename, std::ios::binary | std::ios::in);
  }

private:
  configuration * m_config;
};

class director {
public:

  void set_builder(builder * b) { m_builder = b; }

  configuration * constuct(std::string repr) {

    std::istringstream ss{ repr };
    std::string line;

    while (std::getline(ss, line, '\n')) {

      if (starts_with(line, "sockets")) {
        std::vector<std::string> tokens = split(line, ' ');
        if (tokens.size() > 1 && tokens[1] != "") {
          m_builder->sockets(std::stoi(tokens[1]));
        }
      } else if (starts_with(line, "file")) {
        std::vector<std::string> tokens = split(line, ' ');
        if (tokens.size() > 1 && tokens[1] != "") {
          m_builder->file(tokens[1]);
        }
      } else if (starts_with(line, "queue")) {
        std::vector<std::string> tokens = split(line, ' ');
        if (tokens.size() > 1 && tokens[1] != "") {
          m_builder->queue(std::stoi(tokens[1]));
        }
      }
    }

    return m_builder->create();
  }
private:

  bool starts_with(const std::string & s, std::string && prefix) {
    return s.size() >= prefix.size() && s.find(prefix) != std::string::npos;
  }

  std::vector<std::string> split(const std::string & s, char delim) {
    using namespace std;
    vector<string> out;
    string token;
    istringstream ss(s);
    while (getline(ss, token, delim))
      out.push_back(token);
    return std::move(out);
  }

  builder * m_builder;
};



#endif /* DESIGN_PATTERNS_BUILDER_H_ */
