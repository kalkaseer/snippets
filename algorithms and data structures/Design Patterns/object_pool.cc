/*
 * object_pool.cc
 *
 *  Created on: May 3, 2016
 *      Author: Kareem Alkaseer
 */

#include "object_pool.h"
#include <iostream>

int main() {

  connection_pool pool;

  connection_pool::pointer conn = pool.checkout();
  std::cout << conn->address() << std::endl;
  pool.checkin(conn);

  return 0;
}

