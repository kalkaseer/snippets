/*
 * factory_method.cc
 *
 *  Created on: May 3, 2016
 *      Author: Kareem Alkaseer
 */

#include "factory_method.h"

int main() {

  auto pdf = document::make_pdf("pdf document name");
  auto rtf = document::make_rtf("rtf document name");
  auto odf = document::make_odf();

  pdf->print();
  rtf->print();
  odf->print();

  return 0;
}


