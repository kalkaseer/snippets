/*
 * prototype.h
 *
 *  Created on: May 3, 2016
 *      Author: Kareem Alkaseer
 */

#ifndef DESIGN_PATTERNS_PROTOTYPE_H_
#define DESIGN_PATTERNS_PROTOTYPE_H_

#include <vector>
#include <iostream>

enum class operation_type { matrix_analysis, vector_multiplication };

struct operation_args{
  virtual ~operation_args() { }
};

struct matrix_analysis_args : public operation_args {
  explicit matrix_analysis_args(int id) : matrix_id{id} { }
  int matrix_id;
};

class activity {
public:
  static activity * get(operation_type operation, operation_args * args);
  static void destroy();
  virtual ~activity() { }
  virtual void execute() = 0;
protected:
  static void add_prototype(activity * a);
  virtual bool satisfy(operation_type operation, operation_args * args) = 0;
  virtual operation_type operation() = 0;
  virtual activity * clone() = 0;
  virtual activity * make(operation_args * args) = 0;
private:
  static std::vector<activity *> k_prototypes;
};

std::vector<activity *> activity::k_prototypes;

activity * activity::get(operation_type operation, operation_args * args) {
  for (auto a : k_prototypes) {
    if (a->satisfy(operation, args))
      return a->clone();
    else if (a->operation() == operation)
      return a->make(args);
  }
  return NULL;
}

void activity::add_prototype(activity * a) {
  k_prototypes.push_back(a);
}

void activity::destroy() {
  for (auto a : k_prototypes)
    delete a;
  k_prototypes.clear();
}

class matrix_analysis : public activity {
public:
  void execute() override { std::cout << "matrix analysis execute" << std::endl; }
  operation_type operation() override { return operation_type::matrix_analysis; }
  activity * clone() override {
    matrix_analysis * a = new matrix_analysis();
    a->m_tdidf = m_tdidf;
    a->m_determinant = m_determinant;
    a->m_matrix_id = m_matrix_id;
    return a;
  }
  bool satisfy(operation_type operation, operation_args * args) override {
    return operation == operation_type::matrix_analysis &&
      dynamic_cast<matrix_analysis_args *>(args)->matrix_id == m_matrix_id;
  }
  activity * make(operation_args * args) override {
    matrix_analysis * a = new matrix_analysis(
      dynamic_cast<matrix_analysis_args *>(args)->matrix_id);
    add_prototype(a);
    return a;
  }
  int matrix_id() { return m_matrix_id; }
protected:
  matrix_analysis() :
    m_tdidf{0}, m_determinant{0}, m_matrix_id{0}
  { }
  explicit matrix_analysis(int matrix_id)
    : m_tdidf{0}, m_determinant{0}, m_matrix_id{matrix_id}
  { }
private:
  static matrix_analysis * init() {
    auto a = new matrix_analysis(0);
    add_prototype(a);
    return a;
  }
  static matrix_analysis * k_default;
  int m_tdidf;
  int m_determinant;
  int m_matrix_id;
};

matrix_analysis * matrix_analysis::k_default = matrix_analysis::init();



#endif /* DESIGN_PATTERNS_PROTOTYPE_H_ */
