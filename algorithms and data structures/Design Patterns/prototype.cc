/*
 * prototype.cc
 *
 *  Created on: May 4, 2016
 *      Author: Kareem Alkaseer
 */

#include "prototype.h"

int main() {

  matrix_analysis_args args{ 1 };
  activity * a = activity::get(operation_type::matrix_analysis, &args);
  if (a)
    a->execute();
  activity::destroy();
  return 0;
}
