/*
 * abstact_factory.cc
 *
 *  Created on: May 2, 2016
 *      Author: Kareem Alkaseer
 */

#include "abstract_factory.h"

int main() {

  std::cout << "non-templated" << std::endl;
  basic::usage(basic::mechanism::basic);

  std::cout << "templated" << std::endl;
  templated::usage(templated::mechanism::robust);

  return 0;
}
