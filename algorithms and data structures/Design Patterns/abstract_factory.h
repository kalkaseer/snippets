/*
 * abstract_factory.h
 *
 *  Created on: May 2, 2016
 *      Author: Kareem Alkaseer
 */

#ifndef DESIGN_PATTERNS_ABSTRACT_FACTORY_H_
#define DESIGN_PATTERNS_ABSTRACT_FACTORY_H_

#include <iostream>

/* ***************************************************************************
 * Inheritance implementation
 * **************************************************************************/

namespace basic {

/* Interface */
struct shape {
  virtual ~shape() { } // This is not necessary for a pure virtual class.
  virtual void draw() = 0;
};

struct rectangle
  : public shape
{
  void draw() override {
    std::cout << "rectangle draw" << std::endl;
  }
};

struct square
  : public shape
{
  void draw() override {
    std::cout << "square draw" << std::endl;
  }
};

struct circle
  : public shape
{
  void draw() override {
    std::cout << "circle draw" << std::endl;
  }
};

struct ellipse
  : public shape
{
  void draw() override {
    std::cout << "ellipse draw" << std::endl;
  }
};


/* Interface */
struct shape_factory {
  virtual ~shape_factory() { }
  virtual shape * create_straight() = 0;
  virtual shape * create_curved() = 0;
};

struct basic_shape_factory
  : public shape_factory
{
  shape * create_straight() { return new rectangle(); }
  shape * create_curved() { return new circle(); }
};

struct robust_shape_factory
  : public shape_factory
{
  shape * create_straight() { return new square(); }
  shape * create_curved() { return new ellipse(); }
};

enum class mechanism {
  basic,
  robust
};

void usage(mechanism m) {

  shape * shapes[3];
  shape_factory * factory;

  if (m == mechanism::basic) {
    factory = new basic_shape_factory();
  } else {
    factory = new robust_shape_factory();
  }

  shapes[0] = factory->create_curved();
  shapes[1] = factory->create_straight();
  shapes[2] = factory->create_curved();

  for (auto s : shapes)
    s->draw();
}
} /* namespace basic */

/* ***************************************************************************
 * Non-inheritance implementation
 * **************************************************************************/

namespace templated {

struct rectangle {
  void draw() { std::cout << "rectangle draw" << std::endl; }
};

struct square {
  void draw() { std::cout << "square draw" << std::endl; }
};

struct circle {
  void draw() { std::cout << "circle draw" << std::endl; }
};

struct ellipse {
  void draw() { std::cout << "ellipse draw" << std::endl; }
};

template< class Impl >
struct shape_factory {

  using engine_type = Impl;

  using straight_type = typename Impl::straight_type;

  using curved_type = typename Impl::curved_type;

  explicit shape_factory(Impl * m) : impl{ m } { }

  ~shape_factory() { if (impl) delete impl; }

  straight_type * create_straight() { return impl->create_straight(); }

  curved_type * create_curved() { return impl->create_curved(); }

private:
  Impl * impl;
};

struct basic_shape_factory {

  using straight_type = rectangle;
  using curved_type = circle;

  straight_type * create_straight() { return new straight_type(); }

  curved_type * create_curved() { return new curved_type(); }
};

struct robust_shape_factory {

  using straight_type = square;
  using curved_type = ellipse;

  straight_type * create_straight() { return new straight_type(); }

  curved_type * create_curved() { return new curved_type(); }
};

enum class mechanism { basic, robust };

void usage(mechanism m) {

  auto create = [](auto & factory) {
    factory.create_curved()->draw();
    factory.create_straight()->draw();
    factory.create_curved()->draw();
  };

  if (m == mechanism::basic) {
    shape_factory<basic_shape_factory> factory{ new basic_shape_factory() };
    create(factory);
  } else {
    shape_factory<robust_shape_factory> factory{ new robust_shape_factory() };
    create(factory);
  }
}

} /* namespace templated */

#endif /* DESIGN_PATTERNS_ABSTRACT_FACTORY_H_ */
