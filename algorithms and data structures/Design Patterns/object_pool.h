/*
 * object_pool.h
 *
 *  Created on: May 3, 2016
 *      Author: Kareem Alkaseer
 */

#ifndef DESIGN_PATTERNS_OBJECT_POOL_H_
#define DESIGN_PATTERNS_OBJECT_POOL_H_

#include <chrono>
#include <unordered_map>
#include <vector>
#include <mutex>
#include <string>

template< class T >
class object_pool {

public:

  using pointer = T*;

  object_pool()
    : m_expiration_period{ 30000 }
  { }

  virtual ~object_pool() { }

  virtual bool validate(pointer object) = 0;

  virtual void expire(pointer object) = 0;

  pointer checkout() {

    std::lock_guard<mutex_type> lock(m_mutex);
    auto now = std::chrono::steady_clock::now();
    pointer object;

    std::vector<pointer> to_remove;

    for (typename map_type::value_type & pair : m_unlocked) {

      object = pair.first;
      if ((now - pair.second) > m_expiration_period) {
        to_remove.push_back(object);
        expire(object);
        object = nullptr;
      } else {
        if (validate(object)) {
          to_remove.push_back(object);
          m_locked[object] = now;
          for (auto o : to_remove) m_unlocked.erase(o);
          return object;
        } else {
          to_remove.push_back(object);
          expire(object);
          object = nullptr;
        }
      }
    }

    object = create();
    m_locked.emplace(object, now);
    return object;
  }

  void checkin(pointer object) {
    std::lock_guard<mutex_type> lock(m_mutex);
    m_locked.erase(object);
    m_unlocked.emplace(object, std::chrono::steady_clock::now());
  }

protected:

  virtual pointer create() = 0;

private:

  using map_type = std::unordered_map<
    pointer, std::chrono::steady_clock::time_point>;
  using mutex_type = std::recursive_mutex;

  std::chrono::milliseconds m_expiration_period;
  map_type m_locked;
  map_type m_unlocked;
  mutex_type m_mutex;
};

struct connection {
  explicit connection(std::string addr)
    : m_address{ addr }, m_closed{ false }
  { }
  std::string address() { return m_address; }
  bool closed() { return m_closed; }
  void close() { m_closed = true; }
private:
  std::string m_address;
  bool m_closed;
};

class connection_pool
  : public object_pool<connection>
{

public:

  using pointer = object_pool<connection>::pointer;

protected:

  pointer create() override { return new connection("localhost:8080"); }

  bool validate(pointer o) override { return !o->closed(); }

  void expire(pointer o) override { o->close(); }
};



#endif /* DESIGN_PATTERNS_OBJECT_POOL_H_ */
