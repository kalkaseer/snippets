/*
 * builder.cc
 *
 *  Created on: May 3, 2016
 *      Author: Kareem Alkaseer
 */

#include "builder.h"

int main() {

  std::string resource =
    "file stat.txt\n"
    "queue 12\n"
    "sockets 10";

  linux_builder linux;
  bsd_builder bsd;

  director d;

  d.set_builder(&linux);
  configuration * linux_config = d.constuct(resource);

  d.set_builder(&bsd);
  configuration * bsd_config = d.constuct(resource);

  return 0;
}
