# Convex Hull
# Andrew's monotone chain convex hull algorithm constructs the convex hull
# of a set of 2-dimensional points in O(n log n) time.
# It does so by first sorting the points lexicographically 
# (first by x-coordinate, and in case of a tie, by y-coordinate), 
# and then constructing upper and lower hulls of the points in O(n) time.

def convex_hull(points):
    
    """Computes the convex hull of a set of 2D points.

    Input: an iterable sequence of (x, y) pairs representing the points.
    Output: a list of vertices of the convex hull in counter-clockwise
    order, starting from the vertex with the lexicographically smallest
    coordinates.
    Implements Andrew's monotone chain algorithm. O(n log n) complexity.
    """
    
    if not len(points): return points
    
    # Sort lexicographically (tuples are so sorted)
    points = sorted(set(points))
    
    # 2D cross product of OA and OB vectors, i.e. z-component of their
    # 3D cross product.
    # Returns a positive value, if OAB makes a counter-clockwise turn,
    # negative for clockwise turn, and zero if the points are collinear.
    
    def cross(o, a, b):
        return (a[0] - o[0]) * (b[1] - o[1]) - (a[1] - o[1]) * (b[0] - o[0])
    
    lower = []
    for p in points:
        while len(lower) >= 2 and cross(lower[-2], lower[-1], p) <= 0:
            lower.pop()
        lower.append(p)
    
    upper = []
    for p in reversed(points):
        while len(upper) >= 2 and cross(upper[-2], upper[-1], p) <= 0:
            upper.pop()
        upper.append(p)
    
    # Concatenation of the lower and upper hulls gives the convex hull.
    # Last point of each list is omitted because it is repeated at 
    # beginning of the other list.
    return lower[:-1] + upper[:-1]
    
# convex hull of a 10-by-10 grid.
def test_convex_hull():
    assert convex_hull( [(i//10, i%10) for i in range(100)] ) == \
        [ (0,0), (9,0), (9,9), (0,9) ]

#######################

'''
lines = [
"36 30 36 30",
"15 15 15 15",
"46 96 90 100",
"86 86 86 86",
"100 200 100 200",
"-100 200 -100 200",
]
'''

def polygon_test():
    lines = []
    while True:
        try:
            lines.append(raw_input())
        except EOFError:
            break
    squares = 0
    rects = 0
    others = 0
    for line in lines:
        a, b, c, d = map(int, line.split())
        if a < 1 or b < 1 or c < 1 or d < 1:
            others += 1
        elif a == b and a == c and a == d:
            squares += 1
        elif a == c and b == d:
            rects += 1
        else:
            others += 1
    
    print squares, rects, others