/*
 * trie.cc
 *
 *  Created on: Apr 19, 2016
 *      Author: Kareem Alkaseer
 */

#include <vector>
#include <string>
#include <unordered_map>
#include <iostream>

template< class T >
class trie {

public:

  using value_type = T;

  using self_type = trie<value_type>;

  using string_type = std::basic_string<value_type>;

  using suffixes_type = std::vector<string_type>;

  trie() : m_word_end(false) { }

  void insert(const string_type & word) {

    auto node = this;
    for(auto & ch : word) {
      node = &node->m_children[ch];
    }
    node->m_word_end = true;
  }

  bool contains(const string_type & word) {

    auto node = this;
    for(auto & ch : word) {
      if (node->m_children.find(ch) == node->m_children.end())
        return false;
      node = node->m_children.at(ch);
    }
    return node->m_word_end;
  }

  suffixes_type all_suffixes(const string_type & prefix) {

    suffixes_type suffixes;
    if (m_word_end)
      suffixes.push_back(prefix);
    if (m_children.size() != 0) {
      std::for_each(m_children.begin(), m_children.end(),
        [&suffixes, &prefix](child_pair & it) {
          suffixes_type v = it.second.all_suffixes(prefix + it.first);
          std::move(v.begin(), v.end(), std::back_inserter(suffixes));
        }
      );
    }
    return std::move(suffixes);
  }

  suffixes_type autocomplete(const string_type & prefix) {

    auto node = this;
    for(auto & ch : prefix) {
      node = &node->m_children[ch];
    }
    return std::move(node->all_suffixes(prefix));
  }

  void print(int level = 0) {
    for (auto & p : m_children) {
      std::cout << std::string(level, ' ') << p.first << std::endl;
      p.second.print(level + 1);
    }
  }

private:

  using children_type = std::unordered_map<value_type, self_type>;
  using child_pair = typename children_type::value_type;

  bool m_word_end;
  children_type m_children;
};

template< class Iterator >
void print(Iterator first, Iterator last, bool newline = true) {
  for(; first != last; ++first)
    std::cout << *first << " ";
  if (newline)
    std::cout << std::endl;
}

std::string long_string {
  "This is the biggest library change since the initial release."
  "The major problem with this keyword- like identifier is that it must have "
  "a single type as it’s implemented as a thread-local variable. Since there "
  "are so many different kinds of actors (event-based or blocking, untyped or "
  "typed), self needs to perform type erasure at some point, rendering it "
  "ultimately useless. Instead of a thread- local pointer, you can now use the "
  "first argument in functor-based actors to ”catch” the self pointer with "
  "proper type information. actor_ptr has been replaced "
  "CAF now distinguishes between handles to actors, i.e., typed_actor<...> "
  "or simply actor, and addresses of actors, i.e., actor_addr. The reason for "
  "this change is that each actor has a logical, (network-wide) unique "
  "address, which is used by the networking layer of CAF. Furthermore, "
  "for monitoring or linking, the address is all you need. However, the "
  "address is not sufficient for sending messages, because it doesn’t have "
  "any type information. The function last_sender() now returns the address "
  "of the sender. This means that previously valid code suchassend("
  "last_sender(), ...)willcauseacompilererror.However,ther ecommended "
  "way of replying to messages is to return the result from the message "
  "handler. The API for typed actors is now similar to the API for untyped "
  "actors The APIs of typed and untyped actors have been harmonized. Typed "
  "actors can now be published in the network and also use all operations "
  "untyped actors can. The first release under the new name CAF is an "
  "overhaul of the entire library. Some classes have been renamed or "
  "relocated, others have been removed. The purpose of this refactoring"
  " was to make the library easier to grasp and to make its API more "
  "consistent. All classes now live in the namespace caf and all "
  "headers have the top level folder “caf” instead of “cppa”. For "
  "example, #include \"cppa/actor.hpp\" becomes #include \"caf/actor.hpp\". "
  "Further, the convenience header to get all parts of the user API is now "
  "\"caf/all.hpp\". The networking has been separated from the core library. "
  "To get the networking components, simply include \"caf/io/all.hpp\" and "
  "use the namespace caf::io, e.g., caf::io::remote_actor. Version 0.10 still "
  "includes the header cppa/cppa.hpp to make the transition process for "
  "users easier and to not break existing code right away. The header defines "
  "the namespace cppa as an alias for caf. Furthermore, it provides "
  "implementations or type aliases for renamed or removed classes such "
  "as cow_tuple. You won’t get any warning about deprecated headers with "
  "0.10. However, we will add this warnings in the next library version and "
  "remove deprecated code eventually. Even when using the backwards "
  "compatibility header, the new library has breaking changes. "
  "For instance, guard expressions have been removed entirely. "
  "The reasoning behind this decision is that we already have projections "
  "to modify the outcome of a match. Guard expressions add little expressive "
  "power to the library but a whole lot of code that is hard to maintain in "
  "the long run due to its complexity. Using projections to not only perform "
  "type conversions but also to restrict values is the more natural choice."
  "The following table summarizes the changes made to the API."
};

std::vector<std::string> split(const std::string & s, char delim) {
	
	using namespace std;
	vector<string> out;
	string token;
	istringstream ss(s);                // we can use non-copying stream
	while (getline(ss, token, delim))
		out.push_back(token);
	return std::move(out);
}

int main() {

  trie<char> t;
  std::string s = "hello";
  t.insert(s);
  s = "hell";
  t.insert(s);
  s = "hey";
  t.insert(s);

  auto sf = t.all_suffixes("h"); print(sf.begin(), sf.end());
  std::cout << std::endl;

  auto a = t.autocomplete("h");
  print(a.begin(), a.end());
  std::cout << std::endl;
  a = t.autocomplete("hel");
  print(a.begin(), a.end());

  t = trie<char>();
  auto v = split(long_string, ' ');
  for (auto & s : v)
    t.insert(s);
  a = t.autocomplete("t");
  print(a.begin(), a.end());

  return 0;
}
