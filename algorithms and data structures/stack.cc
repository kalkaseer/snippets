/*
 * stack.cc
 *
 *  Created on: Apr 13, 2016
 *      Author: Kareem Alkaseer
 */

#include <cstddef> // size_t
#include <cassert>
#include <vector>

template< typename T, std::size_t N >
class bounded_stack {

public:

  using value_type = T;
  using size_type = std::size_t;

  explicit bounded_stack(bool destruct = true)
    : m_size(0), m_destruct(destruct)
  {}

  ~bounded_stack() {
    if (m_destruct) {
      for (std::size_t pos = 0; pos < m_size; ++pos)
        reinterpret_cast<const T*>(m_data +pos )->~value_type();
    }
  }

  bool push(const value_type & value) {
    if (m_size == N)
      return false;
    new(m_data + m_size) value_type(value);
    ++m_size;
    return true;
  }

  bool push(value_type && value) {
    if (m_size == N)
      return false;
    new(m_data + m_size) value_type(value);
    ++m_size;
    return true;
  }

  // cannot be called on empty stack
  const value_type & pop() const {
    return *reinterpret_cast<const value_type*>(m_data + --m_size);
  }

  // cannot be called on empty stack
  value_type & pop() {
    return *reinterpret_cast<value_type*>(m_data + --m_size);
  }

  size_type size() {
    return m_size;
  }

  bool empty() {
    return m_size == 0;
  }

private:

  using data_type = typename std::aligned_storage<sizeof(T), alignof(T)>::type;

  data_type m_data[N]; // uninitialized storage of size N of type T.
  size_type m_size;    // size of the stack.
  bool m_destruct;     // whether to call dctor of objects in the storage.
};

template< typename T >
class stack {

public:

  using value_type = T;
  using size_type = typename std::vector<T>::size_type;

  stack() = default;

  bool push(const value_type & value) {
    m_data.push_back(value);
    return true;
  }

  bool push(value_type && value) {
    m_data.push_back(std::forward<value_type>(value));
    return true;
  }

  // cannot be called on empty stack
  const value_type & pop() const {
    const value_type & val = m_data.back();
    m_data.pop_back();
    return val;
  }

  // cannot be called on empty stack
  value_type & pop() {
    value_type & val = m_data.back();
    m_data.pop_back();
    return val;
  }

  size_type size() {
    return m_data.size();
  }

  bool empty() {
    return m_data.empty();
  }

private:

  std::vector<value_type> m_data;
};

template< class T >
void test(T & stack, bool bounded) {

  for (int i = 0; i < 10; ++i) {
    assert(stack.push(i));
  }

  if (bounded) assert(!stack.push(11));

  for (int i = 9; i > -1; --i) {
    int val = stack.pop();
    assert(val == static_cast<typename T::size_type>(i));
  }

  assert(stack.empty());
}

int main() {

  bounded_stack<int, 10> bstack;
  test(bstack, true);

  stack<int> ustack;
  test(ustack, false);

  return 0;
}
