# Checks whether an integer is palindromic.
def is_palindrome_int(n):
    
    m = n
    r = 0
    while n > 0:
        d = n % 10;
        r = r * 10 + d;
        n /= 10
    return m == r

# Check whether a string is palindromic.
# Precondition: low < high
def is_palindrome(s, start, end):
    if start >= end:
        return False
    while start < end:
        if s[start] != s[end]:
            return False
        start += 1
        end -= 1
    return True

# Finds the largest palindromic number that contains 'digits' number of digits.
def largest_palindrome_int(digits):
    
    n = pow(10, digits) - 1
    k = pow(10, digits - 1) - 1
    t = pow(n/k, digits - 1)
    max = (0,0,0)
    for i in xrange(n, t, -1):
        for j in xrange(i, t, -1):
            m = i * j
            if is_palindrome_int(m) and m > max[2]:
                max = (i, j, m)
    return max

# Finds the longest palindrome substring in a string
# Time complexity: O(n^2)
# Space complexity: O(1)
def longest_palindrome(s):
    
    maxlength = 1
    
    start = 0
    
    for i in xrange(1, len(s)):
        
        # Search for longest potential even and odd palindromes centred at
        # i - 1, i and i - 1, i + 1 respectively. Mark the bounds if found.
        
        low = i - 1
        higheven = i
        highodd = i + 1
        
        while low >= 0:
            start = low
            low -= 1
            
            if higheven < len(s) and s[low] == s[higheven]:
                if higheven - low + 1 > maxlength:
                    start = low
                    maxlength = max(higheven - low + 1, maxlength)
                higheven += 1
                
            if highodd < len(s) and s[low] == s[highodd]:
                if highodd - low + 1 > maxlength:
                    start = low
                    maxlength = max(highodd - low + 1, maxlength)
                highodd += 1
        
    return s[start : start + maxlength]

# Find all palindromic partitions in a string.
def palindromes_partitions(s, start, end, partitions, visited):
    
    if start < end:
        for i in xrange(end):
            j = end - 1
            while i < j:
                if s[i:j+1] in visited: # this whole string has been scanned
                    break
                if is_palindrome(s, i, j):
                    partitions.append(s[i:j+1])
                    visited.add(s[i:j+1])
                    palindromes_partitions(s, start + i, j, partitions, visited)
                j -= 1
    
    return partitions

def all_palindromes(s, start, end):
    partitions = []
    visited = set()
    return palindromes_partitions(s, start, end, partitions, visited)
    

# Complexity O(log n)
def next_greater_palindrome(n):

    nstring = str(n)
    length = len(nstring)

    def round_up():
        increment = pow(10, (length / 2) + 1)
        return ((n / increment) + 1) * increment

    is_odd = length % 2 != 0
    left_half = nstring[:length / 2]
    middle = nstring[(length - 1) / 2]

    if is_odd:
        increment = pow(10, length / 2)
        m = int(left_half + middle + left_half[::-1])
    else:
        increment = int(1.1 * pow(10, length / 2))
        m = int(left_half + left_half[::-1])

    if m > n: return m

    if middle != '9':
        return m + increment
    else:
        return next_greater_palindrome(round_up())


def test_next_greater_palindrome():

    assert next_greater_palindrome(125) == 131
    assert next_greater_palindrome(250) == 252
    assert next_greater_palindrome(123) == 131
    assert next_greater_palindrome(393) == 404
    assert next_greater_palindrome(4512) == 4554
    assert next_greater_palindrome(1234) == 1331
    assert next_greater_palindrome(1997) == 2002
    assert next_greater_palindrome(1091) == 1111
    assert next_greater_palindrome(31) == 33
    assert next_greater_palindrome(129913) == 129921
    assert next_greater_palindrome(999) == 1001