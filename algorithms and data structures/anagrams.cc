/*
 * anagrams.cc
 *
 *  Created on: Aug 9, 2016
 *      Author: Kareem alkaseer
 */

#ifndef ANAGRAMS_CC_
#define ANAGRAMS_CC_

#include <string>
#include <unordered_map>
#include <algorithm>
#include <iostream>
#include <vector>
#include <memory>
#include <unordered_set>


bool is_anagram(const std::string & s) {

  if (s.size() == 0) return false;
  size_t mid = s.size() / 2;
  for (size_t i = 0; i < mid; ++i)
    if (s[i] != s[s.size() - i - 1])
      return false;
  return true;
}

bool no_ignore(std::string::value_type) { return false; }

auto ignore_ws = [](std::string::value_type c) { return c == ' '; };

auto char_to_pos = [](std::string::value_type c) {
  return static_cast<size_t>(c - 'a');
};

bool is_anagram(
  const std::string & s1, const std::string & s2,
  std::function<bool(std::string::value_type)> ignore = no_ignore
) {

  std::unordered_map<std::string::value_type, size_t> m;
  size_t i;

  for (auto c : s1) {
    if (! ignore(c)) {
      if (m.find(c) != m.end())
        ++m[c];
      else
        m[c] = 1;
    }
  }

  for (auto c : s2) {
    if (! ignore(c)) {
      if (m.find(c) != m.end())
        --m[c];
      else
        m[c] = -1;
    }
  }

  for (auto i : m) {
    if (i.second) return false; // non-zero
  }

  return true;
}

bool is_anagram(
  const std::string & s1, const std::string & s2,
  size_t alphabet_count,
  std::function<size_t(std::string::value_type)> pos,
  std::function<bool(std::string::value_type)> ignore = no_ignore
) {

  size_t i;

  const auto counts = std::get_temporary_buffer<size_t>(alphabet_count);
  std::uninitialized_fill(counts.first, counts.first + counts.second, 0);

  for (i = 0; i < s1.size(); ++i) {
    if (!ignore(s1[i])) ++(*(counts.first + pos(s1[i])));
  }

  for (i = 0; i < s2.size(); ++i)
    if (!ignore(s2[i])) --(*(counts.first + pos(s2[i])));

  for (i = 0; i < alphabet_count; ++i)
    if (counts.first[i]) // non-zero
      return false;

  std::return_temporary_buffer(counts.first);

  return true;
}

std::vector<std::vector<std::string>>
all_anagrams(const std::vector<std::string> & v) {

  std::unordered_set<size_t> processed;
  std::hash<std::string> hasher;

  std::vector< std::vector< std::string >> out;

  for (auto & s1 : v) {
    if (processed.find(hasher(s1)) == processed.end()) {
      std::vector<std::string> current(1, s1);
      for (auto & s2 : v) {
        if (s1 != s2 && is_anagram(s1, s2, ignore_ws)) {
          current.push_back(s2);
          processed.insert(hasher(s2));
        }
      }
      if (current.size() == 1)
        continue;
      std::sort(current.begin(), current.end());
      out.emplace_back(std::move(current));
    }
  }

  return std::move(out);
}


template< class C >
void print(const C & c) {
  for (auto & i : c)
    std::cout << i << " ";
}


int main() {

  std::vector<std::string> v {
    "pear",
    "amleth",
    "dormitory",
    "tinsel",
    "dirty room",
    "hamlet",
    "listen",
    "silent"
  };

  auto result = all_anagrams(v);
  for (auto & i : result) {
    print(i);
    std::cout << std::endl;
  }
}


#endif /* ANAGRAMS_CC_ */
