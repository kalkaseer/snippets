/*
 * compile_time.cc
 *
 *  Created on: Apr 19, 2016
 *      Author: Kareem Alkaseer
 */

#include <cstddef>
#include <cinttypes>
#include <type_traits>

namespace compile_time {

template< std::size_t N >
struct factorial {
  constexpr static std::size_t value = N * factorial<N - 1>::value;
};

template<>
struct factorial<1> {
  constexpr static std::size_t value = 1;
};



template< class T, T n >
constexpr T power(const T x) {
  return power<T, n-1>(x) * x;
}

template<> constexpr int8_t  power<int8_t,  1>(const int8_t x)  { return x; }
template<> constexpr int16_t power<int16_t, 1>(const int16_t x) { return x; }
template<> constexpr int32_t power<int32_t, 1>(const int32_t x) { return x; }
template<> constexpr int64_t power<int64_t, 1>(const int64_t x) { return x; }

template<> constexpr uint8_t  power<uint8_t,  1>(const uint8_t x)  { return x; }
template<> constexpr uint16_t power<uint16_t, 1>(const uint16_t x) { return x; }
template<> constexpr uint32_t power<uint32_t, 1>(const uint32_t x) { return x; }
template<> constexpr uint64_t power<uint64_t, 1>(const uint64_t x) { return x; }

template<> constexpr
typename std::enable_if<
  !std::is_same<std::size_t, uint64_t>::value, std::size_t >::type
power<std::size_t, 1>(const std::size_t x) { return x; }

template<> constexpr int8_t  power<int8_t,  0>(const int8_t x)  { return 1; }
template<> constexpr int16_t power<int16_t, 0>(const int16_t x) { return 1; }
template<> constexpr int32_t power<int32_t, 0>(const int32_t x) { return 1; }
template<> constexpr int64_t power<int64_t, 0>(const int64_t x) { return 1; }

template<> constexpr uint8_t  power<uint8_t,  0>(const uint8_t x)  { return 1; }
template<> constexpr uint16_t power<uint16_t, 0>(const uint16_t x) { return 1; }
template<> constexpr uint32_t power<uint32_t, 0>(const uint32_t x) { return 1; }
template<> constexpr uint64_t power<uint64_t, 0>(const uint64_t x) { return 1; }

template<> constexpr
typename std::enable_if<
  !std::is_same<std::size_t, uint64_t>::value, std::size_t >::type
power<std::size_t, 0>(const std::size_t x) { return 1; }



template< std::size_t N>
struct unroll {
  template< class Function >
  constexpr static void iterate(std::size_t i, Function func) {
    func(i);
    unroll<N - 1>::iterate(i + 1, func);
  }
};

template< std::size_t N >
struct loop {
  template< class Function >
  constexpr static void iterate(Function func) {
    for (std::size_t i = 0; i < N; ++i)
      func(i);
  }
};

} /* namespace compile_time */

#include <iostream>

int main() {
  std::cout << compile_time::factorial<5>::value << std::endl;
  std::cout << compile_time::power<std::size_t, 16>(2) << std::endl;

  std::size_t m = 0;
  compile_time::loop<10>::iterate([&](std::size_t i){ m +=i; });
  std::cout << m << std::endl;
  return 0;
}
