# http://www.geeksforgeeks.org/given-matrix-o-x-find-largest-subsquare-surrounded-x/

def solve(matrix):
  
  mx = 1 # initial result
  
  h = [ [0]*len(matrix[0]) for _ in xrange(len(matrix[0])) ]
  v = [ [0]*len(matrix) for _ in xrange(len(matrix)) ]
  
  for i in xrange(len(matrix)):
    for j in xrange(len(matrix[0])):
      if matrix[i][j] == 'O':
        v[i][j] = h[i][j] = 0
      else:
        h[i][j] = 1 if j == 0 else h[i][j-1] + 1
        v[i][j] = 1 if i == 0 else v[i-1][j] + 1
  
  for i in xrange(len(matrix)-1, -1, -1):
    for j in xrange(len(matrix[0])-1, -1, -1):
      mn = min(h[i][j], v[i][j])
      while mn > mx:
        print "check", (i,j-mn+1), 'and', (i-mn+1, j)
        print v[i][j-mn+1], h[i-mn+1][j]
        if v[i][j-mn+1] >= mn and h[i-mn+1][j] >= mn:
          mx = mn
        mn -= 1
  
  for i in h: print i
  print '\n'
  for i in v: print i
  
  return mx


testcases = [
  [
    [ 'X', 'O', 'X', 'X', 'X', 'X' ],
    [ 'X', 'O', 'X', 'X', 'O', 'X' ],
    [ 'X', 'X', 'X', 'O', 'O', 'X' ],
    [ 'O', 'X', 'X', 'X', 'X', 'X' ],
    [ 'X', 'X', 'X', 'O', 'X', 'O' ],
    [ 'O', 'O', 'X', 'O', 'O', 'O' ],
  ]
]