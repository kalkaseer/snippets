/* Copyright © Kareem Alkaseer 2016 */

#ifndef STRING_LENGTH_H_
#define STRING_LENGTH_H_

#include <cstdint>

enum class base : uint8_t {
	binary    = 2,
	octal     = 8,
	decimal   = 10,
	hex       = 16,
	b36       = 26
};

constexpr uint8_t integer(base b) {
	return static_cast<uint8_t>(b);
}

template< class I >
struct length_t { };


namespace details {

template< class T >
struct unsigned_t { };

template<>
struct unsigned_t<int8_t> {
	typedef uint8_t type;
};

template<>
struct unsigned_t<int16_t> {
	typedef uint16_t type;
};

template<>
struct unsigned_t<int32_t> {
	typedef uint32_t type;
};

template<>
struct unsigned_t<int64_t> {
	typedef uint64_t type;
};

template< class I, base b >
struct length_t {

	static int32_t size(I i) {
		
		int32_t carry = i < 0;
		return length_t< typename unsigned_t<I>::type, b >::size(
			static_cast< typename unsigned_t<I>::type >(carry ? -i : i)) + carry;
	}
};

template<>
struct length_t< uint8_t, base::decimal > {
	
	static int32_t size(uint8_t i) {
		
	  if (i <  10) return 1;
	  if (i < 100) return 2;
	  return 3;
	}
};

template<>
struct length_t< uint16_t, base::decimal > {
	
	static int32_t size(uint16_t i) {
		
	  if (i <    10) return 1;
	  if (i <   100) return 2;
	  if (i <  1000) return 3;
	  if (i < 10000) return 4;
	  return 5;
	}
};

template<>
struct length_t< uint32_t, base::decimal > {
	
	static int32_t size(uint32_t i) {
		
	  if (i <         10) return 1;
	  if (i <        100) return 2;
	  if (i <       1000) return 3;
	  if (i <      10000) return 4;
	  if (i <     100000) return 5;
	  if (i <    1000000) return 6;
	  if (i <   10000000) return 7;
	  if (i <  100000000) return 8;
	  if (i < 1000000000) return 9;
	  return 10;
	}
};

template<>
struct length_t< uint64_t, base::decimal > {
	
	static int32_t size(uint64_t i) {
		
		if (i <                   10ULL) return  1;
		if (i <                  100ULL) return  2;
		if (i <                 1000ULL) return  3;
		if (i <                10000ULL) return  4;
		if (i <               100000ULL) return  5;
		if (i <              1000000ULL) return  6;
		if (i <             10000000ULL) return  7;
		if (i <            100000000ULL) return  8;
		if (i <           1000000000ULL) return  9;
		if (i <          10000000000ULL) return 10;
		if (i <         100000000000ULL) return 11;
		if (i <        1000000000000ULL) return 12;
		if (i <       10000000000000ULL) return 13;
		if (i <      100000000000000ULL) return 14;
		if (i <     1000000000000000ULL) return 15;
		if (i <    10000000000000000ULL) return 16;
		if (i <   100000000000000000ULL) return 17;
		if (i <  1000000000000000000ULL) return 18;
		if (i < 10000000000000000000ULL) return 19;
		return 20;
	}
};

template<>
struct length_t< uint8_t, base::binary > {
	
	static int32_t size(uint8_t i) {
		
		if (i < (1 << 1)) return 1;
		if (i < (1 << 2)) return 2;
		if (i < (1 << 3)) return 3;
		if (i < (1 << 4)) return 4;
		if (i < (1 << 5)) return 5;
		if (i < (1 << 6)) return 6;
		if (i < (1 << 7)) return 7;
		return 8;
	}
};

template<>
struct length_t< uint16_t, base::binary > {
	
	static int32_t size(uint16_t i) {
		
		if (i < 1 <<  1) return  1;
		if (i < 1 <<  2) return  2;
		if (i < 1 <<  3) return  3;
		if (i < 1 <<  4) return  4;
		if (i < 1 <<  5) return  5;
		if (i < 1 <<  6) return  6;
		if (i < 1 <<  7) return  7;
		if (i < 1 <<  8) return  8;
		if (i < 1 <<  9) return  9;
		if (i < 1 << 10) return 10;
		if (i < 1 << 11) return 11;
		if (i < 1 << 12) return 12;
		if (i < 1 << 13) return 13;
		if (i < 1 << 14) return 14;
		if (i < 1 << 15) return 15;
		return 16;
	}
};

template<>
struct length_t< uint32_t, base::binary > {
	
	static int32_t size(uint32_t i) {
		
		if (i < 1 <<  1) return  1;
		if (i < 1 <<  2) return  2;
		if (i < 1 <<  3) return  3;
		if (i < 1 <<  4) return  4;
		if (i < 1 <<  5) return  5;
		if (i < 1 <<  6) return  6;
		if (i < 1 <<  7) return  7;
		if (i < 1 <<  8) return  8;
		if (i < 1 <<  9) return  9;
		if (i < 1 << 10) return 10;
		if (i < 1 << 11) return 11;
		if (i < 1 << 12) return 12;
		if (i < 1 << 13) return 13;
		if (i < 1 << 14) return 14;
		if (i < 1 << 15) return 15;
		if (i < 1 << 16) return 16;
		if (i < 1 << 17) return 17;
		if (i < 1 << 18) return 18;
		if (i < 1 << 19) return 19;
		if (i < 1 << 20) return 20;
		if (i < 1 << 21) return 21;
		if (i < 1 << 22) return 22;
		if (i < 1 << 23) return 23;
		if (i < 1 << 24) return 24;
		if (i < 1 << 25) return 25;
		if (i < 1 << 26) return 26;
		if (i < 1 << 27) return 27;
		if (i < 1 << 28) return 28;
		if (i < 1 << 29) return 29;
		if (i < 1 << 30) return 30;
		if (i < 1 << 31) return 31;
		return 32;
	}
};

template<>
struct length_t< uint64_t, base::binary > {
	
	static int32_t size(uint64_t i) {
		
		if (i < 1ULL <<  1) return  1;
		if (i < 1ULL <<  2) return  2;
		if (i < 1ULL <<  3) return  3;
		if (i < 1ULL <<  4) return  4;
		if (i < 1ULL <<  5) return  5;
		if (i < 1ULL <<  6) return  6;
		if (i < 1ULL <<  7) return  7;
		if (i < 1ULL <<  8) return  8;
		if (i < 1ULL <<  9) return  9;
		if (i < 1ULL << 10) return 10;
		if (i < 1ULL << 11) return 11;
		if (i < 1ULL << 12) return 12;
		if (i < 1ULL << 13) return 13;
		if (i < 1ULL << 14) return 14;
		if (i < 1ULL << 15) return 15;
		if (i < 1ULL << 16) return 16;
		if (i < 1ULL << 17) return 17;
		if (i < 1ULL << 18) return 18;
		if (i < 1ULL << 19) return 19;
		if (i < 1ULL << 20) return 20;
		if (i < 1ULL << 21) return 21;
		if (i < 1ULL << 22) return 22;
		if (i < 1ULL << 23) return 23;
		if (i < 1ULL << 24) return 24;
		if (i < 1ULL << 25) return 25;
		if (i < 1ULL << 26) return 26;
		if (i < 1ULL << 27) return 27;
		if (i < 1ULL << 28) return 28;
		if (i < 1ULL << 29) return 29;
		if (i < 1ULL << 30) return 30;
		if (i < 1ULL << 31) return 31;
		if (i < 1ULL << 32) return 32;
		if (i < 1ULL << 33) return 33;
		if (i < 1ULL << 34) return 34;
		if (i < 1ULL << 35) return 35;
		if (i < 1ULL << 36) return 36;
		if (i < 1ULL << 37) return 37;
		if (i < 1ULL << 38) return 38;
		if (i < 1ULL << 39) return 39;
		if (i < 1ULL << 40) return 40;
		if (i < 1ULL << 41) return 41;
		if (i < 1ULL << 42) return 42;
		if (i < 1ULL << 43) return 43;
		if (i < 1ULL << 44) return 44;
		if (i < 1ULL << 45) return 45;
		if (i < 1ULL << 46) return 46;
		if (i < 1ULL << 47) return 47;
		if (i < 1ULL << 48) return 48;
		if (i < 1ULL << 49) return 49;
		if (i < 1ULL << 50) return 50;
		if (i < 1ULL << 51) return 51;
		if (i < 1ULL << 52) return 52;
		if (i < 1ULL << 53) return 53;
		if (i < 1ULL << 54) return 54;
		if (i < 1ULL << 55) return 55;
		if (i < 1ULL << 56) return 56;
		if (i < 1ULL << 57) return 57;
		if (i < 1ULL << 58) return 58;
		if (i < 1ULL << 59) return 59;
		if (i < 1ULL << 60) return 60;
		if (i < 1ULL << 61) return 61;
		if (i < 1ULL << 62) return 62;
		if (i < 1ULL << 63) return 63;
		return 64;
	}
};

template<>
struct length_t< uint8_t, base::hex > {
	
	static int32_t size(uint8_t i) {
		
		if (i < 0x10) return 1;
		return 2;
	}
};

template<>
struct length_t< uint16_t, base::hex > {
	
	static int32_t size(uint16_t i) {
		
		if (i <   0x10) return 1;
		if (i <  0x100) return 2;
		if (i < 0x1000) return 3;
		return 4;
	}
};

template<>
struct length_t< uint32_t, base::hex > {
	
	static int32_t size(uint32_t i) {
		
		if (i <       0x10) return 1;
		if (i <      0x100) return 2;
		if (i <     0x1000) return 3;
		if (i <    0x10000) return 4;
		if (i <   0x100000) return 5;
		if (i <  0x1000000) return 6;
		if (i < 0x10000000) return 7;
		return 8;
	}
};

template<>
struct length_t< uint64_t, base::hex > {
	
	static int32_t size(uint64_t i) {
		
		if (i <               0x10) return  1;
		if (i <              0x100) return  2;
		if (i <             0x1000) return  3;
		if (i <            0x10000) return  4;
		if (i <           0x100000) return  5;
		if (i <          0x1000000) return  6;
		if (i <         0x10000000) return  7;
		if (i <        0x100000000) return  8;
		if (i <       0x1000000000) return  9;
		if (i <      0x10000000000) return 10;
		if (i <     0x100000000000) return 11;
		if (i <    0x1000000000000) return 12;
		if (i <   0x10000000000000) return 13;
		if (i <  0x100000000000000) return 14;
		if (i < 0x1000000000000000) return 15;
		return 16;
	}
};

template<>
struct length_t< uint8_t, base::octal > {
	
	static int32_t size(uint8_t i) {
	
		if (i <  010) return 1;
		if (i < 0100) return 2;
		return 3;
	}
};

template<>
struct length_t< uint16_t, base::octal > {
	
	static int32_t size(uint16_t i) {
	
		if (i <     010) return 1;
		if (i <    0100) return 2;
		if (i <   01000) return 3;
		if (i <  010000) return 4;
		if (i < 0100000) return 5;
		return 6;
	}
};

template<>
struct length_t< uint32_t, base::octal > {
	
	static int32_t size(uint32_t i) {
	
		if (i <          010) return  1;
		if (i <         0100) return  2;
		if (i <        01000) return  3;
		if (i <       010000) return  4;
		if (i <      0100000) return  5;
		if (i <     01000000) return  6;
		if (i <    010000000) return  7;
		if (i <   0100000000) return  8;
		if (i <  01000000000) return  9;
		if (i < 010000000000) return 10;
		return 11;
	}
};

template<>
struct length_t< uint64_t, base::octal > {
	
	static int32_t size(uint64_t i) {
	
		if (i <                     010) return  1;
		if (i <                    0100) return  2;
		if (i <                   01000) return  3;
		if (i <                  010000) return  4;
		if (i <                 0100000) return  5;
		if (i <                01000000) return  6;
		if (i <               010000000) return  7;
		if (i <              0100000000) return  8;
		if (i <             01000000000) return  9;
		if (i <            010000000000) return 10;
		if (i <           0100000000000) return 11;
		if (i <          01000000000000) return 12;
		if (i <         010000000000000) return 13;
		if (i <        0100000000000000) return 14;
		if (i <       01000000000000000) return 15;
		if (i <      010000000000000000) return 16;
		if (i <     0100000000000000000) return 17;
		if (i <    01000000000000000000) return 18;
		if (i <   010000000000000000000) return 19;
		if (i <  0100000000000000000000) return 20;
		if (i < 01000000000000000000000) return 21;
		return 22;
	}
};

template<>
struct length_t< uint8_t, base::b36 > {
	
	static int32_t size(uint8_t i) {
		
		if (i < 36) return 1;
		return 2;
	}
};

template<>
struct length_t< uint16_t, base::b36 > {
	
	static int32_t size(uint16_t i) {
		
		if (i <    36) return 1;
		if (i <  1296) return 2;
		if (i < 46656) return 3;
		return 4;
	}
};

template<>
struct length_t< uint32_t, base::b36 > {
	
	static int32_t size(uint32_t i) {
		
		if (i <         36) return 1;
		if (i <       1296) return 2;
		if (i <      46656) return 3;
		if (i <    1679616) return 4;
		if (i <   60466176) return 5;
		if (i < 2176782336) return 6;
		return 7;
	}
};

template<>
struct length_t< uint64_t, base::b36 > {
	
	static int32_t size(uint64_t i) {
		
		if (i <                  36) return 1;
		if (i <                1296) return 2;
		if (i <               46656) return 3;
		if (i <             1679616) return 4;
		if (i <            60466176) return 5;
		if (i <          2176782336) return 6;
		if (i <         78364164096) return 7;
		if (i <       2821109907456) return 8;
		if (i <     101559956668416) return 9;
		if (i <    3656158440062976) return 10;
		if (i <  131621703842267136) return 11;
		if (i < 4738381338321616896) return 12;
		return 13;
	}
};

} // namespace details


template< class I >
int32_t length(I i, base b) {
	switch (b) {
		case base::binary:
			return details::length_t<I, base::binary>::size(i);
		case base::octal:
			return details::length_t<I, base::octal>::size(i);
		case base::decimal:
			return details::length_t<I, base::decimal>::size(i);
		case base::hex:
			return details::length_t<I, base::hex>::size(i);
		case base::b36:
			return details::length_t<I, base::b36>::size(i);
		default:
			return -1;
	}
}


#endif // STRING_LENGTH_H_