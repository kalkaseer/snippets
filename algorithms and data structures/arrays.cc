/*
 * array.cc
 *
 *  Created on: Apr 16, 2016
 *      Author: Kareem Alkaseer
 */

#include <algorithm>
#include <cassert>
#include <iterator>
#include <vector>
#include <iostream>
#include <cstdlib> // std::abs
#include <tuple>
#include <random>
#include <unordered_set>
#include <set>
#include <future>
#include <forward_list>
#include "timeit.h"

template< class T >
void print(T & heap) {
  for (auto it = heap.begin(); it != heap.end(); ++it)
    std::cout << *it << " ";
  std::cout << std::endl;
}

template< class Iterator >
void print(Iterator first, Iterator last) {
  for(; first != last; ++first)
    std::cout << *first << " ";
}

/* *********************************************************************
 * Given an array of distinct integers, find length of the longest subarray
 * which contains numbers that can be arranged in a continuous sequence.
 *
 *  Space complexity: O(1)
 *  Time complexity: O(n)
 * *********************************************************************/

template< class Iterator >
std::size_t max_contiguous_length(Iterator begin, Iterator end) {

  std::size_t maxlen = 0;
  auto low = begin;
  bool ok = false;
  for (auto it = begin + 1; it != end; ++it) {
    if (*it - *(it-1) == 1) {
      if (!ok) {
        ok = true;
        low = it - 1;
      }
      maxlen = std::max(maxlen, static_cast<std::size_t>(std::distance(low, it)));
    } else {
      ok = false;
    }
  }

  return maxlen + 1;
}

/* *********************************************************************
 * Space complexity: O(1)
 * Time complexity: O(n^2)
 *
 * The important thing to note in question is, it is given that all elements
 * are distinct. If all elements are distinct, then a subarray has contiguous
 * elements if and only if the difference between maximum and minimum elements
 * in subarray is equal to the difference between last and first indexes of
 * subarray. So the idea is to keep track of minimum and maximum element in
 * every subarray.
 * *********************************************************************/

template< class Iterator >
std::size_t max_contiguous_length2(Iterator begin, Iterator end) {

  std::size_t maxlen = 1;
  for (auto it = begin; it != end - 1; ++it) {
    auto min = *it;
    auto max = *it;
    for (auto it2 = it + 1; it2 != end; ++it2) {
      min = std::min(min, *it2);
      max = std::max(max, *it2);
      if (max - min == std::distance(it, it2))
        maxlen = std::max(maxlen, static_cast<std::size_t>(max-min+1));
    }
  }
  return maxlen;
}


void test_find_contiguous() {

  std::vector<int> v{ 10, 2, 3, 4, 43, 54 };
  assert(max_contiguous_length(v.begin(), v.end()) == 3);

  v.clear();
  v.assign({ 1, 2, 3, 54, 6, 8, 10, 10, 11, 11, 12, 13, 5 });
  assert(max_contiguous_length(v.begin(), v.end()) == 3);

  v.clear();
  v.assign({ 10, 2, 3, 4, 43, 54 });
  using iterator = std::vector<int>::iterator;
  std::uint64_t t1 = timeit(max_contiguous_length<iterator>, v.begin(), v.end());
  std::uint64_t t2 = timeit(max_contiguous_length2<iterator>, v.begin(), v.end());
  std::cout << "max_contiguous_length v1 "
            << (t1 < t2 ? "faster than" : t1 > t2 ? "slower than" : "as fast as")
            << " v2 -- difference: "
            << std::abs(static_cast<int64_t>(t1) - static_cast<int64_t>(t2))
            << std::endl;
}

/* *********************************************************************
 * Kadane's algorithm solves the largest contiguous subarray (in sum) in O(n)
 * which works for arrays having both any mixture of negative, zero and positive
 * elements.
 * *********************************************************************/
template< class T >
T max_sum_subarray(T arr[], size_t n) {
  T max_so_far = arr[0];
  T max_ending_here = arr[0];
  
  for (size_t i = 1; i < n; ++i) {
    max_ending_here = std::max(arr[i], max_ending_here + arr[i]);
    max_so_far = std::max(max_ending_here, max_so_far);
  }
  
  return max_so_far;
}

template< class Iterator, class T >
void max_sum_subarray(Iterator begin, Iterator end, T & mx) {
  T max_so_far = *begin;
  T max_ending_here = *begin;
  ++begin;
  for (; begin != end; ++begin) {
    max_ending_here = std::max(*begin, max_ending_here + *begin);
    max_so_far = std::max(max_ending_here, max_so_far);
  }
  
  mx = max_so_far;
}

/* *********************************************************************
 * Kadane's algorithm which works only for arrays whose elements are not all
 * negative. Time complexity O(n).
 * *********************************************************************/
template< class T >
T max_sum_subarray_positive(T arr[], size_t n) {
  T max_so_far = 0;
  T max_ending_here = 0;
  
  for (size_t i = 0; i < n; ++i) {
    max_ending_here = std::max(0, max_ending_here + arr[i]);
    max_so_far = std::max(max_ending_here, max_so_far);
  }
  
  return max_so_far;
}

/* Kadane's algorithm **********************************************/

/* *********************************************************************
 * Maximum contiguous sumarray sum using divide and conquer.
 * Time complexity: O(n log n).
 *
 * The idea is to divide the array into two halves. Return the maximum of
 * 1. maximum subarray in the left half
 * 2. maximum subarray in the right half
 * 3. maximum subarray that crosses the midpoint
 * *********************************************************************/

template< class T >
T max_crossing_sum_subarray_divide_and_conquer(
  T arr[], size_t lo, size_t mid, size_t hi
) {
  T sum = 0;
  T left_sum = std::numeric_limits<T>::min();
  
  for (size_t i = mid; i > lo-1; --i) {
    sum += arr[i];
    if (sum > left_sum)
      left_sum = sum;
  }
  
  sum = 0;
  T right_sum = std::numeric_limits<T>::min();
  
  for (size_t i = mid + 1; i < hi+1; ++i) {
    sum += arr[i];
    if (sum > right_sum)
      right_sum = sum;
  }
  
  return left_sum + right_sum;
}

template< class T >
T max_sum_subarray_divide_and_conquer(
  T arr[], size_t lo, size_t hi
) {
  if (lo == hi)
    return arr[lo];
  
  size_t mid = (lo + hi) / 2;
  
  return std::max(
    max_sum_subarray_divide_and_conquer(arr, lo, mid),
    std::max(
      max_sum_subarray_divide_and_conquer(arr, mid+1, hi),
      max_crossing_sum_subarray_divide_and_conquer(arr, lo, mid, hi)));
}

/* Maximum contiguous subarray using divide and conquer ***********/

/* *********************************************************************
 * Maximum contiguous subarray in terms of product (not sum)
 * *********************************************************************/
template< class T >
T max_product_subarray(T arr[], size_t n) {
  
  T max_forward = std::numeric_limits<T>::min();
  T max_backward = std::numeric_limits<T>::min();
  
  T max_so_far = 1;
  
  for (size_t i = 0; i < n; ++i) {
    max_so_far *= arr[i];
    if (max_so_far == 0) {
      max_so_far = 1;
      continue;
    }
    if (max_forward < max_so_far)
      max_forward = max_so_far;
  }
  
  max_so_far = 1;
  
  size_t i = n;
  while (i != 0) {
    --i;
    max_so_far *= arr[i];
    if (max_so_far == 0) {
      max_so_far = 1;
      continue;
    }
    if (max_backward < max_so_far)
      max_backward = max_so_far;
  }
  
  return std::max(max_forward, max_backward);
}

void test_max_product_subarray() {
  
  {
    int v[] = { 6, -3, -10, 0, 2 };
    size_t n = sizeof(v) / sizeof(v[0]);
    assert(max_product_subarray(v, n) == 180);
  }

  {
    int v[] = { -1, -3, -10, 0, 60 };
    size_t n = sizeof(v) / sizeof(v[0]);
    assert(max_product_subarray(v, n) == 60);
  }
  
  {
    int v[] = { -1, -2, -3, 4 };
    size_t n = sizeof(v) / sizeof(v[0]);
    assert(max_product_subarray(v, n) == 24);
  }
  
  {
    int v[] = { -10 };
    size_t n = sizeof(v) / sizeof(v[0]);
    assert(max_product_subarray(v, n) == -10);
  }
  
  {
    int v[] = { -1, -2, -3, -4 };
    size_t n = sizeof(v) / sizeof(v[0]);
    assert(max_product_subarray(v, n) == 24);
  }
}



/* *********************************************************************
 * Given an array of positive integer sorted in non-decreasing order, find the
 * smallest positive integer that cannot be the sum of any elements of
 * the array.
 *
 * The naive solution is: starting at [1...], check if all elements of the
 * array can sum to the current value of the [1...] sequence. This is an
 * NP complete approach because it reduces to the subset sum problem.
 *
 * Another approach that has O(n) time complexity is:
 *
 * a) the smallest possible such integer is 1.
 * b) let the solution be b.
 * c) for each element of the array, let the element be a:
 *    note that if a > b then every element after a is greater than b, so if
 *    we let b = b + a if a <= b at the current element and we find out the
 *    the next element a' > b then a + b sum to a number less than a' that is
 *    by definition not in the array. This is the smallest positive integer.
 *
 * Time complexity: O(n)
 * Space complexity: O(1)
 * *********************************************************************/
template< class Iterator >
std::size_t smallest_excluded_int(Iterator begin, Iterator end) {

  std::size_t b = 1;
  for (auto it = begin; it != end && *it <= b; ++it) {
    b += *it;
  }

  return b;
}

void test_smallest_excluded_int() {
  std::vector<unsigned int> v{ 43, 56, 58, 60, 70 };
  assert(smallest_excluded_int(v.begin(), v.end()) == 1);

  v.clear(); v.assign({1, 2, 3, 4, 5, 7});
  assert(smallest_excluded_int(v.begin(), v.end()) == 23);

  v.clear(); v.assign({1, 3, 6, 10, 11, 15});
  assert(smallest_excluded_int(v.begin(), v.end()) == 2);

  v.clear(); v.assign({1, 1, 1, 1});
  assert(smallest_excluded_int(v.begin(), v.end()) == 5);

  v.clear(); v.assign({1, 1, 3, 4});
  assert(smallest_excluded_int(v.begin(), v.end()) == 10);

  v.clear(); v.assign({1, 2, 5, 10, 20, 40});
  assert(smallest_excluded_int(v.begin(), v.end()) == 4);

  v.clear(); v.assign({1, 2, 3, 4, 5, 6});
  assert(smallest_excluded_int(v.begin(), v.end()) == 22);
}

/* *********************************************************************
 * Given an array of integers and a number x, find the smallest subarray with
 * sum greater than the given value.
 *
 * Examples:
 *
 * arr[] = {1, 4, 45, 6, 0, 19}
 * x  =  51
 * Output: 3
 * Minimum length subarray is {4, 45, 6}
 *
 * arr[] = {1, 10, 5, 2, 7}
 * x  = 9
 * Output: 1
 * Minimum length subarray is {10}
 *
 * arr[] = {1, 11, 100, 1, 0, 200, 3, 2, 1, 250}
 * x = 280
 * Output: 4
 * Minimum length subarray is {100, 1, 0, 200}
 * *********************************************************************/

template< class Iter >
std::tuple<Iter, Iter> smallest_subarray_ge(
  Iter begin, Iter end, typename Iter::value_type x
) {

  using value_type = typename Iter::value_type;

  value_type sum = 0;
  auto a = begin;
  auto b = begin;

  for (auto it = begin; it != end; ++it) {

    std::cout << "it " << *it << std::endl;

    if (*it > x) {
      std::vector<value_type> v{it, it+1}; print(v);
      return std::make_tuple(it, it+1);
    }
    sum += *it;
    if (sum > x) {
      for (auto it2 = b; it2 < it; ++it2) {
        std::cout << "it2 " << *it2 << " sum " << sum << " it " << *it <<std::endl;
        sum -= *it2;
        if (sum <= x) {
          sum = 0;
          if (it2 - it < b - a) {
            a = it2;
            b = it;
            std::cout << "\ta " << *a << " b " << *b << std::endl;
          }
          break;
        }
      }
    }
  }

  std::vector<value_type> v{a, b+1}; print(v);

  return std::make_tuple(a, b + 1);
}

/* *********************************************************************
 * Time complexity: O(n)
 * *********************************************************************/
template< class Iter >
std::tuple<Iter, Iter> smallest_subarray_ge2(
  Iter begin, Iter end, typename Iter::value_type x
) {

  using value_type = typename Iter::value_type;

  std::size_t minlen = std::distance(begin, end);

  value_type sum = 0;
  auto b = begin;
  auto m = begin;
  auto k = begin;

  while (b != end) {
    for (; sum <= x && b < end; ++b)
      sum += *b;
    for (; sum > x && begin < end; ++begin) {
      if (b - begin < minlen) {
        minlen = b - begin;
        m = begin;
        k = b;
        std::vector<value_type> v{m, k}; print(v); // todo remove
      }
      sum -= *begin;
    }
  }

  return std::make_tuple(m, k);
}

void test_smallest_subarray_ge() {

  std::vector<int> v{ 1, 4, 45, 6, 0, 19 };
  auto result = std::make_tuple(v.begin() + 1, v.begin() + 4);
//  assert(smallest_subarray_ge(v.begin(), v.end(), 51) == result);
  assert(smallest_subarray_ge2(v.begin(), v.end(), 51) == result);


  v.clear(); v.assign({ 1, 10, 5, 2, 7 });
  result = std::make_tuple(v.begin() + 1, v.begin() + 2);
//  assert(smallest_subarray_ge(v.begin(), v.end(), 9) == result);
  assert(smallest_subarray_ge2(v.begin(), v.end(), 9) == result);
//  auto r = smallest_subarray_ge(v.begin(), v.end(), 9);
//  std::cout << *std::get<0>(r) << " " << *std::get<1>(r) << std::endl;

  v.clear(); v.assign({ 1, 11, 100, 1, 0, 200, 3, 2, 1, 250 });
  result = std::make_tuple(v.begin() + 2, v.begin() + 6);
//  assert(smallest_subarray_ge(v.begin(), v.end(), 280) == result);
  assert(smallest_subarray_ge2(v.begin(), v.end(), 280) == result);

  v.clear(); v.assign({ 1, 11, 100, 1, 0, 200, 80, 2, 1, 250 });
  result = std::make_tuple(v.begin() + 5, v.begin() + 8);
//  assert(smallest_subarray_ge(v.begin(), v.end(), 280) == result);
  assert(smallest_subarray_ge2(v.begin(), v.end(), 280) == result);

//  using iterator = std::vector<int>::iterator;
//  std::uint64_t t1 = timeit(smallest_subarray_ge<iterator>, v.begin(), v.end(), 280);
//  std::uint64_t t2 = timeit(smallest_subarray_ge2<iterator>, v.begin(), v.end(), 280);
//  std::cout << "smallest_subarray_ge v1 "
//            << (t1 < t2 ? "faster than" : t1 > t2 ? "slower than" : "as fast as")
//            << " v2 -- difference: "
//            << std::abs(static_cast<int64_t>(t1) - static_cast<int64_t>(t2))
//            << std::endl;
}

/* ********************************************************************* */
class ksmallest {

public:

  /*
   * Time complexity (O log n)
   */
  template< class RAIter >
  static RAIter naive(RAIter begin, RAIter end, size_t k) {
    assert(end > begin);
    std::sort(begin, end);
    return begin + (k-1);
  }

  /*
   * Time complexity: average O(n), worst (On^2)
   */
  template< class RAIter >
  static RAIter quickselect(RAIter begin, RAIter end, size_t k) {
    return quickselect_impl(begin, end, k,
      quickselect<RAIter>, quick_partition<RAIter>);
  }

  /*
   * Time complexity: average O(n), worst (On^2)
   *
   * THIS WORKS BETTER THAN BANALANCED QUICKSORT METHOD IN PRACTICAL
   * SITUATIONS BECAUSE OF THE BALANCED METHOD's LARGE AMOUNT OF CONSTANTS.
   */
  template< class RAIter >
  static RAIter random_quickselect(RAIter begin, RAIter end, size_t k) {
    return quickselect_impl(begin, end, k,
      random_quickselect<RAIter>, random_quick_partition<RAIter>);
  }

  template< class RAIter >
  static RAIter maxheap(RAIter begin, RAIter end, size_t k) {

    std::vector<RAIter> heap{ begin, begin + k };
    auto func = [](const RAIter & a, const RAIter & b){ return *a < *b; };
    std::make_heap(heap.begin(), heap.end(), func);
    for (auto it = begin + k; it != end; ++it) {
      if (func(it, heap.front())) {
        heap.push_back(it);
        std::push_heap(heap.begin(), heap.end(), func);
      }
    }
    return heap.front();
  }

private:

  template< class RAIter >
  static inline RAIter quick_partition(RAIter begin, RAIter end) {
    for (auto it = begin; it != end; ++it) {
      if (*it <= *end) {
        std::iter_swap(begin, it);
        ++begin;
      }
    }
    std::iter_swap(begin, end);
    return begin;
  }

  template< class RAIter >
  static inline RAIter random_quick_partition(RAIter begin, RAIter end) {
//    auto pivot = std::rand() % (end - begin + 1);
    std::uniform_int_distribution<> dist{
      0, static_cast<std::uniform_int_distribution<>::result_type>(
        std::abs(end - begin + 1)) };
    auto pivot = dist(random_generator);
    std::iter_swap(begin + pivot, end);
    return quick_partition(begin, end);
  }

  template< class RAIter, class Function, class Partition >
  static RAIter quickselect_impl(
    RAIter & begin, RAIter & end, size_t k, Function func, Partition partition
  ) {

    assert(end > begin);

    if (k > 0 && k <= end - begin + 1) {
      auto it = partition(begin, end-1);
      if (it - begin == k - 1) {
        return it;
      } else if (it - begin > k - 1) {
        return func(begin, it, k);
      }
      return func(++it, end, k - std::abs(begin - it));
    }
    return end;
  }

  static std::mt19937 random_generator;

  using random_result_type = std::mt19937::result_type;
};

std::mt19937 ksmallest::random_generator = std::mt19937(std::random_device()());

void test_ksmallest() {

  using value_type = std::mt19937_64::result_type;
  using iterator = std::vector<value_type>::iterator;

//  std::srand(std::time(0));
  std::random_device rd;
  std::mt19937_64 engine{ rd() };
  std::unordered_set<value_type> numbers;
  std::generate_n(std::inserter(numbers, numbers.begin()), 10000, engine);

  std::vector<value_type> v;
  v.reserve(numbers.size());
  std::move(numbers.begin(),numbers.end(), std::back_inserter(v));
  numbers.clear();

  std::size_t k = v.size()/2;
  auto tmp = v;
  auto it = ksmallest::naive(tmp.begin(), tmp.end(), k);

  tmp = v;
  auto it2 = ksmallest::quickselect(tmp.begin(), tmp.end(), k);
  assert(*it == *it2);

  tmp = v;
  it2 = ksmallest::random_quickselect(tmp.begin(), tmp.end(), k);
  assert(*it == *it2);

  tmp = v;
  it2 = ksmallest::random_quickselect(tmp.begin(), tmp.end(), k);
  assert(*it == *it2);

  std::size_t timing_loops = 100;

  tmp = v;
  std::cout << "naive: " <<
    timeit(
        [&]() { tmp = v; ksmallest::naive(tmp.begin(), tmp.end(), k); },
        timing_loops)
    << std::endl;

  std::cout << "quickselect: " <<
    timeit(
      [&]() { tmp = v; ksmallest::quickselect(tmp.begin(), tmp.end(), k); },
      timing_loops)
    << std::endl;

  std::cout << "random quickselect: " <<
    timeit(
      [&]() { tmp = v; ksmallest::random_quickselect(tmp.begin(), tmp.end(), k); },
      timing_loops)
    << std::endl;

  std::cout << "maxheap: " <<
    timeit(
      [&]() { tmp = v; ksmallest::maxheap(tmp.begin(), tmp.end(), k); },
      timing_loops)
    << std::endl;
}
/* ********************************************************************* */

/* *********************************************************************
 * Given an array and a number 'k', count the distinct pairs whose difference
 * is 'k'.
 *
 * Add is a callable such that
 *  Iter::value_type (const Iter::value_type&, const Difference&)
 * *********************************************************************/
template< class Iter, class Difference, class Add >
std::size_t kdiff_count(Iter begin, Iter end, Difference diff, Add add_op) {

  std::size_t count = 0;

  std::unordered_set<typename Iter::value_type> set;
  for (auto it = begin; it != end; ++it)
    set.insert(*it);

  for (auto it = set.begin(); it != set.end(); ++it)
    if (set.find(add_op(*it, diff)) != set.end())
      ++count;

  return count;
}

void test_kdiff_count() {

  std::vector<int> v{ 1, 2, 3, 5, 10, 8, 6 };
  auto count = kdiff_count(v.begin(), v.end(), 1,
    [](const int & a, const int & diff) -> int { return a + diff; });
  assert(count == 3);
}

/* ********************************************************************* */
template< class Iter, class Subtract>
bool sum_exists(
  Iter begin, Iter end, typename Iter::value_type sum, Subtract subtract_op
) {

  std::unordered_set<typename Iter::value_type> set;

  for (auto it = begin; it != end; ++it) {
    auto diff = subtract_op(sum, *it);
    if (set.find(diff) != set.end())
      return true;
    else
      set.insert(*it);
  }

  return false;
}

void test_sum_exists() {

  std::vector<int> v { 1, 2, 3, 5, 7, 12 };
  assert(sum_exists(v.begin(), v.end(), 12,
    [](const int& sum, const int& a) -> int { return sum - a; }));
}
/* ********************************************************************* */

/*
 * Multiset intersection
 *
 * A = [0,1,1,2,2,5]
 * B = [0,1,2,2,2,6]
 * A ∩ B = [0,1,2,2]
 *
 * A = [0,1,1]
 * B = [0,1,2,3,4,5,6]
 * A ∩ B = [0,1]
 *
 *
 * Time complexity: O(2 * (N1 + N2 - 1) ) where
 * N1 = distance(first1, last2)
 * N2 = distance(first2, last2)
 */

template< class InputIt1, class InputIt2, class OutputIt >
OutputIt set_intersect(
  InputIt1 first1, InputIt1 last1, InputIt2 first2, InputIt2 last2, OutputIt out
) {

  while (first1 < last1 && first2 < last2) {
    if (*first1 == *first2) {
      *out++ = *first1;
      ++first1;
      ++first2;
    } else if (*first1 < *first2) {
      ++first1;
    } else {
      ++first2;
    }
  }
  return out;
}

void test_set_intersection() {

  std::vector<int> v1 { 0, 1, 1, 2, 2, 5 };
  std::vector<int> v2 { 0, 1, 2, 2, 2, 6 };
  std::vector<int> vo;

  set_intersect(
    v1.begin(), v1.end(), v2.begin(), v2.end(), std::back_inserter(vo));

  assert(vo[0] == 0); assert(vo[1] == 1); assert(vo[2] == 2); assert(vo[3] == 2);

  v1.assign({ 0,1,1 });
  v2.assign({ 0,1,2,3,4,5,6 });
  vo.clear();

  set_intersect(
    v1.begin(), v1.end(), v2.begin(), v2.end(), std::back_inserter(vo));

  assert(vo[0] == 0); assert(vo[1] == 1);
}

template< class InputIt1, class InputIt2, class Function>
void set_intersect_parallel_impl(
  InputIt1 & first1, InputIt1 & last1,
  InputIt2 & first2, InputIt2 & last2,
  Function func
) {

  if (last1 - first1 < 500 && last2 - first2 < 500) {
    set_intersect(first1, last1, first2, last2, std::back_inserter(*func()));
    return;
  }

  auto mid1 = std::next(first1, (last1 - first1) / 2);
  auto mid2 = std::next(first2, (last2 - first2) / 2);

  auto fut = std::async(
    std::launch::async | std::launch::deferred,
    set_intersect_parallel_impl<InputIt1, InputIt2, Function>,
    std::ref(mid1), std::ref(last1), std::ref(mid2), std::ref(last2), func);

  set_intersect_parallel_impl(first1, mid1, first2, mid2, func);

  return;
}

template< class InputIt1, class InputIt2, class OutputIt >
OutputIt set_intersect_parallel(
  InputIt1 first1, InputIt1 last1, InputIt2 first2, InputIt2 last2, OutputIt out
) {

  using buffer_type = std::vector<typename InputIt1::value_type>;

  if (last1 - first1 < 500 && last2 - first2 < 500)
    return set_intersect(first1, last1, first2, last2, out);

  std::recursive_mutex mutex;
  std::forward_list<buffer_type> parts;
  set_intersect_parallel_impl(
    first1, last1, first2, last2,
    [&parts, &mutex]() {
      std::lock_guard<std::recursive_mutex> guard(mutex);
      parts.emplace_front();
      return parts.begin();
    }
  );

  for (auto & v : parts)
    std::move(v.begin(), v.end(), out);

  return out;
}

void test_set_intersect_parallel() {

  std::vector<int> v1;
  std::vector<int> v2;
  std::vector<int> vo;

  std::random_device rd;
  std::mt19937_64 engine{ rd() };
  std::generate_n(std::inserter(v1, v1.begin()), 1000000, engine);
  std::generate_n(std::inserter(v2, v2.begin()), 1000000, engine);

  set_intersect_parallel(
    v1.begin(), v1.end(), v2.begin(), v2.end(), std::back_inserter(vo));

  print(vo.begin(), vo.end()); std::cout << std::endl;
}

template< class Iter >
int max_slice(Iter begin, Iter end) {
  
  int max_ending{ 0 };
  int max_slice{ 0 };
  for (; begin != end; ++begin) {
    max_ending = std::max(0, max_ending + *begin);
    max_slice = std::max(max_slice, max_ending);
  }
  return max_slice;
}

template< class Iter >
int max_difference(Iter begin, Iter end) {
  
  int max_ending{ 0 };
  int max_slice{ 0 };
  for (++begin; begin != end; ++begin) {
    max_ending = std::max(0, max_ending + *begin - *(begin - 1));
    max_slice = std::max(max_slice, max_ending);
  }
  return max_slice;
}

template< class Iter >
int max_double_slice(Iter begin, Iter end) {
  
  std::vector<int> max_ending_at{ begin - end + 1, 0 };
  size_t i = 0;
  for(auto it = begin; it != end; ++it) {
    max_ending_at[i] = std::max(0, *it + max_ending_at[i-1]);
    ++i;
  }
  
  std::vector<int> max_starting_at{ begin - end + 1, 0 };
  i = 0;
  for(auto it = end - 2; it >= begin; --it) {
    max_starting_at[i] = std::max(0, *it + max_starting_at[i+1]);
    ++i;
  }
  
  i = 0;
  int max_double_slice = 0;
  for(; begin != end - 2; ++begin) {
    max_double_slice = std::max(
      max_double_slice, max_ending_at[i] + max_starting_at[i+2]);
    ++i;
  }
  
  return max_double_slice;
}


/*
 * Given an array, reverse every sub-array formed by consecutive k elements.
 */
template< class InputIterator >
void reverse_array_in_k_groups(
  InputIterator begin, InputIterator end, size_t k
) {
  for(auto it = begin; std::distance(begin, end) > std::distance(begin, it); it += k) {
    auto left = it;
    auto right =
      std::distance(begin, it + k) < std::distance(begin, end) ?
        it + k - 1 : end - 1;
    while (std::distance(begin, left) < std::distance(begin, right)) {
      std::swap(*left, *right);
      ++left;
      --right;
    }
  }
}

template< class T >
void reverse_array_in_k_groups(T arr[], size_t n, size_t k) {
  for (size_t i = 0; i < n; i += k) {
    size_t left = i;
    size_t right = std::min(i + k - 1, n - 1);
    while (left < right) {
      std::swap(arr[left], arr[right]);
      ++left;
      --right;
    }
  }
}

void test_reverse_array_in_k_groups() {
  
  size_t k = 3;
  
  {
    std::vector<int> v = { 1, 2, 3, 4, 5, 6, 7, 8 };
    reverse_array_in_k_groups(v.begin(), v.end(), k);
  
    std::vector<int> vref = { 3, 2, 1, 6, 5, 4, 8, 7 };
    assert(std::equal(v.begin(), v.end(), vref.begin()));
  }
  
  {
    int arr[] = { 1, 2, 3, 4, 5, 6, 7, 8 };
    size_t n = sizeof(arr) / sizeof(arr[0]);
    reverse_array_in_k_groups(arr, n, k);
  
    int aref[] = { 3, 2, 1, 6, 5, 4, 8, 7 };
    assert(std::equal(arr, arr + n, aref));
  }
  
  {
    std::vector<int> v = { 1, 2, 3, 4, 5, 6, 7, 8};
    reverse_array_in_k_groups(v.begin(), v.end(), 5);
  
    std::vector<int> vref = { 5, 4, 3, 2, 1, 8, 7, 6 };
    assert(std::equal(v.begin(), v.end(), vref.begin()));
  }
  
  {
    std::vector<int> v = { 1, 2, 3, 4, 5, 6 };
    reverse_array_in_k_groups(v.begin(), v.end(), 1);
  
    std::vector<int> vref = { 1, 2, 3, 4, 5, 6 };
    assert(std::equal(v.begin(), v.end(), vref.begin()));
  }
  
  {
    std::vector<int> v = { 1, 2, 3, 4, 5, 6, 7, 8 };
    reverse_array_in_k_groups(v.begin(), v.end(), 10);

    std::vector<int> vref = { 8, 7, 6, 5, 4, 3, 2, 1 };
    assert(std::equal(v.begin(), v.end(), vref.begin()));
  }
}


int main() {
  test_max_product_subarray();
  test_reverse_array_in_k_groups();
  test_find_contiguous();
  test_smallest_excluded_int();
  test_smallest_subarray_ge();
  test_ksmallest();
  test_kdiff_count();
  test_sum_exists();
  test_set_intersection();
  test_set_intersect_parallel();
  return 0;
}
