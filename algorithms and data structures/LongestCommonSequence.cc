// Longest Common Subsequence
// Author: Kareem Alkaseer

#include <string>
#include <iostream>
#include <algorithm>
#include <exception>

using namespace std;

// Time complixty: O(n ^ 2) in the worst case which happens when LCS is 0.
size_t lcs_naive(const char * x, size_t n, const char * y, size_t m) {
  if (n == 0 || m == 0)
    return 0;
  if (x[n-1] == y[m-1])
    return 1 + lcs_naive(x, n-1, y, m-1);
  else
    return max(lcs_naive(x, n, y, m-1), lcs_naive(x, n-1, y, m));
}


template< class F >
void test(F f) {

  string x;
  string y;
  string ns;
  size_t n;
  cout << "Number of tests: ";
  getline(cin, ns);
  try {
    n = stoull(ns);
  } catch (exception & e) {
    cout << "Error: invalid number of tests" << endl;
    return;
  }
  for (size_t i = 0; i < n; ++i) {
    cout << "S1: ";
    getline(cin, x);
    cout << "S2: ";
    getline(cin, y);
    cout << "=> LCS: " << f(x.data(), x.size(), y.data(), y.size()) << endl;
  }
}

// Time complexity: O(mn) by avoiding computing the same subproblem twice.
size_t lcs(const char * x, size_t n, const char * y, size_t m) {
  
  size_t table[n + 1][m + 1];
  size_t i, j;
  
  for (i = 0; i <= n; ++i) {
    for (j = 0; j <= m; ++j) {
      
      if (i == 0 || j == 0)
        table[i][j] = 0;
      
      else if (x[i - 1] == y[j - 1])
        table[i][j] = table[i - 1][j - 1] + 1;
      
      else
        table[i][j] = max(table[i - 1][j], table[i][j - 1]);
      
    }
  }
  
  return table[n][m];
}



int main() {
  test(lcs);
  return 0;
}