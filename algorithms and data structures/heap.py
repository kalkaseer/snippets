from abc import abstractmethod

class heap(object):
    
    def __init__(self, alist):
        self.data = alist
        self.size = len(alist)
        i = (self.size - 1) // 2
        while i >= 0:
            self._heapify(i)
            i -= 1
    
    @abstractmethod
    def _heapify(self, index):
        pass
    
    def pop(self):
        
        if self.size < 1: return None
        root = self.data[0]
        if self.size > 1:
            self.data[0] = self.data[self.size - 1]
            self._heapify(0)
        self.size -= 1
        return root
    
    def peek(self):
        return self.data[0] if self.data else None
    
    def parent(self, index):
        return (index - 1) /2
    
    def left(self, index):
        return (2 * index) + 1
        
    def right(self, index):
        return (2 * index) + 2
    
    def setroot(self, value):
        self.data[0] = value
        self._heapify(0)

class minheap(heap):
    
    def __init__(self, alist):
        super(self.__class__, self).__init__(alist)
    
    def _heapify(self, index, ):
        
        l = self.left(index)
        r = self.right(index)
        smallest = index
        if (l < self.size and self.data[l] < self.data[index]):
            smallest = l
        if (r < self.size and self.data[r] < self.data[smallest]):
            smallest = r
        if smallest != index:
            self.data[index], self.data[smallest] = \
                self.data[smallest], self.data[index]
            self._heapify(smallest)


class maxheap(heap):
    
    def __init__(self, alist):
        super(self.__class__, self).__init__(alist)
    
    def _heapify(self, index):

        l = self.left(index)
        r = self.right(index)
        largest = index
        if l < self.size and self.data[l] > self.data[index]:
            largest = l
        if r < self.size and self.data[r] > self.data[largest]:
            largest = r
        if largest != index:
            self.data[index], self.data[largest] = \
                self.data[largest], self.data[index]
            self._heapify(largest)
    
    