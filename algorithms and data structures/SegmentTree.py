# Segment Tree
#
# http://www.cse.iitk.ac.in/users/aca/lop12/slides/06.pdf

from math import ceil, log, pow
from sys import maxint

def middle(start, end):
  return start + (end - start) / 2

# ss: segment start index in tree
# se: segment end index in tree
# si: index of current node in tree
# qs: start index of the query interval
# qe: end index of the query interval
def _segment_sum(tree, ss, se, qs, qe, si):
  
  # node's segment is part of the query range, return sum of segment
  if qs <= ss and qe >= se:
    return tree[si]
  
  # node's segment is out of the query range
  if se < qs or ss > qe:
    return 0
  
  mid = middle(ss, se)
  return _segment_sum(tree, ss, mid, qs, qe, 2*si+1) + \
         _segment_sum(tree, mid+1, se, qs, qe, 2*si+2)


# Return sum of elements from query start (qs) to query end (qe)
# alen: original array length
def segment_sum(tree, alen, qs, qe):
  
  if qs < 0 or qe > alen -1 or qs > qe:
    return -1
  
  return _segment_sum(tree, 0, alen-1, qs, qe, 0)
  

# si: index of current node in segment tree
# ss: segment start index in the array
# se: segment end indes in the array
def _construct(alist, ss, se, tree, si):
  
  if ss == se:
    tree[si] = alist[ss]
    return alist[ss]
  
  mid = middle(ss, se)
  tree[si] = _construct(alist, ss, mid, tree, si*2+1) + \
             _construct(alist, mid+1, se, tree, si*2+2)

  return tree[si]
  
  
def construct(alist):
  
  n = len(alist)
  height = int(ceil(log(n, 2)))
  max_size = 2 * int(pow(2, height)) - 1
  tree = [0] * max_size
  _construct(alist, 0, n-1, tree, 0)
  return tree

# idx: index in the array whose value to be updated
# si: index of the current node in the tree
# diff: value to add to all nodes which have idx in range
def _update(tree, ss, se, idx, diff, si):
  
  # idx lies outside segment range
  if idx < ss or idx > se:
    return
  
  tree[si] += diff
  
  if ss != se:
    mid = middle(ss, se)
    _update(tree, ss, mid, idx, diff, 2*si+1)
    _update(tree, mid+1, se, idx, diff, 2*si+2)
  

# idx: index in the array whose value to be updated
def update(alist, tree, idx, val):
  
  if idx < 0 or idx > len(alist)-1:
    return
  
  diff = val - alist[idx]

  alist[idx] = val
  
  _update(tree, 0, len(alist)-1, idx, diff, 0)
  

# ss: segment start index in the tree
# se: segment end index in the tree
# qs: query start index in the array
# qe: query end index in the array
# si: index of the current node in the tree
def _rmq(tree, ss, se, qs, qe, si):
  
  # if segment of current node is part of the query range
  if qs <= ss and qe >= se:
    return tree[si]
  
  # if segment lies outside of the query range
  if se < qs or ss > qe:
    return maxint
  
  mid = middle(ss, se)
  return min(
    _rmq(tree, ss, mid, qs, qe, 2*si+1),
    _rmq(tree, mid+1, se, qs, qe, 2*si+2))

# range minimum query: return minimum element in range[qs, qe]
# n: length of original array
def rmq(tree, n, qs, qe):
  if qs < 0 or qe > n-1 or qs > qe:
    return -1
  return _rmq(tree, 0, n-1, qs, qe, 0)


def test():
  alist = [1, 3, 5, 7, 9, 11]
  tree = construct(alist)
  assert segment_sum(tree, len(alist), 1, 3) == 15
  update(alist, tree, 1, 10) # set alist[1] = 10
  assert segment_sum(tree, len(alist), 1, 3) == 22
  
  alist = [1, 3, 2, 7, 9, 11 ]
  tree = construct(alist)
  assert rmq(tree, len(alist), 1, 5) == 2
  