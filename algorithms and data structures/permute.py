# String permutations
#
#  Execution examples
#
# permute(LSE) : ch = L, [SE, ES] -> LSE, SLE, SEL, LES, ELS, ESL
# permute(SE) : ch = S, [E] -> SE, ES
# permute(E) -> [E]
#
# permute(ABCD) : ch = A, [BCD, CBD, DCB, BDC, DBC, DCB] -> ...
# permute(BCD)  : ch = B, [CD, DC] -> BCD, CBD, DCB, BDC, DBC, DCB
# permute(CD)   : ch = C, [D] -> CD, DC
# permute(D)    : [D]
#
# Complexity: O(n!) optimal

def permute(s):

    if len(s) == 0:
        return []
    elif len(s) == 1:
        return [s]

    # permutations of length n - 1
    perms = permute(s[1:])
    ch = s[0]
    result = []

    for p in perms:
        # insert ch into every possible location
        for i in xrange(len(p) + 1):
            result.append(p[:i] + ch + p[i:])

    return result


def test_permute():

    import itertools

    r = permute("LSE")
    s1 = set(r)
    s2 = set(["LSE", "SLE", "SEL", "LES", "ELS", "ESL"])
    assert len(s1.difference(s2)) == 0

    r = permute('')
    assert r == []
    r = permute('p')
    assert r == ['p']

    r = permute("ABCD")
    s1 = set(r)
    s2 = set([x for x in itertools.permutations('ABCD')])
    s2 = set([''.join(x) for x in s2])
    assert len(s1.difference(s2)) == 0
    
    
# Time complexity n * (n-1)! = n!
def permute(s):
    
    if not s: return [s]
    
    result = []
    
    for i in xrange(len(s)):
        current = s[:i] + s[i+1:]
        for perm in permute(current):
            result.append(s[i:i+1] + perm)
        
    return result

from ctypes import create_string_buffer as csb

def swap(s, n, m):
    tmp  = s[n]
    s[n] = s[m]
    s[m] = tmp
    
def permute2(buf, first = 0, last = len(s)):
    result = []
    _permute2(csb(s), result)
    return result

def _permute2(buf, result, first = 0, last = len(s)):
    
    if first == last: result.append(buf[0:last])
    
    for i in xrange(first, last):
        swap(buf, first, i)
        _permute2(buf, result, first + 1, last)
        swap(buf, first, i)