/*
 * multiply_test.cc
 *
 *  Created on: Aug 9, 2016
 *      Author: Kareem Alkaseer
 */


#include "multiply.h"
#include <tuple>
#include <gtest/gtest.h>
#include <iostream>
#include <memory>


std::tuple<matrix::dense_matrix<int> *, matrix::dense_matrix<int> *> multiply() {

  const matrix::dense_matrix<int> x {
    { 1, 2, 3 },
    { 4, 5, 6 }
  };

  const matrix::dense_matrix<int> y {
    { 7,  8 },
    { 9,  10 },
    { 11, 12 }
  };

  auto r = new matrix::dense_matrix<int>{
    { 58,  64 },
    { 139, 154 },
  };

  matrix::dense_matrix<int> * p = matrix::multiply(x, y);

  return std::make_tuple(r, p);
}

TEST (MatrixMultipy, SimpleMultiply) {

  matrix::dense_matrix<int> * r;
  matrix::dense_matrix<int> * p;
  std::tie(r, p) = multiply();

  std::shared_ptr<matrix::dense_matrix<int>> rp{ r };
  std::shared_ptr<matrix::dense_matrix<int>> pp{ p };

  EXPECT_TRUE(*r == *p);
}

TEST (MatrixMultipy, ChainOrder) {

  matrix::dense_matrix<size_t> * m;
  matrix::dense_matrix<size_t> * s;

  matrix::dimensions<> p { 30, 35, 15, 5, 10, 20, 25 };
  std::tie(m, s) = matrix::chain_order(p);

  EXPECT_EQ(15125, matrix::minimum_chain_cost(*m, p));

  delete m;
  delete s;
}

TEST (MatrixMultiply, ChainMultiply) {

  matrix::dense_matrix<size_t> * m;
  matrix::dense_matrix<size_t> * s;

  matrix::dense_matrix<size_t> m1{ 30, 35 };
  matrix::dense_matrix<size_t> m2{ 35, 15 };
  matrix::dense_matrix<size_t> m3{ 15,  5 };

  matrix::dimensions<> p {
    m1.rows_size(), m1.columns_size(),
    m2.columns_size(), m3.columns_size() };
  std::tie(m, s) = matrix::chain_order(p);

  matrix::fill_increasing(m1, 1);
  matrix::fill_increasing(m2, 1);
  matrix::fill_increasing(m3, 1);

  std::vector<matrix::dense_matrix<size_t> *> v { &m1, &m2, &m3 };

  matrix::dense_matrix<size_t> ref {
    { 119271600,   122560200,  125848800,  129137400,  132426000 },
    { 294961100,   303082325,  311203550,  319324775,  327446000 },
    { 470650600,   483604450,  496558300,  509512150,  522466000 },
    { 646340100,   664126575,  681913050,  699699525,  717486000 },
    { 822029600,   844648700,  867267800,  889886900,  912506000 },
    { 997719100,  1025170825, 1052622550, 1080074275, 1107526000 },
    { 1173408600, 1205692950, 1237977300, 1270261650, 1302546000 },
    { 1349098100, 1386215075, 1423332050, 1460449025, 1497566000 },
    { 1524787600, 1566737200, 1608686800, 1650636400, 1692586000 },
    { 1700477100, 1747259325, 1794041550, 1840823775, 1887606000 },
    { 1876166600, 1927781450, 1979396300, 2031011150, 2082626000 },
    { 2051856100, 2108303575, 2164751050, 2221198525, 2277646000 },
    { 2227545600, 2288825700, 2350105800, 2411385900, 2472666000 },
    { 2403235100, 2469347825, 2535460550, 2601573275, 2667686000 },
    { 2578924600, 2649869950, 2720815300, 2791760650, 2862706000 },
    { 2754614100, 2830392075, 2906170050, 2981948025, 3057726000 },
    { 2930303600, 3010914200, 3091524800, 3172135400, 3252746000 },
    { 3105993100, 3191436325, 3276879550, 3362322775, 3447766000 },
    { 3281682600, 3371958450, 3462234300, 3552510150, 3642786000 },
    { 3457372100, 3552480575, 3647589050, 3742697525, 3837806000 },
    { 3633061600, 3733002700, 3832943800, 3932884900, 4032826000 },
    { 3808751100, 3913524825, 4018298550, 4123072275, 4227846000 },
    { 3984440600, 4094046950, 4203653300, 4313259650, 4422866000 },
    { 4160130100, 4274569075, 4389008050, 4503447025, 4617886000 },
    { 4335819600, 4455091200, 4574362800, 4693634400, 4812906000 },
    { 4511509100, 4635613325, 4759717550, 4883821775, 5007926000 },
    { 4687198600, 4816135450, 4945072300, 5074009150, 5202946000 },
    { 4862888100, 4996657575, 5130427050, 5264196525, 5397966000 },
    { 5038577600, 5177179700, 5315781800, 5454383900, 5592986000 },
    { 5214267100, 5357701825, 5501136550, 5644571275, 5788006000 }
  };

  matrix::dense_matrix<size_t> * res = matrix::multiply_chain(v,*s);

  EXPECT_TRUE(ref == *res);
}

int main(int argc, char ** argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}

