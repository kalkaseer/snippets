/*
 * multiply.h
 *
 *  Created on: Aug 9, 2016
 *      Author: Kareem Alkaseer
 */

#ifndef MATRIX_MULTIPLY_H_
#define MATRIX_MULTIPLY_H_

#include <tuple>
#include <utility>
#include <initializer_list>
#include <limits>
#include <vector>
#include <cassert>
#include <string>

#include <iostream> // TODO remove;
#include <iomanip>

namespace matrix {

template< class T, class S = size_t >
class dense_matrix {

public:

  using size_type = S;
  using value_type = T;
  using self_type = dense_matrix<value_type>;

private:

  size_type m_width;
  size_type m_height;
  std::vector<value_type> m_data;
  std::string m_name;

public:

  dense_matrix()
    : m_width{ 0 }, m_height{ 0 }, m_data{ }
  { }

  explicit dense_matrix(
    const std::initializer_list<std::initializer_list<value_type>> & il
  ) : m_width{ il.begin()->size() }, m_height{ il.size() }
  {
    auto inserter = std::back_inserter(m_data);
    for (const auto & row : il) {
      std::copy(row.begin(), row.end(), inserter);
    }
  }

  explicit dense_matrix(
    std::initializer_list<std::initializer_list<value_type>> && il
  ) : m_width{ il.begin()->size() }, m_height{ il.size() }
  {
    auto inserter = std::back_inserter(m_data);
    for (const auto & row : il) {
      std::move(row.begin(), row.end(), inserter);
    }
  }

  explicit dense_matrix(
    const size_type rows, const size_type columns, const T & initial = T()
  ) : m_height{ rows }, m_width{ columns }, m_data( columns * rows, initial )
  { }

  dense_matrix(const self_type &) = delete;

  dense_matrix(self_type && rhs)
    : m_width{ std::move(rhs.m_width) },
      m_height{ std::move(rhs.m_height) },
      m_data{ std::move(rhs.m_data) },
      m_name{ std::move(rhs.m_name) }
  { }

  value_type & operator()(const size_type i, const size_type j) {
    return m_data[m_width * i + j];
  }

  const value_type & operator()(const size_type i, const size_type j) const {
    return m_data[m_width * i + j];
  }

  value_type & at(const size_type i, const size_type j) {
    return m_data[m_width * i + j];
  }

  const value_type & at(const size_type i, const size_type j) const {
    return m_data[m_width * i + j];
  }

  inline size_type columns_size() const { return m_width; }

  inline size_type rows_size() const { return m_height; }

  bool operator==(const self_type & rhs) {
    if (rows_size() != rhs.rows_size() || columns_size() != rhs.columns_size())
      return false;
    return std::equal(
      m_data.cbegin(), m_data.cend(), rhs.m_data.cbegin(), rhs.m_data.cend());
  }

  void name(std::string && s) { m_name = s; }
  std::string name() { return m_name; }
};


template< class M, class Allocator = std::allocator<M> >
M * multiply(const M & a, const M & b, Allocator allocator = Allocator()) {

  if (a.columns_size() != b.rows_size())
    assert("a width != b height");

  typename Allocator::pointer p = allocator.allocate(1);
  allocator.construct(p, a.rows_size(), b.columns_size());
  M * m{ reinterpret_cast<M *>(p) };

  for (typename M::size_type i = 0; i < a.rows_size(); ++i) {
    for (typename M::size_type j = 0; j < b.columns_size(); ++j) {
//      m->at(i, j) = 0;
      for (typename M::size_type k = 0; k < a.columns_size(); ++k)
        m->at(i, j) = m->at(i, j) + a(i, k) * b(k, j);
    }
  }

  return m;
}

template< class S = size_t >
struct dimensions {

  dimensions() { }

  explicit dimensions(const std::initializer_list<S> & l)
    : dims{ l }
  { }

  explicit dimensions(std::initializer_list<S> && l)
    : dims{ l }
  { }

  template< class Iter >
  explicit dimensions(Iter first, Iter last) {
    std::copy(first, last, std::back_inserter(dims));
  }

  size_t size() const { return dims.size(); }

  S & operator[](size_t i) { return dims[i]; }

  const S & operator[](size_t i) const { return dims[i]; }

  using size_type = S;
  std::vector<S> dims;
};

template< class M >
struct matrix_printer {
  static std::ostream & print(const M & m) {
    for (matrix::dense_matrix<int>::size_type i = 0; i < m.rows_size(); ++i) {
      for (matrix::dense_matrix<int>::size_type j = 0; j < m.columns_size(); ++j)
        std::cout << std::setw(10) << m(i, j) << " ";
      std::cout << '\n';
    }
    return std::cout;
  }
};

template< class M >
struct matrix_pointer_printer {
  static inline std::ostream & print(const M m) {
    return matrix_printer< std::remove_pointer_t<M> >::print(*m);
  }
};

template< class M >
std::ostream & print(const M & m) {
  return std::conditional_t<
    std::is_pointer<M>::value,
    matrix_pointer_printer<M>,
    matrix_printer<M>
  >::print(m);
}


// p = [p0, p1, ..., pn]
template< class Dims, class Allocator = std::allocator<dense_matrix<size_t>> >
std::tuple< dense_matrix<size_t> *, dense_matrix<size_t> * >
chain_order(const Dims & p, Allocator alloc = Allocator()) {

  using S = typename Dims::size_type;
  S n = p.size() - 1;
  S l, i, j, k, q;

  typename Allocator::pointer pm = alloc.allocate(1);
  alloc.construct(pm, n, n);

  typename Allocator::pointer ps = alloc.allocate(1);
  alloc.construct(ps, n, n);

  dense_matrix<size_t> * m { reinterpret_cast<dense_matrix<size_t> *>(pm) };
  dense_matrix<size_t> * s { reinterpret_cast<dense_matrix<size_t> *>(ps) };


  for (l = 1; l < n; ++l) {

    for (i = 0; i < n - l + 1; ++i) {

      j = i + l;
      m->at(i, j) = std::numeric_limits<size_t>::max();

      for (k = i; k < j; ++k) {

        q = m->at(i,k) + m->at(k+1, j) + (p[i-1] * p[k] * p[j]);
        if (q < m->at(i,j)) {
          m->at(i, j) = q;
          s->at(i, j) = k;
        }
      }
    }
  }

  return std::make_tuple(m, s);
}

template< class M, class Dims >
inline size_t minimum_chain_cost(const M & m, const Dims & p) {
  return minimum_chain_cost(m, p, 1, p.size()-1);
}

template< class M, class Dims >
inline typename M::size_type minimum_chain_cost(
  const M & m, const Dims & p,
  const typename M::size_type i, const typename M::size_type j
) {
  typename M::size_type min = m(i, i) + m(i+1, j) + (p[i-1] * p[i] * p[j]);
  for(size_t k = i + 1; k < j; ++k)
    min = std::min(min, m(i,k) + m(k+1, j) + (p[i-1] * p[k] * p[j]));
  return min;
}

template< class MContainer, class Table >
struct chain_multiplier {

  using P = typename MContainer::value_type *;

  ~chain_multiplier() {
    m_data.pop_back();
    for (auto i : m_data) delete i;
  }

  P apply(
    MContainer & m, const Table & s,
    typename Table::size_type i, typename Table::size_type j
  ) {

    if (j > i) {
      P x = apply(m, s, i, s(i, j));
      P y = apply(m, s, s(i, j) + 1, j);
      P p = multiply(*x, *y);

      for (auto i : m_data) delete i;
      m_data.erase(m_data.begin(), m_data.end());
      m_data.push_back(p);
      return p;
    }

    return &m[i];
  }
private:
  std::vector<P> m_data;
};

template< class MContainer, class Table >
struct chain_pointer_multiplier {

  using P = typename MContainer::value_type;

  ~chain_pointer_multiplier() {
    m_data.pop_back();
    for (auto i : m_data) delete i;
  }

  P apply(
    MContainer & m, const Table & s,
    typename Table::size_type i, typename Table::size_type j
  ) {
    if (j > i) {
      typename MContainer::value_type x = apply(m, s, i, s(i, j));
      typename MContainer::value_type y = apply(m, s, s(i, j) + 1, j);

      P p = multiply(*x, *y);

      for (auto i : m_data) delete i;
      m_data.erase(m_data.begin(), m_data.end());
      m_data.push_back(p);
      return p;
    }
    return m[i];
  }
private:
  std::vector<P> m_data;
};

template< class MContainer, class Table >
  std::conditional_t<
    std::is_pointer< typename MContainer::value_type >::value,
    typename MContainer::value_type,
    typename MContainer::value_type *>
multiply_chain(
  MContainer & m, const Table & s,
  typename Table::size_type i, typename Table::size_type j
) {
  std::conditional_t<
    std::is_pointer< typename MContainer::value_type >::value,
    chain_pointer_multiplier<MContainer, Table>,
    chain_multiplier<MContainer, Table>
  >multiplier;
  return multiplier.apply(m, s, i, j);
}

template< class MContainer, class Table >
typename MContainer::value_type multiply_chain(
  const MContainer & m, const Table & s
) {
  return multiply_chain(m, s, 0, m.size()-1);
}

template< class M, class F >
void fill(const M & m, F f) {
  for (matrix::dense_matrix<int>::size_type i = 0; i < m.rows_size(); ++i)
    for (matrix::dense_matrix<int>::size_type j = 0; j < m.columns_size(); ++j)
      f(m(i, j), i, j);
}

template< class M >
void fill_increasing(
  M & m, typename M::value_type initial = typename M::value_type()
) {
  for (matrix::dense_matrix<int>::size_type i = 0; i < m.rows_size(); ++i)
    for (matrix::dense_matrix<int>::size_type j = 0; j < m.columns_size(); ++j) {
      m(i, j) = initial;
      ++initial;
    }
}

} // namespace matrix


#endif /* MATRIX_MULTIPLY_H_ */
