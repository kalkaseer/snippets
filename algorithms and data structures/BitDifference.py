# 
# Sum of bit differences among all pairs
# 
# Given an integer array of n integers, find sum of bit differences in all pairs that can be formed from array elements. Bit difference of a pair (x, y) is count of different bits at same positions in binary representations of x and y.
# For example, bit difference for 2 and 7 is 2. Binary representation of 2 is 010 and 7 is 111 ( first and last bits differ in two numbers).
# 
# Examples:
# 
# Input: arr[] = {1, 2}
# Output: 4
# All pairs in array are (1, 1), (1, 2)
#                        (2, 1), (2, 2)
# Sum of bit differences = 0 + 2 +
#                          2 + 0
#                       = 4
# 
# Input:  arr[] = {1, 3, 5}
# Output: 8
# All pairs in array are (1, 1), (1, 3), (1, 5)
#                      (3, 1), (3, 3) (3, 5),
#                      (5, 1), (5, 3), (5, 5)
# Sum of bit differences =  0 + 1 + 1 +
#                           1 + 0 + 2 +
#                           1 + 2 + 0 
#                        = 8


def bit_set(x, n):
  return x & (1 << n)


def bit_diff(a, b, bn=32):
  count = 0
  for i in xrange(bn):
    if (bit_set(a, i) and not bit_set(b, i)) or \
       ((not bit_set(a,i)) and bit_set(b,i)):
      count += 1
  return count


# Solution in O(n)
# Assuming a fixed number of bits per integer, loop over all array elements
# and check with bit i is set. There will be N - (number of elements for
# which bit i is not set). Hence every difference add:
# (Number of bit differences) * (Number of elements for which bit i is not set)
# * 2 as (1, 2) and (2, 1) are different pairs.

def solve(a, nb=32):
  res = 0
  for i in xrange(nb):
    count = 0
    for x in a:
      if x & (1 << i):
        count +=1
    
    res += (count * (len(a) - count) * 2)
  
  return res


def test():
  assert solve([1, 3, 5]) == 8
  assert solve([2, 7]) == 4