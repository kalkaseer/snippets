#include <iostream>
#include <string>
#include <vector>
#include <utility>
#include <algorithm>

template< class T >
void print(const T & a) {
	
	using namespace std;
	for (auto & e : a)
		cout << e << " ";
	cout << endl;
}

template< class T, class A, class I >
A & permute(T & s, A & v, I first, I last, int step = 0) {
	
	using namespace std;
	
	if (first == last - 1) {
		v.push_back(s);
		return v;
	}
	
	for (I it = first; it < last; ++it) {
		
		swap(s[first], s[it]);
		permute(s, v, first + 1, last, ++step);
		swap(s[first], s[it]);
	}
	
	return v;
}

template< class T, class Iter >
Iter ceil(const T & s, Iter ch, Iter first, Iter last) {
	
	Iter it = first;
	
	for (; it < last; ++it) {
		if (*it > *ch && *it < *first)
			first = it;
	}
	
	return it;
}

// Time Complexity: O(n2 * n!)
// Auxiliary Space: O(1)

template< class T, class A, class Iter, class I >
A & permute_unique(
	T & s, A & v, Iter begin, Iter end, I first, I last
) {
	
	using namespace std;
	
	sort(begin + first, begin + last);
	
	while (true) {
		
		v.push_back(s);
		
		I i = last - 2;
		
		// Rightmost character smaller than its next character.
		for (; i >= first; --i)
			if (*(begin+i) < *(begin+i+1))
				break;
	
		// If there is no such character then s is sorted in decreasing
		// order and we are finished
		if (i == first - 1)
			break;
		else {
			Iter it = begin + i;
			// Find the ceil of the character we have just found.
			Iter cit = ceil(s, it, it + 1, begin + last);
			
			swap(s[i], s[cit - begin]);
			cout << s << endl;
			cout << i << " " << cit - begin << endl;
			sort(it + 1, begin + last);
		}
	}
	
	return v;
}

int main() {
	
	using namespace std;
	
	vector<string> v;
	string s;
	cout << "String: ";
	cin >> s;
	
	v.clear();
	
	permute_unique(s, v, s.begin(), s.end(), 0UL, s.size());
	
	print(v);
	
	return 0;
}