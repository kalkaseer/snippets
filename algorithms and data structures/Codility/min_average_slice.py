# coding: utf-8

'''
A non-empty zero-indexed array A consisting of N integers is given.
A pair of integers (P, Q), such that 0 ≤ P < Q < N, is called a slice of
array A (notice that the slice contains at least two elements). The average
of a slice (P, Q) is the sum of A[P] + A[P + 1] + ... + A[Q] divided by the
length of the slice. To be precise, the average equals
(A[P] + A[P + 1] + ... + A[Q]) / (Q − P + 1).

For example, array A such that:
    A[0] = 4
    A[1] = 2
    A[2] = 2
    A[3] = 5
    A[4] = 1
    A[5] = 5
    A[6] = 8

contains the following example slices:

        slice (1, 2), whose average is (2 + 2) / 2 = 2;
        slice (3, 4), whose average is (5 + 1) / 2 = 3;
        slice (1, 4), whose average is (2 + 2 + 5 + 1) / 4 = 2.5.

The goal is to find the starting position of a slice whose average is minimal.

Write a function:

    int solution(int A[], int N);

that, given a non-empty zero-indexed array A consisting of N integers, returns
the starting position of the slice with the minimal average. If there is more
than one slice with a minimal average, you should return the smallest starting
position of such a slice.

For example, given array A such that:
    A[0] = 4
    A[1] = 2
    A[2] = 2
    A[3] = 5
    A[4] = 1
    A[5] = 5
    A[6] = 8

the function should return 1, as explained above.

Assume that:

        N is an integer within the range [2..100,000];
        each element of array A is an integer within the range [−10,000..10,000].

Complexity:

        expected worst-case time complexity is O(N);
        expected worst-case space complexity is O(N), beyond input storage (not counting the storage required for input arguments).

Elements of input arrays can be modified.
'''

'''
Explanation

Despite all the effort, I failed to solve this problem. Only after I saw the article, I could finally realize that this was not a coding problem, but a mathematical one. I was deeply sad about this, but did not want to stop the challenge. So below is my own interpretation of the problem (or excuse for the failure I have made.)

So basically the problem is to prove next statement.

Let len(s) be the length of a slice s, sum(s) the sum of the all elements of the slice s, and ave(s) the average of the slice s. Then for an arbitrary array, every slice s having len(s) > 3 contains a sub-slice s’ such that ave(s) >= ave(s’).

Proof.

Suppose that s is a slice having len(s) > 3 and does NOT contain a sub-slice s’ such that ave(s) >= ave(s’). Since len(s) > 3, s can be divided into sub-slices t and u.

Then,

ave(t) = sum(t) / len(t), and ave(u) = sum(u) / len(u).

ave(s) = sum(s) / len(s)
= [sum(t) + sum(u)] / [len(t) + len(u)]
= [len(t) * ave(t) + len(u) * ave(u)] / [len(t) + len(u)].

If ave(u) >= ave(t) then let s’ be t

ave(s) = [len(t) * ave(t) + len(u) * ave(u)] / [len(t) + len(u)]
>= [[len(t) + len(u)] * ave(t)] / [len(t) + len(u)]
= ave(t)
= ave(s’).

Otherwise, if ave(t) >= ave(u) then let s’ be u

ave(s) = [len(t) * ave(t) + len(u) * ave(u)] / [len(t) + len(u)]
>= [[len(t) + len(u)] * ave(u)] / [len(t) + len(u)]
= ave(u)
= ave(s’).

This leads a contradiction, and completes the proof.

ps. After writing this note, I saw a counter example [8, 0, 0, 8] as Hunter2 indicated. A workaround of this is to permit a slice having length of one, although the problem description defines a slice to contain at least two elements. I guess that doing this does not harm the soundness of the proof. The point is to ignore all slices having the length of 4 or more, not to consider the slice having length of one. For example, you may argue that [0, 8, 1, 1] can be splitted into [0] and [8, 1, 1], but we can simply ignore [0] and take [1, 1] instead.
'''

def min_average_slice(a):
    
    avg = (a[0] + a[1]) / 2.0 # the initial minimum
    pos = 0 # initial position
    
    for i in xrange(len(a)-2):
        # Try next 2-element slice
        if (a[i] + a[i+1]) / 2.0 < avg:
            avg = (a[i] + a[i+1]) / 2.0
            pos = i
        # Try next 3-element slice
        if (a[i] + a[i+1] + a[i+2]) / 3.0 < avg:
            avg = (a[i] + a[i+1] + a[i+2]) / 3.0
            pos = i
    
    # Try the last 2-element slice
    if (a[-1] + a[-2]) / 2.0 < avg:
        avg = (a[-1] + a[-2]) / 2.0
        pos = len(a) - 2
    
    return pos