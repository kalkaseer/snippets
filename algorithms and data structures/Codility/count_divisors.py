# coding: utf-8

'''
Write a function:

    int solution(int A, int B, int K);

that, given three integers A, B and K, returns the number of integers within
the range [A..B] that are divisible by K, i.e.:

    { i : A ≤ i ≤ B, i mod K = 0 }

For example, for A = 6, B = 11 and K = 2, your function should return 3,
because there are three numbers divisible by 2 within the range [6..11],
namely 6, 8 and 10.

Assume that:

        A and B are integers within the range [0..2,000,000,000];
        K is an integer within the range [1..2,000,000,000];
        A ≤ B.

Complexity:

        expected worst-case time complexity is O(1);
        expected worst-case space complexity is O(1).


Solutions explanation

Clearly the size of the interval [A, B] determines the answer, however we
distinguish two different cases because whether or not K divides A affects
the answer. Take K=10 for example. If A=20 and B=30 then the answer is 2.
If A=21 and B=31 then the answer is 1 despite the fact that the difference
A - B here is the same. Since K divides A in the first instance the answer
is one greater.

Now, let a = A % K (i.e. A = a (mod K))
so we may write A = a + mK for some integer m, here mK is the largest multiple
of K that is less than or equal to A and A – A % K = a + mK – a = mK
(note if A is a multiple of K then a = 0)

The number of multiples of K in [A,B] is the same as the number of multiples
of K in [mK,B] if a = 0 (i.e. K divides M) and one less than this if a > 0
(i.e. K doesn’t divide M), because in this case mK is not in the interval [A,B]

Now, the number of multiples of K in [mK,B] is equal to the number of multiples
of K in the interval [0,B-mK], which is floor((B-mK)/K) + 1 (the plus one is
there because 0 is a multiple)

so the answer is
(B - (A - A % K)) // K                             if A % K > 0
and
(B - (A - A % K)) // K + 1 = (B - A) // K + 1      if A % K == 0

'''

def count_divisors(a, b, k):

    if a % k == 0: return (b - a) // k + 1
    return (b - (a - (a % k))) // k