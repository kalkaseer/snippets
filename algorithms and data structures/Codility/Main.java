import java.util.*;
import java.util.stream.IntStream;

public class Main {

    public static int binaryGap(int N) {
        int max = 0;
        int current = 0;

        // skip leading 0's
        while (N > 0 && (N & 1) == 0) {
            N /= 2;
        }

        while (N > 0) {
            if ((N & 1) == 0) { // inside a gap
                ++current;
            } else { // gap ends here
                if (current > max)
                    max = current;
                current = 0;
            }
            N /= 2;
        }
        return max;
    }

//    public static void reverse(int[] a, int start, int end) {
//
//        int mid = (start + end);
//        for (int i = start; i < mid / 2; ++i) {
//            int temp = a[i];
//            a[i] = a[mid - i - 1];
//            a[mid - i - 1] = temp;
//        }
//    }
//
//    public static void rotate2(int[] a, int k) {
//        assert(k > 0 && k <= a.length);
//        reverse(a, 0, a.length);
//        reverse(a, 0, k);
//        reverse(a, k, a.length);
//    }

    public static void reverse(int[] a, int start, int end) {
        int tmp;
        while (start < end) {
            tmp = a[start];
            a[start++] = a[end];
            a[end--] = tmp;
        }
    }

    public static void rotate(int[] a, int k) {
        if (a.length == 0 || k == 0) return;
        k = k % a.length;
        reverse(a, a.length - k, a.length - 1);
        reverse(a, 0, a.length - k - 1);
        reverse(a, 0, a.length - 1);
    }

    public static long oddOccurrences(long[] a) {
        HashSet<Long> set = new HashSet<>();
        for (int i = 0; i < a.length; ++i) {
            if (! set.contains(a[i]))
                set.add(a[i]);
            else
                set.remove(a[i]);
        }
        return !set.isEmpty() ? set.iterator().next() : -1;
    }

    public static long oddOccurrences2(long[] a) {
        long r = 0;
        for (long n : a)
            r ^= n;
        return r;
    }

    public static int jumps(int x, int y, int d) {
        return (y - x) % d != 0 ? ((y - x) / d) + 1 : ((y - x) / d);
    }

    public static int tape_equilibrium(int[] a) {

        int leftSum = 0;
        int rightSum = 0;
        int i = 0;

        if (a.length == 2) return Math.abs(a[1] - a[0]);
        if (a.length == 1) return Math.abs(a[0]);

        for (; i < a.length / 2; ++i)
            leftSum += a[i];

        for (i = a.length / 2; i < a.length; ++i)
            rightSum += a[i];

        int diff = Math.abs(leftSum - rightSum);
        int cdiff = Integer.MAX_VALUE;

        int left = leftSum;
        int right = rightSum;

        for (i = a.length / 2 + 1; i < a.length; ++i) {
            left += a[i - 1];
            right -= a[i - 1];
            cdiff = Math.abs(left - right);
            if (cdiff < diff)
                diff = cdiff;
        }

        for (i = a.length / 2 - 1; i > -1; --i) {
            leftSum -= a[i];
            rightSum += a[i];
            cdiff = Math.abs(leftSum - rightSum);
            if (cdiff < diff)
                diff = cdiff;
        }

//        rightSum += leftSum;
//        leftSum = 0;
//
//        for (i = 1; i < a.length / 2; ++i) {
//            leftSum += a[i-1];
//            rightSum -= a[i-1];
//            cdiff = Math.abs(leftSum - rightSum);
//            if (cdiff < diff)
//                diff = cdiff;
//        }
        return diff;
    }

    public static int tape_equilibrium2(int[] a) {

        if (a.length == 2) return Math.abs(a[1] - a[0]);
        if (a.length == 1) return Math.abs(a[0]);

        int i = 0;
        int left = 0;
        int total = 0;

        for (; i < a.length; ++i)
            total += a[i];
        int diff = Math.abs(total - a[0] - a[0]);
        int cdiff = Integer.MAX_VALUE;

        for (i = 1; i < a.length; ++i) {
            left += a[i - 1];
            cdiff = Math.abs(left - (total - left));
            if (cdiff < diff)
                diff = cdiff;
        }

        return diff;
    }

    public static int findMissing(int[] a) {

        /*
        equivalently
        int sum = 0;
        for (int i = 0; i < a.length; ++i)
            sum += a[i] - (i + 1);
        return a.length + 1 - sum;
        */

        int sum = 0;
        for (int i = 0; i < a.length; ++i)
            sum ^= a[i] ^ (i + 1);
        return sum ^ (a.length + 1);
    }

    // Solution is O(n + m)
    public static int[] elementCounts(int[] a, int m) {
        int b[] = new int[m + 1];
        for (int i = 0; i < a.length; ++i)
            b[a[i]] += 1;
        return b;
    }

    /*
    You are given an integer m (1 <= m <= 1 000 000) and two non-empty,
    zero-indexed arrays A and B of n integers,
    a0, a1, . . . , aN−1 and b0, b1, . . . , bN−1 respectively
    (0 <= aI, bI <= m).
    The goal is to check whether there is a swap operation which can be
    performed on these arrays in such a way that the sum of elements in
    array A equals the sum of elements in array B after the swap.
    By swap operation we mean picking one element from array A and one
    element from array B and exchanging them.
    Solution is O(n + m)
     */
    public static boolean oneSwapSumEquals(int a[], int b[], int m) {
        int aSum = IntStream.of(a).sum(); // or parallel().sum()
        int bSum = IntStream.of(b).sum();
        int diff = bSum - aSum;
        if (diff % 2 == 1)
            return false
        diff /= 2;
        int counts[] = elementCounts(a, m);
        for (int i = 0; i < a.length; ++i)
            if (0 <= b[i] - diff && b[i] - diff <= m && counts[b[i] - diff] > 0)
                return true;
        return false;
    }

    /*
    b[1] = true
    missing = 0
    i = 1
        b[3] == false
            b[3] = true
            j = 2
                b[2] == false
                    missing = 2
                    break
     i = 2
        b[1] == true
     i = 3
        b[4] == false
            b[4] = true
            j = 2
            b[2] == false
            missing = 2
            break
     i = 4
        b[2] == false
            b[2] = true
            missing = 1
     i = 5
        b[3] == true
     i = 6
        b[5] == false
            b[5] = true
            j = 4
                b[4] == true
            j = 3
                b[3] == true
            j = 2
                b[2] == true
            j = 1
                b[1] == true
            return i = 6

     */
    public static int riverCrossing(int x, int a[]) {

        if (a.length == 1 && a[0] != x && x != 1) return -1;
        if (a.length == 0) return -1;

        boolean counts[] = new boolean[x + 1];
        counts[a[0]] = true;
        int missing = 0;
        for (int i = 1; i < a.length; ++i) {
            System.out.println("i " + i + " missing " + missing);
            if (! counts[a[i]]) {
                counts[a[i]] = true;
                if (a[i] == missing) {
                    missing = a[i] - 1;
                }
                int j = missing == 0 ? a[i] - 1 : missing;
                for (; j > 0; --j) {
                    if (! counts[j]) {
                        missing = j;
                        break;
                    } else {
                        missing = 0;
                    }
                }
                System.out.println(Arrays.toString(counts) + " i " + i);
                if (counts[x] && j == 0 && missing == 0)
                    return i;
            }
        }

        return -1;
    }

    public static int riverCrossing2(int x, int a[]) {

        if (a.length == 1 && a[0] != x) {
            if (x != 1)
                return -1;
            else
                return 0;
        }
        if (a.length == 0) return -1;

        boolean counts[] = new boolean[x + 1];

        for (int i = 0; i < a.length; ++i) {
            if (! counts[a[i]]) {
                counts[a[i]] = true;
                if (counts[x]) {
                    int j = 1;
                    for (; j < x; ++j) {
                        if (! counts[j]) break;
                    }
                    if (j == x) return i;
                }
            }
        }

        return -1;
    }

    // Best
    public static int riverCrossing3(int x, int a[]) {
        boolean[] covered = new boolean[x+1];
        int uncovered = x;

        for (int i = 0; i < a.length; ++i) {
            if (covered[a[i]])
                continue;
            else {
                covered[a[i]] = true;
                --uncovered;
                if (uncovered == 0)
                    return i;
            }
        }
        return -1;
    }

    // A permutation is a sequence from 1 to N in which all elements occur only once.
    public static boolean isPermutation(int a[]) {
        boolean counts[] = new boolean[a.length];
        for (int i = 0; i < a.length; ++i) {
            if (a[i] < 1 || a[i] > a.length)
                return false;
            else if (counts[a[i] - 1])
                return false;
            counts[a[i] - 1] = true;
        }
        return true;
    }

    private static <T> void ensureSize(List<T> list, int size, T obj) {
        int diff = size - list.size();
        if (diff > 0) {
            for (; diff > 0; --diff)
                list.add(obj);
        }
    }

    public static int missingElement(int[] a) {

        ArrayList<Boolean> list = new ArrayList<>();
        for (int i = 0; i < a.length; ++i) {
            if (a[i] > 0) {
                ensureSize(list, a[i], false);
                if (list.get(a[i] - 1))
                    continue;
                list.set(a[i] - 1, true);
                System.out.println(list);
            }
        }
        for (int i = 0; i < list.size(); ++i) {
            if (! list.get(i))
                return i + 1;
        }
        return -1;
    }

    public static int missingElement2(int[] a) {

        if (a.length == 1) {
            if (a[0] == 1) return 2;
            return 1;
        }

        ArrayList<Boolean> list = new ArrayList<>();
        for (int i = 0; i < a.length; ++i) {
            if (a[i] > 0) {
                ensureSize(list, a[i], false);
                if (list.get(a[i] - 1))
                    continue;
                list.set(a[i] - 1, true);
                System.out.println(list);
            }
        }

        if (list.isEmpty())
            return 1;

        for (int i = 0; i < list.size(); ++i) {
            if (! list.get(i))
                return i + 1;
        }

        return list.size() + 1;

//        long supposed = (n * (n + 1)) / 2;
//        if (first != 1)
//            supposed -= ((first-1) * first) / 2;
//
//        System.out.println("sum " + sum + " supposed " + supposed);
//        sum =  sum - supposed;
//        return sum != 0 ? (int) ++sum : n + 1;
    }

    public static int missingElement3(int[] a) {

        if (a.length == 1) {
            if (a[0] == 1) return 2;
            return 1;
        }

        HashSet<Integer> set = new HashSet<>();
        int min = Integer.MAX_VALUE;
        int max = 0;
        for (int i = 0; i < a.length; ++i) {
            if (a[i] > 0) {
                if (set.contains(a[i])) continue;
                set.add(a[i]);
                if (a[i] < min) min = a[i];
                if (a[i] > max) max = a[i];
            }
        }

        if (min > 1) return 1;

        if (set.size() == 1) {
            if (set.iterator().next() == 1) return 2;
            return 1;
        }

        long sum = 0;
        for (int n : set)
            sum += n;
        long expected = (max * (max + 1)) / 2;

        if (sum == expected) return max + 1;

        return (int) Math.abs(sum - expected);
    }

    public static int missingElement4(int[] a) {
        boolean occurred[] = new boolean[a.length + 1];
        for (int n : a)
            if (n >= 1 && n < a.length + 1)
                occurred[n] = true;
        for (int i = 1; i < occurred.length; ++i)
            if (! occurred[i])
                return i;
        return occurred.length;
    }

    public static int[] countersOperations(int n, int[] a) {

        int[] b = new int[n];
        int max = 0;
        int currentMax = 0;

        for (int m : a) {
            if (m >= 1 && m <= n) {
                if (max > b[m - 1])
                    b[m - 1] = max;
                ++b[m - 1];
                if (currentMax < b[m - 1])
                    currentMax = b[m - 1];
            } else if (m == n + 1) {
                max = currentMax;
            }
        }

        for (int i = 0; i < b.length; ++i)
            if (b[i] < max)
                b[i] = max;

        return b;
    }

    public static int discIntersect(int[] a) {

        int[] lower = new int[a.length];
        int[] upper = new int[a.length];

        for (int i = 0; i < a.length; ++i) {
            lower[i] = i - a[i];
            upper[i] = i + a[i];
        }

        Arrays.sort(lower);
        Arrays.sort(upper);

        int count = 0;
        int low = 0;

        for (int high = 0; high < a.length; ++high) {
            while (low < a.length && upper[high] >= lower[low])
                ++low;
            count += low - high - 1;
            if (count > 10000000)
                return -1;
        }
        return count;
    }

    public int solution(String s) {
        Stack<Character> stack = new Stack<>();
        for (int i = 0; i < s.length(); ++i) {
            char c = s.charAt(i);
            switch (c) {
                case '{': case '[': case '(':
                    stack.push(c);
                    break;
                default:
                    if (stack.empty())
                        return 0;
                    else {
                        char c2 = stack.pop();
                        switch (c2) {
                            case '{': if (c != '}') return 0; break;
                            case '(': if (c != ')') return 0; break;
                            case '[': if (c != ']') return 0; break;
                        }
                    }
            }
        }
        return stack.empty() ? 1 : 0;
    }

    public static int fish(int[] a, int[] b) {

        ArrayList<Integer> downstream =  new ArrayList<>();
        int alive = 0;

        for (int i = 0; i < a.length; ++i) {
            if (b[i] == 1) {
                downstream.add(a[i]);
            } else if (downstream.isEmpty()) {
                ++alive;
            } else {
                while (! downstream.isEmpty()) {
                    if (downstream.get(downstream.size() - 1) < a[i]) {
                        downstream.remove(downstream.size() - 1);
                    } else {
                        break;
                    }
                }
                if (downstream.isEmpty())
                    ++alive;
            }
        }

        return alive + downstream.size();
    }

    public static void main(String[] args) {

//        System.out.println("226  " + Integer.toBinaryString(226));
//        System.out.println("gap(226)  " + binaryGap(226));
//
//        int a[] = { 1, 2, 3, 4, 5, 6 };
//        rotate(a, 2);
//        System.out.println(Arrays.toString(a));
//
//        long b[] = { 9, 3, 9, 3, 9, 7, 9 };
//        System.out.println("odd occurrences " + oddOccurrences(b));
//        System.out.println("odd occurrences " + oddOccurrences2(b));
//
//        System.out.println("frog jumps " + jumps(10, 85, 30));
//
//        int c[] = { -1, 3, 3, 1, 2, 0, 0, -89, 4, 5, 6, 130, 1, 1, };
//        System.out.println("tape equilibrium 1 " + tape_equilibrium(c));
//        System.out.println("tape equilibrium 2 " + tape_equilibrium2(c));
//
//        int d[] = { 2, 3, 1, 5 };
//        System.out.println("find missing " + findMissing(d));
//
//        int e[] = { 1, 3, 1, 4, 5, 3, 2, 4 };
//        System.out.println("river crossing 1 " + riverCrossing(5, e));
//        System.out.println("river crossing 2 " + riverCrossing2(5, e));
//        System.out.println("river crossing 3 " + riverCrossing3(5, e));
//
//        int f[] = { 4, 1, 3, 2 };
//        int g[] = { 4, 1, 3 };
//        System.out.println("is permutation " + isPermutation(f) + " " + isPermutation(g));

        int h[][] = {
            { 1, 2, 3, 4 },
            { -1, -2, -3, -5, -2 },
            { -1, -2, -3, -5, -2, 0 },
            { 1, 3, 2, 4, 6, 1, 2, 3, 4, 7 },
            { 2, 3, 2, 4, 6, 5, 2, 3, 4, 7 },
            { -2147483648, 2147483647 },
            { 1, 3, 6, 4, 1, 2 },
        };
        System.out.println("find missing " + missingElement4(h[0]) + " expected 5");
        System.out.println("find missing " + missingElement4(h[1]) + " expected 1");
        System.out.println("find missing " + missingElement4(h[2]) + " expected 1");
        System.out.println("find missing " + missingElement4(h[3]) + " expected 5");
        System.out.println("find missing " + missingElement4(h[4]) + " expected 1");
        System.out.println("find missing " + missingElement4(h[5]) + " expected 1");
        System.out.println("find missing " + missingElement4(h[6]) + " expected 5");

        class CounterInstruction {
            public CounterInstruction(int n, int[] instructions, int[] expected) {
                this.n = n;
                this.instructions = instructions;
                this.expected = expected;
            }
            public int n;
            public int[] instructions;
            public int[] expected;
        }
        CounterInstruction [] counters = {
            new CounterInstruction(
                5, new int[]{ 3, 4, 4, 6, 1, 4, 4 }, new int[]{ 3, 2, 2, 4, 2 })
        };
        System.out.println("counters operations "
            + Arrays.toString(countersOperations(counters[0].n, counters[0].instructions))
            + " expected " + Arrays.toString(counters[0].expected));

        System.out.println("discs " + discIntersect(new int[]{1,5,2,1,4,0}));

        int[] fish = { 4, 3, 2, 1, 5 }; int[] fishDirection = { 0, 1, 0 , 0, 0 };
        System.out.println("fish " + fish(fish, fishDirection));
    }
}
