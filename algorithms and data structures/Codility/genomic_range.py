# coding: utf-8

'''
A DNA sequence can be represented as a string consisting of the letters
A, C, G and T, which correspond to the types of successive nucleotides in
the sequence. Each nucleotide has an impact factor, which is an integer.
Nucleotides of types A, C, G and T have impact factors of 1, 2, 3 and 4,
respectively. You are going to answer several queries of the form:
What is the minimal impact factor of nucleotides contained in a particular
part of the given DNA sequence?

The DNA sequence is given as a non-empty string S = S[0]S[1]...S[N-1]
consisting of N characters. There are M queries, which are given in non-empty
arrays P and Q, each consisting of M integers. The K-th query (0 ≤ K < M)
requires you to find the minimal impact factor of nucleotides contained in
the DNA sequence between positions P[K] and Q[K] (inclusive).

For example, consider string S = CAGCCTA and arrays P, Q such that:
    P[0] = 2    Q[0] = 4
    P[1] = 5    Q[1] = 5
    P[2] = 0    Q[2] = 6

The answers to these M = 3 queries are as follows:

  The part of the DNA between positions 2 and 4 contains nucleotides
  G and C (twice), whose impact factors are 3 and 2 respectively, so the answer is 2.

  The part between positions 5 and 5 contains a single nucleotide T, whose impact
  factor is 4, so the answer is 4.

  The part between positions 0 and 6 (the whole string) contains all nucleotides,
  in particular nucleotide A whose impact factor is 1, so the answer is 1.

Write a function:

    class Solution { public int[] solution(String S, int[] P, int[] Q); }

that, given a non-empty zero-indexed string S consisting of N characters and
two non-empty zero-indexed arrays P and Q consisting of M integers, returns an
array consisting of M integers specifying the consecutive answers to all queries.

The sequence should be returned as:

        a Results structure (in C), or
        a vector of integers (in C++), or
        a Results record (in Pascal), or
        an array of integers (in any other programming language).

For example, given the string S = CAGCCTA and arrays P, Q such that:
    P[0] = 2    Q[0] = 4
    P[1] = 5    Q[1] = 5
    P[2] = 0    Q[2] = 6

the function should return the values [2, 4, 1], as explained above.

Assume that:

        N is an integer within the range [1..100,000];
        M is an integer within the range [1..50,000];
        each element of arrays P, Q is an integer within the range [0..N − 1];
        P[K] ≤ Q[K], where 0 ≤ K < M;
        string S consists only of upper-case English letters A, C, G, T.

Complexity:

        expected worst-case time complexity is O(N+M);
        expected worst-case space complexity is O(N), beyond input storage
        (not counting the storage required for input arguments).

Elements of input arrays can be modified.

'''

def genome_range(s, p, q):

    mapping = { 'A': 0, 'C': 1, 'G': 2, 'T': 3 }
    result =[]
    length = len(s)
    
    # pos[i][j] = k means the next position for the nucleotides denoted by i
    # at position j in the string is position k.
    pos = [ [-1] * length for i in xrange(4) ]
    
    pos[ mapping[s[-1]] ][-1] = length - 1
    
    for j in range(length-2, -1, -1):
        for i in mapping.itervalues():
            pos[i][j] = pos[i][j+1]
        pos[ mapping[s[j]] ][j] = j
    
    for j in xrange(len(p)):
        for i in mapping.itervalues():
            if pos[i][ p[j] ] != -1 and pos[i][ p[j] ] <= q[j]:
                result.append(i+1)
                break
    
    return result


def minimum_impact_factor(s, q):

    impact_factors = { 'A': 0, 'C': 1, 'G': 2, 'T': 3 }
    length = len(s)

    # Aggregate running nucleotides positions. For each nucleotide at position
    # i, sorted by increasing impact factor, map a list of length equal to the
    # DNA string length. Each element of the list at position j is the next
    # position in the DNA string we encounter this nucleotide.
    aggregate = [ [-1] * length for i in xrange(4) ]

    # positions:   0 1 2 3 4 5 6
    # nucleotides: C A G C C T A
    #
    # aggregate:
    #      0  1  2  3   4  5  6
    #   A: 1  1  6  6   6  6  6
    #   C: 0  3  3  4   4 -1 -1
    #   G: 2  2  2  -1 -1 -1 -1
    #   T: 5  5  5  5   5  5 -1

    # working from end to start, the last nucleotide in the DNA string
    # corresponds to position length - 1 for the corresponding
    # nucleotide occurring at this position.
    aggregate[ impact_factors[s-1] ][-1] = length - 1

    # iterate over the string
    for j in xrange(length - 2, - 1, -1):
        for i in impact_factors.itervalues():
            aggregate[i][j] = aggregate[i][j+1]
        aggregate[ impact_factors[j] ] = j

    result = []

    for query in xrange(len(q)):
        for i in impact_factors.itervalues():
            if aggregate[i][query[0]] != -1 and aggregate[i][query[0]] <= query[1]:
                result.append(i + 1)  # nucleotides were damped by 1
                # we are scanning lower to higher, we have hit the lowest impact factor
                break

    return result