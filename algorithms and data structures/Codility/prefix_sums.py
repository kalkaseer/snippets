# Prefix sums
#
# A prefix sum table is an accumulated sum of elements in a sequence
# that allows us to find sums of continuous segments of the sequence
# in constant time.
#
# Given a sequence A = [a1, a2, a3, ..., an], the prefix sums sequence P is
# of length: length(A) + 1 and is defined as
# P = [0, a1 + 0, a2 + a1 + 0, a3 + a2 + a1 + a3 + 0, ..., an + an-1 + ... + 0]
# such that P[i] = P[i-1] + A[i-1] for i = [0, length(A) + 1]
#
# Construction of a prefix sum sequence requires O(n) in time where
# n is the number of elements in the original sequence

def prefix_sums(arr):
    p = [0] * (len(arr) + 1)
    for i in xrange(1, len(arr) + 1):
        p[i] = p[i-1] + arr[i-1]
    return p

# Suffix sum sequence is defined similiarly in O(n) too

def suffix_sums(arr):
    p = [0] * (len(arr) + 1)
    for i in xrange(len(arr)-1, -1, -1):
        p[i] = p[i+1] + arr[i]
    return p

# The sum of a sub-sequence of the original sequence can then be found
# in O(1) time.

def sequence_sum(prefixes, start, end):
    return prefixes[end + 1] - prefixes[start]

def sequence_sum_reversed(suffixes, start, end):
    return suffixes[start] - suffixes[end+1]


# Application
#
# Given a non-empty zero-indexed array of size 1 <= n <= 100000 integers
# such that every element is 0 <= a_i <= 1000. The array represents the number
# of mushrooms growing along consecutive spots along the road.
# A picker starts at position 0 <= k < n and is allowed to move 0 <= m < n
# moves. The picker can only moves to adject spots.
# Find the maximum amount of mushrooms the picker can pick.
#
# For example: given k = 4, m = 6
# array: [2, 3, 7, 5, 1, 3, 9]
#   pos:  0  1  2  3  4  5  6
# the maximum will be collected if the picker visits position 3, 2, 3, 4, 5, 6
# to collect 1 + 5 + 7 + 3 + 9 mushrooms.
#
# Observe that the best strategy would be to minimise changing directions, so 
# picker moves in one directions a few times then go to the opposite direction.
# Let p = 0, 1, ..., m. Then the picker moves in one direction p times then
# m - p > 0 times in the other direction. This solution requires O(m ^ 2).
# 
# Another solutions is to use prefix sums to find two extremes and collect 
# between these two extremes. This requires O (n + m).

def pick_mushrooms(arr, k, m):
    n = len(arr)
    result = 0
    prefixes = prefix_sums(arr)
    for p in xrange( min(m, k) + 1 ):
        left = k - p
        right = min( n - 1, max( k, k + m - 2 * p ) )
        result = max(result, sequence_sum(prefixes, left, right))
    for p in xrange( min( m + 1, n - k ) ):
        right = k + p
        left = max( 0, min( k, k - ( m - 2 * p ) ) )
        result = max(result, sequence_sum(prefixes, left, right))
    return result