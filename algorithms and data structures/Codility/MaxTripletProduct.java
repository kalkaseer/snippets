/*
A non-empty zero-indexed array A consisting of N integers is given.
The product of triplet (P, Q, R) equates to A[P] * A[Q] * A[R] (0 ≤ P < Q < R < N).

For example, array A such that:
  A[0] = -3
  A[1] = 1
  A[2] = 2
  A[3] = -2
  A[4] = 5
  A[5] = 6

contains the following example triplets:

        (0, 1, 2), product is −3 * 1 * 2 = −6
        (1, 2, 4), product is 1 * 2 * 5 = 10
        (2, 4, 5), product is 2 * 5 * 6 = 60

Your goal is to find the maximal product of any triplet.

Write a function:

    int solution(int A[], int N);

that, given a non-empty zero-indexed array A, returns the value of the
maximal product of any triplet.

For example, given array A such that:
  A[0] = -3
  A[1] = 1
  A[2] = 2
  A[3] = -2
  A[4] = 5
  A[5] = 6

the function should return 60, as the product of triplet (2, 4, 5) is maximal.

Assume that:

        N is an integer within the range [3..100,000];
        each element of array A is an integer within the range [−1,000..1,000].

Complexity:

        expected worst-case time complexity is O(N*log(N));
        expected worst-case space complexity is O(1), beyond input storage (not counting the storage required for input arguments).

Elements of input arrays can be modified.


The only thing to note is that two big negative numbers produce a big positive
number.
*/

import java.util.Arrays;

class MaxTripletProduct {
  
  // O( n log n)
  public int solve1(int[] a) {
    Arrays.sort(a);
    return Math.max(
      a[0] * a[1] * a[a.length - 1],
      a[a.length - 3] * a[a.length - 2] * a[a.length - 1]);
  }
  
  // O(n) without sorting
  public int solve2(int[] a) {
    // invariant s1 <= s2 <= s3
    int[] max = { Integer.MIN_VALUE, Integer.MIN_VALUE, Integer.MIN_VALUE };
    // invariant s1 <= s2
    int[] min = { Integer.MAX_VALUE, Integer.MAX_VALUE };
    
    for (int e : a) {
      updateMax(max, e);
      updateMin(min, e);
    }
    
    return Math.max(
      min[0] * min[1] * max[2],
      max[0] * max[1] * max[2]);
  }
  
  private void updateMax(int[] max, int a) {
    if (a > max[2]) {
      max[0] = max[1];
      max[1] = max[2];
      max[2] = a;
    } else if (a > max[1]) {
      max[0] = max[1];
      max[1] = a;
    } else if (a > max[0]) {
      max[0] = a;
    }
  }
  
  private void updateMin(int[] min, int a) {
    if (a < min[0]) {
      min[1] = min[0];
      min[0] = a;
    } else if (a < min[1]) {
      min[1] = a;
    }
  }
}