# coding: utf-8

# https://codility.com/programmers/task/peaks/
#
# 
#
# A non-empty zero-indexed array A consisting of N integers is given.
# 
# A peak is an array element which is larger than its neighbors. More
# precisely, it is an index P such that 0 < P < N − 1,  A[P − 1] < A[P]
# and A[P] > A[P + 1].
# 
# For example, the following array A:
#     A[0] = 1
#     A[1] = 2
#     A[2] = 3
#     A[3] = 4
#     A[4] = 3
#     A[5] = 4
#     A[6] = 1
#     A[7] = 2
#     A[8] = 3
#     A[9] = 4
#     A[10] = 6
#     A[11] = 2
# 
# has exactly three peaks: 3, 5, 10.
# 
# We want to divide this array into blocks containing the same number of
# elements. More precisely, we want to choose a number K that will yield
# the following blocks:
# 
#         A[0], A[1], ..., A[K − 1],
#         A[K], A[K + 1], ..., A[2K − 1],
#         ...
#         A[N − K], A[N − K + 1], ..., A[N − 1].
# 
# What's more, every block should contain at least one peak. Notice that
# extreme elements of the blocks (for example A[K − 1] or A[K]) can also be
# peaks, but only if they have both neighbors (including one in an
# adjacent blocks).
# 
# The goal is to find the maximum number of blocks into which the array
# A can be divided.
# 
# Array A can be divided into blocks as follows:
# 
#   one block (1, 2, 3, 4, 3, 4, 1, 2, 3, 4, 6, 2)
#   This block contains three peaks.
# 
#   two blocks (1, 2, 3, 4, 3, 4) and (1, 2, 3, 4, 6, 2)
#   Every block has a peak.
# 
#   three blocks (1, 2, 3, 4), (3, 4, 1, 2), (3, 4, 6, 2)
#   Every block has a peak. Notice in particular that the first block
#   (1, 2, 3, 4) has a peak at A[3], because A[2] < A[3] > A[4],
#   even though A[4] is in the adjacent block.
# 
# However, array A cannot be divided into four blocks, (1, 2, 3),
# (4, 3, 4), (1, 2, 3) and (4, 6, 2), because the (1, 2, 3) blocks do not
# contain a peak. Notice in particular that the (4, 3, 4) block contains
# two peaks: A[3] and A[5].
# 
# The maximum number of blocks that array A can be divided into is three.
# 
# Write a function:
# 
#     int solution(int A[], int N);
# 
# that, given a non-empty zero-indexed array A consisting of N integers,
# returns the maximum number of blocks into which A can be divided.
# 
# If A cannot be divided into some number of blocks, the function should
# return 0.
# 
# For example, given:
#     A[0] = 1
#     A[1] = 2
#     A[2] = 3
#     A[3] = 4
#     A[4] = 3
#     A[5] = 4
#     A[6] = 1
#     A[7] = 2
#     A[8] = 3
#     A[9] = 4
#     A[10] = 6
#     A[11] = 2
# 
# the function should return 3, as explained above.
# 
# Assume that:
# 
#  N is an integer within the range [1..100,000];
#  each element of array A is an integer within the range [0..1,000,000,000].
# 
# Complexity:
# 
#   expected worst-case time complexity is O(N*log(log(N)));
#   expected worst-case space complexity is O(N), beyond input storage
#   (not counting the storage required for input arguments).
# 
# Elements of input arrays can be modified.

'''
#include <numeric>

int solution(std::vector<int>& input) {

    if(input.size() < 3) // 1
        return 0;

    std::vector<unsigned> peaks(input.size() + 1); // 2
    for(std::size_t i = 1; i < input.size() - 1; ++i) {
        if(input[i] > input[i - 1] && input[i] > input[i + 1])
            peaks[i+1] = 1;
    }
    std::partial_sum(peaks.begin(), peaks.end(), peaks.begin());

    for(std::size_t size = 3; size <= input.size() / 2; ++size) { // 3
        if(input.size() % size) // 4
            continue;

        bool hasPeaks = true;
        for(std::size_t end = size; end < peaks.size(); end += size) { // 5
            if(peaks[end - size] == peaks[end]) {
                hasPeaks = false;
                break;
            }
        }
        if(hasPeaks) // 6
            return input.size() / size;
    }

    return peaks.back() ? 1 : 0; // 7
}
'''

def peaks_1(a):

    length = len(a)
    if length < 3:
        return 0

    peaks = [0] * (length + 1)

    for i in xrange(1, length - 1):
        if a[i - 1] < a[i] > a[i + 1]:
            peaks[i + 1] = peaks[i] + 1
        else:
            peaks[i + 1] = peaks[i]
    peaks[length] = peaks[length - 1]

    for block_size in xrange(3, length / 2):

        if length % block_size != 0:
            continue

        has_peaks = True

        for end in xrange(block_size, len(peaks), block_size):
            if peaks[end - block_size] == peaks[end]:
                has_peaks = False
                break

        if has_peaks:
            return length / block_size

    if peaks[-1] != 0:
        return 1

    return 0


def test_peaks_count():

    a = [1, 2, 3, 4, 3, 4, 1, 2, 3, 4, 6, 2]
    assert peaks_1(a) == 3

    a = [1, 4, 3, 4, 3, 4, 1, 2, 3, 4, 6, 2]
    assert peaks_1(a) == 3

    a = [0, 1, 0, 0, 1, 0, 0, 1, 0]
    assert peaks_1(a) == 3

    a = [0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0]
    assert peaks_1(a) == 1

    a = [0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1]
    assert peaks_1(a) == 4

    a = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    assert peaks_1(a) == 0

    a = [0] * 100000
    for i in xrange(1, len(a), 2):
        a[i] = 1
    assert peaks_1(a) == 25000


def peaks(a):
    
    length = len(a)
    if length < 3:
        return 0
        
    peaks = [0] * length
    
    for i in range(1, length - 1):
        peaks[i] = peaks[i-1]
        if a[i-1] < a[i] > a[i+1]:
            peaks[i] += 1
    
    if peaks[-2] == 0:
        return 0
    
    peaks[-1] = peaks[-2]
    
    count = 0
    
    for candidate in range(1, int(sqrt(length)) + 1):
        
        if length % candidate == 0:
            blocks, blocksize = candidate, length // candidate
            if peaks[blocksize - 1] != 0:
                for block in range(blocksize, length, blocksize):
                    if peaks[block - 1] == peaks[block + blocksize - 1]:
                        break
                else:
                    count = blocks
        
            if candidate * candidate == length:
                continue
        
            blocks, blocksize = blocksize, blocks
            if peaks[blocksize - 1] != 0:
                for block in range(blocksize, length, blocksize):
                    if peaks[block - 1] == peaks[block + blocksize - 1]:
                        break
                else:
                    return blocks
        
    return count



'''
https://codility.com/programmers/task/flags/

A non-empty zero-indexed array A consisting of N integers is given.

A peak is an array element which is larger than its neighbours. More precisely,
it is an index P such that 0 < P < N − 1 and A[P − 1] < A[P] > A[P + 1].

For example, the following array A:
    A[0] = 1
    A[1] = 5
    A[2] = 3
    A[3] = 4
    A[4] = 3
    A[5] = 4
    A[6] = 1
    A[7] = 2
    A[8] = 3
    A[9] = 4
    A[10] = 6
    A[11] = 2

has exactly four peaks: elements 1, 3, 5 and 10.

You are going on a trip to a range of mountains whose relative heights are
represented by array A, as shown in a figure below. You have to choose how many
flags you should take with you. The goal is to set the maximum number of flags
on the peaks, according to certain rules.

Flags can only be set on peaks. What's more, if you take K flags, then the
distance between any two flags should be greater than or equal to K. The
distance between indices P and Q is the absolute value |P − Q|.

For example, given the mountain range represented by array A, above,
with N = 12, if you take:

        two flags, you can set them on peaks 1 and 5;
        three flags, you can set them on peaks 1, 5 and 10;
        four flags, you can set only three flags, on peaks 1, 5 and 10.

You can therefore set a maximum of three flags in this case.

Write a function:

    int solution(int A[], int N);

that, given a non-empty zero-indexed array A of N integers, returns the maximum
number of flags that can be set on the peaks of the array.

For example, the following array A:
    A[0] = 1
    A[1] = 5
    A[2] = 3
    A[3] = 4
    A[4] = 3
    A[5] = 4
    A[6] = 1
    A[7] = 2
    A[8] = 3
    A[9] = 4
    A[10] = 6
    A[11] = 2

the function should return 3, as explained above.

Assume that:

        N is an integer within the range [1..400,000];
        each element of array A is an integer within the range [0..1,000,000,000].

Complexity:

        expected worst-case time complexity is O(N);
        expected worst-case space complexity is O(N), beyond input storage (not counting the storage required for input arguments).

Elements of input arrays can be modified.

'''
    
def _create_peaks(a):

    peaks = [False] * len(a)
    for i in xrange(1, len(a) - 1):
        if a[i-1] < a[i] > a[i+1]:
            peaks[i] = True
    return peaks

def _next_peak(a):
    peaks = _create_peaks(a)
    next = [0] * len(a)
    next[-1] = -1
    for i in xrange(len(a) - 2, -1, -1):
        if peaks[i]:
            next[i] = i
        else:
            next[i] = next[i + 1]
    return next

def flags(a):
    next = _next_peak(a)
    print 'next', next
    result = 0
    i = 1
    while (i - 1) * i <= len(a):
        print 'i', i
        pos = 0
        count = 0
        while pos < len(a) and count < i: 
            print '\t(1) pos', pos, 'count', count
            pos = next[pos]
            if pos == -1:
                break
            count += 1
            pos += i
            print '\t(2) pos', pos, 'count', count
        print 'result', result
        result = max(result, count)
        i += 1
    return result