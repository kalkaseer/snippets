# A leader in a sequence is the element which occurs more than N/2 times.
def leader1(a):
    stack = []
    for n in a:
        if not stack or stack[-1] == n:
            stack.append(n)
        else:
            stack.pop()
    if not stack:
        return -1
    count = 0
    for n in a:
        if n == stack[-1]: count += 1
    if (len(a) // 2) < count:
        return stack[-1]
    return -1

# We don't need to remember all the stacked values but only the last value
# and the count because stacked values are always the same
def leader2(a):

    size = 0
    candidate = -1
    for n in a:
        if size == 0:
            candidate = n
            size += 1
        elif candidate != n:
            size = 0
        else:
            size += 1
    if size == 0:
        return -1
    count = 0
    for n in a:
        if n == candidate:
            count += 1
    if len(a) // 2 < count:
        return candidate
    return -1


# I don't know why this runs slightly faster in Python
# Space O(n)
def equileaders(a):
    size = len(a)
    m = leader2(a)
    if m == -1:
        return -1
    counts = [0] * size
    leaders = 0
    equi = 0
    for i in xrange(size):
        if a[i] == m:
            leaders += 1
        counts[i] = leaders
    for i in xrange(size):
        if a[i] == m and \
                counts[i] > (i + 1) // 2 and \
                counts[size - 1] - counts[i] > (size - i - 1) // 2:
            equi += 1
    return equi


def equileaders2(a):
    size = len(a)
    m = leader2(a)
    if m == -1:
        return -1
    leaders = 0
    equi = 0
    encountered = 0
    for n in a:
        if n == m: leaders += 1
    for i in xrange(size):
        if a[i] == m: encountered += 1
        if encountered > (i + 1) // 2 and \
                (leaders - encountered) > (size - i - 1) // 2:
            equi += 1
    return equi
