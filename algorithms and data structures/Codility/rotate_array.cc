/*


A zero-indexed array A consisting of N integers is given. Rotation of the array means that each element is shifted right by one index, and the last element of the array is also moved to the first place.

For example, the rotation of array A = [3, 8, 9, 7, 6] is [6, 3, 8, 9, 7]. The goal is to rotate array A K times; that is, each element of A will be shifted to the right by K indexes.

Write a function:

    struct Results solution(int A[], int N, int K);

that, given a zero-indexed array A consisting of N integers and an integer K, returns the array A rotated K times.

For example, given array A = [3, 8, 9, 7, 6] and K = 3, the function should return [9, 7, 6, 3, 8].

Assume that:

        N and K are integers within the range [0..100];
        each element of array A is an integer within the range [−1,000..1,000].

In your solution, focus on correctness. The performance of your solution will not be the focus of the assessment.

*/

#include <vector>
using namespace std;
static inline void swap(vector<int> &A, int start, int end)
{
	int tmp = 0;
	while (start < end)
	{
		tmp = A[start];
		A[start++] = A[end];
		A[end--] = tmp;
	}
}
vector<int> rotate_array1(vector<int> &A, int K) {
	int len = A.size();
	if (0 == len || 0 == K)
		return A;
	K = K % len;
	swap(A, len - K, len - 1);
	swap(A, 0, len - K - 1);
	swap(A, 0, len - 1);
	return A;
}

int gcd(int a, int b) {
   if(b==0) return a;
   else return gcd(b, a%b);
}

void rotate_array2(vector &a, int k) {
  int j, d, tmp;
  for (int i = 0; i < gcd(a.size(), k); ++i) {
    tmp a[i];
    j = i;
    while(true) {
      d = j + k;
      if (d >= n)
        d = d - n;
      if (d == i)
        break;
      a[j] = a[d];
      j = d;
    }
    a[j] = tmp;
  }
}