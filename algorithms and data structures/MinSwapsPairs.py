# Minimum number of swaps required to arrange pairs
#
# There are n-pairs and therefore 2n people. everyone has one unique number
# ranging from 1 to 2n. All these 2n persons are arranged in random fashion
# in an Array of size 2n. We are also given who is partner of whom. Find
# the minimum number of swaps required to arrange these pairs such that all
# pairs become adjacent to each other.
#
# Input:
# n = 3  
# pairs[] = { (1,3), (2,6), (4,5) }  // 1 is partner of 3 and so on
# arr[] = {3, 5, 6, 4, 1, 2}
# 
# Output
# We can get {3, 1, 5, 4, 6, 2} by swapping 5 & 6, and 6 & 1


def is_in_pair(pair, person):
  if pair[0] == person: return 0
  if pair[1] == person: return 1
  return -1

def missing_from_pair(pair, person1, person2):
  
  p1_0 = pair[0] == person1
  p1_1 = pair[1] == person1
  p2_0 = pair[0] == person2
  p2_1 = pair[1] == person2
  
  if p1_0 and p1_1 and p2_0 and p2_1:
    return -1, -1
  elif p1_0:
    return 1, 1
  elif p1_1:
    return 1, 0
  elif p2_0:
    return 0, 1
  elif p2_1:
    return 0, 0
  else:
    return -2, -1

def search_for_missing(pair_person, people, idx):
  
  for i in xrange(idx, len(people)):
    if pair_person == people[i]:
      return i
  
  return -1

def min_swaps_count(pairs, people, i=0):
    
  hi = len(people)
  
  if i >= hi - 2:
    return 0
    
  k, p = missing_from_pair(pairs[i], people[i], people[i+1])
  
  print 'pair', pairs[i], 'people', people[i], people[i+1], 'k', k, 'p',p
    
  if k == -1:
    return min_swaps_count(pairs, people, i+2)
  elif k == -2:
    k = search_for_missing(pairs[i][p], people, i)
    people[i], people[k] = people[k], people[i]
    k = search_for_missing(pairs[i][p], people, i+1)
    people[i+1], people[k] = people[k], people[i+1]
    j = 2
  else:
    j = search_for_missing(pairs[i][p], people, i+k)
    print 'j', j
    people[j], people[i+k] = people[i+k], people[j]
    j = 1
    
    
  return min_swaps_count(pairs, people, i+2) + j


def test():
  pairs = [ (1,3), (2,6), (4,5) ]
  people = [ 3, 5, 6, 4, 1, 2 ]
  return min_swaps_count(pairs, people), people
  



def update_index(indices, a, ai, b, bi):
  indices[a] = ai
  indices[b] = bi


def _min_swaps_count2(pairs, people, indices, i, n):
  
  # all pairs processed
  if i > n:
    return 0
  
  # current pair is valid, advance
  if pairs[people[i]] == people[i+1]:
    return _min_swap_count2(pairs, people, indices, i+2, n)
  
  # people[i] and people[i+1] are not a pair
  
  one = people[i+1]
  indextwo = i+1
  indexone = indices[pairs[people[i]]]
  two = people[indices[pairs[people[i]]]]
  
  people[i+1], people[indexone] = people[indexone], people[i+1]
  update_index(indices, one, indexone, two, indextwo)
  
  a = _min_swap_count2(pairs, people, indices, i+2, n);
  
  # backtrack to previous configuration and restore previous
  # indices of one and two
  people[i+1], people[indexone] = people[indexone], people[i+1]
  update_index(indices, one, indextwo, two, indexone)
  one = people[i]
  indexone = indices[pairs[people[i+1]]]
  
  # swap people[i] with pair of people[i+1] and recursively compute
  # minimum swaps for the subproblem after this swap
  two = people[indices[pairs[people[i+1]]]]
  indextwo = 1
  people[i], people[indexone] = people[indexone], people[i]
  update_index(indices, one, indexone, two, indextwo)
  
  b = _min_swap_count2(pairs, people, indices, i+2, n);
  
  # backtrack
  people[i], people[indexone] = people[indexone], people[i]
  update_index(indices, one, indexone, two, indextwo)
  
  return 1 + min(a, b)
  

def min_swaps_count3(pairs, people, indices):
  
  count = 0
  for i in xrange(1, len(indices)-1):
    if people[i+1] != pairs[people[i]] and people[i-1] != pairs[people[i]]:
      pos = indices[pairs[people[i]]]
      indices[people[i+1]] = pos
      indices[people[pos]] = i+1
      people[i+1], people[pos] = people[pos], people[i+1]
      coutn += 1
  return count


# for each pair (a, b) let pairs[a] = b, pairs[b] = a
def arrange_pairs(pairs):
  p = [0] * (len(pairs) * 2 + 1)
  for i in xrange(0, len(pairs)):
    a = pairs[i][0]
    b = pairs[i][1]
    print (a,b)
    p[a] = b
    p[b] = a
  return p
  

def arrange_people(people):
  people.insert(0, 0)

def min_swaps_count2(pairs, people):
  
  pairs = rearrange_paris(pairs)
  people = arrange_people(people)
  
  indices = [0] * (2*len(pairs)+1)
  
  for i in xrange(len(indices)):
    indices[people[i]] = i
  
  return _min_swaps_count2(pairs, people, indices, 1, len(indices)-1)