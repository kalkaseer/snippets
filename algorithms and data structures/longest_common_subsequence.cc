/*
 * longest_common_subsequence.cc
 *
 *  Created on: Aug 8, 2016
 *      Author: Kareem Alkaseer
 */


#include <iostream>
#include "longest_common_subsequence.h"

using namespace std;

int main() {
  string s1;
  string s2;

  cout << "S1: ";
  getline(cin, s1);

  cout << "S2: ";
  getline(cin, s2);

  cout << "recursive: "
       << recursive::lcs_length(s1, s1.size(), s2, s2.size()) <<'\n';

  lcs m{ s1, s2 };
  cout << "dynamic: " << m.longest_length() << '\n';
  cout << "dynamic: " << m.longest_string() << '\n';

  vector<int> x{ 0, 8, 4, 12, 2, 10, 6, 14, 1, 9, 5, 13, 3, 11, 7, 15 };
  vector<int> v;
  lis(x, v);

  for (auto i : v)
      cout << i << " ";
    cout << "\n";

  return 0;
}
