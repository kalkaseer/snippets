/* Author: Kareem Alkaseer */

// Greatest Common Divisor

#include <utility>  // swap
#include <iostream> // cout, cin, etc.
#include <chrono>
#include <tuple>
#include <cstdint>


// Euclidean method

uint64_t gcd_euclidean(uint64_t a, uint64_t b) {
	
	uint64_t c;
	while (a != 0) {
		c = a;
		a = b % a;
		b = c;
	}
	return b;
}

// Binary method

uint64_t gcd_binary(uint64_t a, uint64_t b) {
	
	uint64_t shift;
	
	if (a == 0) return b;
	if (b == 0) return a;
	
	// Find the greatest power of 2 factor that divides both a and b
	for (shift = 0; ((a | b) & 1) == 0; ++shift) {
		a >>= 1;
		b >>= 1;
	}
	
	while ((a & 1) == 0)  // while a is even
		a >>= 1;            // make it odd
	
	do {
		// All factors of 2 in b are not common because a is odd.
		// Hence we remove them from b.
		// The loop will terminate because b != 0
		while ((b & 1) == 0)  // while b is even
			b >>= 1;            // make it odd
		
		// make sure a <= b
		if (a > b)
			std::swap(a, b);
				
		b -= a; // Now b >= a
	} while (b != 0);
	
	// Restore common of factors of 2
	return a << shift;
}

namespace recursive {

// Recursive naive method 
	
uint64_t gcd_naive(uint64_t a, uint64_t b) {
	
	if (a == 0) return b;
	return gcd_naive( b % a, a);
}

// Binary method

uint64_t gcd_binary(uint64_t a, uint64_t b) {
	
	if (a == b) return a;
	if (a == 0) return b;
	if (b == 0) return a;
	
	// Now we look for factors of 2
	
	if (~a & 1) { // a is even
		if (b & 1) // b is odd
			return gcd_binary(a >> 1, b);
		else // a and b are even
			return gcd_binary(a >> 1, b >> 1) << 1;
	}
	
	if (~b & 1) // a is odd and b is even
		return gcd_binary(a, b >> 1);
	
	if (a > b)
		return gcd_binary((a - b) >> 1, b);
	
	return gcd_binary((b - a) >> 1, a);
}

} // namespace recursive

uint64_t lcm(
	uint64_t a, uint64_t b,
	std::function<uint64_t (uint64_t, uint64_t)> func = gcd_binary
) {
	uint64_t c = func(a, b);
	return c == 0 ? 0 : a / c * b;
}

std::tuple<uint64_t, uint64_t>
timeit(
	std::function<uint64_t (uint64_t, uint64_t)> func,
	uint64_t a, uint64_t b
) {
	
	using namespace std::chrono;
	
	milliseconds millis;
	uint64_t result;
	
	for (int i = 0; i < 1000; ++i) {
		auto start = high_resolution_clock::now();
		result = func(a, b);
		auto end = high_resolution_clock::now();
		millis = duration_cast<milliseconds>(end - start);
	}
	
	return std::make_tuple(result, millis.count());
}

int main() {
	
	using namespace std;
	namespace r = recursive;
	
	uint64_t a;
	uint64_t b;
	uint64_t result;
	uint64_t millis;
	
	cout << "a b: ";
	cin >> a >> b;
	
	std::tie(result, millis) = timeit(r::gcd_naive, a, b);
	cout << "Naive: result = " << result << " time = " << millis << endl;
	
	std::tie(result, millis) = timeit(r::gcd_binary, a, b);
	cout << "Recursive binary: result = " << result << " time = " << millis << endl;

	std::tie(result, millis) = timeit(gcd_euclidean, a, b);
	cout << "Euclidean: result = " << result << " time = " << millis << endl;

	std::tie(result, millis) = timeit(gcd_binary, a, b);
	cout << "Binary: result = " << result << " time = " << millis << endl;
	
	result = lcm(a, b);
	cout << "LCM: result = " << result << endl;
	
	return 0;
}

