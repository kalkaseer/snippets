# Basic operations on data structutres

from collections import deque

class Node(object):
  def __init__(self, key, left=None, right=None):
    self.key = key
    self.left = left
    self.right = right
  
  def __repr__(self):
    return str(self.key)

class Graph(object):
  def __init__(self):
    from collections import defaultdict
    self.data = defaultdict(list)
  
  def add(self, vertex, adjacent):
    self.data[vertex].extend(adjacent)
    
  def adjacent(self, vertex):
    return self.data[vertex]

######################################################################
#            27
#     14            23
# 10      19    31      42
######################################################################

def make_tree():
  
  root = Node(27)
  root.left = Node(14)
  root.right =  Node(23)
  
  root.left.left = Node(10)
  root.left.right = Node(19)
  
  root.right.left = Node(31)
  root.right.right = Node(42)
  return root

def make_graph():
  
  g = Graph()
  g.add(27, [14, 23])
  g.add(14, [10, 19])
  g.add(23, [31, 42])
  return g

######################################################################
# Breadth First Search
#
# The breadth first search explores current node and neighbours before
# moving to the next level. For trees, it is equivalent to level-order
# traversal.
#######################################################################

def bfs_tree(root, visit):
  
  if not root:
    return
  
  q = deque()
  
  node = root
  q.append(root)
  
  while q:
    node = q.popleft()
    visit(node)
    if node.left:
      q.append(node.left)
    if node.right:
      q.append(node.right)

def test_bfs_tree():
  
  root = make_tree()
  visit = lambda node: tree.append(node)
  tree = []
  bfs_tree(root, visit)
  
  assert(str(tree) == str([27, 14, 23, 10, 19, 31, 42]))


def bfs_graph(graph, vertex, visit):
  
  if not vertex:
    return
  
  v = vertex
  
  q = deque()
  q.append(v)
  
  seen = set()
  
  while q:
    v = q.popleft()
    visit(v)
    for a in graph.adjacent(v):
      if a not in seen:
        q.append(a)
        seen.add(a)


def test_bfs_graph():
  
  g = make_graph()
  v = []
  visit = lambda vertex: v.append(vertex)
  bfs_graph(g, 27, visit)
  
  assert(str(v) == str([27, 14, 23, 10, 19, 31, 42]))

######################################################################
# Depth First Search
# Traveses a sub-branch then goes back to the original parent node,
# traveses the other branch, the processes the original parent node.
# The ordering of the traversal could be either pre-order, post-order,
# or in-order.
######################################################################

def dfs_preorder_tree(root, visit):
  
  if not root:
    return
    
  node = root
  s = [node]
  
  while s:
    node = s.pop()
    visit(node)
    if node.right:
      s.append(node.right)
    if node.left:
      s.append(node.left)

def test_dfs_preorder_tree():
  
  root = make_tree()
  visit = lambda node: tree.append(node)
  tree = []
  dfs_preorder_tree(root, visit)
  
  assert(str(tree) == str([27, 14, 10, 19, 23, 31, 42]))


def dfs_inorder_tree(root, visit):
  
  if not root:
    return
  
  node = root
  s = []
  
  while s or node:
    if node:
      s.append(node)
      node = node.left
    else:
      node = s.pop()
      visit(node)
      node = node.right

def test_dfs_inorder_tree():
  
  root = make_tree()
  visit = lambda node: tree.append(node)
  tree = []
  dfs_inorder_tree(root, visit)
  
  assert(str(tree) == str([10, 14, 19, 27, 31, 23, 42]))


def dfs_postorder_tree(root, visit):
  
  if not root:
    return
    
  node = root
  last = None
  s = []
  
  while s or node:
    if node:
      s.append(node)
      node = node.left
    else:
      peek = s[-1]
      if peek.right and peek.right != last:
        node = peek.right
      else:
        visit(peek)
        last = s.pop()
  
def test_dfs_postorder_tree():
  
  root = make_tree()
  visit = lambda node: tree.append(node)
  tree = []
  dfs_postorder_tree(root, visit)
  
  assert(str(tree) == str([10, 19, 14, 31, 42, 23, 27]))

def dfs_preorder_graph(graph, vertex, visit):
  
  if not vertex:
    return
  
  v = vertex
  
  s = [v]
  
  seen = set()
  
  while s:
    v = s.pop()
    if v not in seen:
      seen.add(v)
      visit(v)
      # reversed is actually optional but is done here for symmetry with trees
      for a in reversed(graph.adjacent(v)):
        s.append(a)

def test_dfs_preorder_graph():
  
  g = make_graph()
  visit = lambda node: v.append(node)
  v = []
  dfs_preorder_graph(g, 27, visit)
  
  assert(str(v) == str([27, 14, 10, 19, 23, 31, 42]))



#######################################################################
# Heap operations
#
# A max-heap has the property that the key of a parent node is larger 
# than or equal to its children's keys.
# A min-heap has the propert that the key of a parent node is smaller
# than or equal to its childres's keys. 
# Operations are implemented on a binary heap.
# Heap is usually stored in an array such that:
# [root, left child, right child, left's left, left's right, ...]
# Hence,
#  left(pos)   = a[(2 * pos) + 1]
#  right(pos)  = a[(2 * pos) + 2]
#  parent(pos) = a[(pos - 1) / 2]
#######################################################################

# def make_max_heap():
#   return [44, 42, 35, 33, 31, 19, 27, 10, 26, 14]
#
# def make_min_heap():
#   return [10, 14, 19, 26, 31, 42, 27, 44, 35, 33]
#
# def heap_parent(pos):
#   return (pos - 1) / 2
#
# def heap_left(pos):
#   return (2 * pos) + 1
#
# def heap_right(pos):
#   return (2 * pos) + 2
#
# def is_max_heap(arr):
#   for i in xrange(len(arr) - 1, -1, -1):
#     if arr[heap_parent(i)] < arr[i]:
#       return False
#   return True
#
# def is_min_heap(arr):
#   for i in xrange(len(arr) - 1, -1, -1):
#     if arr[heap_parent(i)] > arr[i]:
#       return False
#   return True


# def max_heap_insert(heap, key):
#
#   # 1) Append node to the end of the heap
#   # 2) Compare node to its parent
#   # 3) If parent < node, swap
#   # 4) Repeat (2) and (3) until parent >= node
#
#   heap.append(key)
#
#   pos = len(heap) - 1
#
#   while heap[heap_parent(pos)] < heap[pos]:
#     heap[heap_parent(pos)], heap[pos] = heap[pos], heap[heap_parent(pos)]
#     pos = heap_parent(pos)
#
#
# def test_max_heap_insert():
#
#   ref = make_max_heap()
#   heap = []
#
#   for x in sorted(ref):
#     max_heap_insert(heap, x)
#
#   print sorted(ref)
#   print heap
#
#   assert(is_max_heap(heap))
#
#
# def min_heap_insert(heap, key):
#
#   # 1) Append node to the end of the heap
#   # 2) Compare node to its parent
#   # 3) If parent > node, swap
#   # 4) Repeat (2) and (3) until parent <= node
#
#   heap.append(key)
#
#   pos = len(heap) - 1
#
#   while heap[heap_parent(pos)] > heap[pos]:
#     heap[heap_parent(pos)], heap[pos] = heap[pos], heap[heap_parent(pos)]
#     pos = heap_parent(pos)
#
#
# def test_min_heap_insert():
#
#   ref = make_min_heap()
#   heap = []
#
#   for x in reversed(sorted(ref)):
#     min_heap_insert(heap, x)
#
#   print heap
#
#   assert(is_min_heap(heap))