# Author: Kareem Alkaseer 

def fib(n):
    
    if (n == 0): return 0
    a = 0
    b = 1
    c = 0
    
    for i in xrange(2, n+1):
        c = a + b
        a = b
        b = c
    
    return b


def fibsum(n): return fib(n+2) - 1

def fibevensum(n):
    
    if (n == 0): return 0
    a = 0
    b = 1
    c = 0
    s = 0
    
    for i in xrange(2, n+1):
        c = a + b
        if (~c & 1): s+= c
        a = b
        b = c
    
    return s