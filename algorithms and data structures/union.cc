#include <vector>
#include <iostream>

using namespace std;

// input: [0,1,1,2,2,5] [0,1,2,2,2,6] output: [0,1,2,2]
// input: [0,1,1] [0,1,2,3,4,5] output: [0,1]
void intersect(vector<int> a, vector<int> b, vector<int> & v) {
	if (!(a.size() && b.size()))
		return;
	for (int i = 0, j = 0; i < a.size() - 1 || j < b.size() - 1; ) {
		if (a[i] == b[j]) {
			v.push_back(a[i]);
			++i;
			++j;
		} else if (a[i] < b[j]) {
			++i;
		} else {
			++j;
		}
	}
}

int main() {
	vector<int> a = {1,2,3,3,3,4,5,5,6,6};
	vector<int> b = {2,3,3,3,3,4,5,6};
	vector<int> v;
	intersect(a, b, v);
	for (int i : v)
		cout << i << " ";
	cout << endl;
	
	a = {0,1,1,2,2,5};
	b = {0,1,2,2,2,6};
	v.clear();
	intersect(a, b, v);
	for (int i : v)
		cout << i << " ";
	cout << endl;
	
	a = {0,1,1};
	b = {0,1,2,3,4,5,6};
	v.clear();
	intersect(a, b, v);
	for (int i : v)
		cout << i << " ";
	cout << endl;
	
	return 0;
}