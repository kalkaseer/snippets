# Max Number of Platforms
#
# http://www.geeksforgeeks.org/minimum-number-platforms-required-railwaybus-station/

def solve(arrival, departure):
  
  arrival.sort()
  departure.sort()
  
  curr_count = 1
  max_count = 1
  
  i = 1
  j = 0
  
  while i < len(arrival) and j < len(arrival):
    
    if arrival[i] < departure[j]:
      curr_count += 1
      i += 1
      if curr_count > max_count:
        max_count = curr_count
    else:
      curr_count -= 1
      j += 1
  
  return max_count


testcases = [
  [
    [ 900, 940, 950, 1100, 1500, 1800 ],  # arrival
    [ 910, 1200, 1120, 1130, 1900, 2000 ] # departure
  ]
]