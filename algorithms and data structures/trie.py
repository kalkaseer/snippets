# A trie also represents a node
class Trie(object):
    
    def __init__(self):
        self.children = {}
        self.flag = False # whether a word ends at this node
    
    def add(self, ch):
        self.children[ch] = Trie()
    
    def insert(self, word):
        node = self
        for ch in word:
            if ch not in node.children:
                node.add(ch)
            node = node.children[ch]
        node.flag = True
        
    def contains(self, word):
        node = self
        for ch in word:
            if ch not in node.children:
                return False
            node = node.children[ch]
        return node.flag
    
    def all_suffixes(self, prefix):
        results = set()
        if self.flag:
            results.add(prefix)
        if not self.children:
            return results
        return reduce(
            lambda a, b: a | b,
            [node.all_suffixes(prefix + ch) for (ch, node) in self.children.items()]) | results
    
    def autocomplete(self, prefix):
        node = self
        for ch in prefix:
            if ch not in node.children:
                return set()
            node = node.children[ch]
        return node.all_suffixes(prefix)