/*
 * binary_indexed_tree.h
 *
 *  Created on: Aug 8, 2016
 *      Author: Kareem Alkaseer
 */

#ifndef BINARY_INDEXED_TREE_H_
#define BINARY_INDEXED_TREE_H_

#include <vector>
#include <cassert>

/*
 * Fenwick tree (binary indexed tree)
 * https://www.topcoder.com/community/data-science/data-science-tutorials/binary-indexed-trees/
 *
 * Update entry and calculate prefix sum in O(log n)
 */

template< class T >
class fenwick_tree {

  std::vector<T> m_tree;

public:

  using value_type = T;
  using size_type = std::size_t;

  template< class Iterator >
  explicit fenwick_tree(Iterator first, Iterator last) {
    m_tree.resize(last - first + 1);
    for (size_type i = 0; first != last; ++first, ++i)
      update(i, *first);
  }

  void insert(value_type v) {
    update(m_tree.size(), v);
  }

  void update(size_type i, value_type delta) {
    for (; i < m_tree.size(); i |= i + 1)
      m_tree[i] += delta;
  }

  value_type sum(size_type left, size_type right) {
    assert(left > 0 && "left < 1 ")
    return sum(right) - sum(left - 1);
  }

  value_type sum(size_type idx) {
    value_type sum = 0;
    while (idx >= 0) {
      sum += m_tree[idx];
      idx &= idx + 1;
      --idx;
    }
    return sum;
  }
};



#endif /* BINARY_INDEXED_TREE_H_ */
