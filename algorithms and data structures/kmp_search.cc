// Knuth-Morris-Pratt string search
// Author: Kareem Alkaseer

#include <iostream>
#include <string>

ssize_t kmpSearch(const std::string & w, const std::string & s, const ssize_t t[]) {
	
	ssize_t m = -1; // beginning of the current match in s
	
	for (ssize_t i = 0; i < w.size(); ++i) {
		while (m > -1 && s[m + 1] != w[i]) m = t[m];
		++m;
		if (m == s.size() - 1) return i - m;
	}
	
	return -1;
}

void kmpTable(std::string & s, ssize_t t[]) {
	
	size_t pos = 2; // current position in t
	size_t cnd = 0; // index in s of the next char of the current candidate
	
	t[0] = -1;
	t[1] = 0;
	
	while (pos < s.size()) {
		
		if (s[pos - 1] == s[cnd]) {
			t[pos] = cnd + 1;
			++cnd;
			++pos;
		} else if (cnd > 0) {
			cnd = t[cnd];
		} else {
			t[pos] = 0;
			++pos;
		}	
	}
}

int main() {
	
	std::string s{ "ABCDABD" };
	std::string w{ "ABC ABCDAB ABCDABCDABDE" };
	ssize_t t[s.size()];
	
	kmpTable(s, t);
	
	ssize_t m = kmpSearch(w, s, t);
	std::cout << s << "  m: " << m << std::endl;
	
	return 0;
}
