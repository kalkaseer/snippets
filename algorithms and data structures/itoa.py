# Author: Kareem Alkaseer

def reverse_slow(s):
    r = ''
    for i in xrange(len(s)-1, -1, -1):
        r = ''.join((r, s[i]))
    return r

def reverse_buitin(s):
    return ''.join(reversed(s))

def reverse(s):
    return s[::-1]
    
def itoa_1(n):
    s = ''
    while n > 0:
        m = n % 10; 
        n /= 10
        s += str(m)
    return reverse(s)

_itoa_alphabet = "zyxwvutsrqponmlkjihgfedcba9876543210123456789abcdefghijklmnopqrstuvwxyz"

def itoa_2(n, base):
    
    from cStringIO import StringIO
    
    if base < 2 or base > 36: return None
    buf = StringIO()
    
    sign = 1
    
    if n < 0: 
        sign = -1
        n = -n
    
    while True:
        tmp = n
        n /= base
        buf.write(_itoa_alphabet[35 + (tmp - n * base)])
        if n == 0:
            break
    
    if sign < 0: buf.write('-')
    
    return buf.getvalue()[::-1]

_itoa_alphabet2 = "0123456789abcdefghijklmnopqrstuvwxyz"

def itoa_3(n, base):
    
    from cStringIO import StringIO
    
    if base < 2 or base > 36: return None
    buf = StringIO()
    
    sign = 1
    
    if n < 0: 
        sign = -1
        n = -n
    
    while True:
        tmp = n
        n /= base
        buf.write(_itoa_alphabet2[(tmp % base)])
        if n == 0:
            break
    
    if sign < 0: buf.write('-')
    
    return buf.getvalue()[::-1]