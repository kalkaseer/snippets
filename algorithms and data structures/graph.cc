/*
 * graph.cc
 *
 *  Created on: Apr 18, 2016
 *      Author: Kareem Alkaseer
 */

#include <unordered_map>
#include <vector>
#include <initializer_list>
#include <type_traits>
#include <string>
#include <iostream>
#include <list>
#include <unordered_set>
#include <random>

template< class... T > struct all_same : std::false_type { };
template< class T > struct all_same< T > : std::true_type { };
template<> struct all_same<> : std::true_type { };
template< class T, class... Ts >
struct all_same< T, T, Ts... > : all_same< T, Ts... > { };


template< class T >
std::ostream & print(const T & a, bool newline = true) {
  for (auto & e : a)
    std::cout << e << " ";
  if (newline)
    std::cout << std::endl;
  return std::cout;
}

template< class T >
std::ostream & print_all(const T & a, bool newline = true) {
  std::for_each(a.begin(), a.end(), [](auto m){ std::cout << "\t"; print(m); });
  if (newline)
    std::cout << std::endl;
  return std::cout;
}


template< class T >
class graph {

public:

  using value_type = T;

  using const_value_ref = const value_type &;

  using rvalue_type = value_type &&;

  using path_type = std::vector<value_type>;

  using path_ref = path_type &;

  using paths_type = std::list<path_type>;

  using path_initializer = std::initializer_list<value_type>;


  graph() = default;

  void add(
    const value_type & value, const path_initializer & list
  ) {
      if (m_data.find(value) == m_data.end()) {
      m_data.emplace(value, path_type(list));
    } else {
      path_type & v { m_data[value] };
      v.insert(v.end(), list);
    }
  }

  void add(value_type && value, path_initializer && list) {
    if (m_data.find(std::forward<value_type>(value)) == m_data.end()) {
      m_data.emplace(
        std::forward<value_type>(value),
        path_type(std::forward<path_initializer>(list)));
    } else {
      path_type & v { m_data[value] };
      v.insert(v.end(), std::forward<path_initializer>(list));
    }
  }

  template< class... V >
  typename std::enable_if<
    all_same< value_type, typename std::decay<V>::type... >::value,
    void
  >::type
  add(const_value_ref value, const V &... list) {
    if (m_data.find(value) == m_data.end()) {
      m_data.emplace(
        value,
        path_type( { list... } ));
    } else {
      path_type & v { m_data[value] };
      v.insert(v.end(), { list... });
    }
  }

  template< class... V >
  typename std::enable_if<
    all_same< value_type, typename std::decay<V>::type... >::value,
    void
  >::type
  add(rvalue_type value, V &&... list) {
    if (m_data.find(std::forward<value_type>(value)) == m_data.end()) {
      m_data.emplace(
        std::forward<value_type>(value),
        path_type( { std::forward<value_type>(list)... } ));
    } else {
      path_type & v { m_data[value] };
      v.insert(v.end(), { std::forward<value_type>(list)... });
    }
  }

  path_type path(
    const_value_ref start, const_value_ref end,
    path_type cpath = path_type()
  ) {
    cpath.push_back(start);

    if (start == end)
      return std::move(cpath);

    if (m_data.find(start) == m_data.end())
      return std::move(path_type());
    const path_type & s = m_data.at(start);

    if (s.empty())
      return std::move(path_type());

    auto end_iter = cpath.end();

    for (auto & node : s) {
      if (std::find(cpath.begin(), end_iter, node) == end_iter) {
        auto newpath = path(node, end, cpath);
        if (! newpath.empty())
          return std::move(newpath);
      }
    }

    return std::move(path_type());
  }

  inline path_type path(
    rvalue_type start, rvalue_type end, path_type cpath = path_type()
  ) {
    return path(start, end, cpath);
  }

  paths_type all_paths(
    const_value_ref start, const_value_ref end,
    path_type cpath = path_type(),
    paths_type apath = paths_type()
  ) {
    cpath.push_back(start);

    if (start == end) {
      apath.push_back(cpath);
      return std::move(apath);
    }

    if (m_data.find(start) == m_data.end())
      return std::move(paths_type());

    const path_type & s = m_data.at(start);

    if (s.empty())
      return std::move(paths_type());

    paths_type paths;
    auto end_iter = cpath.end();

    for (auto & node : s) {
      if (std::find(cpath.begin(), end_iter, node) == end_iter) {
        auto newpaths = all_paths(node, end, cpath, apath);
        if (! newpaths.empty())
          std::move(newpaths.begin(), newpaths.end(), std::back_inserter(paths));
      }
    }

    return std::move(paths);
  }

  inline paths_type all_paths(
    rvalue_type start, rvalue_type end,
    path_type cpath = path_type(), paths_type apath = paths_type()
  ) {
    return all_paths(start, end, cpath, apath);
  }

  path_type shortest_path(
    const_value_ref start, const_value_ref end,
    path_type cpath = path_type()
  ) {
    cpath.push_back(start);

    if (start == end)
      return std::move(cpath);

    if (m_data.find(start) == m_data.end())
      return std::move(path_type());

    const path_type & s = m_data.at(start);
    if (s.empty())
      return std::move(path_type());

    auto end_iter = cpath.end();
    path_type shortest;

    for (auto & node : s) {
      if (std::find(cpath.begin(), end_iter, node) == end_iter) {
        auto newpath = shortest_path(node, end, cpath);
        if (shortest.size() == 0 || newpath.size() < shortest.size()) {
          std::swap(newpath, shortest);
        }
      }
    }

    return std::move(shortest);
  }

  inline path_type shortest_path(
    rvalue_type start, rvalue_type end, path_type cpath = path_type()
  ) {
    return shortest_path(start, end, cpath);
  }

  path_ref get(const_value_ref node) {
    return m_data.at(node);
  }

  path_ref get(rvalue_type node) {
    return m_data.at(std::forward<value_type>(node));
  }

  path_type nodes() {
    path_type v;
    std::transform(m_data.begin(), m_data.end(), std::back_inserter(v),
      [](const typename map_type::value_type & pair){ return pair.first; });
    return std::move(v);
  }

private:

  using map_type = std::unordered_map<value_type, path_type>;

  map_type m_data;
};

void test() {

  std::string alphabet { "abcdefghijklmnopqrstuwxyz" };
  std::string numbers { "0123456789" };

  std::mt19937 gen { std::random_device()() };
  std::uniform_int_distribution<> dist_alphabet { 0, 25 };
  std::uniform_int_distribution<> dist_count { 1, 1000 };
  std::unordered_set<std::string> s;

  graph<std::string> g;

  for (int k = 0; k < 1000; ++k) {

    s.insert(std::string(1, alphabet[dist_alphabet(gen)]));

    auto it = s.begin();
    g.add(*it, {});
    auto v = g.get(*it);
    ++it;
    for (int i = 1; i < s.size(); ++i, ++it)
      v.push_back(std::move(*it));
    print(v);
  }

  auto keys = g.nodes();
  auto zero_key = keys[2];
  for (auto & k : keys) std::cout << k << std::endl;
  auto zero_key_set = g.get(zero_key);
  print(zero_key_set);
}

int main() {

  graph<std::string> g;
  g.add(std::string("a"), std::string("b"), std::string("c"));
  g.add(std::string("b"), std::string("c"), std::string("d"));
  g.add(std::string("c"), std::string("d"));
  g.add(std::string("d"), std::string("c"));
  g.add(std::string("e"), std::string("f"));
  g.add(std::string("f"), std::string("c"));

  auto p = g.path("a", "d");
  print(p);

  auto ap = g.all_paths("a", "d");
  print_all(ap);

  auto shortest = g.shortest_path("a", "d");
  print(shortest);

  test();

  auto keys = g.nodes();
  print(keys);

  auto a = g.get("a");
  print(a);

  return 0;
}
