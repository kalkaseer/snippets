import primes

def divisors(n):
    a = primes.factorize(n)
    a = set(a)
    sq = int(n/2)
    results = []
    for k in xrange(1, sq+1):
        if n % k == 0: results.append(k) 
    return results


'''
https://en.wikipedia.org/wiki/Pythagorean_triple#Generating_a_triple
Solution using Euclid's formula

Let's do some math: In general, every solution will have the form

a = k (x² - y²)
b = 2 k x y
c = k (x² + y²)

where k, x and y are positive integers, y < x and gcd(x,y) = 1 

a + b + c = k · x² - k · y² + 2k · x · y + k · x² + k · y²
          = 2k · x² + 2k · x · y
          = 2k · x · (x+y) 
          = 1000

Divide by 2: k · x · (x+y) = 500

Set s = x + y: k · x · s = 500

Solve k · x · s = 500, where k, x and s are integers and x < s < 2x.
Since all of them divide 500, they can only take the values
1, 2, 4, 5, 10, 20, 25, 50, 100, 125, 250, 500.

If n is odd
  return "no solution"
else
  L = List of divisors of n/2
for x in L
  for s in L
    if x< s <2*x and n/2 is divisible by x*s
      y=s-x
      k=((n/2)/x)/s      
      add (k*(x*x-y*y),2*k*x*y,k*(x*x+y*y)) to list of solutions
sort the triples in the list of solutions
delete solutions appearing twice
return list of solutions

You can still improve this:

    x will never be bigger than the root of n/2
    the loop for s can start at x and stop after 2x has been passed (if the list is ordered)

For n = 1000, the program has to check six values for x and depending on the details of implementation up to one value for y.

A simplified version that will not generate all triplet could be expressed in
Eculid's original method that didn't include the k-factor
    a = 2mn;
    b = m^2 - n^2; 
    c = m^2 + n^2;
    a + b + c = 1000;
    2mn + (m^2 - n^2) + (m^2 + n^2) = 1000; 
    2mn + 2m^2 = 1000; 
    2m(m+n) = 1000; 
    m(m+n) = 500; 
    m > n; m = 20; n= 5; 
    a = 200; b = 375; c = 425;

'''
def solve(n):
    if n % 2 != 0:
        return None
    else:
        results = []
        L = divisors(int(n/2))
        for x in L:
            for s in L:
                if x < s and s < 2 * x and ((n/2) % (x*s) == 0):
                    y = s - x
                    if not nt.gcd(x,y) == 1: continue
                    k = (n/2) / x / s
                    results.append(
                        (k * (x*x - y*y), 2 * k * x * y, k * (x*x + y*y)))
        return results  

'''
THIS IS PARTIALLY CORRECT: Doesnt return all triplets
    b = a;
    if a, b (a <= b)) and c are the Pythagorean triplet,
    then b, a (b >= a) and c - also the solution, so we can search only one case
    c = 1000 - a -b; it's one of the conditions of the problem (don't need to scan all possible 'c': just calculate it) 
'''
def solve2(n):
    for a in xrange(1, int(n/3)):
        for b in xrange(a+1, int(n/2)+1):
            c = n - a - b
            if a**2 + b**2 == c**2:
                return (a, b, c)
            