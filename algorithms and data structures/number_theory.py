# coding: utf-8
# Author: Kareem Alkaseer

'''
Sum of natural numbers less than N

0: 0  1  2  3  4  5  6  7  8  9                          
 
1: 10 11 12 13 14 15 16 17 18 19  10 + 12 * 3 + 9        
   **    --       --       --
2: 20 21 22 23 24 25 26 27 28 29  20 + 21 * 3 + 9 + 25   
   ** --       -- **    --
3: 30 31 32 33 34 35 36 37 38 39       30 * 3 + 9 + 35   
   --       --    ** --
4: 40 41 42 43 44 45 46 47 48 49  40 + 42 * 3 + 9        
   **    --       --       --
5: 50 51 52 53 54 55 56 57 58 59  50 + 51 * 3 + 9 + 55   
   ** --       -- **    --
6: 60 61 62 63 64 65 66 67 68 69       60 * 3 + 9 + 65   
   --       --    ** --
	 
	 
2 (1*10) + (1*10 + 2) * 3 + 9
1 (2*10) + (2*10 + 1) * 3 + 9 + (2*10) + 5
0        + (3*10 + 0) * 3 + 9 + (3*10) + 5 + (3*10) + 9
'''

def sum_multiples_35(n):
    
    c = n / 10
    if c == 0:
        s = 0
        for i in xrange(n):
            if i % 3 == 0 or i % 5 == 0:
                s += i
        return s
        
    r = n % 10
    m = 2
    s = 23
    for i in xrange(1, c):
        k = i * 10
        if m == 2:
            s += k
            s = s + (3*(k+2)) + 9
            m = 1
        elif m == 1:
            s += 2*k
            s = s + (3*(k+1)) + 14
            m = 0
        else:
            s += (5*k)
            s += 23
            m = 2
    
    if r != 0:
        k = n - r
        s += k
        for i in xrange(k + 1, n):
            if i % 3 == 0 or i % 5 == 0:
                s += i
    return s

# a is the first term, b is the nth term, n is the number of terms
# sum = (n / 2) (a + b)
# d is the difference between two successive terms
# b = a + (n - 1) d
# given x the last term then
# a + (n - 1) d = x
# n = int( ((x - a) / d) + 1 )
# by definition of this problem a = d then
# n = ((x - d) / d) + 1 = int( x / d )
# let int(x / d) = c
# b = a + (n - 1) d 
#   = a + (c - 1) a 
#   = a + (c * a) - a
#   = a * c
# sum = (n/2) (a+b)
#     = (c / 2) (a + a * c)
#     = ( c * (a + a * c) ) / 2
#     = ( (a * c) + (a * c^2) )  / 2
#
# which allows us to make a general optimal function to calculate
# the sum of multiples of a number less than N

def sum_multiples_1(n, a):
    n -= 1
    c = n/a
    return ( (a * c) + (a * pow(c,2)) ) / 2 

# for multiples of two numbers we need to check if they have a common
# divisor because we will sum duplicates if we don't subtract the sum
# of the common terms from the sum of the two multiples, i.e., we
# subtract the least common multiple sequence.
# lcm(3, 5) = 15, so for 3 and 5 we will calculate 15, 30, 45,... twice

def sum_multiples_2(n, a, b):
    n -= 1
    if gcd(a, b) == 1:
        ca = n / a
        cb = n / b
        f  = lcm(a, b)
        cf = n / f
        return \
            ( (a * ca) + (a * pow(ca, 2)) ) / 2 + \
            ( (b * cb) + (b * pow(cb, 2)) ) / 2 - \
            ( (f * cf) + (f * pow(cf, 2)) ) / 2
    else:
        c = n / a
        return ( (a * c) + (a * pow(c, 2)) ) / 2 
            

def gcd(a, b):
    
    shift = 0
    
    # Find the greatest power of 2 factor that divides both a and b
    while ((a | b) & 1) == 0:
        shift += 1
        a >>= 1
        b >>= 1
    
    while (a & 1) == 0: # while a is even
        a >>= 1         # make it odd
    
    while True:
        
        # All factors of 2 in b are not common because a is odd.
        # Hence we remove them from b.
        # The loop will terminate because b != 0
        while (b & 1) == 0:
            b >>= 1
        
        # make sure a <= b, swap if necessary.
        if a > b:
            a ^= b
            b ^= a
            a ^= b
        
        b -= a # now b >= a
        
        if b == 0:
            break
    
    return a << shift # Restore common of factors of 2

def lcm(a, b):
    c = gcd(a, b)
    return a / c * b if c != 0 else 0

# Find triplets meeting a condition in a list.
# Naive method
# Time complexity: O(n^3)
def find_triplet_sums_1(alist, condition=lambda a,b,c:a+b+c<12):
    
    result = []
    for i in xrange(len(alist)):
        for j in xrange(i+1, len(alist)):
            for k in xrange(j+1, len(alist)):
                if condition(alist[i], alist[j], alist[k]):
                    result.append((alist[i], alist[j], alist[k]))
    return result


# Find triplets meeting a condition in a list.
# Assumes linear total order so that if condition is not met for a < b
# it cannot be met for a >= b.
# Note: python cost of function call will hinder the performance.
def find_triplet_sums_2(alist, condition=lambda a,b,c:a+b+c<12):
    
    alist.sort()
    result = []
    for i in xrange(len(alist)):
        if not condition(alist[i], alist[i], alist[i]):
            return result
        for j in xrange(i+1, len(alist)):
            if not condition(alist[i], alist[j], alist[j]):
                return result
            for k in xrange(j+1, len(alist)):
                if condition(alist[i], alist[j], alist[k]):
                    result.append((alist[i], alist[j], alist[k]))
                else:
                    break
    return result
    

# Time complexity: O(n^2)
def count_triplet_sums(alist, condition=lambda a,b,c:a+b+c<12):
    
    alist.sort()
    result = 0
    
    for i in xrange(len(alist) - 2):
        j = i + 1
        k = len(alist) - 1
        
        while j < k:
            if condition(alist[i], alist[j], alist[k]):
                result += k - j
                j += 1
            else:
                k -= 1
    
    return result

# 3SUM problem asks if a given set of n real numbers contains three
# elements that sum to N. Solved in O(n^2) on average with a hashtable.
# Solved in O(n^2) worst-case by this algorithm.
def threesum(alist, n):
    
    results = []
    alist.sort()
    
    for i in xrange(len(alist) - 3):
        a = alist[i]
        start = i + 1
        end = len(alist) - 1
        while start < end:
            b = alist[start]
            c = alist[end]
            if a + b + c == n:
                results.append((a, b, c))
                end -= 1
            elif a + b + c > n:
                end -= 1
            else:
                start += 1
    
    return results

# Given an array of integers, write a function that returns true
# if there is a triplet (a, b, c) that satisfies a^2 + b^2 = c^2.
# Time complexity: average O(n^2)
# Space complexity: O(1)
def find_pythagorean_triplet(alist):
    
    alist.sort()
    
    # Square every element
    for i in xrange(len(alist)):
        alist[i] = alist[i] * alist[i]
    
    for i in xrange(len(alist)-1, 1, -1):
        
        low = 0
        high = i - 1
        
        while low < high:
            if alist[low] + alist[high] == alist[i]:
                return (alist[low], alist[high], alist[i])
            if alist[low] + alist[high] < alist[i]:
                low += 1
            else:
                high -= 1
    
    return None

# Given two lists, return elements in the first whose squares are present in
# the second.
# input: [3,1,4,5,19,6], [14,9,22,36,8,0,64,25]
# output: [9,25,36]

def find_squares(a, b):
    
    def collect_squares(a):
        s = set()
        i = 1
        for n in a:
            m = n
            while m > 0:
                m -= i
                i += 2
            if n != 0 and m == 0:
                s.add(n)
            i = 1
        return s
    
    if not a or not b: return None
    
    s = collect_squares(b) # or s = set(b)
    
    if not s: return None
    
    result = []
    for n in a:
        m = n * n
        if m in s:
            result.append(m)
    return result

# Given an array, sort it in zig-zag order, i.e., a < b > c < d > e < f.
# Sorting the array and then fixing the the first element and swapping
# the next two consecutive elements: a[0], swap(a[1], a[2]), swap[3], a[4])
# can achieve zig-zag sort in O(n log n) because we need to sort first.
# But if we maintain a flag of the required order we need and pass the array
# only once swapping the next two elements if the required order is currently
# reversed let this problem to be solved in O(n). For example, supposed we 
# require '<' but we have a > b > c then swapping b and c ensures that
# a > c < b.
# Time complexity: O(n)
# Space complixty: O(1)
def zigzag(alist):
    
    flag = True # true indicates we expect '<', false indicates '>'
    for i in xrange(len(alist)-2):
        if flag:
            if alist[i] > alist[i + 1]:
                alist[i], alist[i + 1] = alist[i + 1], alist[i]
        else:
            if alist[i] < alist[i + 1]:
                alist[i], alist[i + 1] = alist[i + 1], alist[i]
        flag = not flag


import bisect

'''
Given two sorted arrays A and B, generate all possible arrays such that
first element is taken from A then from B then from A and so on in increasing
order till the arrays exhausted. The generated arrays should end with an
element from B.

For Example 
A = {10, 15, 25}
B = {1, 5, 20, 30}

The resulting arrays are:
  10 20
  10 20 25 30
  10 30
  15 20
  15 20 25 30
  15 30
  25 30
'''
def duozigzag_(a, b, result):
    
    if not a: return result
    current = []
    j = 0
    
    for n in a:
        if current and n < current[len(current)-1]: continue
        j = bisect.bisect(b, n, j)
        if j >= len(b): # n > max(b[j])
            break
        current.append(n)
        current.append(b[j])
    
    if current: 
        result.append(tuple(current))
    
        if len(current) > 2:
            for i in xrange(1,len(current),2):
                result.append((current[0], current[i]))
    
    return f2(a[1:], b, result)

def duo_zigzag(a, b):
    result = []
    duozigzag_(a, b, result)
    return result

def duo_zigzag_verbose(a, b, result):
    print 'a:', a
    print 'b:', b
    if not a: return result
    current = []

    for n in a:
        if current and n < current[len(current)-1]:
            print 'skipping:', n, '..', current
            continue
        print 'n:', n
        j = bisect.bisect(b, n, j)
        if j >= len(b): # n > max(b[j])
            break
        print 'j:', j

        current.append(n)
        current.append(b[j])
        print 'current add', (n, b[j])

    if current: result.append(tuple(current))
    print '\nresult:', result, '\n'
    return duo_zigzag_verbose(a[1:], b, result)

def _duo_zigzag(a, b, c, i, j, clen, flag):
    
    # include valid element from a
    if flag:
        if clen: print c
        for k in xrange(i, len(a)):
            if not clen:
                c[clen] = a[k]
                _duo_zigzag(a, b, c, k+1, j, clen, not flag)
            else:
                if a[k] > c[clen]:
                    c[clen+1] = a[k]
                    _duo_zigzag(a, b, c, k+1, j, clen+1, not flag)
    else:
        for k in xrange(j, len(b)):
            if b[k] > c[clen]:
                c[clen+1] = b[k]
                _duo_zigzag(a, b, c, k+1, j, clen+1, not flag)
    
def duo_zigzag2(a, b):
    c = [None] * (len(a) + len(b))
    _duo_zigzag(a, b, c, 0, 0, 0, True)

# Compute x ^ y in O(log y)

def power(x, y):
    
    if y < 0:
        raise ValueError('exponent must be non-negative')
    
    a = 1
    while y > 0:
        if y & 1: # if y is odd
            a *= x
        # y is now even 
        y >>= 1 # y / 2
        x *= x  # x to x^2
    return a

# Modular Exponentiation
# The problem with above solutions is, overflow may occur for large value
# of n or x. Therefore, power is generally evaluated under modulo of
# a large number.
#
# Below is the fundamental modular property that is used for efficiently
# computing power under modular arithmetic.
#
# (a mod p) (b mod p) ≡  (ab) mod p
# 
# or equivalently 
#
# ( (a mod p) (b mod p) ) mod p  =  (ab) mod p
#
# For example a = 50,  b = 100, p = 13
# 50  mod 13  = 11
# 100 mod 13  = 9
# 
# 11*9 ≡ 1500 mod 13
# or 
# 11*9 mod 13 = 1500 mod 13
#
# Time complexity: O(log y)

def mod_power(x, y, p):
    
    if y < 0:
        raise ValueError('exponent must be non-negative')
    
    a = 1
    x %= p # ensure x <= p
    
    while y > 0:
        if y & 1: # if y is odd
            a = (a * x) % p
        # y is now even 
        y >>= 1 # y / 2
        x = (x * x) % p  # x to x^2
    return a