/*
 * heap.cc
 *
 *  Created on: Apr 13, 2016
 *      Author: Kareem Alkaseer
 */

#include <deque>
#include <cassert>
#include <iostream>
#include <type_traits>
#include <algorithm>
#include <vector>

#define call_member_func(object, fn_ptr) ((object).*(fn_ptr))

enum class heap_type {
  max, min
};

template< class T, heap_type H, class C = std::deque<T> >
class heap {

public:

  using container_type = C;
  using value_type = T;
  using size_type = typename container_type::size_type;
  using self_type = heap<T, H, C>;
  using iterator = typename container_type::iterator;
  using const_iterator = typename container_type::const_iterator;

  heap()
    : compare(determine_compare_func())
  { }

  template< class Iter >
  heap(Iter begin, Iter end)
    : compare(determine_compare_func()), m_data(begin, end)
  {
    for (int i = size()/2; i >= 0; --i)
      heapify(i);
  }

  heap(std::initializer_list<value_type> list)
    : compare(determine_compare_func()), m_data(std::move(list))
  {
    for (int i = size()/2; i >= 0; --i)
      heapify(i);
  }

  ~heap() { }

  size_type size() const { return m_data.size(); }

  bool empty() const { return size() == 0; }

  size_type parent(size_type pos) const {
    return (pos - 1) / 2;
  }

  size_type left(size_type pos) const {
    return (2 * pos) + 1;
  }

  size_type right(size_type pos) const {
    return (2 * pos) + 2;
  }

  const T & peek() const {
    assert(size() != 0);
    return m_data.front();
  }

  T & peek() {
    assert(size() != 0);
    return m_data.front();
  }

  T pop() {
    assert(size() != 0);
    T root = m_data.front();
    m_data.pop_front();
    return root;
  }

  std::ostream & print() const {
    for (auto it = m_data.cbegin(); it != m_data.cend(); ++it)
      std::cout << *it << " ";
    return std::cout;
  }

  std::ostream & print(std::ostream & s) const {
    for (auto it = m_data.cbegin(); it != m_data.cend(); ++it)
      s << *it << " ";
    return s;
  }

  void push(const value_type & value) {
    m_data.push_back(value);
    push_helper(size() - 1);
  }

  void push(value_type && value) {
    m_data.push_back(value);
    push_helper(size() - 1);
  }

  void push(const std::initializer_list<value_type> & list) {
    for (auto it = list.begin(); it != list.end(); ++it)
      push(*it);
  }

  void push(std::initializer_list<value_type> && list) {
    for (auto it = list.begin(); it != list.end(); ++it)
      push(*it);
  }

  inline iterator begin() {
    return m_data.begin();
  }

  inline iterator end() {
    return m_data.end();
  }

  inline void clear() {
    m_data.clear();
  }


private:

  using compare_func = bool (heap::*)(const T &, const T &);

  inline compare_func determine_compare_func() {
    return H == heap_type::max ?
      &self_type::compare_max : &self_type::compare_min;
  }

  inline bool compare_min(const T & lhs, const T & rhs) {
    return lhs < rhs;
  }

  inline bool compare_max(const T & lhs, const T & rhs) {
    return lhs > rhs;
  }

  void push_helper(size_type pos) {
    size_type ppos = parent(pos);
    if (
      ppos < size() &&
      call_member_func(*this, compare)(m_data[pos], m_data[ppos])
    ) {
      std::swap(m_data[pos], m_data[ppos]);
      push_helper(ppos);
    }
  }

  void heapify(size_type pos) {

    size_type target = pos;
    if (
      left(pos) < size() &&
      call_member_func(*this, compare)(m_data[left(pos)], m_data[pos])
    )
      target = left(pos);
    if (
      right(pos) < size() &&
      call_member_func(*this, compare)(m_data[right(pos)], m_data[target])
    )
      target = right(pos);

    if (target != pos) {
      std::swap(m_data[pos], m_data[target]);
      heapify(target);
    }
  }

  compare_func compare;

  container_type m_data;
};

template< class T >
using max_heap = heap< T, heap_type::max >;

template< class T >
using min_heap = heap< T, heap_type::min >;


template< class T >
void print(T & heap) {
  for (auto it = heap.begin(); it != heap.end(); ++it)
    std::cout << *it << " ";
  std::cout << std::endl;
}

int main() {

  std::cout << "original 1 3 2 5 4 9" << std::endl;

  std::cout << std::endl;

  max_heap<int> maxh{ 1, 3, 2, 5, 4, 9 };

  min_heap<int> minh{ 1, 3, 2, 5, 4, 9 };

  print(maxh); std::cout << "reference: 9 5 2 3 4 1" << std::endl;
  std::cout << "is max heap: " << std::boolalpha
            << std::is_heap(maxh.begin(), maxh.end()) << std::endl;

  std::cout << std::endl;

  std::vector<int> v{ 1, 3, 2, 5, 4, 9 };
  std::make_heap(v.begin(), v.end());
  print(v);

  std::cout << std::endl;

  maxh.clear();
  maxh.push({ 1, 3, 2, 5, 4, 9 });
  maxh.print() << "  reference ";

  v.clear();
  v.push_back(1); std::push_heap(v.begin(), v.end());
  v.push_back(3); std::push_heap(v.begin(), v.end());
  v.push_back(2); std::push_heap(v.begin(), v.end());
  v.push_back(5); std::push_heap(v.begin(), v.end());
  v.push_back(4); std::push_heap(v.begin(), v.end());
  v.push_back(9); std::push_heap(v.begin(), v.end()); print(v);

  std::cout << std::endl;

  print(minh); std::cout << "reference: 1 3 2 5 4 9" << std::endl;

  return 0;
}


