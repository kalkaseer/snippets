_base36alphabet = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'

def base36encode(n):
    
    if not isinstance(n, (int, long)):
        raise TypeError('number must be an integer')

    if n < 0:
        base36 = '-'
        n = -n
    else:
        base36 = ''

    while n:
        n, i = divmod(n, 36)
        base36 = ''.join((base36, _base36alphabet[i]))

    return base36 or _base36alphabet[0]


def base36decode(number):
    return int(number, 36)