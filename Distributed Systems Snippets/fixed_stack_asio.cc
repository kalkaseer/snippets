/**
 * Copyright 2016 Kareem Alkaseer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <vector>
#include <memory>
#include <mutex>
#include <thread>
#include <iostream>
#include "asio.hpp"
#include "asio/spawn.hpp"
#include "sync_printer.h"

template< class T >
class fixed_stack {

public:

  using value_type = T;

  explicit fixed_stack(std::size_t max)
    : m_max_size(max)
  { }

  ~fixed_stack() { }

  bool put(value_type value) {

    unique_lock ulock(m_lock);
    if (m_data.size() == m_max_size)
      return false;
    m_data.push_back(value);
    return true;
  }

  std::tuple<bool, value_type> get() {

    unique_lock ulock(m_lock);
    if (m_data.size() == 0)
      return std::make_tuple(false, 0);
    value_type result = m_data.back();
    m_data.pop_back();
    return std::make_tuple(true, result);
  }

private:

  using unique_lock = std::unique_lock<std::mutex>;

  std::vector<value_type> m_data;
  std::mutex m_lock;
  std::size_t m_max_size;
};


int main() {

  fixed_stack<std::size_t> stack(10);

  asio::io_context io_context;
  auto work = std::make_shared<asio::io_context::work>(io_context);

  std::vector<std::thread> threads;
  for (int i = 0; i < 10; ++i) {
    threads.emplace_back([&io_context](){ io_context.run(); });
  }

  for (std::size_t i = 0; 10000; ++i) {

    asio::spawn(
      io_context,
      [&stack, i](asio::yield_context yield) {
        bool ok = stack.put(i);
        if (ok)
          printer::println("pushed ", i);
        else
          printer::println("full stack");
      });

    asio::spawn(
      io_context,
      [&stack](asio::yield_context yield) {
        bool ok;
        size_t value;
        std::tie(ok, value) = stack.get();
        if (ok)
          printer::println("poped ", value);
        else
          printer::println("empty stack");
      });
  }

  work.reset();
  for (auto & th : threads) th.join();
  return 0;

}



