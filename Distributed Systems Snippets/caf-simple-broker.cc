/*
 * caf-simple-broker.cc
 *
 *  Created on: Apr 13, 2016
 *      Author: Kareem Alkaseer
 */


#include "caf/config.hpp"

#ifdef WIN32
# define _WIN32_WINNT 0x0600
# include <Winsock2.h>
#else
# include <arpa/inet.h> // htonl
#endif

#include <vector>
#include <string>
#include <limits>
#include <memory>
#include <cstdint>
#include <cassert>
#include <iostream>

#include "caf/all.hpp"
#include "caf/io/all.hpp"

using std::cout;
using std::cerr;
using std::endl;

using ping_atom = caf::atom_constant<caf::atom("ping")>;
using pong_atom = caf::atom_constant<caf::atom("pong")>;
using kickoff_atom = caf::atom_constant<caf::atom("kickoff")>;

// print an exit message with custom name
void print_on_exit(const caf::actor & handle, const std::string & name) {
  handle->attach_functor([=](caf::abstract_actor * ptr, uint32_t reason) {
    caf::aout(ptr) << name << " exited with reason " << reason << endl;
  });
}

caf::behavior ping(caf::event_based_actor * self, size_t num_pings) {
  auto count = std::make_shared<size_t>(0);
  return {
    [=](kickoff_atom, const caf::actor & pong) {
      self->send(pong, ping_atom::value, int32_t(1));
      self->become(
        [=](pong_atom, int32_t value) -> caf::message {
          if (++*count >= num_pings) self->quit();
          return caf::make_message(ping_atom::value, value + 1);
        }
      );
    }
  };
}

caf::behavior pong() {
  return {
    [](ping_atom, int32_t value) {
      return caf::make_message(pong_atom::value, value);
    }
  };
}

// utility function to sending an integer type.
template< class T >
void write_int(caf::io::broker * self, caf::io::connection_handle handle, T value) {

  using unsigned_type = typename std::make_unsigned<T>::type;
  auto copy = static_cast<T>(htonl(static_cast<unsigned_type>(value)));
  self->write(handle, sizeof(T), &copy);
  self->flush(handle);
}

void write_int(caf::io::broker * self, caf::io::connection_handle handle, uint64_t value) {

  // write two uint32 value instead (htonl does not work for 64bit ints)
  write_int(self, handle, static_cast<uint32_t>(value));
  write_int(self, handle, static_cast<uint32_t>(value >> sizeof(uint32_t)));
}

// utility function for reading an integer from incoming data
template< class T >
void read_int(const void * data, T & storage) {

  using unsigned_type = typename std::make_unsigned<T>::type;
  memcpy(&storage, data, sizeof(T));
  storage = static_cast<T>(ntohl(static_cast<unsigned_type>(storage)));
}

void read_int(const void * data, uint64_t & storage) {
  uint32_t first;
  uint32_t second;
  read_int(data, first);
  read_int(reinterpret_cast<const char *>(data) + sizeof(uint32_t), second);
  storage = first | (static_cast<uint64_t>(second) << sizeof(uint32_t));
}

// broker implementation
caf::behavior broker_impl(
  caf::io::broker * self, caf::io::connection_handle handle, const caf::actor & buddy
) {
  // we assume io_fsm manages a broker with exactly one connection, i.e., the
  // connection pointed to by 'handle'.
  assert(self->num_connections() == 1);

  // monitor buddy to quit broker if buddy is done
  self->monitor(buddy);

  self->configure_read(handle, caf::io::receive_policy::exactly(
    sizeof(uint64_t) + sizeof(int32_t)));

  // our message handlers
  return {
    [=](const caf::io::connection_closed_msg & msg) {
      // brokers can multiplex any number of connection, however, this example
      // assumes io_fsm to manage a broker with exactly one connection.
      if (msg.handle == handle) {
        caf::aout(self) << "connection closed" << endl;
        // force buddy to quit
        self->send_exit(buddy, caf::exit_reason::remote_link_unreachable);
        self->quit(caf::exit_reason::remote_link_unreachable);
      }
    },
    [=](caf::atom_value av, int32_t i) {
      assert(av == ping_atom::value || av == pong_atom::value);
      caf::aout(self) << "send {" << caf::to_string(av) << ", " << i << "}" << endl;
      // cast atom to its underlying type, i.e., uint64_t
      write_int(self, handle, static_cast<uint64_t>(av));
      write_int(self, handle, i);
    },
    [=](const caf::down_msg & dm) {
      if (dm.source == buddy) {
        caf::aout(self) << "our buddy is down" << endl;
        self->quit(dm.reason);
      }
    },
    [=](const caf::io::new_data_msg & msg) {
      // read the atom value as uint64_t from buffer
      uint64_t atom_value;
      read_int(msg.buf.data(), atom_value);
      // cast to original type
      auto atom = static_cast<caf::atom_value>(atom_value);
      // read integer value from buffer, jumping to the correct
      // position via offset_data(...)
      int32_t ival;
      read_int(msg.buf.data() + sizeof(uint64_t), ival);
      // show output
      caf::aout(self) << "received {" << caf::to_string(atom)
                      << ", " << ival << "}" << endl;
      // send composed message to our buddy
      self->send(buddy, atom, ival);
    },
    caf::others >> [=] {
      caf::aout(self) << "unexpected: "
                      << caf::to_string(self->current_message()) << endl;
    }
  };
}

caf::behavior server(caf::io::broker * self, const caf::actor & buddy) {

  caf::aout(self) << "server is running" << endl;
  return {
    [=](const caf::io::new_connection_msg & msg) {
      caf::aout(self) << "server accepted new connection" << endl;
      // by forking into a new broker, makes the new responsible for the
      // connection
      auto impl = self->fork(broker_impl, msg.handle, buddy);
      print_on_exit(impl, "broker impl");
//      caf::aout(self) << "quit server (only handles 1 connection)" << endl;
//      self->quit();
    },
    caf::others >> [=] {
      caf::aout(self) << "unexpected: "
                      << caf::to_string(self->current_message()) << endl;
    }
  };
}

caf::optional<uint16_t> as_u16(const std::string & str) {
  return static_cast<uint16_t>(std::stoul(str));
}

int main(int argc, char ** argv) {

  using std::string;
  uint16_t port = 0;
  string host = "localhost";
  auto res = caf::message_builder(argv + 1, argv + argc).extract_opts({
    { "server,s", "run in server mode" },
    { "client,c", "run in client mode" },
    { "port,p", "set port", port },
    { "host,H", "set host (client mode only)" }
  });
  if (! res.error.empty()) {
    cerr << res.error << endl;
    return 1;
  }
  if (res.opts.count("help") > 0) {
    cout << res.helptext << endl;
    return 0;
  }
  if (! res.remainder.empty()) {
    // not all cli arguments were consumed
    cerr << "*** too many arguments" << endl << res.helptext << endl;
    return 1;
  }
  if (res.opts.count("port") == 0) {
    cerr << "*** no port given" << endl << res.helptext << endl;
    return 1;
  }
  if (res.opts.count("server") > 0) {
    cout << "run in server mode" << endl;
    auto pong_actor = caf::spawn(pong);
    auto server_actor = caf::io::spawn_io_server(server, port, pong_actor);
    print_on_exit(server_actor, "server");
    print_on_exit(pong_actor, "pong");
  } else if (res.opts.count("client") > 0) {
    auto ping_actor = caf::spawn(ping, size_t{ 20 });
    auto io_actor = caf::io::spawn_io_client(broker_impl, host, port, ping_actor);
    print_on_exit(io_actor, "io_actor");
    print_on_exit(ping_actor, "pint");
  } else {
    cerr << "*** neither client nor server mode set" << endl << res.helptext << endl;
    return 1;
  }

  caf::await_all_actors_done();
  caf::shutdown();

  return 0;
}

