/**
 * Copyright 2016 Kareem Alkaseer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CLIENT_H_
#define CLIENT_H_

#include <asio/io_context.hpp>
#include <asio/read.hpp>
#include <asio/write.hpp>
#include <asio/buffer.hpp>
#include <asio/spawn.hpp>
#include <asio/ip/tcp.hpp>
#include <system_error>
#include <iostream>
#include <memory>
#include <string>
#include <chrono>
#include <thread>
#include <random>
#include <string>
#include <cerrno>
#include "sync_printer.h"
#include "logger.h"

#define NONCOPYABLE(type) \
  type(const type &) = delete; \
  const type & operator=(const type &) = delete;

using namespace asio;
using namespace asio::ip;

const int msg_size = 128;

class client
  : public std::enable_shared_from_this< client >
{
public:

  NONCOPYABLE(client);

private:

  using random_device = std::random_device;
  using random_engine = std::mt19937;
  using distribution = std::uniform_int_distribution<uint32_t>;
  using value_type = typename distribution::result_type;

  static logger lg;

public:

  client(tcp::socket socket, uint32_t id)
    : m_id(id),
      m_data(0),
      m_parse_error(false),
      m_sock(std::move(socket)),
      m_started(false),
      m_random_engine{ random_device{}() },
      m_distrib{ 0, 1023 }
  {
    LOG(info) << "client constructed with id " << id;
  }

  ~client() {
    stop();
  }

  void start(yield_context yield, tcp::endpoint & ep) {

    auto self(shared_from_this());

    m_started = true;
    m_sock.async_connect(ep, yield[m_ec]);
    if (m_ec) { error(); return; }
    LOG(info) << "client " << m_id << " started";
    do_write(yield);
  }

private:

  value_type next_value() {
    return m_distrib(m_random_engine);
  }

  void do_write(yield_context yield) {

    auto self(shared_from_this());

    if (!started()) return;
    std::string s = std::to_string(next_value());
    LOG(info) << "client " << m_id << " sent " << s;
    s += '\n';
    async_write(m_sock, buffer(s), yield[m_ec]);
    if (m_ec) { error(); return; }
    on_write(yield);
  }

  void on_write(yield_context yield) {
    do_read(yield);
  }

  void do_read(yield_context yield) {

    auto self(shared_from_this());

    size_t n = async_read(
      m_sock, buffer(m_read_buffer, 128),
      [this](const error_code & ec, size_t bytes) -> size_t {
        if (ec) return 0;
        // no buffering
        return std::find(
          m_read_buffer, m_read_buffer + bytes, '\n') < m_read_buffer + bytes ?
            0 : 1;
      },
      yield[m_ec]
    );
    if (m_ec) { error(); return; }
    on_read(yield, m_ec, n);
  }

  void on_read(yield_context yield, const std::error_code & ec, size_t bytes) {

    if (!ec && bytes > 0) {
      m_read_buffer[bytes - 1] = '\0';

      LOG(info) << "client " << m_id << " read buffer " << m_read_buffer;

      m_data = std::strtod(m_read_buffer, nullptr);
      if (errno == ERANGE) {
        parse_error();
        errno = 0;
      } else {
        clear_parse_error();
      }

      if (!m_parse_error)
        LOG(info) << "client " << m_id << " received " << m_data;
      else
        LOG(warning) << "client " << m_id << " parsing error";
    } else {
      LOG(warning) << m_id << " received malformed data";
    }
    if (ec) {
      stop();
    } else {
      spawn(
        m_sock.get_executor().context(),
        [&](yield_context yield) { do_write(yield); });
    }

  }

  void stop() {
    LOG(info) << "client " << m_id << " stop";
    if (!started()) return;
    m_started = false;
    m_sock.close();
  }

  bool started() { return m_started; }

  void error() {
    LOG(info) << "failed to connect (" << m_id << "): " << m_ec.message();
    stop();
  }

  void parse_error() { m_parse_error = true; }

  void clear_parse_error() { m_parse_error = false; }

  uint32_t m_id;
  double m_data;
  std::error_code m_ec;
  bool m_parse_error;
  tcp::socket m_sock;
  char m_read_buffer[msg_size];
  bool m_started;
  random_device m_random_device;
  random_engine m_random_engine;
  distribution m_distrib;
};

logger client::lg{ "client" };




#endif /* CLIENT_H_ */
