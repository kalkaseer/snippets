/**
 * Copyright 2016 Kareem Alkaseer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef BRANSON_AVL_H_
#define BRANSON_AVL_H_


#include <mutex>
#include <cassert>
#include <unordered_map>
#include <atomic>

#include "hash.h"
#include "hp.h"

namespace branson_avl {

enum function {
  update_if_present,
  update_if_absent
};

enum class direction : uint8_t {
  none,
  left,
  right,
};

enum result_t {
  FOUND,
  NOT_FOUND,
  RETRY
};

constexpr int spin_count = 100;

constexpr long begin_change(long ovl) { return ovl | 1; }

constexpr long end_change(long ovl) { return (ovl | 3) + 1; }

constexpr bool is_shrinking(long ovl) { return (ovl & 1) != 0; }

constexpr bool is_unlinked(long ovl) { return (ovl & 2) != 0; }

constexpr bool is_shrinking_or_linked(long ovl) { return (ovl & 3) != 0L; }

constexpr long unlinked_ovl = 2;

constexpr int unlink_required    = -1;
constexpr int rebalance_required = -2;
constexpr int nothing_required   = -3;

/* ************************************************************************
 * Class Set
 * ************************************************************************/
template<typename T, int Threads>
class set {

private:

  class node_t;

public:

  template< class NT >
  class iterator_t;

  using value_type = T;
  using value_ref = T &;
  using self_type = set<T, Threads>;
  using self_ptr = self_type *;
  using iterator = iterator_t< node_t >;
  using const_iterator = iterator_t< const node_t >;

  set(value_ref min);
  set(value_type && min);
  ~set();

  bool find(const T & value);
  bool find(T & value);

  bool insert(const T & value);
  bool insert(T & value);

  iterator iter();

  void attach_thread();

  void detach_thread();

private:

  using scoped_lock = std::lock_guard<std::mutex>;

  using node_type = node_t;
  using node_ptr = node_t *;

  node_ptr new_node(value_ref key);

  node_ptr new_node(
    int height, value_ref key, long version, bool value,
    node_ptr parent, node_ptr left, node_ptr right);

  result_t attempt_get(
    value_ref key, node_ptr node, direction dir, long nodeV);

  result_t update_under_root(
    value_ref key, function func, bool expected,
    bool newValue, node_ptr holder);

  result_t update_under_root(
    value_type && key, function func, bool expected,
    bool newValue, node_ptr holder);

  bool attempt_insert_into_empty(value_ref key, bool value, node_ptr holder);

  result_t attemp_update(
    value_ref key, function func, bool expected,
    bool newValue, node_ptr parent, node_ptr node, long nodeOVL);

  result_t attempt_node_update(
    function func, bool expected, bool newValue,
    node_ptr parent, node_ptr node);

  bool attempt_unlink(node_ptr parent, node_ptr node);

  void wait_until_shrinking_completed(node_ptr node);


  void fix_height_and_rebalance(node_ptr node);

  node_ptr rebalance(node_ptr nParent, node_ptr n);

  node_ptr rebalance_to_right(
    node_ptr nParent, node_ptr n, node_ptr nL, int hR0);

  node_ptr rebalance_to_left(
    node_ptr nParent, node_ptr n, node_ptr nR, int hL0);


  node_ptr rotate_left_over_right(
    node_ptr nParent, node_ptr n, int hL, node_ptr nR,
    node_ptr nRL, int hRR, int hRLR);

  node_ptr rotate_right_over_left(
    node_ptr nParent, node_ptr n, node_ptr nL, int hR,
    int hLL, node_ptr nLR, int hLRL);

  node_ptr rotate_left(
    node_ptr nParent, node_ptr n, int hL, node_ptr nR,
    node_ptr nRL, int hRL, int hRR);

  node_ptr rotate_right(
    node_ptr nParent, node_ptr n, node_ptr nL, int hR,
    int hLL, node_ptr nLR, int hLR);


  node_ptr bounded_min(
    node_ptr node, value_type * min_key, bool include);

  node_ptr bounded_max(
    node_ptr node, value_type * max_key, bool include);

  void publish(node_ptr ref);
  void release_all();

  static node_ptr fix_height(node_ptr node);

  static size_t height(node_ptr node);

  static int node_condition(node_ptr node);

  static direction find_direction(value_ref key, node_ptr node);

  static direction find_direction_if_set(value_ref key, node_ptr node);


  node_ptr m_root_holder;

  hazard_manager<node_type, Threads, 6> m_hazard;

  unsigned int m_current[Threads];


  /* **********************************************************************
   * Node class
   * **********************************************************************/
  struct node_t {

    using pointer = node_t *;

    int height;
    value_type key;
    long version;
    bool mark;
    pointer parent;
    pointer left;
    pointer right;
    std::mutex lock;

    pointer child(direction dir) {

      if (dir == direction::right) {
        return right;
      } else if (dir == direction::left) {
        return left;
      }

      assert(false);
    }

    // Node must be locked
    void set_child(direction dir, pointer child) {
      if (dir == direction::right) {
        right = child;
      } else if (dir == direction::left) {
        left = child;
      }
    }
  };

public:

  /* **********************************************************************
   * Iterator class: prototype (not conforming to standard iterators).
   * **********************************************************************/
  template< class NT >
  class iterator_t {

  public:

    using node_type = NT;
    using node_ptr = node_type *;
    using self_type = iterator_t< node_type >;
    using tree_type = set<T, Threads>;
    using tree_ptr = tree_type *;

  private:

    tree_ptr const m;
    bool descending;
    direction forward;
    direction reverse;
    std::unique_ptr<node_ptr[]> path;
    uint32_t depth;
    node_ptr most_recent_node;
    value_type * end_key;

  public:

    explicit iterator_t(tree_ptr p, bool last = false)
      : m{ p },
        descending{ false },
        forward{ direction::right },
        reverse{ direction::left },
        path{ },
        depth{ 0 },
        most_recent_node{ nullptr },
        end_key{ nullptr }
    {
      if (!last) {
        node_ptr root = m->m_root_holder->right;
        path = std::make_unique<node_ptr[]>(1 + height(root));
        push_first(root);
      }
    }

    iterator_t(
      tree_ptr p,
      value_type * min_key, bool include_min,
      value_type * max_key, bool include_max,
      bool descend
    ) : m{ p },
        descending{ descend },
        forward{ !descend ? direction::right : direction::left },
        reverse{ !descend ? direction::left : direction::right },
        path{ },
        depth{ 0 },
        most_recent_node{ nullptr },
        end_key{ nullptr }
    {
      value_type *from, *to;
      bool include_from, include_to;
      if (!descend) {
        include_from = include_min;
        include_to = include_max;
        from = min_key;
        to = max_key;
      } else {
        include_from = include_max;
        include_to = include_min;
        from = max_key;
        to = min_key;
      }

      node_ptr root = m->m_root_holder->right;
      if (to) {
        result_key result = m->bounded_extreme(
          min_key, include_min, max_key, include_max, forward);
        if (! result.key) {
          if (has_next()) most_recent_node = top();
          return;
        }
      }

      path = std::make_unique<node_ptr[]>(1 + height(root));

      if (! from) {
        push_first(root);
      } else {
        push_first(root, from, include_from);
        if (depth > 0 && !(top()->mark))
          advance();
      }
    }

    bool has_next() {
      return depth > 0;
    }

    const value_type & next() {
      assert(depth != 0);
      most_recent_node = top();
      advance();
      return most_recent_node->key;
    }

  private:

    int compare(value_type * k1, value_type * k2) {
      if (!descending)
        return *k1 < *k2 ? -1 : *k1 == *k2 ? 0 : 1;
      return *k1 < *k2 ? 1 : *k1 == *k2 ? 0 : -1;
    }

    void push_first(node_ptr node) {
      while (node) {
        path[depth] = node;
        ++depth;
        node = node->child(reverse);
      }
    }

    void push_first(node_ptr node, value_type * from_key, bool include) {
      while (node) {
        int c = compare(from_key, &(node->key));
        if (c > 0 || (c == 0 && !include)) {
          node = node->child(forward);
        } else {
          path[depth] = node;
          ++depth;
          if (c == 0)
            return;
          else
            node = node->child(reverse);
        }
      }
    }

    node_ptr top() { return path[depth - 1]; }

    void advance() {

      do {
        node_ptr t = top();
        if (end_key && *end_key == t->key) {
          depth = 0;
          path.reset();
          return;
        }

        node_ptr fwd = t->child(forward);
        if (fwd) {
          push_first(fwd);
        } else {
          node_ptr popped{ nullptr };
          do {
            popped = path[--depth];
          } while (depth > 0 && popped == top()->child(forward));
        }

        if (depth == 0) {
          path.reset();
          return;
        }
      } while (! top()->mark);
    }

  };

  /* **********************************************************************
   * ResultKey class
   * **********************************************************************/

  struct result_key {
    result_key()
      : result{ NOT_FOUND }, key{ nullptr }
    { }
    explicit result_key(result_t r, value_type * k)
      : result{ r }, key{ k }
    { }
    result_t result;
    value_type * key;
  };

  result_key attempt_extreme(direction dir, node_ptr node, long ovl);

  result_key extreme(direction dir);

  result_key bounded_extreme(
    value_type * min_key, bool include_min,
    value_type * max_key, bool include_max,
    direction dir
  );

};



template<typename T, int Threads>
typename set<T, Threads>::result_key
set<T, Threads>::attempt_extreme(
  direction dir, node_ptr node, long ovl
) {

  while (true) {
    node_ptr child = node->child(dir);
    if (! child) {
      if (node->version != ovl)
        return result_key{ RETRY, nullptr };

      assert (node->mark);

      return result_key{ FOUND, &(node->key) };
    } else {
      if (is_shrinking_or_linked(child->version)) {
        wait_until_shrinking_completed(child);
        if (node->version != ovl)
          return result_key{ RETRY, nullptr };
      } else if (child != node->child(dir)) {
        if (node->version != ovl)
          return result_key{ RETRY, nullptr };
      } else {
        if (node->version != ovl)
          return result_key{ RETRY, nullptr };
        result_key vo = attempt_extreme(dir, child, ovl);
        if (vo.result != RETRY)
          return std::move(vo);
      }
    }
  }
}

template<typename T, int Threads>
typename set<T, Threads>::result_key
set<T, Threads>::extreme(direction dir) {

  while (true) {
    node_ptr right = m_root_holder->right;
    if (! right) {
      return result_key{ NOT_FOUND, nullptr };
    } else {
      long version = right->version;

      if (is_shrinking_or_linked(version)) {
        wait_until_shrinking_completed(right);
      } else if (right == m_root_holder->right) {
        result_key vo = attempt_extreme(dir, right, version);
        if (vo.result != RETRY)
          return std::move(vo);
      }
    }
  }
}

template<typename T, int Threads>
typename set<T, Threads>::result_key
set<T, Threads>::bounded_extreme(
  value_type * min_key, bool include_min,
  value_type * max_key, bool include_max,
  direction dir
) {

  result_key result;
  if (
    (dir == direction::left && !min_key) ||
    (dir == direction::right && !max_key)
  ) {
    result = std::move(extreme(dir));
    if (result.result == NOT_FOUND)
      return std::move(result);
  } else {
    node_ptr root = m_root_holder;
    node_ptr node = dir == direction::left ?
      bounded_min(root->right, min_key, include_min) :
      bounded_max(root->right, max_key, include_max);
    if (! node)
      return std::move(result);
    result.key = &(node->key);
  }

  if (dir == direction::left && max_key) {
    if (*max_key < *result.key || (*max_key == *result.key && !include_max))
      return std::move(result);
  } else if (dir == direction::right && min_key) {
    if (*min_key > *result.key || (*min_key == *result.key && !include_min))
      return std::move(result);
  }

  result.result = FOUND;
  return std::move(result);
}

template<typename T, int Threads>
typename set<T, Threads>::node_ptr
set<T, Threads>::bounded_min(
  node_ptr node, value_type * min_key, bool include
) {

  while (node) {

    if (*min_key < node->key) {
      node_ptr n = bounded_min(node->left, min_key, include);
      if (n) return n;

      if (node->mark)
        return node;
    }

    if (node->mark && *min_key == node->key && include)
      return node;

    node = node->right;
  }
  return nullptr;
}

template<typename T, int Threads>
typename set<T, Threads>::node_ptr
set<T, Threads>::bounded_max(
  node_ptr node, value_type * max_key, bool include
) {

  while (node) {

    if (*max_key > node->key) {
      node_ptr n = bounded_max(node->right, max_key, include);
      if (n) return n;

      if (node->mark)
        return node;
    }

    if (node->mark && *max_key == node->key && include)
      return node;

    node = node->left;
  }
  return nullptr;
}


template<typename T, int Threads>
size_t set<T, Threads>::height(node_ptr node) {
  return !node ? 0 : node->height;
}

template<typename T, int Threads>
typename set<T, Threads>::node_ptr
set<T, Threads>::fix_height(node_ptr node) {

  int c = node_condition(node);

  switch(c) {
    case rebalance_required:
    case unlink_required:
      return node;
    case nothing_required:
      return nullptr;
    default:
      node->height = c;
      return node->parent;
  }
}

template<typename T, int Threads>
int set<T, Threads>::node_condition(node_ptr node) {

  node_ptr nL = node->left;
  node_ptr nR = node->right;

  if ((!nL || !nR) && !node->mark) {
      return unlink_required;
  }

  int hN = node->height;
  int hL0 = height(nL);
  int hR0 = height(nR);
  int hNRepl = 1 + std::max(hL0, hR0);
  int bal = hL0 - hR0;

  if (bal < -1 || bal > 1) {
    return rebalance_required;
  }

  return hN != hNRepl ? hNRepl : nothing_required;
}

template<typename T, int Threads>
set<T, Threads>::set(value_ref min) {
  m_root_holder = new_node(min);
  for (unsigned int i = 0; i < Threads; ++i)
    m_current[i] = 0;
}

template<typename T, int Threads>
set<T, Threads>::set(value_type && min)
{
  m_root_holder = new_node(min);
  for (unsigned int i = 0; i < Threads; ++i)
    m_current[i] = 0;
}

template<typename T, int Threads>
set<T, Threads>::~set() {
  m_hazard.release_node(m_root_holder);
}

template<typename T, int Threads>
void set<T, Threads>::publish(node_ptr ref) {
  m_hazard.publish(ref, m_current[m_hazard.tid()]);
  ++m_current[m_hazard.tid()];
}

template<typename T, int Threads>
void set<T, Threads>::release_all() {
  for (unsigned int i = 0; i < m_current[m_hazard.tid()]; ++i) {
    m_hazard.release(i);
  }
  m_current[m_hazard.tid()] = 0;
}

template<typename T, int Threads>
typename set<T, Threads>::node_ptr
set<T, Threads>::new_node(value_ref key) {
  return new_node(1, key, 0L, false, nullptr, nullptr, nullptr);
}

template<typename T, int Threads>
typename set<T, Threads>::node_ptr
set<T, Threads>::new_node(
  int height, value_ref key, long version, bool value,
  node_ptr parent, node_ptr left, node_ptr right
) {
  node_ptr node = m_hazard.get_free_node();

  node->height = height;
  node->key = key;
  node->version = version;
  node->mark = value;
  node->parent = parent;
  node->left = left;
  node->right = right;

  return node;
}

template<typename T, int Threads>
direction set<T, Threads>::find_direction(value_ref key, node_ptr node) {

  return key > node->key ?
    direction::right : key < node->key ?
      direction::left : node->mark ?
        direction::right : direction::none;
}

template<typename T, int Threads>
direction set<T, Threads>::find_direction_if_set(value_ref key, node_ptr node) {

  return key > node->key ?
    direction::right : key < node->key ?
      direction::left : direction::none;
}

template<typename T, int Threads>
bool set<T, Threads>::find(value_type & value) {
  return find(const_cast<value_type &>(value));
}

template<typename T, int Threads>
bool set<T, Threads>::find(const value_type & value) {

  value_ref key = hash(value);

  while(true) {

    node_ptr right = m_root_holder->right;

    if (!right) {
      return false;
    } else {
      direction dir = find_direction(key, right);

      int ovl = right->version;

      if (is_shrinking_or_linked(ovl)) {
        wait_until_shrinking_completed(right);
      } else if (right == m_root_holder->right) {
        result_t vo = attempt_get(key, right, dir, ovl);
        if (vo != RETRY) {
          return vo == FOUND;
        }
      }
    }
  }
}

template<typename T, int Threads>
typename set<T, Threads>::iterator set<T, Threads>::iter() {
  return iterator(this, &(m_root_holder->key), true, nullptr, false, false);
}

template<typename T, int Threads>
result_t set<T, Threads>::attempt_get(
  value_ref key, node_ptr node, direction dir, long nodeV
) {

  while (true) {

    node_ptr child = node->child(dir);

    if (!child) {
      if (node->version != nodeV) { return RETRY; }
      return NOT_FOUND;
    } else {
      direction child_dir = find_direction_if_set(key, child);
      if (child_dir == direction::none) {
        return child->mark ? FOUND : NOT_FOUND;
      }

      long childOVL = child->version;
      if (is_shrinking_or_linked(childOVL)) {
        wait_until_shrinking_completed(child);
        if (node->version != nodeV) {
          return RETRY;
        }
      } else if (child != node->child(dir)) {
        if (node->version != nodeV) {
          return RETRY;
        }
      } else {
        if (node->version != nodeV) {
          return RETRY;
        }

        result_t result = attempt_get(key, child, child_dir, childOVL);
        if (result != RETRY) {
          return result;
        }
      }
    }
  }
}

inline bool should_update(function func, bool prev, bool/* expected*/) {
    return func == update_if_absent ? !prev : prev;
}

inline result_t updateResult(function func, bool/* prev*/) {
    return func == update_if_absent ? NOT_FOUND : FOUND;
}

inline result_t no_update_result(function func, bool/* prev*/) {
    return func == update_if_absent ? FOUND : NOT_FOUND;
}

template<typename T, int Threads>
bool set<T, Threads>::insert(value_type & value) {
  return insert(const_cast<value_type &>(value));
}

template<typename T, int Threads>
bool set<T, Threads>::insert(const value_type & value) {
  return update_under_root(
    hash(value), update_if_absent, false, true, m_root_holder) == NOT_FOUND;
}

template<typename T, int Threads>
result_t set<T, Threads>::update_under_root(
  value_type && key, function func, bool expected, bool newValue, node_ptr holder
) {
  return update_under_root(key, func, expected, newValue, holder);
}

template<typename T, int Threads>
result_t set<T, Threads>::update_under_root(
  value_ref key, function func, bool expected, bool newValue, node_ptr holder
) {
  while(true) {
    node_ptr right = holder->right;

    if (!right) {
        if (!should_update(func, false, expected)) {
          return no_update_result(func, false);
        }

        if (!newValue || attempt_insert_into_empty(key, newValue, holder)) {
          return updateResult(func, false);
        }

    } else {

      long ovl = right->version;

      if (is_shrinking_or_linked(ovl)) {
        wait_until_shrinking_completed(right);
      } else if (right == holder->right) {
        result_t vo = attemp_update(key, func, expected, newValue, holder, right, ovl);
        if (vo != RETRY) {
          return vo;
        }
      }
    }
  }
}

template<typename T, int Threads>
bool set<T, Threads>::attempt_insert_into_empty(
  value_ref key, bool value, node_ptr holder
) {
  publish(holder);
  scoped_lock lock(holder->lock);

  if (!holder->right) {
    holder->right = new_node(1, key, 0, value, holder, nullptr, nullptr);
    holder->height = 2;
    release_all();
    return true;
  } else {
    release_all();
    return false;
  }
}

template<typename T, int Threads>
result_t set<T, Threads>::attemp_update(
  value_ref key, function func, bool expected, bool newValue,
  node_ptr parent, node_ptr node, long nodeOVL
) {
  direction dir = find_direction_if_set(key, node);
  if (dir == direction::none) {
    return attempt_node_update(func, expected, newValue, parent, node);
  }

  while(true) {

    node_ptr child = node->child(dir);

    if (node->version != nodeOVL) {
      return RETRY;
    }

    if (!child) {
      if (!newValue) {
        return NOT_FOUND;
      } else {
        bool success;
        node_ptr damaged;

        {
          publish(node);
          scoped_lock lock(node->lock);

          if (node->version != nodeOVL) {
            release_all();
            return RETRY;
          }

          if (node->child(dir)) {
            success = false;
            damaged = nullptr;
          } else {
            if (!should_update(func, false, expected)) {
              release_all();
              return no_update_result(func, false);
            }

            node_ptr node_child = new_node(1, key, 0, true, node, nullptr, nullptr);
            node->set_child(dir, node_child);

            success = true;
            damaged = fix_height(node);
          }

          release_all();
        }

        if (success) {
          fix_height_and_rebalance(damaged);
          return updateResult(func, /*dummy*/ false);
        }
      }

    } else {
      long childOVL = child->version;

      if (is_shrinking_or_linked(childOVL)) {
        wait_until_shrinking_completed(child);
      } else if (child != node->child(dir)) {
        // RETRY
      } else {
        if (node->version != nodeOVL) {
          return RETRY;
        }

        result_t vo = attemp_update(
          key, func, expected, newValue, node, child, childOVL);
        if (vo != RETRY) {
          return vo;
        }
      }
    }

  }
}

template<typename T, int Threads>
result_t set<T, Threads>::attempt_node_update(
  function func, bool expected, bool newValue, node_ptr parent, node_ptr node
) {
  if (!newValue) {
    if (!node->mark) {
      return NOT_FOUND;
    }
  }

  if (!newValue && (!node->left || !node->right)) {
    bool prev;
    node_ptr damaged;

    {
      publish(parent);
      scoped_lock parentLock(parent->lock);

      if (is_unlinked(parent->version) || node->parent != parent) {
        release_all();
        return RETRY;
      }

      {
          publish(node);
          scoped_lock lock(node->lock);

          prev = node->mark;

          if (!should_update(func, prev, expected)) {
            release_all();
            return no_update_result(func, prev);
          }

          if (!prev) {
            release_all();
            return updateResult(func, prev);
          }

          if (!attempt_unlink(parent, node)) {
            release_all();
            return RETRY;
          }
      }

      release_all();

      damaged = fix_height(parent);
    }

    fix_height_and_rebalance(damaged);

    return updateResult(func, prev);

  } else {

    publish(node);
    scoped_lock lock(node->lock);

    if (is_unlinked(node->version)) {
      release_all();
      return RETRY;
    }

    bool prev = node->mark;
    if (! should_update(func, prev, expected)) {
      release_all();
      return no_update_result(func, prev);
    }

    if (!newValue && (!node->left || !node->right)) {
      release_all();
      return RETRY;
    }

    node->mark = newValue;

    release_all();
    return updateResult(func, prev);
  }
}

template<typename T, int Threads>
void set<T, Threads>::wait_until_shrinking_completed(node_ptr node) {
  long version = node->version;

  if (is_shrinking(version)) {
    for (int i = 0; i < spin_count; ++i) {
      if (version != node->version) {
        return;
      }
    }

    node->lock.lock();
    node->lock.unlock();
  }
}

template<typename T, int Threads>
bool set<T, Threads>::attempt_unlink(node_ptr parent, node_ptr node) {

  node_ptr parentL = parent->left;
  node_ptr parentR = parent->right;

  if (parentL != node && parentR != node) {
    return false;
  }

  node_ptr left = node->left;
  node_ptr right = node->right;

  if (left && right) {
    return false;
  }

  node_ptr splice = left ? left : right;

  if (parentL == node) {
    parent->left = splice;
  } else {
    parent->right = splice;
  }

  if (splice) {
    splice->parent = parent;
  }

  node->version = unlinked_ovl;
  node->mark = false;

  m_hazard.release_node(node);

  return true;
}

template<typename T, int Threads>
void set<T, Threads>::fix_height_and_rebalance(node_ptr node) {

  while (node && node->parent) {

    int condition = node_condition(node);
    if (condition == nothing_required || is_unlinked(node->version))
      return;

    if (condition != unlink_required && condition != rebalance_required) {

      publish(node);
      scoped_lock lock(node->lock);

      node = fix_height(node);

      release_all();

    } else {

      node_ptr nParent = node->parent;
      publish(nParent);
      scoped_lock lock(nParent->lock);

      if (!is_unlinked(nParent->version) && node->parent == nParent) {
        publish(node);
        scoped_lock node_lock(node->lock);
        node = rebalance(nParent, node);
      }

      release_all();
    }
  }
}

template<typename T, int Threads>
typename set<T, Threads>::node_ptr
set<T, Threads>::rebalance(node_ptr nParent, node_ptr n) {

  node_ptr nL = n->left;
  node_ptr nR = n->right;

  if ((!nL || !nR) && !n->mark) {
    if (attempt_unlink(nParent, n)) {
      return fix_height(nParent);
    } else {
      return n;
    }
  }

  int hN = n->height;
  int hL0 = height(nL);
  int hR0 = height(nR);
  int hNRepl = 1 + std::max(hL0, hR0);
  int bal = hL0 - hR0;

  if (bal > 1) {
    return rebalance_to_right(nParent, n, nL, hR0);
  } else if (bal < -1) {
    return rebalance_to_left(nParent, n, nR, hL0);
  } else if (hNRepl != hN) {
    n->height = hNRepl;
    return fix_height(nParent);
  } else {
    return nullptr;
  }
}

template<typename T, int Threads>
typename set<T, Threads>::node_ptr
set<T, Threads>::rebalance_to_right(
  node_ptr nParent, node_ptr n, node_ptr nL, int hR0
) {
  publish(nL);
  scoped_lock lock(nL->lock);

  int hL = nL->height;
  if (hL - hR0 <= 1) {
      return n;
  } else {
    publish(nL->right);
    node_ptr nLR = nL->right;

    int hLL0 = height(nL->left);
    int hLR0 = height(nLR);

    if (hLL0 > hLR0) {
      return rotate_right(nParent, n, nL, hR0, hLL0, nLR, hLR0);
    } else {
      {
        if (reinterpret_cast<long>(&nLR->lock) == 0x30) {
          return n;
        }
        scoped_lock subLock(nLR->lock);

        int hLR = nLR->height;
        if (hLL0 >= hLR) {
          return rotate_right(nParent, n, nL, hR0, hLL0, nLR, hLR);
        } else {
          int hLRL = height(nLR->left);
          int b = hLL0 - hLRL;
          if (b >= -1 && b <= 1) {
              return rotate_right_over_left(nParent, n, nL, hR0, hLL0, nLR, hLRL);
          }
        }
      }

      return rebalance_to_left(n, nL, nLR, hLL0);
    }
  }
}

template<typename T, int Threads>
typename set<T, Threads>::node_ptr
set<T, Threads>::rebalance_to_left(
  node_ptr nParent, node_ptr n, node_ptr nR, int hL0
) {
  publish(nR);
  scoped_lock lock(nR->lock);

  int hR = nR->height;
  if (hL0 - hR >= -1) {
    return n;
  } else {
    node_ptr nRL = nR->left;
    int hRL0 = height(nRL);
    int hRR0 = height(nR->right);

    if (hRR0 >= hRL0) {
      return rotate_left(nParent, n, hL0, nR, nRL, hRL0, hRR0);
    } else {
      {
        publish(nRL);
        scoped_lock subLock(nRL->lock);

        int hRL = nRL->height;
        if (hRR0 >= hRL) {
          return rotate_left(nParent, n, hL0, nR, nRL, hRL, hRR0);
        } else {
          int hRLR = height(nRL->right);
          int b = hRR0 - hRLR;
          if (b >= -1 && b <= 1) {
            return rotate_left_over_right(nParent, n, hL0, nR, nRL, hRR0, hRLR);
          }
        }
      }

      return rebalance_to_right(n, nR, nRL, hRR0);
    }
  }
}

template<typename T, int Threads>
typename set<T, Threads>::node_ptr
set<T, Threads>::rotate_right(
  node_ptr nParent, node_ptr n, node_ptr nL, int hR,
  int hLL, node_ptr nLR, int hLR
) {
  long nodeOVL = n->version;
  node_ptr nPL = nParent->left;
  n->version = begin_change(nodeOVL);

  n->left = nLR;
  if (nLR) {
    nLR->parent = n;
  }

  nL->right = n;
  n->parent = nL;

  if (nPL == n) {
    nParent->left = nL;
  } else {
    nParent->right = nL;
  }
  nL->parent = nParent;

  int hNRepl = 1 + std::max(hLR, hR);
  n->height = hNRepl;
  nL->height = 1 + std::max(hLL, hNRepl);

  n->version = end_change(nodeOVL);

  int balN = hLR - hR;
  if (balN < -1 || balN > 1) {
    return n;
  }

  int balL = hLL - hNRepl;
  if (balL < -1 || balL > 1) {
    return nL;
  }

  return fix_height(nParent);
}

template<typename T, int Threads>
typename set<T, Threads>::node_ptr
set<T, Threads>::rotate_left(
  node_ptr nParent, node_ptr n, int hL, node_ptr nR,
  node_ptr nRL, int hRL, int hRR
) {
  long nodeOVL = n->version;
  node_ptr nPL = nParent->left;
  n->version = begin_change(nodeOVL);

  n->right = nRL;
  if (nRL) {
    nRL->parent = n;
  }

  nR->left = n;
  n->parent = nR;

  if (nPL == n) {
    nParent->left = nR;
  } else {
    nParent->right = nR;
  }
  nR->parent = nParent;

  int hNRepl = 1 + std::max(hL, hRL);
  n->height = hNRepl;
  nR->height = 1 + std::max(hNRepl, hRR);

  n->version = end_change(nodeOVL);

  int balN = hRL - hL;
  if (balN < -1 || balN > 1) {
    return n;
  }

  int balR = hRR - hNRepl;
  if (balR < -1 || balR > 1) {
    return nR;
  }

  return fix_height(nParent);
}

template<typename T, int Threads>
typename set<T, Threads>::node_ptr
set<T, Threads>::rotate_right_over_left(
  node_ptr nParent, node_ptr n, node_ptr nL, int hR,
  int hLL, node_ptr nLR, int hLRL
) {

  long nodeOVL = n->version;
  long leftOVL = nL->version;

  node_ptr nPL = nParent->left;
  node_ptr nLRL = nLR->left;
  node_ptr nLRR = nLR->right;
  int hLRR = height(nLRR);

  n->version = begin_change(nodeOVL);
  nL->version = begin_change(leftOVL);

  n->left = nLRR;
  if (nLRR) {
      nLRR->parent = n;
  }

  nL->right = nLRL;
  if (nLRL) {
    nLRL->parent = nL;
  }

  nLR->left = nL;
  nL->parent = nLR;
  nLR->right = n;
  n->parent = nLR;

  if (nPL == n) {
    nParent->left = nLR;
  } else {
    nParent->right = nLR;
  }
  nLR->parent = nParent;

  int hNRepl = 1 + std::max(hLRR, hR);
  n->height = hNRepl;

  int hLRepl = 1 + std::max(hLL, hLRL);
  nL->height = hLRepl;

  nLR->height = 1 + std::max(hLRepl, hNRepl);

  n->version = end_change(nodeOVL);
  nL->version = end_change(leftOVL);

  int balN = hLRR - hR;
  if (balN < -1 || balN > 1) {
    return n;
  }

  int balLR = hLRepl - hNRepl;
  if (balLR < -1 || balLR > 1) {
    return nLR;
  }

  return fix_height(nParent);
}

template<typename T, int Threads>
typename set<T, Threads>::node_ptr
set<T, Threads>::rotate_left_over_right(
  node_ptr nParent, node_ptr n, int hL, node_ptr nR,
  node_ptr nRL, int hRR, int hRLR
) {

  long nodeOVL = n->version;
  long rightOVL = nR->version;

  n->version = begin_change(nodeOVL);
  nR->version = begin_change(rightOVL);

  node_ptr nPL = nParent->left;
  node_ptr nRLL = nRL->left;
  node_ptr nRLR = nRL->right;
  int hRLL = height(nRLL);

  n->right = nRLL;
  if (nRLL) {
    nRLL->parent = n;
  }

  nR->left = nRLR;
  if (nRLR) {
    nRLR->parent = nR;
  }

  nRL->right = nR;
  nR->parent = nRL;
  nRL->left = n;
  n->parent = nRL;

  if (nPL == n) {
    nParent->left = nRL;
  } else {
    nParent->right = nRL;
  }
  nRL->parent = nParent;

  int hNRepl = 1 + std::max(hL, hRLL);
  n->height = hNRepl;
  int hRRepl = 1 + std::max(hRLR, hRR);
  nR->height = hRRepl;
  nRL->height = 1 + std::max(hNRepl, hRRepl);

  n->version = end_change(nodeOVL);
  nR->version = end_change(rightOVL);

  int balN = hRLL - hL;
  if (balN < -1 || balN > 1) {
    return n;
  }

  int balRL = hRRepl - hNRepl;
  if (balRL < -1 || balRL > 1) {
    return nRL;
  }

  return fix_height(nParent);
}

template<typename T, int Threads>
void set<T, Threads>::attach_thread() {
  m_hazard.attach_thread();
}

template<typename T, int Threads>
void set<T, Threads>::detach_thread() {
  m_hazard.detach_thread();
}

} //end of avltree


#endif /* BRANSON_AVL_H_ */

