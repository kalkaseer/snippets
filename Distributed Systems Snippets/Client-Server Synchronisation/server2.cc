/**
 * Copyright 2016 Kareem Alkaseer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * $COMPILER server.cc -o server -std=c++1y \
 *   -I/Users/kalkaseer/Sources/asio/include \
 *   -I/Users/kalkaseer/Sources/cds-2.1.0 \
 *   -L/opt/local/lib -lboost_system-mt -lboost_coroutine-mt  \
 *   -lboost_thread-mt -lboost_filesystem-mt \
 *   -lboost_log_setup-mt -lboost_log-mt -DBOOST_LOG_DYN_LINK
 */
#include <thread>
#include "server2.h" // or "server1.h

int main() {

  server srv{ "0.0.0.0", "8001" } ;
  auto work = std::make_shared<io_context::work>(srv.io());

  std::thread thread([&srv](){ srv.run(); });

  work.reset();
  thread.join();
  return 0;
}


