/**
 * Copyright 2016 Kareem Alkaseer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LOG_H_
#define LOG_H_

#include <boost/log/core.hpp>
#include <boost/log/trivial.hpp>
#include <boost/log/expressions.hpp>
#include <boost/log/sinks/text_file_backend.hpp>
#include <boost/log/utility/setup/file.hpp>
#include <boost/log/utility/setup/common_attributes.hpp>
#include <boost/log/sources/severity_logger.hpp>
#include <boost/log/sources/record_ostream.hpp>
#include <boost/log/utility/setup/console.hpp>

#define LOG(lvl) BOOST_LOG_SEV(lg(), logger::lvl)

namespace logging = boost::log;
namespace src = boost::log::sources;
namespace sinks = boost::log::sinks;
namespace keywords = boost::log::keywords;

class logger {

public:

  enum severity_level {
    trace   = logging::trivial::trace,
    debug   = logging::trivial::debug,
    info    = logging::trivial::info,
    warning = logging::trivial::warning,
    error   = logging::trivial::error,
    fatal   = logging::trivial::fatal,
  };

  using log_t = src::severity_logger_mt< severity_level >;

  logger(std::string && prefix) { init(prefix); }

  explicit operator log_t&() {
    return lg;
  }

  log_t & operator()() { return lg; }

  void init(const std::string & prefix) {

    if (prefix != "") {
      logging::add_file_log (
        keywords::file_name = prefix + "_%N.log",
        keywords::rotation_size = 10 * 1024 * 1024,
        keywords::time_based_rotation = sinks::file::rotation_at_time_point(0, 0, 0),
        keywords::format = "[%TimeStamp%]: %Message%"
      );
    }

//    logging::core::get()->set_filter(
//      logging::trivial::severity >= logging::trivial::info);

    logging::add_common_attributes();
    logging::add_console_log();
  }

  log_t lg;
};


#endif /* LOG_H_ */
