/*
 * hp.h
 *
 *  Created on: Aug 1, 2016
 *      Author: kalkaseer
 */

#ifndef HP_H_
#define HP_H_

#include <cassert>

//#define DEBUG

#include <list>
#include <array>
#include <algorithm>
#include <iostream>

template<
  typename Node,
  unsigned int Threads,
  unsigned int Size = 2,
  unsigned int Prefill = 50
>
class hazard_manager {

public:

  hazard_manager();
  ~hazard_manager();

  hazard_manager(const hazard_manager& rhs) = delete;
  hazard_manager& operator=(const hazard_manager& rhs) = delete;

  void release_node(Node* node);

  void safe_release_node(Node* node);

  Node* get_free_node();

  void publish(Node* node, unsigned int i);

  void release(unsigned int i);

  void release_all();

  std::list<Node * > & direct_free(unsigned int t);

  std::list<Node * > & direct_local(unsigned int t);

  void attach_thread();

  void detach_thread();

  unsigned int tid();

private:

  std::array<std::array<Node*, Size>, Threads> m_pointers;
  std::array<std::list<Node*>, Threads> m_local_queues;
  std::array<std::list<Node*>, Threads> m_free_queues;

  std::unordered_map<std::thread::id, unsigned int> tid_map;

  bool isReferenced(Node* node);

  static_assert(Threads > 0, "Number of threads must be greater than 0");
  static_assert(Size > 0, "Number of hazard pointers must greater than 0");
};

template<
  typename Node,
  unsigned int Threads,
  unsigned int Size,
  unsigned int Prefill
>
hazard_manager<Node, Threads, Size, Prefill>::hazard_manager() {
  for(unsigned int tid = 0; tid < Threads; ++tid) {
    for(unsigned int j = 0; j < Size; ++j) {
      #ifdef DEBUG
      m_pointers.at(tid).at(j) = nullptr;
      #else
      m_pointers[tid][j] = nullptr;
      #endif
    }

    if(Prefill > 0) {
      for(unsigned int i = 0; i < Prefill; i++) {
        #ifdef DEBUG
        m_free_queues.at(tid).push_back(new Node());
        #else
        m_free_queues[tid].push_back(new Node());
        #endif
      }
    }
  }
}

template<
  typename Node,
  unsigned int Threads,
  unsigned int Size,
  unsigned int Prefill
>
hazard_manager<Node, Threads, Size, Prefill>::~hazard_manager() {

  for(unsigned int tid = 0; tid < Threads; ++tid) {

    #ifdef DEBUG

    while(!m_local_queues.at(tid).empty()){
      delete m_local_queues.at(tid).front();
      m_local_queues.at(tid).pop_front();
    }

    while(!m_free_queues.at(tid).empty()){
      delete m_free_queues.at(tid).front();
      m_free_queues.at(tid).pop_front();
    }

    #else

    while(!m_local_queues[tid].empty()){
      delete m_local_queues[tid].front();
      m_local_queues[tid].pop_front();
    }

    while(!m_free_queues[tid].empty()){
      delete m_free_queues[tid].front();
      m_free_queues[tid].pop_front();
    }
    #endif
  }
}

template<
  typename Node,
  unsigned int Threads,
  unsigned int Size,
  unsigned int Prefill
>
std::list< Node * > & hazard_manager<Node, Threads, Size, Prefill>::direct_free(
  unsigned int t
){
  #ifdef DEBUG
  return m_free_queues.at(t);
  #else
  return m_free_queues[t];
  #endif
}

template<
  typename Node,
  unsigned int Threads,
  unsigned int Size,
  unsigned int Prefill
>
std::list< Node * > & hazard_manager<Node, Threads, Size, Prefill>::direct_local(
  unsigned int t
){
  #ifdef DEBUG
  return m_local_queues.at(t);
  #else
  return m_local_queues[t];
  #endif
}

template<
  typename Node,
  unsigned int Threads,
  unsigned int Size,
  unsigned int Prefill
>
void hazard_manager<Node, Threads, Size, Prefill>::safe_release_node(Node * node) {
  unsigned int thread_num = tid_map[std::this_thread::get_id()];
  if(node) {
    if(std::find(
        m_local_queues.at(thread_num).begin(),
        m_local_queues.at(thread_num).end(),
        node
      ) != m_local_queues.at(thread_num).end()
    ) {
      return;
    }

    m_local_queues.at(thread_num).push_back(node);
  }
}

template<
  typename Node,
  unsigned int Threads,
  unsigned int Size,
  unsigned int Prefill
>
void hazard_manager<Node, Threads, Size, Prefill>::release_node(Node * node) {

  unsigned int thread_num = tid_map[std::this_thread::get_id()];
  if(node) {
    #ifdef DEBUG
    m_local_queues.at(thread_num).push_back(node);
    #else
    m_local_queues[thread_num].push_back(node);
    #endif
  }
}

template<
  typename Node,
  unsigned int Threads,
  unsigned int Size,
  unsigned int Prefill
>
Node * hazard_manager<Node, Threads, Size, Prefill>::get_free_node() {

  unsigned int tid = tid_map[std::this_thread::get_id()];

#ifdef DEBUG
  //First, try to get a free node from the free queue
  if(!m_free_queues.at(tid).empty()){
    Node* free = m_free_queues.at(tid).front();
    FreeQueues.at(tid).pop_front();
    return free;
  }

  //If there are enough local nodes, move then to the free queue
  if(m_local_queues.at(tid).size() > (Size + 1) * Threads){
    typename std::list<Node*>::iterator it = m_local_queues.at(tid).begin();
    typename std::list<Node*>::iterator end = m_local_queues.at(tid).end();

    while(it != end){
      if(!isReferenced(*it)){
        m_free_queues.at(tid).push_back(*it);
        it = m_local_queues.at(tid).erase(it);
      } else {
        ++it;
      }
    }

    Node* free = m_free_queues.at(tid).front();
    m_free_queues.at(tid).pop_front();

    return free;
  }
#else

  if(!m_free_queues[tid].empty()){
    Node* free = m_free_queues[tid].front();
    m_free_queues[tid].pop_front();
    return free;
  }

  if(m_local_queues[tid].size() > (Size + 1) * Threads){
    typename std::list<Node*>::iterator it = m_local_queues[tid].begin();
    typename std::list<Node*>::iterator end = m_local_queues[tid].end();

    while(it != end){
      if(!isReferenced(*it)) {
        m_free_queues[tid].push_back(*it);
        it = m_local_queues[tid].erase(it);
      } else {
        ++it;
      }
    }

    Node* free = m_free_queues[tid].front();
    m_free_queues[tid].pop_front();

    return free;
  }
  #endif

  //There was no way to get a free node, allocate a new one
  return new Node();
}

template<
typename Node,
unsigned int Threads,
unsigned int Size,
unsigned int Prefill
>
bool hazard_manager<Node, Threads, Size, Prefill>::isReferenced(Node * node){

#ifdef DEBUG
for(unsigned int tid = 0; tid < Threads; ++tid)
  for(unsigned int i = 0; i < Size; ++i)
    if(m_pointers.at(tid).at(i) == node)
      return true;
#else
for(unsigned int tid = 0; tid < Threads; ++tid)
  for(unsigned int i = 0; i < Size; ++i)
    if(m_pointers[tid][i] == node)
      return true;
#endif

return false;
}

template<
  typename Node,
  unsigned int Threads,
  unsigned int Size,
  unsigned int Prefill
>
void hazard_manager<Node, Threads, Size, Prefill>::publish(
  Node* node, unsigned int i
) {
  unsigned int thread_num = tid_map[std::this_thread::get_id()];
  #ifdef DEBUG
  m_pointers.at(thread_num).at(i) = node;
  ]#else
  m_pointers[thread_num][i] = node;
  #endif
}

template<
  typename Node,
  unsigned int Threads,
  unsigned int Size,
  unsigned int Prefill
>
void hazard_manager<Node, Threads, Size, Prefill>::release(unsigned int i) {
  unsigned int thread_num = tid_map[std::this_thread::get_id()];
  #ifdef DEBUG
  m_pointers.at(thread_num).at(i) = nullptr;
  #else
  m_pointers[thread_num][i] = nullptr;
  #endif
}

template<
  typename Node,
  unsigned int Threads,
  unsigned int Size,
  unsigned int Prefill
>
void hazard_manager<Node, Threads, Size, Prefill>::release_all() {
  unsigned int thread_num = tid_map[std::this_thread::get_id()];
  #ifdef DEBUG
  for(unsigned int i = 0; i < Size; ++i){
    m_pointers.at(thread_num).at(i) = nullptr;
  }
  #else
  for(unsigned int i = 0; i < Size; ++i){
    m_pointers[thread_num][i] = nullptr;
  }
  #endif
}

template<
  typename Node,
  unsigned int Threads,
  unsigned int Size,
  unsigned int Prefill
>
void hazard_manager<Node, Threads, Size, Prefill>::attach_thread() {
  static unsigned int current_increment = 0;
  static std::mutex m;
  std::lock_guard<std::mutex> guard(m);
  tid_map[std::this_thread::get_id()] = current_increment;
  ++current_increment;
}

template<
  typename Node,
  unsigned int Threads,
  unsigned int Size,
  unsigned int Prefill
>
void hazard_manager<Node, Threads, Size, Prefill>::detach_thread() {
  tid_map.erase(std::this_thread::get_id());
}


template<
  typename Node,
  unsigned int Threads,
  unsigned int Size,
  unsigned int Prefill
>
unsigned int hazard_manager<Node, Threads, Size, Prefill>::tid() {
  return tid_map[std::this_thread::get_id()];
}


#endif /* HP_H_ */
