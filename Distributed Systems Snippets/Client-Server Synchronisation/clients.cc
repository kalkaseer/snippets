/**
 * Copyright 2016 Kareem Alkaseer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * c++ clients.cc -o clients -std=c++1y \
 *   -I/Users/kalkaseer/Sources/asio/include \
 *   -L/opt/local/lib \
 *   -lboost_system-mt -lboost_coroutine-mt -lboost_log-mt \
 *   -lboost_thread-mt -lboost_filesystem-mt \
 *   -lboost_log_setup-mt -DBOOST_LOG_DYN_LINK
 */

#include <thread>
#include "client.h"

int main (int argc, char * argv[]) {

  io_context iocontext{ 30 };
  auto work = std::make_shared<io_context::work>(iocontext);

  tcp::endpoint endpoint{ address::from_string("127.0.0.1"), 8001 };

  std::vector<std::thread> threads;
  std::vector<std::shared_ptr<client>> clients;

  // use thread and work to simulate delay, this is totally artificial
  // However, this paradigm actually makes the io loop totally independent
  // from the main program
  std::thread th([&iocontext](){ iocontext.run(); });


  for (uint32_t i = 0; i < 30; ++i) {
    threads.emplace_back(
      [&] {
        spawn(
          iocontext,
          [&](yield_context yield) {
            clients.push_back(
              std::make_shared<client>(tcp::socket(iocontext), i));
            clients.back()->start(yield, endpoint);
          });
      });
    std::this_thread::sleep_for(std::chrono::milliseconds(10));
  }

  for (auto & t : threads)
    t.join();

  work.reset();
  th.join();

  return 0;
}



