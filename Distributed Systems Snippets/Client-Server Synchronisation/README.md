Client-Server Synchronisation

Client implementation is the same for the three server types.

‣ Dependencies: Asio (standalone), Boost.System, Boost.Coroutine, Boost.Thread 
(Boost.Log requirement, threading in the code is done through std::thread), 
Boost.Filesystem Boost.LogSetup, Boost.Log

‣ There are three types of servers provided. The main reason is the that the problem definition would differ according to whether the average should be calculated as a snapshot when the request was made. This would require fine-grained locking. But if the average is calculated online over the map, it is more like streaming.

1. [Sever 1] Multi-threaded reactor-based server. Coroutines are used but they
are not essential, the design could be changed to asynchronous callbacks as
desired. This server is uses fine- grained locking. Saving the tree is done by
a single consumer single producer queue. The queue is could be made lock-free
using multiple implementations including Boost.Lockfree or Visual Studio PPL.

2. [Sever 2] Single-threaded reactor-based server with sequenced coroutines for 
request processing. Saving the tree to file is done by having two channels: 
primary and secondary. Clients write to the primary channel by default but when
the save is to take place, the channels are switched. The clients then write to
the secondary channel and the tree writer reads from the primary channel. When
the tree writer is done, it switches the channels again and inserts the data
written by clients to the secondary channel into the primary channel. This has
the effect of lowering copying data around. This server leans towards stream
processing.

3. [Sever 3] Multi-threaded reactor-based server with optimistic AVL.
The optimistic AVL used is an (incomplete) Bronson et al. optimistic concurrency
version control self-balancing binary tree as described in A Practical 
Concurrent Binary Search Tree published in PPoPP’10. Saving and averaging 
the tree is done in a streaming manner. The implementation is non-blocking 
lock-free. An almost complete implementation of this tree exists in libcds. 
However, iterators are missing. Implementing the class’s iterators is viable. 
Another option for a concurrent AVL is a logical ordering tree.