/**
 * Copyright 2016 Kareem Alkaseer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef BOUNDED_BUFFER_H_
#define BOUNDED_BUFFER_H_

#include <mutex>

template< class T >
class bounded_buffer {

public:

  explicit bounded_buffer(std::size_t capacity)
    : m_capacity(capacity), m_count(0), m_front(0), m_rear(0)
  {
    m_buffer = new T[capacity];
  }

  ~bounded_buffer() {
    delete [] m_buffer;
  }

  void put(T value) {

    unique_lock ulock(m_lock);

    m_not_full.wait(ulock, [this]{ return m_count != m_capacity; });

    m_buffer[m_rear] = value;
    m_rear = (m_rear + 1) % m_capacity;
    ++m_count;

    m_not_empty.notify_one();
  }

  T get() {

    unique_lock ulock(m_lock);

    m_not_empty.wait(ulock, [this]{ return m_count != 0; });

    T value = m_buffer[m_front];
    m_front = (m_front + 1) % m_capacity;
    --m_count;

    m_not_full.notify_one();

    return value;
  }

private:

  using unique_lock = std::unique_lock<std::mutex>;

  T * m_buffer;
  std::size_t m_capacity;
  std::size_t m_count;
  std::size_t m_front;
  std::size_t m_rear;
  std::condition_variable m_not_full;
  std::condition_variable m_not_empty;
  std::mutex m_lock;
};


#endif /* BOUNDED_BUFFER_H_ */
