/**
 * Copyright 2016 Kareem Alkaseer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef SERVER2_H_
#define SERVER2_H_

#include <memory>
#include <system_error>
#include <mutex>
#include <chrono>
#include <set>
#include <numeric>
#include <atomic>
#include <unordered_set>
#include <cstdlib>
#include <fstream>
#include <cstdio>
#include "asio/io_context.hpp"
#include "asio/spawn.hpp"
#include "asio.hpp"
#include "logger.h"
#include "bounded_buffer.h"
#include <boost/date_time/posix_time/posix_time.hpp>

#include <limits>

#define non_copyable(type)                       \
  type(const type &) = delete;                   \
  type(type &&) = delete;                        \
  type & operator=(const type &) = delete;       \
  type & operator=(type &&) = delete

using namespace asio;

class connection;

using connection_ptr = std::shared_ptr< connection >;

class tree {

public:

  using set = std::set<uint32_t>;
  using value_type = typename set::value_type;
  using iterator = set::iterator;
  using const_iterator = set::const_iterator;

  tree() { };

  std::pair<iterator, bool> insert(const value_type & v) {
    return m_set.insert(v);
  }

  std::pair<iterator, bool> insert(value_type && v) {
    return m_set.insert(std::forward<value_type>(v));
  }

  double average() {
    double x = 0;
    uint32_t total = 0;
    for (const_iterator it = m_set.cbegin(); it != m_set.cend(); ++it) {
      total += *it;
      x += 1;
    }
    return total / x;
  }

  template< class F >
  void iterate(F f) {
    for (const_iterator it = m_set.cbegin(); it != m_set.cend(); ++it)
      f(*it);
  }

  size_t size() {
    return m_set.size();
  }

  void clear() { m_set.clear(); }

  set m_set;
};

class connection_manager {

public:

  using socket = asio::ip::tcp::socket;

  non_copyable(connection_manager);

  connection_manager(io_context & io)
    : m_io{ io }
  { }

  void start(connection_ptr & c);

  void start(connection_ptr && c) {
    start(c);
  }

  void stop(connection_ptr & c);

  void stop_all();

private:

  io_context & m_io;
  std::unordered_set<connection_ptr> m_connections;
};

constexpr int channel_primary = 0;
constexpr int channel_secondary = 1;
constexpr int channel_wait = 2;

struct connection_data {

  connection_data(logger & l, std::atomic<tree *> & t)
    : lg{ l }, sink{ t }
  { }

  logger & lg;
  std::atomic<tree *> & sink;
};

class connection
  : public std::enable_shared_from_this<connection>
{
public:

  using socket = asio::ip::tcp::socket;

  non_copyable(connection);

  connection(
    socket sock, connection_manager & manager, connection_data && data
  )
    : m_socket{ std::move(sock) }, m_manager{ manager }, lg{ data.lg },
      m_conn_data{ data }, m_index{ 0 }, m_data{ 0 }
  { }

  void start(yield_context yield) {
    read(yield);
  }

  void stop() {
    m_socket.close();
  }

  void started() {
    m_socket.is_open();
  }

private:

  void read(yield_context yield) {

    auto self{ shared_from_this() };
    std::error_code ec;
    std::size_t length = m_socket.async_read_some(buffer(m_buffer), yield[ec]);

    if (!ec) {
      char * p = std::find(m_buffer, m_buffer + length, '\n');
      if (p < m_buffer + length) {
        size_t idx = std::distance(m_buffer, p);
        m_buffer[idx] = '\0';
        m_data = strtoul(m_buffer, nullptr, 10);
        if (errno != ERANGE) {
          std::string s = std::to_string(m_data);
          strncpy(m_buffer, s.data(), s.size());
          m_index = s.size();
          m_buffer[m_index] = '\n';
          ++m_index;
          LOG(info) << "received " << m_data;
          write(yield);
        } else {
          errno = 0;
        }
      }
    } else if (ec != error::operation_aborted) {
      m_manager.stop(self);
      return;
    }
  }

  void write(yield_context yield) {

    auto self{ shared_from_this() };
    std::error_code ec;

    tree * t = m_conn_data.sink.load();
    t->insert(m_data);
    double avg = t->average();

    std::string s = std::to_string(avg);
    s += '\n';
    strncpy(m_buffer, s.data(), s.size());

    m_socket.async_write_some(buffer(m_buffer, s.size()), yield[ec]);

    if (!ec) {
      LOG(info) << "sent " << avg;
      spawn(
        m_socket.get_executor().context(),
        [&](yield_context yield) { read(yield); });
    } else if (ec != error::operation_aborted) {
      m_manager.stop(self);
    }
  }

  socket m_socket;
  connection_manager & m_manager;
  logger & lg;
  connection_data m_conn_data;
  char m_buffer[128];
  size_t m_index;
  uint32_t m_data;
};

void connection_manager::start(connection_ptr & c) {
  m_connections.insert(c);
  spawn(m_io, [this, c](yield_context yield){ c->start(yield); });
}

void connection_manager::stop(connection_ptr & c) {
  m_connections.erase(c);
  c->stop();
}

void connection_manager::stop_all() {
  for (auto & c : m_connections)
    c->stop();
  m_connections.clear();
}

const uint32_t k_default_save_interval{ 20 };

class server
  : public std::enable_shared_from_this<server>
{
public:

  using socket = asio::ip::tcp::socket;

  using value_type = uint32_t;

  server(const std::string & host, std::string & port)
    : m_io{}, m_signals{ m_io }, m_acceptor{ m_io },
      m_save_interval{ k_default_save_interval },
      m_timer{ m_io, boost::posix_time::seconds(m_save_interval) },
      m_manager{ m_io }, m_version{ 0 },
      m_stopped{ false }, m_current{ &m_tree }
  { init(host, port); }

  server(std::string && host, std::string && port)
    : m_io{}, m_signals{ m_io }, m_acceptor{ m_io },
      m_save_interval{ k_default_save_interval },
      m_timer{ m_io, boost::posix_time::seconds(m_save_interval) },
      m_manager{ m_io }, m_version{ 0 },
      m_stopped{ false }, m_current{ &m_tree }
  { init(host, port); }

  void run() {
    m_io.run();
  }

  void stop() {
    m_stopped = true;
    m_acceptor.close();
    m_manager.stop_all();
  }

  io_context & io() { return m_io; }

private:

  void init(const std::string & host, const std::string & port) {

    m_signals.add(SIGINT);
    m_signals.add(SIGTERM);
#   if defined(SIGQUIT)
    m_signals.add(SIGQUIT);
#   endif

    spawn(m_io, [this](yield_context yield) { await_stop(); });

    spawn(m_io, [this, &host, &port](yield_context yield) {
      asio::ip::tcp::resolver resolver{ m_io };
      asio::ip::tcp::endpoint ep = *resolver.resolve({ host, port });
      m_acceptor.open(ep.protocol());
      m_acceptor.set_option(asio::ip::tcp::acceptor::reuse_address(true));
      m_acceptor.bind(ep);
      m_acceptor.listen();
      accept();
    });

    spawn(m_io, [this](yield_context yield) {
      save_tree();
    });

    LOG(info) << "server started";
  }

  void accept() {

    spawn(m_io, [this](yield_context yield) {
      socket sock{ m_io };
      std::error_code ec;
      m_acceptor.async_accept(sock, yield[ec]);
      if (!m_acceptor.is_open()) return;
      if (!ec) {
        connection_data data{ lg, m_current };
        m_manager.start(
          std::make_shared<connection>(
            std::move(sock), m_manager, std::move(data)));
      }
      accept();
    });
  }

  void await_stop() {

    spawn(m_io, [this](yield_context yield){
      m_signals.async_wait(yield);
      stop();
    });
  }

  void save_tree() {

    if (m_stopped) return;

    spawn(m_io, [this](yield_context yield) {
      std::error_code ec;
      m_timer.async_wait(yield[ec]);
      if (ec) {
        save_tree();
      }

      char buf[128];
      std::snprintf(buf, 128UL, "tree_%u.log", m_version);
      ++m_version;
      buf[127] = '\0';
      std::ofstream os(buf, std::ios_base::out | std::ios_base::binary);

      m_current.store(&m_secondary);

      std::thread th {
        [&] {
          m_tree.iterate([&](const tree::value_type & v) { os << v << '\n'; });
          os.close();
          m_current.store(&m_tree);
        }
      };

      th.join();

      m_secondary.iterate([&](const tree::value_type & v) { m_tree.insert(v); });
      m_secondary.clear();

      m_timer.expires_at(
        m_timer.expires_at() + boost::posix_time::seconds(m_save_interval));

      save_tree();
    });
  }

  io_context m_io;
  signal_set m_signals;
  connection_manager m_manager;
  asio::ip::tcp::acceptor m_acceptor;
  uint32_t m_save_interval;
  asio::deadline_timer m_timer;
  tree m_tree;
  tree m_secondary;
  uint32_t m_version;
  bool m_stopped;
  std::atomic<tree *> m_current;

  static logger lg;
};


logger server::lg{ "server" };


#endif /* SERVER_H2_ */
