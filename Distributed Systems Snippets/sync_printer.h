/**
 * Copyright 2016 Kareem Alkaseer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <iostream>

class printer {
	
public:
	
	template< class T, class ...Args >
	static inline void print(const T & t, Args &&... args) {
		guard lock(m_mutex);
		std::cout << t;
		printer::print(std::forward<Args>(args)...);
	}
	
	template< class T, class ...Args >
	static inline void println(const T & t, Args &&... args) {
		guard lock(m_mutex);
		std::cout << t;
		printer::println(std::forward<Args>(args)...);
	}
	
private:
	
	static inline void print() { }
	
	static inline void println() {
		guard lock(m_mutex);
		std::cout << std::endl;
	}
	
	using guard = std::lock_guard<std::recursive_mutex>;
	
	static std::recursive_mutex m_mutex;
};

std::recursive_mutex printer::m_mutex;