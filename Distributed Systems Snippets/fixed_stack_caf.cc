/**
 * Copyright 2016 Kareem Alkaseer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <string>
#include <vector>
#include <iostream>
#include <caf/all.hpp>

using pop_atom = caf::atom_constant<caf::atom("pop")>;
using push_atom = caf::atom_constant<caf::atom("push")>;

template< class T >
class fixed_stack
  : public caf::event_based_actor
{

public:

  using value_type = T;

  explicit fixed_stack(std::size_t max)
    : m_max_size(max)
  {
    m_full.assign(
      [=](push_atom, value_type what) {
        return caf::error_atom::value;
      },
      [=](pop_atom) -> caf::message {
        auto result = m_data.back();
        m_data.pop_back();
        become(m_filled);
        return caf::make_message(caf::ok_atom::value, result);
      }
    );

    m_filled.assign(
      [=](push_atom, value_type what) {
        m_data.push_back(what);
        if (m_data.size() == m_max_size)
          become(m_full);
        return caf::ok_atom::value;
      },
      [=](pop_atom) -> caf::message {
        auto result = m_data.back();
        m_data.pop_back();
        if (m_data.empty())
          become(m_empty);
        return caf::make_message(caf::ok_atom::value, result);
      }
    );

    m_empty.assign(
      [=](push_atom, value_type what) {
        m_data.push_back(what);
        become(m_filled);
        return caf::ok_atom::value;
      },
      [=](pop_atom) {
        return caf::error_atom::value;
      }
    );
  }

  ~fixed_stack() { }

  caf::behavior make_behavior() override {
    return m_empty;
  }

private:

  std::size_t m_max_size;
  std::vector<value_type> m_data;
  caf::behavior m_full;
  caf::behavior m_filled;
  caf::behavior m_empty;
};

template< class T >
class pusher
  : public caf::event_based_actor
{

public:

  using value_type = T;

  explicit pusher(const caf::actor & stack)
    : m_stack(stack)
  {
    m_behavior.assign(
      [=] (T & what) {
        sync_send(m_stack, push_atom::value, what).then(
          [=](caf::ok_atom ok) {
            caf::aout(this) << "pushed " << what << std::endl;
          },
          [=](caf::error_atom e) {
            caf::aout(this) << "full stack" << std::endl;
          }
        );
      }
    );
  }

  ~pusher() { }

  caf::behavior make_behavior() override {
    return m_behavior;
  }

private:

  caf::behavior m_behavior;
  const caf::actor & m_stack;
};

int main() {
  
  size_t concurrency_level = 100000;
  size_t total_requests = 1000000;

  auto stack = caf::spawn<fixed_stack<size_t>>(concurrency_level);

//  caf::scoped_actor pusher;
  std::vector<caf::scoped_actor> pushers{ concurrency_level };
  
  auto t1 = std::chrono::high_resolution_clock::now();

  for (std::size_t i = 0; i < total_requests; ++i) {

    size_t k = i % pushers.size();
    pushers[k]->sync_send(stack, push_atom::value, i).await(
      [&](caf::ok_atom ok) {
        caf::aout(pushers[k]) << "pushed " << i << std::endl;
      },
      [&](caf::error_atom e) {
        caf::aout(pushers[k]) << "full stack" << std::endl;
      },
      caf::after(std::chrono::seconds(3)) >> [&]() { }
    );

    {
      caf::scoped_actor self;
      self->sync_send(stack, pop_atom::value).await(
        [&](caf::ok_atom ok, size_t what) {
          caf::aout(self) << what << std::endl;
        },
        [&](caf::error_atom e) {
          caf::aout(self) << "empty stack" << std::endl;
        },
        caf::after(std::chrono::seconds(3)) >> [&]() { }
      );
    }

  }
  
  auto t2 = std::chrono::high_resolution_clock::now();
  auto ms = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();
  auto sec = std::chrono::duration_cast<std::chrono::seconds>(t2 - t1).count();
  
  std::cout
    << "concurrency level: " << concurrency_level
    << std::endl
    << "microseconds: "
    << ms
    << std::endl
    << "request/second: "
    << total_requests / sec
    << std::endl;

  caf::await_all_actors_done();
  caf::shutdown();

  return 0;
}
